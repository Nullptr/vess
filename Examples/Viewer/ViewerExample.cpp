/*=========================================================================

   Library: iMSTK

   Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
   & Imaging in Medicine, Rensselaer Polytechnic Institute.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0.txt

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

=========================================================================*/

#include "imstkSimulationManager.h"
#include "imstkAPIUtilities.h"
#include "imstkCylinder.h"

using namespace imstk;

///
/// \brief Hello world example
///  Adds basic scene objects, lights, camera and renders
///
int main()
{
    // SDK and Scene
    auto sdk = std::make_shared<SimulationManager>();
    auto sceneTest = sdk->createNewScene("HelloWorld");

    // Add Plane
    //auto planeObj = apiutils::createVisualAnalyticalSceneObject(Geometry::Type::Plane, sceneTest, "VisualPlane", 10);

    // Add Sphere
    //auto sphereObj = apiutils::createVisualAnalyticalSceneObject(Geometry::Type::Sphere, sceneTest, "VisualSphere", 0.3);

    // Add point light (white)
    auto whiteLight = std::make_shared<PointLight>("whiteLight");
    whiteLight->setPosition(Vec3d(50, 80, 50));
    whiteLight->setIntensity(10000);
    sceneTest->addLight(whiteLight);

    // Add spot light (red)
    auto colorLight = std::make_shared<SpotLight>("colorLight");
    colorLight->setPosition(Vec3d(60, 60, 60));
    colorLight->setFocalPoint(Vec3d(0, 0, 0));
    colorLight->setColor(Color::Red);
    colorLight->setIntensity(10000);
    colorLight->setSpotAngle(1);
    //sceneTest->addLight(colorLight);

    // Update Camera
    auto cam1 = sceneTest->getCamera();
    cam1->setPosition(Vec3d(-5.5, 2.5, 32));
    cam1->setFocalPoint(Vec3d(1, 1, 0));

    ////< XZH Step 2: colon-tumor
    std::string pathColon = "I:/iMSTK/resources/VESS/Nick/tumorcolon7_scale2_den.obj";   ////< XZH cubeTet cubeTest tumorcolon7_scale2.obj
    auto objMesh = imstk::MeshIO::read(pathColon);
    auto visualColonMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh);
    //visualColonMesh->computeBoundingBox(min1, max1);
    //visualColonMesh->scale(0.01, imstk::Geometry::TransformType::ApplyToData);
    visualColonMesh->rotate(imstk::UP_VECTOR, imstk::PI, imstk::Geometry::TransformType::ApplyToData);
    //visualColonMesh->translate(0, 0, 0.1, imstk::Geometry::TransformType::ApplyToData);
    //visualColonMesh->computeBoundingBox(min1, max1);
    auto posVisualList = visualColonMesh->getVertexPositions();
    auto triLists = visualColonMesh->getTrianglesVertices();
    visualColonMesh->setVertexPositionsXZH(posVisualList);
    visualColonMesh->setTrianglesVerticesXZH(triLists);

    auto materialWire = std::make_shared<RenderMaterial>();
    auto diffuseTextureWire = std::make_shared<Texture>("I:/iMSTK/resources/VESS/Nick/diffuse2.png", Texture::Type::DIFFUSE);
    auto normalTextureWire = std::make_shared<Texture>("I:/iMSTK/resources/VESS/Nick/normal.png", Texture::Type::NORMAL);
    auto roughnessTextureWire = std::make_shared<Texture>("I:/iMSTK/resources/VESS/Nick/roughness.png", Texture::Type::ROUGHNESS);
    auto metalnessTextureWire = std::make_shared<Texture>("I:/iMSTK/resources/VESS/colontex5r.png", Texture::Type::METALNESS);
    auto sssTextureWire = std::make_shared<Texture>("I:/iMSTK/resources/VESS/colon_texture_mat_sss.png", Texture::Type::SUBSURFACE_SCATTERING);
    materialWire->addTexture(diffuseTextureWire);
    materialWire->addTexture(normalTextureWire);
    materialWire->addTexture(roughnessTextureWire);
    materialWire->addTexture(sssTextureWire);

    auto visualColonMeshModel = std::make_shared<VisualModel>(visualColonMesh);
    //visualColonMeshModel->setRenderMaterial(materialWire);

    auto objectColonV = std::make_shared<imstk::VisualObject>("visualColonMesh0");
    objectColonV->addVisualModel(visualColonMeshModel); // change to any mesh created above
    //sceneTest->addSceneObject(objectColonV);

    ////< XZH test the vertex color
    auto posVisualListTet = visualColonMesh->getVertexPositions();
    for (int i = 0; i < posVisualListTet.size(); i++)
    {
        visualColonMesh->setVertexColor(i, imstk::Color::Blue);
    }

    // Cylinder
    auto visualCylinder = std::make_shared<imstk::Cylinder>();
    auto visualCylinderModel = std::make_shared<VisualModel>(visualCylinder);
    visualCylinder->setLength(30.0);
    visualCylinder->setRadius(4.0);
    auto cylinderMaterial = std::make_shared<RenderMaterial>();
    cylinderMaterial->setColor(Color::Red);
    cylinderMaterial->setBackFaceCulling(false);
    
    visualCylinderModel->setRenderMaterial(cylinderMaterial); // (materialSphere0);
    auto cylinderObj = std::make_shared<VisualObject>("Cylinder1");
    //cylinderObj->addVisualModel(visualCylinderModel);
    cylinderObj->setVisualGeometry(visualCylinder);
    //cylinderObj->getVisualModel(0)->setRenderMaterial(cylinderMaterial);
    sceneTest->addSceneObject(cylinderObj);  ////< XZH KNIFE TIP
    

    // Run
    sdk->setActiveScene(sceneTest);
    sdk->startSimulation(SimulationStatus::INACTIVE);

    return 0;
}
