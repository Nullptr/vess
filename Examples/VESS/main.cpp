//<<<<<<< HEAD
//=======
/*
* DO NOT COMMIT CHANGES TO THE LINE BELOW !!!
* Keep your change local. You can use `git gui`
* to stage or unstage part of a file diff.
*/
#define DATA_ROOT_PATH "I:/iMSTK/resources"

//>>>>>>> CameraNavigation
#include <cstring>
#include <iostream>
#include <memory>
#include <thread>
#include <iomanip>

#include "imstkMath.h"
#include "imstkTimer.h"
#include "imstkSimulationManager.h"

// Objects
#include "imstkForceModelConfig.h"
#include "imstkFEMDeformableBodyModel.h"
#include "imstkDeformableObject.h"
#include "imstkSceneObject.h"
#include "imstkLight.h"
#include "imstkCamera.h"
#include "imstkPBDVirtualCouplingObject.h"
#include "imstkPBDSceneObjectController.h" // xzh
#include "imstkXZHTool.h"

// Time Integrators
#include "imstkBackwardEuler.h"

// Solvers
#include "imstkNonlinearSystem.h"
#include "imstkNewtonSolver.h"
#include "imstkConjugateGradient.h"
#include "imstkPbdSolver.h"

// Geometry
#include "imstkPlane.h"
#include "imstkSphere.h"
#include "imstkCylinder.h"
#include "imstkCube.h"
#include "imstkTetrahedralMesh.h"
#include "imstkSurfaceMesh.h"
#include "imstkMeshIO.h"
#include "imstkLineMesh.h"

// Maps
#include "imstkTetraTriangleMap.h"
#include "imstkIsometricMap.h"
#include "imstkOneToOneMapVESS.h"
#include "imstkOneToOneMap.h"

// Devices
#include "imstkHDAPIDeviceClient.h"
#include "imstkHDAPIDeviceServer.h"
#include "imstkVRPNDeviceClient.h"
#include "imstkVRPNDeviceServer.h"
#include "imstkCameraController.h"
#include "imstkSceneObjectController.h"
#include "imstkLaparoscopicToolController.h"

// Collisions
#include "imstkInteractionPair.h"

// logger
#include "g3log/g3log.hpp"

// imstk utilities
#include "imstkPlotterUtils.h"
#include "imstkAPIUtilities.h"

#include "imstkVirtualCouplingPBDObject.h"
#include "imstkPbdObject.h"

// testVTKTexture
#include <vtkOBJReader.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <string>
#include <vtkJPEGReader.h>

// VTK shader xzh
#include "vtkOpenGLPolyDataMapper.h"
#include "vtkPolyDataNormals.h"
#include "vtkMapper.h"
#include "vtkProperty.h"

// xzh
#include "imstkPBDVirtualCouplingObject.h"
#include "imstkVESSBasicMesh.h"
#include "imstkVESSTetrahedralMesh.h"
#include "imstkVESSTetraTriangleMap.h"
// cutting
//#include "imstkCuttingVESSTetMesh.h"
//#include "imstkCuttingVESSCutMesh.h"
#include "imstkVESSTetCutMesh.h"

#include "imstkGUICanvas.h"
#include "imstkGUIWindow.h"
#include "imstkGUIShapes.h"
#include "imstkGUIText.h"

using namespace imstk;
// test distance 
#include <math.h>
#include <iostream>

void testTetCutColon();   ////< XZH 10/24/2018
void testMarking();
void testCCD();

  ////< XZH Rendering
void testRendering();

  ////*< XZH cut strings for loading
bool cutStrLineBySeparator(string strLine, char separator, vector<string>&strBufferLineVec);

int main()
{
    std::cout << "****************\n"
        << "Starting VESS\n"
        << "****************\n";

    //testTetCutColon();   ////< XZH latest use 20181114

    //testMarking();   ////< XZH latest use 20181114
    //testCCD();

    testRendering();
    return 0;
}

bool cutStrLineBySeparator(string strLine, char separator, vector<string>&strBufferLineVec)
{
	vector<string>::size_type startPos = 0;
	vector<string>::size_type endPos = 0;

	endPos = strLine.find_first_of(separator, startPos);
	while (endPos != string::npos)
	{
		strBufferLineVec.push_back(strLine.substr(startPos, endPos - startPos));
		startPos = strLine.find_first_not_of(separator, endPos + 1);
		endPos = strLine.find_first_of(separator, startPos);
	}
	strBufferLineVec.push_back(strLine.substr(startPos, endPos));
	return true;
}

void testRendering()
{
#ifdef iMSTK_USE_OPENHAPTICS
	////< XZH scale
	double scalexzh = 0.01*0.25;   ////< XZH for scale to the normal, small 100times first, then 4 times again

								   // SDK and Scene
	auto sdk = std::make_shared<imstk::SimulationManager>();
	auto scene = sdk->createNewScene("SceneTestDevice");

	Vec3d minBox, maxBox;
	// Device Client
	auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

	// Device Server
	auto server = std::make_shared<imstk::HDAPIDeviceServer>();
	server->addDeviceClient(client);
	sdk->addModule(server);

	////< XZH Cutting
	std::string path2obj_c = DATA_ROOT_PATH"/VESS/Nick/dualKnife.obj"; //  ITKnife7_4.obj  knife correct ITKnife6  // cube knife knife3.obj";  //  //  113.obj";  // / VESS / TriangleTipKnife2.obj";  // blade2.obj  cube.obj kcube_s2.obj
	auto readMesh = imstk::MeshIO::read(path2obj_c); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
	Vec3d vertexPos3;
	Vec3d min1, max1;
	readMesh->computeBoundingBox(min1, max1);

	auto visualMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(readMesh); // std::make_shared<imstk::SurfaceMesh>(); //  
	visualMesh->rotate(imstk::BACKWARD_VECTOR, -imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);
	visualMesh->computeBoundingBox(min1, max1);
	auto& initPosesTool = visualMesh->getVertexPositionsChangeable();
	//Vec3d moveTmp = Vec3d(-9.093, 1.6348, -8.859199524)*scalexzh;   ////< XZH avg, avg, min  full DOFs
	Vec3d moveTmp = Vec3d(1.6348, -8.859199524, -9.093)*scalexzh + Vec3d(1.2, 0, -0.6)*scalexzh * 4;   ////< XZH avg, avg, min Camera endoscopy
	for (int i = 0; i < initPosesTool.size(); i++)
	{
		Vec3d pos = initPosesTool[i];
		visualMesh->setVertexPosition(i, pos + moveTmp);
	}
	//auto& initPosesToolXZH = visualMesh->getVertexPositionsChangeable();
	visualMesh->setInitialVertexPositions(initPosesTool);
	visualMesh->setInitialVertexPositionsChangeable(initPosesTool);

	visualMesh->computeBoundingBox(min1, max1);
	Vec3d x0, x1;
	x1.z() = (min1.z() + max1.z())*0.5;
	x1.y() = min1.y(); // (min1.y() + max1.y())*0.5;
	x1.x() = (min1.x() + max1.x())*0.5;
	x0.z() = (min1.z() + max1.z())*0.5; // max1.z();
	x0.y() = max1.y(); // (min1.y() + max1.y())*0.5;
	x0.x() = (min1.x() + max1.x())*0.5;

	////< XZH tool1 Marking
	std::string path2obj_c1 = DATA_ROOT_PATH"/VESS/Nick/dualKnife.obj"; // DATA_ROOT_PATH"/VESS/HybridKnife.obj"; // DATA_ROOT_PATH"/VESS/ITKnife7_4H.obj"; //  ITKnife7_4.obj  knife correct ITKnife6  // cube knife knife3.obj";  //  //  113.obj";  // / VESS / TriangleTipKnife2.obj";  // blade2.obj  cube.obj kcube_s2.obj
	auto readMesh1 = imstk::MeshIO::read(path2obj_c1); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
	auto visualMesh1 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(readMesh1); // std::make_shared<imstk::SurfaceMesh>(); //  
	visualMesh1->rotate(imstk::BACKWARD_VECTOR, -imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);  // visualMesh1->rotate(imstk::BACKWARD_VECTOR, -imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);
																											   //visualMesh->rotate(imstk::RIGHT_VECTOR, -imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);
	auto& initPosesTool1 = visualMesh1->getVertexPositionsChangeable();
	for (int i = 0; i < initPosesTool1.size(); i++)
	{
		Vec3d pos = initPosesTool1[i];
		visualMesh1->setVertexPosition(i, pos + moveTmp);
	}
	//auto& initPosesTool1XZH = visualMesh1->getVertexPositionsChangeable();
	visualMesh1->setInitialVertexPositions(initPosesTool1);
	visualMesh1->setInitialVertexPositionsChangeable(initPosesTool1);

	////< XZH tool2 Injecting
	std::string path2obj_c2 = DATA_ROOT_PATH"/VESS/Nick/needleKnifeL.obj"; // DATA_ROOT_PATH"/VESS/HybridKnife.obj"; // DATA_ROOT_PATH"/VESS/ITKnife7_4H.obj"; //  ITKnife7_4.obj  knife correct ITKnife6  // cube knife knife3.obj";  //  //  113.obj";  // / VESS / TriangleTipKnife2.obj";  // blade2.obj  cube.obj kcube_s2.obj
	auto readMesh2 = imstk::MeshIO::read(path2obj_c2); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
	auto visualMesh2 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(readMesh2); // std::make_shared<imstk::SurfaceMesh>(); //  
	visualMesh2->rotate(imstk::BACKWARD_VECTOR, -imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);  // visualMesh2->rotate(imstk::UP_VECTOR, imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);
																											   //visualMesh->rotate(imstk::RIGHT_VECTOR, -imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);
	auto& initPosesTool2 = visualMesh2->getVertexPositionsChangeable();
	for (int i = 0; i < initPosesTool2.size(); i++)
	{
		Vec3d pos = initPosesTool2[i];
		visualMesh2->setVertexPosition(i, pos + moveTmp);
	}
	visualMesh2->setInitialVertexPositions(initPosesTool2);
	visualMesh2->setInitialVertexPositionsChangeable(initPosesTool2);

	visualMesh2->computeBoundingBox(min1, max1);

	////< XZH overtube Tool binding with Camera
	std::string path2obj_overtube = DATA_ROOT_PATH"/VESS/Nick/overtubePlane.obj";
	auto readMeshOvertube = imstk::MeshIO::read(path2obj_overtube);
	auto visualMeshOvertube = std::dynamic_pointer_cast<imstk::SurfaceMesh>(readMeshOvertube);
	visualMeshOvertube->rotate(imstk::BACKWARD_VECTOR, -imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);
	visualMeshOvertube->computeBoundingBox(min1, max1);
	//visualMeshOvertube->rotate(imstk::BACKWARD_VECTOR, std::atan2(2,1), imstk::Geometry::TransformType::ApplyToData);
	auto& initPosesToolTube = visualMeshOvertube->getVertexPositionsChangeable();
	Vec3d moveTmpOT = Vec3d(1.6348, -8.859199524, -9.093)*scalexzh+Vec3d(-0.1, 0.2, -0.0)*scalexzh * 4; // +Vec3d(0.3, -0.1, -0.0)*scalexzh * 4;
	for (int i = 0; i < initPosesToolTube.size(); i++)
	{
		Vec3d pos = initPosesToolTube[i];
		visualMeshOvertube->setVertexPosition(i, pos + moveTmpOT);
	}
	//visualMeshOvertube->rotate(imstk::BACKWARD_VECTOR, std::atan2(-0.00, 1.0), imstk::Geometry::TransformType::ApplyToData);  // std::atan2(-0.16, 1.0), init
	visualMeshOvertube->setInitialVertexPositions(initPosesToolTube);
	visualMeshOvertube->setInitialVertexPositionsChangeable(initPosesToolTube);
	auto materialOvertube = std::make_shared<RenderMaterial>();
	materialOvertube->setColor(imstk::Color::LightGray);
	materialOvertube->setDisplayMode(imstk::RenderMaterial::SURFACE);
	materialOvertube->setEmissivity(-1);
	auto overtubeVisualModel = std::make_shared<VisualModel>(visualMeshOvertube);
	overtubeVisualModel->setRenderMaterial(materialOvertube);
	auto objectOvertube = std::make_shared<imstk::SceneObject>("OvertubeScene");
	objectOvertube->addVisualModel(overtubeVisualModel);
	//scene->addSceneObject(objectOvertube);

	// Line mesh
	auto lineMesh = std::make_shared<imstk::LineMesh>();
	imstk::StdVectorOfVec3d vertList;
	vertList.resize(2);  // using 5 segment
	vertList[0] = x0; // imstk::Vec3d(-14.4, -1.4, 7.9);
	vertList[1] = x1; // imstk::Vec3d(-7.8, -1.2, 7.9);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
	double lineLen = (x0 - x1).norm(); // offset for colliding mesh 20190228
	vertList[0] = Vec3d(0, 0, 0) + lineLen*Vec3d(0, 1, 0) + Vec3d(1.0, 0, -0.0)*scalexzh * 4; // *Vec3d(0, 0, 1)*scalexzh;
	vertList[1] = Vec3d(0, 0, 0) + Vec3d(1.0, 0, -0.0)*scalexzh * 4;
	Vec3d moveLine = vertList[1] - x1;
	//std::vector<std::vector<int> > connectivity;
	std::vector<LineMesh::LineArray> connectivity;
	for (int i = 0; i < 1;) {
		//std::vector<int> line;
		LineMesh::LineArray line;
		line[0] = i;
		//line.push_back(i);
		i++;
		line[1] = i;
		//line.push_back(i);

		connectivity.push_back(line);
	}

	lineMesh->setInitialVertexPositions(vertList);
	lineMesh->setVertexPositions(vertList);
	lineMesh->setLinesVertices(connectivity); //  setConnectivity(connectivity);
											  //lineMesh->rotate(imstk::UP_VECTOR, imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);

	auto physicsMesh = lineMesh; //visualMesh; //  lineMesh; //  imstk::MeshIO::read(path2obj_c);  //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
	auto collidingMesh = lineMesh; // visualMesh; //  lineMesh;
								   //visualMesh->setRotation(Vec3d(0, 0, 1),PI/4); // move

	auto material0 = std::make_shared<RenderMaterial>();
	auto diffuseTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/texture/DualKnife_BaseColor.png", Texture::Type::DIFFUSE);
	auto normalTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/texture/DualKnife_Normal.png", Texture::Type::NORMAL);
	auto roughnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/texture/DualKnife_Roughness.png", Texture::Type::ROUGHNESS);
	auto metalnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/texture/DualKnife_Metallic.png", Texture::Type::METALNESS);
	auto aoTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/texture/DualKnife_Occlusion.png", Texture::Type::AMBIENT_OCCLUSION);
	material0->addTexture(diffuseTexture0);
	material0->addTexture(normalTexture0);
	material0->addTexture(roughnessTexture0);
	material0->addTexture(metalnessTexture0);
	material0->addTexture(aoTexture0);
	material0->setDisplayMode(imstk::RenderMaterial::SURFACE);

	auto material01 = std::make_shared<RenderMaterial>();
	auto diffuseTexture01 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/texture/InjectionNeedle_BaseColor.png", Texture::Type::DIFFUSE);
	auto normalTexture01 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/texture/InjectionNeedle_Normal.png", Texture::Type::NORMAL);
	auto roughnessTexture01 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/texture/InjectionNeedle_Roughness.png", Texture::Type::ROUGHNESS);
	auto metalnessTexture01 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/texture/InjectionNeedle_Metallic.png", Texture::Type::METALNESS);
	auto aoTexture01 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/texture/InjectionNeedle_Occlusion.png", Texture::Type::AMBIENT_OCCLUSION);
	material01->addTexture(diffuseTexture01);
	material01->addTexture(normalTexture01);
	material01->addTexture(roughnessTexture01);
	material01->addTexture(metalnessTexture01);
	material01->addTexture(aoTexture01);
	material01->setDisplayMode(imstk::RenderMaterial::SURFACE);

	auto visualMeshModel = std::make_shared<VisualModel>(visualMesh);
	auto visualMeshModel1 = std::make_shared<VisualModel>(visualMesh1);
	auto visualMeshModel2 = std::make_shared<VisualModel>(visualMesh2);

	visualMeshModel->setRenderMaterial(material0);
	visualMeshModel1->setRenderMaterial(material0);
	visualMeshModel2->setRenderMaterial(material01);

	auto object = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject");

	auto objMapP2V = std::make_shared<imstk::OneToOneMapVESS>();  // std::make_shared<imstk::TriangleLineMap>(); //  std::make_shared<imstk::OneToOneMapVESS>();
	objMapP2V->setMaster(physicsMesh);
	objMapP2V->setSlave(visualMesh);
	objMapP2V->compute();

	auto objMapP2C = std::make_shared<imstk::OneToOneMapVESS>();  //  colliding is physical mesh
	objMapP2C->setMaster(physicsMesh);
	objMapP2C->setSlave(collidingMesh);
	objMapP2C->compute();

	auto objMapC2V = std::make_shared<imstk::OneToOneMapVESS>(); //  std::make_shared<imstk::TriangleLineMap>(); //  s std::make_shared<imstk::OneToOneMapVESS>();
	objMapC2V->setMaster(collidingMesh);
	objMapC2V->setSlave(visualMesh);
	objMapC2V->compute();

	auto objMapC2P = std::make_shared<imstk::OneToOneMapVESS>(); // colliding is physical mesh
	objMapC2P->setMaster(collidingMesh);
	objMapC2P->setSlave(physicsMesh);
	objMapC2P->compute();

	object->addVisualModel(visualMeshModel);
	object->setCollidingGeometry(collidingMesh);
	object->setPhysicsGeometry(physicsMesh);
	object->setPhysicsToCollidingMap(objMapP2C);
	object->setPhysicsToVisualMap(objMapP2V);
	object->setCollidingToVisualMap(objMapC2V);
	object->setColldingToPhysicsMap(objMapC2P);

	Vec3d pos0 = collidingMesh->getVertexPosition(0);
	auto& posList = visualMesh->getVertexPositionsChangeable();
	std::vector<Vec3d>& dirGapVisualDissecting0 = object->getGapVisualDissecting();
	for (int i = 0; i < posList.size(); i++)
	{
		dirGapVisualDissecting0.push_back(posList[i] - pos0);
	}

	////< XZH tool0
	object->setVisualMesh(visualMesh, 0);
	object->setMaps(objMapP2V, objMapP2C, objMapC2V, objMapC2P, 0);

	auto pbdModel = std::make_shared<PbdModel>();
	pbdModel->setModelGeometry(physicsMesh);
	pbdModel->configure(/*Number of Constraints*/1,
		/*Constraint configuration*/"Distance 0.1",
		/*Mass*/0.0,
		/*Gravity*/"0 0 0",
		/*TimeStep*/0.0003,
		/*FixedPoint*/"",
		/*NumberOfIterationInConstraintSolver*/2, // 5
		/*Proximity*/0.003, //0.3, // use linemesh 0.3  // 0.01 use  //  cube 0.1
		/*Contact stiffness*/0.001);
	object->setDynamicalModel(pbdModel);
	scene->addSceneObject(object);

	////< XZH tool1
	auto object11 = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject1");

	auto objMapP2V1 = std::make_shared<imstk::OneToOneMapVESS>();  // std::make_shared<imstk::TriangleLineMap>(); //  std::make_shared<imstk::OneToOneMapVESS>();
	objMapP2V1->setMaster(physicsMesh);
	objMapP2V1->setSlave(visualMesh1);
	objMapP2V1->compute();

	auto objMapP2C1 = std::make_shared<imstk::OneToOneMapVESS>();  //  colliding is physical mesh
	objMapP2C1->setMaster(physicsMesh);
	objMapP2C1->setSlave(collidingMesh);
	objMapP2C1->compute();

	auto objMapC2V1 = std::make_shared<imstk::OneToOneMapVESS>(); //  std::make_shared<imstk::TriangleLineMap>(); //  s std::make_shared<imstk::OneToOneMapVESS>();
	objMapC2V1->setMaster(collidingMesh);
	objMapC2V1->setSlave(visualMesh1);
	objMapC2V1->compute();

	auto objMapC2P1 = std::make_shared<imstk::OneToOneMapVESS>(); // colliding is physical mesh
	objMapC2P1->setMaster(collidingMesh);
	objMapC2P1->setSlave(physicsMesh);
	objMapC2P1->compute();

	object11->addVisualModel(visualMeshModel1);
	object11->setCollidingGeometry(collidingMesh);
	object11->setPhysicsGeometry(physicsMesh);
	object11->setPhysicsToCollidingMap(objMapP2C1);
	object11->setPhysicsToVisualMap(objMapP2V1);
	object11->setCollidingToVisualMap(objMapC2V1);
	object11->setColldingToPhysicsMap(objMapC2P1);

	////< XZH tool0
	object11->setVisualMesh(visualMesh1, 1);
	object11->setMaps(objMapP2V1, objMapP2C1, objMapC2V1, objMapC2P1, 1);

	auto pbdModel1 = std::make_shared<PbdModel>();
	pbdModel1->setModelGeometry(physicsMesh);
	pbdModel1->configure(/*Number of Constraints*/1,
		/*Constraint configuration*/"Distance 0.1",
		/*Mass*/0.0,
		/*Gravity*/"0 0 0",
		/*TimeStep*/0.0003,
		/*FixedPoint*/"",
		/*NumberOfIterationInConstraintSolver*/2, // 5
		/*Proximity*/0.003, //0.3, // use linemesh 0.3  // 0.01 use  //  cube 0.1
		/*Contact stiffness*/0.001);
	object11->setDynamicalModel(pbdModel1);
	scene->addSceneObject(object11);

	////< XZH tool2
	auto object12 = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject2");

	auto objMapP2V2 = std::make_shared<imstk::OneToOneMapVESS>();  // std::make_shared<imstk::TriangleLineMap>(); //  std::make_shared<imstk::OneToOneMapVESS>();
	objMapP2V2->setMaster(physicsMesh);
	objMapP2V2->setSlave(visualMesh2);
	objMapP2V2->compute();

	auto objMapP2C2 = std::make_shared<imstk::OneToOneMapVESS>();  //  colliding is physical mesh
	objMapP2C2->setMaster(physicsMesh);
	objMapP2C2->setSlave(collidingMesh);
	objMapP2C2->compute();

	auto objMapC2V2 = std::make_shared<imstk::OneToOneMapVESS>(); //  std::make_shared<imstk::TriangleLineMap>(); //  s std::make_shared<imstk::OneToOneMapVESS>();
	objMapC2V2->setMaster(collidingMesh);
	objMapC2V2->setSlave(visualMesh2);
	objMapC2V2->compute();

	auto objMapC2P2 = std::make_shared<imstk::OneToOneMapVESS>(); // colliding is physical mesh
	objMapC2P2->setMaster(collidingMesh);
	objMapC2P2->setSlave(physicsMesh);
	objMapC2P2->compute();

	object12->addVisualModel(visualMeshModel2);
	object12->setCollidingGeometry(collidingMesh);
	object12->setPhysicsGeometry(physicsMesh);
	object12->setPhysicsToCollidingMap(objMapP2C2);
	object12->setPhysicsToVisualMap(objMapP2V2);
	object12->setCollidingToVisualMap(objMapC2V2);
	object12->setColldingToPhysicsMap(objMapC2P2);

	////< XZH tool0
	object->setVisualMesh(visualMesh2, 1);
	object->setMaps(objMapP2V2, objMapP2C2, objMapC2V2, objMapC2P2, 1);

	auto pbdModel2 = std::make_shared<PbdModel>();
	pbdModel2->setModelGeometry(physicsMesh);
	pbdModel2->configure(/*Number of Constraints*/1,
		/*Constraint configuration*/"Distance 0.1",
		/*Mass*/0.0,
		/*Gravity*/"0 0 0",
		/*TimeStep*/0.0003,
		/*FixedPoint*/"",
		/*NumberOfIterationInConstraintSolver*/2, // 5
		/*Proximity*/0.003, //0.3,  // use linemesh 0.3  // 0.01 use  //  cube 0.1
		/*Contact stiffness*/0.001);
	object12->setDynamicalModel(pbdModel2);
	scene->addSceneObject(object12);

	////< XZH OverTube
	auto objectOT = std::make_shared<imstk::PbdVirtualCouplingObject>("Overtube");

	auto objMapP2VOT = std::make_shared<imstk::OneToOneMapVESS>();
	objMapP2VOT->setMaster(physicsMesh);
	objMapP2VOT->setSlave(visualMeshOvertube);
	objMapP2VOT->compute();

	auto objMapP2COT = std::make_shared<imstk::OneToOneMapVESS>();
	objMapP2COT->setMaster(physicsMesh);
	objMapP2COT->setSlave(collidingMesh);
	objMapP2COT->compute();

	auto objMapC2VOT = std::make_shared<imstk::OneToOneMapVESS>();
	objMapC2VOT->setMaster(collidingMesh);
	objMapC2VOT->setSlave(visualMeshOvertube);
	objMapC2VOT->compute();

	auto objMapC2POT = std::make_shared<imstk::OneToOneMapVESS>();
	objMapC2POT->setMaster(collidingMesh);
	objMapC2POT->setSlave(physicsMesh);
	objMapC2POT->compute();

	objectOT->addVisualModel(overtubeVisualModel);
	objectOT->setCollidingGeometry(collidingMesh);
	objectOT->setPhysicsGeometry(physicsMesh);
	objectOT->setPhysicsToCollidingMap(objMapP2COT);
	objectOT->setPhysicsToVisualMap(objMapP2VOT);
	objectOT->setCollidingToVisualMap(objMapC2VOT);
	objectOT->setColldingToPhysicsMap(objMapC2POT);

	////< XZH tool0
	object->setVisualMesh(visualMeshOvertube, 1);
	object->setMaps(objMapP2VOT, objMapP2COT, objMapC2VOT, objMapC2POT, 1);

	auto pbdModelOT = std::make_shared<PbdModel>();
	pbdModelOT->setModelGeometry(physicsMesh);
	pbdModelOT->configure(/*Number of Constraints*/1,
		/*Constraint configuration*/"Distance 0.1",
		/*Mass*/0.0,
		/*Gravity*/"0 0 0",
		/*TimeStep*/0.0003,
		/*FixedPoint*/"",
		/*NumberOfIterationInConstraintSolver*/2, // 5
		/*Proximity*/0.003, //0.3,  // use linemesh 0.3  // 0.01 use  //  cube 0.1
		/*Contact stiffness*/0.001);
	objectOT->setDynamicalModel(pbdModelOT);
	scene->addSceneObject(objectOT);

	auto trackCtrl = std::make_shared<imstk::DeviceTracker>(client);
	trackCtrl->setTranslationScaling(0.0010); // (0.025);  // 0.02 0.25 // 0.1  test 0.1 should use  Scale 0.001 will be better 
											  //auto controller = std::make_shared<imstk::PBDSceneObjectController>(object, trackCtrl);
	auto controller = std::make_shared<imstk::PBDSceneObjectController>(object11, object12, object, trackCtrl);
	controller->setActived(true);
	controller->setToolIndex(0);
	//scene->addObjectController(controller);

	auto controller1 = std::make_shared<imstk::PBDSceneObjectController>(object12, trackCtrl);
	controller1->setActived(false);
	controller1->setToolIndex(1);
	//scene->addObjectController(controller1);

	// Update Camera position
	auto cam = scene->getCamera();
	cam->setPosition(Vec3d(0, 0, 10 * scalexzh));
	// Update Camera
	auto cam1 = scene->getCamera();
	cam1->setPosition(Vec3d(0.0258, 0.1502, 0.2136)); // (Vec3d(0.031, 0.0677, 0.233)); //  (Vec3d(3.4, 10.0, 10.3)); // (Vec3d(0.0, 57.0, 48)); //  (imstk::Vec3d(-12.5, -7, -33)); // -25, -8, -4.5
	cam1->setFocalPoint(Vec3d(0.025, 0.0028, 0.215)); // (Vec3d(0.067, 0.2287, 0.096)); // (Vec3d(3.0, -31.6, -26.1)); // (Vec3d(0.0, -30, -28));
	cam1->setFieldOfView(90); //  (75);
	cam1->setViewUp(0, 1, 0);
	// Light (white)
	auto whiteLight = std::make_shared<imstk::SpotLight>("whiteLight");
	//whiteLight->setPosition(imstk::Vec3d(-2.011496, 3, 3.486862) + move); //(imstk::Vec3d(-2.111103, 5.906062, -24.198272) + move);
	whiteLight->setPosition(Vec3d(0.025, .092, 0.235));  // whiteLight->setPosition(Vec3d(2.5, 9.2, 23.5)); // 5.4)); // (imstk::Vec3d(6.871600, 13, 10.749434) + move); // (imstk::Vec3d(-2.111103, 5.906062, -24.198272) + move);
	whiteLight->setIntensity(0.002);//810;	// whiteLight->setCastsShadow(false);
	scene->addLight(whiteLight);
	//auto lightSphere = apiutils::createVisualAnalyticalSceneObject(
	//	imstk::Geometry::Type::Sphere, scene, "lightSphere", 1.0, Vec3d(0.025, .092, 0.235));
	//lightSphere->getVisualGeometry()->setTranslation(whiteLight->getPosition());

	auto camControllerInput = std::make_shared<XZHTool>(object11, object12, object, objectOT, whiteLight, *cam, client);  ////< XZH only camera controller std::make_shared<CameraController>(*cam, client);
	int mm = 1;
	// Set camera controller
	auto camController = cam->setController(camControllerInput);

	//// Sphere Test cutting surface middle
	//auto sphereDissectObj = apiutils::createCollidingAnalyticalSceneObject(
	//    imstk::Geometry::Type::Sphere, scene, "SphereDissect", 0.05, Vec3d(0, 0.095, 0.09)); // -2.23
	//sphereDissectObj->getVisualGeometry()->setTranslation(Vec3d(0, 0.095, 0.09));
	//Vec3d xzhDissectSphere = Vec3d(0, 0.095, 0.09) + 0.02 * Vec3d(0.22872038256899530, -0.33268419282762951, -0.91488153027598118);   ////< XZH normal of dissecting plane
	//auto sphereDissect2Obj = apiutils::createCollidingAnalyticalSceneObject(
	//    imstk::Geometry::Type::Sphere, scene, "SphereDissect2", 0.05, xzhDissectSphere); // -2.23
	//sphereDissect2Obj->getVisualGeometry()->setTranslation(xzhDissectSphere);

	////< XZH Axis |||||
	//auto sphereOrg = apiutils::createCollidingAnalyticalSceneObject(
	//    imstk::Geometry::Type::Sphere, scene, "SphereOrg", 0.05, Vec3d(0, 0, 0));
	//sphereOrg->getVisualGeometry()->setTranslation(Vec3d(0, 0, 0));
	auto materialOrg = std::make_shared<RenderMaterial>();
	auto diffTexOrg = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/yellow.png", Texture::Type::DIFFUSE);
	materialOrg->addTexture(diffTexOrg);
	auto sphereOrg = std::make_shared<imstk::Sphere>();
	sphereOrg->setTranslation(Vec3d(0.0, 0, 0));
	sphereOrg->setRadius(0.01);
	auto sphereOrgModel = std::make_shared<VisualModel>(sphereOrg);
	sphereOrgModel->setRenderMaterial(materialOrg);
	auto sphereOOO = std::make_shared<SceneObject>("SphereOrg");
	sphereOOO->addVisualModel(sphereOrgModel);
	scene->addSceneObject(sphereOOO);

	auto materialX = std::make_shared<RenderMaterial>();
	auto diffTexX = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/red.png", Texture::Type::DIFFUSE);
	materialX->addTexture(diffTexX);
	auto sphereX = std::make_shared<imstk::Sphere>();
	sphereX->setTranslation(Vec3d(0.4, 0, 0));
	sphereX->setRadius(0.01);
	auto sphereXModel = std::make_shared<VisualModel>(sphereX);
	sphereXModel->setRenderMaterial(materialX);
	auto sphereXXX = std::make_shared<SceneObject>("SphereX");
	sphereXXX->addVisualModel(sphereXModel);
	scene->addSceneObject(sphereXXX);

	auto materialY = std::make_shared<RenderMaterial>();
	auto diffTexY = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/green.png", Texture::Type::DIFFUSE);
	materialY->addTexture(diffTexY);
	auto sphereY = std::make_shared<imstk::Sphere>();
	sphereY->setTranslation(Vec3d(0, 0.4, 0));
	sphereY->setRadius(0.01);
	auto sphereYModel = std::make_shared<VisualModel>(sphereY);
	sphereYModel->setRenderMaterial(materialY);
	auto sphereYYY = std::make_shared<SceneObject>("SphereY");
	sphereYYY->addVisualModel(sphereYModel);
	scene->addSceneObject(sphereYYY);

	auto materialZ = std::make_shared<RenderMaterial>();
	auto diffTexZ = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/blue.png", Texture::Type::DIFFUSE);
	materialZ->addTexture(diffTexZ);
	auto sphereZ = std::make_shared<imstk::Sphere>();
	sphereZ->setTranslation(Vec3d(0, 0, 0.4));
	sphereZ->setRadius(0.01);
	auto sphereZModel = std::make_shared<VisualModel>(sphereZ);
	sphereZModel->setRenderMaterial(materialZ);
	auto sphereZZZ = std::make_shared<SceneObject>("SphereZ");
	sphereZZZ->addVisualModel(sphereZModel);
	scene->addSceneObject(sphereZZZ);

	// Sphere1 ROOT? of tool
	//auto sphere1Obj = apiutils::createCollidingAnalyticalSceneObject(
	//    imstk::Geometry::Type::Sphere, scene, "Sphere1", 0.003, Vec3d(-14.4, -1.4, 7.9)); // -2.23
	//sphere1Obj->getVisualGeometry()->setTranslation(Vec3d(-14.4, -1.4, 7.9));   ////< XZH KNIFE ROOT
	auto visualSphere11 = std::make_shared<imstk::Sphere>();
	visualSphere11->setTranslation(Vec3d(-14.4, -1.4, 7.9)*scalexzh);
	visualSphere11->setRadius(0.0005);
	auto visualSphere11Model = std::make_shared<VisualModel>(visualSphere11);
	visualSphere11Model->setRenderMaterial(materialY); // (materialSphere0);
	auto sphere1Obj = std::make_shared<CollidingObject>("Sphere1");
	sphere1Obj->setCollidingGeometry(visualSphere11);
	sphere1Obj->addVisualModel(visualSphere11Model);
	//scene->addSceneObject(sphere1Obj);  ////< XZH KNIFE TIP

										////< XZH Tip of tool
										//auto materialSphere0 = std::make_shared<RenderMaterial>();
										//auto diffuseTextureSphere0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/testtest.png", Texture::Type::DIFFUSE);
										//materialSphere0->addTexture(diffuseTextureSphere0);
	auto visualSphere22 = std::make_shared<imstk::Sphere>();
	visualSphere22->setTranslation(Vec3d(-7.8, -1.2, 7.9));
	visualSphere22->setRadius(0.0005);
	Vec3d mm2 = visualSphere22->getPosition();
	auto sphere2Obj = std::make_shared<CollidingObject>("Sphere2");
	auto visualSphere22Model = std::make_shared<VisualModel>(visualSphere22);
	visualSphere22Model->setRenderMaterial(materialX); // (materialSphere0);
	sphere2Obj->setCollidingGeometry(visualSphere22);
	sphere2Obj->addVisualModel(visualSphere22Model);
	//scene->addSceneObject(sphere2Obj);  ////< XZH KNIFE TIP

										////< XZH draw tool
	auto visualSphere11d = std::make_shared<imstk::Sphere>();
	visualSphere11d->setTranslation(Vec3d(-14.4, -1.4, 7.9)*scalexzh);
	visualSphere11d->setRadius(0.0025);
	auto sphere1dObj = std::make_shared<CollidingObject>("Sphere1d");
	auto visualSphere11dModel = std::make_shared<VisualModel>(visualSphere11d);
	visualSphere11dModel->setRenderMaterial(materialY); // (materialSphere0);
	sphere1dObj->setCollidingGeometry(visualSphere11d);
	sphere1dObj->addVisualModel(visualSphere11dModel);
	//scene->addSceneObject(sphere1dObj);  ////< XZH KNIFE TIP

	auto visualSphere22d = std::make_shared<imstk::Sphere>();
	visualSphere22d->setTranslation(Vec3d(-7.8, -1.2, 7.9));
	visualSphere22d->setRadius(0.001); //  (0.0025);
	auto sphere2dObj = std::make_shared<CollidingObject>("Sphere2d");
	auto visualSphere22dModel = std::make_shared<VisualModel>(visualSphere22d);
	visualSphere22dModel->setRenderMaterial(materialX); // (materialSphere0);
	sphere2dObj->setCollidingGeometry(visualSphere22d);
	sphere2dObj->addVisualModel(visualSphere22dModel);
	//scene->addSceneObject(sphere2dObj);  ////< XZH KNIFE TIP

	////< XZH for tool draw position

	auto probe = std::make_shared<IBLProbe>(DATA_ROOT_PATH "/VESS/texture/organIrradiance.dds",
		DATA_ROOT_PATH "/VESS/texture/organRadiance.dds",
		iMSTK_DATA_ROOT "/IBL/roomBRDF.png");
	scene->setGlobalIBLProbe(probe);

	Vec3d move = Vec3d(0, -5, 15);  // Vec3d(0, 35, 15);
	////< XZH Step 1 END

	////< XZH Step3: Run
	sdk->setActiveScene(scene);
	//  ////< XZH key switch
	auto viewer = std::dynamic_pointer_cast<VulkanViewer>(sdk->getViewer());
	//material01->setEmissivity(-1);
	//materialWire->setEmissivity(-1);
	//material->setEmissivity(-1);
	viewer->setResolution(920, 920); // 1000, 920
	viewer->setResolutionScale(5);

	//viewer->enableLensDistortion(0.000000001);
	//viewer->setResolution(1920, 1080);   ////< XZH 1920*1080
	//viewer->enableFullscreen();
	sdk->startSimulation(SimulationStatus::INACTIVE);
#endif
}

  ////< XZH marking with real camera manipulation
void testMarking()
{
#ifdef iMSTK_USE_OPENHAPTICS
	////< XZH scale
	double scalexzh = 0.01*0.25;   ////< XZH for scale to the normal, small 100times first, then 4 times again

								   // SDK and Scene
	auto sdk = std::make_shared<imstk::SimulationManager>();
	auto scene = sdk->createNewScene("SceneTestDevice");
    auto canvas = sdk->getViewer()->getCanvas();

    auto overRectangle = std::make_shared<GUIOverlay::Rectangle>("OverRectangle", 0.0f, 0.0f, 300.0f, 900.0f, Color(0.0f, 0.0f, 0.0f, 1.0f), true);
    
    // Window logic
    auto window = std::make_shared<GUIOverlay::Window>("Test Window", "Test Window", 300.0f, 500.0f, 200.0f, 0.0f);
    auto windowText = std::make_shared<GUIOverlay::Text>("Window Text", "Here shows information for the simulator:", 10.0f, 10.0f);
    auto windowText2 = std::make_shared<GUIOverlay::Text>("Window Text", "Information for the simulator:ID:\nMarking:\n  num:\n\nInjecting:\n  num:\n\nCutting:\n  num:\n\nDissecting:\n  num:\n", 10.0f, 60.0f, 16.0f);
    //window->addWidget(windowText);
    canvas->addWidget(overRectangle);
    //canvas->addWidget(window);
    //canvas->addWidget(windowText);
    canvas->addWidget(windowText2);
	Vec3d minBox, maxBox;
	// Device Client
	auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

	// Device Server
	auto server = std::make_shared<imstk::HDAPIDeviceServer>();
	server->addDeviceClient(client);
	sdk->addModule(server);

    // Cylinder
    double ratio = 0.8;
    imstk::StdVectorOfVec3d vertListCylinder;
    vertListCylinder.resize(2);
    vertListCylinder[0] = imstk::Vec3d(0.0, -0.05, 0); // Bottom
    vertListCylinder[1] = imstk::Vec3d(0, 0.05, 0);  // Top
    auto visualCylinder = std::make_shared<imstk::Cylinder>();
    auto visualCylinderModel = std::make_shared<VisualModel>(visualCylinder);
    visualCylinder->setLength(0.1);
    visualCylinder->setRadius(0.038*ratio);
    visualCylinder->setInitialVertexPositionsChangeable(vertListCylinder);
    visualCylinder->setVertexPositions(vertListCylinder);
    //visualCylinder->rotate(imstk::RIGHT_VECTOR, -0.2810, imstk::Geometry::TransformType::ApplyToData);
    //visualCylinder->rotate(imstk::BACKWARD_VECTOR, -0.28798, imstk::Geometry::TransformType::ApplyToData);
    //visualCylinder->setRotation(imstk::BACKWARD_VECTOR, -0.28798);
    //visualCylinder->translate(Vec3d(0.038 - 0.019 + 0.0006 + 0.0087, 0.088 + 0.055 - 0.01810, 0.226 - 0.0073 + 0.0036), imstk::Geometry::TransformType::ApplyToData);
    //visualCylinder->setTranslation(Vec3d(0.038 - 0.019 + 0.0006 + 0.0087, 0.088 + 0.055 - 0.01810, 0.226 - 0.0073 + 0.0036));
    auto cylinderMaterial = std::make_shared<RenderMaterial>();
    cylinderMaterial->setColor(Color::Blue);
    cylinderMaterial->setBackFaceCulling(false);
    cylinderMaterial->setDisplayMode(RenderMaterial::DisplayMode::WIREFRAME);

    visualCylinderModel->setRenderMaterial(cylinderMaterial); // (materialSphere0);
    auto cylinderObj = std::make_shared<CollidingObject>("Cylinder1");
    cylinderObj->setCollidingGeometry(visualCylinder);
    cylinderObj->addVisualModel(visualCylinderModel);
    //cylinderObj->setVisualGeometry(visualCylinder);
    //cylinderObj->getVisualModel(0)->setRenderMaterial(cylinderMaterial);
    //scene->addSceneObject(cylinderObj);  ////< XZH KNIFE TIP

    // Line mesh for the middle cylinder
    auto lineMeshCylinder = std::make_shared<imstk::LineMesh>();
    LineMesh::LineArray lineCy;
    lineCy[0] = 0;
    lineCy[1] = 1;
    std::vector<LineMesh::LineArray> connectivityCylinder;
    connectivityCylinder.push_back(lineCy);
    lineMeshCylinder->setInitialVertexPositions(vertListCylinder);
    lineMeshCylinder->setVertexPositions(vertListCylinder);
    lineMeshCylinder->setLinesVertices(connectivityCylinder);
    lineMeshCylinder->initialize(vertListCylinder, connectivityCylinder);
    //lineMeshCylinder->rotate(imstk::RIGHT_VECTOR, -0.2810, imstk::Geometry::TransformType::ApplyToData);
    /*lineMeshCylinder->translate(Vec3d(0.038 - 0.019 + 0.0006 + 0.0087, 0.088 + 0.055 - 0.01810, 0.226 - 0.0073 + 0.0036), imstk::Geometry::TransformType::ApplyToData);*/
    auto lineCylinderModel = std::make_shared<VisualModel>(lineMeshCylinder);
    auto materialLineCylinder = std::make_shared<RenderMaterial>();
    materialLineCylinder->setColor(imstk::Color::Red);
    materialLineCylinder->setDisplayMode(RenderMaterial::DisplayMode::WIREFRAME);
	materialLineCylinder->setBlendMode(RenderMaterial::BlendMode::ALPHA);
    lineCylinderModel->setRenderMaterial(materialLineCylinder);
	lineCylinderModel->hide();
    auto lineCylinder = std::make_shared<SceneObject>("LineCylinder");
    lineCylinder->addVisualModel(lineCylinderModel);
    scene->addSceneObject(lineCylinder);

	////< XZH Cutting
	std::string path2obj_c = DATA_ROOT_PATH"/VESS/Nick/dualKnife_v2.obj"; //  ITKnife7_4.obj  knife correct ITKnife6  // cube knife knife3.obj";  //  //  113.obj";  // / VESS / TriangleTipKnife2.obj";  // blade2.obj  cube.obj kcube_s2.obj
	auto readMesh = imstk::MeshIO::read(path2obj_c); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
	Vec3d vertexPos3;
	Vec3d min1, max1;
	readMesh->computeBoundingBox(min1, max1);

	double offsetToolX = 0.1;  // 0.2
	double offsetToolZ = -0.1;  // -0.2
	auto visualMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(readMesh); // std::make_shared<imstk::SurfaceMesh>(); //  
	visualMesh->rotate(imstk::BACKWARD_VECTOR, -imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);
	visualMesh->computeBoundingBox(min1, max1);
	auto& initPosesTool = visualMesh->getVertexPositionsChangeable();
	//Vec3d moveTmp = Vec3d(-9.093, 1.6348, -8.859199524)*scalexzh;   ////< XZH avg, avg, min  full DOFs
	Vec3d moveTmp = Vec3d(1.6348, -8.859199524, -9.093)*scalexzh + Vec3d(offsetToolX, 0, offsetToolZ)*scalexzh * 4;   ////< XZH  1.2  avg, avg, min Camera endoscopy
	for (int i = 0; i < initPosesTool.size(); i++)
	{
		Vec3d pos = initPosesTool[i];
		visualMesh->setVertexPosition(i, pos + moveTmp);
	}
	//auto& initPosesToolXZH = visualMesh->getVertexPositionsChangeable();
	visualMesh->setInitialVertexPositions(initPosesTool);
	visualMesh->setInitialVertexPositionsChangeable(initPosesTool);

	visualMesh->computeBoundingBox(min1, max1);
	Vec3d x0, x1;
	x1.z() = (min1.z() + max1.z())*0.5;
	x1.y() = min1.y(); // (min1.y() + max1.y())*0.5;
	x1.x() = (min1.x() + max1.x())*0.5;
	x0.z() = (min1.z() + max1.z())*0.5; // max1.z();
	x0.y() = max1.y(); // (min1.y() + max1.y())*0.5;
	x0.x() = (min1.x() + max1.x())*0.5;

	////< XZH tool1 Marking
	std::string path2obj_c1 = DATA_ROOT_PATH"/VESS/Nick/dualKnife_v2.obj"; //  dualKnife.obj   DATA_ROOT_PATH"/VESS/HybridKnife.obj"; // DATA_ROOT_PATH"/VESS/ITKnife7_4H.obj"; //  ITKnife7_4.obj  knife correct ITKnife6  // cube knife knife3.obj";  //  //  113.obj";  // / VESS / TriangleTipKnife2.obj";  // blade2.obj  cube.obj kcube_s2.obj
	auto readMesh1 = imstk::MeshIO::read(path2obj_c1); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
	auto visualMesh1 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(readMesh1); // std::make_shared<imstk::SurfaceMesh>(); //  
	visualMesh1->rotate(imstk::BACKWARD_VECTOR, -imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);  // visualMesh1->rotate(imstk::BACKWARD_VECTOR, -imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);
																											   //visualMesh->rotate(imstk::RIGHT_VECTOR, -imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);
	auto& initPosesTool1 = visualMesh1->getVertexPositionsChangeable();
	for (int i = 0; i < initPosesTool1.size(); i++)
	{
		Vec3d pos = initPosesTool1[i];
		visualMesh1->setVertexPosition(i, pos + moveTmp);
	}
	//auto& initPosesTool1XZH = visualMesh1->getVertexPositionsChangeable();
	visualMesh1->setInitialVertexPositions(initPosesTool1);
	visualMesh1->setInitialVertexPositionsChangeable(initPosesTool1);

	////< XZH tool2 Injecting
	std::string path2obj_c2 = DATA_ROOT_PATH"/VESS/Nick/needleKnifeL_v2.obj"; // DATA_ROOT_PATH"/VESS/HybridKnife.obj"; // DATA_ROOT_PATH"/VESS/ITKnife7_4H.obj"; //  ITKnife7_4.obj  knife correct ITKnife6  // cube knife knife3.obj";  //  //  113.obj";  // / VESS / TriangleTipKnife2.obj";  // blade2.obj  cube.obj kcube_s2.obj
	auto readMesh2 = imstk::MeshIO::read(path2obj_c2); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
	auto visualMesh2 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(readMesh2); // std::make_shared<imstk::SurfaceMesh>(); //  
	visualMesh2->rotate(imstk::BACKWARD_VECTOR, -imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);  // visualMesh2->rotate(imstk::UP_VECTOR, imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);
																											   //visualMesh->rotate(imstk::RIGHT_VECTOR, -imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);
	auto& initPosesTool2 = visualMesh2->getVertexPositionsChangeable();
	for (int i = 0; i < initPosesTool2.size(); i++)
	{
		Vec3d pos = initPosesTool2[i];
		visualMesh2->setVertexPosition(i, pos + moveTmp);
	}
	visualMesh2->setInitialVertexPositions(initPosesTool2);
	visualMesh2->setInitialVertexPositionsChangeable(initPosesTool2);

	visualMesh2->computeBoundingBox(min1, max1);

	////< XZH overtube Tool binding with Camera
	std::string path2obj_overtube = DATA_ROOT_PATH"/VESS/Nick/overtube_cylinder.dae";
	auto readMeshOvertube = imstk::MeshIO::read(path2obj_overtube);
	auto visualMeshOvertube = std::dynamic_pointer_cast<imstk::SurfaceMesh>(readMeshOvertube);
	visualMeshOvertube->rotate(imstk::BACKWARD_VECTOR, -imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);
	visualMeshOvertube->computeBoundingBox(min1, max1);
	//visualMeshOvertube->rotate(imstk::BACKWARD_VECTOR, std::atan2(2,1), imstk::Geometry::TransformType::ApplyToData);
	auto& initPosesToolTube = visualMeshOvertube->getVertexPositionsChangeable();
	Vec3d moveTmpOT = Vec3d(1.6348, -8.859199524, -9.093)*scalexzh + Vec3d(-0.1, 0.2, -0.0)*scalexzh * 4; // +Vec3d(0.3, -0.1, -0.0)*scalexzh * 4;
	for (int i = 0; i < initPosesToolTube.size(); i++)
	{
		Vec3d pos = initPosesToolTube[i];
		visualMeshOvertube->setVertexPosition(i, pos + moveTmpOT);
	}
	//visualMeshOvertube->rotate(imstk::BACKWARD_VECTOR, std::atan2(-0.00, 1.0), imstk::Geometry::TransformType::ApplyToData);  // std::atan2(-0.16, 1.0), init
	visualMeshOvertube->setInitialVertexPositions(initPosesToolTube);
	visualMeshOvertube->setInitialVertexPositionsChangeable(initPosesToolTube);
	auto materialOvertube = std::make_shared<RenderMaterial>();
	materialOvertube->setColor(imstk::Color(1.0,1.0,1.0,0.5));
	materialOvertube->setDisplayMode(imstk::RenderMaterial::SURFACE);
	materialOvertube->setTransparency(true);
	materialOvertube->setRoughness(0.1);
	materialOvertube->setBackFaceCulling(false);
	//materialOvertube->setEmissivity(-1);
	auto overtubeVisualModel = std::make_shared<VisualModel>(visualMeshOvertube);
	overtubeVisualModel->setRenderMaterial(materialOvertube);
	auto objectOvertube = std::make_shared<imstk::SceneObject>("OvertubeScene");
	objectOvertube->addVisualModel(overtubeVisualModel);
	//scene->addSceneObject(objectOvertube);

	// Line mesh
	auto lineMesh = std::make_shared<imstk::LineMesh>();
	imstk::StdVectorOfVec3d vertList;
	vertList.resize(7);  // using 5 segment
	vertList[0] = x0; // imstk::Vec3d(-14.4, -1.4, 7.9);
	vertList[1] = x1; // imstk::Vec3d(-7.8, -1.2, 7.9);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
	double lineLen = (x0 - x1).norm(); // offset for colliding mesh 20190228
	vertList[0] = Vec3d(0, 0, 0) + lineLen*Vec3d(0, 1, 0) + Vec3d(offsetToolX, 0, offsetToolZ)*scalexzh * 4;  // + Vec3d(1.0, 0, -0.0)*scalexzh * 4; // *Vec3d(0, 0, 1)*scalexzh;
	vertList[1] = Vec3d(0, 0, 0) + lineLen*Vec3d(0, 0.05, 0) + Vec3d(offsetToolX, 0, offsetToolZ)*scalexzh * 4;  // +  Vec3d(1.0, 0, -0.0)*scalexzh * 4;
	vertList[2] = Vec3d(0, 0, 0) + lineLen * Vec3d(0.07, 0.14, -0.07) + Vec3d(offsetToolX, 0, offsetToolZ)*scalexzh * 4;
	vertList[3] = Vec3d(0, 0, 0) + lineLen * Vec3d(0.07, 0.14, 0.07) + Vec3d(offsetToolX, 0, offsetToolZ)*scalexzh * 4;
	vertList[4] = Vec3d(0, 0, 0) + lineLen * Vec3d(-0.07, 0.14, 0.07) + Vec3d(offsetToolX, 0, offsetToolZ)*scalexzh * 4;
	vertList[5] = Vec3d(0, 0, 0) + lineLen * Vec3d(-0.07, 0.14, -0.07) + Vec3d(offsetToolX, 0, offsetToolZ)*scalexzh * 4;
	vertList[6] = Vec3d(0, 0, 0) + lineLen * Vec3d(-0.20, 0.50, 0.1) + Vec3d(offsetToolX, 0, offsetToolZ)*scalexzh * 4;
	double llenl = (vertList[0]- vertList[1]).norm();
	printf("%.10f\n", llenl);
	Vec3d moveLine = vertList[1] - x1;
	//std::vector<std::vector<int> > connectivity;
	std::vector<LineMesh::LineArray> connectivity;
	for (int i = 0; i < 1;) 
	{
		//std::vector<int> line;
		LineMesh::LineArray line;
		line[0] = i;
		//line.push_back(i);
		i++;
		line[1] = i;
		//line.push_back(i);

		connectivity.push_back(line);
	}

	lineMesh->setInitialVertexPositions(vertList);
	lineMesh->setVertexPositions(vertList);
	lineMesh->setLinesVertices(connectivity); //  setConnectivity(connectivity);
											  //lineMesh->rotate(imstk::UP_VECTOR, imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);

	// Create surface mesh to simulate the surrounding of the tool tip
	std::vector<SurfaceMesh::TriangleArray> triSurroundingList;
	SurfaceMesh::TriangleArray triSurrounding[2];
	triSurrounding[0] = { { 2, 3, 4 } };
	triSurrounding[1] = { { 2, 4, 5 } };
	triSurroundingList.push_back(triSurrounding[0]);
	triSurroundingList.push_back(triSurrounding[1]);
	auto surroundingSurfaceMesh = std::make_shared<SurfaceMesh>();
	surroundingSurfaceMesh->initialize(vertList, triSurroundingList);
	auto surroundingSurfaceMeshModel = std::make_shared<VisualModel>(surroundingSurfaceMesh);
	auto surroundingSurfaceMeshObj = std::make_shared<PbdVirtualCouplingObject>("SurroundingTool");
	surroundingSurfaceMeshModel->setRenderMaterial(materialOvertube);
	//surroundingSurfaceMeshObj->addVisualModel(surroundingSurfaceMeshModel);
	auto objMapP2VSUR = std::make_shared<imstk::OneToOneMapVESS>();  // std::make_shared<imstk::TriangleLineMap>(); //  std::make_shared<imstk::OneToOneMapVESS>();
	objMapP2VSUR->setMaster(surroundingSurfaceMesh);
	objMapP2VSUR->setSlave(surroundingSurfaceMesh);
	objMapP2VSUR->compute();

	auto objMapP2CSUR = std::make_shared<imstk::OneToOneMapVESS>();  //  colliding is physical mesh
	objMapP2CSUR->setMaster(surroundingSurfaceMesh);
	objMapP2CSUR->setSlave(surroundingSurfaceMesh);
	objMapP2CSUR->compute();

	auto objMapC2VSUR = std::make_shared<imstk::OneToOneMapVESS>(); //  std::make_shared<imstk::TriangleLineMap>(); //  s std::make_shared<imstk::OneToOneMapVESS>();
	objMapC2VSUR->setMaster(surroundingSurfaceMesh);
	objMapC2VSUR->setSlave(surroundingSurfaceMesh);
	objMapC2VSUR->compute();

	surroundingSurfaceMeshObj->addVisualModel(surroundingSurfaceMeshModel);
	surroundingSurfaceMeshObj->setCollidingGeometry(surroundingSurfaceMesh);
	surroundingSurfaceMeshObj->setPhysicsGeometry(surroundingSurfaceMesh);
	surroundingSurfaceMeshObj->setPhysicsToCollidingMap(objMapP2CSUR);
	surroundingSurfaceMeshObj->setPhysicsToVisualMap(objMapP2VSUR);
	surroundingSurfaceMeshObj->setCollidingToVisualMap(objMapC2VSUR);
	surroundingSurfaceMeshObj->setVisualMesh(surroundingSurfaceMesh, 0);
	auto pbdModelSUR = std::make_shared<PbdModel>();
	pbdModelSUR->setModelGeometry(surroundingSurfaceMesh);
	pbdModelSUR->configure(/*Number of Constraints*/1,
		/*Constraint configuration*/"Distance 0.1",
		/*Mass*/0.0,
		/*Gravity*/"0 0 0",
		/*TimeStep*/0.0003,
		/*FixedPoint*/"",
		/*NumberOfIterationInConstraintSolver*/2, // 5
		/*Proximity*/0.003, //0.3, // use linemesh 0.3  // 0.01 use  //  cube 0.1
		/*Contact stiffness*/0.001);
	surroundingSurfaceMeshObj->setDynamicalModel(pbdModelSUR);
	//scene->addSceneObject(surroundingSurfaceMeshObj);

	auto physicsMesh = lineMesh; //visualMesh; //  lineMesh; //  imstk::MeshIO::read(path2obj_c);  //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
	auto collidingMesh = lineMesh; // visualMesh; //  lineMesh;
								   //visualMesh->setRotation(Vec3d(0, 0, 1),PI/4); // move

	auto material0 = std::make_shared<RenderMaterial>();
	auto diffuseTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/texture/DualKnife_BaseColor.png", Texture::Type::DIFFUSE);
	auto normalTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/texture/DualKnife_Normal.png", Texture::Type::NORMAL);
	auto roughnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/texture/DualKnife_Roughness.png", Texture::Type::ROUGHNESS);
	auto metalnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/texture/DualKnife_Metallic.png", Texture::Type::METALNESS);
	auto aoTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/texture/DualKnife_Occlusion.png", Texture::Type::AMBIENT_OCCLUSION);
	material0->addTexture(diffuseTexture0);
	material0->addTexture(normalTexture0);
	material0->addTexture(roughnessTexture0);
	material0->addTexture(metalnessTexture0);
	material0->addTexture(aoTexture0);
	material0->setDisplayMode(imstk::RenderMaterial::SURFACE);

	auto material01 = std::make_shared<RenderMaterial>();
	auto diffuseTexture01 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/texture/InjectionNeedle_BaseColor.png", Texture::Type::DIFFUSE);
	auto normalTexture01 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/texture/InjectionNeedle_Normal.png", Texture::Type::NORMAL);
	auto roughnessTexture01 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/texture/InjectionNeedle_Roughness.png", Texture::Type::ROUGHNESS);
	auto metalnessTexture01 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/texture/InjectionNeedle_Metallic.png", Texture::Type::METALNESS);
	auto aoTexture01 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/texture/InjectionNeedle_Occlusion.png", Texture::Type::AMBIENT_OCCLUSION);
	material01->addTexture(diffuseTexture01);
	material01->addTexture(normalTexture01);
	material01->addTexture(roughnessTexture01);
	material01->addTexture(metalnessTexture01);
	material01->addTexture(aoTexture01);
	material01->setDisplayMode(imstk::RenderMaterial::SURFACE);

	auto visualMeshModel = std::make_shared<VisualModel>(visualMesh);
	auto visualMeshModel1 = std::make_shared<VisualModel>(visualMesh1);
	auto visualMeshModel2 = std::make_shared<VisualModel>(visualMesh2);

	visualMeshModel->setRenderMaterial(material0);
	visualMeshModel1->setRenderMaterial(material0);
	visualMeshModel2->setRenderMaterial(material01);

	auto object = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject");

	auto objMapP2V = std::make_shared<imstk::OneToOneMapVESS>();  // std::make_shared<imstk::TriangleLineMap>(); //  std::make_shared<imstk::OneToOneMapVESS>();
	objMapP2V->setMaster(physicsMesh);
	objMapP2V->setSlave(visualMesh);
	objMapP2V->compute();

	auto objMapP2C = std::make_shared<imstk::OneToOneMapVESS>();  //  colliding is physical mesh
	objMapP2C->setMaster(physicsMesh);
	objMapP2C->setSlave(collidingMesh);
	objMapP2C->compute();

	auto objMapC2V = std::make_shared<imstk::OneToOneMapVESS>(); //  std::make_shared<imstk::TriangleLineMap>(); //  s std::make_shared<imstk::OneToOneMapVESS>();
	objMapC2V->setMaster(collidingMesh);
	objMapC2V->setSlave(visualMesh);
	objMapC2V->compute();

	auto objMapC2P = std::make_shared<imstk::OneToOneMapVESS>(); // colliding is physical mesh
	objMapC2P->setMaster(collidingMesh);
	objMapC2P->setSlave(physicsMesh);
	objMapC2P->compute();

	object->addVisualModel(visualMeshModel);
	object->setCollidingGeometry(collidingMesh);
	object->setPhysicsGeometry(physicsMesh);
	object->setPhysicsToCollidingMap(objMapP2C);
	object->setPhysicsToVisualMap(objMapP2V);
	object->setCollidingToVisualMap(objMapC2V);
	object->setColldingToPhysicsMap(objMapC2P);

	Vec3d pos0 = collidingMesh->getVertexPosition(0);
	auto& posList = visualMesh->getVertexPositionsChangeable();
	std::vector<Vec3d>& dirGapVisualDissecting0 = object->getGapVisualDissecting();
	for (int i = 0; i < posList.size(); i++)
	{
		dirGapVisualDissecting0.push_back(posList[i] - pos0);
	}

	////< XZH tool0
	object->setVisualMesh(visualMesh, 0);
	object->setMaps(objMapP2V, objMapP2C, objMapC2V, objMapC2P, 0);

	auto pbdModel = std::make_shared<PbdModel>();
	pbdModel->setModelGeometry(physicsMesh);
	pbdModel->configure(/*Number of Constraints*/1,
		/*Constraint configuration*/"Distance 0.1",
		/*Mass*/0.0,
		/*Gravity*/"0 0 0",
		/*TimeStep*/0.0003,
		/*FixedPoint*/"",
		/*NumberOfIterationInConstraintSolver*/2, // 5
		/*Proximity*/0.003, //0.3, // use linemesh 0.3  // 0.01 use  //  cube 0.1
		/*Contact stiffness*/0.001);
	object->setDynamicalModel(pbdModel);
	scene->addSceneObject(object);

	////< XZH tool1
	auto object11 = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject1");

	auto objMapP2V1 = std::make_shared<imstk::OneToOneMapVESS>();  // std::make_shared<imstk::TriangleLineMap>(); //  std::make_shared<imstk::OneToOneMapVESS>();
	objMapP2V1->setMaster(physicsMesh);
	objMapP2V1->setSlave(visualMesh1);
	objMapP2V1->compute();

	auto objMapP2C1 = std::make_shared<imstk::OneToOneMapVESS>();  //  colliding is physical mesh
	objMapP2C1->setMaster(physicsMesh);
	objMapP2C1->setSlave(collidingMesh);
	objMapP2C1->compute();

	auto objMapC2V1 = std::make_shared<imstk::OneToOneMapVESS>(); //  std::make_shared<imstk::TriangleLineMap>(); //  s std::make_shared<imstk::OneToOneMapVESS>();
	objMapC2V1->setMaster(collidingMesh);
	objMapC2V1->setSlave(visualMesh1);
	objMapC2V1->compute();

	auto objMapC2P1 = std::make_shared<imstk::OneToOneMapVESS>(); // colliding is physical mesh
	objMapC2P1->setMaster(collidingMesh);
	objMapC2P1->setSlave(physicsMesh);
	objMapC2P1->compute();

	object11->addVisualModel(visualMeshModel1);
	object11->setCollidingGeometry(collidingMesh);
	object11->setPhysicsGeometry(physicsMesh);
	object11->setPhysicsToCollidingMap(objMapP2C1);
	object11->setPhysicsToVisualMap(objMapP2V1);
	object11->setCollidingToVisualMap(objMapC2V1);
	object11->setColldingToPhysicsMap(objMapC2P1);

	////< XZH tool0
	object11->setVisualMesh(visualMesh1, 1);
	object11->setMaps(objMapP2V1, objMapP2C1, objMapC2V1, objMapC2P1, 1);

	auto pbdModel1 = std::make_shared<PbdModel>();
	pbdModel1->setModelGeometry(physicsMesh);
	pbdModel1->configure(/*Number of Constraints*/1,
		/*Constraint configuration*/"Distance 0.1",
		/*Mass*/0.0,
		/*Gravity*/"0 0 0",
		/*TimeStep*/0.0003,
		/*FixedPoint*/"",
		/*NumberOfIterationInConstraintSolver*/2, // 5
		/*Proximity*/0.003, //0.3, // use linemesh 0.3  // 0.01 use  //  cube 0.1
		/*Contact stiffness*/0.001);
	object11->setDynamicalModel(pbdModel1);
	scene->addSceneObject(object11);

	////< XZH tool2
	auto object12 = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject2");

	auto objMapP2V2 = std::make_shared<imstk::OneToOneMapVESS>();  // std::make_shared<imstk::TriangleLineMap>(); //  std::make_shared<imstk::OneToOneMapVESS>();
	objMapP2V2->setMaster(physicsMesh);
	objMapP2V2->setSlave(visualMesh2);
	objMapP2V2->compute();

	auto objMapP2C2 = std::make_shared<imstk::OneToOneMapVESS>();  //  colliding is physical mesh
	objMapP2C2->setMaster(physicsMesh);
	objMapP2C2->setSlave(collidingMesh);
	objMapP2C2->compute();

	auto objMapC2V2 = std::make_shared<imstk::OneToOneMapVESS>(); //  std::make_shared<imstk::TriangleLineMap>(); //  s std::make_shared<imstk::OneToOneMapVESS>();
	objMapC2V2->setMaster(collidingMesh);
	objMapC2V2->setSlave(visualMesh2);
	objMapC2V2->compute();

	auto objMapC2P2 = std::make_shared<imstk::OneToOneMapVESS>(); // colliding is physical mesh
	objMapC2P2->setMaster(collidingMesh);
	objMapC2P2->setSlave(physicsMesh);
	objMapC2P2->compute();

	object12->addVisualModel(visualMeshModel2);
	object12->setCollidingGeometry(collidingMesh);
	object12->setPhysicsGeometry(physicsMesh);
	object12->setPhysicsToCollidingMap(objMapP2C2);
	object12->setPhysicsToVisualMap(objMapP2V2);
	object12->setCollidingToVisualMap(objMapC2V2);
	object12->setColldingToPhysicsMap(objMapC2P2);

	////< XZH tool0
	object->setVisualMesh(visualMesh2, 1);
	object->setMaps(objMapP2V2, objMapP2C2, objMapC2V2, objMapC2P2, 1);

	auto pbdModel2 = std::make_shared<PbdModel>();
	pbdModel2->setModelGeometry(physicsMesh);
	pbdModel2->configure(/*Number of Constraints*/1,
		/*Constraint configuration*/"Distance 0.1",
		/*Mass*/0.0,
		/*Gravity*/"0 0 0",
		/*TimeStep*/0.0003,
		/*FixedPoint*/"",
		/*NumberOfIterationInConstraintSolver*/2, // 5
		/*Proximity*/0.003, //0.3,  // use linemesh 0.3  // 0.01 use  //  cube 0.1
		/*Contact stiffness*/0.001);
	object12->setDynamicalModel(pbdModel2);
	scene->addSceneObject(object12);

	////< XZH OverTube
	auto objectOT = std::make_shared<imstk::PbdVirtualCouplingObject>("Overtube");

	auto objMapP2VOT = std::make_shared<imstk::OneToOneMapVESS>();
	objMapP2VOT->setMaster(physicsMesh);
	objMapP2VOT->setSlave(visualMeshOvertube);
	objMapP2VOT->compute();

	auto objMapP2COT = std::make_shared<imstk::OneToOneMapVESS>();
	objMapP2COT->setMaster(physicsMesh);
	objMapP2COT->setSlave(collidingMesh);
	objMapP2COT->compute();

	auto objMapC2VOT = std::make_shared<imstk::OneToOneMapVESS>();
	objMapC2VOT->setMaster(collidingMesh);
	objMapC2VOT->setSlave(visualMeshOvertube);
	objMapC2VOT->compute();

	auto objMapC2POT = std::make_shared<imstk::OneToOneMapVESS>();
	objMapC2POT->setMaster(collidingMesh);
	objMapC2POT->setSlave(physicsMesh);
	objMapC2POT->compute();

	objectOT->addVisualModel(overtubeVisualModel);
	objectOT->setCollidingGeometry(collidingMesh);
	objectOT->setPhysicsGeometry(physicsMesh);
	objectOT->setPhysicsToCollidingMap(objMapP2COT);
	objectOT->setPhysicsToVisualMap(objMapP2VOT);
	objectOT->setCollidingToVisualMap(objMapC2VOT);
	objectOT->setColldingToPhysicsMap(objMapC2POT);

	////< XZH tool0
	object->setVisualMesh(visualMeshOvertube, 1);
	object->setMaps(objMapP2VOT, objMapP2COT, objMapC2VOT, objMapC2POT, 1);

	auto pbdModelOT = std::make_shared<PbdModel>();
	pbdModelOT->setModelGeometry(physicsMesh);
	pbdModelOT->configure(/*Number of Constraints*/1,
		/*Constraint configuration*/"Distance 0.1",
		/*Mass*/0.0,
		/*Gravity*/"0 0 0",
		/*TimeStep*/0.0003,
		/*FixedPoint*/"",
		/*NumberOfIterationInConstraintSolver*/2, // 5
		/*Proximity*/0.003, //0.3,  // use linemesh 0.3  // 0.01 use  //  cube 0.1
		/*Contact stiffness*/0.001);
	objectOT->setDynamicalModel(pbdModelOT);
	scene->addSceneObject(objectOT);

	auto trackCtrl = std::make_shared<imstk::DeviceTracker>(client);
	trackCtrl->setTranslationScaling(0.0010); // (0.025);  // 0.02 0.25 // 0.1  test 0.1 should use  Scale 0.001 will be better 
											  //auto controller = std::make_shared<imstk::PBDSceneObjectController>(object, trackCtrl);
	auto controller = std::make_shared<imstk::PBDSceneObjectController>(object11, object12, object, trackCtrl);
	controller->setActived(true);
	controller->setToolIndex(0);
	//scene->addObjectController(controller);

	auto controller1 = std::make_shared<imstk::PBDSceneObjectController>(object12, trackCtrl);
	controller1->setActived(false);
	controller1->setToolIndex(1);
	//scene->addObjectController(controller1);

	// Update Camera position
	auto cam = scene->getCamera();
	cam->setPosition(Vec3d(0, 0, 10 * scalexzh));
	// Update Camera
	auto cam1 = scene->getCamera();
	cam1->setPosition(Vec3d(0.0258, 0.1502, 0.2136)); // (Vec3d(0.031, 0.0677, 0.233)); //  (Vec3d(3.4, 10.0, 10.3)); // (Vec3d(0.0, 57.0, 48)); //  (imstk::Vec3d(-12.5, -7, -33)); // -25, -8, -4.5
	cam1->setFocalPoint(Vec3d(0.025, 0.0028, 0.215)); // (Vec3d(0.067, 0.2287, 0.096)); // (Vec3d(3.0, -31.6, -26.1)); // (Vec3d(0.0, -30, -28));
	cam1->setFieldOfView(92); // (90);  //  (90); //  (75);
	cam1->setViewUp(0, 1, 0);
	// Light (white)
	auto whiteLight = std::make_shared<imstk::SpotLight>("whiteLight");
	//whiteLight->setPosition(imstk::Vec3d(-2.011496, 3, 3.486862) + move); //(imstk::Vec3d(-2.111103, 5.906062, -24.198272) + move);
	whiteLight->setPosition(Vec3d(0.025, .092, 0.235));  // whiteLight->setPosition(Vec3d(2.5, 9.2, 23.5)); // 5.4)); // (imstk::Vec3d(6.871600, 13, 10.749434) + move); // (imstk::Vec3d(-2.111103, 5.906062, -24.198272) + move);
	whiteLight->setIntensity(0.002);//810;	// whiteLight->setCastsShadow(false);
	scene->addLight(whiteLight);

	auto whiteLightOrg = std::make_shared<imstk::SpotLight>("whiteLightOrg");
	//whiteLight->setPosition(imstk::Vec3d(-2.011496, 3, 3.486862) + move); //(imstk::Vec3d(-2.111103, 5.906062, -24.198272) + move);
	whiteLightOrg->setPosition(Vec3d(0.0, 0.0, 0.0));  // whiteLight->setPosition(Vec3d(2.5, 9.2, 23.5)); // 5.4)); // (imstk::Vec3d(6.871600, 13, 10.749434) + move); // (imstk::Vec3d(-2.111103, 5.906062, -24.198272) + move);
	whiteLightOrg->setIntensity(1);//810;	// whiteLight->setCastsShadow(false);
	//scene->addLight(whiteLightOrg);

	//auto camControllerInput = std::make_shared<XZHTool>(object11, object12, object, objectOT, whiteLight, *cam, client);  ////< XZH only camera controller std::make_shared<CameraController>(*cam, client);
	auto camControllerInput = std::make_shared<XZHTool>(object11, object12, object, objectOT, surroundingSurfaceMeshObj , whiteLight, *cam, client);  
	int mm = 1;
	// Set camera controller
	auto camController = cam->setController(camControllerInput);

	//// Sphere Test cutting surface middle
	//auto sphereDissectObj = apiutils::createCollidingAnalyticalSceneObject(
	//    imstk::Geometry::Type::Sphere, scene, "SphereDissect", 0.05, Vec3d(0, 0.095, 0.09)); // -2.23
	//sphereDissectObj->getVisualGeometry()->setTranslation(Vec3d(0, 0.095, 0.09));
	//Vec3d xzhDissectSphere = Vec3d(0, 0.095, 0.09) + 0.02 * Vec3d(0.22872038256899530, -0.33268419282762951, -0.91488153027598118);   ////< XZH normal of dissecting plane
	//auto sphereDissect2Obj = apiutils::createCollidingAnalyticalSceneObject(
	//    imstk::Geometry::Type::Sphere, scene, "SphereDissect2", 0.05, xzhDissectSphere); // -2.23
	//sphereDissect2Obj->getVisualGeometry()->setTranslation(xzhDissectSphere);

	////< XZH Axis |||||
	//auto sphereOrg = apiutils::createCollidingAnalyticalSceneObject(
	//    imstk::Geometry::Type::Sphere, scene, "SphereOrg", 0.05, Vec3d(0, 0, 0));
	//sphereOrg->getVisualGeometry()->setTranslation(Vec3d(0, 0, 0));
	auto materialOrg = std::make_shared<RenderMaterial>();
	auto diffTexOrg = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/yellow.png", Texture::Type::DIFFUSE);
	materialOrg->addTexture(diffTexOrg);
	auto sphereOrg = std::make_shared<imstk::Sphere>();
	sphereOrg->setTranslation(Vec3d(0.0, 0, 0));
	sphereOrg->setRadius(0.01);
	auto sphereOrgModel = std::make_shared<VisualModel>(sphereOrg);
	sphereOrgModel->setRenderMaterial(materialOrg);
	auto sphereOOO = std::make_shared<SceneObject>("SphereOrg");
	sphereOOO->addVisualModel(sphereOrgModel);
	scene->addSceneObject(sphereOOO);

	auto materialX = std::make_shared<RenderMaterial>();
	auto diffTexX = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/red.png", Texture::Type::DIFFUSE);
	materialX->addTexture(diffTexX);
	//materialX->setDisplayMode(RenderMaterial::DisplayMode::SURFACE);
	auto sphereX = std::make_shared<imstk::Sphere>();
	sphereX->setTranslation(Vec3d(0.25, 0, 0));
	
	sphereX->setRadius(0.01);
	auto sphereXModel = std::make_shared<VisualModel>(sphereX);
	sphereXModel->setRenderMaterial(materialX);
	auto sphereXXX = std::make_shared<SceneObject>("SphereX");
	sphereXXX->addVisualModel(sphereXModel);
	scene->addSceneObject(sphereXXX);

	auto materialY = std::make_shared<RenderMaterial>();
	auto diffTexY = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/green.png", Texture::Type::DIFFUSE);
	materialY->setEmissivity(1.0);
	materialY->addTexture(diffTexY);
	auto sphereY = std::make_shared<imstk::Sphere>();
	sphereY->setTranslation(Vec3d(0, 0.25, 0));
	sphereY->setRadius(0.01);
	auto sphereYModel = std::make_shared<VisualModel>(sphereY);
	sphereYModel->setRenderMaterial(materialY);
	auto sphereYYY = std::make_shared<SceneObject>("SphereY");
	sphereYYY->addVisualModel(sphereYModel);
	scene->addSceneObject(sphereYYY);

	auto materialZ = std::make_shared<RenderMaterial>();
	auto diffTexZ = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/blue.png", Texture::Type::DIFFUSE);
	materialZ->addTexture(diffTexZ);
	auto sphereZ = std::make_shared<imstk::Sphere>();
	sphereZ->setTranslation(Vec3d(0, 0, 0.25));
	sphereZ->setRadius(0.01);
	auto sphereZModel = std::make_shared<VisualModel>(sphereZ);
	sphereZModel->setRenderMaterial(materialZ);
	auto sphereZZZ = std::make_shared<SceneObject>("SphereZ");
	sphereZZZ->addVisualModel(sphereZModel);
	scene->addSceneObject(sphereZZZ);

	  ////< XZH AXIS X Y Z
	  // Line mesh
	auto lineMeshX = std::make_shared<imstk::LineMesh>();
	imstk::StdVectorOfVec3d vertListXYZ;
	vertListXYZ.resize(2);
	vertListXYZ[0] =  imstk::Vec3d(0.0, 0, 0);
	vertListXYZ[1] = imstk::Vec3d(0.25, 0, 0);
	LineMesh::LineArray lineXYZ;
	lineXYZ[0] = 0;
	lineXYZ[1] = 1;
	std::vector<LineMesh::LineArray> connectivityXYZ;
	connectivityXYZ.push_back(lineXYZ);
	lineMeshX->setInitialVertexPositions(vertListXYZ);
	lineMeshX->setVertexPositions(vertListXYZ);
	lineMeshX->setLinesVertices(connectivityXYZ);
	lineMeshX->initialize(vertListXYZ, connectivityXYZ);
	auto lineXModel = std::make_shared<VisualModel>(lineMeshX);
	auto materialLineX = std::make_shared<RenderMaterial>();
	materialLineX->setColor(imstk::Color::Red);
	materialLineX->setDisplayMode(RenderMaterial::DisplayMode::SURFACE);
	lineXModel->setRenderMaterial(materialLineX);
	auto lineXXX = std::make_shared<SceneObject>("LineX");
	lineXXX->addVisualModel(lineXModel);
	scene->addSceneObject(lineXXX);

	auto lineMeshY = std::make_shared<imstk::LineMesh>();
	vertListXYZ.clear();
	vertListXYZ.resize(2);
	vertListXYZ[0] = imstk::Vec3d(0.0, 0, 0);
	vertListXYZ[1] = imstk::Vec3d(0.0, 0.25, 0);
	lineMeshY->setInitialVertexPositions(vertListXYZ);
	lineMeshY->setVertexPositions(vertListXYZ);
	lineMeshY->setLinesVertices(connectivityXYZ);
	lineMeshY->initialize(vertListXYZ, connectivityXYZ);
	auto lineYModel = std::make_shared<VisualModel>(lineMeshY);
	auto materialLineY = std::make_shared<RenderMaterial>();
	materialLineY->setColor(imstk::Color::Green);
	materialLineY->setDisplayMode(RenderMaterial::DisplayMode::SURFACE);
	lineYModel->setRenderMaterial(materialLineY);
	auto lineYYY = std::make_shared<SceneObject>("LineY");
	lineYYY->addVisualModel(lineYModel);
	scene->addSceneObject(lineYYY);

	auto lineMeshZ = std::make_shared<imstk::LineMesh>();
	vertListXYZ.clear();
	vertListXYZ.resize(2);
	vertListXYZ[0] = imstk::Vec3d(0.0, 0, 0);
	vertListXYZ[1] = imstk::Vec3d(0.0, 0.0, 0.25);
	lineMeshZ->setInitialVertexPositions(vertListXYZ);
	lineMeshZ->setVertexPositions(vertListXYZ);
	lineMeshZ->setLinesVertices(connectivityXYZ);
	lineMeshZ->initialize(vertListXYZ, connectivityXYZ);
	auto lineZModel = std::make_shared<VisualModel>(lineMeshZ);
	auto materialLineZ = std::make_shared<RenderMaterial>();
	materialLineZ->setColor(imstk::Color::Blue);
	materialLineZ->setDisplayMode(RenderMaterial::DisplayMode::SURFACE);
	lineZModel->setRenderMaterial(materialLineZ);
	auto lineZZZ = std::make_shared<SceneObject>("LineZ");
	lineZZZ->addVisualModel(lineZModel);
	scene->addSceneObject(lineZZZ);

	// Sphere1 ROOT? of tool
	//auto sphere1Obj = apiutils::createCollidingAnalyticalSceneObject(
	//    imstk::Geometry::Type::Sphere, scene, "Sphere1", 0.003, Vec3d(-14.4, -1.4, 7.9)); // -2.23
	//sphere1Obj->getVisualGeometry()->setTranslation(Vec3d(-14.4, -1.4, 7.9));   ////< XZH KNIFE ROOT
	auto visualSphere11 = std::make_shared<imstk::Sphere>();
	visualSphere11->setTranslation(Vec3d(-14.4, -1.4, 7.9)*scalexzh);
	visualSphere11->setRadius(0.0005);
	auto visualSphere11Model = std::make_shared<VisualModel>(visualSphere11);
	visualSphere11Model->setRenderMaterial(materialY); // (materialSphere0);
	auto sphere1Obj = std::make_shared<CollidingObject>("Sphere1");
	sphere1Obj->setCollidingGeometry(visualSphere11);
	sphere1Obj->addVisualModel(visualSphere11Model);
	//scene->addSceneObject(sphere1Obj);  ////< XZH KNIFE TIP

										////< XZH Tip of tool
										//auto materialSphere0 = std::make_shared<RenderMaterial>();
										//auto diffuseTextureSphere0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/testtest.png", Texture::Type::DIFFUSE);
										//materialSphere0->addTexture(diffuseTextureSphere0);
	auto visualSphere22 = std::make_shared<imstk::Sphere>();
	visualSphere22->setTranslation(Vec3d(-7.8, -1.2, 7.9));
	visualSphere22->setRadius(0.0005);
	Vec3d mm2 = visualSphere22->getPosition();
	auto sphere2Obj = std::make_shared<CollidingObject>("Sphere2");
	auto visualSphere22Model = std::make_shared<VisualModel>(visualSphere22);
	visualSphere22Model->setRenderMaterial(materialX); // (materialSphere0);
	sphere2Obj->setCollidingGeometry(visualSphere22);
	sphere2Obj->addVisualModel(visualSphere22Model);
	//scene->addSceneObject(sphere2Obj);  ////< XZH KNIFE TIP

										////< XZH draw tool
	auto visualSphere11d = std::make_shared<imstk::Sphere>();
	visualSphere11d->setTranslation(Vec3d(-14.4, -1.4, 7.9)*scalexzh);
	visualSphere11d->setRadius(0.0005);
	auto sphere1dObj = std::make_shared<CollidingObject>("Sphere1d");
	auto visualSphere11dModel = std::make_shared<VisualModel>(visualSphere11d);
	visualSphere11dModel->setRenderMaterial(materialY); // (materialSphere0);
	sphere1dObj->setCollidingGeometry(visualSphere11d);
	sphere1dObj->addVisualModel(visualSphere11dModel);
	//scene->addSceneObject(sphere1dObj);  ////< XZH KNIFE TIP

	auto visualSphere22d = std::make_shared<imstk::Sphere>();
	visualSphere22d->setTranslation(Vec3d(-7.8, -1.2, 7.9));
	visualSphere22d->setRadius(0.0005); //  (0.0025);
	auto sphere2dObj = std::make_shared<CollidingObject>("Sphere2d");
	auto visualSphere22dModel = std::make_shared<VisualModel>(visualSphere22d);
	visualSphere22dModel->setRenderMaterial(materialX); // (materialSphere0);
	sphere2dObj->setCollidingGeometry(visualSphere22d);
	sphere2dObj->addVisualModel(visualSphere22dModel);
	//scene->addSceneObject(sphere2dObj);  ////< XZH KNIFE TIP

	////< XZH for tool draw position

	auto probe = std::make_shared<IBLProbe>(DATA_ROOT_PATH "/VESS/texture/organIrradiance.dds",
		DATA_ROOT_PATH "/VESS/texture/organRadiance.dds",
		iMSTK_DATA_ROOT "/IBL/roomBRDF.png");
	scene->setGlobalIBLProbe(probe);

	Vec3d move = Vec3d(0, -5, 15);  // Vec3d(0, 35, 15);
	////< XZH Step 1 END

    ////< XZH Step 2: colon-tumor
    std::string pathColon = DATA_ROOT_PATH"/VESS/Nick/tumorcolon7_scale2_den_v3.obj";   ////< XZH V3: dense:   tumorcolon7_scale2_den_v3   || V2: middle dense  tumorcolon7_scale2_den   cubeTet cubeTest tumorcolon7_scale2.obj
    auto objMesh = imstk::MeshIO::read(pathColon);
    auto visualColonMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh);
    visualColonMesh->computeBoundingBox(min1, max1);
    //visualColonMesh->scale(0.01, imstk::Geometry::TransformType::ApplyToData);
    visualColonMesh->rotate(imstk::UP_VECTOR, imstk::PI, imstk::Geometry::TransformType::ApplyToData);
    //visualColonMesh->translate(0, 0, 0.1, imstk::Geometry::TransformType::ApplyToData);
    visualColonMesh->computeBoundingBox(min1, max1);
    auto posVisualList = visualColonMesh->getVertexPositions();
    auto triLists = visualColonMesh->getTrianglesVertices();
    visualColonMesh->setVertexPositionsXZH(posVisualList);
    visualColonMesh->setTrianglesVerticesXZH(triLists);

    std::string pathCube = DATA_ROOT_PATH"/VESS/cubeTet.obj";   ////< XZH cubeTet cubeTest
    auto objMeshCube = imstk::MeshIO::read(pathCube);
    auto visualCubeMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMeshCube);
    visualCubeMesh->rotate(imstk::UP_VECTOR, imstk::PI, imstk::Geometry::TransformType::ApplyToData);
    auto posVisualListCube = visualCubeMesh->getVertexPositions();
    auto triListsCube = visualCubeMesh->getTrianglesVertices();
    visualCubeMesh->setVertexPositionsXZH(posVisualListCube);
    visualCubeMesh->setTrianglesVerticesXZH(triListsCube);

    auto material = std::make_shared<RenderMaterial>();
    auto diffuseTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/Nick/diffuse.png", Texture::Type::DIFFUSE);
    auto normalTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/Nick/normal.png", Texture::Type::NORMAL);
    auto roughnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/Nick/roughness.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    //auto sssTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_sss2.png", Texture::Type::SUBSURFACE_SCATTERING);
    auto sssTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_sss.png", Texture::Type::SUBSURFACE_SCATTERING);
    material->addTexture(diffuseTexture);
    material->addTexture(normalTexture);
    material->addTexture(roughnessTexture);
    material->addTexture(sssTexture);
    //material->setBackFaceCulling(false);
    material->setDisplayMode(RenderMaterial::DisplayMode::SURFACE);
    ////material->setTexture(metalnessTexture);
    //material->addTexture(sssTexture);
    //material->setTessellated(true);

    auto materialWire = std::make_shared<RenderMaterial>();
    auto diffuseTextureWire = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/Nick/diffuse2.png", Texture::Type::DIFFUSE);
    auto normalTextureWire = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/Nick/normal.png", Texture::Type::NORMAL);
    auto roughnessTextureWire = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/Nick/roughness.png", Texture::Type::ROUGHNESS);
    auto metalnessTextureWire = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    auto sssTextureWire = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_sss.png", Texture::Type::SUBSURFACE_SCATTERING);
    materialWire->addTexture(diffuseTextureWire);
    materialWire->addTexture(normalTextureWire);
    materialWire->addTexture(roughnessTextureWire);
    materialWire->addTexture(sssTextureWire);
    //materialWire->setTessellated(true);
    //material->setBackFaceCulling(false);
    materialWire->setDisplayMode(RenderMaterial::DisplayMode::SURFACE);

    auto visualColonMeshModel = std::make_shared<VisualModel>(visualColonMesh);
    visualColonMeshModel->setRenderMaterial(material);
    //visualCubeMesh->setRenderMaterial(material);

    auto objectColonV = std::make_shared<imstk::VisualObject>("visualColonMesh0");
    objectColonV->addVisualModel(visualColonMeshModel); // change to any mesh created above
    //scene->addSceneObject(objectColonV);
    //auto objectCube = std::make_shared<imstk::VisualObject>("visualCubeMesh");
    //objectCube->setVisualGeometry(visualCubeMesh); // change to any mesh created above
    //scene->addSceneObject(objectCube);

    ////< XZH 2 and 3
    std::string pathColon2 = DATA_ROOT_PATH"/VESS/Nick/colonPart0.obj";  //  colon_s01
    std::string pathColon3 = DATA_ROOT_PATH"/VESS/Nick/colonPart1.obj";  // colon_s03
    auto objMesh2 = imstk::MeshIO::read(pathColon2);
    auto objMesh3 = imstk::MeshIO::read(pathColon3);
    auto visualColonMesh2 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh2);
    auto visualColonMesh3 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh3);
    visualColonMesh2->rotate(imstk::UP_VECTOR, imstk::PI, imstk::Geometry::TransformType::ConcatenateToTransform);
    visualColonMesh3->rotate(imstk::UP_VECTOR, imstk::PI, imstk::Geometry::TransformType::ConcatenateToTransform);
    auto visualColonMesh2Model = std::make_shared<VisualModel>(visualColonMesh2);
    auto visualColonMesh3Model = std::make_shared<VisualModel>(visualColonMesh3);
    visualColonMesh2Model->setRenderMaterial(material);
    visualColonMesh3Model->setRenderMaterial(material);
    auto object2 = std::make_shared<imstk::VisualObject>("visualColonMesh2");
    object2->addVisualModel(visualColonMesh2Model); // change to any mesh created above
    //scene->addSceneObject(object2);
    auto object3 = std::make_shared<imstk::VisualObject>("visualColonMesh3");
    object3->addVisualModel(visualColonMesh3Model); // change to any mesh created above
    //scene->addSceneObject(object3);

    ////< XZH TET    // Tetrahedral mesh
    std::vector<Vec3d> centralLine2;
    centralLine2.push_back(Vec3d(2.4, 1.7, 7.1));   ////< XZH  (Vec3d(-2.5, 7, -23));
    centralLine2.push_back(Vec3d(3.4, 11.8, 7.1));   ////< XZH  (Vec3d(-3.5, 15, -23));
    auto materialTet = std::make_shared<RenderMaterial>();
    //auto diffuseTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    materialTet->setColor(imstk::Color::Blue);
    materialTet->setDisplayMode(RenderMaterial::DisplayMode::SURFACE);

    imstk::StdVectorOfVec3d vertListCutTet;
    std::vector<imstk::TetrahedralMesh::TetraArray> cutTetConnectivity;
    std::vector<physXVertTetLink> liksVolSurCut;
    auto cutMesh = std::make_shared<imstk::VESSTetCutMesh>();
    //cutMesh->setVisualMeshXZH(visualColonMesh);
    //cutMesh->loadTetXZH(DATA_ROOT_PATH"/VESS/Nick/tumorcolon72_scale.tet", DATA_ROOT_PATH"/VESS/Nick/tumorcolon72_scale.fc", DATA_ROOT_PATH"/VESS/Nick/tumorcolon72_scale.fca", DATA_ROOT_PATH"/VESS/Nick/tumorcolon72_scale.eg", DATA_ROOT_PATH"/VESS/Nick/tumorcolon72_scale.ega", DATA_ROOT_PATH"/VESS/Nick/tumorcolon72_scale.nd", vertListCutTet, cutTetConnectivity, liksVolSurCut);   ////< XZH cubeTet cubeTest
    //cutMesh->loadTetXZH(DATA_ROOT_PATH"/VESS/Nick/tumorcolon72_scale_den.tet", DATA_ROOT_PATH"/VESS/Nick/tumorcolon72_scale_den.fc", DATA_ROOT_PATH"/VESS/Nick/tumorcolon72_scale_den.fca", DATA_ROOT_PATH"/VESS/Nick/tumorcolon72_scale_den.eg", DATA_ROOT_PATH"/VESS/Nick/tumorcolon72_scale_den.ega", DATA_ROOT_PATH"/VESS/Nick/tumorcolon72_scale_den.nd", vertListCutTet, cutTetConnectivity, liksVolSurCut);   ////< XZH cubeTet cubeTest
	// cutMesh->loadTetXZH(DATA_ROOT_PATH"/VESS/Tet4/tetD2.dat", DATA_ROOT_PATH"/VESS/Tet4/fc.dat", DATA_ROOT_PATH"/VESS/Tet4/fcat.dat", DATA_ROOT_PATH"/VESS/Tet4/eg.dat", DATA_ROOT_PATH"/VESS/Tet4/ega.dat", DATA_ROOT_PATH"/VESS/Tet4/nd.dat", vertListCutTet, cutTetConnectivity, liksVolSurCut);   ////< XZH cubeTet cubeTest
	cutMesh->loadTetXZH(DATA_ROOT_PATH"/VESS/Tet7/tetD2.dat", DATA_ROOT_PATH"/VESS/Tet7/fc.dat", DATA_ROOT_PATH"/VESS/Tet7/fcat.dat", DATA_ROOT_PATH"/VESS/Tet7/eg.dat", DATA_ROOT_PATH"/VESS/Tet7/ega.dat", DATA_ROOT_PATH"/VESS/Tet7/nd.dat", vertListCutTet, cutTetConnectivity, liksVolSurCut);     ////< XZH denser mesh for dissecting
	//cutMesh->loadTetXZH(DATA_ROOT_PATH"/VESS/Tet8/tetD2.dat", DATA_ROOT_PATH"/VESS/Tet8/fc.dat", DATA_ROOT_PATH"/VESS/Tet8/fcat.dat", DATA_ROOT_PATH"/VESS/Tet8/eg.dat", DATA_ROOT_PATH"/VESS/Tet8/ega.dat", DATA_ROOT_PATH"/VESS/Tet8/nd.dat", vertListCutTet, cutTetConnectivity, liksVolSurCut);     ////< XZH denser mesh for dissecting
	cutMesh->computeBoundingBox(min1, max1);
    //cutMesh->scale(0.01, imstk::Geometry::TransformType::ApplyToData);
    cutMesh->rotate(imstk::UP_VECTOR, imstk::PI, imstk::Geometry::TransformType::ApplyToData);
    cutMesh->rotate(imstk::RIGHT_VECTOR, imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);
    cutMesh->updateVertexPositions();
    cutMesh->computeBoundingBox(min1, max1);
    ////< XZH test visual mesh extracted from the tet mesh
    //StdVectorOfVec3d posVisualListTest; 
    //std::vector<imstk::SurfaceMesh::TriangleArray> triListsTest;
    //auto visualColonMeshTest = std::make_shared<SurfaceMesh>();

	  ////< XZH try to display all the surface test 20190404

    //visualColonMeshTest->initialize(posVisualListTest, triListsTest);
    cutMesh->extractSurfaceMeshVisual();
    auto visualColonMeshTet = std::dynamic_pointer_cast<imstk::SurfaceMesh>(cutMesh->getVisualSurface());
    //visualColonMeshTet->rotate(imstk::UP_VECTOR, imstk::PI, imstk::Geometry::TransformType::ApplyToData);
    auto visualColonMeshTetModel = std::make_shared<VisualModel>(visualColonMeshTet);
    visualColonMeshTetModel->setRenderMaterial(materialWire);
    auto posVisualListTet = visualColonMeshTet->getVertexPositions();
    auto triListsTet = visualColonMeshTet->getTrianglesVertices();
    visualColonMeshTet->setVertexPositionsXZH(posVisualListTet);
    visualColonMeshTet->setTrianglesVerticesXZH(triListsTet);
    //////< XZH test the vertex color
    //for (int i = 0; i < posVisualListTet.size(); i++)
    //{
    //    visualColonMeshTet->setVertexColor(i, imstk::Color(0.2, 0.5, 0.5, 1.0));
    //}

    auto objectColon = std::make_shared<imstk::VisualObject>("visualColonMesh");
    objectColon->addVisualModel(visualColonMeshTetModel); // change to any mesh created above
    //scene->addSceneObject(objectColon);

    cutMesh->setVisualMeshXZH(visualColonMeshTet);
    cutMesh->setVisualMeshTexture(visualColonMesh);
    cutMesh->calcTex2TetSurfTriLink();   ////< XZH the above two link

    cutMesh->extractSurfaceMeshBoundary(centralLine2);
    cutMesh->extractSurfaceMeshVolume();
    cutMesh->mappingLinks();   ////< XZH visual texture -> its vertex mapping in tet
    cutMesh->mappingTriTetLinks();   ////< XZH visual texture  -> its tet index
    cutMesh->mappingTetTriLinks();   ////< XZH tet its 4 faces index in visual mesh, not texture mesh

    cutMesh->initializeCuttingMesh();
    cutMesh->initializeCuttingPara();

      ////< XZH for injection
    auto tetPosListXZH = cutMesh->getVertexPositions();
    cutMesh->setInitialPhysVertex(tetPosListXZH);
	////< XZH update the injection flag for each vertex
	cutMesh->updatePhysVertex();

      ////< XZH for collision temp,might use the above way.
    std::vector<Vec3d> centralLine;
    //centralLine.push_back(Vec3d(2.4, 1.7, 7.1)*0.04);
    //centralLine.push_back(Vec3d(2.4, 11.8, 7.1)*0.04);
    centralLine.push_back(Vec3d(0.02706308, 0.154157, 0.2207738));
    centralLine.push_back(Vec3d(0.02706308, 0.08378759, 0.2207738));
    cutMesh->extractSurfaceMesh2(centralLine); // not using central line

    auto spherecentralLine1 = std::make_shared<imstk::Sphere>();
    spherecentralLine1->setTranslation(centralLine[0]);
    spherecentralLine1->setRadius(0.006);
    auto spherecentralLine1Model = std::make_shared<VisualModel>(spherecentralLine1);
    spherecentralLine1Model->setRenderMaterial(materialY);
    auto spherecentralLine11Obj = std::make_shared<SceneObject>("spherecentralLine1");
    spherecentralLine11Obj->addVisualModel(spherecentralLine1Model);
    //scene->addSceneObject(spherecentralLine11Obj);

    auto spherecentralLine2 = std::make_shared<imstk::Sphere>();
    spherecentralLine2->setTranslation(centralLine[1]);
    spherecentralLine2->setRadius(0.006);
    auto spherecentralLine2Model  = std::make_shared<VisualModel>(spherecentralLine2);
    spherecentralLine2Model->setRenderMaterial(materialY);
    auto spherecentralLine22Obj = std::make_shared<SceneObject>("spherecentralLine2");
    spherecentralLine22Obj->addVisualModel(spherecentralLine2Model);
    //scene->addSceneObject(spherecentralLine22Obj);

    ////< XZH 
    auto& statesCut = cutMesh->getTetrahedronStates();
    statesCut.clear();
    auto& mapsCut = cutMesh->getTetrahedronMap();
    mapsCut.clear();
    for (int i = 0; i < cutTetConnectivity.size(); i++)
    {
        statesCut.push_back(1); // exist
        mapsCut.push_back(1); // original
    }

    auto materialTetB = std::make_shared<RenderMaterial>();
    //auto diffuseTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    materialTetB->setColor(imstk::Color::Red);
    materialTetB->setDisplayMode(RenderMaterial::DisplayMode::WIREFRAME);

    ////< XZH test triangle
    auto materialLine = std::make_shared<RenderMaterial>();
    materialLine->setColor(imstk::Color::Red);
    materialLine->setDisplayMode(RenderMaterial::DisplayMode::SURFACE);
    std::shared_ptr<SurfaceMesh> surfMeshCentralLine = std::make_shared<SurfaceMesh>();
    StdVectorOfVec3d vertListLine;

	vertListLine.push_back(Vec3d(0.02706308, 0.154157, 0.2207738)); // 0  centralLine.push_back(Vec3d(0.02706308, 0.154157, 0.2207738));	centralLine.push_back(Vec3d(0.02706308, 0.08378759, 0.2207738));
    vertListLine.push_back(Vec3d(0.02706308, 0.08378759, 0.2207738));  // 1
    vertListLine.push_back(Vec3d(0.029, 0.154157, 0.2207738)); // 2
	
    SurfaceMesh::TriangleArray triLine[2];
    triLine[0] = { { 0, 1, 2 } };
    triLine[1] = { { 0, 2, 1 } };
    std::vector<SurfaceMesh::TriangleArray> trianglesLine;
    trianglesLine.push_back(triLine[0]);
    trianglesLine.push_back(triLine[1]);
    surfMeshCentralLine->setTrianglesVertices(trianglesLine);
    surfMeshCentralLine->setInitialVertexPositions(vertListLine);
    surfMeshCentralLine->setVertexPositions(vertListLine);
    surfMeshCentralLine->initialize(vertListLine, trianglesLine);
    auto surfMeshCentralLineModel = std::make_shared<VisualModel>(surfMeshCentralLine);
    surfMeshCentralLineModel->setRenderMaterial(materialLine);
    auto objectLine = std::make_shared<imstk::VisualObject>("surfMeshCentralLine");
    objectLine->addVisualModel(surfMeshCentralLineModel);
    scene->addSceneObject(objectLine);

    ////< XZH 
    // Construct a map
    auto colonMapP2V = std::make_shared<imstk::OneToOneMapVESS>();
    colonMapP2V->setMaster(cutMesh);
    colonMapP2V->setSlave(visualColonMeshTet);
    colonMapP2V->computeNewXZH();

    auto colonMapP2C = std::make_shared<imstk::OneToOneMapVESS>();
    colonMapP2C->setMaster(cutMesh);
    colonMapP2C->setSlave(cutMesh);
    colonMapP2C->compute();

    auto colonObj = std::make_shared<PbdObject>("Colon");
    colonObj->setCollidingGeometry(cutMesh);
    colonObj->addVisualModel(visualColonMeshTetModel);
    colonObj->setPhysicsGeometry(cutMesh);
    colonObj->setPhysicsToCollidingMap(colonMapP2C);
    colonObj->setPhysicsToVisualMap(colonMapP2V);
    //colonObj->setCollidingToVisualMap(colonMapC2V);

	////< XZH adding spheres(tetrahedrons) for test
	auto& physVertices = cutMesh->getPhysVertex();
	auto materialSphereFix = std::make_shared<RenderMaterial>();
	materialSphereFix->setColor(imstk::Color::Green);
	auto materialSphereInject = std::make_shared<RenderMaterial>();
	materialSphereInject->setColor(imstk::Color::Red);
	auto tetSphereObj = std::make_shared<SceneObject>("TetSphereObj");
	for (int i = 0; i < physVertices.size(); i++)
	{
		auto& physVert = physVertices[i];
		auto pos = physVert.pos;
		auto tetSphere = std::make_shared<imstk::Sphere>();
		tetSphere->setTranslation(pos);
		tetSphere->setRadius(0.0003);
		auto tetSphereModel = std::make_shared<VisualModel>(tetSphere);
		if (!physVert.bInject)		tetSphereModel->setRenderMaterial(materialSphereFix);
		else tetSphereModel->setRenderMaterial(materialSphereInject);
		tetSphereObj->addVisualModel(tetSphereModel);
	}
	//scene->addSceneObject(tetSphereObj);
	////< XZH spheres test END

    //fixed point
    Vec3d posTumorCenter = Vec3d(0.027785, 0.087666, 0.209199); // Vec3d(0.032, 0.090416, 0.193864); // Vec3d(0.029985, 0.092161, 0.197533); // Vec3d(3.5, 8.9, 19.0)*scalexzh;
    auto& posTetlist2 = cutMesh->getVertexPositions();
    std::string fixed_corner2;
    double distTumor = 0.02; //  7.0*scalexzh;
    char intStr2[33];
    Vec3d tmpPos2;
    std::vector<int> fixedVertice2; // add later 06/04/2017
    std::vector<int> fixedVerticeStates2; // fixed states

	  ////< XZH Method 3 to set fixed vertices
	auto tetListAll = cutMesh->getTetrahedraVertices();
	auto tetDissectFlagAll = cutMesh->getDissectFlag();
	for (int i = 0; i < cutMesh->getNumVertices(); ++i) // physicsColonMesh  visualColonMesh
	{
		bool flag = false;
		//for (int k=0;k<tetListAll.size();k++)
		//{
		//	auto ttet = tetListAll[k];
		//	int nn0 = ttet[0];
		//	int nn1= ttet[1];
		//	int nn2= ttet[2];
		//	int nn3 = ttet[3];
		//	int fflag = tetDissectFlagAll[k];
		//	if (fflag<1)   ////< XZH dissect, should not be deformable
		//	{
		//		if ( (i==nn0) || (i == nn1) || (i == nn2)  || (i == nn3) )
		//		{
		//			flag = flag||true;
		//		}
		//	}
		//}
		//if (!physVertices[i].bInject) flag = true;
		if (!physVertices[i].bDissect) flag = true;

		if (flag)
		{
			std::sprintf(intStr2, "%d", i + 1);
			fixed_corner2 += std::string(intStr2) + ' ';
			fixedVertice2.push_back(i); //  all vertices
			fixedVerticeStates2.push_back(1);
		}
		else
		{
			fixedVerticeStates2.push_back(0);
		}
	}
	  ////< XZH END Method 3 to set fixed vertices

    // set fixed in the tetmesh add later 06/04/2017
    cutMesh->setFixedVertice(fixedVertice2);
    cutMesh->updateFixedVertice();
    cutMesh->initDeformMesh(); // init for deform mesh : injection solution 06/20/2017

      ////< XZH test surface of colliding
    auto& physVertices0 = cutMesh->getPhysVertex();
    auto& physEdge0 = cutMesh->getPhysEdge();
	auto& physSTris0 = cutMesh->getInjectSurfTris(); // cutMesh->getPhysSurfTris();  //   // cutMesh->getInjectSurfTris(); // test inject 06/02/2019  
    auto surfaceMeshColliding = std::make_shared<imstk::SurfaceMesh>();
    StdVectorOfVec3d vertPosesColliding;
    std::vector<imstk::SurfaceMesh::TriangleArray> surfaceTriColliding;
    for (int i = 0; i < physVertices0.size();i++)
    {
        vertPosesColliding.push_back(physVertices0[i].pos  );
    }
    for (int i = 0; i < physSTris0.size(); i++)
    {
		size_t ni0 = physSTris0[i].nodeIdx[0];
		size_t ni1 = physSTris0[i].nodeIdx[1];
		size_t ni2 = physSTris0[i].nodeIdx[2];
        surfaceTriColliding.push_back(imstk::SurfaceMesh::TriangleArray{ { ni0, ni1, ni2 } });
    }
    surfaceMeshColliding->initialize(vertPosesColliding, surfaceTriColliding);
    auto surfaceMeshCollidingModel = std::make_shared<VisualModel>(surfaceMeshColliding);

	auto materialInjecting = std::make_shared<RenderMaterial>();
	materialInjecting->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME SURFACE
	auto tmpXZH = std::make_shared<Texture>("I:/iMSTK/resources/VESS/texture/dissecting2.png", Texture::Type::DIFFUSE);
	materialInjecting->addTexture(tmpXZH); 
	//materialInjecting->setColor(imstk::Color::Blue);
	materialInjecting->setBackFaceCulling(true);
    materialInjecting->setDisplayMode(RenderMaterial::DisplayMode::SURFACE);
	surfaceMeshCollidingModel->setRenderMaterial(materialInjecting);
    auto surfaceCollidingObj = std::make_shared<SceneObject>("surfaceCollidingObj");
    surfaceCollidingObj->addVisualModel(surfaceMeshCollidingModel);
    //scene->addSceneObject(surfaceCollidingObj);

    auto pbdColonModel = std::make_shared<PbdModel>();
    pbdColonModel->setModelGeometry(cutMesh);
    pbdColonModel->configure(/*Number of Constraints*/2,
        /*Constraint configuration*/"Volume 0.90",  // Volume 0.80  0.7 "Dihedral 0.8", // 0.01 0.8 effect is couhe not very well
        /*Constraint configuration*/"Distance 0.95",  // Distance 0.95 0.9 effect is couhe
        /*Mass*/0.1, // 0.1
        /*Gravity*/"0 0 0.3", //  0.098 98",  // "0 -9.8 0",
        /*TimeStep*/0.004, // 0.004, // 0.001
        /*FixedPoint*/ fixed_corner2.c_str(), // "1 2", //   ////< XZH  fixed_corner2.c_str(),
        /*NumberOfIterationInConstraintSolver*/2, //   2 12
        /*Proximity*/0.002, // 0.01,  // 0.1
        /*Contact stiffness*/0.1);
    colonObj->setDynamicalModel(pbdColonModel);

    auto pbdColonSolver = std::make_shared<PbdSolver>();
    pbdColonSolver->setPbdObject(colonObj);
    scene->addNonlinearSolver(pbdColonSolver);

    scene->addSceneObject(colonObj);

    // Collisions
    auto colGraph = scene->getCollisionGraph();
    //auto pair = std::make_shared<PbdInteractionPair>(PbdInteractionPair(object, colonObj));
    auto pair = std::make_shared<PbdInteractionPair>(PbdInteractionPair(object11, object12, object, colonObj));
    pair->setNumberOfInterations(3); // 2
    double& injIncrement = pair->getInjectIncrement();
	injIncrement = 0.0005; // 0.002; // 0.001
    double& maxInjIncrement = pair->getMaxInjectIncrement();
	maxInjIncrement = 0.005; // 0.01; //  0.020; // 0.005
    int& levs = pair->getAdjLevels();
	levs = 5; // 10; // 3; // 5
    bool& dirs = pair->getInflationDir();
    dirs = true;

      ////< XZH adding cylinder
    pair->setSimpleObject(cylinderObj, lineCylinder);

    colGraph->addInteractionPair(pair);

    //graph sphere Tet
    //auto sphereTumor = apiutils::createCollidingAnalyticalSceneObject(
    //    imstk::Geometry::Type::Sphere, scene, "SphereTumor3", distTumor*2, posTumorCenter);
    //sphereTumor->getVisualGeometry()->setTranslation(posTumorCenter);

    auto materialTumor = std::make_shared<RenderMaterial>();
    auto diffTexTumor = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/blue.png", Texture::Type::DIFFUSE);
    materialTumor->addTexture(diffTexTumor);
    materialTumor->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE);
    auto sphereTumor = std::make_shared<imstk::Sphere>();
    sphereTumor->setTranslation(posTumorCenter);
    sphereTumor->setRadius(distTumor);
    auto sphereTumorModel = std::make_shared<VisualModel>(sphereTumor);
    sphereTumorModel->setRenderMaterial(materialTumor);
    auto sphereTumorObj = std::make_shared<SceneObject>("SphereTumor");
    sphereTumorObj->addVisualModel(sphereTumorModel);
    //scene->addSceneObject(sphereTumorObj);

      ////< XZH injected point
    auto materialInject = std::make_shared<RenderMaterial>();
    auto diffTexInject = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/red.png", Texture::Type::DIFFUSE);
    materialInject->addTexture(diffTexInject);
    materialInject->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE);
    auto sphereInject = std::make_shared<imstk::Sphere>();
    sphereInject->setTranslation(posTumorCenter);
    sphereInject->setRadius(0.0005);
    auto sphereInjectModel = std::make_shared<VisualModel>(sphereInject);
    sphereInjectModel->setRenderMaterial(materialInject);
    auto sphereInjectObj = std::make_shared<SceneObject>("SphereInject");
    sphereInjectObj->addVisualModel(sphereInjectModel);
    scene->addSceneObject(sphereInjectObj);

    auto sphereInject1 = std::make_shared<imstk::Sphere>();
    sphereInject1->setTranslation(posTumorCenter);
    sphereInject1->setRadius(0.001);
    auto sphereInject1Model = std::make_shared<VisualModel>(sphereInject1);
    sphereInject1Model->setRenderMaterial(materialY);
    auto sphereInjectObj1 = std::make_shared<SceneObject>("SphereInject1");
    sphereInjectObj1->addVisualModel(sphereInject1Model);
    scene->addSceneObject(sphereInjectObj1);

    auto sphereInject2 = std::make_shared<imstk::Sphere>();
    sphereInject2->setTranslation(posTumorCenter);
    sphereInject2->setRadius(0.001);
    auto sphereInject2Model = std::make_shared<VisualModel>(sphereInject2);
    sphereInject2Model->setRenderMaterial(materialY);
    auto sphereInjectObj2 = std::make_shared<SceneObject>("SphereInject2");
    sphereInjectObj2->addVisualModel(sphereInject2Model);
    scene->addSceneObject(sphereInjectObj2);
    //< XZH Step 2 END

	//  ////*< XZH keyboard
	//sdk->getViewer()->setOnCharFunction('c', [&](InteractorStyle* c) -> bool
	//{
	//	bool hasMarking = false;
	//	std::vector<std::shared_ptr<DecalPool>> decalPoolList;
	//	std::shared_ptr<DecalPool> decalPool;
	//	auto decalMaterial = std::make_shared<RenderMaterial>();
	//	
	//	for (auto obj : scene->getSceneObjects())
	//	{
	//		if (auto decalPool = std::dynamic_pointer_cast<DecalPool>(obj->getVisualGeometry()))
	//		{
	//			auto& decallist = decalPool->getDecals() ;
	//			int idx = decallist.at(0)->getVisualIndex();
	//			if (idx >= 0)
	//			{
	//				decallist.at(0)->setPosition(visualColonMeshTet->getVertexPosition(idx));
	//			}
	//			decalPoolList.push_back(decalPool);
	//		}
	//	}
	//	if (decalPoolList.size()>0)   ////< XZH loading
	//	{
	//		  ////TODO:< XZH saving to the file...
	//		FILE *outPos = fopen("I:/marking2.dat", "w");
	//		fprintf(outPos, "%d\n", decalPoolList.size());
	//		for (int n=0;n<decalPoolList.size();n++)
	//		{
	//			auto dpool = decalPoolList[n];
	//			auto& decallist = dpool->getDecals();
	//			int idx = decallist.at(0)->getVisualIndex();
	//			fprintf(outPos, "%d\n", idx);
	//		}
	//		fclose(outPos);
	//		return false;
	//	}
	//	else
	//	{
	//		  ////TODO:< XZH may be write a function later loading...
	//		decalPool = std::make_shared<DecalPool>();
	//		vector<string> strBufferVec;
	//		std::fstream infile("I:/marking.dat");
	//		if (!infile)
	//		{
	//			cout << "failed" << endl;
	//			//exit(1);
	//		}
	//		cout << "start..." << endl;
	//		string strLine;
	//		int numLines = 0;
	//		int numTotal = 0;
	//		int i0 = 0, i1 = 0, i2 = 0, i3 = 0;
	//		while (getline(infile, strLine))
	//		{
	//			if (numLines > numTotal) break;

	//			strBufferVec.push_back(strLine);

	//			vector<string> strBufferVecTmp;
	//			strBufferVecTmp.push_back(strLine);
	//			////< XZH split
	//			vector<int> intVec;
	//			int width;
	//			for (vector<string>::iterator iter = strBufferVecTmp.begin(); iter != strBufferVecTmp.end(); iter++)
	//			{
	//				vector<string> strBufferLineVec;
	//				char separator = '\x20';
	//				cutStrLineBySeparator(*iter, separator, strBufferLineVec);
	//				width = strBufferLineVec.size();
	//				int numCols = 0;
	//				for (vector<string>::iterator iterLine = strBufferLineVec.begin(); iterLine != strBufferLineVec.end(); iterLine++)
	//				{
	//					stringstream ss;
	//					ss << *iterLine;
	//					int num;
	//					ss >> num;
	//					ss.clear();
	//					intVec.push_back(num);
	//					//cout << num<<" "; 
	//					if ((numLines == 0) && (numCols == 0))   ////< XZH total num
	//					{
	//						numTotal = num;
	//					}
	//					if ((numLines > 0) && (numLines <= 100))   ////< XZH each index
	//					{
	//						i0 = num;

	//						////< XZH marking
	//						auto decalTestModel = std::make_shared<VisualModel>(decalPool);
	//						auto diffuseTexture = std::make_shared<Texture>("I:/iMSTK/resources/textures/test5.png", Texture::Type::DIFFUSE);
	//						decalMaterial->addTexture(diffuseTexture);
	//						char name[256];
	//						auto decal1 = decalPool->addDecal();
	//						decal1->setPosition(visualColonMeshTet->getVertexPosition(i0));
	//						decal1->scale(0.01*0.3); // 0.4
	//						decalTestModel->setRenderMaterial(decalMaterial);
	//						decal1->setVisualIndex(i0);
	//						sprintf(name, "DecalObject%d", numLines);
	//						auto decalObject = std::make_shared<VisualObject>(name); // "DecalObject"
	//						decalObject->addVisualModel(decalTestModel);
	//						scene->addSceneObject(decalObject);
	//					}
	//				}
	//			}
	//			numLines++;
	//		}
	//		cout << numLines << "->end..." << endl;
	//		infile.close();	
	//		return false;
	//	}
	//});
	//  ////*< XZH keyboard END

	////< XZH Step3: Run
		sdk->setActiveScene(scene);
    //  ////< XZH key switch
    auto viewer = std::dynamic_pointer_cast<VulkanViewer>(sdk->getViewer());
	//material01->setEmissivity(-1);
	//materialWire->setEmissivity(-1);
	//material->setEmissivity(-1);
	//material01->setTransparency(true);
	//material01->setColor(Color(1,1,1,0.5));
	viewer->setResolution(1520, 920);
	viewer->setResolutionScale(2);
	  ////*< XZH 
	viewer->enableLensDistortion(0.001);
    //viewer->setResolution(1920, 1080);   ////< XZH 1920*1080
    //viewer->enableFullscreen();
    sdk->startSimulation(SimulationStatus::INACTIVE);
#endif
}

  ////< XZH cut with colon-tumor new model
void testTetCutColon()
{
#ifdef iMSTK_USE_OPENHAPTICS
    // SDK and Scene
    auto sdk = std::make_shared<imstk::SimulationManager>();
    auto scene = sdk->createNewScene("SceneTestDevice");

    Vec3d minBox, maxBox;
    // Device Client
    auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

    // Device Server
    auto server = std::make_shared<imstk::HDAPIDeviceServer>();
    server->addDeviceClient(client);
    sdk->addModule(server);

    std::string path2obj = DATA_ROOT_PATH"/VESS/Nick/cutKnife.obj"; // ITKnife7_6 is too big ITKnife7_4.obj knife correct ITKnife6 // cube knife knife3.obj";  // //  113.obj"; //  /  blade2.obj  cube.obj  kcube.obj  test cloth using this 
    std::string path2obj_c = DATA_ROOT_PATH"/VESS/Nick/cutKnife.obj"; //  ITKnife7_4.obj  knife correct ITKnife6  // cube knife knife3.obj";  //  //  113.obj";  // / VESS / TriangleTipKnife2.obj";  // blade2.obj  cube.obj kcube_s2.obj
    auto readMesh = imstk::MeshIO::read(path2obj_c); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
    Vec3d vertexPos3;
    Vec3d min1, max1;
    readMesh->computeBoundingBox(min1, max1);

    auto visualMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(readMesh); // std::make_shared<imstk::SurfaceMesh>(); //  
    visualMesh->rotate(imstk::UP_VECTOR, imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);
    visualMesh->computeBoundingBox(min1, max1);
    auto& initPosesTool = visualMesh->getVertexPositionsChangeable();
    Vec3d moveTmp = Vec3d(-9.093, 1.6348, -8.859199524)*0.01;   ////< XZH avg, avg, min
    for (int i = 0; i < initPosesTool.size(); i++)
    {
        Vec3d pos = initPosesTool[i];
        visualMesh->setVertexPosition(i, pos + moveTmp);
    }
    //auto& initPosesToolXZH = visualMesh->getVertexPositionsChangeable();
    visualMesh->setInitialVertexPositions(initPosesTool);
    visualMesh->setInitialVertexPositionsChangeable(initPosesTool);

    visualMesh->computeBoundingBox(min1, max1);
    Vec3d x0, x1;
    x1.z() = min1.z();
    x1.y() = (min1.y() + max1.y())*0.5;
    x1.x() = (min1.x() + max1.x())*0.5;
    x0.z() = max1.z();
    x0.y() = (min1.y() + max1.y())*0.5;
    x0.x() = (min1.x() + max1.x())*0.5;

    ////< XZH tool1
    std::string path2obj_c1 = DATA_ROOT_PATH"/VESS/Nick/injKnife.obj"; // DATA_ROOT_PATH"/VESS/HybridKnife.obj"; // DATA_ROOT_PATH"/VESS/ITKnife7_4H.obj"; //  ITKnife7_4.obj  knife correct ITKnife6  // cube knife knife3.obj";  //  //  113.obj";  // / VESS / TriangleTipKnife2.obj";  // blade2.obj  cube.obj kcube_s2.obj
    auto readMesh1 = imstk::MeshIO::read(path2obj_c1); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
    auto visualMesh1 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(readMesh1); // std::make_shared<imstk::SurfaceMesh>(); //  
    visualMesh1->rotate(imstk::UP_VECTOR, imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);
    //visualMesh->rotate(imstk::RIGHT_VECTOR, -imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);
    auto& initPosesTool1 = visualMesh1->getVertexPositionsChangeable();
    for (int i = 0; i < initPosesTool1.size(); i++)
    {
        Vec3d pos = initPosesTool1[i];
        visualMesh1->setVertexPosition(i, pos + moveTmp);
    }
    //auto& initPosesTool1XZH = visualMesh1->getVertexPositionsChangeable();
    visualMesh1->setInitialVertexPositions(initPosesTool1);
    visualMesh1->setInitialVertexPositionsChangeable(initPosesTool1);

    ////< XZH tool2
    std::string path2obj_c2 = DATA_ROOT_PATH"/VESS/Nick/injKnife.obj"; // DATA_ROOT_PATH"/VESS/HybridKnife.obj"; // DATA_ROOT_PATH"/VESS/ITKnife7_4H.obj"; //  ITKnife7_4.obj  knife correct ITKnife6  // cube knife knife3.obj";  //  //  113.obj";  // / VESS / TriangleTipKnife2.obj";  // blade2.obj  cube.obj kcube_s2.obj
    auto readMesh2 = imstk::MeshIO::read(path2obj_c2); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
    auto visualMesh2 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(readMesh2); // std::make_shared<imstk::SurfaceMesh>(); //  
    visualMesh2->rotate(imstk::UP_VECTOR, imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);
    //visualMesh->rotate(imstk::RIGHT_VECTOR, -imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);
    auto& initPosesTool2 = visualMesh2->getVertexPositionsChangeable();
    for (int i = 0; i < initPosesTool2.size(); i++)
    {
        Vec3d pos = initPosesTool2[i];
        visualMesh2->setVertexPosition(i, pos + moveTmp);
    }
    visualMesh2->setInitialVertexPositions(initPosesTool2);
    visualMesh2->setInitialVertexPositionsChangeable(initPosesTool2);

    visualMesh2->computeBoundingBox(min1, max1);
    // Line mesh
    auto lineMesh = std::make_shared<imstk::LineMesh>();
    imstk::StdVectorOfVec3d vertList;
    vertList.resize(2);  // using 5 segment
    vertList[0] = x0; // imstk::Vec3d(-14.4, -1.4, 7.9);
    vertList[1] = x1; // imstk::Vec3d(-7.8, -1.2, 7.9);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    double lineLen = (x0 - x1).norm();
    vertList[0] = Vec3d(0, 0, 0) + lineLen*Vec3d(0, 0, 1);
    vertList[1] = Vec3d(0, 0, 0);
    Vec3d moveLine = vertList[1] - x1;
    //std::vector<std::vector<int> > connectivity;
    std::vector<LineMesh::LineArray> connectivity;
    for (int i = 0; i < 1;){
        //std::vector<int> line;
        LineMesh::LineArray line;
        line[0] = i;
        //line.push_back(i);
        i++;
        line[1] = i;
        //line.push_back(i);

        connectivity.push_back(line);
    }

    lineMesh->setInitialVertexPositions(vertList);
    lineMesh->setVertexPositions(vertList);
    lineMesh->setLinesVertices(connectivity); //  setConnectivity(connectivity);
    //lineMesh->rotate(imstk::UP_VECTOR, imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);

    auto physicsMesh = lineMesh; //visualMesh; //  lineMesh; //  imstk::MeshIO::read(path2obj_c);  //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
    auto collidingMesh = lineMesh; // visualMesh; //  lineMesh;
    //visualMesh->setRotation(Vec3d(0, 0, 1),PI/4); // move

    auto material0 = std::make_shared<RenderMaterial>();
    auto diffuseTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto normalTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Normal.png", Texture::Type::NORMAL);
    auto roughnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Roughness.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material0->addTexture(diffuseTexture0);
    material0->addTexture(normalTexture0);
    material0->addTexture(roughnessTexture0);
    //material->setTexture(metalnessTexture);

    auto visualMeshModel = std::make_shared<VisualModel>(visualMesh);
    auto visualMesh1Model = std::make_shared<VisualModel>(visualMesh1);
    auto visualMesh2Model = std::make_shared<VisualModel>(visualMesh2);

    visualMeshModel->setRenderMaterial(material0);
    visualMesh1Model->setRenderMaterial(material0);
    visualMesh2Model->setRenderMaterial(material0);

    FILE *outpointtri;
    outpointtri = fopen("i:/AABB.txt", "w");
    fprintf(outpointtri, "V: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    collidingMesh->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "C: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    fclose(outpointtri);

    auto object = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject");

    auto objMapP2V = std::make_shared<imstk::OneToOneMapVESS>();  // std::make_shared<imstk::TriangleLineMap>(); //  std::make_shared<imstk::OneToOneMapVESS>();
    objMapP2V->setMaster(physicsMesh);
    objMapP2V->setSlave(visualMesh);
    objMapP2V->compute();


    auto objMapP2C = std::make_shared<imstk::OneToOneMapVESS>();  //  colliding is physical mesh
    objMapP2C->setMaster(physicsMesh);
    objMapP2C->setSlave(collidingMesh);
    objMapP2C->compute();

    auto objMapC2V = std::make_shared<imstk::OneToOneMapVESS>(); //  std::make_shared<imstk::TriangleLineMap>(); //  s std::make_shared<imstk::OneToOneMapVESS>();
    objMapC2V->setMaster(collidingMesh);
    objMapC2V->setSlave(visualMesh);
    objMapC2V->compute();

    auto objMapC2P = std::make_shared<imstk::OneToOneMapVESS>(); // colliding is physical mesh
    objMapC2P->setMaster(collidingMesh);
    objMapC2P->setSlave(physicsMesh);
    objMapC2P->compute();

    object->addVisualModel(visualMeshModel);
    object->setCollidingGeometry(collidingMesh);
    object->setPhysicsGeometry(physicsMesh);
    object->setPhysicsToCollidingMap(objMapP2C);
    object->setPhysicsToVisualMap(objMapP2V);
    object->setCollidingToVisualMap(objMapC2V);
    object->setColldingToPhysicsMap(objMapC2P);

    Vec3d pos0 = collidingMesh->getVertexPosition(0);
    auto& posList = visualMesh->getVertexPositionsChangeable();
    std::vector<Vec3d>& dirGapVisualDissecting0 = object->getGapVisualDissecting();
    for (int i = 0; i < posList.size(); i++)
    {
        dirGapVisualDissecting0.push_back(posList[i] - pos0);
    }

    ////< XZH tool0
    object->setVisualMesh(visualMesh, 0);
    object->setMaps(objMapP2V, objMapP2C, objMapC2V, objMapC2P, 0);

    auto pbdModel = std::make_shared<PbdModel>();
    pbdModel->setModelGeometry(physicsMesh);
    pbdModel->configure(/*Number of Constraints*/1,
        /*Constraint configuration*/"Distance 0.1",
        /*Mass*/0.0,
        /*Gravity*/"0 0 0",
        /*TimeStep*/0.0003,
        /*FixedPoint*/"",
        /*NumberOfIterationInConstraintSolver*/2, // 5
        /*Proximity*/0.3,  // use linemesh 0.3  // 0.01 use  //  cube 0.1
        /*Contact stiffness*/0.001);
    object->setDynamicalModel(pbdModel);

    //auto pbdSolver = std::make_shared<PbdSolver>();
    //pbdSolver->setPbdObject(object);
    //scene->addNonlinearSolver(pbdSolver);

    scene->addSceneObject(object);

    ////< XZH tool1
    auto object11 = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject1");

    auto objMapP2V1 = std::make_shared<imstk::OneToOneMapVESS>();  // std::make_shared<imstk::TriangleLineMap>(); //  std::make_shared<imstk::OneToOneMapVESS>();
    objMapP2V1->setMaster(physicsMesh);
    objMapP2V1->setSlave(visualMesh1);
    objMapP2V1->compute();


    auto objMapP2C1 = std::make_shared<imstk::OneToOneMapVESS>();  //  colliding is physical mesh
    objMapP2C1->setMaster(physicsMesh);
    objMapP2C1->setSlave(collidingMesh);
    objMapP2C1->compute();

    auto objMapC2V1 = std::make_shared<imstk::OneToOneMapVESS>(); //  std::make_shared<imstk::TriangleLineMap>(); //  s std::make_shared<imstk::OneToOneMapVESS>();
    objMapC2V1->setMaster(collidingMesh);
    objMapC2V1->setSlave(visualMesh1);
    objMapC2V1->compute();

    auto objMapC2P1 = std::make_shared<imstk::OneToOneMapVESS>(); // colliding is physical mesh
    objMapC2P1->setMaster(collidingMesh);
    objMapC2P1->setSlave(physicsMesh);
    objMapC2P1->compute();

    object11->addVisualModel(visualMesh1Model);
    object11->setCollidingGeometry(collidingMesh);
    object11->setPhysicsGeometry(physicsMesh);
    object11->setPhysicsToCollidingMap(objMapP2C1);
    object11->setPhysicsToVisualMap(objMapP2V1);
    object11->setCollidingToVisualMap(objMapC2V1);
    object11->setColldingToPhysicsMap(objMapC2P1);

    ////< XZH tool0
    object11->setVisualMesh(visualMesh1, 1);
    object11->setMaps(objMapP2V1, objMapP2C1, objMapC2V1, objMapC2P1, 1);

    auto pbdModel1 = std::make_shared<PbdModel>();
    pbdModel1->setModelGeometry(physicsMesh);
    pbdModel1->configure(/*Number of Constraints*/1,
        /*Constraint configuration*/"Distance 0.1",
        /*Mass*/0.0,
        /*Gravity*/"0 0 0",
        /*TimeStep*/0.0003,
        /*FixedPoint*/"",
        /*NumberOfIterationInConstraintSolver*/2, // 5
        /*Proximity*/0.3,  // use linemesh 0.3  // 0.01 use  //  cube 0.1
        /*Contact stiffness*/0.001);
    object11->setDynamicalModel(pbdModel1);
    scene->addSceneObject(object11);

    ////< XZH tool2
    auto object12 = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject2");

    auto objMapP2V2 = std::make_shared<imstk::OneToOneMapVESS>();  // std::make_shared<imstk::TriangleLineMap>(); //  std::make_shared<imstk::OneToOneMapVESS>();
    objMapP2V2->setMaster(physicsMesh);
    objMapP2V2->setSlave(visualMesh2);
    objMapP2V2->compute();


    auto objMapP2C2 = std::make_shared<imstk::OneToOneMapVESS>();  //  colliding is physical mesh
    objMapP2C2->setMaster(physicsMesh);
    objMapP2C2->setSlave(collidingMesh);
    objMapP2C2->compute();

    auto objMapC2V2 = std::make_shared<imstk::OneToOneMapVESS>(); //  std::make_shared<imstk::TriangleLineMap>(); //  s std::make_shared<imstk::OneToOneMapVESS>();
    objMapC2V2->setMaster(collidingMesh);
    objMapC2V2->setSlave(visualMesh2);
    objMapC2V2->compute();

    auto objMapC2P2 = std::make_shared<imstk::OneToOneMapVESS>(); // colliding is physical mesh
    objMapC2P2->setMaster(collidingMesh);
    objMapC2P2->setSlave(physicsMesh);
    objMapC2P2->compute();

    object12->addVisualModel(visualMesh2Model);
    object12->setCollidingGeometry(collidingMesh);
    object12->setPhysicsGeometry(physicsMesh);
    object12->setPhysicsToCollidingMap(objMapP2C2);
    object12->setPhysicsToVisualMap(objMapP2V2);
    object12->setCollidingToVisualMap(objMapC2V2);
    object12->setColldingToPhysicsMap(objMapC2P2);

    ////< XZH tool0
    object->setVisualMesh(visualMesh2, 1);
    object->setMaps(objMapP2V2, objMapP2C2, objMapC2V2, objMapC2P2, 1);

    auto pbdModel2 = std::make_shared<PbdModel>();
    pbdModel2->setModelGeometry(physicsMesh);
    pbdModel2->configure(/*Number of Constraints*/1,
        /*Constraint configuration*/"Distance 0.1",
        /*Mass*/0.0,
        /*Gravity*/"0 0 0",
        /*TimeStep*/0.0003,
        /*FixedPoint*/"",
        /*NumberOfIterationInConstraintSolver*/2, // 5
        /*Proximity*/0.3,  // use linemesh 0.3  // 0.01 use  //  cube 0.1
        /*Contact stiffness*/0.001);
    object12->setDynamicalModel(pbdModel2);
    scene->addSceneObject(object12);

    auto trackCtrl = std::make_shared<imstk::DeviceTracker>(client);
    trackCtrl->setTranslationScaling(0.0010); // (0.025);  // 0.02 0.25 // 0.1  test 0.1 should use  Scale 0.001 will be better 
    //auto controller = std::make_shared<imstk::PBDSceneObjectController>(object, trackCtrl);
    auto controller = std::make_shared<imstk::PBDSceneObjectController>(object11, object12, object, trackCtrl);
    controller->setActived(true);
    controller->setToolIndex(0);
    scene->addObjectController(controller);

    auto controller1 = std::make_shared<imstk::PBDSceneObjectController>(object12, trackCtrl);
    controller1->setActived(false);
    controller1->setToolIndex(1);
    //scene->addObjectController(controller1);

    // Sphere1
    auto sphere1Obj = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "Sphere1", 0.003, Vec3d(-14.4, -1.4, 7.9)); // -2.23
    sphere1Obj->getVisualGeometry()->setTranslation(Vec3d(-14.4, -1.4, 7.9));   ////< XZH KNIFE ROOT

    //// Sphere Test cutting surface middle
    //auto sphereDissectObj = apiutils::createCollidingAnalyticalSceneObject(
    //    imstk::Geometry::Type::Sphere, scene, "SphereDissect", 0.05, Vec3d(0, 0.095, 0.09)); // -2.23
    //sphereDissectObj->getVisualGeometry()->setTranslation(Vec3d(0, 0.095, 0.09));
    //Vec3d xzhDissectSphere = Vec3d(0, 0.095, 0.09) + 0.02 * Vec3d(0.22872038256899530, -0.33268419282762951, -0.91488153027598118);   ////< XZH normal of dissecting plane
    //auto sphereDissect2Obj = apiutils::createCollidingAnalyticalSceneObject(
    //    imstk::Geometry::Type::Sphere, scene, "SphereDissect2", 0.05, xzhDissectSphere); // -2.23
    //sphereDissect2Obj->getVisualGeometry()->setTranslation(xzhDissectSphere);

    auto materialSphere0 = std::make_shared<RenderMaterial>();
    auto diffuseTextureSphere0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/testtest.png", Texture::Type::DIFFUSE);
    materialSphere0->addTexture(diffuseTextureSphere0);
    auto visualSphere22 = std::make_shared<imstk::Sphere>();
    visualSphere22->setTranslation(Vec3d(-7.8, -1.2, 7.9));
    visualSphere22->setRadius(0.001);
    Vec3d mm2 = visualSphere22->getPosition();
    auto visualSphere22Model = std::make_shared<VisualModel>(visualSphere22);
    visualSphere22Model->setRenderMaterial(materialSphere0);
    auto sphere2Obj = std::make_shared<CollidingObject>("Sphere2");
    sphere2Obj->setCollidingGeometry(visualSphere22);
    sphere2Obj->addVisualModel(visualSphere22Model);
    scene->addSceneObject(sphere2Obj);  ////< XZH KNIFE TIP

    ////< XZH Axis |||||
    //auto sphereOrg = apiutils::createCollidingAnalyticalSceneObject(
    //    imstk::Geometry::Type::Sphere, scene, "SphereOrg", 0.05, Vec3d(0, 0, 0));
    //sphereOrg->getVisualGeometry()->setTranslation(Vec3d(0, 0, 0));
    auto materialOrg = std::make_shared<RenderMaterial>();
    auto diffTexOrg = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/yellow.png", Texture::Type::DIFFUSE);
    materialOrg->addTexture(diffTexOrg);
    auto sphereOrg = std::make_shared<imstk::Sphere>();
    sphereOrg->setTranslation(Vec3d(0.0, 0, 0));
    sphereOrg->setRadius(0.03);
    auto sphereOrgModel = std::make_shared<VisualModel>(sphereOrg);
    sphereOrgModel->setRenderMaterial(materialOrg);
    auto sphereOOO = std::make_shared<SceneObject>("SphereOrg");
    sphereOOO->addVisualModel(sphereOrgModel);
    scene->addSceneObject(sphereOOO);

    auto materialX = std::make_shared<RenderMaterial>();
    auto diffTexX = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/red.png", Texture::Type::DIFFUSE);
    materialX->addTexture(diffTexX);
    auto sphereX = std::make_shared<imstk::Sphere>();
    sphereX->setTranslation(Vec3d(0.4, 0, 0));
    sphereX->setRadius(0.03);
    auto sphereXModel = std::make_shared<VisualModel>(sphereX);
    sphereXModel->setRenderMaterial(materialX);
    auto sphereXXX = std::make_shared<SceneObject>("SphereX");
    sphereXXX->addVisualModel(sphereXModel);
    scene->addSceneObject(sphereXXX);

    auto materialY = std::make_shared<RenderMaterial>();
    auto diffTexY = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/green.png", Texture::Type::DIFFUSE);
    materialY->addTexture(diffTexY);
    auto sphereY = std::make_shared<imstk::Sphere>();
    sphereY->setTranslation(Vec3d(0, 0.4, 0));
    sphereY->setRadius(0.03);
    auto sphereYModel = std::make_shared<VisualModel>(sphereY);
    sphereYModel->setRenderMaterial(materialY);
    auto sphereYYY = std::make_shared<SceneObject>("SphereY");
    sphereYYY->addVisualModel(sphereYModel);
    scene->addSceneObject(sphereYYY);

    auto materialZ = std::make_shared<RenderMaterial>();
    auto diffTexZ = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/blue.png", Texture::Type::DIFFUSE);
    materialZ->addTexture(diffTexZ);
    auto sphereZ = std::make_shared<imstk::Sphere>();
    sphereZ->setTranslation(Vec3d(0, 0, 0.4));
    sphereZ->setRadius(0.03);
    auto sphereZModel = std::make_shared<VisualModel>(sphereZ);
    sphereZModel->setRenderMaterial(materialZ);
    auto sphereZZZ = std::make_shared<SceneObject>("SphereZ");
    sphereZZZ->addVisualModel(sphereZModel);
    scene->addSceneObject(sphereZZZ);

    auto probe = std::make_shared<IBLProbe>(DATA_ROOT_PATH "/VESS/texture/organIrradiance.dds",
        DATA_ROOT_PATH "/VESS/texture/organRadiance.dds",
        iMSTK_DATA_ROOT "/IBL/roomBRDF.png");
    scene->setGlobalIBLProbe(probe);

    Vec3d move = Vec3d(0, -5, 15);  // Vec3d(0, 35, 15);
    // Light (white)
    auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
    //whiteLight->setPosition(imstk::Vec3d(-2.011496, 3, 3.486862) + move); //(imstk::Vec3d(-2.111103, 5.906062, -24.198272) + move);
    whiteLight->setPosition(Vec3d(0.025, .092, 0.235));  // whiteLight->setPosition(Vec3d(2.5, 9.2, 23.5)); // 5.4)); // (imstk::Vec3d(6.871600, 13, 10.749434) + move); // (imstk::Vec3d(-2.111103, 5.906062, -24.198272) + move);
    whiteLight->setIntensity(0.005);//810;
    //whiteLight->setCastsShadow(false);
    scene->addLight(whiteLight);
    auto lightSphere = apiutils::createVisualAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "lightSphere", 1.0, Vec3d(0.025, .092, 0.235));
    lightSphere->getVisualGeometry()->setTranslation(whiteLight->getPosition());

    // Light (white)
    //auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
    //whiteLight->setPosition(imstk::Vec3d(-3.158539, 11.283570, -25.795277));
    ////whiteLight->setFocalPoint(imstk::Vec3d(1, -1, 0));
    //whiteLight->setIntensity(400);
    //scene->addLight(whiteLight);

    //// Light (red)
    //auto colorLight = std::make_shared<imstk::SpotLight>("colorLight");
    //colorLight->setPosition(imstk::Vec3d(-2.111103, 5.906062, -24.198272) + move); // - 25, -8, -4.5
    //colorLight->setFocalPoint(imstk::Vec3d(-6.532188, 16.588575, -13.986447) + move); // - 3.7, 5.5, 7
    //colorLight->setColor(imstk::Color::White); // red
    //colorLight->setSpotAngle(45);
    //colorLight->setIntensity(120);

    // Update Camera
    auto cam1 = scene->getCamera();
    cam1->setPosition(Vec3d(0.0258, 0.1502, 0.2136)); // (Vec3d(0.031, 0.0677, 0.233)); //  (Vec3d(3.4, 10.0, 10.3)); // (Vec3d(0.0, 57.0, 48)); //  (imstk::Vec3d(-12.5, -7, -33)); // -25, -8, -4.5
    cam1->setFocalPoint(Vec3d(0.025, 0.0028, 0.215)); // (Vec3d(0.067, 0.2287, 0.096)); // (Vec3d(3.0, -31.6, -26.1)); // (Vec3d(0.0, -30, -28));
    cam1->setFieldOfView(75); //  (75);
    cam1->setViewUp(0, 1, 0);
      ////< XZH Step 1 END

    ////< XZH vessels
    //std::string pathVessels = DATA_ROOT_PATH"/VESS/Nick/vessel01.obj";   ////< XZH cubeTet cubeTest tumorcolon7_scale2.obj
    //auto objMeshVessels = imstk::MeshIO::read(pathVessels);
    //auto visualVesselsMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMeshVessels);
    //visualVesselsMesh->computeBoundingBox(min1, max1);
    ////visualColonMesh->scale(0.01, imstk::Geometry::TransformType::ApplyToData);
    //visualVesselsMesh->rotate(imstk::UP_VECTOR, imstk::PI, imstk::Geometry::TransformType::ApplyToData);
    //visualVesselsMesh->translate(0, 0, 0.03, imstk::Geometry::TransformType::ApplyToData);
    //visualVesselsMesh->computeBoundingBox(min1, max1);
    //auto materialVessels = std::make_shared<RenderMaterial>();
    //materialVessels->setColor(imstk::Color::Red);
    //materialVessels->setDisplayMode(RenderMaterial::DisplayMode::SURFACE);
    //auto objectVessles = std::make_shared<imstk::VisualObject>("visualVessles00");
    //visualVesselsMesh->setRenderMaterial(materialVessels);
    //objectVessles->setVisualGeometry(visualVesselsMesh); // change to any mesh created above
    ////scene->addSceneObject(objectVessles);
    //std::string pathVessels1 = DATA_ROOT_PATH"/VESS/Nick/vessel01.obj";   ////< XZH cubeTet cubeTest tumorcolon7_scale2.obj
    //auto objMeshVessels1 = imstk::MeshIO::read(pathVessels1);
    //auto visualVesselsMesh1 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMeshVessels1);
    //visualVesselsMesh1->computeBoundingBox(min1, max1);
    ////visualColonMesh->scale(0.01, imstk::Geometry::TransformType::ApplyToData);
    //visualVesselsMesh1->rotate(imstk::UP_VECTOR, imstk::PI, imstk::Geometry::TransformType::ApplyToData);
    //visualVesselsMesh1->translate(0, 0, 0.03, imstk::Geometry::TransformType::ApplyToData);
    //visualVesselsMesh1->computeBoundingBox(min1, max1);
    //auto objectVessles1 = std::make_shared<imstk::VisualObject>("visualVessles01");
    //visualVesselsMesh1->setRenderMaterial(materialVessels);
    //objectVessles1->setVisualGeometry(visualVesselsMesh1); // change to any mesh created above
    //scene->addSceneObject(objectVessles1);

      ////< XZH Step 2: colon-tumor
    std::string pathColon = DATA_ROOT_PATH"/VESS/Nick/tumorcolon7_scale2_den.obj";   ////< XZH cubeTet cubeTest tumorcolon7_scale2.obj
    auto objMesh = imstk::MeshIO::read(pathColon);
    auto visualColonMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh);
    visualColonMesh->computeBoundingBox(min1, max1);
    //visualColonMesh->scale(0.01, imstk::Geometry::TransformType::ApplyToData);
    visualColonMesh->rotate(imstk::UP_VECTOR, imstk::PI, imstk::Geometry::TransformType::ApplyToData);
    //visualColonMesh->translate(0, 0, 0.1, imstk::Geometry::TransformType::ApplyToData);
    visualColonMesh->computeBoundingBox(min1, max1);
    auto posVisualList = visualColonMesh->getVertexPositions();
    auto triLists = visualColonMesh->getTrianglesVertices();
    visualColonMesh->setVertexPositionsXZH(posVisualList);
    visualColonMesh->setTrianglesVerticesXZH(triLists);

    std::string pathCube = DATA_ROOT_PATH"/VESS/cubeTet.obj";   ////< XZH cubeTet cubeTest
    auto objMeshCube = imstk::MeshIO::read(pathCube);
    auto visualCubeMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMeshCube);
    visualCubeMesh->rotate(imstk::UP_VECTOR, imstk::PI, imstk::Geometry::TransformType::ApplyToData);
    auto posVisualListCube = visualCubeMesh->getVertexPositions();
    auto triListsCube = visualCubeMesh->getTrianglesVertices();
    visualCubeMesh->setVertexPositionsXZH(posVisualListCube);
    visualCubeMesh->setTrianglesVerticesXZH(triListsCube);

    auto material = std::make_shared<RenderMaterial>();
    auto diffuseTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/Nick/diffuse2.png", Texture::Type::DIFFUSE);
    auto normalTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/Nick/normal.png", Texture::Type::NORMAL);
    auto roughnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/Nick/roughness.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    auto sssTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_sss.png", Texture::Type::SUBSURFACE_SCATTERING);
    material->addTexture(diffuseTexture);
    material->addTexture(normalTexture);
    material->addTexture(roughnessTexture);
    //material->setBackFaceCulling(false);
    material->setDisplayMode(RenderMaterial::DisplayMode::SURFACE);
    ////material->setTexture(metalnessTexture);
    //material->addTexture(sssTexture);
    //material->setTessellated(true);

    auto materialWire = std::make_shared<RenderMaterial>();
    auto diffuseTextureWire = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/Nick/diffuse2.png", Texture::Type::DIFFUSE);
    auto normalTextureWire = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/Nick/normal.png", Texture::Type::NORMAL);
    auto roughnessTextureWire = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/Nick/roughness.png", Texture::Type::ROUGHNESS);
    auto metalnessTextureWire = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    auto sssTextureWire = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_sss.png", Texture::Type::SUBSURFACE_SCATTERING);
    materialWire->addTexture(diffuseTextureWire);
    materialWire->addTexture(normalTextureWire);
    materialWire->addTexture(roughnessTextureWire);
    materialWire->setTessellated(true);
    //material->setBackFaceCulling(false);
    materialWire->setDisplayMode(RenderMaterial::DisplayMode::SURFACE);
    auto visualColonMeshModel = std::make_shared<VisualModel>(visualColonMesh);
    visualColonMeshModel->setRenderMaterial(material);
    //visualCubeMesh->setRenderMaterial(material);

    auto objectColonV = std::make_shared<imstk::VisualObject>("visualColonMesh0");
    objectColonV->addVisualModel(visualColonMeshModel); // change to any mesh created above
    //scene->addSceneObject(objectColonV);
    //auto objectCube = std::make_shared<imstk::VisualObject>("visualCubeMesh");
    //objectCube->setVisualGeometry(visualCubeMesh); // change to any mesh created above
    //scene->addSceneObject(objectCube);

    ////< XZH 2 and 3
    std::string pathColon2 = DATA_ROOT_PATH"/VESS/Nick/tumorcolon7s0_scale2.obj";  //  colon_s01
    std::string pathColon3 = DATA_ROOT_PATH"/VESS/Nick/tumorcolon7s1_scale2.obj";  // colon_s03
    auto objMesh2 = imstk::MeshIO::read(pathColon2);
    auto objMesh3 = imstk::MeshIO::read(pathColon3);
    auto visualColonMesh2 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh2);
    auto visualColonMesh3 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh3);
    visualColonMesh2->rotate(imstk::UP_VECTOR, imstk::PI, imstk::Geometry::TransformType::ApplyToData);
    visualColonMesh3->rotate(imstk::UP_VECTOR, imstk::PI, imstk::Geometry::TransformType::ApplyToData);
    auto visualColonMesh2Model = std::make_shared<VisualModel>(visualColonMesh2);
    auto visualColonMesh3Model = std::make_shared<VisualModel>(visualColonMesh3);
    visualColonMesh2Model->setRenderMaterial(material);
    visualColonMesh3Model->setRenderMaterial(material);
    auto object2 = std::make_shared<imstk::VisualObject>("visualColonMesh2");
    object2->addVisualModel(visualColonMesh2Model); // change to any mesh created above
    //scene->addSceneObject(object2);
    auto object3 = std::make_shared<imstk::VisualObject>("visualColonMesh3");
    object3->addVisualModel(visualColonMesh3Model); // change to any mesh created above
    //scene->addSceneObject(object3);

    ////< XZH TET    // Tetrahedral mesh
    std::vector<Vec3d> centralLine2;
    centralLine2.push_back(Vec3d(2.4, 1.7, 7.1));   ////< XZH  (Vec3d(-2.5, 7, -23));
    centralLine2.push_back(Vec3d(3.4, 11.8, 7.1));   ////< XZH  (Vec3d(-3.5, 15, -23));
    auto materialTet = std::make_shared<RenderMaterial>();
    //auto diffuseTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    materialTet->setColor(imstk::Color::Blue);
    materialTet->setDisplayMode(RenderMaterial::DisplayMode::SURFACE);

    imstk::StdVectorOfVec3d vertListCutTet;
    std::vector<imstk::TetrahedralMesh::TetraArray> cutTetConnectivity;
    std::vector<physXVertTetLink> liksVolSurCut;
    auto cutMesh = std::make_shared<imstk::VESSTetCutMesh>();
    //cutMesh->setVisualMeshXZH(visualColonMesh);
    //cutMesh->loadTetXZH(DATA_ROOT_PATH"/VESS/Nick/tumorcolon72_scale.tet", DATA_ROOT_PATH"/VESS/Nick/tumorcolon72_scale.fc", DATA_ROOT_PATH"/VESS/Nick/tumorcolon72_scale.fca", DATA_ROOT_PATH"/VESS/Nick/tumorcolon72_scale.eg", DATA_ROOT_PATH"/VESS/Nick/tumorcolon72_scale.ega", DATA_ROOT_PATH"/VESS/Nick/tumorcolon72_scale.nd", vertListCutTet, cutTetConnectivity, liksVolSurCut);   ////< XZH cubeTet cubeTest
    cutMesh->loadTetXZH(DATA_ROOT_PATH"/VESS/Nick/tumorcolon72_scale_den.tet", DATA_ROOT_PATH"/VESS/Nick/tumorcolon72_scale_den.fc", DATA_ROOT_PATH"/VESS/Nick/tumorcolon72_scale_den.fca", DATA_ROOT_PATH"/VESS/Nick/tumorcolon72_scale_den.eg", DATA_ROOT_PATH"/VESS/Nick/tumorcolon72_scale_den.ega", DATA_ROOT_PATH"/VESS/Nick/tumorcolon72_scale_den.nd", vertListCutTet, cutTetConnectivity, liksVolSurCut);   ////< XZH cubeTet cubeTest
    cutMesh->computeBoundingBox(min1, max1);
    //cutMesh->scale(0.01, imstk::Geometry::TransformType::ApplyToData);
    cutMesh->rotate(imstk::UP_VECTOR, imstk::PI, imstk::Geometry::TransformType::ApplyToData);
    cutMesh->rotate(imstk::RIGHT_VECTOR, imstk::PI/2, imstk::Geometry::TransformType::ApplyToData);
    cutMesh->updateVertexPositions();
    cutMesh->computeBoundingBox(min1, max1);
    ////< XZH test visual mesh extracted from the tet mesh
    //StdVectorOfVec3d posVisualListTest; 
    //std::vector<imstk::SurfaceMesh::TriangleArray> triListsTest;
    //auto visualColonMeshTest = std::make_shared<SurfaceMesh>();

    //visualColonMeshTest->initialize(posVisualListTest, triListsTest);
    cutMesh->extractSurfaceMeshVisual();
    auto visualColonMeshTet = std::dynamic_pointer_cast<imstk::SurfaceMesh>(cutMesh->getVisualSurface());
    //visualColonMeshTet->rotate(imstk::UP_VECTOR, imstk::PI, imstk::Geometry::TransformType::ApplyToData);
    auto visualColonMeshTetModel = std::make_shared<VisualModel>(visualColonMeshTet);
    visualColonMeshTetModel->setRenderMaterial(materialWire);
    auto posVisualListTet = visualColonMeshTet->getVertexPositions();
    auto triListsTet = visualColonMeshTet->getTrianglesVertices();
    visualColonMeshTet->setVertexPositionsXZH(posVisualListTet);
    visualColonMeshTet->setTrianglesVerticesXZH(triListsTet);
    

    auto objectColon = std::make_shared<imstk::VisualObject>("visualColonMesh");
    objectColon->addVisualModel(visualColonMeshTetModel); // change to any mesh created above
    //scene->addSceneObject(objectColon);

    cutMesh->setVisualMeshXZH(visualColonMeshTet);
    cutMesh->setVisualMeshTexture(visualColonMesh);
    cutMesh->calcTex2TetSurfTriLink();   ////< XZH the above two link

    cutMesh->extractSurfaceMeshBoundary(centralLine2);
    cutMesh->extractSurfaceMeshVolume();
    cutMesh->mappingLinks();
    cutMesh->mappingTriTetLinks();
    cutMesh->mappingTetTriLinks();

    cutMesh->initializeCuttingMesh();
    cutMesh->initializeCuttingPara();
    auto surfaceMeshB = cutMesh->getBoundarySurface();

    ////< XZH 
    auto& statesCut = cutMesh->getTetrahedronStates();
    statesCut.clear();
    auto& mapsCut = cutMesh->getTetrahedronMap();
    mapsCut.clear();
    for (int i = 0; i < cutTetConnectivity.size(); i++)
    {
        statesCut.push_back(1); // exist
        mapsCut.push_back(1); // original
    }

    auto surfaceMeshBModel = std::make_shared<VisualModel>(surfaceMeshB);
    surfaceMeshBModel->setRenderMaterial(materialTet);
    auto objectB = std::make_shared<imstk::VisualObject>("CutColonMeshBoundary");
    objectB->addVisualModel(surfaceMeshBModel); // change to any mesh created above
    //scene->addSceneObject(objectB);
    surfaceMeshB->computeBoundingBox(minBox, maxBox);
    printf("min: %f %f %f\n max: %f %f %f\n", minBox.x(), minBox.y(), minBox.z(), maxBox.x(), maxBox.y(), maxBox.z());

    auto materialTetB = std::make_shared<RenderMaterial>();
    //auto diffuseTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    materialTetB->setColor(imstk::Color::Red);
    materialTetB->setDisplayMode(RenderMaterial::DisplayMode::WIREFRAME);

    auto surfaceMeshB1 = cutMesh->getBoundarySurfaceInside();
    auto surfaceMeshB1Model = std::make_shared<VisualModel>(surfaceMeshB1);
    surfaceMeshB1Model->setRenderMaterial(materialTet);
    auto surfaceMeshB2 = cutMesh->getBoundarySurfaceOutside();
    auto surfaceMeshB2Model = std::make_shared<VisualModel>(surfaceMeshB2);
    surfaceMeshB2Model->setRenderMaterial(materialTetB);

    auto objectB1 = std::make_shared<imstk::VisualObject>("CutColonMeshBoundary1");   ////< XZH inside
    objectB1->addVisualModel(surfaceMeshB1Model); // change to any mesh created above
    //scene->addSceneObject(objectB1);
    auto objectB2 = std::make_shared<imstk::VisualObject>("CutColonMeshBoundary2");   ////< XZH outside
    objectB2->addVisualModel(surfaceMeshB2Model); // change to any mesh created above
    //scene->addSceneObject(objectB2);

    ////< XZH surface volume
    auto surfaceVolume = cutMesh->getVolumeSurface();
    auto surfaceVolumeModel = std::make_shared<VisualModel>(surfaceVolume);
    surfaceVolumeModel->setRenderMaterial(material);
    auto objectBV = std::make_shared<imstk::VisualObject>("CutColonMeshVolume");   ////< XZH inside
    objectBV->addVisualModel(surfaceVolumeModel); // change to any mesh created above
    //scene->addSceneObject(objectBV);


    ////< XZH test triangle
    auto materialLine = std::make_shared<RenderMaterial>();
    materialLine->setColor(imstk::Color::Red);
    materialLine->setDisplayMode(RenderMaterial::DisplayMode::SURFACE);
    std::shared_ptr<SurfaceMesh> surfMeshCentralLine = std::make_shared<SurfaceMesh>();
    StdVectorOfVec3d vertListLine;

    vertListLine.push_back(Vec3d(2.4, 1.7, 7.1)); // 0
    vertListLine.push_back(Vec3d(3.4, 11.8, 7.1));  // 1
    vertListLine.push_back(Vec3d(2.6, 1.7, 7.1)); // 2
    SurfaceMesh::TriangleArray triLine[2];
    triLine[0] = { { 0, 1, 2 } };
    triLine[1] = { { 0, 2, 1 } };
    std::vector<SurfaceMesh::TriangleArray> trianglesLine;
    trianglesLine.push_back(triLine[0]);
    trianglesLine.push_back(triLine[1]);
    auto surfMeshCentralLineModel = std::make_shared<VisualModel>(surfMeshCentralLine);
    surfMeshCentralLine->setTrianglesVertices(trianglesLine);
    surfMeshCentralLine->setInitialVertexPositions(vertListLine);
    surfMeshCentralLine->setVertexPositions(vertListLine);
    surfMeshCentralLine->initialize(vertListLine, trianglesLine);
    surfMeshCentralLineModel->setRenderMaterial(materialLine);
    auto objectLine = std::make_shared<imstk::VisualObject>("surfMeshCentralLine");
    objectLine->addVisualModel(surfMeshCentralLineModel);
    scene->addSceneObject(objectLine);

    ////< XZH SPH Fluid Start
    //auto sphFluid = std::make_shared<SPHXZH>();
    //sphFluid->init_system();
    char name[256];
    cutMesh->initSPHParticles();
    auto sphFluid = cutMesh->getSPHParticles();
    auto& fluidPoses = sphFluid->getParticlePoses();
    for (int i = 0; i < fluidPoses.size(); i++)
    {
        sprintf(name, "XZHFluidSphere%d", i);
        auto visualSphereFluid = std::make_shared<imstk::Sphere>();
        visualSphereFluid->setTranslation(fluidPoses[i]);
        visualSphereFluid->setRadius(0.004);
        auto visualSphereFluidObject = std::make_shared<VisualModel>(visualSphereFluid);
        visualSphereFluidObject->setRenderMaterial(materialX);        
        auto sphereFluidObj = std::make_shared<SceneObject>(name);
        sphereFluidObj->addVisualModel(visualSphereFluidObject);
        //scene->addSceneObject(sphereFluidObj);
    }
    ////< XZH SPH Fluid END

    ////< XZH 
    // Construct a map
    auto colonMapP2V = std::make_shared<imstk::OneToOneMapVESS>();
    colonMapP2V->setMaster(cutMesh);
    colonMapP2V->setSlave(visualColonMeshTet);
    colonMapP2V->computeNewXZH();

    auto colonMapP2C = std::make_shared<imstk::OneToOneMapVESS>();
    colonMapP2C->setMaster(cutMesh);
    colonMapP2C->setSlave(cutMesh);
    colonMapP2C->compute();

    auto colonObj = std::make_shared<PbdObject>("Colon");
    colonObj->setCollidingGeometry(cutMesh);
    colonObj->addVisualModel(visualColonMeshTetModel);
    colonObj->setPhysicsGeometry(cutMesh);
    colonObj->setPhysicsToCollidingMap(colonMapP2C);
    colonObj->setPhysicsToVisualMap(colonMapP2V);
    //colonObj->setCollidingToVisualMap(colonMapC2V);

      ////< XZH test plane
    auto planeDissect = std::make_shared<imstk::Plane>();
    planeDissect->setPosition(Vec3d(0, 0, 0.19));  // -4.68, -0.63, 1.48
    planeDissect->setWidth(0.2);
    planeDissect->setTranslation(Vec3d(0, 0, 0.19));
    auto planeDissectModel = std::make_shared<VisualModel>(planeDissect);
    auto planeDissectObj = std::make_shared<imstk::CollidingObject>("planeDissect");
    planeDissectObj->addVisualModel(planeDissectModel);
    planeDissectObj->setCollidingGeometry(planeDissect);
    scene->addSceneObject(planeDissectObj);


    //fixed point
    Vec3d posTumorCenter = Vec3d(3.5, 8.9, 19.0)*0.01;
    auto& posTetlist2 = cutMesh->getVertexPositions();
    std::string fixed_corner2;
    double distTumor = 2.0*0.01;
    char intStr2[33];
    Vec3d tmpPos2;
    std::vector<int> fixedVertice2; // add later 06/04/2017
    std::vector<int> fixedVerticeStates2; // fixed states
    for (int i = 0; i < cutMesh->getNumVertices(); ++i) // physicsColonMesh  visualColonMesh
    {
        double dt = (posTetlist2[i] - posTumorCenter).norm();

        if (dt > distTumor)   ////< XZH for colon
        //if (posTetlist2[i].z() < 8.0) // for CUBE
        {
            std::sprintf(intStr2, "%d", i + 1);
            fixed_corner2 += std::string(intStr2) + ' ';
            fixedVertice2.push_back(i); //  all vertices
            fixedVerticeStates2.push_back(1);
        }
        else
        {
            fixedVerticeStates2.push_back(0);
        }
    }

    auto pbdColonModel = std::make_shared<PbdModel>();
    pbdColonModel->setModelGeometry(cutMesh);
    pbdColonModel->configure(/*Number of Constraints*/2,
        /*Constraint configuration*/"Volume 0.80",  // 0.7 "Dihedral 0.8", // 0.01 0.8 effect is couhe not very well
        /*Constraint configuration*/"Distance 0.95",  // 0.9 effect is couhe
        /*Mass*/0.1, // 0.1
        /*Gravity*/"0 0 0.098",  // "0 -9.8 0",
        /*TimeStep*/0.004, // 0.004, // 0.001
        /*FixedPoint*/ fixed_corner2.c_str(), // "1 2", //   ////< XZH  fixed_corner2.c_str(),
        /*NumberOfIterationInConstraintSolver*/3, //   2 12
        /*Proximity*/0.01,  // 0.1
        /*Contact stiffness*/0.1);
    colonObj->setDynamicalModel(pbdColonModel);

    auto pbdColonSolver = std::make_shared<PbdSolver>();
    pbdColonSolver->setPbdObject(colonObj);
    scene->addNonlinearSolver(pbdColonSolver);

    scene->addSceneObject(colonObj);

    FILE *outPos = fopen("i:/outPosCubeZ.dat", "w");
    fprintf(outPos, "===============================\n\n");
    for (int i = 0; i < posTetlist2.size(); i++)
    {
        fprintf(outPos, "%f %f %f\n", posTetlist2[i].x(), posTetlist2[i].y(), posTetlist2[i].z());
    }
    fclose(outPos);

     //graph sphere Tet
    auto sphereTumor = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereTumor3", distTumor, posTumorCenter);
    sphereTumor->getVisualGeometry()->setTranslation(posTumorCenter);
      //< XZH Step 2 END



      ////< XZH Step3: Run
    sdk->setActiveScene(scene);
    //  ////< XZH key switch
    auto viewer = std::dynamic_pointer_cast<VulkanViewer>(sdk->getViewer());
    viewer->setResolution(1920, 1000);
    //viewer->setResolution(1920, 1080);   ////< XZH 1920*1080
    //viewer->enableFullscreen();
    sdk->startSimulation(SimulationStatus::INACTIVE);
#endif
}

  ////< XZH test CCD with simple mesh
void testCCD()
{
#ifdef iMSTK_USE_OPENHAPTICS
    ////< XZH scale
    double scalexzh = 0.01*0.25;   ////< XZH for scale to the normal, small 100times first, then 4 times again

    // SDK and Scene
    auto sdk = std::make_shared<imstk::SimulationManager>();
    auto scene = sdk->createNewScene("SceneTestDevice");

    Vec3d minBox, maxBox;
    // Device Client
    auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

    // Device Server
    auto server = std::make_shared<imstk::HDAPIDeviceServer>();
    server->addDeviceClient(client);
    sdk->addModule(server);

    ////< XZH Cutting
    std::string path2obj_c = DATA_ROOT_PATH"/VESS/Nick/dualKnife.obj"; //  ITKnife7_4.obj  knife correct ITKnife6  // cube knife knife3.obj";  //  //  113.obj";  // / VESS / TriangleTipKnife2.obj";  // blade2.obj  cube.obj kcube_s2.obj
    auto readMesh = imstk::MeshIO::read(path2obj_c); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
    Vec3d vertexPos3;
    Vec3d min1, max1;
    readMesh->computeBoundingBox(min1, max1);

    auto visualMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(readMesh); // std::make_shared<imstk::SurfaceMesh>(); //  
    visualMesh->rotate(imstk::BACKWARD_VECTOR, -imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);
    visualMesh->computeBoundingBox(min1, max1);
    auto& initPosesTool = visualMesh->getVertexPositionsChangeable();
    //Vec3d moveTmp = Vec3d(-9.093, 1.6348, -8.859199524)*scalexzh;   ////< XZH avg, avg, min  full DOFs
    Vec3d moveTmp = Vec3d(1.6348, -8.859199524, -9.093)*scalexzh + Vec3d(1.0, 0, -0.0)*scalexzh * 4;   ////< XZH avg, avg, min Camera endoscopy
    for (int i = 0; i < initPosesTool.size(); i++)
    {
        Vec3d pos = initPosesTool[i];
        visualMesh->setVertexPosition(i, pos + moveTmp);
    }
    //auto& initPosesToolXZH = visualMesh->getVertexPositionsChangeable();
    visualMesh->setInitialVertexPositions(initPosesTool);
    visualMesh->setInitialVertexPositionsChangeable(initPosesTool);

    visualMesh->computeBoundingBox(min1, max1);
    Vec3d x0, x1;
    x1.z() = (min1.z() + max1.z())*0.5;
    x1.y() = min1.y(); // (min1.y() + max1.y())*0.5;
    x1.x() = (min1.x() + max1.x())*0.5;
    x0.z() = (min1.z() + max1.z())*0.5; // max1.z();
    x0.y() = max1.y(); // (min1.y() + max1.y())*0.5;
    x0.x() = (min1.x() + max1.x())*0.5;

    ////< XZH tool1 Marking
    std::string path2obj_c1 = DATA_ROOT_PATH"/VESS/Nick/dualKnife.obj"; // DATA_ROOT_PATH"/VESS/HybridKnife.obj"; // DATA_ROOT_PATH"/VESS/ITKnife7_4H.obj"; //  ITKnife7_4.obj  knife correct ITKnife6  // cube knife knife3.obj";  //  //  113.obj";  // / VESS / TriangleTipKnife2.obj";  // blade2.obj  cube.obj kcube_s2.obj
    auto readMesh1 = imstk::MeshIO::read(path2obj_c1); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
    auto visualMesh1 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(readMesh1); // std::make_shared<imstk::SurfaceMesh>(); //  
    visualMesh1->rotate(imstk::BACKWARD_VECTOR, -imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);  // visualMesh1->rotate(imstk::BACKWARD_VECTOR, -imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);
    //visualMesh->rotate(imstk::RIGHT_VECTOR, -imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);
    auto& initPosesTool1 = visualMesh1->getVertexPositionsChangeable();
    for (int i = 0; i < initPosesTool1.size(); i++)
    {
        Vec3d pos = initPosesTool1[i];
        visualMesh1->setVertexPosition(i, pos + moveTmp);
    }
    //auto& initPosesTool1XZH = visualMesh1->getVertexPositionsChangeable();
    visualMesh1->setInitialVertexPositions(initPosesTool1);
    visualMesh1->setInitialVertexPositionsChangeable(initPosesTool1);

    ////< XZH tool2 Injecting
    std::string path2obj_c2 = DATA_ROOT_PATH"/VESS/Nick/needleKnife.obj"; // DATA_ROOT_PATH"/VESS/HybridKnife.obj"; // DATA_ROOT_PATH"/VESS/ITKnife7_4H.obj"; //  ITKnife7_4.obj  knife correct ITKnife6  // cube knife knife3.obj";  //  //  113.obj";  // / VESS / TriangleTipKnife2.obj";  // blade2.obj  cube.obj kcube_s2.obj
    auto readMesh2 = imstk::MeshIO::read(path2obj_c2); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
    auto visualMesh2 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(readMesh2); // std::make_shared<imstk::SurfaceMesh>(); //  
    visualMesh2->rotate(imstk::BACKWARD_VECTOR, -imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);  // visualMesh2->rotate(imstk::UP_VECTOR, imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);
    //visualMesh->rotate(imstk::RIGHT_VECTOR, -imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);
    auto& initPosesTool2 = visualMesh2->getVertexPositionsChangeable();
    for (int i = 0; i < initPosesTool2.size(); i++)
    {
        Vec3d pos = initPosesTool2[i];
        visualMesh2->setVertexPosition(i, pos + moveTmp);
    }
    visualMesh2->setInitialVertexPositions(initPosesTool2);
    visualMesh2->setInitialVertexPositionsChangeable(initPosesTool2);

    visualMesh2->computeBoundingBox(min1, max1);
    // Line mesh
    auto lineMesh = std::make_shared<imstk::LineMesh>();
    imstk::StdVectorOfVec3d vertList;
    vertList.resize(2);  // using 5 segment
    vertList[0] = x0; // imstk::Vec3d(-14.4, -1.4, 7.9);
    vertList[1] = x1; // imstk::Vec3d(-7.8, -1.2, 7.9);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    double lineLen = (x0 - x1).norm(); // offset for colliding mesh 20190228
    vertList[0] = Vec3d(0, 0, 0) + lineLen*Vec3d(0, 1, 0) + Vec3d(1.0, 0, -0.0)*scalexzh * 4; // *Vec3d(0, 0, 1)*scalexzh;
    vertList[1] = Vec3d(0, 0, 0) + Vec3d(1.0, 0, -0.0)*scalexzh * 4;
    Vec3d moveLine = vertList[1] - x1;
    //std::vector<std::vector<int> > connectivity;
    std::vector<LineMesh::LineArray> connectivity;
    for (int i = 0; i < 1;){
        //std::vector<int> line;
        LineMesh::LineArray line;
        line[0] = i;
        //line.push_back(i);
        i++;
        line[1] = i;
        //line.push_back(i);

        connectivity.push_back(line);
    }

    lineMesh->setInitialVertexPositions(vertList);
    lineMesh->setVertexPositions(vertList);
    lineMesh->setLinesVertices(connectivity); //  setConnectivity(connectivity);
    //lineMesh->rotate(imstk::UP_VECTOR, imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);

    auto physicsMesh = lineMesh; //visualMesh; //  lineMesh; //  imstk::MeshIO::read(path2obj_c);  //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
    auto collidingMesh = lineMesh; // visualMesh; //  lineMesh;
    //visualMesh->setRotation(Vec3d(0, 0, 1),PI/4); // move

    auto material0 = std::make_shared<RenderMaterial>();
    auto diffuseTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/texture/DualKnife_BaseColor.png", Texture::Type::DIFFUSE);
    auto normalTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/texture/DualKnife_Normal.png", Texture::Type::NORMAL);
    auto roughnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/texture/DualKnife_Roughness.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/texture/DualKnife_Metallic.png", Texture::Type::METALNESS);
    auto aoTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/texture/DualKnife_Occlusion.png", Texture::Type::AMBIENT_OCCLUSION);
    material0->addTexture(diffuseTexture0);
    material0->addTexture(normalTexture0);
    material0->addTexture(roughnessTexture0);
    material0->addTexture(metalnessTexture0);
    material0->addTexture(aoTexture0);
    material0->setDisplayMode(imstk::RenderMaterial::SURFACE);

    auto material01 = std::make_shared<RenderMaterial>();
    auto diffuseTexture01 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/texture/InjectionNeedle_BaseColor.png", Texture::Type::DIFFUSE);
    auto normalTexture01 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/texture/InjectionNeedle_Normal.png", Texture::Type::NORMAL);
    auto roughnessTexture01 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/texture/InjectionNeedle_Roughness.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture01 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/texture/InjectionNeedle_Metallic.png", Texture::Type::METALNESS);
    auto aoTexture01 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/texture/InjectionNeedle_Occlusion.png", Texture::Type::AMBIENT_OCCLUSION);
    material01->addTexture(diffuseTexture01);
    material01->addTexture(normalTexture01);
    material01->addTexture(roughnessTexture01);
    material01->addTexture(metalnessTexture01);
    material01->addTexture(aoTexture01);
    material01->setDisplayMode(imstk::RenderMaterial::SURFACE);

    auto visualMeshModel = std::make_shared<VisualModel>(visualMesh);
    auto visualMeshModel1 = std::make_shared<VisualModel>(visualMesh1);
    auto visualMeshModel2 = std::make_shared<VisualModel>(visualMesh2);

    visualMeshModel->setRenderMaterial(material0);
    visualMeshModel1->setRenderMaterial(material0);
    visualMeshModel2->setRenderMaterial(material01);

    auto object = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject");

    auto objMapP2V = std::make_shared<imstk::OneToOneMapVESS>();  // std::make_shared<imstk::TriangleLineMap>(); //  std::make_shared<imstk::OneToOneMapVESS>();
    objMapP2V->setMaster(physicsMesh);
    objMapP2V->setSlave(visualMesh);
    objMapP2V->compute();

    auto objMapP2C = std::make_shared<imstk::OneToOneMapVESS>();  //  colliding is physical mesh
    objMapP2C->setMaster(physicsMesh);
    objMapP2C->setSlave(collidingMesh);
    objMapP2C->compute();

    auto objMapC2V = std::make_shared<imstk::OneToOneMapVESS>(); //  std::make_shared<imstk::TriangleLineMap>(); //  s std::make_shared<imstk::OneToOneMapVESS>();
    objMapC2V->setMaster(collidingMesh);
    objMapC2V->setSlave(visualMesh);
    objMapC2V->compute();

    auto objMapC2P = std::make_shared<imstk::OneToOneMapVESS>(); // colliding is physical mesh
    objMapC2P->setMaster(collidingMesh);
    objMapC2P->setSlave(physicsMesh);
    objMapC2P->compute();

    object->addVisualModel(visualMeshModel);
    object->setCollidingGeometry(collidingMesh);
    object->setPhysicsGeometry(physicsMesh);
    object->setPhysicsToCollidingMap(objMapP2C);
    object->setPhysicsToVisualMap(objMapP2V);
    object->setCollidingToVisualMap(objMapC2V);
    object->setColldingToPhysicsMap(objMapC2P);

    Vec3d pos0 = collidingMesh->getVertexPosition(0);
    auto& posList = visualMesh->getVertexPositionsChangeable();
    std::vector<Vec3d>& dirGapVisualDissecting0 = object->getGapVisualDissecting();
    for (int i = 0; i < posList.size(); i++)
    {
        dirGapVisualDissecting0.push_back(posList[i] - pos0);
    }

    ////< XZH tool0
    object->setVisualMesh(visualMesh, 0);
    object->setMaps(objMapP2V, objMapP2C, objMapC2V, objMapC2P, 0);

    auto pbdModel = std::make_shared<PbdModel>();
    pbdModel->setModelGeometry(physicsMesh);
    pbdModel->configure(/*Number of Constraints*/1,
        /*Constraint configuration*/"Distance 0.1",
        /*Mass*/0.0,
        /*Gravity*/"0 0 0",
        /*TimeStep*/0.0003,
        /*FixedPoint*/"",
        /*NumberOfIterationInConstraintSolver*/2, // 5
        /*Proximity*/0.003, //0.3, // use linemesh 0.3  // 0.01 use  //  cube 0.1
        /*Contact stiffness*/0.001);
    object->setDynamicalModel(pbdModel);
    scene->addSceneObject(object);

    ////< XZH tool1
    auto object11 = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject1");

    auto objMapP2V1 = std::make_shared<imstk::OneToOneMapVESS>();  // std::make_shared<imstk::TriangleLineMap>(); //  std::make_shared<imstk::OneToOneMapVESS>();
    objMapP2V1->setMaster(physicsMesh);
    objMapP2V1->setSlave(visualMesh1);
    objMapP2V1->compute();

    auto objMapP2C1 = std::make_shared<imstk::OneToOneMapVESS>();  //  colliding is physical mesh
    objMapP2C1->setMaster(physicsMesh);
    objMapP2C1->setSlave(collidingMesh);
    objMapP2C1->compute();

    auto objMapC2V1 = std::make_shared<imstk::OneToOneMapVESS>(); //  std::make_shared<imstk::TriangleLineMap>(); //  s std::make_shared<imstk::OneToOneMapVESS>();
    objMapC2V1->setMaster(collidingMesh);
    objMapC2V1->setSlave(visualMesh1);
    objMapC2V1->compute();

    auto objMapC2P1 = std::make_shared<imstk::OneToOneMapVESS>(); // colliding is physical mesh
    objMapC2P1->setMaster(collidingMesh);
    objMapC2P1->setSlave(physicsMesh);
    objMapC2P1->compute();

    object11->addVisualModel(visualMeshModel1);
    object11->setCollidingGeometry(collidingMesh);
    object11->setPhysicsGeometry(physicsMesh);
    object11->setPhysicsToCollidingMap(objMapP2C1);
    object11->setPhysicsToVisualMap(objMapP2V1);
    object11->setCollidingToVisualMap(objMapC2V1);
    object11->setColldingToPhysicsMap(objMapC2P1);

    ////< XZH tool0
    object11->setVisualMesh(visualMesh1, 1);
    object11->setMaps(objMapP2V1, objMapP2C1, objMapC2V1, objMapC2P1, 1);

    auto pbdModel1 = std::make_shared<PbdModel>();
    pbdModel1->setModelGeometry(physicsMesh);
    pbdModel1->configure(/*Number of Constraints*/1,
        /*Constraint configuration*/"Distance 0.1",
        /*Mass*/0.0,
        /*Gravity*/"0 0 0",
        /*TimeStep*/0.0003,
        /*FixedPoint*/"",
        /*NumberOfIterationInConstraintSolver*/2, // 5
        /*Proximity*/0.003, //0.3, // use linemesh 0.3  // 0.01 use  //  cube 0.1
        /*Contact stiffness*/0.001);
    object11->setDynamicalModel(pbdModel1);
    scene->addSceneObject(object11);

    ////< XZH tool2
    auto object12 = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject2");

    auto objMapP2V2 = std::make_shared<imstk::OneToOneMapVESS>();  // std::make_shared<imstk::TriangleLineMap>(); //  std::make_shared<imstk::OneToOneMapVESS>();
    objMapP2V2->setMaster(physicsMesh);
    objMapP2V2->setSlave(visualMesh2);
    objMapP2V2->compute();

    auto objMapP2C2 = std::make_shared<imstk::OneToOneMapVESS>();  //  colliding is physical mesh
    objMapP2C2->setMaster(physicsMesh);
    objMapP2C2->setSlave(collidingMesh);
    objMapP2C2->compute();

    auto objMapC2V2 = std::make_shared<imstk::OneToOneMapVESS>(); //  std::make_shared<imstk::TriangleLineMap>(); //  s std::make_shared<imstk::OneToOneMapVESS>();
    objMapC2V2->setMaster(collidingMesh);
    objMapC2V2->setSlave(visualMesh2);
    objMapC2V2->compute();

    auto objMapC2P2 = std::make_shared<imstk::OneToOneMapVESS>(); // colliding is physical mesh
    objMapC2P2->setMaster(collidingMesh);
    objMapC2P2->setSlave(physicsMesh);
    objMapC2P2->compute();

    object12->addVisualModel(visualMeshModel2);
    object12->setCollidingGeometry(collidingMesh);
    object12->setPhysicsGeometry(physicsMesh);
    object12->setPhysicsToCollidingMap(objMapP2C2);
    object12->setPhysicsToVisualMap(objMapP2V2);
    object12->setCollidingToVisualMap(objMapC2V2);
    object12->setColldingToPhysicsMap(objMapC2P2);

    ////< XZH tool0
    object->setVisualMesh(visualMesh2, 1);
    object->setMaps(objMapP2V2, objMapP2C2, objMapC2V2, objMapC2P2, 1);

    auto pbdModel2 = std::make_shared<PbdModel>();
    pbdModel2->setModelGeometry(physicsMesh);
    pbdModel2->configure(/*Number of Constraints*/1,
        /*Constraint configuration*/"Distance 0.1",
        /*Mass*/0.0,
        /*Gravity*/"0 0 0",
        /*TimeStep*/0.0003,
        /*FixedPoint*/"",
        /*NumberOfIterationInConstraintSolver*/2, // 5
        /*Proximity*/0.003, //0.3,  // use linemesh 0.3  // 0.01 use  //  cube 0.1
        /*Contact stiffness*/0.001);
    object12->setDynamicalModel(pbdModel2);
    scene->addSceneObject(object12);

    auto trackCtrl = std::make_shared<imstk::DeviceTracker>(client);
    trackCtrl->setTranslationScaling(0.0010); // (0.025);  // 0.02 0.25 // 0.1  test 0.1 should use  Scale 0.001 will be better 
    //auto controller = std::make_shared<imstk::PBDSceneObjectController>(object, trackCtrl);
    auto controller = std::make_shared<imstk::PBDSceneObjectController>(object11, object12, object, trackCtrl);
    controller->setActived(true);
    controller->setToolIndex(0);
    //scene->addObjectController(controller);

    auto controller1 = std::make_shared<imstk::PBDSceneObjectController>(object12, trackCtrl);
    controller1->setActived(false);
    controller1->setToolIndex(1);
    //scene->addObjectController(controller1);

    // Update Camera position
    auto cam = scene->getCamera();
    cam->setPosition(Vec3d(0, 0, 10 * scalexzh));
    auto camControllerInput = std::make_shared<XZHTool>(object11, object12, object, *cam, client);  ////< XZH only camera controller std::make_shared<CameraController>(*cam, client);
    int mm = 1;
    // Set camera controller
    auto camController = cam->setController(camControllerInput);

    auto materialLineRed = std::make_shared<RenderMaterial>();
    materialLineRed->setColor(imstk::Color::Red);
    materialLineRed->setLineWidth(5.0);
    materialLineRed->setDisplayMode(RenderMaterial::DisplayMode::SURFACE);
    auto materialLineGreen = std::make_shared<RenderMaterial>();
    materialLineGreen->setColor(imstk::Color::Green);
    materialLineGreen->setLineWidth(5.0);
    materialLineGreen->setDisplayMode(RenderMaterial::DisplayMode::SURFACE);
    auto materialLineBlue = std::make_shared<RenderMaterial>();
    materialLineBlue->setColor(imstk::Color::Blue);
    materialLineBlue->setLineWidth(5.0);
    materialLineBlue->setDisplayMode(RenderMaterial::DisplayMode::SURFACE);

    auto materialYellow = std::make_shared<RenderMaterial>();
    auto diffTexYellow = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/yellow.png", Texture::Type::DIFFUSE);
    //materialYellow->addTexture(diffTexYellow);
    materialYellow->setColor(imstk::Color::Yellow);
    //materialYellow->setDisplayMode(RenderMaterial::DisplayMode::SURFACE);
    auto materialRed = std::make_shared<RenderMaterial>();
    auto diffTexRed = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/red.png", Texture::Type::DIFFUSE);
    //materialRed->addTexture(diffTexRed);
    materialRed->setColor(imstk::Color::Red);
    materialRed->setDisplayMode(imstk::RenderMaterial::DisplayMode::POINTS);
    //materialRed->setLineWidth(50.0);
    auto materialGreen = std::make_shared<RenderMaterial>();
    auto diffTexGreen = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/green.png", Texture::Type::DIFFUSE);
    //materialGreen->addTexture(diffTexGreen);
    materialGreen->setColor(imstk::Color::Green);
    //materialGreen->setLineWidth(50.0);
    materialGreen->setDisplayMode(imstk::RenderMaterial::DisplayMode::POINTS);
    auto materialBlue = std::make_shared<RenderMaterial>();
    auto diffTexBlue = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/blue.png", Texture::Type::DIFFUSE);
    //materialBlue->addTexture(diffTexBlue);
    materialBlue->setColor(imstk::Color::Blue);
    //materialBlue->setLineWidth(50.0);
    materialBlue->setDisplayMode(imstk::RenderMaterial::DisplayMode::POINTS);

    auto sphereOrg = std::make_shared<imstk::Sphere>();
    sphereOrg->setTranslation(Vec3d(0.0, 0, 0));
    sphereOrg->setRadius(0.01);
    auto sphereOrgModel = std::make_shared<VisualModel>(sphereOrg);
    sphereOrgModel->setRenderMaterial(materialYellow);
    auto sphereOOO = std::make_shared<SceneObject>("SphereOrg");
    sphereOOO->addVisualModel(sphereOrgModel);
    scene->addSceneObject(sphereOOO);

    auto sphereX = std::make_shared<imstk::Sphere>();
    sphereX->setTranslation(Vec3d(0.4, 0, 0));
    sphereX->setRadius(0.01);
    auto sphereXModel = std::make_shared<VisualModel>(sphereX);
    sphereXModel->setRenderMaterial(materialRed);
    auto sphereXXX = std::make_shared<SceneObject>("SphereX");
    sphereXXX->addVisualModel(sphereXModel);
    scene->addSceneObject(sphereXXX);
    StdVectorOfVec3d vertListAxis;
    std::shared_ptr<LineMesh> axisX = std::make_shared<LineMesh>();
    vertListAxis.push_back(Vec3d(0, 0, 0)); // 0
    vertListAxis.push_back(Vec3d(0.4, 0, 0));  // 1
    LineMesh::LineArray vertLine;
    vertLine[0] = 0 ;
    vertLine[1] =  1 ;
    std::vector<LineMesh::LineArray> vecVertLine;
    vecVertLine.push_back(vertLine);
    axisX->setLinesVertices(vecVertLine);
    axisX->setInitialVertexPositions(vertListAxis);
    axisX->setVertexPositions(vertListAxis);
    axisX->initialize(vertListAxis, vecVertLine);
    auto axisXModel = std::make_shared<VisualModel>(axisX);
    axisXModel->setRenderMaterial(materialLineRed);
    auto axisXLine = std::make_shared<imstk::VisualObject>("axisX");
    axisXLine->addVisualModel(axisXModel);
    scene->addSceneObject(axisXLine);
    std::vector<Color> colors;
    colors.push_back(imstk::Color::Red);
    colors.push_back(imstk::Color::Red);
    axisX->setVertexColors(colors);
   
    auto sphereY = std::make_shared<imstk::Sphere>();
    sphereY->setTranslation(Vec3d(0, 0.4, 0));
    sphereY->setRadius(0.01);
    auto sphereYModel = std::make_shared<VisualModel>(sphereY);
    sphereYModel->setRenderMaterial(materialGreen);
    auto sphereYYY = std::make_shared<SceneObject>("SphereY");
    sphereYYY->addVisualModel(sphereYModel);
    scene->addSceneObject(sphereYYY);
    vertListAxis.clear();
    std::shared_ptr<LineMesh> axisY = std::make_shared<LineMesh>();
    vertListAxis.push_back(Vec3d(0, 0, 0)); // 0
    vertListAxis.push_back(Vec3d(0, 0.4, 0));  // 1
    axisY->setLinesVertices(vecVertLine);
    axisY->setInitialVertexPositions(vertListAxis);
    axisY->setVertexPositions(vertListAxis);
    axisY->initialize(vertListAxis, vecVertLine);
    auto axisYModel = std::make_shared<VisualModel>(axisY);
    axisYModel->setRenderMaterial(materialLineGreen);
    auto axisYLine = std::make_shared<imstk::VisualObject>("axisY");
    axisYLine->addVisualModel(axisYModel);
    scene->addSceneObject(axisYLine);
    colors.clear();
    colors.push_back(imstk::Color::Green);
    colors.push_back(imstk::Color::Green);
    axisY->setVertexColors(colors);

    auto sphereZ = std::make_shared<imstk::Sphere>();
    sphereZ->setTranslation(Vec3d(0, 0, 0.4));
    sphereZ->setRadius(0.01);
    auto sphereZModel = std::make_shared<VisualModel>(sphereZ);
    sphereZModel->setRenderMaterial(materialBlue);
    auto sphereZZZ = std::make_shared<SceneObject>("SphereZ");
    sphereZZZ->addVisualModel(sphereZModel);
    scene->addSceneObject(sphereZZZ);
    vertListAxis.clear();
    std::shared_ptr<LineMesh> axisZ = std::make_shared<LineMesh>();
    vertListAxis.push_back(Vec3d(0, 0, 0)); // 0
    vertListAxis.push_back(Vec3d(0, 0, 0.4));  // 1
    axisZ->setLinesVertices(vecVertLine);
    axisZ->setInitialVertexPositions(vertListAxis);
    axisZ->setVertexPositions(vertListAxis);
    axisZ->initialize(vertListAxis, vecVertLine);
    auto axisZModel = std::make_shared<VisualModel>(axisZ);
    axisZModel->setRenderMaterial(materialLineBlue);
    auto axisZLine = std::make_shared<imstk::VisualObject>("axisZ");
    axisZLine->addVisualModel(axisZModel);
    scene->addSceneObject(axisZLine);
    colors.clear();
    colors.push_back(imstk::Color::Blue);
    colors.push_back(imstk::Color::Blue);
    axisZ->setVertexColors(colors);

    auto visualSphere11 = std::make_shared<imstk::Sphere>();
    visualSphere11->setTranslation(Vec3d(-14.4, -1.4, 7.9)*scalexzh);
    visualSphere11->setRadius(0.0005);
    auto visualSphere11Model = std::make_shared<VisualModel>(visualSphere11);
    visualSphere11Model->setRenderMaterial(materialGreen); // (materialSphere0);
    auto sphere1Obj = std::make_shared<CollidingObject>("Sphere1");
    sphere1Obj->setCollidingGeometry(visualSphere11);
    sphere1Obj->addVisualModel(visualSphere11Model);
    scene->addSceneObject(sphere1Obj);  ////< XZH KNIFE TIP ROOT

    auto visualSphere22 = std::make_shared<imstk::Sphere>();
    visualSphere22->setTranslation(Vec3d(-7.8, -1.2, 7.9));
    visualSphere22->setRadius(0.0005);
    Vec3d mm2 = visualSphere22->getPosition();
    auto sphere2Obj = std::make_shared<CollidingObject>("Sphere2");
    auto visualSphere22Model = std::make_shared<VisualModel>(visualSphere22);
    visualSphere22Model->setRenderMaterial(materialRed); // (materialSphere0);
    sphere2Obj->setCollidingGeometry(visualSphere22);
    sphere2Obj->addVisualModel(visualSphere22Model);
    scene->addSceneObject(sphere2Obj);  ////< XZH KNIFE TIP

    ////< XZH draw tool
    auto visualSphere11d = std::make_shared<imstk::Sphere>();
    visualSphere11d->setTranslation(Vec3d(-14.4, -1.4, 7.9)*scalexzh);
    visualSphere11d->setRadius(0.0025);
    auto sphere1dObj = std::make_shared<CollidingObject>("Sphere1d");
    auto visualSphere11dModel = std::make_shared<VisualModel>(visualSphere11d);
    visualSphere11dModel->setRenderMaterial(materialGreen); // (materialSphere0);
    sphere1dObj->setCollidingGeometry(visualSphere11d);
    sphere1dObj->addVisualModel(visualSphere11dModel);
    //scene->addSceneObject(sphere1dObj);  ////< XZH KNIFE TIP

    auto visualSphere22d = std::make_shared<imstk::Sphere>();
    visualSphere22d->setTranslation(Vec3d(-7.8, -1.2, 7.9));
    visualSphere22d->setRadius(0.001); //  (0.0025);
    auto sphere2dObj = std::make_shared<CollidingObject>("Sphere2d");
    auto visualSphere22dModel = std::make_shared<VisualModel>(visualSphere22d);
    visualSphere22dModel->setRenderMaterial(materialRed); // (materialSphere0);
    sphere2dObj->setCollidingGeometry(visualSphere22d);
    sphere2dObj->addVisualModel(visualSphere22dModel);
    //scene->addSceneObject(sphere2dObj);  ////< XZH KNIFE TIP

    ////< XZH for tool draw position

    auto probe = std::make_shared<IBLProbe>(DATA_ROOT_PATH "/VESS/texture/organIrradiance.dds",
        DATA_ROOT_PATH "/VESS/texture/organRadiance.dds",
        iMSTK_DATA_ROOT "/IBL/roomBRDF.png");
    scene->setGlobalIBLProbe(probe);

    Vec3d move = Vec3d(0, -5, 15);  // Vec3d(0, 35, 15);
    // Light (white)
    auto whiteLight = std::make_shared<imstk::SpotLight>("whiteLight");
    //whiteLight->setPosition(imstk::Vec3d(-2.011496, 3, 3.486862) + move); //(imstk::Vec3d(-2.111103, 5.906062, -24.198272) + move);
    whiteLight->setPosition(Vec3d(0.025, .092, 0.235));  // whiteLight->setPosition(Vec3d(2.5, 9.2, 23.5)); // 5.4)); // (imstk::Vec3d(6.871600, 13, 10.749434) + move); // (imstk::Vec3d(-2.111103, 5.906062, -24.198272) + move);
    whiteLight->setIntensity(0.005);//810;
    //whiteLight->setCastsShadow(false);
    scene->addLight(whiteLight);
    auto lightSphere = apiutils::createVisualAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "lightSphere", 1.0, Vec3d(0.025, .092, 0.235));
    lightSphere->getVisualGeometry()->setTranslation(whiteLight->getPosition());

    // Update Camera
    auto cam1 = scene->getCamera();
    cam1->setPosition(Vec3d(0.0258, 0.1502, 0.2136)); // (Vec3d(0.031, 0.0677, 0.233)); //  (Vec3d(3.4, 10.0, 10.3)); // (Vec3d(0.0, 57.0, 48)); //  (imstk::Vec3d(-12.5, -7, -33)); // -25, -8, -4.5
    cam1->setFocalPoint(Vec3d(0.025, 0.0028, 0.215)); // (Vec3d(0.067, 0.2287, 0.096)); // (Vec3d(3.0, -31.6, -26.1)); // (Vec3d(0.0, -30, -28));
    cam1->setFieldOfView(75); //  (75);
    cam1->setViewUp(0, 1, 0);
    ////< XZH Step 1 END

      ////< XZH Step2 deformable mesh
    std::string pathColon = DATA_ROOT_PATH"/VESS/Nick/ColonCube.obj";   ////< XZH cubeTet cubeTest tumorcolon7_scale2.obj
    auto objMesh = imstk::MeshIO::read(pathColon);
    auto visualColonMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh);
    visualColonMesh->computeBoundingBox(min1, max1);
    //visualColonMesh->scale(0.01, imstk::Geometry::TransformType::ApplyToData);
    visualColonMesh->rotate(imstk::UP_VECTOR, imstk::PI, imstk::Geometry::TransformType::ApplyToData);
    //visualColonMesh->translate(0, 0, 0.1, imstk::Geometry::TransformType::ApplyToData);
    visualColonMesh->computeBoundingBox(min1, max1);
    auto posVisualList = visualColonMesh->getVertexPositions();
    auto triLists = visualColonMesh->getTrianglesVertices();
    visualColonMesh->setVertexPositionsXZH(posVisualList);
    visualColonMesh->setTrianglesVerticesXZH(triLists);

    auto material = std::make_shared<RenderMaterial>();
    auto diffuseTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/Nick/diffuse.png", Texture::Type::DIFFUSE);
    auto normalTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/Nick/normal.png", Texture::Type::NORMAL);
    auto roughnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/Nick/roughness.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    //auto sssTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_sss2.png", Texture::Type::SUBSURFACE_SCATTERING);
    auto sssTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_sss.png", Texture::Type::SUBSURFACE_SCATTERING);
    material->addTexture(diffuseTexture);
    material->addTexture(normalTexture);
    material->addTexture(roughnessTexture);
    material->addTexture(sssTexture);
    material->setBackFaceCulling(false);
    material->setDisplayMode(RenderMaterial::DisplayMode::SURFACE);
    ////material->setTexture(metalnessTexture);
    //material->addTexture(sssTexture);
    //material->setTessellated(true);

    auto materialWire = std::make_shared<RenderMaterial>();
    auto diffuseTextureWire = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/Nick/diffuse2.png", Texture::Type::DIFFUSE);
    auto normalTextureWire = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/Nick/normal.png", Texture::Type::NORMAL);
    auto roughnessTextureWire = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/Nick/roughness.png", Texture::Type::ROUGHNESS);
    auto metalnessTextureWire = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    auto sssTextureWire = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_sss.png", Texture::Type::SUBSURFACE_SCATTERING);
    materialWire->addTexture(diffuseTextureWire);
    materialWire->addTexture(normalTextureWire);
    materialWire->addTexture(roughnessTextureWire);
    materialWire->addTexture(sssTextureWire);
    //materialWire->setTessellated(true);
    //material->setBackFaceCulling(false);
    materialWire->setDisplayMode(RenderMaterial::DisplayMode::SURFACE);

    auto visualColonMeshModel = std::make_shared<VisualModel>(visualColonMesh);
    visualColonMeshModel->setRenderMaterial(material);
    //visualCubeMesh->setRenderMaterial(material);

    auto objectColonV = std::make_shared<imstk::VisualObject>("visualColonMesh0");
    objectColonV->addVisualModel(visualColonMeshModel); // change to any mesh created above
    //scene->addSceneObject(objectColonV);

    ////< XZH TET    // Tetrahedral mesh
    std::vector<Vec3d> centralLine2;
    centralLine2.push_back(Vec3d(2.4, 1.7, 7.1));   ////< XZH  (Vec3d(-2.5, 7, -23));
    centralLine2.push_back(Vec3d(3.4, 11.8, 7.1));   ////< XZH  (Vec3d(-3.5, 15, -23));
    auto materialTet = std::make_shared<RenderMaterial>();
    //auto diffuseTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    materialTet->setColor(imstk::Color::Blue);
    materialTet->setDisplayMode(RenderMaterial::DisplayMode::SURFACE);

    imstk::StdVectorOfVec3d vertListCutTet;
    std::vector<imstk::TetrahedralMesh::TetraArray> cutTetConnectivity;
    std::vector<physXVertTetLink> liksVolSurCut;
    auto cutMesh = std::make_shared<imstk::VESSTetCutMesh>();
    //cutMesh->setVisualMeshXZH(visualColonMesh);
    cutMesh->loadTetXZH(DATA_ROOT_PATH"/VESS/Nick/ColonCube.tet", DATA_ROOT_PATH"/VESS/Nick/ColonCube.fc", DATA_ROOT_PATH"/VESS/Nick/ColonCube.fca", DATA_ROOT_PATH"/VESS/Nick/ColonCube.eg", DATA_ROOT_PATH"/VESS/Nick/ColonCube.ega", DATA_ROOT_PATH"/VESS/Nick/ColonCube.nd", vertListCutTet, cutTetConnectivity, liksVolSurCut);   ////< XZH cubeTet cubeTest
    cutMesh->computeBoundingBox(min1, max1);
    //cutMesh->scale(0.01, imstk::Geometry::TransformType::ApplyToData);
    cutMesh->rotate(imstk::UP_VECTOR, imstk::PI, imstk::Geometry::TransformType::ApplyToData);
    cutMesh->rotate(imstk::RIGHT_VECTOR, imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);
    cutMesh->updateVertexPositions();
    cutMesh->computeBoundingBox(min1, max1);

    ////< XZH test visual mesh extracted from the tet mesh
    cutMesh->extractSurfaceMeshVisual();
    auto visualColonMeshTet = std::dynamic_pointer_cast<imstk::SurfaceMesh>(cutMesh->getVisualSurface());
    //visualColonMeshTet->rotate(imstk::UP_VECTOR, imstk::PI, imstk::Geometry::TransformType::ApplyToData);
    auto visualColonMeshTetModel = std::make_shared<VisualModel>(visualColonMeshTet);
    visualColonMeshTetModel->setRenderMaterial(materialWire);
    auto posVisualListTet = visualColonMeshTet->getVertexPositions();
    auto triListsTet = visualColonMeshTet->getTrianglesVertices();
    visualColonMeshTet->setVertexPositionsXZH(posVisualListTet);
    visualColonMeshTet->setTrianglesVerticesXZH(triListsTet);

    auto objectColon = std::make_shared<imstk::VisualObject>("visualColonMesh");
    objectColon->addVisualModel(visualColonMeshTetModel); // change to any mesh created above
    //scene->addSceneObject(objectColon);

    cutMesh->setVisualMeshXZH(visualColonMeshTet);
    cutMesh->setVisualMeshTexture(visualColonMesh);
    cutMesh->calcTex2TetSurfTriLink();   ////< XZH the above two link

    cutMesh->extractSurfaceMeshBoundary(centralLine2);
    cutMesh->extractSurfaceMeshVolume();
    cutMesh->mappingLinks();
    cutMesh->mappingTriTetLinks();
    cutMesh->mappingTetTriLinks();

    cutMesh->initializeCuttingMesh();
    cutMesh->initializeCuttingPara();

    ////< XZH for injection
    auto tetPosListXZH = cutMesh->getVertexPositions();
    cutMesh->setInitialPhysVertex(tetPosListXZH);

    ////< XZH for collision temp,might use the above way.
    std::vector<Vec3d> centralLine;
    //centralLine.push_back(Vec3d(2.4, 1.7, 7.1)*0.04);
    //centralLine.push_back(Vec3d(2.4, 11.8, 7.1)*0.04);
    centralLine.push_back(Vec3d(0.02706308, 0.154157, 0.2207738));
    centralLine.push_back(Vec3d(0.02706308, 0.08378759, 0.2207738));
    cutMesh->extractSurfaceMesh21(centralLine); // not using central line

    auto spherecentralLine1 = std::make_shared<imstk::Sphere>();
    spherecentralLine1->setTranslation(centralLine[0]);
    spherecentralLine1->setRadius(0.006);
    auto spherecentralLine1Model = std::make_shared<VisualModel>(spherecentralLine1);
    spherecentralLine1Model->setRenderMaterial(materialGreen);
    auto spherecentralLine11Obj = std::make_shared<SceneObject>("spherecentralLine1");
    spherecentralLine11Obj->addVisualModel(spherecentralLine1Model);
    scene->addSceneObject(spherecentralLine11Obj);

    auto spherecentralLine2 = std::make_shared<imstk::Sphere>();
    spherecentralLine2->setTranslation(centralLine[1]);
    spherecentralLine2->setRadius(0.006);
    auto spherecentralLine2Model = std::make_shared<VisualModel>(spherecentralLine2);
    spherecentralLine2Model->setRenderMaterial(materialGreen);
    auto spherecentralLine22Obj = std::make_shared<SceneObject>("spherecentralLine2");
    spherecentralLine22Obj->addVisualModel(spherecentralLine2Model);
    scene->addSceneObject(spherecentralLine22Obj);

    ////< XZH 
    auto& statesCut = cutMesh->getTetrahedronStates();
    statesCut.clear();
    auto& mapsCut = cutMesh->getTetrahedronMap();
    mapsCut.clear();
    for (int i = 0; i < cutTetConnectivity.size(); i++)
    {
        statesCut.push_back(1); // exist
        mapsCut.push_back(1); // original
    }

    ////< XZH test triangle
    std::shared_ptr<SurfaceMesh> surfMeshCentralLine = std::make_shared<SurfaceMesh>();
    
    StdVectorOfVec3d vertListLine;
    vertListLine.push_back(Vec3d(2.4, 1.7, 7.1)); // 0
    vertListLine.push_back(Vec3d(3.4, 11.8, 7.1));  // 1
    vertListLine.push_back(Vec3d(2.6, 1.7, 7.1)); // 2
    SurfaceMesh::TriangleArray triLine[2];
    triLine[0] = { { 0, 1, 2 } };
    triLine[1] = { { 0, 2, 1 } };
    std::vector<SurfaceMesh::TriangleArray> trianglesLine;
    trianglesLine.push_back(triLine[0]);
    trianglesLine.push_back(triLine[1]);
    surfMeshCentralLine->setTrianglesVertices(trianglesLine);
    surfMeshCentralLine->setInitialVertexPositions(vertListLine);
    surfMeshCentralLine->setVertexPositions(vertListLine);
    surfMeshCentralLine->initialize(vertListLine, trianglesLine);
    auto surfMeshCentralLineModel = std::make_shared<VisualModel>(surfMeshCentralLine);
    surfMeshCentralLineModel->setRenderMaterial(materialLineRed);
    auto objectLine = std::make_shared<imstk::VisualObject>("surfMeshCentralLine");
    objectLine->addVisualModel(surfMeshCentralLineModel);
    scene->addSceneObject(objectLine);

    ////< XZH 
    // Construct a map
    auto colonMapP2V = std::make_shared<imstk::OneToOneMapVESS>();
    colonMapP2V->setMaster(cutMesh);
    colonMapP2V->setSlave(visualColonMeshTet);
    colonMapP2V->computeNewXZH();

    auto colonMapP2C = std::make_shared<imstk::OneToOneMapVESS>();
    colonMapP2C->setMaster(cutMesh);
    colonMapP2C->setSlave(cutMesh);
    colonMapP2C->compute();

    auto colonObj = std::make_shared<PbdObject>("Colon");
    colonObj->setCollidingGeometry(cutMesh);
    colonObj->addVisualModel(visualColonMeshTetModel);
    colonObj->setPhysicsGeometry(cutMesh);
    colonObj->setPhysicsToCollidingMap(colonMapP2C);
    colonObj->setPhysicsToVisualMap(colonMapP2V);
    //colonObj->setCollidingToVisualMap(colonMapC2V);

    //fixed point
    Vec3d posTumorCenter = Vec3d(0.027785, 0.087666, 0.209199); // Vec3d(0.032, 0.090416, 0.193864); // Vec3d(0.029985, 0.092161, 0.197533); // Vec3d(3.5, 8.9, 19.0)*scalexzh;
    auto& posTetlist2 = cutMesh->getVertexPositions();
    std::string fixed_corner2;
    double distTumor = 0.02; //  7.0*scalexzh;
    char intStr2[33];
    Vec3d tmpPos2;
    std::vector<int> fixedVertice2; // add later 06/04/2017
    std::vector<int> fixedVerticeStates2; // fixed states
    auto colSphereObj = std::make_shared<SceneObject>("CollidingSphere");
    for (int i = 0; i < cutMesh->getNumVertices(); ++i) // physicsColonMesh  visualColonMesh
    {
        bool flag = true;
        if (posTetlist2[i].y()<0) flag = false;
        if (!flag)
        {
            std::sprintf(intStr2, "%d", i + 1);
            fixed_corner2 += std::string(intStr2) + ' ';
            fixedVertice2.push_back(i); //  all vertices
            fixedVerticeStates2.push_back(1);

            ////< XZH draw sphere
            //char name[256];
            //sprintf(name, "CollidingSphere%d", i);
            auto colSphere = std::make_shared<imstk::Sphere>();
            colSphere->setTranslation(posTetlist2[i]);
            colSphere->setRadius(0.003);
            auto colSphereModel = std::make_shared<VisualModel>(colSphere);
            colSphereModel->setRenderMaterial(materialGreen);
            colSphereObj->addVisualModel(colSphereModel);
        }
        else
        {
            fixedVerticeStates2.push_back(0);
        }
    }
    scene->addSceneObject(colSphereObj);
    
    ////< XZH test the vertex color
    for (int i = 0; i < posVisualListTet.size(); i++)
    {
        bool flag = true;
        if (flag)
        {
            visualColonMeshTet->setVertexColor(i, imstk::Color(0.2, 0.5, 0.5, 1));
        }
    }

    // set fixed in the tetmesh add later 06/04/2017
    cutMesh->setFixedVertice(fixedVertice2);
    cutMesh->updateFixedVertice();
    cutMesh->initDeformMesh(); // init for deform mesh : injection solution 06/20/2017

    ////< XZH test surface of colliding
    auto& physVertices0 = cutMesh->getPhysVertex();
    auto& physEdge0 = cutMesh->getPhysEdge();
    auto& physSTris0 = cutMesh->getPhysSurfTris();
    auto surfaceMeshColliding = std::make_shared<imstk::SurfaceMesh>();
    StdVectorOfVec3d vertPosesColliding;
    std::vector<imstk::SurfaceMesh::TriangleArray> surfaceTriColliding;
    for (int i = 0; i < physVertices0.size(); i++)
    {
        vertPosesColliding.push_back(physVertices0[i].pos);
    }
    for (int i = 0; i < physSTris0.size(); i++)
    {
		size_t ni0 = physSTris0[i].nodeIdx[0];
		size_t ni1 = physSTris0[i].nodeIdx[1];
		size_t ni2 = physSTris0[i].nodeIdx[2];
        surfaceTriColliding.push_back(imstk::SurfaceMesh::TriangleArray{ { ni0, ni1, ni2 } });
    }
    surfaceMeshColliding->initialize(vertPosesColliding, surfaceTriColliding);
    auto surfaceMeshCollidingModel = std::make_shared<VisualModel>(surfaceMeshColliding);
    surfaceMeshCollidingModel->setRenderMaterial(material);
    auto surfaceCollidingObj = std::make_shared<SceneObject>("surfaceCollidingObj");
    surfaceCollidingObj->addVisualModel(surfaceMeshCollidingModel);
    //scene->addSceneObject(surfaceCollidingObj);

    auto pbdColonModel = std::make_shared<PbdModel>();
    pbdColonModel->setModelGeometry(cutMesh);
    pbdColonModel->configure(/*Number of Constraints*/2,
        /*Constraint configuration*/"Volume 0.005",  // 0.7 "Dihedral 0.8", // 0.01 0.8 effect is couhe not very well
        /*Constraint configuration*/"Distance 0.02",  // 0.9 effect is couhe
        /*Mass*/0.1, // 0.1
        /*Gravity*/"0 0 0.98", // 98",  // "0 -9.8 0",
        /*TimeStep*/0.004, // 0.004, // 0.001
        /*FixedPoint*/ fixed_corner2.c_str(), // "1 2", //   ////< XZH  fixed_corner2.c_str(),
        /*NumberOfIterationInConstraintSolver*/3, //   2 12
        /*Proximity*/0.002, // 0.01,  // 0.1
        /*Contact stiffness*/0.1);
    colonObj->setDynamicalModel(pbdColonModel);

    auto pbdColonSolver = std::make_shared<PbdSolver>();
    pbdColonSolver->setPbdObject(colonObj);
    scene->addNonlinearSolver(pbdColonSolver);

    scene->addSceneObject(colonObj);

    // Collisions
    auto colGraph = scene->getCollisionGraph();
    //auto pair = std::make_shared<PbdInteractionPair>(PbdInteractionPair(object, colonObj));
    auto pair = std::make_shared<PbdInteractionPair>(PbdInteractionPair(object11, object12, object, colonObj));
    pair->setNumberOfInterations(3); // 2

    colGraph->addInteractionPair(pair);
      ////< XZH Step2 END

    ////< XZH Step3: Run
    sdk->setActiveScene(scene);
    //  ////< XZH key switch
    auto viewer = std::dynamic_pointer_cast<VulkanViewer>(sdk->getViewer());
    viewer->setResolution(1000, 920);
    viewer->enableLensDistortion(0.05);
    //viewer->setResolution(1920, 1080);   ////< XZH 1920*1080
    //viewer->enableFullscreen();
    sdk->startSimulation(SimulationStatus::INACTIVE);
#endif
}