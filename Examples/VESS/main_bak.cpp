//<<<<<<< HEAD
//=======
/*
* DO NOT COMMIT CHANGES TO THE LINE BELOW !!!
* Keep your change local. You can use `git gui`
* to stage or unstage part of a file diff.
*/
#define DATA_ROOT_PATH "I:/iMSTK/resources"

//>>>>>>> CameraNavigation
#include <cstring>
#include <iostream>
#include <memory>
#include <thread>
#include <iomanip>

#include "imstkMath.h"
#include "imstkTimer.h"
#include "imstkSimulationManager.h"

// Objects
#include "imstkForceModelConfig.h"
#include "imstkFEMDeformableBodyModel.h"
#include "imstkDeformableObject.h"
#include "imstkSceneObject.h"
#include "imstkLight.h"
#include "imstkCamera.h"
#include "imstkPBDVirtualCouplingObject.h"
#include "imstkPBDSceneObjectController.h" // xzh

// Time Integrators
#include "imstkBackwardEuler.h"

// Solvers
#include "imstkNonlinearSystem.h"
#include "imstkNewtonSolver.h"
#include "imstkConjugateGradient.h"
#include "imstkPbdSolver.h"

// Geometry
#include "imstkPlane.h"
#include "imstkSphere.h"
#include "imstkCube.h"
#include "imstkTetrahedralMesh.h"
#include "imstkSurfaceMesh.h"
#include "imstkMeshIO.h"
#include "imstkLineMesh.h"

// Maps
#include "imstkTetraTriangleMap.h"
#include "imstkIsometricMap.h"
#include "imstkOneToOneMap.h"

// Devices
#include "imstkHDAPIDeviceClient.h"
#include "imstkHDAPIDeviceServer.h"
#include "imstkVRPNDeviceClient.h"
#include "imstkVRPNDeviceServer.h"
#include "imstkCameraController.h"
#include "imstkSceneObjectController.h"
#include "imstkLaparoscopicToolController.h"

// Collisions
#include "imstkInteractionPair.h"

// logger
#include "g3log/g3log.hpp"

// imstk utilities
#include "imstkPlotterUtils.h"
#include "imstkAPIUtilities.h"

#include "imstkVirtualCouplingPBDObject.h"
#include "imstkPbdObject.h"

// testVTKTexture
#include <vtkOBJReader.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <string>
#include <vtkJPEGReader.h>

// VTK shader xzh
#include "vtkOpenGLPolyDataMapper.h"
#include "vtkPolyDataNormals.h"
#include "vtkMapper.h"
#include "vtkProperty.h"

// xzh
#include "imstkPBDVirtualCouplingObject.h"
#include "imstkVESSBasic.h"
#include "imstkVESSTetrahedralMesh.h"
#include "imstkVESSTetraTriangleMap.h"
// cutting
#include "imstkCuttingVESSTetMesh.h"
#include "imstkCuttingVESSCutMesh.h"

using namespace imstk;

void testVTKVESS();
// test distance 
#include <math.h>
#include <iostream>
void testDistance();
void testVESSPBD();
void testVESS();
void testLineMeshVESS();
void testVESSPBDCD();
void testLineMesh();
void testPBDLineMesh();
void testController();
//void testPBDVirtualCoupling();
void testPBDObj();
void testPBDVCController();
void testPbdVirtualCoupling();
void testPbdVirtualCoupling2();
void testPbdVirtualCoupling3();
void testPbdVirtualCoupling4();
void testPbdVirtualCoupling5();
void testPbdVirtualCoupling6();
void testPbdVisutralCouplingObject();
void testPbdVisutralCouplingObject2();
void testVTKVESSViewer();
void testVolumeMeshCutting(); // cutting

void testViewer();
void testPbdVirtualCoupling7();
void testPbdVirtualCoupling13();
void testPbdVirtualCoupling15();
void testPbdVirtualCoupling16();
void testPbdVirtualCoupling17();
void testPbdVirtualCoupling18();
void testPbdVirtualCoupling19();

void testPbdVirtualCoupling20(); // colon new
void testPbdVirtualCoupling21(); // colon new merged with tumor
void testPbdVirtualCoupling21_2(); // viewer
void testPbdVirtualCoupling22(); // colon with injection knife for marking and injection

void testPbdVirtualCoupling16_2();

void testNewModel();

// Cutting
void testCutting();
void testCuttingLess();
void testCutting2();
void testCuttingNotComplete();
void testCuttingNotComplete2();
void testCuttingNotComplete3();
void testCuttingCube();
void testCuttingCubeDense();
void testHapticPlane();
void testCuttingCubeDenseHaptic();
void testCuttingCubeDenseHapticMap();
// Cutting END

// test map
void testTetraTriangleMap();
// end map

int main()
{
	std::cout << "****************\n"
		<< "Starting Sandbox\n"
		<< "****************\n";

	/*------------------
	Test rendering
	------------------*/
	//testMultiTextures();
	//testVTKTexture();
	//testMultiObjectWithTextures();
	//testViewer();


	/*------------------
	Test CD and CR
	------------------*/
	//testMeshCCD();
	//testPenaltyRigidCollision();


	/*------------------
	Test geometry, maps
	------------------*/
	//testIsometricMap();
	//testTetraTriangleMap();
	//testExtractSurfaceMesh();
	//testOneToOneNodalMap();
	//testSurfaceMeshOptimizer();
	//testAnalyticalGeometry();


	/*------------------
	Test physics
	------------------*/
	//testPbdVolume();
	//testPbdCloth();
	//testPbdCollision();
	//testDeformableBody();


	/*------------------
	Test mesh I/O
	------------------*/
	//testLineMesh();
	//testMshAndVegaIO();
	//testReadMesh();


	/*------------------
	Test devices, controllers
	------------------*/
	//testObjectController();
	//testTwoFalcons();
	//testCameraController();
	//testTwoOmnis();
	//testLapToolController();


	/*------------------
	Test Misc.
	------------------*/
	//testScenesManagement();
	//testVectorPlotters();

	//testVTKVESS();
	//testDistance();
	//testVESSPBD();
	//testVESS();
	//testLineMeshVESS();
	//testVESSPBDCD();
	//testLineMesh();
	//testPBDLineMesh();
	//testController();
	//testPBDVirtualCoupling();
	//testPBDObj();
	//testPBDVCController();
	//testPbdVirtualCoupling(); 
	//testPbdVirtualCoupling2();
	//testPbdVirtualCoupling3();
	//testPbdVirtualCoupling4();
	//testPbdVirtualCoupling5();
	//testPbdVirtualCoupling6(); // currently use
    //testVTKVESSViewer(); // camera navigation // 
	//testPbdVisutralCouplingObject();
	//testPbdVisutralCouplingObject2();

    //testVolumeMeshCutting();

    //testViewer();
    //testPbdVirtualCoupling7();
    //testPbdVirtualCoupling13();
    //testPbdVirtualCoupling15();
    //testPbdVirtualCoupling16(); // for colon
    //testPbdVirtualCoupling16_2(); // for colon   Nick test
    //testPbdVirtualCoupling17(); // for rect test with 25 vertices
    //testPbdVirtualCoupling18();  // for rect test with 4 vertices
    //testPbdVirtualCoupling19();  // for new colon 06/20/2017
    //testPbdVirtualCoupling20();  // for new colon more dense 07/18/2017
    //testPbdVirtualCoupling21();  //  new merged colon and tumor for new colon more dense 07/18/2017
    testPbdVirtualCoupling21_2(); // for viewer
    //testPbdVirtualCoupling22();

    /** Cutting***/
    //testCuttingCube();
    //testCuttingCubeDense();
    //testHapticPlane();
    //testCuttingCubeDenseHaptic();
    //testCuttingCubeDenseHapticMap();

    //testNewModel();
    
    // map
    //testTetraTriangleMap();

    //testCutting();
    //testCutting2();
    //testCuttingNotComplete();
    //testCuttingNotComplete2();
    //testCuttingNotComplete3();
    //testCuttingLess();
    /** Cutting END*/
	return 0;
}

void testTetraTriangleMap()
{
    auto sdk = std::make_shared<imstk::SimulationManager>();

    //// Tetrahedral mesh
    imstk::StdVectorOfVec3d vertListTet;
    std::vector<imstk::TetrahedralMesh::TetraArray> tetConnectivity;

    //physXLink linkVolSur;
    std::vector<physXLink> linksVolSur;
    bool flag = loadTetFile(DATA_ROOT_PATH"/VESS/cube1003_SB.tet", vertListTet, tetConnectivity, linksVolSur); // colon_tumor3_3_SB for test   colon_tumor3_SB  cubeTest2_SB
    auto cutTissueMesh = std::make_shared<imstk::CuttingVESSCutMesh>();
    cutTissueMesh->setInitialVertexPositions(vertListTet);
    cutTissueMesh->setVertexPositions(vertListTet);
    cutTissueMesh->setTetrahedraVertices(tetConnectivity);
    cutTissueMesh->setPhysXLink(linksVolSur);
    cutTissueMesh->setInitialPhysVertex(vertListTet);  // add later 06/04/2017

    // cube visual
    std::string pathColon = DATA_ROOT_PATH"/VESS/cube1003_SB.obj";
    auto objMesh = imstk::MeshIO::read(pathColon);
    auto visualColonMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh);

    // Construct a map
    auto tetTriMap = std::make_shared<imstk::TetraTriangleMap>();
    tetTriMap->setMaster(cutTissueMesh);
    tetTriMap->setSlave(visualColonMesh);
    tetTriMap->compute();

    tetTriMap->print();

    auto& m_verticesEnclosingTetraId = tetTriMap->getTetraID();
    auto& m_verticesWeights = tetTriMap->getVertWeights();

    FILE *outMap = fopen("i:/outTetTriMap.dat", "w");
    // Print vertices and weight info
    LOG(INFO) << "Vertex (<vertNum>): Tetrahedra: <TetNum> - Weights: (w1, w2, w3, w4)\n";
    for (size_t vertexId = 0; vertexId < m_verticesEnclosingTetraId.size(); ++vertexId)
    {
        LOG(INFO) << "Vertex (" << vertexId << "):"
            << "\tTetrahedra: " << m_verticesEnclosingTetraId.at(vertexId)
            << " - Weights: " << "("
            << m_verticesWeights.at(vertexId)[0] << ", "
            << m_verticesWeights.at(vertexId)[1] << ", "
            << m_verticesWeights.at(vertexId)[2] << ", "
            << m_verticesWeights.at(vertexId)[3] << ")";
        fprintf(outMap, "l %d %f %f %f\n", m_verticesEnclosingTetraId.at(vertexId), m_verticesWeights.at(vertexId)[0], m_verticesWeights.at(vertexId)[1], m_verticesWeights.at(vertexId)[2]);
    }
    fclose(outMap);

    getchar();
}

void testCuttingCubeDenseHapticMap()
{
#ifdef iMSTK_USE_OPENHAPTICS
    // SDK and Scene
    auto sdk = std::make_shared<imstk::SimulationManager>();
    auto scene = sdk->createNewScene("SceneTestDevice");

    // Device Client
    auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

    // Device Server
    auto server = std::make_shared<imstk::HDAPIDeviceServer>();
    server->addDeviceClient(client);
    sdk->addModule(server);

    std::string path2obj = DATA_ROOT_PATH"/VESS/ITKnife7_5.obj"; // knife correct ITKnife6 // cube knife knife3.obj";  // //  113.obj"; //  /  blade2.obj  cube.obj  kcube.obj  test cloth using this 
    std::string path2obj_c = DATA_ROOT_PATH"/VESS/ITKnife7_5.obj"; // knife correct ITKnife6  // cube knife knife3.obj";  //  //  113.obj";  // / VESS / TriangleTipKnife2.obj";  // blade2.obj  cube.obj kcube_s2.obj
    auto readMesh = imstk::MeshIO::read(path2obj_c); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
    auto visualMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(readMesh);

    Vec3d min1, max1;
    visualMesh->computeBoundingBox(min1, max1);

    Vec3d x0, x1;
    x0.x() = min1.x();
    x0.y() = (min1.y() + max1.y())*0.5;
    x0.z() = (min1.z() + max1.z())*0.5;
    x1.x() = max1.x();
    x1.y() = (min1.y() + max1.y())*0.5;
    x1.z() = (min1.z() + max1.z())*0.5;

    // Line mesh
    auto lineMesh = std::make_shared<imstk::LineMesh>();
    imstk::StdVectorOfVec3d vertList;
    vertList.resize(2);  // using 5 segment
    //vertList[0] = imstk::Vec3d(-13, -2.25, 9.15); // should - volume mesh therold than surface mesh (9.0, -1.9, -0.6) AABB 0: 9.0, -2.23, -0.6   -0.7 ITKnife6.DAE  // knife3.obj 9.5, -0.9, 1.18
    //vertList[1] = imstk::Vec3d(-5.93, -1.825, 9.125); // 9.7 ITKnife6.DAE // knife3.obj 9.5, -0.9, 8.73
    //vertList[2] = imstk::Vec3d(1.14, -1.4, 9.1); // (9.0, -2.55, 9.5)  AABB 1: 9.0, -2.23, 9.6  9.7 ITKnife6.DAE // knife3.obj 9.5, -0.9, 8.73
    vertList[0] = x0; // imstk::Vec3d(-16.4, -1.85, 9.15);
    vertList[1] = x1; //  imstk::Vec3d(-9.3, -1.4, 9.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    std::vector<std::vector<int> > connectivity;
    for (int i = 0; i < 1;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity.push_back(line);
    }

    lineMesh->setInitialVertexPositions(vertList);
    lineMesh->setVertexPositions(vertList);
    lineMesh->setConnectivity(connectivity);

    // using for line mesh display
    auto lineSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertListLine;
    vertListLine.resize(3);
    vertListLine[0] = x0; // imstk::Vec3d(-16.4, -1.85, 9.15);
    vertListLine[1] = x1; // imstk::Vec3d(-9.3, -1.4, 9.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertListLine[2] = x0; // imstk::Vec3d(-16.4, -1.85, 9.15);
    std::vector<std::vector<int> > connectivityLine;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivityLine.push_back(line);
    }

    lineSurfMesh->setInitialVertexPositions(vertListLine);
    lineSurfMesh->setVertexPositions(vertListLine);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> trianglesLine;
    imstk::SurfaceMesh::TriangleArray triLine[1];
    triLine[0] = { { 0, 1, 2 } };
    trianglesLine.push_back(triLine[0]);
    lineSurfMesh->setTrianglesVertices(trianglesLine);
    auto objectLine = std::make_shared<imstk::VisualObject>("visualLineMesh");
    auto materialLine = std::make_shared<RenderMaterial>();
    materialLine->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpLine = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialLine->addTexture(tmpLine);
    lineSurfMesh->setRenderMaterial(materialLine);
    objectLine->setVisualGeometry(lineSurfMesh); // change to any mesh created above
    scene->addSceneObject(objectLine);
    // XZH END

    auto physicsMesh = lineMesh; //visualMesh; //  lineMesh; //  imstk::MeshIO::read(path2obj_c);  //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
    auto collidingMesh = lineMesh; // visualMesh; //  lineMesh;

    auto material0 = std::make_shared<RenderMaterial>();
    auto diffuseTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto normalTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Normal.png", Texture::Type::NORMAL);
    auto roughnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Roughness.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material0->addTexture(diffuseTexture0);
    material0->addTexture(normalTexture0);
    material0->addTexture(roughnessTexture0);
    //material->setTexture(metalnessTexture);
    visualMesh->setRenderMaterial(material0);

    FILE *outpointtri;
    outpointtri = fopen("i:/AABB.txt", "w");
    fprintf(outpointtri, "V: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    collidingMesh->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "C: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    fclose(outpointtri);

    auto object = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject");

    auto objMapP2V = std::make_shared<imstk::OneToOneMap>();  // std::make_shared<imstk::TriangleLineMap>(); //  std::make_shared<imstk::OneToOneMap>();
    objMapP2V->setMaster(physicsMesh);
    objMapP2V->setSlave(visualMesh);
    objMapP2V->compute();


    auto objMapP2C = std::make_shared<imstk::OneToOneMap>();  //  colliding is physical mesh
    objMapP2C->setMaster(physicsMesh);
    objMapP2C->setSlave(collidingMesh);
    objMapP2C->compute();

    auto objMapC2V = std::make_shared<imstk::OneToOneMap>(); //  std::make_shared<imstk::TriangleLineMap>(); //  s std::make_shared<imstk::OneToOneMap>();
    objMapC2V->setMaster(collidingMesh);
    objMapC2V->setSlave(visualMesh);
    objMapC2V->compute();

    auto objMapC2P = std::make_shared<imstk::OneToOneMap>(); // colliding is physical mesh
    objMapC2P->setMaster(collidingMesh);
    objMapC2P->setSlave(physicsMesh);
    objMapC2P->compute();

    object->setVisualGeometry(visualMesh);
    object->setCollidingGeometry(collidingMesh);
    object->setPhysicsGeometry(physicsMesh);
    object->setPhysicsToCollidingMap(objMapP2C);
    object->setPhysicsToVisualMap(objMapP2V);
    object->setCollidingToVisualMap(objMapC2V);
    object->setColldingToPhysicsMap(objMapC2P);

    auto pbdModel = std::make_shared<PbdModel>();
    pbdModel->setModelGeometry(physicsMesh);
    pbdModel->configure(/*Number of Constraints*/1,
        /*Constraint configuration*/"Distance 0.1",
        /*Mass*/0.0,
        /*Gravity*/"0 0 0",
        /*TimeStep*/0.0003,
        /*FixedPoint*/"",
        /*NumberOfIterationInConstraintSolver*/2, // 5
        /*Proximity*/0.3,  // use linemesh 0.3  // 0.01 use  //  cube 0.1
        /*Contact stiffness*/0.001);
    object->setDynamicalModel(pbdModel);

    //auto pbdSolver = std::make_shared<PbdSolver>();
    //pbdSolver->setPbdObject(object);
    //scene->addNonlinearSolver(pbdSolver);

    scene->addSceneObject(object);

    auto trackCtrl = std::make_shared<imstk::DeviceTracker>(client);
    trackCtrl->setTranslationScaling(0.5);  // 0.25 0.1  test 0.1 should use  Scale 0.001 will be better 
    auto controller = std::make_shared<imstk::PBDSceneObjectController>(object, trackCtrl);
    scene->addObjectController(controller);

    // cube
    //// Tetrahedral mesh
    imstk::StdVectorOfVec3d vertListTet;
    std::vector<imstk::TetrahedralMesh::TetraArray> tetConnectivity;
    //vector< vec3d > vertices;
    //vector< vec4u32 > elements;

    //physXLink linkVolSur;
    std::vector<physXLink> linksVolSur;
    bool flag = loadTetFile(DATA_ROOT_PATH"/VESS/cube1003_SB_m.tet", vertListTet, tetConnectivity, linksVolSur); // colon_tumor3_3_SB for test   colon_tumor3_SB  cubeTest2_SB
    auto cutTissueMesh = std::make_shared<imstk::CuttingVESSCutMesh>();
    cutTissueMesh->setInitialVertexPositions(vertListTet);
    cutTissueMesh->setVertexPositions(vertListTet);
    cutTissueMesh->setTetrahedraVertices(tetConnectivity);
    cutTissueMesh->setPhysXLink(linksVolSur);
    cutTissueMesh->setInitialPhysVertex(vertListTet);  // add later 06/04/2017

    cutTissueMesh->initializeCuttingMesh();
    cutTissueMesh->initializeCutting();
    cutTissueMesh->extractSurfaceMesh();
    cutTissueMesh->extractSurfaceMeshVolume();
    //auto cutTissueMesh = std::make_shared<imstk::CuttingVESSCutMesh>(tetMesh);
    cutTissueMesh->setFlagSplitMeshAfterCut(true);

    //Vec3d min1, max1;
    cutTissueMesh->computeBoundingBox(min1, max1);

    // c. Extract the surface mesh
    auto surfMesh = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMesh = cutTissueMesh->getSurfaceMesh();
    surfMesh->computeVertexNormals();
    auto objectVisual = std::make_shared<imstk::VisualObject>("visualColonMeshVisualNew");
    auto material = std::make_shared<RenderMaterial>();
    material->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME); // WIREFRAME SURFACE
    auto tmp = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    material->addTexture(tmp);
    surfMesh->setRenderMaterial(material);
    objectVisual->setVisualGeometry(surfMesh); // change to any mesh created above
    //scene->addSceneObject(objectVisual);

    // cube visual
    std::string pathColon = DATA_ROOT_PATH"/VESS/cube1003_SB.obj";
    auto objMesh = imstk::MeshIO::read(pathColon);
    auto visualColonMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh);
    auto collidingColonMesh = cutTissueMesh;

    auto materialVisualNew = std::make_shared<RenderMaterial>();
    materialVisualNew->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME); // WIREFRAME SURFACE
    auto tmpVisualNew = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    materialVisualNew->addTexture(tmpVisualNew);
    visualColonMesh->setRenderMaterial(materialVisualNew);
    visualColonMesh->computeBoundingBox(min1, max1);

    //XZH
    FILE *outMesh = fopen("i:/outOBJ_cube1003.dat", "w");
    //visualColonMesh->getInitialVerticesPositions();
    fprintf(outMesh, "num vertices:%d\n", visualColonMesh->getNumVertices());
    Vec3d tempPos;
    for (int i = 0; i < visualColonMesh->getNumVertices(); ++i)
    {
        tempPos = visualColonMesh->getVertexPosition(i);
        fprintf(outMesh, "%f %f %f\n", tempPos.x(), tempPos.y(), tempPos.z());
    }
    fprintf(outMesh, "\n num tri:%d\n", visualColonMesh->getNumTriangles());
    for (size_t triId = 0; triId < visualColonMesh->getNumTriangles(); ++triId)
    {
        Vec3d center(0, 0, 0);
        auto vert = visualColonMesh->getTrianglesVertices().at(triId); // getTetrahedronVertices(triId);
        fprintf(outMesh, "%d %d %d\n", vert[0], vert[1], vert[2]);
        for (size_t i = 0; i < 3; ++i)
        {
            center = visualColonMesh->getInitialVertexPosition(vert[i]);
            //fprintf(outMesh, "i: %d %f %f %f  ", i, center.x(), center.y(), center.z());
        }
        //fprintf(outMesh, "\n");
    }

    fclose(outMesh);
    // XZH END

    // Construct a map
    auto colonMapP2V = std::make_shared<imstk::TetraTriangleMap>();  // <imstk::OneToOneMap>();
    colonMapP2V->setMaster(cutTissueMesh);
    colonMapP2V->setSlave(visualColonMesh);
    colonMapP2V->compute();


    auto colonMapP2C = std::make_shared<imstk::OneToOneMap>();
    colonMapP2C->setMaster(cutTissueMesh);
    colonMapP2C->setSlave(collidingColonMesh);
    colonMapP2C->compute();

    auto colonMapC2V = std::make_shared<imstk::TetraTriangleMap>();  //<imstk::OneToOneMap>();
    colonMapC2V->setMaster(collidingColonMesh);
    colonMapC2V->setSlave(visualColonMesh);
    colonMapC2V->compute();

    auto colonObj = std::make_shared<PbdObject>("Cube");

    colonObj->setCollidingGeometry(collidingColonMesh);
    colonObj->setVisualGeometry(visualColonMesh);
    colonObj->setPhysicsGeometry(cutTissueMesh);
    colonObj->setPhysicsToCollidingMap(colonMapP2C);
    colonObj->setPhysicsToVisualMap(colonMapP2V);
    colonObj->setCollidingToVisualMap(colonMapC2V);

    auto pbdColonModel = std::make_shared<PbdModel>();
    pbdColonModel->setModelGeometry(cutTissueMesh);
    pbdColonModel->configure(/*Number of Constraints*/2,
        /*Constraint configuration*/"Volume 0.70",  // 0.7 "Dihedral 0.8", // 0.01 0.8 effect is couhe not very well
        /*Constraint configuration*/"Distance 0.90",  // 0.9 effect is couhe
        /*Mass*/0.1, // 0.1
        /*Gravity*/"0 0 0",  // "0 -9.8 0",
        /*TimeStep*/0.001, // 0.001
        /*FixedPoint*/"", //  fixed_corner.c_str()
        /*NumberOfIterationInConstraintSolver*/3, //   2 12
        /*Proximity*/0.01,  // 0.1
        /*Contact stiffness*/0.1);
    colonObj->setDynamicalModel(pbdColonModel);

    scene->addSceneObject(colonObj);

    // Collisions
    auto colGraph = scene->getCollisionGraph();
    auto pair = std::make_shared<PbdInteractionPair>(PbdInteractionPair(object, colonObj));
    pair->setNumberOfInterations(3); // 2
    colGraph->addInteractionPair(pair);


    // Sphere1
    auto sphere1Obj = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "Sphere1", 0.5, x0); // -2.23
    sphere1Obj->getVisualGeometry()->setTranslation(x0);
    auto visualSphere22 = std::make_shared<imstk::Sphere>();
    visualSphere22->setTranslation(x1);
    visualSphere22->setRadius(0.5);
    Vec3d mm2 = visualSphere22->getPosition();
    auto sphere2Obj = std::make_shared<CollidingObject>("Sphere2");
    sphere2Obj->setCollidingGeometry(visualSphere22);
    sphere2Obj->setVisualGeometry(visualSphere22);
    scene->addSceneObject(sphere2Obj);

    // Light (white)
    auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
    whiteLight->setPosition(imstk::Vec3d(-10, 20, -50));
    //whiteLight->setFocalPoint(imstk::Vec3d(1, -1, 0));
    whiteLight->setIntensity(10000);
    scene->addLight(whiteLight);
    auto whiteLight2 = std::make_shared<imstk::PointLight>("whiteLight2");
    whiteLight2->setPosition(imstk::Vec3d(-10, 20, 40));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight2->setIntensity(10000);
    scene->addLight(whiteLight2);
    auto whiteLight3 = std::make_shared<imstk::PointLight>("whiteLight3");
    whiteLight3->setPosition(imstk::Vec3d(-60, 20, -5));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight3->setIntensity(10000);
    scene->addLight(whiteLight3);
    auto whiteLight4 = std::make_shared<imstk::PointLight>("whiteLight4");
    whiteLight4->setPosition(imstk::Vec3d(35, 20, -5));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight4->setIntensity(10000);
    scene->addLight(whiteLight4);
    auto whiteLight5 = std::make_shared<imstk::PointLight>("whiteLight5");
    whiteLight5->setPosition(imstk::Vec3d(-10, -25, -5));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight5->setIntensity(10000);
    scene->addLight(whiteLight5);
    auto whiteLight6 = std::make_shared<imstk::PointLight>("whiteLight6");
    whiteLight6->setPosition(imstk::Vec3d(-10, 65, -5));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight6->setIntensity(10000);
    scene->addLight(whiteLight6);

    // Light (red)
    auto colorLight = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight->setPosition(imstk::Vec3d(-10, 20, 50)); // - 25, -8, -4.5
    colorLight->setFocalPoint(imstk::Vec3d(-10, 20, -25)); // - 3.7, 5.5, 7
    colorLight->setColor(imstk::Color::White); // red
    colorLight->setSpotAngle(70);
    colorLight->setIntensity(10000);
    auto colorLight2 = std::make_shared<imstk::SpotLight>("colorLight2");
    colorLight2->setPosition(imstk::Vec3d(-10, 20, -60));
    colorLight2->setFocalPoint(imstk::Vec3d(-10, 20, 15));
    colorLight2->setColor(imstk::Color::White);
    colorLight2->setSpotAngle(70);
    colorLight2->setIntensity(10000);
    auto colorLight3 = std::make_shared<imstk::SpotLight>("colorLight3");
    colorLight3->setPosition(imstk::Vec3d(-2.5, 7, -23));  //-25, -5, -2.5
    colorLight3->setFocalPoint(imstk::Vec3d(-2.5, 17, -23));  // -3.7, 5.5, 7
    colorLight3->setColor(imstk::Color::White); // red
    colorLight3->setSpotAngle(150);
    colorLight3->setIntensity(2000);
    scene->addLight(colorLight);
    scene->addLight(colorLight2);
    //sceneTest->addLight(colorLight3);

    // Update Camera
    auto cam1 = scene->getCamera();
    cam1->setPosition(imstk::Vec3d(-12.5, -7, -33)); // -25, -8, -4.5
    cam1->setFocalPoint(imstk::Vec3d(2.5, 17, -13)); // -3.7, 5.5, 7
    cam1->setViewAngle(75);

    // Run
    sdk->setActiveScene(scene);
    sdk->startSimulation(true);
#endif
}

void testCuttingCubeDenseHaptic()
{
#ifdef iMSTK_USE_OPENHAPTICS
    // SDK and Scene
    auto sdk = std::make_shared<imstk::SimulationManager>();
    auto scene = sdk->createNewScene("SceneTestDevice");

    // Device Client
    auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

    // Device Server
    auto server = std::make_shared<imstk::HDAPIDeviceServer>();
    server->addDeviceClient(client);
    sdk->addModule(server);

    std::string path2obj = DATA_ROOT_PATH"/VESS/ITKnife7_5.obj"; // knife correct ITKnife6 // cube knife knife3.obj";  // //  113.obj"; //  /  blade2.obj  cube.obj  kcube.obj  test cloth using this 
    std::string path2obj_c = DATA_ROOT_PATH"/VESS/ITKnife7_5.obj"; // knife correct ITKnife6  // cube knife knife3.obj";  //  //  113.obj";  // / VESS / TriangleTipKnife2.obj";  // blade2.obj  cube.obj kcube_s2.obj
    auto readMesh = imstk::MeshIO::read(path2obj_c); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
    auto visualMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(readMesh);
    
    Vec3d min1, max1;
    visualMesh->computeBoundingBox(min1, max1);

    Vec3d x0, x1;
    x0.x() = min1.x();
    x0.y() = (min1.y() + max1.y())*0.5;
    x0.z() = (min1.z() + max1.z())*0.5;
    x1.x() = max1.x();
    x1.y() = (min1.y() + max1.y())*0.5;
    x1.z() = (min1.z() + max1.z())*0.5;

    // Line mesh
    auto lineMesh = std::make_shared<imstk::LineMesh>();
    imstk::StdVectorOfVec3d vertList;
    vertList.resize(2);  // using 5 segment
    //vertList[0] = imstk::Vec3d(-13, -2.25, 9.15); // should - volume mesh therold than surface mesh (9.0, -1.9, -0.6) AABB 0: 9.0, -2.23, -0.6   -0.7 ITKnife6.DAE  // knife3.obj 9.5, -0.9, 1.18
    //vertList[1] = imstk::Vec3d(-5.93, -1.825, 9.125); // 9.7 ITKnife6.DAE // knife3.obj 9.5, -0.9, 8.73
    //vertList[2] = imstk::Vec3d(1.14, -1.4, 9.1); // (9.0, -2.55, 9.5)  AABB 1: 9.0, -2.23, 9.6  9.7 ITKnife6.DAE // knife3.obj 9.5, -0.9, 8.73
    vertList[0] = x0; // imstk::Vec3d(-16.4, -1.85, 9.15);
    vertList[1] = x1; //  imstk::Vec3d(-9.3, -1.4, 9.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    std::vector<std::vector<int> > connectivity;
    for (int i = 0; i < 1;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity.push_back(line);
    }

    lineMesh->setInitialVertexPositions(vertList);
    lineMesh->setVertexPositions(vertList);
    lineMesh->setConnectivity(connectivity);

    // using for line mesh display
    auto lineSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertListLine;
    vertListLine.resize(3);
    vertListLine[0] = x0; // imstk::Vec3d(-16.4, -1.85, 9.15);
    vertListLine[1] = x1; // imstk::Vec3d(-9.3, -1.4, 9.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertListLine[2] = x0; // imstk::Vec3d(-16.4, -1.85, 9.15);
    std::vector<std::vector<int> > connectivityLine;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivityLine.push_back(line);
    }

    lineSurfMesh->setInitialVertexPositions(vertListLine);
    lineSurfMesh->setVertexPositions(vertListLine);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> trianglesLine;
    imstk::SurfaceMesh::TriangleArray triLine[1];
    triLine[0] = { { 0, 1, 2 } };
    trianglesLine.push_back(triLine[0]);
    lineSurfMesh->setTrianglesVertices(trianglesLine);
    auto objectLine = std::make_shared<imstk::VisualObject>("visualLineMesh");
    auto materialLine = std::make_shared<RenderMaterial>();
    materialLine->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpLine = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialLine->addTexture(tmpLine);
    lineSurfMesh->setRenderMaterial(materialLine);
    objectLine->setVisualGeometry(lineSurfMesh); // change to any mesh created above
    scene->addSceneObject(objectLine);
    // XZH END

    auto physicsMesh = lineMesh; //visualMesh; //  lineMesh; //  imstk::MeshIO::read(path2obj_c);  //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
    auto collidingMesh = lineMesh; // visualMesh; //  lineMesh;

    auto material0 = std::make_shared<RenderMaterial>();
    auto diffuseTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto normalTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Normal.png", Texture::Type::NORMAL);
    auto roughnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Roughness.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material0->addTexture(diffuseTexture0);
    material0->addTexture(normalTexture0);
    material0->addTexture(roughnessTexture0);
    //material->setTexture(metalnessTexture);
    visualMesh->setRenderMaterial(material0);

    FILE *outpointtri;
    outpointtri = fopen("i:/AABB.txt", "w");
    fprintf(outpointtri, "V: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    collidingMesh->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "C: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    fclose(outpointtri);

    auto object = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject");

    auto objMapP2V = std::make_shared<imstk::OneToOneMap>();  // std::make_shared<imstk::TriangleLineMap>(); //  std::make_shared<imstk::OneToOneMap>();
    objMapP2V->setMaster(physicsMesh);
    objMapP2V->setSlave(visualMesh);
    objMapP2V->compute();


    auto objMapP2C = std::make_shared<imstk::OneToOneMap>();  //  colliding is physical mesh
    objMapP2C->setMaster(physicsMesh);
    objMapP2C->setSlave(collidingMesh);
    objMapP2C->compute();

    auto objMapC2V = std::make_shared<imstk::OneToOneMap>(); //  std::make_shared<imstk::TriangleLineMap>(); //  s std::make_shared<imstk::OneToOneMap>();
    objMapC2V->setMaster(collidingMesh);
    objMapC2V->setSlave(visualMesh);
    objMapC2V->compute();

    auto objMapC2P = std::make_shared<imstk::OneToOneMap>(); // colliding is physical mesh
    objMapC2P->setMaster(collidingMesh);
    objMapC2P->setSlave(physicsMesh);
    objMapC2P->compute();

    object->setVisualGeometry(visualMesh);
    object->setCollidingGeometry(collidingMesh);
    object->setPhysicsGeometry(physicsMesh);
    object->setPhysicsToCollidingMap(objMapP2C);
    object->setPhysicsToVisualMap(objMapP2V);
    object->setCollidingToVisualMap(objMapC2V);
    object->setColldingToPhysicsMap(objMapC2P);

    auto pbdModel = std::make_shared<PbdModel>();
    pbdModel->setModelGeometry(physicsMesh);
    pbdModel->configure(/*Number of Constraints*/1,
        /*Constraint configuration*/"Distance 0.1",
        /*Mass*/0.0,
        /*Gravity*/"0 0 0",
        /*TimeStep*/0.0003,
        /*FixedPoint*/"",
        /*NumberOfIterationInConstraintSolver*/2, // 5
        /*Proximity*/0.3,  // use linemesh 0.3  // 0.01 use  //  cube 0.1
        /*Contact stiffness*/0.001);
    object->setDynamicalModel(pbdModel);

    //auto pbdSolver = std::make_shared<PbdSolver>();
    //pbdSolver->setPbdObject(object);
    //scene->addNonlinearSolver(pbdSolver);

    scene->addSceneObject(object);

    auto trackCtrl = std::make_shared<imstk::DeviceTracker>(client);
    trackCtrl->setTranslationScaling(0.5);  // 0.25 0.1  test 0.1 should use  Scale 0.001 will be better 
    auto controller = std::make_shared<imstk::PBDSceneObjectController>(object, trackCtrl);
    scene->addObjectController(controller);

    // cube
    //// Tetrahedral mesh
    imstk::StdVectorOfVec3d vertListTet;
    std::vector<imstk::TetrahedralMesh::TetraArray> tetConnectivity;
    //vector< vec3d > vertices;
    //vector< vec4u32 > elements;

    //physXLink linkVolSur;
    std::vector<physXLink> linksVolSur;
    bool flag = loadTetFile(DATA_ROOT_PATH"/VESS/cube1003_SB.tet", vertListTet, tetConnectivity, linksVolSur); // colon_tumor3_3_SB for test   colon_tumor3_SB  cubeTest2_SB
    auto cutTissueMesh = std::make_shared<imstk::CuttingVESSCutMesh>();
    cutTissueMesh->setInitialVertexPositions(vertListTet);
    cutTissueMesh->setVertexPositions(vertListTet);
    cutTissueMesh->setTetrahedraVertices(tetConnectivity);
    cutTissueMesh->setPhysXLink(linksVolSur);
    cutTissueMesh->setInitialPhysVertex(vertListTet);  // add later 06/04/2017

    cutTissueMesh->initializeCuttingMesh();
    cutTissueMesh->initializeCutting();
    cutTissueMesh->extractSurfaceMesh();
    cutTissueMesh->extractSurfaceMeshVolume();
    //auto cutTissueMesh = std::make_shared<imstk::CuttingVESSCutMesh>(tetMesh);
    cutTissueMesh->setFlagSplitMeshAfterCut(true);

    //Vec3d min1, max1;
    cutTissueMesh->computeBoundingBox(min1, max1);

    // c. Extract the surface mesh
    auto surfMesh = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMesh = cutTissueMesh->getSurfaceMesh();
    surfMesh->computeVertexNormals();
    auto objectVisual = std::make_shared<imstk::VisualObject>("visualColonMeshVisualNew");
    auto material = std::make_shared<RenderMaterial>();
    material->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME); // WIREFRAME SURFACE
    auto tmp = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    material->addTexture(tmp);
    surfMesh->setRenderMaterial(material);
    objectVisual->setVisualGeometry(surfMesh); // change to any mesh created above
    //scene->addSceneObject(objectVisual);

    // cube visual
    std::string pathColon = DATA_ROOT_PATH"/VESS/cube1003_SB.obj";
    auto objMesh = imstk::MeshIO::read(pathColon);
    auto visualColonMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh);
    auto collidingColonMesh = cutTissueMesh;

    auto materialVisualNew = std::make_shared<RenderMaterial>();
    materialVisualNew->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME SURFACE
    auto tmpVisualNew = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    materialVisualNew->addTexture(tmpVisualNew);
    visualColonMesh->setRenderMaterial(materialVisualNew);
    visualColonMesh->computeBoundingBox(min1, max1); // min -34.25 -3.0 -24.56  max 6.55  40.0  15.74

    //XZH
    FILE *outMesh = fopen("i:/outOBJ_cube1003.dat", "w");
    //visualColonMesh->getInitialVerticesPositions();
    fprintf(outMesh, "num vertices:%d\n", visualColonMesh->getNumVertices());
    Vec3d tempPos;
    for (int i = 0; i < visualColonMesh->getNumVertices(); ++i)
    {
        tempPos = visualColonMesh->getVertexPosition(i);
        fprintf(outMesh, "%f %f %f\n", tempPos.x(), tempPos.y(), tempPos.z());
    }
    fprintf(outMesh, "\n num tri:%d\n", visualColonMesh->getNumTriangles());
    for (size_t triId = 0; triId < visualColonMesh->getNumTriangles(); ++triId)
    {
        Vec3d center(0, 0, 0);
        auto vert = visualColonMesh->getTrianglesVertices().at(triId); // getTetrahedronVertices(triId);
        fprintf(outMesh, "%d %d %d\n", vert[0], vert[1], vert[2]);
        for (size_t i = 0; i < 3; ++i)
        {
            center = visualColonMesh->getInitialVertexPosition(vert[i]);
            //fprintf(outMesh, "i: %d %f %f %f  ", i, center.x(), center.y(), center.z());
        }
        //fprintf(outMesh, "\n");
    }

    fclose(outMesh);
    // XZH END

    //fixed point
    std::string fixed_corner;
    char intStr[500];
    Vec3d tmpPos;
    for (int i = 0; i < cutTissueMesh->getNumVertices(); ++i) // physicsColonMesh  visualColonMesh
    {
        tmpPos = cutTissueMesh->getVertexPosition(i);   
        if ((tmpPos.z() >15.0))  
        {
            std::sprintf(intStr, "%d", i + 1);
            fixed_corner += std::string(intStr) + ' ';
        }
    }

    // Construct a map
    auto colonMapP2V = std::make_shared<imstk::VESSTetraTriangleMap>();  // <imstk::OneToOneMap>();
    colonMapP2V->setMaster(cutTissueMesh);
    colonMapP2V->setSlave(visualColonMesh);
    colonMapP2V->compute();


    auto colonMapP2C = std::make_shared<imstk::OneToOneMap>();
    colonMapP2C->setMaster(cutTissueMesh);
    colonMapP2C->setSlave(collidingColonMesh);
    colonMapP2C->compute();

    auto colonMapC2V = std::make_shared<imstk::VESSTetraTriangleMap>();  //<imstk::OneToOneMap>();
    colonMapC2V->setMaster(collidingColonMesh);
    colonMapC2V->setSlave(visualColonMesh);
    colonMapC2V->compute();

    auto colonObj = std::make_shared<PbdObject>("Cube");

    colonObj->setCollidingGeometry(collidingColonMesh);
    colonObj->setVisualGeometry(visualColonMesh);
    colonObj->setPhysicsGeometry(cutTissueMesh);
    colonObj->setPhysicsToCollidingMap(colonMapP2C);
    colonObj->setPhysicsToVisualMap(colonMapP2V);
    colonObj->setCollidingToVisualMap(colonMapC2V);

    auto pbdColonModel = std::make_shared<PbdModel>();
    pbdColonModel->setModelGeometry(cutTissueMesh);
    pbdColonModel->configure(/*Number of Constraints*/2,
        /*Constraint configuration*/"Volume 0.5",  // 0.7 "Dihedral 0.8", // 0.01 0.8 effect is couhe not very well
        /*Constraint configuration*/"Distance 0.9",  // 0.9 effect is couhe
        /*Mass*/0.1, // 0.1
        /*Gravity*/"0 0 -980",  // "0 -9.8 0",
        /*TimeStep*/0.01, // 0.001
        /*FixedPoint*/ fixed_corner.c_str(),  // "", //  fixed_corner.c_str()
        /*NumberOfIterationInConstraintSolver*/2, //  3  2 12
        /*Proximity*/0.01,  // 0.1
        /*Contact stiffness*/0.1);
    colonObj->setDynamicalModel(pbdColonModel);

    auto pbdColonSolver = std::make_shared<PbdSolver>();
    pbdColonSolver->setPbdObject(colonObj);
    scene->addNonlinearSolver(pbdColonSolver);

    scene->addSceneObject(colonObj);

    // Collisions
    auto colGraph = scene->getCollisionGraph();
    auto pair = std::make_shared<PbdInteractionPair>(PbdInteractionPair(object, colonObj));
    pair->setNumberOfInterations(3); // 2
    colGraph->addInteractionPair(pair);


    // Sphere1
    auto sphere1Obj = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "Sphere1", 0.5, x0); // -2.23
    sphere1Obj->getVisualGeometry()->setTranslation(x0);
    auto visualSphere22 = std::make_shared<imstk::Sphere>();
    visualSphere22->setTranslation(x1);
    visualSphere22->setRadius(1.0);
    Vec3d mm2 = visualSphere22->getPosition();
    auto sphere2Obj = std::make_shared<CollidingObject>("Sphere2");
    sphere2Obj->setCollidingGeometry(visualSphere22);
    sphere2Obj->setVisualGeometry(visualSphere22);
    scene->addSceneObject(sphere2Obj);

    // Light (white)
    auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
    whiteLight->setPosition(imstk::Vec3d(-10, 20, -50));
    //whiteLight->setFocalPoint(imstk::Vec3d(1, -1, 0));
    whiteLight->setIntensity(10000);
    scene->addLight(whiteLight);
    auto whiteLight2 = std::make_shared<imstk::PointLight>("whiteLight2");
    whiteLight2->setPosition(imstk::Vec3d(-10, 20, 40));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight2->setIntensity(10000);
    scene->addLight(whiteLight2);
    auto whiteLight3 = std::make_shared<imstk::PointLight>("whiteLight3");
    whiteLight3->setPosition(imstk::Vec3d(-60, 20, -5));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight3->setIntensity(10000);
    scene->addLight(whiteLight3);
    auto whiteLight4 = std::make_shared<imstk::PointLight>("whiteLight4");
    whiteLight4->setPosition(imstk::Vec3d(35, 20, -5));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight4->setIntensity(10000);
    scene->addLight(whiteLight4);
    auto whiteLight5 = std::make_shared<imstk::PointLight>("whiteLight5");
    whiteLight5->setPosition(imstk::Vec3d(-10, -25, -5));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight5->setIntensity(10000);
    scene->addLight(whiteLight5);
    auto whiteLight6 = std::make_shared<imstk::PointLight>("whiteLight6");
    whiteLight6->setPosition(imstk::Vec3d(-10, 65, -5));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight6->setIntensity(10000);
    scene->addLight(whiteLight6);

    // Light (red)
    auto colorLight = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight->setPosition(imstk::Vec3d(-10, 20, 50)); // - 25, -8, -4.5
    colorLight->setFocalPoint(imstk::Vec3d(-10, 20, -25)); // - 3.7, 5.5, 7
    colorLight->setColor(imstk::Color::White); // red
    colorLight->setSpotAngle(70);
    colorLight->setIntensity(10000);
    auto colorLight2 = std::make_shared<imstk::SpotLight>("colorLight2");
    colorLight2->setPosition(imstk::Vec3d(-10, 20, -60));
    colorLight2->setFocalPoint(imstk::Vec3d(-10, 20, 15));
    colorLight2->setColor(imstk::Color::White);
    colorLight2->setSpotAngle(70);
    colorLight2->setIntensity(10000);
    auto colorLight3 = std::make_shared<imstk::SpotLight>("colorLight3");
    colorLight3->setPosition(imstk::Vec3d(-2.5, 7, -23));  //-25, -5, -2.5
    colorLight3->setFocalPoint(imstk::Vec3d(-2.5, 17, -23));  // -3.7, 5.5, 7
    colorLight3->setColor(imstk::Color::White); // red
    colorLight3->setSpotAngle(150);
    colorLight3->setIntensity(2000);
    scene->addLight(colorLight);
    scene->addLight(colorLight2);
    //sceneTest->addLight(colorLight3);

    auto sphereOrg = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereOrg", 0.2, Vec3d(0, 0, 0));
    sphereOrg->getVisualGeometry()->setTranslation(Vec3d(0, 0, 0));

    auto sphereX = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereX", 0.5, Vec3d(10, 0, 0));
    sphereX->getVisualGeometry()->setTranslation(Vec3d(10, 0, 0));

    auto sphereY = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereY", 3.8, Vec3d(0, 10, 0));
    sphereY->getVisualGeometry()->setTranslation(Vec3d(0, 10, 0));

    auto sphereZ = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereZ", 1.0, Vec3d(0, 0, 10));
    sphereZ->getVisualGeometry()->setTranslation(Vec3d(0, 0, 10));
    //scene->addSceneObject(sphereOrg);
    //scene->addSceneObject(sphereX);
    //scene->addSceneObject(sphereY);
    //scene->addSceneObject(sphereZ);

    // Update Camera
    auto cam1 = scene->getCamera();
    cam1->setPosition(Vec3d(-83.048929, -57.310934, 36.955925)); //  (imstk::Vec3d(-12.5, -7, -33)); // -25, -8, -4.5
    cam1->setFocalPoint(Vec3d(-7.010332, 27.817278, -8.666576)); //  (imstk::Vec3d(2.5, 17, -13)); // -3.7, 5.5, 7
    cam1->setViewAngle(75); //  (75);

    auto surfMeshVolume = std::make_shared<imstk::SurfaceMesh>();
    auto surfMesh2 = std::make_shared<imstk::SurfaceMesh>();
    cutTissueMesh->extractSurfaceMesh();
    surfMeshVolume = cutTissueMesh->getSurfaceMesh(); //->getSurfaceMeshVolume();
    surfMeshVolume->computeVertexNormals();
    auto objectVolume = std::make_shared<imstk::VisualObject>("visualColonMeshVolume");
    auto materialVolume = std::make_shared<RenderMaterial>();
    materialVolume->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME); // WIREFRAME SURFACE
    auto tmpVolume = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    materialVolume->addTexture(tmpVolume);
    surfMeshVolume->setRenderMaterial(materialVolume);
    objectVolume->setVisualGeometry(surfMeshVolume); // change to any mesh created above
    scene->addSceneObject(objectVolume);
////TEST START
//    // Decal Test
//    auto decalTest2 = std::make_shared<DecalPool>();
//
//    auto decalMaterial2 = std::make_shared<RenderMaterial>();
//    auto diffuseTexture22 = std::make_shared<Texture>("I:/iMSTK/resources/textures/test4.png");
//    decalMaterial2->addTexture(diffuseTexture22);
//
//    auto decal12 = decalTest2->addDecal();
//    decal12->setPosition(20.5, 30, 20);
//    decal12->rotate(RIGHT_VECTOR, PI_4);
//    //auto decal2 = decalTest->addDecal();
//    //decal2->setPosition(-2,0,0);
//    decalTest2->setRenderMaterial(decalMaterial2);
//
//    auto decalObject2 = std::make_shared<VisualObject>("DecalObject2");
//    decalObject2->setVisualGeometry(decalTest2);
//    scene->addSceneObject(decalObject2);
////TEST END
    // decal system
    // Decal Test
    auto decalTest = std::make_shared<DecalPool>(1);
    auto decalMaterial = std::make_shared<RenderMaterial>();
    //auto diffuseTexture = std::make_shared<Texture>("F:/assets/miscBloodDecalParticles/miscBloodDecalParticles/blood_particle_01.png", Texture::DIFFUSE);
    auto diffuseTexture2 = std::make_shared<Texture>("I:/iMSTK/resources/textures/test4.png", Texture::DIFFUSE);
    decalMaterial->addTexture(diffuseTexture2);
    decalTest->setRecycle(true);
    auto decal1 = decalTest->addDecal();
    //decal1->setPosition(Vec3d(20.5, 30, 20));
    //decal1->rotate(FORWARD_VECTOR, PI_4);
    //decal1->scale(10.0);
    auto decal2 = decalTest->addDecal();
    decal2->setPosition(Vec3d(20.5, 30, 20));
    decal2->rotate(RIGHT_VECTOR, PI_4);
    //decal2->scale(10.0);
    decalTest->setRenderMaterial(decalMaterial);

    auto decalObject = std::make_shared<VisualObject>("DecalObject");
    decalObject->setVisualGeometry(decalTest);
    scene->addSceneObject(decalObject);

    auto sphereObj = apiutils::createVisualAnalyticalSceneObject(
        Geometry::Type::Sphere, scene, "VisualSphere1", 1, Vec3d(70, 50, 50));
    // Sphere
    auto sphereObj2 = apiutils::createVisualAnalyticalSceneObject(
        Geometry::Type::Sphere, scene, "VisualSphere2", 1, Vec3d(20, 30, 20));
    sphereObj2->getVisualGeometry()->setTranslation(Vec3d(20, 30, 20));
    //visualSphere2222->setRadius(0.5);
    auto sphereMaterial222 = std::make_shared<RenderMaterial>();
    sphereMaterial222->setDiffuseColor(Color::Red);
    sphereObj2->getVisualGeometry()->setRenderMaterial(sphereMaterial222);

    //// Sphere
    //auto sphereObj = apiutils::createVisualAnalyticalSceneObject(
    //    Geometry::Type::Sphere, scene, "VisualSphere1", 1, Vec3d(70,50,50));
    //// Sphere
    //auto sphereObj2 = apiutils::createVisualAnalyticalSceneObject(
    //    Geometry::Type::Sphere, scene, "VisualSphere2", 1, Vec3d(70, 80, 50));
    //sphereObj2->getVisualGeometry()->setTranslation(Vec3d(0, 0, 10));
    //auto sphereMaterial = std::make_shared<RenderMaterial>();
    //sphereMaterial->setDiffuseColor(Color::Blue);
    //sphereObj2->getVisualGeometry()->setRenderMaterial(sphereMaterial);
    //scene->addSceneObject(sphereObj);
    //scene->addSceneObject(sphereObj2);
    // Run
    sdk->setActiveScene(scene);
    sdk->startSimulation(true);
#endif
}

void testHapticPlane()
{
#ifdef iMSTK_USE_OPENHAPTICS
    // SDK and Scene
    auto sdk = std::make_shared<imstk::SimulationManager>();
    auto scene = sdk->createNewScene("SceneTestDevice");

    // Device Client
    auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

    // Device Server
    auto server = std::make_shared<imstk::HDAPIDeviceServer>();
    server->addDeviceClient(client);
    sdk->addModule(server);

    std::string path2obj = DATA_ROOT_PATH"/VESS/ITKnife7_4.obj"; // knife correct ITKnife6 // cube knife knife3.obj";  // //  113.obj"; //  /  blade2.obj  cube.obj  kcube.obj  test cloth using this 
    std::string path2obj_c = DATA_ROOT_PATH"/VESS/ITKnife7_4.obj"; // knife correct ITKnife6  // cube knife knife3.obj";  //  //  113.obj";  // / VESS / TriangleTipKnife2.obj";  // blade2.obj  cube.obj kcube_s2.obj
    auto readMesh = imstk::MeshIO::read(path2obj_c); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);

    // Line mesh
    auto lineMesh = std::make_shared<imstk::LineMesh>();
    imstk::StdVectorOfVec3d vertList;
    vertList.resize(2);  // using 5 segment
    //vertList[0] = imstk::Vec3d(-13, -2.25, 9.15); // should - volume mesh therold than surface mesh (9.0, -1.9, -0.6) AABB 0: 9.0, -2.23, -0.6   -0.7 ITKnife6.DAE  // knife3.obj 9.5, -0.9, 1.18
    //vertList[1] = imstk::Vec3d(-5.93, -1.825, 9.125); // 9.7 ITKnife6.DAE // knife3.obj 9.5, -0.9, 8.73
    //vertList[2] = imstk::Vec3d(1.14, -1.4, 9.1); // (9.0, -2.55, 9.5)  AABB 1: 9.0, -2.23, 9.6  9.7 ITKnife6.DAE // knife3.obj 9.5, -0.9, 8.73
    vertList[0] = imstk::Vec3d(-16.4, -1.85, 9.15);
    vertList[1] = imstk::Vec3d(-9.3, -1.4, 9.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    std::vector<std::vector<int> > connectivity;
    for (int i = 0; i < 1;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity.push_back(line);
    }

    lineMesh->setInitialVertexPositions(vertList);
    lineMesh->setVertexPositions(vertList);
    lineMesh->setConnectivity(connectivity);

    // using for line mesh display
    auto lineSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertListLine;
    vertListLine.resize(3);
    vertListLine[0] = imstk::Vec3d(-16.4, -1.85, 9.15);
    vertListLine[1] = imstk::Vec3d(-9.3, -1.4, 9.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertListLine[2] = imstk::Vec3d(-16.4, -1.85, 9.15);
    std::vector<std::vector<int> > connectivityLine;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivityLine.push_back(line);
    }

    lineSurfMesh->setInitialVertexPositions(vertListLine);
    lineSurfMesh->setVertexPositions(vertListLine);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> trianglesLine;
    imstk::SurfaceMesh::TriangleArray triLine[1];
    triLine[0] = { { 0, 1, 2 } };
    trianglesLine.push_back(triLine[0]);
    lineSurfMesh->setTrianglesVertices(trianglesLine);
    auto objectLine = std::make_shared<imstk::VisualObject>("visualLineMesh");
    auto materialLine = std::make_shared<RenderMaterial>();
    materialLine->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpLine = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialLine->addTexture(tmpLine);
    lineSurfMesh->setRenderMaterial(materialLine);
    objectLine->setVisualGeometry(lineSurfMesh); // change to any mesh created above
    scene->addSceneObject(objectLine);
    // XZH END

    auto visualMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(readMesh);
    auto physicsMesh = lineMesh; //visualMesh; //  lineMesh; //  imstk::MeshIO::read(path2obj_c);  //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
    auto collidingMesh = lineMesh; // visualMesh; //  lineMesh;
    Vec3d min1, max1;
    visualMesh->computeBoundingBox(min1, max1);

    auto material0 = std::make_shared<RenderMaterial>();
    auto diffuseTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto normalTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Normal.png", Texture::Type::NORMAL);
    auto roughnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Roughness.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material0->addTexture(diffuseTexture0);
    material0->addTexture(normalTexture0);
    material0->addTexture(roughnessTexture0);
    //material->setTexture(metalnessTexture);
    visualMesh->setRenderMaterial(material0);

    FILE *outpointtri;
    outpointtri = fopen("i:/AABB.txt", "w");
    fprintf(outpointtri, "V: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    collidingMesh->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "C: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    fclose(outpointtri);

    auto object = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject");

    auto objMapP2V = std::make_shared<imstk::OneToOneMap>();  // std::make_shared<imstk::TriangleLineMap>(); //  std::make_shared<imstk::OneToOneMap>();
    objMapP2V->setMaster(physicsMesh);
    objMapP2V->setSlave(visualMesh);
    objMapP2V->compute();


    auto objMapP2C = std::make_shared<imstk::OneToOneMap>();  //  colliding is physical mesh
    objMapP2C->setMaster(physicsMesh);
    objMapP2C->setSlave(collidingMesh);
    objMapP2C->compute();

    auto objMapC2V = std::make_shared<imstk::OneToOneMap>(); //  std::make_shared<imstk::TriangleLineMap>(); //  s std::make_shared<imstk::OneToOneMap>();
    objMapC2V->setMaster(collidingMesh);
    objMapC2V->setSlave(visualMesh);
    objMapC2V->compute();

    auto objMapC2P = std::make_shared<imstk::OneToOneMap>(); // colliding is physical mesh
    objMapC2P->setMaster(collidingMesh);
    objMapC2P->setSlave(physicsMesh);
    objMapC2P->compute();

    object->setVisualGeometry(visualMesh);
    object->setCollidingGeometry(collidingMesh);
    object->setPhysicsGeometry(physicsMesh);
    object->setPhysicsToCollidingMap(objMapP2C);
    object->setPhysicsToVisualMap(objMapP2V);
    object->setCollidingToVisualMap(objMapC2V);
    object->setColldingToPhysicsMap(objMapC2P);

    auto pbdModel = std::make_shared<PbdModel>();
    pbdModel->configure(/*Number of Constraints*/1,
        /*Constraint configuration*/"Distance 0.1",
        /*Mass*/0.0,
        /*Gravity*/"0 0 0",
        /*TimeStep*/0.0003,
        /*FixedPoint*/"",
        /*NumberOfIterationInConstraintSolver*/2, // 5
        /*Proximity*/0.3,  // use linemesh 0.3  // 0.01 use  //  cube 0.1
        /*Contact stiffness*/0.001);
    object->setDynamicalModel(pbdModel);

    //auto pbdSolver = std::make_shared<PbdSolver>();
    //pbdSolver->setPbdObject(object);
    //scene->addNonlinearSolver(pbdSolver);

    scene->addSceneObject(object);

    auto trackCtrl = std::make_shared<imstk::DeviceTracker>(client);
    trackCtrl->setTranslationScaling(0.25);  // 0.1  test 0.1 should use  Scale 0.001 will be better 
    auto controller = std::make_shared<imstk::PBDSceneObjectController>(object, trackCtrl);
    scene->addObjectController(controller);

    // Light (white)
    auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
    whiteLight->setPosition(imstk::Vec3d(-15, -1, 0));
    whiteLight->setFocalPoint(imstk::Vec3d(1, -1, 0));
    scene->addLight(whiteLight);
    auto whiteLight2 = std::make_shared<imstk::PointLight>("whiteLight2");
    whiteLight2->setPosition(imstk::Vec3d(1, -1, 0));
    whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    //scene->addLight(whiteLight2);

    // Light (red)
    auto colorLight = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight->setPosition(imstk::Vec3d(-3.5, 2, -28)); // - 25, -8, -4.5
    colorLight->setFocalPoint(imstk::Vec3d(-1.5, 22, -18)); // - 3.7, 5.5, 7
    colorLight->setColor(imstk::Color::White); // red
    colorLight->setSpotAngle(15);
    auto colorLight2 = std::make_shared<imstk::SpotLight>("colorLight2");
    colorLight2->setPosition(imstk::Vec3d(-2.5, 17, -23));
    colorLight2->setFocalPoint(imstk::Vec3d(-2.5, 7, -23));
    colorLight2->setColor(imstk::Color::White);
    colorLight2->setSpotAngle(15);
    auto colorLight3 = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight3->setPosition(imstk::Vec3d(-2.5, 7, -23));  //-25, -5, -2.5
    colorLight3->setFocalPoint(imstk::Vec3d(-2.5, 17, -23));  // -3.7, 5.5, 7
    colorLight3->setColor(imstk::Color::White); // red
    colorLight3->setSpotAngle(15);
    scene->addLight(colorLight);
    scene->addLight(colorLight2);
    scene->addLight(colorLight3);

    // Update Camera
    auto cam1 = scene->getCamera();
    cam1->setPosition(imstk::Vec3d(-12.5, -7, -33)); // -25, -8, -4.5
    cam1->setFocalPoint(imstk::Vec3d(2.5, 17, -13)); // -3.7, 5.5, 7
    cam1->setViewAngle(75);

    // Run
    sdk->setActiveScene(scene);
    sdk->startSimulation(true);
#endif
}

void testCuttingCubeDense()
{
    // SDK and Scene
    auto sdk = std::make_shared<imstk::SimulationManager>();
    auto sceneTest = sdk->createNewScene("SceneTest");

    //// Tetrahedral mesh
    imstk::StdVectorOfVec3d vertListTet;
    std::vector<imstk::TetrahedralMesh::TetraArray> tetConnectivity;
    //vector< vec3d > vertices;
    //vector< vec4u32 > elements;

    //physXLink linkVolSur;
    std::vector<physXLink> linksVolSur;
    bool flag = loadTetFile(DATA_ROOT_PATH"/VESS/cube1003_SB.tet", vertListTet, tetConnectivity, linksVolSur); // colon_tumor3_3_SB for test   colon_tumor3_SB  cubeTest2_SB
    auto cutTissueMesh = std::make_shared<imstk::CuttingVESSCutMesh>();
    cutTissueMesh->setInitialVertexPositions(vertListTet);
    cutTissueMesh->setVertexPositions(vertListTet);
    cutTissueMesh->setTetrahedraVertices(tetConnectivity);
    cutTissueMesh->setPhysXLink(linksVolSur);
    cutTissueMesh->setInitialPhysVertex(vertListTet);  // add later 06/04/2017

    cutTissueMesh->initializeCuttingMesh();
    cutTissueMesh->initializeCutting();
    cutTissueMesh->extractSurfaceMesh();
    cutTissueMesh->extractSurfaceMeshVolume();
    //auto cutTissueMesh = std::make_shared<imstk::CuttingVESSCutMesh>(tetMesh);
    cutTissueMesh->setFlagSplitMeshAfterCut(true);

    Vec3d min1, max1;
    cutTissueMesh->computeBoundingBox(min1, max1);

    // link surface control points
    imstk::StdVectorOfVec3d vertListLink;
    std::vector<imstk::SurfaceMesh::TriangleArray> trianglesLink;
    // volumetric surface mesh for whole tetrahedrons
    std::list<size_t> uniqueVertIdList2Volume;
    auto& linklist = cutTissueMesh->getPhysXLink();
    auto& verList = cutTissueMesh->getTetrahedraVertices();
    auto& posList = cutTissueMesh->getVertexPositions();
    using triA = imstk::SurfaceMesh::TriangleArray;
    for (int n = 0; n < linklist.size(); n++)
    {
        auto idxList = verList[linklist[n].tetraIndex];
        trianglesLink.push_back(triA{ { idxList[0], idxList[1], idxList[2] } });
        trianglesLink.push_back(triA{ { idxList[0], idxList[1], idxList[3] } });
        trianglesLink.push_back(triA{ { idxList[0], idxList[2], idxList[3] } });
        trianglesLink.push_back(triA{ { idxList[1], idxList[2], idxList[3] } });
    }

    for (const auto &face : trianglesLink)
    {
        uniqueVertIdList2Volume.push_back(face[0]);
        uniqueVertIdList2Volume.push_back(face[1]);
        uniqueVertIdList2Volume.push_back(face[2]);
    }
    uniqueVertIdList2Volume.sort();
    uniqueVertIdList2Volume.unique();

    std::list<size_t>::iterator it;
    size_t vertId;
    for (vertId = 0, it = uniqueVertIdList2Volume.begin(); it != uniqueVertIdList2Volume.end(); ++vertId, it++)
    {
        vertListLink.push_back(posList[*it]);
        for (auto &face : trianglesLink)
        {
            for (size_t i = 0; i < 3; ++i)
            {
                if (face[i] == *it)
                {
                    face[i] = vertId;
                }
            }
        }
    }

    auto surfMeshLink = std::make_shared<imstk::SurfaceMesh>();
    surfMeshLink->initialize(vertListLink, trianglesLink);

    // cube visual
    std::string pathColon = DATA_ROOT_PATH"/VESS/cube1003_SB.obj";
    auto objMesh = imstk::MeshIO::read(pathColon);
    auto visualColonMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh);
    auto collidingColonMesh = cutTissueMesh;
    //XZH
    FILE *outMesh = fopen("i:/outOBJ_cube1003.dat", "w");
    //visualColonMesh->getInitialVerticesPositions();
    fprintf(outMesh, "num vertices:%d\n", visualColonMesh->getNumVertices());
    Vec3d tempPos;
    for (int i = 0; i < visualColonMesh->getNumVertices(); ++i)
    {
        tempPos = visualColonMesh->getVertexPosition(i);
        fprintf(outMesh, "%f %f %f\n", tempPos.x(), tempPos.y(), tempPos.z());
    }
    fprintf(outMesh, "\n num tri:%d\n", visualColonMesh->getNumTriangles());
    for (size_t triId = 0; triId < visualColonMesh->getNumTriangles(); ++triId)
    {
        Vec3d center(0, 0, 0);
        auto vert = visualColonMesh->getTrianglesVertices().at(triId); // getTetrahedronVertices(triId);
        fprintf(outMesh, "%d %d %d\n", vert[0], vert[1], vert[2]);
        for (size_t i = 0; i < 3; ++i)
        {
            center = visualColonMesh->getInitialVertexPosition(vert[i]);
            //fprintf(outMesh, "i: %d %f %f %f  ", i, center.x(), center.y(), center.z());
        }
        //fprintf(outMesh, "\n");
    }

    fclose(outMesh);
    // XZH END

    // Construct a map
    auto colonMapP2V = std::make_shared<imstk::VESSTetraTriangleMap>();  // <imstk::OneToOneMap>();
    colonMapP2V->setMaster(cutTissueMesh);
    colonMapP2V->setSlave(visualColonMesh);
    colonMapP2V->compute();


    auto colonMapP2C = std::make_shared<imstk::OneToOneMap>();
    colonMapP2C->setMaster(cutTissueMesh);
    colonMapP2C->setSlave(collidingColonMesh);
    colonMapP2C->compute();

    auto colonMapC2V = std::make_shared<imstk::VESSTetraTriangleMap>();  //<imstk::OneToOneMap>();
    colonMapC2V->setMaster(collidingColonMesh);
    colonMapC2V->setSlave(visualColonMesh);
    colonMapC2V->compute();

    auto colonObj = std::make_shared<PbdObject>("Cube");
    colonObj->setCollidingGeometry(collidingColonMesh);
    colonObj->setVisualGeometry(visualColonMesh);
    colonObj->setPhysicsGeometry(cutTissueMesh);
    colonObj->setPhysicsToCollidingMap(colonMapP2C);
    colonObj->setPhysicsToVisualMap(colonMapP2V);
    colonObj->setCollidingToVisualMap(colonMapC2V);
    //sceneTest->addSceneObject(colonObj);

    DWORD t1, t2;
    t1 = GetTickCount();
    // start cutting
    vector<Vec3d> m_vBladeSegments;
    vector<Vec3d> m_vSweptQuad;
    Vec3d temp1, temp2;
    //temp1.x = -2.7982251644134521; temp1.y = 1.2786393165588379; temp1.z = -2.44757509231567;
    //temp2.x = 3.9769670963287354; temp2.y = 1.2786393165588379; temp2.z = -2.44757509231567;
    temp1.x() = -2.8; temp1.y() = 1.3; temp1.z() = -2.5;
    temp2.x() = 4.0; temp2.y() = 1.3; temp2.z() = -2.5;
    m_vBladeSegments.push_back(temp1);
    m_vBladeSegments.push_back(temp2);

    Vec3d temp3, temp4;
    //temp1.x = -2.7982251644134521; temp1.y = 1.2786393165588379; temp1.z = 1.0033550262451172;
    //temp2.x = -2.7982251644134521; temp2.y = 1.2786393165588379; temp2.z = -2.44757509231567;
    //temp3.x = 3.9769670963287354; temp3.y = 1.2786393165588379; temp3.z = 1.0033550262451172;
    //temp4.x = 3.9769670963287354; temp4.y = 1.2786393165588379; temp4.z = -2.44757509231567;
    //temp1.x() = -45; temp1.y() = -11; temp1.z() = 27;  // 2
    //temp2.x() = -45; temp2.y() = -11; temp2.z() = -35; // 1
    //temp3.x() = 17; temp3.y() = 51; temp3.z() = 27; // 3
    //temp4.x() = 17; temp4.y() = 51; temp4.z() = -35; // 4  // completed cutting
    //temp1.x() = -45; temp1.y() = -11; temp1.z() = 13;  // 2
    //temp2.x() = -45; temp2.y() = -11; temp2.z() = -35; // 1
    //temp3.x() = 17; temp3.y() = 51; temp3.z() = 13; // 3
    //temp4.x() = 17; temp4.y() = 51; temp4.z() = -35; // 4  // partial cutting 
    //temp1.x() = -10; temp1.y() = -11; temp1.z() = 0;  // 2 x=4
    //temp2.x() = -10; temp2.y() = -11; temp2.z() = -25; // 1
    //temp3.x() = 0; temp3.y() = 51; temp3.z() = 0; // 3
    //temp4.x() = 0; temp4.y() = 51; temp4.z() = -25; // 4  // partial cutting straight
    // Test 1
    temp1.x() = -10; temp1.y() = -11; temp1.z() = 0;  // 2 x=4
    temp2.x() = -10; temp2.y() = -11; temp2.z() = -25; // 1
    temp3.x() = 0; temp3.y() = 51; temp3.z() = 0; // 3
    temp4.x() = 0; temp4.y() = 51; temp4.z() = -25; // 4  // partial cutting straight
    //// Test 2
    //temp1.x() = -20; temp1.y() = -11; temp1.z() = 0;  // 2 x=4
    //temp2.x() = -20; temp2.y() = -11; temp2.z() = -15; // 1
    //temp3.x() = 0; temp3.y() = 51; temp3.z() = 0; // 3
    //temp4.x() = 0; temp4.y() = 51; temp4.z() = -15; // 4  // partial cutting straight
    //// Test 3
    //temp1.x() = -4; temp1.y() = -11; temp1.z() = 0;  // 2 x=4
    //temp2.x() = -4; temp2.y() = -11; temp2.z() = -5; // 1
    //temp3.x() = 0; temp3.y() = 51; temp3.z() = 0; // 3
    //temp4.x() = 0; temp4.y() = 51; temp4.z() = -5; // 4  // partial cutting straight
    // //Test 4
    //temp1.x() = 0; temp1.y() = 31; temp1.z() = 0;  // 2 x=4
    //temp2.x() = 0; temp2.y() = 31; temp2.z() = -5; // 1
    //temp3.x() = 0; temp3.y() = 51; temp3.z() = 0; // 3
    //temp4.x() = 0; temp4.y() = 51; temp4.z() = -5; // 4  // partial cutting straight
    //// Test 5
    //temp1.x() = 0; temp1.y() = 35; temp1.z() = 0;  // 2 x=4
    //temp2.x() = 0; temp2.y() = 35; temp2.z() = -5; // 1
    //temp3.x() = 0; temp3.y() = 51; temp3.z() = 0; // 3
    //temp4.x() = 0; temp4.y() = 51; temp4.z() = -5; // 4  // partial cutting straight
    //// Test 6
    //temp1.x() = 0; temp1.y() = 31; temp1.z() = 0;  // 2 x=4
    //temp2.x() = 0; temp2.y() = 31; temp2.z() = -30; // 1
    //temp3.x() = 0; temp3.y() = 51; temp3.z() = 0; // 3
    //temp4.x() = 0; temp4.y() = 51; temp4.z() = -30; // 4  // partial cutting straight
    //// Test 7
    //temp1.x() = 0; temp1.y() = 31; temp1.z() = -20;  // 2 x=4
    //temp2.x() = 0; temp2.y() = 31; temp2.z() = -30; // 1
    //temp3.x() = 0; temp3.y() = 51; temp3.z() = 20; // 3
    //temp4.x() = 0; temp4.y() = 51; temp4.z() = -30; // 4  // partial cutting straight
    //// Test 8
    //temp1.x() = 4; temp1.y() = 35; temp1.z() = 13;  // 2
    //temp2.x() = 4; temp2.y() =35; temp2.z() = -35; // 1
    //temp3.x() = 4; temp3.y() = 51; temp3.z() = 13; // 3
    //temp4.x() = 4; temp4.y() = 51; temp4.z() = -35; // 4  // partial cutting straight
    //// Test 9
    //temp1.x() = 4; temp1.y() = 35; temp1.z() = 0;  // 2
    //temp2.x() = 4; temp2.y() = 35; temp2.z() = -35; // 1
    //temp3.x() = 4; temp3.y() = 51; temp3.z() = 0; // 3
    //temp4.x() = 4; temp4.y() = 51; temp4.z() = -35; // 4  // partial cutting straight
    //// Test 10
    //temp1.x() = 4; temp1.y() = 35; temp1.z() = -13;  // 2
    //temp2.x() = 4; temp2.y() = 35; temp2.z() = -35; // 1
    //temp3.x() = 4; temp3.y() = 51; temp3.z() = -13; // 3
    //temp4.x() = 4; temp4.y() = 51; temp4.z() = -35; // 4  // partial cutting straight
    m_vSweptQuad.push_back(temp1);
    m_vSweptQuad.push_back(temp2);
    m_vSweptQuad.push_back(temp3);
    m_vSweptQuad.push_back(temp4);
    imstk::StdVectorOfVec3d vertListSwept;
    std::vector<imstk::SurfaceMesh::TriangleArray> trianglesSwept;
    vertListSwept.push_back(temp1);
    vertListSwept.push_back(temp2);
    vertListSwept.push_back(temp3);
    vertListSwept.push_back(temp4);
    trianglesSwept.push_back({ { 0, 1, 2 } });
    trianglesSwept.push_back({ { 1, 2, 3 } });
    auto surfMeshSwept = std::make_shared<imstk::SurfaceMesh>();
    surfMeshSwept->initialize(vertListSwept, trianglesSwept);

    int res = 0;
    res = cutTissueMesh->cut(m_vBladeSegments, m_vSweptQuad, true);

    // display the cut points related tet
    imstk::StdVectorOfVec3d vertPosListTet;
    std::vector<imstk::SurfaceMesh::TriangleArray> trianglesTet;
    // volumetric surface mesh for whole tetrahedrons
    std::list<size_t> uniqueVertIdTet;
    auto& tetList = cutTissueMesh->getCutElements();
    auto& verListTet = cutTissueMesh->getTetrahedraVertices();
    auto& posListTet = cutTissueMesh->getVertexPositions();
    using triA = imstk::SurfaceMesh::TriangleArray;
    for (int n = 0; n < tetList.size(); n++)
    {
        auto idxList = verListTet[tetList[n]];
        trianglesTet.push_back(triA{ { idxList[0], idxList[1], idxList[2] } });
        trianglesTet.push_back(triA{ { idxList[0], idxList[1], idxList[3] } });
        trianglesTet.push_back(triA{ { idxList[0], idxList[2], idxList[3] } });
        trianglesTet.push_back(triA{ { idxList[1], idxList[2], idxList[3] } });
    }

    for (const auto &face : trianglesTet)
    {
        uniqueVertIdTet.push_back(face[0]);
        uniqueVertIdTet.push_back(face[1]);
        uniqueVertIdTet.push_back(face[2]);
    }
    uniqueVertIdTet.sort();
    uniqueVertIdTet.unique();

    //std::list<size_t>::iterator it;
    //size_t vertId;
    for (vertId = 0, it = uniqueVertIdTet.begin(); it != uniqueVertIdTet.end(); ++vertId, it++)
    {
        vertPosListTet.push_back(posList[*it]);
        for (auto &face : trianglesTet)
        {
            for (size_t i = 0; i < 3; ++i)
            {
                if (face[i] == *it)
                {
                    face[i] = vertId;
                }
            }
        }
    }

    auto surfMeshTet = std::make_shared<imstk::SurfaceMesh>();
    surfMeshTet->initialize(vertPosListTet, trianglesTet);

    // finding out the related triangles in visual surface mesh from vecCutElements
    std::vector<U32> cutNodesVisual;
    for (int m = 0; m < linksVolSur.size();m++) // traverse the whole links
    {
        auto link = linksVolSur[m];
        std::vector<U32>::iterator result = find(tetList.begin(), tetList.end(), link.tetraIndex);  // find whether 
        if (result != tetList.end()) // existing
        {
            cutNodesVisual.push_back(m); // related nodes in visual surface mesh
        }
    }
    std::vector<U32> m_trianglesNumCut;
    auto& triLists = visualColonMesh->getTrianglesVertices();
    for (int m = 0; m < triLists.size(); m++)
    {
        auto& tri = triLists[m];
        std::vector<U32>::iterator result = find(cutNodesVisual.begin(), cutNodesVisual.end(), tri[0]);
        if (result != cutNodesVisual.end()) // existing
        {
            m_trianglesNumCut.push_back(m); // related triangle in visual surface mesh
            continue; // next m
        }
        result = find(cutNodesVisual.begin(), cutNodesVisual.end(), tri[1]);
        if (result != cutNodesVisual.end()) // existing
        {
            m_trianglesNumCut.push_back(m); // related triangle in visual surface mesh
            continue;
        }
        result = find(cutNodesVisual.begin(), cutNodesVisual.end(), tri[2]);
        if (result != cutNodesVisual.end()) // existing
        {
            m_trianglesNumCut.push_back(m); // related triangle in visual surface mesh
        }
    }

    // mapping assimp visual mesh TO original mesh
    FILE *fp = fopen("i:/connect_cube1003.txt", "r");
    if (!fp) return;
    int n1, n2;
    std::unordered_map<size_t, size_t> assimp2orgMap;
    for (int i = 0; i < 477; i++)
    {
        fscanf(fp, "%d %d\n", &n1, &n2);
        assimp2orgMap.insert(make_pair(n1, n2));
    }
    fclose(fp);
    
    // 
    auto& posVisualList = visualColonMesh->getVertexPositions(); // changable 
    //vars
    Vec3d uvw, xyz, ss0, ss1;
    double t;
    Vec3d tri1[3] = { m_vSweptQuad[0], m_vSweptQuad[2], m_vSweptQuad[1] };
    Vec3d tri2[3] = { m_vSweptQuad[2], m_vSweptQuad[3], m_vSweptQuad[1] };
    res = 0;
    imstk::StdVectorOfVec3d newVertList;
    std::vector<Vec3d> newVertListTriangulation; //
    std::list<size_t> newVertIndex;
    std::vector<imstk::SurfaceMesh::TriangleArray> newTriangles;
    for (int m = 0; m < m_trianglesNumCut.size();m++)
    {
        auto& tri = triLists[m_trianglesNumCut[m]];
        ss0 = posVisualList[tri[0]];
        ss1 = posVisualList[tri[1]];

        res = IntersectSegmentTriangle(ss0, ss1, tri1, t, uvw, xyz); //  6  9: res ← IntersectSegmentTri(edge0, edge1, tri0, p)
        if (res == 0)
        {
            res = IntersectSegmentTriangle(ss0, ss1, tri2, t, uvw, xyz);
            if (res>0)
            {
                Vec3d temp = ss0 + (ss1 - ss0).normalized() * t;
                if (t<=0.5) // move to ss0
                {
                    visualColonMesh->setVertexPosition(tri[0], temp);
                    newVertIndex.push_back(tri[0]);
                    for (std::unordered_map< size_t, size_t >::iterator it = assimp2orgMap.begin(); it != assimp2orgMap.end(); it++)
                    {
                        if (it->second == assimp2orgMap.at(tri[0]))  // in the original is the same vertex
                        {
                            visualColonMesh->setVertexPosition(it->first, temp);
                        }
                    }
                }
                else // move to ss1
                {
                    visualColonMesh->setVertexPosition(tri[1], temp);
                    newVertIndex.push_back(tri[1]);
                    for (std::unordered_map< size_t, size_t >::iterator it = assimp2orgMap.begin(); it != assimp2orgMap.end(); it++)
                    {
                        if (it->second == assimp2orgMap.at(tri[1]))  // in the original is the same vertex
                        {
                            visualColonMesh->setVertexPosition(it->first, temp);
                        }
                    }
                }
                newVertList.push_back(temp);             
            }
        }
        else // >0
        {
            Vec3d temp = ss0 + (ss1 - ss0).normalized() * t;
            if (t <= 0.5) // move to ss0
            {
                visualColonMesh->setVertexPosition(tri[0], temp);
                newVertIndex.push_back(tri[0]);
                for (std::unordered_map< size_t, size_t >::iterator it = assimp2orgMap.begin(); it != assimp2orgMap.end(); it++)
                {
                    if (it->second == assimp2orgMap.at(tri[0]))  // in the original is the same vertex
                    {
                        visualColonMesh->setVertexPosition(it->first, temp);
                    }
                }
            }
            else // move to ss1
            {
                visualColonMesh->setVertexPosition(tri[1], temp);
                newVertIndex.push_back(tri[1]);
                for (std::unordered_map< size_t, size_t >::iterator it = assimp2orgMap.begin(); it != assimp2orgMap.end(); it++)
                {
                    if (it->second == assimp2orgMap.at(tri[1]))  // in the original is the same vertex
                    {
                        visualColonMesh->setVertexPosition(it->first, temp);
                    }
                }
            }
            newVertList.push_back(temp);
        }

        // edge 1
        ss0 = posVisualList[tri[1]];
        ss1 = posVisualList[tri[2]];

        res = IntersectSegmentTriangle(ss0, ss1, tri1, t, uvw, xyz); //  6  9: res ← IntersectSegmentTri(edge0, edge1, tri0, p)
        if (res == 0)
        {
            res = IntersectSegmentTriangle(ss0, ss1, tri2, t, uvw, xyz);
            if (res > 0)
            {
                Vec3d temp = ss0 + (ss1 - ss0).normalized() * t;
                if (t <= 0.5) // move to ss0
                {
                    visualColonMesh->setVertexPosition(tri[1], temp);
                    newVertIndex.push_back(tri[1]);
                    for (std::unordered_map< size_t, size_t >::iterator it = assimp2orgMap.begin(); it != assimp2orgMap.end(); it++)
                    {
                        if (it->second == assimp2orgMap.at(tri[1]))  // in the original is the same vertex
                        {
                            visualColonMesh->setVertexPosition(it->first, temp);
                        }
                    }
                }
                else // move to ss1
                {
                    visualColonMesh->setVertexPosition(tri[2], temp);
                    newVertIndex.push_back(tri[2]);
                    for (std::unordered_map< size_t, size_t >::iterator it = assimp2orgMap.begin(); it != assimp2orgMap.end(); it++)
                    {
                        if (it->second == assimp2orgMap.at(tri[2]))  // in the original is the same vertex
                        {
                            visualColonMesh->setVertexPosition(it->first, temp);
                        }
                    }
                }
                newVertList.push_back(temp);
            }
        }
        else // >0
        {
            Vec3d temp = ss0 + (ss1 - ss0).normalized() * t;
            if (t <= 0.5) // move to ss0
            {
                visualColonMesh->setVertexPosition(tri[1], temp);
                newVertIndex.push_back(tri[1]);
                for (std::unordered_map< size_t, size_t >::iterator it = assimp2orgMap.begin(); it != assimp2orgMap.end(); it++)
                {
                    if (it->second == assimp2orgMap.at(tri[1]))  // in the original is the same vertex
                    {
                        visualColonMesh->setVertexPosition(it->first, temp);
                    }
                }
            }
            else // move to ss1
            {
                visualColonMesh->setVertexPosition(tri[2], temp);
                newVertIndex.push_back(tri[2]);
                for (std::unordered_map< size_t, size_t >::iterator it = assimp2orgMap.begin(); it != assimp2orgMap.end(); it++)
                {
                    if (it->second == assimp2orgMap.at(tri[2]))  // in the original is the same vertex
                    {
                        visualColonMesh->setVertexPosition(it->first, temp);
                    }
                }
            }
            newVertList.push_back(temp);
        }

        // edge 2
        ss0 = posVisualList[tri[0]];
        ss1 = posVisualList[tri[2]];

        res = IntersectSegmentTriangle(ss0, ss1, tri1, t, uvw, xyz); //  6  9: res ← IntersectSegmentTri(edge0, edge1, tri0, p)
        if (res == 0)
        {
            res = IntersectSegmentTriangle(ss0, ss1, tri2, t, uvw, xyz);
            if (res > 0)
            {
                Vec3d temp = ss0 + (ss1 - ss0).normalized() * t;
                if (t <= 0.5) // move to ss0
                {
                    visualColonMesh->setVertexPosition(tri[0], temp);
                    newVertIndex.push_back(tri[0]);
                    for (std::unordered_map< size_t, size_t >::iterator it = assimp2orgMap.begin(); it != assimp2orgMap.end(); it++)
                    {
                        if (it->second == assimp2orgMap.at(tri[0]))  // in the original is the same vertex
                        {
                            visualColonMesh->setVertexPosition(it->first, temp);
                        }
                    }
                }
                else // move to ss1
                {
                    visualColonMesh->setVertexPosition(tri[2], temp);
                    newVertIndex.push_back(tri[2]);
                    for (std::unordered_map< size_t, size_t >::iterator it = assimp2orgMap.begin(); it != assimp2orgMap.end(); it++)
                    {
                        if (it->second == assimp2orgMap.at(tri[2]))  // in the original is the same vertex
                        {
                            visualColonMesh->setVertexPosition(it->first, temp);
                        }
                    }
                }
                newVertList.push_back(temp);
            }
        }
        else // >0
        {
            Vec3d temp = ss0 + (ss1 - ss0).normalized() * t;
            if (t <= 0.5) // move to ss0
            {
                visualColonMesh->setVertexPosition(tri[0], temp);
                newVertIndex.push_back(tri[0]);
                for (std::unordered_map< size_t, size_t >::iterator it = assimp2orgMap.begin(); it != assimp2orgMap.end(); it++)
                {
                    if (it->second == assimp2orgMap.at(tri[0]))  // in the original is the same vertex
                    {
                        visualColonMesh->setVertexPosition(it->first, temp);
                    }
                }
            }
            else // move to ss1
            {
                visualColonMesh->setVertexPosition(tri[2], temp);
                newVertIndex.push_back(tri[2]);
                for (std::unordered_map< size_t, size_t >::iterator it = assimp2orgMap.begin(); it != assimp2orgMap.end(); it++)
                {
                    if (it->second == assimp2orgMap.at(tri[2]))  // in the original is the same vertex
                    {
                        visualColonMesh->setVertexPosition(it->first, temp);
                    }
                }
            }
            newVertList.push_back(temp);
        }
    }
    newVertList.clear();
    imstk::StdVectorOfVec3d newVertList2;
    newVertIndex.sort();
    newVertIndex.unique();
    std::unordered_map< size_t, size_t> mapFromG2L; // global to local
    std::unordered_map< size_t, size_t>::iterator itList; // = mesh2->m_mapEdgeTriIndex.find(edgekey);
    size_t vertID;
    std::list<size_t>::iterator itt;
    std::vector<size_t> newVertIndexLocal;
    auto& posVisualListUpdate = visualColonMesh->getVertexPositions(); // changable 
    for (vertID = 0, itt = newVertIndex.begin(); itt != newVertIndex.end(); ++vertID, itt++)
    {
        newVertList.push_back(posVisualListUpdate[*itt]);
        newVertList2.push_back(posVisualListUpdate[*itt] + Vec3d(0, 0, 10));
        mapFromG2L.insert(make_pair(*itt, vertID));
        newVertIndexLocal.push_back(vertID);
    }
    //newVertIndexLocal.push_back(vertID);
    //newVertList.push_back(m_vSweptQuad[0]);
    //newVertList2.push_back(m_vSweptQuad[0] + Vec3d(0, 0, 10));
    //vertID++;
    //newVertIndexLocal.push_back(vertID);
    //newVertList.push_back(m_vSweptQuad[2]);
    //newVertList2.push_back(m_vSweptQuad[2] + Vec3d(0, 0, 10));

    std::vector<SurfaceMesh::TriangleArray> surfaceNewTri;
    using triArray = SurfaceMesh::TriangleArray;
    int mCnt = newVertIndexLocal.size();
    //for (int k = 0; k < mCnt - 3;k++)
    //{
    //    surfaceNewTri.push_back(triArray{ { k, k+1, mCnt-2 } });
    //}
    //surfaceNewTri.push_back(triArray{ { mCnt - 3, mCnt - 2, mCnt - 1 } });

    auto delaunayTriangulation = std::make_shared<imstk::Delaunay>();
    for (auto& vert:newVertList)
    {
        newVertListTriangulation.push_back(vert);
    }
    std::vector<Triangle> triangles = delaunayTriangulation->triangulate(newVertListTriangulation);
    std::unordered_map< size_t, Vec3d> mapNewSurface; // global to local
    std::unordered_map< size_t, Vec3d>::iterator itnew; // = mesh2->m_mapEdgeTriIndex.find(edgekey);
    newVertList.clear();
    int cnt = 0;
    for (int k = 0; k < triangles.size();k++)
    {
        int num[3];
        bool isin = false;
        for (int m = 0; m < 3;m++)
        {
            Vec3d temp;
            switch (m)
            {
            case 0:
                temp = triangles[k].p1;
                isin = false;
                for (itnew = mapNewSurface.begin(); itnew != mapNewSurface.end(); itnew++)
                {
                    if (itnew->second == temp)  // in the original is the same vertex
                    {
                        isin = true; // existing
                        num[0] = itnew->first;
                        break;;  // next m
                    }
                }
                if (!isin) // not existing
                {
                    mapNewSurface.insert(make_pair(cnt, temp));
                    newVertList.push_back(temp);
                    num[0] = cnt;
                    cnt++;
                }
                break;
            case 1:
                temp = triangles[k].p2;
                isin = false;
                for (itnew = mapNewSurface.begin(); itnew != mapNewSurface.end(); itnew++)
                {
                    if (itnew->second == temp)  // in the original is the same vertex
                    {
                        isin = true; // existing
                        num[1] = itnew->first;
                        break;;  // next m
                    }
                }
                if (!isin) // not existing
                {
                    mapNewSurface.insert(make_pair(cnt, temp));
                    newVertList.push_back(temp);
                    num[1] = cnt;
                    cnt++;
                }
                break;
            case 2:
                temp = triangles[k].p3;
                isin = false;
                for (itnew = mapNewSurface.begin(); itnew != mapNewSurface.end(); itnew++)
                {
                    if (itnew->second == temp)  // in the original is the same vertex
                    {
                        isin = true; // existing
                        num[2] = itnew->first;
                        break;;  // next m
                    }
                }
                if (!isin) // not existing
                {
                    mapNewSurface.insert(make_pair(cnt, temp));
                    newVertList.push_back(temp);
                    num[2] = cnt;
                    cnt++;
                }
                break;
            }
        }
        surfaceNewTri.push_back(triArray{ { num[0], num[1], num[2] } });
    }
    auto surfMeshVisualNew = std::make_shared<imstk::SurfaceMesh>();
    surfMeshVisualNew->initialize(newVertList, surfaceNewTri);

    t2 = GetTickCount();
    printf("cutting time: %d ms, HZ: %f\n", t2 - t1, 1000 / double(t2 - t1));

    // c. Extract the surface mesh
    auto surfMesh = std::make_shared<imstk::SurfaceMesh>();
    cutTissueMesh->extractSurfaceMesh(); //  (surfMesh);
    surfMesh = cutTissueMesh->getSurfaceMesh();
    surfMesh->computeVertexNormals();

    auto surfMeshVolume = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMeshVolume = cutTissueMesh->getSurfaceMeshVolume();
    surfMeshVolume->computeVertexNormals();

    //auto surfMeshFront = std::make_shared<imstk::SurfaceMesh>();
    ////cutTissueMesh->extractSurfaceMesh(surfMesh);
    //surfMeshFront = cutTissueMesh->getSurfaceCutFrontMesh();
    //surfMeshFront->computeVertexNormals();

    auto surfMeshFrontNew = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMeshFrontNew = cutTissueMesh->getSurfaceCutFrontMeshNew();
    surfMeshFrontNew->computeVertexNormals();

    auto surfMeshFrontVolume = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMeshFrontVolume = cutTissueMesh->getSurfaceCutFrontMeshVolume();
    surfMeshFrontVolume->computeVertexNormals();

    //auto surfMeshBack = std::make_shared<imstk::SurfaceMesh>();
    //surfMeshBack = cutTissueMesh->getSurfaceCutBackMesh();
    //surfMeshBack->computeVertexNormals();

    auto surfMeshBackNew = std::make_shared<imstk::SurfaceMesh>();
    surfMeshBackNew = cutTissueMesh->getSurfaceCutBackMeshNew();
    surfMeshBackNew->computeVertexNormals();

    auto surfMeshBackVolume = std::make_shared<imstk::SurfaceMesh>();
    surfMeshBackVolume = cutTissueMesh->getSurfaceCutBackMeshVolume();
    surfMeshBackVolume->computeVertexNormals();

    // surfMeshVisualNew
    auto objectVisualNew = std::make_shared<imstk::VisualObject>("visualColonMeshVisualNew");
    auto materialVisualNew = std::make_shared<RenderMaterial>();
    materialVisualNew->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME SURFACE
    auto tmpVisualNew = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    materialVisualNew->addTexture(tmpVisualNew);
    surfMeshVisualNew->setRenderMaterial(materialVisualNew);
    objectVisualNew->setVisualGeometry(surfMeshVisualNew); // change to any mesh created above
    //sceneTest->addSceneObject(objectVisualNew);

    auto object0 = std::make_shared<imstk::VisualObject>("visualColonMesh0");
    auto material0 = std::make_shared<RenderMaterial>();
    material0->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME SURFACE
    auto tmp0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    material0->addTexture(tmp0);
    surfMeshSwept->setRenderMaterial(material0);
    object0->setVisualGeometry(surfMeshSwept); // change to any mesh created above
    //sceneTest->addSceneObject(object0);

    auto objectLink = std::make_shared<imstk::VisualObject>("visualColonMeshLink");
    auto materialLink = std::make_shared<RenderMaterial>();
    materialLink->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME SURFACE
    auto tmpLink = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    materialLink->addTexture(tmpLink);
    surfMeshLink->setRenderMaterial(materialLink);
    objectLink->setVisualGeometry(surfMeshLink); // change to any mesh created above
    //sceneTest->addSceneObject(objectLink);

    auto objectTet = std::make_shared<imstk::VisualObject>("visualColonMeshTet");
    auto materialTet = std::make_shared<RenderMaterial>();
    materialTet->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME); // WIREFRAME SURFACE
    auto tmpTet = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    materialTet->addTexture(tmpTet);
    surfMeshTet->setRenderMaterial(materialTet);
    objectTet->setVisualGeometry(surfMeshTet); // change to any mesh created above
    //sceneTest->addSceneObject(objectTet);

    auto objectVisual = std::make_shared<imstk::VisualObject>("visualColonMeshVisual");
    auto materialVisual = std::make_shared<RenderMaterial>();
    materialVisual->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME SURFACE
    auto tmpVisual = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    materialVisual->addTexture(tmpVisual);
    visualColonMesh->setRenderMaterial(materialVisual);
    objectVisual->setVisualGeometry(visualColonMesh); // change to any mesh created above
    sceneTest->addSceneObject(objectVisual);

    auto object = std::make_shared<imstk::VisualObject>("visualColonMesh");
    auto material = std::make_shared<RenderMaterial>();
    material->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME); // WIREFRAME SURFACE
    auto tmp = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    material->addTexture(tmp);
    surfMesh->setRenderMaterial(material);
    object->setVisualGeometry(surfMesh); // change to any mesh created above
    //sceneTest->addSceneObject(object);

    auto objectVolume = std::make_shared<imstk::VisualObject>("visualColonMeshVolume");
    auto materialVolume = std::make_shared<RenderMaterial>();
    materialVolume->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME); // WIREFRAME SURFACE
    auto tmpVolume = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    materialVolume->addTexture(tmpVolume);
    surfMeshVolume->setRenderMaterial(materialVolume);
    objectVolume->setVisualGeometry(surfMeshVolume); // change to any mesh created above
    //sceneTest->addSceneObject(objectVolume);

    //auto object4 = std::make_shared<imstk::VisualObject>("visualColonMesh4");
    //auto material4 = std::make_shared<RenderMaterial>();
    //material4->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME
    //auto tmp4 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    //material4->addTexture(tmp4);
    //surfMeshFront->setRenderMaterial(material4);
    //object4->setVisualGeometry(surfMeshFront); // change to any mesh created above
    ////sceneTest->addSceneObject(object4);

    auto object5 = std::make_shared<imstk::VisualObject>("visualColonMesh5");
    auto material5 = std::make_shared<RenderMaterial>();
    material5->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME
    auto tmp5 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/submucosal/Submucosal_layer_BaseColor.png", Texture::Type::DIFFUSE);
    material5->addTexture(tmp5);
    surfMeshFrontNew->setRenderMaterial(material5);
    object5->setVisualGeometry(surfMeshFrontNew); // change to any mesh created above
    sceneTest->addSceneObject(object5);

    auto object6 = std::make_shared<imstk::VisualObject>("visualColonMesh6");
    auto material6 = std::make_shared<RenderMaterial>();
    material6->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME); // WIREFRAME
    auto tmp6 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/submucosal/Submucosal_layer_BaseColor.png", Texture::Type::DIFFUSE);
    material6->addTexture(tmp6);
    surfMeshFrontVolume->setRenderMaterial(material6);
    object6->setVisualGeometry(surfMeshFrontVolume); // change to any mesh created above
    //sceneTest->addSceneObject(object6);

    //auto object3 = std::make_shared<imstk::VisualObject>("visualColonMesh3");
    //auto material3 = std::make_shared<RenderMaterial>();
    //material3->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    //auto tmp3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    //material3->addTexture(tmp3);
    //surfMeshBack->setRenderMaterial(material3);
    //object3->setVisualGeometry(surfMeshBack); // change to any mesh created above
    ////sceneTest->addSceneObject(object3);

    auto object2 = std::make_shared<imstk::VisualObject>("visualColonMesh2");
    auto material2 = std::make_shared<RenderMaterial>();
    material2->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE);
    auto tmp2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/submucosal/Submucosal_layer_BaseColor.png", Texture::Type::DIFFUSE);
    material2->addTexture(tmp2);
    surfMeshBackNew->setRenderMaterial(material2);
    object2->setVisualGeometry(surfMeshBackNew); // change to any mesh created above
    sceneTest->addSceneObject(object2);

    auto object22 = std::make_shared<imstk::VisualObject>("visualColonMesh22");
    auto material22 = std::make_shared<RenderMaterial>();
    material22->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmp22 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/submucosal/Submucosal_layer_BaseColor.png", Texture::Type::DIFFUSE);
    material22->addTexture(tmp22);
    surfMeshBackVolume->setRenderMaterial(material22);
    object22->setVisualGeometry(surfMeshBackVolume); // change to any mesh created above
    //sceneTest->addSceneObject(object22);

    // Light (white)
    auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
    whiteLight->setPosition(imstk::Vec3d(-10, 20, -50));
    //whiteLight->setFocalPoint(imstk::Vec3d(1, -1, 0));
    whiteLight->setIntensity(10000);
    sceneTest->addLight(whiteLight);
    auto whiteLight2 = std::make_shared<imstk::PointLight>("whiteLight2");
    whiteLight2->setPosition(imstk::Vec3d(-10, 20, 40));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight2->setIntensity(10000);
    sceneTest->addLight(whiteLight2);
    auto whiteLight3 = std::make_shared<imstk::PointLight>("whiteLight3");
    whiteLight3->setPosition(imstk::Vec3d(-60, 20, -5));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight3->setIntensity(10000);
    sceneTest->addLight(whiteLight3);
    auto whiteLight4 = std::make_shared<imstk::PointLight>("whiteLight4");
    whiteLight4->setPosition(imstk::Vec3d(35, 20, -5));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight4->setIntensity(10000);
    sceneTest->addLight(whiteLight4);
    auto whiteLight5 = std::make_shared<imstk::PointLight>("whiteLight5");
    whiteLight5->setPosition(imstk::Vec3d(-10, -25, -5));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight5->setIntensity(10000);
    sceneTest->addLight(whiteLight5);
    auto whiteLight6 = std::make_shared<imstk::PointLight>("whiteLight6");
    whiteLight6->setPosition(imstk::Vec3d(-10, 65, -5));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight6->setIntensity(10000);
    sceneTest->addLight(whiteLight6);

    // Light (red)
    auto colorLight = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight->setPosition(imstk::Vec3d(-10, 20, 50)); // - 25, -8, -4.5
    colorLight->setFocalPoint(imstk::Vec3d(-10, 20, -25)); // - 3.7, 5.5, 7
    colorLight->setColor(imstk::Color::White); // red
    colorLight->setSpotAngle(70);
    colorLight->setIntensity(10000);
    auto colorLight2 = std::make_shared<imstk::SpotLight>("colorLight2");
    colorLight2->setPosition(imstk::Vec3d(-10, 20, -60));
    colorLight2->setFocalPoint(imstk::Vec3d(-10, 20, 15));
    colorLight2->setColor(imstk::Color::White);
    colorLight2->setSpotAngle(70);
    colorLight2->setIntensity(10000);
    auto colorLight3 = std::make_shared<imstk::SpotLight>("colorLight3");
    colorLight3->setPosition(imstk::Vec3d(-2.5, 7, -23));  //-25, -5, -2.5
    colorLight3->setFocalPoint(imstk::Vec3d(-2.5, 17, -23));  // -3.7, 5.5, 7
    colorLight3->setColor(imstk::Color::White); // red
    colorLight3->setSpotAngle(150);
    colorLight3->setIntensity(2000);
    sceneTest->addLight(colorLight);
    sceneTest->addLight(colorLight2);
    //sceneTest->addLight(colorLight3);

    // Update Camera
    auto cam1 = sceneTest->getCamera();
    cam1->setPosition(imstk::Vec3d(-12.5, -7, -33)); // -25, -8, -4.5
    cam1->setFocalPoint(imstk::Vec3d(2.5, 17, -13)); // -3.7, 5.5, 7
    cam1->setViewAngle(75);

    // Run
    sdk->setActiveScene(sceneTest);
    sdk->startSimulation(true);
}

void testCuttingCube()
{
    // SDK and Scene
    auto sdk = std::make_shared<imstk::SimulationManager>();
    auto sceneTest = sdk->createNewScene("SceneTest");

    //// Tetrahedral mesh
    imstk::StdVectorOfVec3d vertListTet;
    std::vector<imstk::TetrahedralMesh::TetraArray> tetConnectivity;
    //vector< vec3d > vertices;
    //vector< vec4u32 > elements;

    //int nx = 4, ny = 4, nz = 4; // 2 4 8
    //double cellsize = 1.0; // 2.0 1.0 0.5
    //Vec3d start = Vec3d(-(double)(nx) / 2.0, 0, -(double)(nz) / 2.0) * (cellsize);

    ////add all nodes
    //for (int i = 0; i < nx; i++)
    //{
    //    for (int j = 0; j < ny; j++)
    //    {
    //        for (int k = 0; k < nz; k++)
    //        {
    //            Vec3d v = start + Vec3d(i, j, k) * cellsize;
    //            vertListTet.push_back(v);
    //        }
    //    }
    //}

    ////corner defs
    //enum CellCorners { LBN = 0, LBF = 1, LTN = 2, LTF = 3, RBN = 4, RBF = 5, RTN = 6, RTF = 7 };
    //U32 cn[8];
    ////imstk::TetrahedralMesh::TetraArray tet1;
    ////add all elements
    //for (int i = 0; i < nx - 1; i++)
    //{
    //    for (int j = 0; j < ny - 1; j++)
    //    {
    //        for (int k = 0; k < nz - 1; k++)
    //        {
    //            //collect cell nodes
    //            cn[LBN] = i * ny * nz + j * nz + k;
    //            cn[LBF] = i * ny * nz + j * nz + k + 1;

    //            cn[LTN] = i * ny * nz + (j + 1) * nz + k;
    //            cn[LTF] = i * ny * nz + (j + 1) * nz + k + 1;

    //            cn[RBN] = (i + 1) * ny * nz + j * nz + k;
    //            cn[RBF] = (i + 1) * ny * nz + j * nz + k + 1;

    //            cn[RTN] = (i + 1) * ny * nz + (j + 1) * nz + k;
    //            cn[RTF] = (i + 1) * ny * nz + (j + 1) * nz + k + 1;

    //            //add elements
    //            imstk::TetrahedralMesh::TetraArray tet1 = { cn[LBN], cn[LTN], cn[RBN], cn[LBF] };
    //            tetConnectivity.push_back(tet1);
    //            imstk::TetrahedralMesh::TetraArray tet2 = { cn[RTN], cn[LTN], cn[LBF], cn[RBN] };
    //            tetConnectivity.push_back(tet2);
    //            imstk::TetrahedralMesh::TetraArray tet3 = { cn[RTN], cn[LTN], cn[LTF], cn[LBF] };
    //            tetConnectivity.push_back(tet3);
    //            imstk::TetrahedralMesh::TetraArray tet4 = { cn[RTN], cn[RBN], cn[LBF], cn[RBF] };
    //            tetConnectivity.push_back(tet4);
    //            imstk::TetrahedralMesh::TetraArray tet5 = { cn[RTN], cn[LBF], cn[LTF], cn[RBF] };
    //            tetConnectivity.push_back(tet5);
    //            imstk::TetrahedralMesh::TetraArray tet6 = { cn[RTN], cn[LTF], cn[RTF], cn[RBF] };
    //            tetConnectivity.push_back(tet6);
    //        }
    //    }
    //}

    //build the final mesh
    //vector<double> vFlatVertices;
    //vector<U32> vFlatElements;
    //FlattenVec3<double>(vertices, vFlatVertices);
    //FlattenVec4<U32>(elements, vFlatElements);

    //physXLink linkVolSur;
    std::vector<physXLink> linksVolSur;
    bool flag = loadTetFile(DATA_ROOT_PATH"/VESS/cube_SB.tet", vertListTet, tetConnectivity, linksVolSur); // colon_tumor3_3_SB for test   colon_tumor3_SB  cubeTest2_SB
    auto cutTissueMesh = std::make_shared<imstk::CuttingVESSCutMesh>();
    cutTissueMesh->setInitialVertexPositions(vertListTet);
    cutTissueMesh->setVertexPositions(vertListTet);
    cutTissueMesh->setTetrahedraVertices(tetConnectivity);
    cutTissueMesh->setPhysXLink(linksVolSur);
    cutTissueMesh->setInitialPhysVertex(vertListTet);  // add later 06/04/2017

    cutTissueMesh->initializeCuttingMesh();
    cutTissueMesh->initializeCutting();
    cutTissueMesh->extractSurfaceMesh();
    cutTissueMesh->extractSurfaceMeshVolume();
    //auto cutTissueMesh = std::make_shared<imstk::CuttingVESSCutMesh>(tetMesh);
    cutTissueMesh->setFlagSplitMeshAfterCut(true);

    Vec3d min1, max1;
    cutTissueMesh->computeBoundingBox(min1, max1);

    // link surface control points
    imstk::StdVectorOfVec3d vertListLink;
    std::vector<imstk::SurfaceMesh::TriangleArray> trianglesLink;
    // volumetric surface mesh for whole tetrahedrons
    std::list<size_t> uniqueVertIdList2Volume;
    auto& linklist=cutTissueMesh->getPhysXLink();
    auto& verList = cutTissueMesh->getTetrahedraVertices();
    auto& posList = cutTissueMesh->getVertexPositions();
    using triA = imstk::SurfaceMesh::TriangleArray;
    for (int n = 0; n < linklist.size(); n++)
    {
        auto idxList = verList[linklist[n].tetraIndex];
        trianglesLink.push_back(triA{ { idxList[0], idxList[1], idxList[2] } });
        trianglesLink.push_back(triA{ { idxList[0], idxList[1], idxList[3] } });
        trianglesLink.push_back(triA{ { idxList[0], idxList[2], idxList[3] } });
        trianglesLink.push_back(triA{ { idxList[1], idxList[2], idxList[3] } });
    }
    
    for (const auto &face : trianglesLink)
    {
        uniqueVertIdList2Volume.push_back(face[0]);
        uniqueVertIdList2Volume.push_back(face[1]);
        uniqueVertIdList2Volume.push_back(face[2]);
    }
    uniqueVertIdList2Volume.sort();
    uniqueVertIdList2Volume.unique();

    std::list<size_t>::iterator it;
    size_t vertId;
    for ( vertId = 0, it = uniqueVertIdList2Volume.begin(); it != uniqueVertIdList2Volume.end(); ++vertId, it++)
    {
        vertListLink.push_back(posList[*it]);
        for (auto &face : trianglesLink)
        {
            for (size_t i = 0; i < 3; ++i)
            {
                if (face[i] == *it)
                {
                    face[i] = vertId;
                }
            }
        }
    }

    auto surfMeshLink = std::make_shared<imstk::SurfaceMesh>();
    surfMeshLink->initialize(vertListLink, trianglesLink);

    // cube visual
    std::string pathColon = DATA_ROOT_PATH"/VESS/cube_SB.obj";
    auto objMesh = imstk::MeshIO::read(pathColon);
    auto visualColonMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh);
    auto collidingColonMesh = cutTissueMesh;
    //XZH
    FILE *outMesh = fopen("i:/outOBJ_cube.dat", "w");
    //visualColonMesh->getInitialVerticesPositions();
    fprintf(outMesh, "num vertices:%d\n", visualColonMesh->getNumVertices());
    Vec3d tempPos;
    for (int i = 0; i < visualColonMesh->getNumVertices(); ++i)
    {
        tempPos = visualColonMesh->getVertexPosition(i);
        fprintf(outMesh, "%f %f %f\n", tempPos.x(), tempPos.y(), tempPos.z());
    }
    fprintf(outMesh, "\n num tri:%d\n", visualColonMesh->getNumTriangles());
    for (size_t triId = 0; triId < visualColonMesh->getNumTriangles(); ++triId)
    {
        Vec3d center(0, 0, 0);
        auto vert = visualColonMesh->getTrianglesVertices().at(triId); // getTetrahedronVertices(triId);
        fprintf(outMesh, "%d %d %d\n", vert[0], vert[1], vert[2]);
        for (size_t i = 0; i < 3; ++i)
        {
            center = visualColonMesh->getInitialVertexPosition(vert[i]);
            //fprintf(outMesh, "i: %d %f %f %f  ", i, center.x(), center.y(), center.z());
        }
        //fprintf(outMesh, "\n");
    }

    fclose(outMesh);
    // XZH END

    // Construct a map
    auto colonMapP2V = std::make_shared<imstk::VESSTetraTriangleMap>();  // <imstk::OneToOneMap>();
    colonMapP2V->setMaster(cutTissueMesh);
    colonMapP2V->setSlave(visualColonMesh);
    colonMapP2V->compute();


    auto colonMapP2C = std::make_shared<imstk::OneToOneMap>();
    colonMapP2C->setMaster(cutTissueMesh);
    colonMapP2C->setSlave(collidingColonMesh);
    colonMapP2C->compute();

    auto colonMapC2V = std::make_shared<imstk::VESSTetraTriangleMap>();  //<imstk::OneToOneMap>();
    colonMapC2V->setMaster(collidingColonMesh);
    colonMapC2V->setSlave(visualColonMesh);
    colonMapC2V->compute();

    auto colonObj = std::make_shared<PbdObject>("Cube");
    colonObj->setCollidingGeometry(collidingColonMesh);
    colonObj->setVisualGeometry(visualColonMesh);
    colonObj->setPhysicsGeometry(cutTissueMesh);
    colonObj->setPhysicsToCollidingMap(colonMapP2C);
    colonObj->setPhysicsToVisualMap(colonMapP2V);
    colonObj->setCollidingToVisualMap(colonMapC2V);
    //sceneTest->addSceneObject(colonObj);

    // start cutting
    vector<Vec3d> m_vBladeSegments;
    vector<Vec3d> m_vSweptQuad;
    Vec3d temp1, temp2;
    //temp1.x = -2.7982251644134521; temp1.y = 1.2786393165588379; temp1.z = -2.44757509231567;
    //temp2.x = 3.9769670963287354; temp2.y = 1.2786393165588379; temp2.z = -2.44757509231567;
    temp1.x() = -2.8; temp1.y() = 1.3; temp1.z() = -2.5;
    temp2.x() = 4.0; temp2.y() = 1.3; temp2.z() = -2.5;
    m_vBladeSegments.push_back(temp1);
    m_vBladeSegments.push_back(temp2);

    Vec3d temp3, temp4;
    //temp1.x = -2.7982251644134521; temp1.y = 1.2786393165588379; temp1.z = 1.0033550262451172;
    //temp2.x = -2.7982251644134521; temp2.y = 1.2786393165588379; temp2.z = -2.44757509231567;
    //temp3.x = 3.9769670963287354; temp3.y = 1.2786393165588379; temp3.z = 1.0033550262451172;
    //temp4.x = 3.9769670963287354; temp4.y = 1.2786393165588379; temp4.z = -2.44757509231567;
    //temp1.x() = -45; temp1.y() = -11; temp1.z() = 27;  // 2
    //temp2.x() = -45; temp2.y() = -11; temp2.z() = -35; // 1
    //temp3.x() = 17; temp3.y() = 51; temp3.z() = 27; // 3
    //temp4.x() = 17; temp4.y() = 51; temp4.z() = -35; // 4  // completed cutting
    //temp1.x() = -45; temp1.y() = -11; temp1.z() = 13;  // 2
    //temp2.x() = -45; temp2.y() = -11; temp2.z() = -35; // 1
    //temp3.x() = 17; temp3.y() = 51; temp3.z() = 13; // 3
    //temp4.x() = 17; temp4.y() = 51; temp4.z() = -35; // 4  // partial cutting 
    temp1.x() = 4; temp1.y() = -11; temp1.z() = 13;  // 2
    temp2.x() = 4; temp2.y() = -11; temp2.z() = -35; // 1
    temp3.x() = 4; temp3.y() = 51; temp3.z() = 13; // 3
    temp4.x() = 4; temp4.y() = 51; temp4.z() = -35; // 4  // partial cutting straight
    m_vSweptQuad.push_back(temp1);
    m_vSweptQuad.push_back(temp2);
    m_vSweptQuad.push_back(temp3);
    m_vSweptQuad.push_back(temp4);
    imstk::StdVectorOfVec3d vertListSwept;
    std::vector<imstk::SurfaceMesh::TriangleArray> trianglesSwept;
    vertListSwept.push_back(temp1);
    vertListSwept.push_back(temp2);
    vertListSwept.push_back(temp3);
    vertListSwept.push_back(temp4);
    trianglesSwept.push_back({ { 0, 1, 2 } });
    trianglesSwept.push_back({ { 1, 2, 3 } });
    auto surfMeshSwept = std::make_shared<imstk::SurfaceMesh>();
    surfMeshSwept->initialize(vertListSwept, trianglesSwept);

    int res = cutTissueMesh->cut(m_vBladeSegments, m_vSweptQuad, true);

    // c. Extract the surface mesh
    auto surfMesh = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMesh = cutTissueMesh->getSurfaceMesh();
    surfMesh->computeVertexNormals();

    auto surfMeshVolume = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMeshVolume = cutTissueMesh->getSurfaceMeshVolume();
    surfMeshVolume->computeVertexNormals();

    //auto surfMeshFront = std::make_shared<imstk::SurfaceMesh>();
    ////cutTissueMesh->extractSurfaceMesh(surfMesh);
    //surfMeshFront = cutTissueMesh->getSurfaceCutFrontMesh();
    //surfMeshFront->computeVertexNormals();

    auto surfMeshFrontNew = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMeshFrontNew = cutTissueMesh->getSurfaceCutFrontMeshNew();
    surfMeshFrontNew->computeVertexNormals();

    auto surfMeshFrontVolume = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMeshFrontVolume = cutTissueMesh->getSurfaceCutFrontMeshVolume();
    surfMeshFrontVolume->computeVertexNormals();

    //auto surfMeshBack = std::make_shared<imstk::SurfaceMesh>();
    //surfMeshBack = cutTissueMesh->getSurfaceCutBackMesh();
    //surfMeshBack->computeVertexNormals();

    auto surfMeshBackNew = std::make_shared<imstk::SurfaceMesh>();
    surfMeshBackNew = cutTissueMesh->getSurfaceCutBackMeshNew();
    surfMeshBackNew->computeVertexNormals();

    auto surfMeshBackVolume = std::make_shared<imstk::SurfaceMesh>();
    surfMeshBackVolume = cutTissueMesh->getSurfaceCutBackMeshVolume();
    surfMeshBackVolume->computeVertexNormals();

    auto object0 = std::make_shared<imstk::VisualObject>("visualColonMesh0");
    auto material0 = std::make_shared<RenderMaterial>();
    material0->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME SURFACE
    auto tmp0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    material0->addTexture(tmp0);
    surfMeshSwept->setRenderMaterial(material0);
    object0->setVisualGeometry(surfMeshSwept); // change to any mesh created above
    //sceneTest->addSceneObject(object0);

    auto objectLink = std::make_shared<imstk::VisualObject>("visualColonMeshLink");
    auto materialLink = std::make_shared<RenderMaterial>();
    materialLink->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME SURFACE
    auto tmpLink = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    materialLink->addTexture(tmpLink);
    surfMeshLink->setRenderMaterial(materialLink);
    objectLink->setVisualGeometry(surfMeshLink); // change to any mesh created above
    sceneTest->addSceneObject(objectLink);

    auto objectVisual = std::make_shared<imstk::VisualObject>("visualColonMeshVisual");
    auto materialVisual = std::make_shared<RenderMaterial>();
    materialVisual->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME SURFACE
    auto tmpVisual = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    materialVisual->addTexture(tmpVisual);
    visualColonMesh->setRenderMaterial(materialVisual);
    objectVisual->setVisualGeometry(visualColonMesh); // change to any mesh created above
    sceneTest->addSceneObject(objectVisual);

    auto object = std::make_shared<imstk::VisualObject>("visualColonMesh");
    auto material = std::make_shared<RenderMaterial>();
    material->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME); // WIREFRAME SURFACE
    auto tmp = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    material->addTexture(tmp);
    surfMesh->setRenderMaterial(material);
    object->setVisualGeometry(surfMesh); // change to any mesh created above
    //sceneTest->addSceneObject(object);

    auto objectVolume = std::make_shared<imstk::VisualObject>("visualColonMeshVolume");
    auto materialVolume = std::make_shared<RenderMaterial>();
    materialVolume->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME); // WIREFRAME SURFACE
    auto tmpVolume = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    materialVolume->addTexture(tmpVolume);
    surfMeshVolume->setRenderMaterial(materialVolume);
    objectVolume->setVisualGeometry(surfMeshVolume); // change to any mesh created above
    //sceneTest->addSceneObject(objectVolume);

    //auto object4 = std::make_shared<imstk::VisualObject>("visualColonMesh4");
    //auto material4 = std::make_shared<RenderMaterial>();
    //material4->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME
    //auto tmp4 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    //material4->addTexture(tmp4);
    //surfMeshFront->setRenderMaterial(material4);
    //object4->setVisualGeometry(surfMeshFront); // change to any mesh created above
    ////sceneTest->addSceneObject(object4);

    auto object5 = std::make_shared<imstk::VisualObject>("visualColonMesh5");
    auto material5 = std::make_shared<RenderMaterial>();
    material5->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME
    auto tmp5 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/submucosal/Submucosal_layer_BaseColor.png", Texture::Type::DIFFUSE);
    material5->addTexture(tmp5);
    surfMeshFrontNew->setRenderMaterial(material5);
    object5->setVisualGeometry(surfMeshFrontNew); // change to any mesh created above
    //sceneTest->addSceneObject(object5);

    auto object6 = std::make_shared<imstk::VisualObject>("visualColonMesh6");
    auto material6 = std::make_shared<RenderMaterial>();
    material6->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME); // WIREFRAME
    auto tmp6 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/submucosal/Submucosal_layer_BaseColor.png", Texture::Type::DIFFUSE);
    material6->addTexture(tmp6);
    surfMeshFrontVolume->setRenderMaterial(material6);
    object6->setVisualGeometry(surfMeshFrontVolume); // change to any mesh created above
    sceneTest->addSceneObject(object6);

    //auto object3 = std::make_shared<imstk::VisualObject>("visualColonMesh3");
    //auto material3 = std::make_shared<RenderMaterial>();
    //material3->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    //auto tmp3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    //material3->addTexture(tmp3);
    //surfMeshBack->setRenderMaterial(material3);
    //object3->setVisualGeometry(surfMeshBack); // change to any mesh created above
    ////sceneTest->addSceneObject(object3);

    auto object2 = std::make_shared<imstk::VisualObject>("visualColonMesh2");
    auto material2 = std::make_shared<RenderMaterial>();
    material2->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE);
    auto tmp2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/submucosal/Submucosal_layer_BaseColor.png", Texture::Type::DIFFUSE);
    material2->addTexture(tmp2);
    surfMeshBackNew->setRenderMaterial(material2);
    object2->setVisualGeometry(surfMeshBackNew); // change to any mesh created above
    //sceneTest->addSceneObject(object2);

    auto object22 = std::make_shared<imstk::VisualObject>("visualColonMesh22");
    auto material22 = std::make_shared<RenderMaterial>();
    material22->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmp22 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/submucosal/Submucosal_layer_BaseColor.png", Texture::Type::DIFFUSE);
    material22->addTexture(tmp22);
    surfMeshBackVolume->setRenderMaterial(material22);
    object22->setVisualGeometry(surfMeshBackVolume); // change to any mesh created above
    sceneTest->addSceneObject(object22);

    // Light (white)
    auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
    whiteLight->setPosition(imstk::Vec3d(-10, 20, -50));
    //whiteLight->setFocalPoint(imstk::Vec3d(1, -1, 0));
    whiteLight->setIntensity(10000);
    sceneTest->addLight(whiteLight);
    auto whiteLight2 = std::make_shared<imstk::PointLight>("whiteLight2");
    whiteLight2->setPosition(imstk::Vec3d(-10, 20, 40));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight2->setIntensity(10000);
    sceneTest->addLight(whiteLight2);
    auto whiteLight3 = std::make_shared<imstk::PointLight>("whiteLight3");
    whiteLight3->setPosition(imstk::Vec3d(-60, 20, -5));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight3->setIntensity(10000);
    sceneTest->addLight(whiteLight3);
    auto whiteLight4 = std::make_shared<imstk::PointLight>("whiteLight4");
    whiteLight4->setPosition(imstk::Vec3d(35, 20, -5));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight4->setIntensity(10000);
    sceneTest->addLight(whiteLight4);
    auto whiteLight5 = std::make_shared<imstk::PointLight>("whiteLight5");
    whiteLight5->setPosition(imstk::Vec3d(-10, -25, -5));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight5->setIntensity(10000);
    sceneTest->addLight(whiteLight5);
    auto whiteLight6 = std::make_shared<imstk::PointLight>("whiteLight6");
    whiteLight6->setPosition(imstk::Vec3d(-10, 65, -5));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight6->setIntensity(10000);
    sceneTest->addLight(whiteLight6);

    // Light (red)
    auto colorLight = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight->setPosition(imstk::Vec3d(-10, 20, 50)); // - 25, -8, -4.5
    colorLight->setFocalPoint(imstk::Vec3d(-10, 20, -25)); // - 3.7, 5.5, 7
    colorLight->setColor(imstk::Color::White); // red
    colorLight->setSpotAngle(70);
    colorLight->setIntensity(10000);
    auto colorLight2 = std::make_shared<imstk::SpotLight>("colorLight2");
    colorLight2->setPosition(imstk::Vec3d(-10, 20, -60));
    colorLight2->setFocalPoint(imstk::Vec3d(-10, 20, 15));
    colorLight2->setColor(imstk::Color::White);
    colorLight2->setSpotAngle(70);
    colorLight2->setIntensity(10000);
    auto colorLight3 = std::make_shared<imstk::SpotLight>("colorLight3");
    colorLight3->setPosition(imstk::Vec3d(-2.5, 7, -23));  //-25, -5, -2.5
    colorLight3->setFocalPoint(imstk::Vec3d(-2.5, 17, -23));  // -3.7, 5.5, 7
    colorLight3->setColor(imstk::Color::White); // red
    colorLight3->setSpotAngle(150);
    colorLight3->setIntensity(2000);
    sceneTest->addLight(colorLight);
    sceneTest->addLight(colorLight2);
    //sceneTest->addLight(colorLight3);

    // Update Camera
    auto cam1 = sceneTest->getCamera();
    cam1->setPosition(imstk::Vec3d(0, 0, 1));
    cam1->setFocalPoint(imstk::Vec3d(0, 0, 0));

    // Run
    sdk->setActiveScene(sceneTest);
    sdk->startSimulation(true);
}

void testCuttingNotComplete3()
{
    // SDK and Scene
    auto sdk = std::make_shared<imstk::SimulationManager>();
    auto sceneTest = sdk->createNewScene("SceneTest");

    //// Tetrahedral mesh
    imstk::StdVectorOfVec3d vertListTet;
    std::vector<imstk::TetrahedralMesh::TetraArray> tetConnectivity;
    //vector< vec3d > vertices;
    //vector< vec4u32 > elements;

    int nx = 8, ny = 8, nz = 8; // 4
    double cellsize = 0.5; // 1.0
    Vec3d start = Vec3d(-(double)(nx) / 2.0, 0, -(double)(nz) / 2.0) * (cellsize);

    //add all nodes
    for (int i = 0; i < nx; i++)
    {
        for (int j = 0; j < ny; j++)
        {
            for (int k = 0; k < nz; k++)
            {
                Vec3d v = start + Vec3d(i, j, k) * cellsize;
                vertListTet.push_back(v);
            }
        }
    }

    //corner defs
    enum CellCorners { LBN = 0, LBF = 1, LTN = 2, LTF = 3, RBN = 4, RBF = 5, RTN = 6, RTF = 7 };
    U32 cn[8];
    //imstk::TetrahedralMesh::TetraArray tet1;
    //add all elements
    for (int i = 0; i < nx - 1; i++)
    {
        for (int j = 0; j < ny - 1; j++)
        {
            for (int k = 0; k < nz - 1; k++)
            {
                //collect cell nodes
                cn[LBN] = i * ny * nz + j * nz + k;
                cn[LBF] = i * ny * nz + j * nz + k + 1;

                cn[LTN] = i * ny * nz + (j + 1) * nz + k;
                cn[LTF] = i * ny * nz + (j + 1) * nz + k + 1;

                cn[RBN] = (i + 1) * ny * nz + j * nz + k;
                cn[RBF] = (i + 1) * ny * nz + j * nz + k + 1;

                cn[RTN] = (i + 1) * ny * nz + (j + 1) * nz + k;
                cn[RTF] = (i + 1) * ny * nz + (j + 1) * nz + k + 1;

                //add elements
                imstk::TetrahedralMesh::TetraArray tet1 = { cn[LBN], cn[LTN], cn[RBN], cn[LBF] };
                tetConnectivity.push_back(tet1);
                imstk::TetrahedralMesh::TetraArray tet2 = { cn[RTN], cn[LTN], cn[LBF], cn[RBN] };
                tetConnectivity.push_back(tet2);
                imstk::TetrahedralMesh::TetraArray tet3 = { cn[RTN], cn[LTN], cn[LTF], cn[LBF] };
                tetConnectivity.push_back(tet3);
                imstk::TetrahedralMesh::TetraArray tet4 = { cn[RTN], cn[RBN], cn[LBF], cn[RBF] };
                tetConnectivity.push_back(tet4);
                imstk::TetrahedralMesh::TetraArray tet5 = { cn[RTN], cn[LBF], cn[LTF], cn[RBF] };
                tetConnectivity.push_back(tet5);
                imstk::TetrahedralMesh::TetraArray tet6 = { cn[RTN], cn[LTF], cn[RTF], cn[RBF] };
                tetConnectivity.push_back(tet6);
            }
        }
    }

    //build the final mesh
    //vector<double> vFlatVertices;
    //vector<U32> vFlatElements;
    //FlattenVec3<double>(vertices, vFlatVertices);
    //FlattenVec4<U32>(elements, vFlatElements);

    //physXLink linkVolSur;
    std::vector<physXLink> linksVolSur;
    //bool flag = loadTetFile(DATA_ROOT_PATH"/VESS/colonrectum_s002_SB.tet", vertListTet, tetConnectivity, linksVolSur); // colon_tumor3_3_SB for test   colon_tumor3_SB  cubeTest2_SB
    auto cutTissueMesh = std::make_shared<imstk::CuttingVESSCutMesh>();
    cutTissueMesh->setInitialVertexPositions(vertListTet);
    cutTissueMesh->setVertexPositions(vertListTet);
    cutTissueMesh->setTetrahedraVertices(tetConnectivity);

    cutTissueMesh->initializeCuttingMesh();
    cutTissueMesh->initializeCutting();
    cutTissueMesh->extractSurfaceMesh();
    //auto cutTissueMesh = std::make_shared<imstk::CuttingVESSCutMesh>(tetMesh);
    cutTissueMesh->setFlagSplitMeshAfterCut(true);

    Vec3d min1, max1;
    cutTissueMesh->computeBoundingBox(min1, max1);

    // start cutting
    vector<Vec3d> m_vBladeSegments;
    vector<Vec3d> m_vSweptQuad;
    Vec3d temp1, temp2;
    //temp1.x = -2.7982251644134521; temp1.y = 1.2786393165588379; temp1.z = -2.44757509231567;
    //temp2.x = 3.9769670963287354; temp2.y = 1.2786393165588379; temp2.z = -2.44757509231567;
    temp1.x() = -2.8; temp1.y() = 1.3; temp1.z() = -2.5;
    temp2.x() = 4.0; temp2.y() = 1.3; temp2.z() = -2.5;
    m_vBladeSegments.push_back(temp1);
    m_vBladeSegments.push_back(temp2);

    Vec3d temp3, temp4;
    //temp1.x = -2.7982251644134521; temp1.y = 1.2786393165588379; temp1.z = 1.0033550262451172;
    //temp2.x = -2.7982251644134521; temp2.y = 1.2786393165588379; temp2.z = -2.44757509231567;
    //temp3.x = 3.9769670963287354; temp3.y = 1.2786393165588379; temp3.z = 1.0033550262451172;
    //temp4.x = 3.9769670963287354; temp4.y = 1.2786393165588379; temp4.z = -2.44757509231567;
    temp1.x() = -2.8; temp1.y() = 0.1; temp1.z() = 1.0;  // 2
    temp2.x() = -2.8; temp2.y() = 0.1; temp2.z() = -2.5; // 1
    temp3.x() = -0.76; temp3.y() = 0.46; temp3.z() = 1.0; // 3
    temp4.x() = -0.76; temp4.y() = 0.46; temp4.z() = -2.5; // 4
    m_vSweptQuad.push_back(temp1);
    m_vSweptQuad.push_back(temp2);
    m_vSweptQuad.push_back(temp3);
    m_vSweptQuad.push_back(temp4);
    imstk::StdVectorOfVec3d vertListSwept;
    std::vector<imstk::SurfaceMesh::TriangleArray> trianglesSwept;
    vertListSwept.push_back(temp1);
    vertListSwept.push_back(temp2);
    vertListSwept.push_back(temp3);
    vertListSwept.push_back(temp4);
    trianglesSwept.push_back({ { 0, 1, 2 } });
    trianglesSwept.push_back({ { 1, 2, 3 } });
    auto surfMeshSwept = std::make_shared<imstk::SurfaceMesh>();
    surfMeshSwept->initialize(vertListSwept, trianglesSwept);

    int res = cutTissueMesh->cut(m_vBladeSegments, m_vSweptQuad, true);

    // c. Extract the surface mesh
    auto surfMesh = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMesh = cutTissueMesh->getSurfaceMesh();
    surfMesh->computeVertexNormals();

    auto surfMeshFront = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMeshFront = cutTissueMesh->getSurfaceCutFrontMesh();
    surfMeshFront->computeVertexNormals();

    auto surfMeshFrontNew = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMeshFrontNew = cutTissueMesh->getSurfaceCutFrontMeshNew();
    surfMeshFrontNew->computeVertexNormals();

    auto surfMeshFrontVolume = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMeshFrontVolume = cutTissueMesh->getSurfaceCutFrontMeshVolume();
    surfMeshFrontVolume->computeVertexNormals();

    auto surfMeshBack = std::make_shared<imstk::SurfaceMesh>();
    surfMeshBack = cutTissueMesh->getSurfaceCutBackMesh();
    surfMeshBack->computeVertexNormals();

    auto surfMeshBackNew = std::make_shared<imstk::SurfaceMesh>();
    surfMeshBackNew = cutTissueMesh->getSurfaceCutBackMeshNew();
    surfMeshBackNew->computeVertexNormals();

    auto surfMeshBackVolume = std::make_shared<imstk::SurfaceMesh>();
    surfMeshBackVolume = cutTissueMesh->getSurfaceCutBackMeshVolume();
    surfMeshBackVolume->computeVertexNormals();


    //auto object0 = std::make_shared<imstk::VisualObject>("visualColonMesh0");
    //auto material0 = std::make_shared<RenderMaterial>();
    //material0->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME SURFACE
    //auto tmp0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    //material0->addTexture(tmp0);
    //surfMeshSwept->setRenderMaterial(material0);
    //object0->setVisualGeometry(surfMeshSwept); // change to any mesh created above
    //sceneTest->addSceneObject(object0);

    //auto object = std::make_shared<imstk::VisualObject>("visualColonMesh");
    //auto material = std::make_shared<RenderMaterial>();
    //material->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME); // WIREFRAME SURFACE
    //auto tmp = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    //material->addTexture(tmp);
    //surfMesh->setRenderMaterial(material);
    //object->setVisualGeometry(surfMesh); // change to any mesh created above
    //sceneTest->addSceneObject(object);

    //auto object4 = std::make_shared<imstk::VisualObject>("visualColonMesh4");
    //auto material4 = std::make_shared<RenderMaterial>();
    //material4->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME
    //auto tmp4 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    //material4->addTexture(tmp4);
    //surfMeshFront->setRenderMaterial(material4);
    //object4->setVisualGeometry(surfMeshFront); // change to any mesh created above
    //sceneTest->addSceneObject(object4);

    auto object5 = std::make_shared<imstk::VisualObject>("visualColonMesh5");
    auto material5 = std::make_shared<RenderMaterial>();
    material5->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME
    auto tmp5 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/submucosal/Submucosal_layer_BaseColor.png", Texture::Type::DIFFUSE);
    material5->addTexture(tmp5);
    surfMeshFrontNew->setRenderMaterial(material5);
    object5->setVisualGeometry(surfMeshFrontNew); // change to any mesh created above
    sceneTest->addSceneObject(object5);

    auto object6 = std::make_shared<imstk::VisualObject>("visualColonMesh6");
    auto material6 = std::make_shared<RenderMaterial>();
    material6->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME); // WIREFRAME
    auto tmp6 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/submucosal/Submucosal_layer_BaseColor.png", Texture::Type::DIFFUSE);
    material6->addTexture(tmp6);
    surfMeshFrontVolume->setRenderMaterial(material6);
    object6->setVisualGeometry(surfMeshFrontVolume); // change to any mesh created above
    sceneTest->addSceneObject(object6);

    //auto object3 = std::make_shared<imstk::VisualObject>("visualColonMesh3");
    //auto material3 = std::make_shared<RenderMaterial>();
    //material3->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    //auto tmp3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    //material3->addTexture(tmp3);
    //surfMeshBack->setRenderMaterial(material3);
    //object3->setVisualGeometry(surfMeshBack); // change to any mesh created above
    //sceneTest->addSceneObject(object3);

    auto object2 = std::make_shared<imstk::VisualObject>("visualColonMesh2");
    auto material2 = std::make_shared<RenderMaterial>();
    material2->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE);
    auto tmp2 = std::make_shared<Texture>(DATA_ROOT_PATH"/textures/test4.png", Texture::Type::DIFFUSE);
    material2->addTexture(tmp2);
    surfMeshBackNew->setRenderMaterial(material2);
    object2->setVisualGeometry(surfMeshBackNew); // change to any mesh created above
    sceneTest->addSceneObject(object2);

    auto object22 = std::make_shared<imstk::VisualObject>("visualColonMesh22");
    auto material22 = std::make_shared<RenderMaterial>();
    material22->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmp22 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/submucosal/Submucosal_layer_BaseColor.png", Texture::Type::DIFFUSE);
    material22->addTexture(tmp22);
    surfMeshBackVolume->setRenderMaterial(material22);
    object22->setVisualGeometry(surfMeshBackVolume); // change to any mesh created above
    sceneTest->addSceneObject(object22);

    // Light (white)
    auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
    whiteLight->setPosition(imstk::Vec3d(-15, -1, 0));
    whiteLight->setFocalPoint(imstk::Vec3d(1, -1, 0));
    sceneTest->addLight(whiteLight);
    auto whiteLight2 = std::make_shared<imstk::PointLight>("whiteLight2");
    whiteLight2->setPosition(imstk::Vec3d(1, -1, 0));
    whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    sceneTest->addLight(whiteLight2);

    // Light (red)
    auto colorLight = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight->setPosition(imstk::Vec3d(-3.5, 2, -28)); // - 25, -8, -4.5
    colorLight->setFocalPoint(imstk::Vec3d(-1.5, 22, -18)); // - 3.7, 5.5, 7
    colorLight->setColor(imstk::Color::White); // red
    colorLight->setSpotAngle(15);
    auto colorLight2 = std::make_shared<imstk::SpotLight>("colorLight2");
    colorLight2->setPosition(imstk::Vec3d(-2.5, 17, -23));
    colorLight2->setFocalPoint(imstk::Vec3d(-2.5, 7, -23));
    colorLight2->setColor(imstk::Color::White);
    colorLight2->setSpotAngle(15);
    auto colorLight3 = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight3->setPosition(imstk::Vec3d(-2.5, 7, -23));  //-25, -5, -2.5
    colorLight3->setFocalPoint(imstk::Vec3d(-2.5, 17, -23));  // -3.7, 5.5, 7
    colorLight3->setColor(imstk::Color::White); // red
    colorLight3->setSpotAngle(15);
    sceneTest->addLight(colorLight);
    sceneTest->addLight(colorLight2);
    sceneTest->addLight(colorLight3);

    // Update Camera
    auto cam1 = sceneTest->getCamera();
    cam1->setPosition(imstk::Vec3d(0, 0, 1));
    cam1->setFocalPoint(imstk::Vec3d(0, 0, 0));

    // Run
    sdk->setActiveScene(sceneTest);
    sdk->startSimulation(true);
}

void testCuttingNotComplete2()
{
    // SDK and Scene
    auto sdk = std::make_shared<imstk::SimulationManager>();
    auto sceneTest = sdk->createNewScene("SceneTest");

    //// Tetrahedral mesh
    imstk::StdVectorOfVec3d vertListTet;
    std::vector<imstk::TetrahedralMesh::TetraArray> tetConnectivity;
    //vector< vec3d > vertices;
    //vector< vec4u32 > elements;

    int nx = 4, ny = 4, nz = 4; // 2 4 8
    double cellsize = 1.0; // 2.0 1.0 0.5
    Vec3d start = Vec3d(-(double)(nx) / 2.0, 0, -(double)(nz) / 2.0) * (cellsize);

    //add all nodes
    for (int i = 0; i < nx; i++)
    {
        for (int j = 0; j < ny; j++)
        {
            for (int k = 0; k < nz; k++)
            {
                Vec3d v = start + Vec3d(i, j, k) * cellsize;
                vertListTet.push_back(v);
            }
        }
    }

    //corner defs
    enum CellCorners { LBN = 0, LBF = 1, LTN = 2, LTF = 3, RBN = 4, RBF = 5, RTN = 6, RTF = 7 };
    U32 cn[8];
    //imstk::TetrahedralMesh::TetraArray tet1;
    //add all elements
    for (int i = 0; i < nx - 1; i++)
    {
        for (int j = 0; j < ny - 1; j++)
        {
            for (int k = 0; k < nz - 1; k++)
            {
                //collect cell nodes
                cn[LBN] = i * ny * nz + j * nz + k;
                cn[LBF] = i * ny * nz + j * nz + k + 1;

                cn[LTN] = i * ny * nz + (j + 1) * nz + k;
                cn[LTF] = i * ny * nz + (j + 1) * nz + k + 1;

                cn[RBN] = (i + 1) * ny * nz + j * nz + k;
                cn[RBF] = (i + 1) * ny * nz + j * nz + k + 1;

                cn[RTN] = (i + 1) * ny * nz + (j + 1) * nz + k;
                cn[RTF] = (i + 1) * ny * nz + (j + 1) * nz + k + 1;

                //add elements
                imstk::TetrahedralMesh::TetraArray tet1 = { cn[LBN], cn[LTN], cn[RBN], cn[LBF] };
                tetConnectivity.push_back(tet1);
                imstk::TetrahedralMesh::TetraArray tet2 = { cn[RTN], cn[LTN], cn[LBF], cn[RBN] };
                tetConnectivity.push_back(tet2);
                imstk::TetrahedralMesh::TetraArray tet3 = { cn[RTN], cn[LTN], cn[LTF], cn[LBF] };
                tetConnectivity.push_back(tet3);
                imstk::TetrahedralMesh::TetraArray tet4 = { cn[RTN], cn[RBN], cn[LBF], cn[RBF] };
                tetConnectivity.push_back(tet4);
                imstk::TetrahedralMesh::TetraArray tet5 = { cn[RTN], cn[LBF], cn[LTF], cn[RBF] };
                tetConnectivity.push_back(tet5);
                imstk::TetrahedralMesh::TetraArray tet6 = { cn[RTN], cn[LTF], cn[RTF], cn[RBF] };
                tetConnectivity.push_back(tet6);
            }
        }
    }

    //build the final mesh
    //vector<double> vFlatVertices;
    //vector<U32> vFlatElements;
    //FlattenVec3<double>(vertices, vFlatVertices);
    //FlattenVec4<U32>(elements, vFlatElements);

    //physXLink linkVolSur;
    std::vector<physXLink> linksVolSur;
    //bool flag = loadTetFile(DATA_ROOT_PATH"/VESS/colonrectum_s002_SB.tet", vertListTet, tetConnectivity, linksVolSur); // colon_tumor3_3_SB for test   colon_tumor3_SB  cubeTest2_SB
    auto cutTissueMesh = std::make_shared<imstk::CuttingVESSCutMesh>();
    cutTissueMesh->setInitialVertexPositions(vertListTet);
    cutTissueMesh->setVertexPositions(vertListTet);
    cutTissueMesh->setTetrahedraVertices(tetConnectivity);

    cutTissueMesh->initializeCuttingMesh();
    cutTissueMesh->initializeCutting();
    cutTissueMesh->extractSurfaceMesh();
    cutTissueMesh->extractSurfaceMeshVolume();
    //auto cutTissueMesh = std::make_shared<imstk::CuttingVESSCutMesh>(tetMesh);
    cutTissueMesh->setFlagSplitMeshAfterCut(true);

    Vec3d min1, max1;
    cutTissueMesh->computeBoundingBox(min1, max1);

    // start cutting
    vector<Vec3d> m_vBladeSegments;
    vector<Vec3d> m_vSweptQuad;
    Vec3d temp1, temp2;
    //temp1.x = -2.7982251644134521; temp1.y = 1.2786393165588379; temp1.z = -2.44757509231567;
    //temp2.x = 3.9769670963287354; temp2.y = 1.2786393165588379; temp2.z = -2.44757509231567;
    temp1.x() = -2.8; temp1.y() = 1.3; temp1.z() = -2.5;
    temp2.x() = 4.0; temp2.y() = 1.3; temp2.z() = -2.5;
    m_vBladeSegments.push_back(temp1);
    m_vBladeSegments.push_back(temp2);

    Vec3d temp3, temp4;
    //temp1.x = -2.7982251644134521; temp1.y = 1.2786393165588379; temp1.z = 1.0033550262451172;
    //temp2.x = -2.7982251644134521; temp2.y = 1.2786393165588379; temp2.z = -2.44757509231567;
    //temp3.x = 3.9769670963287354; temp3.y = 1.2786393165588379; temp3.z = 1.0033550262451172;
    //temp4.x = 3.9769670963287354; temp4.y = 1.2786393165588379; temp4.z = -2.44757509231567;
    temp1.x() = -2.8; temp1.y() = 0.1; temp1.z() = 1.0;  // 2
    temp2.x() = -2.8; temp2.y() = 0.1; temp2.z() = -2.5; // 1
    temp3.x() = -0.76; temp3.y() = 0.46; temp3.z() = 1.0; // 3
    temp4.x() = -0.76; temp4.y() = 0.46; temp4.z() = -2.5; // 4
    m_vSweptQuad.push_back(temp1);
    m_vSweptQuad.push_back(temp2);
    m_vSweptQuad.push_back(temp3);
    m_vSweptQuad.push_back(temp4);
    imstk::StdVectorOfVec3d vertListSwept;
    std::vector<imstk::SurfaceMesh::TriangleArray> trianglesSwept;
    vertListSwept.push_back(temp1);
    vertListSwept.push_back(temp2);
    vertListSwept.push_back(temp3);
    vertListSwept.push_back(temp4);
    trianglesSwept.push_back({ { 0, 1, 2 } });
    trianglesSwept.push_back({ { 1, 2, 3 } });
    auto surfMeshSwept = std::make_shared<imstk::SurfaceMesh>();
    surfMeshSwept->initialize(vertListSwept, trianglesSwept);

    int res = cutTissueMesh->cut(m_vBladeSegments, m_vSweptQuad, true);

    // c. Extract the surface mesh
    auto surfMesh = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMesh = cutTissueMesh->getSurfaceMesh();
    surfMesh->computeVertexNormals();

    auto surfMeshVolume = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMeshVolume = cutTissueMesh->getSurfaceMeshVolume();
    surfMeshVolume->computeVertexNormals();

    auto surfMeshFront = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMeshFront = cutTissueMesh->getSurfaceCutFrontMesh();
    surfMeshFront->computeVertexNormals();

    auto surfMeshFrontNew = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMeshFrontNew = cutTissueMesh->getSurfaceCutFrontMeshNew();
    surfMeshFrontNew->computeVertexNormals();

    auto surfMeshFrontVolume = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMeshFrontVolume = cutTissueMesh->getSurfaceCutFrontMeshVolume();
    surfMeshFrontVolume->computeVertexNormals();

    auto surfMeshBack = std::make_shared<imstk::SurfaceMesh>();
    surfMeshBack = cutTissueMesh->getSurfaceCutBackMesh();
    surfMeshBack->computeVertexNormals();

    auto surfMeshBackNew = std::make_shared<imstk::SurfaceMesh>();
    surfMeshBackNew = cutTissueMesh->getSurfaceCutBackMeshNew();
    surfMeshBackNew->computeVertexNormals();

    auto surfMeshBackVolume = std::make_shared<imstk::SurfaceMesh>();
    surfMeshBackVolume = cutTissueMesh->getSurfaceCutBackMeshVolume();
    surfMeshBackVolume->computeVertexNormals();

    auto object0 = std::make_shared<imstk::VisualObject>("visualColonMesh0");
    auto material0 = std::make_shared<RenderMaterial>();
    material0->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME SURFACE
    auto tmp0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    material0->addTexture(tmp0);
    surfMeshSwept->setRenderMaterial(material0);
    object0->setVisualGeometry(surfMeshSwept); // change to any mesh created above
    //sceneTest->addSceneObject(object0);

    auto object = std::make_shared<imstk::VisualObject>("visualColonMesh");
    auto material = std::make_shared<RenderMaterial>();
    material->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME); // WIREFRAME SURFACE
    auto tmp = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    material->addTexture(tmp);
    surfMesh->setRenderMaterial(material);
    object->setVisualGeometry(surfMesh); // change to any mesh created above
    //sceneTest->addSceneObject(object);

    auto objectVolume = std::make_shared<imstk::VisualObject>("visualColonMeshVolume");
    auto materialVolume = std::make_shared<RenderMaterial>();
    materialVolume->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME); // WIREFRAME SURFACE
    auto tmpVolume = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    materialVolume->addTexture(tmpVolume);
    surfMeshVolume->setRenderMaterial(materialVolume);
    objectVolume->setVisualGeometry(surfMeshVolume); // change to any mesh created above
    sceneTest->addSceneObject(objectVolume);

    auto object4 = std::make_shared<imstk::VisualObject>("visualColonMesh4");
    auto material4 = std::make_shared<RenderMaterial>();
    material4->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME
    auto tmp4 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    material4->addTexture(tmp4);
    surfMeshFront->setRenderMaterial(material4);
    object4->setVisualGeometry(surfMeshFront); // change to any mesh created above
    //sceneTest->addSceneObject(object4);

    auto object5 = std::make_shared<imstk::VisualObject>("visualColonMesh5");
    auto material5 = std::make_shared<RenderMaterial>();
    material5->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME
    auto tmp5 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/submucosal/Submucosal_layer_BaseColor.png", Texture::Type::DIFFUSE);
    material5->addTexture(tmp5);
    surfMeshFrontNew->setRenderMaterial(material5);
    object5->setVisualGeometry(surfMeshFrontNew); // change to any mesh created above
    //sceneTest->addSceneObject(object5);

    auto object6 = std::make_shared<imstk::VisualObject>("visualColonMesh6");
    auto material6 = std::make_shared<RenderMaterial>();
    material6->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME
    auto tmp6 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/submucosal/Submucosal_layer_BaseColor.png", Texture::Type::DIFFUSE);
    material6->addTexture(tmp6);
    surfMeshFrontVolume->setRenderMaterial(material6);
    object6->setVisualGeometry(surfMeshFrontVolume); // change to any mesh created above
    //sceneTest->addSceneObject(object6);

    auto object3 = std::make_shared<imstk::VisualObject>("visualColonMesh3");
    auto material3 = std::make_shared<RenderMaterial>();
    material3->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmp3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    material3->addTexture(tmp3);
    surfMeshBack->setRenderMaterial(material3);
    object3->setVisualGeometry(surfMeshBack); // change to any mesh created above
    //sceneTest->addSceneObject(object3);

    auto object2 = std::make_shared<imstk::VisualObject>("visualColonMesh2");
    auto material2 = std::make_shared<RenderMaterial>();
    material2->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE);
    auto tmp2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/submucosal/Submucosal_layer_BaseColor.png", Texture::Type::DIFFUSE);
    material2->addTexture(tmp2);
    surfMeshBackNew->setRenderMaterial(material2);
    object2->setVisualGeometry(surfMeshBackNew); // change to any mesh created above
    sceneTest->addSceneObject(object2);

    auto object22 = std::make_shared<imstk::VisualObject>("visualColonMesh22");
    auto material22 = std::make_shared<RenderMaterial>();
    material22->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmp22 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/submucosal/Submucosal_layer_BaseColor.png", Texture::Type::DIFFUSE);
    material22->addTexture(tmp22);
    surfMeshBackVolume->setRenderMaterial(material22);
    object22->setVisualGeometry(surfMeshBackVolume); // change to any mesh created above
    sceneTest->addSceneObject(object22);

    // Light (white)
    auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
    whiteLight->setPosition(imstk::Vec3d(-15, -1, 0));
    whiteLight->setFocalPoint(imstk::Vec3d(1, -1, 0));
    sceneTest->addLight(whiteLight);
    whiteLight->setIntensity(10);
    auto whiteLight2 = std::make_shared<imstk::PointLight>("whiteLight2");
    whiteLight2->setPosition(imstk::Vec3d(1, -1, 0));
    whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    sceneTest->addLight(whiteLight2);

    // Light (red)
    auto colorLight = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight->setPosition(imstk::Vec3d(-3.5, 2, -28)); // - 25, -8, -4.5
    colorLight->setFocalPoint(imstk::Vec3d(-1.5, 22, -18)); // - 3.7, 5.5, 7
    colorLight->setColor(imstk::Color::White); // red
    colorLight->setSpotAngle(15);
    auto colorLight2 = std::make_shared<imstk::SpotLight>("colorLight2");
    colorLight2->setPosition(imstk::Vec3d(-2.5, 17, -23));
    colorLight2->setFocalPoint(imstk::Vec3d(-2.5, 7, -23));
    colorLight2->setColor(imstk::Color::White);
    colorLight2->setSpotAngle(15);
    auto colorLight3 = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight3->setPosition(imstk::Vec3d(-2.5, 7, -23));  //-25, -5, -2.5
    colorLight3->setFocalPoint(imstk::Vec3d(-2.5, 17, -23));  // -3.7, 5.5, 7
    colorLight3->setColor(imstk::Color::White); // red
    colorLight3->setSpotAngle(15);
    sceneTest->addLight(colorLight);
    sceneTest->addLight(colorLight2);
    sceneTest->addLight(colorLight3);

    // Update Camera
    auto cam1 = sceneTest->getCamera();
    cam1->setPosition(imstk::Vec3d(0, 0, 1));
    cam1->setFocalPoint(imstk::Vec3d(0, 0, 0));

    // Run
    sdk->setActiveScene(sceneTest);
    sdk->startSimulation(true);
}

void testCuttingNotComplete()
{
    // SDK and Scene
    auto sdk = std::make_shared<imstk::SimulationManager>();
    auto sceneTest = sdk->createNewScene("SceneTest");

    //// Tetrahedral mesh
    imstk::StdVectorOfVec3d vertListTet;
    std::vector<imstk::TetrahedralMesh::TetraArray> tetConnectivity;
    //vector< vec3d > vertices;
    //vector< vec4u32 > elements;

    int nx = 4, ny = 4, nz = 4;
    double cellsize = 1.0;
    Vec3d start = Vec3d(-(double)(nx) / 2.0, 0, -(double)(nz) / 2.0) * (cellsize);

    //add all nodes
    for (int i = 0; i < nx; i++)
    {
        for (int j = 0; j < ny; j++)
        {
            for (int k = 0; k < nz; k++)
            {
                Vec3d v = start + Vec3d(i, j, k) * cellsize;
                vertListTet.push_back(v);
            }
        }
    }

    //corner defs
    enum CellCorners { LBN = 0, LBF = 1, LTN = 2, LTF = 3, RBN = 4, RBF = 5, RTN = 6, RTF = 7 };
    U32 cn[8];
    //imstk::TetrahedralMesh::TetraArray tet1;
    //add all elements
    for (int i = 0; i < nx - 1; i++)
    {
        for (int j = 0; j < ny - 1; j++)
        {
            for (int k = 0; k < nz - 1; k++)
            {
                //collect cell nodes
                cn[LBN] = i * ny * nz + j * nz + k;
                cn[LBF] = i * ny * nz + j * nz + k + 1;

                cn[LTN] = i * ny * nz + (j + 1) * nz + k;
                cn[LTF] = i * ny * nz + (j + 1) * nz + k + 1;

                cn[RBN] = (i + 1) * ny * nz + j * nz + k;
                cn[RBF] = (i + 1) * ny * nz + j * nz + k + 1;

                cn[RTN] = (i + 1) * ny * nz + (j + 1) * nz + k;
                cn[RTF] = (i + 1) * ny * nz + (j + 1) * nz + k + 1;

                //add elements
                imstk::TetrahedralMesh::TetraArray tet1 = { cn[LBN], cn[LTN], cn[RBN], cn[LBF] };
                tetConnectivity.push_back(tet1);
                imstk::TetrahedralMesh::TetraArray tet2 = { cn[RTN], cn[LTN], cn[LBF], cn[RBN] };
                tetConnectivity.push_back(tet2);
                imstk::TetrahedralMesh::TetraArray tet3 = { cn[RTN], cn[LTN], cn[LTF], cn[LBF] };
                tetConnectivity.push_back(tet3);
                imstk::TetrahedralMesh::TetraArray tet4 = { cn[RTN], cn[RBN], cn[LBF], cn[RBF] };
                tetConnectivity.push_back(tet4);
                imstk::TetrahedralMesh::TetraArray tet5 = { cn[RTN], cn[LBF], cn[LTF], cn[RBF] };
                tetConnectivity.push_back(tet5);
                imstk::TetrahedralMesh::TetraArray tet6 = { cn[RTN], cn[LTF], cn[RTF], cn[RBF] };
                tetConnectivity.push_back(tet6);
            }
        }
    }

    //build the final mesh
    //vector<double> vFlatVertices;
    //vector<U32> vFlatElements;
    //FlattenVec3<double>(vertices, vFlatVertices);
    //FlattenVec4<U32>(elements, vFlatElements);

    //physXLink linkVolSur;
    std::vector<physXLink> linksVolSur;
    //bool flag = loadTetFile(DATA_ROOT_PATH"/VESS/colonrectum_s002_SB.tet", vertListTet, tetConnectivity, linksVolSur); // colon_tumor3_3_SB for test   colon_tumor3_SB  cubeTest2_SB
    auto cutTissueMesh = std::make_shared<imstk::CuttingVESSCutMesh>();
    cutTissueMesh->setInitialVertexPositions(vertListTet);
    cutTissueMesh->setVertexPositions(vertListTet);
    cutTissueMesh->setTetrahedraVertices(tetConnectivity);

    cutTissueMesh->initializeCuttingMesh();
    cutTissueMesh->initializeCutting();
    cutTissueMesh->extractSurfaceMesh();
    //auto cutTissueMesh = std::make_shared<imstk::CuttingVESSCutMesh>(tetMesh);
    cutTissueMesh->setFlagSplitMeshAfterCut(true);

    Vec3d min1, max1;
    cutTissueMesh->computeBoundingBox(min1, max1);

    // start cutting
    vector<Vec3d> m_vBladeSegments;
    vector<Vec3d> m_vSweptQuad;
    Vec3d temp1, temp2;
    //temp1.x = -2.7982251644134521; temp1.y = 1.2786393165588379; temp1.z = -2.44757509231567;
    //temp2.x = 3.9769670963287354; temp2.y = 1.2786393165588379; temp2.z = -2.44757509231567;
    temp1.x() = -2.8; temp1.y() = 1.3; temp1.z() = -2.5;
    temp2.x() = 4.0; temp2.y() = 1.3; temp2.z() = -2.5;
    m_vBladeSegments.push_back(temp1);
    m_vBladeSegments.push_back(temp2);

    Vec3d temp3, temp4;
    //temp1.x = -2.7982251644134521; temp1.y = 1.2786393165588379; temp1.z = 1.0033550262451172;
    //temp2.x = -2.7982251644134521; temp2.y = 1.2786393165588379; temp2.z = -2.44757509231567;
    //temp3.x = 3.9769670963287354; temp3.y = 1.2786393165588379; temp3.z = 1.0033550262451172;
    //temp4.x = 3.9769670963287354; temp4.y = 1.2786393165588379; temp4.z = -2.44757509231567;
    temp1.x() = -2.8; temp1.y() = 0.1; temp1.z() = 1.0;  // 2
    temp2.x() = -2.8; temp2.y() = 0.1; temp2.z() = -2.5; // 1
    temp3.x() = -0.76; temp3.y() = 0.46; temp3.z() = 1.0; // 3
    temp4.x() = -0.76; temp4.y() = 0.46; temp4.z() = -2.5; // 4
    m_vSweptQuad.push_back(temp1);
    m_vSweptQuad.push_back(temp2);
    m_vSweptQuad.push_back(temp3);
    m_vSweptQuad.push_back(temp4);
    imstk::StdVectorOfVec3d vertListSwept;
    std::vector<imstk::SurfaceMesh::TriangleArray> trianglesSwept;
    vertListSwept.push_back(temp1);
    vertListSwept.push_back(temp2);
    vertListSwept.push_back(temp3);
    vertListSwept.push_back(temp4);
    trianglesSwept.push_back({ { 0, 1, 2 } });
    trianglesSwept.push_back({ { 1, 2, 3 } });
    auto surfMeshSwept = std::make_shared<imstk::SurfaceMesh>();
    surfMeshSwept->initialize(vertListSwept, trianglesSwept);

    int res = cutTissueMesh->cut(m_vBladeSegments, m_vSweptQuad, true);

    // c. Extract the surface mesh
    auto surfMesh = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMesh = cutTissueMesh->getSurfaceMesh();
    surfMesh->computeVertexNormals();

    auto surfMeshFront = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMeshFront = cutTissueMesh->getSurfaceCutFrontMesh();
    surfMeshFront->computeVertexNormals();

    auto surfMeshFrontNew = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMeshFrontNew = cutTissueMesh->getSurfaceCutFrontMeshNew();
    surfMeshFrontNew->computeVertexNormals();

    auto surfMeshBack = std::make_shared<imstk::SurfaceMesh>();
    surfMeshBack = cutTissueMesh->getSurfaceCutBackMesh();
    surfMeshBack->computeVertexNormals();

    auto surfMeshBackNew = std::make_shared<imstk::SurfaceMesh>();
    surfMeshBackNew = cutTissueMesh->getSurfaceCutBackMeshNew();
    surfMeshBackNew->computeVertexNormals();


    auto object0 = std::make_shared<imstk::VisualObject>("visualColonMesh0");
    auto material0 = std::make_shared<RenderMaterial>();
    material0->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME SURFACE
    auto tmp0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    material0->addTexture(tmp0);
    surfMeshSwept->setRenderMaterial(material0);
    object0->setVisualGeometry(surfMeshSwept); // change to any mesh created above
    sceneTest->addSceneObject(object0);

    auto object = std::make_shared<imstk::VisualObject>("visualColonMesh");
    auto material = std::make_shared<RenderMaterial>();
    material->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME); // WIREFRAME SURFACE
    auto tmp = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    material->addTexture(tmp);
    surfMesh->setRenderMaterial(material);
    object->setVisualGeometry(surfMesh); // change to any mesh created above
    sceneTest->addSceneObject(object);

    auto object4 = std::make_shared<imstk::VisualObject>("visualColonMesh4");
    auto material4 = std::make_shared<RenderMaterial>();
    material4->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME); // WIREFRAME
    auto tmp4 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    material4->addTexture(tmp4);
    surfMeshFront->setRenderMaterial(material4);
    object4->setVisualGeometry(surfMeshFront); // change to any mesh created above
    sceneTest->addSceneObject(object4);

    auto object5 = std::make_shared<imstk::VisualObject>("visualColonMesh5");
    auto material5 = std::make_shared<RenderMaterial>();
    material5->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME
    auto tmp5 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    material5->addTexture(tmp5);
    surfMeshFrontNew->setRenderMaterial(material5);
    object5->setVisualGeometry(surfMeshFrontNew); // change to any mesh created above
    sceneTest->addSceneObject(object5);

    //auto object3 = std::make_shared<imstk::VisualObject>("visualColonMesh3");
    //auto material3 = std::make_shared<RenderMaterial>();
    //material3->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    //auto tmp3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    //material3->addTexture(tmp3);
    //surfMeshBack->setRenderMaterial(material3);
    //object3->setVisualGeometry(surfMeshBack); // change to any mesh created above
    //sceneTest->addSceneObject(object3);

    //auto object2 = std::make_shared<imstk::VisualObject>("visualColonMesh2");
    //auto material2 = std::make_shared<RenderMaterial>();
    //material2->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE);
    //auto tmp2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    //material2->addTexture(tmp2);
    //surfMeshBackNew->setRenderMaterial(material2);
    //object2->setVisualGeometry(surfMeshBackNew); // change to any mesh created above
    //sceneTest->addSceneObject(object2);

    // Light (white)
    auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
    whiteLight->setPosition(imstk::Vec3d(-15, -1, 0));
    whiteLight->setFocalPoint(imstk::Vec3d(1, -1, 0));
    sceneTest->addLight(whiteLight);
    auto whiteLight2 = std::make_shared<imstk::PointLight>("whiteLight2");
    whiteLight2->setPosition(imstk::Vec3d(1, -1, 0));
    whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    sceneTest->addLight(whiteLight2);

    // Light (red)
    auto colorLight = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight->setPosition(imstk::Vec3d(-3.5, 2, -28)); // - 25, -8, -4.5
    colorLight->setFocalPoint(imstk::Vec3d(-1.5, 22, -18)); // - 3.7, 5.5, 7
    colorLight->setColor(imstk::Color::White); // red
    colorLight->setSpotAngle(15);
    auto colorLight2 = std::make_shared<imstk::SpotLight>("colorLight2");
    colorLight2->setPosition(imstk::Vec3d(-2.5, 17, -23));
    colorLight2->setFocalPoint(imstk::Vec3d(-2.5, 7, -23));
    colorLight2->setColor(imstk::Color::White);
    colorLight2->setSpotAngle(15);
    auto colorLight3 = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight3->setPosition(imstk::Vec3d(-2.5, 7, -23));  //-25, -5, -2.5
    colorLight3->setFocalPoint(imstk::Vec3d(-2.5, 17, -23));  // -3.7, 5.5, 7
    colorLight3->setColor(imstk::Color::White); // red
    colorLight3->setSpotAngle(15);
    sceneTest->addLight(colorLight);
    sceneTest->addLight(colorLight2);
    sceneTest->addLight(colorLight3);

    // Update Camera
    auto cam1 = sceneTest->getCamera();
    cam1->setPosition(imstk::Vec3d(0, 0, 1));
    cam1->setFocalPoint(imstk::Vec3d(0, 0, 0));

    // Run
    sdk->setActiveScene(sceneTest);
    sdk->startSimulation(true);
}

void testCutting2()
{
    // SDK and Scene
    auto sdk = std::make_shared<imstk::SimulationManager>();
    auto sceneTest = sdk->createNewScene("SceneTest");

    //// Tetrahedral mesh
  

    //build the final mesh
    //vector<double> vFlatVertices;
    //vector<U32> vFlatElements;
    //FlattenVec3<double>(vertices, vFlatVertices);
    //FlattenVec4<U32>(elements, vFlatElements);
    //// Tetrahedral mesh
    imstk::StdVectorOfVec3d vertListTet;
    std::vector<imstk::TetrahedralMesh::TetraArray> tetConnectivity;
    //physXLink linkVolSur;
    std::vector<physXLink> linksVolSur;
    bool flag = loadTetFile(DATA_ROOT_PATH"/VESS/colonrectum_s002_SB.tet", vertListTet, tetConnectivity, linksVolSur);
    //auto tetMesh = std::make_shared<imstk::VESSTetrahedralMesh>();
    ////for (int i = 0; i < vertListTet.size();i++)
    ////{
    ////    vertListTet[i] += Vec3d(2.5, -12, 22);
    ////}
    //tetMesh->setInitialVertexPositions(vertListTet);
    //tetMesh->setVertexPositions(vertListTet);
    //tetMesh->setTetrahedraVertices(tetConnectivity);
    //tetMesh->setPhysXLink(linksVolSur);
    //tetMesh->setInitialPhysVertex(vertListTet);  // add later 06/04/2017

    //Vec3d min1, max1;
    //visualColonMesh->computeBoundingBox(min1, max1);

    //physXLink linkVolSur;
    //std::vector<physXLink> linksVolSur;
    //bool flag = loadTetFile(DATA_ROOT_PATH"/VESS/colonrectum_s002_SB.tet", vertListTet, tetConnectivity, linksVolSur); // colon_tumor3_3_SB for test   colon_tumor3_SB  cubeTest2_SB
    auto cutTissueMesh = std::make_shared<imstk::CuttingVESSCutMesh>();
    cutTissueMesh->setInitialVertexPositions(vertListTet);
    cutTissueMesh->setVertexPositions(vertListTet);
    cutTissueMesh->setTetrahedraVertices(tetConnectivity);

    cutTissueMesh->initializeCuttingMesh();
    cutTissueMesh->initializeCutting();
    cutTissueMesh->extractSurfaceMesh();
    //auto cutTissueMesh = std::make_shared<imstk::CuttingVESSCutMesh>(tetMesh);
    cutTissueMesh->setFlagSplitMeshAfterCut(true);

    Vec3d min1, max1;
    cutTissueMesh->computeBoundingBox(min1, max1);

    // start cutting
    vector<Vec3d> m_vBladeSegments;
    vector<Vec3d> m_vSweptQuad;
    Vec3d temp1, temp2;
    //temp1.x() = -19.7; temp1.y() = 5.5; temp1.z() = -4.4;
    //temp2.x() = -3.7; temp2.y() = 5.5; temp2.z() = -4.4;
    temp1.x() = -19.7; temp1.y() = 5.5; temp1.z() = -4.4;
    temp2.x() = -3.7; temp2.y() = 5.5; temp2.z() = -4.4;
    m_vBladeSegments.push_back(temp1);
    m_vBladeSegments.push_back(temp2);

    Vec3d temp3, temp4;
    //temp1.x() = -19.7; temp1.y() = -7.9; temp1.z() = 7.0;  // 2
    //temp2.x() = -19.7; temp2.y() = -7.9; temp2.z() = -4.4; // 1
    //temp3.x() = -3.7; temp3.y() = 5.5; temp3.z() = 7.0; // 3
    //temp4.x() = -3.7; temp4.y() = 5.5; temp4.z() = -4.4; // 4
    temp1.x() = -3.7; temp1.y() = -7.9; temp1.z() = 7.0;  // 2
    temp2.x() = -3.7; temp2.y() = -7.9; temp2.z() = -4.4; // 1
    temp3.x() = -19.7; temp3.y() = 5.5; temp3.z() = 7.0; // 3
    temp4.x() = -19.7; temp4.y() = 5.5; temp4.z() = -4.4; // 4
    m_vSweptQuad.push_back(temp1);
    m_vSweptQuad.push_back(temp2);
    m_vSweptQuad.push_back(temp3);
    m_vSweptQuad.push_back(temp4);

    int res = cutTissueMesh->cut(m_vBladeSegments, m_vSweptQuad, true);

    // c. Extract the surface mesh
    auto surfMesh = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMesh = cutTissueMesh->getSurfaceMesh();
    surfMesh->computeVertexNormals();

    auto surfMeshFront = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMeshFront = cutTissueMesh->getSurfaceCutFrontMesh();
    surfMeshFront->computeVertexNormals();

    auto surfMeshFrontNew = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMeshFrontNew = cutTissueMesh->getSurfaceCutFrontMeshNew();
    surfMeshFrontNew->computeVertexNormals();

    auto surfMeshBack = std::make_shared<imstk::SurfaceMesh>();
    surfMeshBack = cutTissueMesh->getSurfaceCutBackMesh();
    surfMeshBack->computeVertexNormals();

    auto surfMeshBackNew = std::make_shared<imstk::SurfaceMesh>();
    surfMeshBackNew = cutTissueMesh->getSurfaceCutBackMeshNew();
    surfMeshBackNew->computeVertexNormals();

    auto object = std::make_shared<imstk::VisualObject>("visualColonMesh");
    auto material = std::make_shared<RenderMaterial>();
    material->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME); // WIREFRAME SURFACE
    auto tmp = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    material->addTexture(tmp);
    surfMesh->setRenderMaterial(material);
    object->setVisualGeometry(surfMesh); // change to any mesh created above
    //sceneTest->addSceneObject(object);

    auto object4 = std::make_shared<imstk::VisualObject>("visualColonMesh4");
    auto material4 = std::make_shared<RenderMaterial>();
    material4->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME); // WIREFRAME
    auto tmp4 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    material4->addTexture(tmp4);
    surfMeshFront->setRenderMaterial(material4);
    object4->setVisualGeometry(surfMeshFront); // change to any mesh created above
    sceneTest->addSceneObject(object4);

    auto object5 = std::make_shared<imstk::VisualObject>("visualColonMesh5");
    auto material5 = std::make_shared<RenderMaterial>();
    material5->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME
    auto tmp5 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/submucosal/Submucosal_layer_BaseColor.png", Texture::Type::DIFFUSE);
    material5->addTexture(tmp5);
    surfMeshFrontNew->setRenderMaterial(material5);
    object5->setVisualGeometry(surfMeshFrontNew); // change to any mesh created above
    sceneTest->addSceneObject(object5);

    auto object3 = std::make_shared<imstk::VisualObject>("visualColonMesh3");
    auto material3 = std::make_shared<RenderMaterial>();
    material3->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmp3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    material3->addTexture(tmp3);
    surfMeshBack->setRenderMaterial(material3);
    object3->setVisualGeometry(surfMeshBack); // change to any mesh created above
    sceneTest->addSceneObject(object3);

    auto object2 = std::make_shared<imstk::VisualObject>("visualColonMesh2");
    auto material2 = std::make_shared<RenderMaterial>();
    material2->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE);
    auto tmp2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/submucosal/Submucosal_layer_BaseColor.png", Texture::Type::DIFFUSE);
    material2->addTexture(tmp2);
    surfMeshBackNew->setRenderMaterial(material2);
    object2->setVisualGeometry(surfMeshBackNew); // change to any mesh created above
    sceneTest->addSceneObject(object2);

    // Light (white)
    auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
    whiteLight->setPosition(imstk::Vec3d(-15, -1, 0));
    whiteLight->setFocalPoint(imstk::Vec3d(1, -1, 0));
    sceneTest->addLight(whiteLight);
    auto whiteLight2 = std::make_shared<imstk::PointLight>("whiteLight2");
    whiteLight2->setPosition(imstk::Vec3d(1, -1, 0));
    whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    sceneTest->addLight(whiteLight2);

    // Light (red)
    auto colorLight = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight->setPosition(imstk::Vec3d(-3.5, 2, -28)); // - 25, -8, -4.5
    colorLight->setFocalPoint(imstk::Vec3d(-1.5, 22, -18)); // - 3.7, 5.5, 7
    colorLight->setColor(imstk::Color::White); // red
    colorLight->setSpotAngle(15);
    auto colorLight2 = std::make_shared<imstk::SpotLight>("colorLight2");
    colorLight2->setPosition(imstk::Vec3d(-2.5, 17, -23));
    colorLight2->setFocalPoint(imstk::Vec3d(-2.5, 7, -23));
    colorLight2->setColor(imstk::Color::White);
    colorLight2->setSpotAngle(15);
    auto colorLight3 = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight3->setPosition(imstk::Vec3d(-2.5, 7, -23));  //-25, -5, -2.5
    colorLight3->setFocalPoint(imstk::Vec3d(-2.5, 17, -23));  // -3.7, 5.5, 7
    colorLight3->setColor(imstk::Color::White); // red
    colorLight3->setSpotAngle(15);
    sceneTest->addLight(colorLight);
    sceneTest->addLight(colorLight2);
    sceneTest->addLight(colorLight3);

    // Update Camera
    auto cam1 = sceneTest->getCamera();
    cam1->setPosition(imstk::Vec3d(0, 0, 1));
    cam1->setFocalPoint(imstk::Vec3d(0, 0, 0));

    // Run
    sdk->setActiveScene(sceneTest);
    sdk->startSimulation(true);
}

void testCuttingLess()
{
    // SDK and Scene
    auto sdk = std::make_shared<imstk::SimulationManager>();
    auto sceneTest = sdk->createNewScene("SceneTest");

    //// Tetrahedral mesh
    imstk::StdVectorOfVec3d vertListTet;
    std::vector<imstk::TetrahedralMesh::TetraArray> tetConnectivity;
    //vector< vec3d > vertices;
    //vector< vec4u32 > elements;

    int nx = 2, ny = 2, nz = 2;
    double cellsize = 2.0;
    Vec3d start = Vec3d(-(double)(nx) / 2.0, 0, -(double)(nz) / 2.0) * (cellsize);

    //add all nodes
    for (int i = 0; i < nx; i++)
    {
        for (int j = 0; j < ny; j++)
        {
            for (int k = 0; k < nz; k++)
            {
                Vec3d v = start + Vec3d(i, j, k) * cellsize;
                vertListTet.push_back(v);
            }
        }
    }

    //corner defs
    enum CellCorners { LBN = 0, LBF = 1, LTN = 2, LTF = 3, RBN = 4, RBF = 5, RTN = 6, RTF = 7 };
    U32 cn[8];
    //imstk::TetrahedralMesh::TetraArray tet1;
    //add all elements
    for (int i = 0; i < nx - 1; i++)
    {
        for (int j = 0; j < ny - 1; j++)
        {
            for (int k = 0; k < nz - 1; k++)
            {
                //collect cell nodes
                cn[LBN] = i * ny * nz + j * nz + k;
                cn[LBF] = i * ny * nz + j * nz + k + 1;

                cn[LTN] = i * ny * nz + (j + 1) * nz + k;
                cn[LTF] = i * ny * nz + (j + 1) * nz + k + 1;

                cn[RBN] = (i + 1) * ny * nz + j * nz + k;
                cn[RBF] = (i + 1) * ny * nz + j * nz + k + 1;

                cn[RTN] = (i + 1) * ny * nz + (j + 1) * nz + k;
                cn[RTF] = (i + 1) * ny * nz + (j + 1) * nz + k + 1;

                //add elements
                imstk::TetrahedralMesh::TetraArray tet1 = { cn[LBN], cn[LTN], cn[RBN], cn[LBF] };
                tetConnectivity.push_back(tet1);
                imstk::TetrahedralMesh::TetraArray tet2 = { cn[RTN], cn[LTN], cn[LBF], cn[RBN] };
                tetConnectivity.push_back(tet2);
                imstk::TetrahedralMesh::TetraArray tet3 = { cn[RTN], cn[LTN], cn[LTF], cn[LBF] };
                tetConnectivity.push_back(tet3);
                imstk::TetrahedralMesh::TetraArray tet4 = { cn[RTN], cn[RBN], cn[LBF], cn[RBF] };
                tetConnectivity.push_back(tet4);
                imstk::TetrahedralMesh::TetraArray tet5 = { cn[RTN], cn[LBF], cn[LTF], cn[RBF] };
                tetConnectivity.push_back(tet5);
                imstk::TetrahedralMesh::TetraArray tet6 = { cn[RTN], cn[LTF], cn[RTF], cn[RBF] };
                tetConnectivity.push_back(tet6);
            }
        }
    }

    //build the final mesh
    //vector<double> vFlatVertices;
    //vector<U32> vFlatElements;
    //FlattenVec3<double>(vertices, vFlatVertices);
    //FlattenVec4<U32>(elements, vFlatElements);

    //physXLink linkVolSur;
    std::vector<physXLink> linksVolSur;
    //bool flag = loadTetFile(DATA_ROOT_PATH"/VESS/colonrectum_s002_SB.tet", vertListTet, tetConnectivity, linksVolSur); // colon_tumor3_3_SB for test   colon_tumor3_SB  cubeTest2_SB
    auto cutTissueMesh = std::make_shared<imstk::CuttingVESSCutMesh>();
    cutTissueMesh->setInitialVertexPositions(vertListTet);
    cutTissueMesh->setVertexPositions(vertListTet);
    cutTissueMesh->setTetrahedraVertices(tetConnectivity);

    cutTissueMesh->initializeCuttingMesh();
    cutTissueMesh->initializeCutting();
    cutTissueMesh->extractSurfaceMesh();
    //auto cutTissueMesh = std::make_shared<imstk::CuttingVESSCutMesh>(tetMesh);
    cutTissueMesh->setFlagSplitMeshAfterCut(true);

    Vec3d min1, max1;
    cutTissueMesh->computeBoundingBox(min1, max1);

    // start cutting
    vector<Vec3d> m_vBladeSegments;
    vector<Vec3d> m_vSweptQuad;
    Vec3d temp1, temp2;
    //temp1.x = -2.7982251644134521; temp1.y = 1.2786393165588379; temp1.z = -2.44757509231567;
    //temp2.x = 3.9769670963287354; temp2.y = 1.2786393165588379; temp2.z = -2.44757509231567;
    temp1.x() = -2.8; temp1.y() = 1.3; temp1.z() = -2.5;
    temp2.x() = 4.0; temp2.y() = 1.3; temp2.z() = -2.5;
    m_vBladeSegments.push_back(temp1);
    m_vBladeSegments.push_back(temp2);

    Vec3d temp3, temp4;
    //temp1.x = -2.7982251644134521; temp1.y = 1.2786393165588379; temp1.z = 1.0033550262451172;
    //temp2.x = -2.7982251644134521; temp2.y = 1.2786393165588379; temp2.z = -2.44757509231567;
    //temp3.x = 3.9769670963287354; temp3.y = 1.2786393165588379; temp3.z = 1.0033550262451172;
    //temp4.x = 3.9769670963287354; temp4.y = 1.2786393165588379; temp4.z = -2.44757509231567;
    temp1.x() = -2.8; temp1.y() = 0.1; temp1.z() = 1.0;  // 2
    temp2.x() = -2.8; temp2.y() = 0.1; temp2.z() = -2.5; // 1
    temp3.x() = 4.0; temp3.y() = 1.3; temp3.z() = 1.0; // 3
    temp4.x() = 4.0; temp4.y() = 1.3; temp4.z() = -2.5; // 4
    m_vSweptQuad.push_back(temp1);
    m_vSweptQuad.push_back(temp2);
    m_vSweptQuad.push_back(temp3);
    m_vSweptQuad.push_back(temp4);

    int res = cutTissueMesh->cut(m_vBladeSegments, m_vSweptQuad, true);

    // c. Extract the surface mesh
    auto surfMesh = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMesh = cutTissueMesh->getSurfaceMesh();
    surfMesh->computeVertexNormals();

    auto surfMeshFront = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMeshFront = cutTissueMesh->getSurfaceCutFrontMesh();
    surfMeshFront->computeVertexNormals();

    auto surfMeshFrontNew = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMeshFrontNew = cutTissueMesh->getSurfaceCutFrontMeshNew();
    surfMeshFrontNew->computeVertexNormals();

    auto surfMeshBack = std::make_shared<imstk::SurfaceMesh>();
    surfMeshBack = cutTissueMesh->getSurfaceCutBackMesh();
    surfMeshBack->computeVertexNormals();

    auto surfMeshBackNew = std::make_shared<imstk::SurfaceMesh>();
    surfMeshBackNew = cutTissueMesh->getSurfaceCutBackMeshNew();
    surfMeshBackNew->computeVertexNormals();

    auto object = std::make_shared<imstk::VisualObject>("visualColonMesh");
    auto material = std::make_shared<RenderMaterial>();
    material->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME); // WIREFRAME SURFACE
    auto tmp = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    material->addTexture(tmp);
    surfMesh->setRenderMaterial(material);
    object->setVisualGeometry(surfMesh); // change to any mesh created above
    //sceneTest->addSceneObject(object);

    auto object4 = std::make_shared<imstk::VisualObject>("visualColonMesh4");
    auto material4 = std::make_shared<RenderMaterial>();
    material4->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME); // WIREFRAME
    auto tmp4 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    material4->addTexture(tmp4);
    surfMeshFront->setRenderMaterial(material4);
    object4->setVisualGeometry(surfMeshFront); // change to any mesh created above
    sceneTest->addSceneObject(object4);

    auto object5 = std::make_shared<imstk::VisualObject>("visualColonMesh5");
    auto material5 = std::make_shared<RenderMaterial>();
    material5->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME
    auto tmp5 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/submucosal/Submucosal_layer_BaseColor.png", Texture::Type::DIFFUSE);
    material5->addTexture(tmp5);
    surfMeshFrontNew->setRenderMaterial(material5);
    object5->setVisualGeometry(surfMeshFrontNew); // change to any mesh created above
    sceneTest->addSceneObject(object5);

    auto object3 = std::make_shared<imstk::VisualObject>("visualColonMesh3");
    auto material3 = std::make_shared<RenderMaterial>();
    material3->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmp3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    material3->addTexture(tmp3);
    surfMeshBack->setRenderMaterial(material3);
    object3->setVisualGeometry(surfMeshBack); // change to any mesh created above
    sceneTest->addSceneObject(object3);

    auto object2 = std::make_shared<imstk::VisualObject>("visualColonMesh2");
    auto material2 = std::make_shared<RenderMaterial>();
    material2->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE);
    auto tmp2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/submucosal/Submucosal_layer_BaseColor.png", Texture::Type::DIFFUSE);
    material2->addTexture(tmp2);
    surfMeshBackNew->setRenderMaterial(material2);
    object2->setVisualGeometry(surfMeshBackNew); // change to any mesh created above
    sceneTest->addSceneObject(object2);

    // Light (white)
    auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
    whiteLight->setPosition(imstk::Vec3d(-15, -1, 0));
    whiteLight->setFocalPoint(imstk::Vec3d(1, -1, 0));
    sceneTest->addLight(whiteLight);
    auto whiteLight2 = std::make_shared<imstk::PointLight>("whiteLight2");
    whiteLight2->setPosition(imstk::Vec3d(1, -1, 0));
    whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    sceneTest->addLight(whiteLight2);

    // Light (red)
    auto colorLight = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight->setPosition(imstk::Vec3d(-3.5, 2, -28)); // - 25, -8, -4.5
    colorLight->setFocalPoint(imstk::Vec3d(-1.5, 22, -18)); // - 3.7, 5.5, 7
    colorLight->setColor(imstk::Color::White); // red
    colorLight->setSpotAngle(15);
    auto colorLight2 = std::make_shared<imstk::SpotLight>("colorLight2");
    colorLight2->setPosition(imstk::Vec3d(-2.5, 17, -23));
    colorLight2->setFocalPoint(imstk::Vec3d(-2.5, 7, -23));
    colorLight2->setColor(imstk::Color::White);
    colorLight2->setSpotAngle(15);
    auto colorLight3 = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight3->setPosition(imstk::Vec3d(-2.5, 7, -23));  //-25, -5, -2.5
    colorLight3->setFocalPoint(imstk::Vec3d(-2.5, 17, -23));  // -3.7, 5.5, 7
    colorLight3->setColor(imstk::Color::White); // red
    colorLight3->setSpotAngle(15);
    sceneTest->addLight(colorLight);
    sceneTest->addLight(colorLight2);
    sceneTest->addLight(colorLight3);

    // Update Camera
    auto cam1 = sceneTest->getCamera();
    cam1->setPosition(imstk::Vec3d(0, 0, 1));
    cam1->setFocalPoint(imstk::Vec3d(0, 0, 0));

    // Run
    sdk->setActiveScene(sceneTest);
    sdk->startSimulation(true);
}
void testCutting()
{
    // SDK and Scene
    auto sdk = std::make_shared<imstk::SimulationManager>();
    auto sceneTest = sdk->createNewScene("SceneTest");

    //// Tetrahedral mesh
    imstk::StdVectorOfVec3d vertListTet;
    std::vector<imstk::TetrahedralMesh::TetraArray> tetConnectivity;
    //vector< vec3d > vertices;
    //vector< vec4u32 > elements;

    int nx=4, ny=4, nz=4;
    double cellsize = 1.0;
    Vec3d start = Vec3d(-(double)(nx) / 2.0, 0, -(double)(nz) / 2.0) * (cellsize);

    //add all nodes
    for (int i = 0; i < nx; i++) 
    {
        for (int j = 0; j < ny; j++) 
        {
            for (int k = 0; k < nz; k++) 
            {
                Vec3d v = start + Vec3d(i, j, k) * cellsize;
                vertListTet.push_back(v);
            }
        }
    }

    //corner defs
    enum CellCorners { LBN = 0, LBF = 1, LTN = 2, LTF = 3, RBN = 4, RBF = 5, RTN = 6, RTF = 7 };
    U32 cn[8];
    //imstk::TetrahedralMesh::TetraArray tet1;
    //add all elements
    for (int i = 0; i < nx - 1; i++) 
    {
        for (int j = 0; j < ny - 1; j++) 
        {
            for (int k = 0; k < nz - 1; k++) 
            {
                //collect cell nodes
                cn[LBN] = i * ny * nz + j * nz + k;
                cn[LBF] = i * ny * nz + j * nz + k + 1;

                cn[LTN] = i * ny * nz + (j + 1) * nz + k;
                cn[LTF] = i * ny * nz + (j + 1) * nz + k + 1;

                cn[RBN] = (i + 1) * ny * nz + j * nz + k;
                cn[RBF] = (i + 1) * ny * nz + j * nz + k + 1;

                cn[RTN] = (i + 1) * ny * nz + (j + 1) * nz + k;
                cn[RTF] = (i + 1) * ny * nz + (j + 1) * nz + k + 1;

                //add elements
                imstk::TetrahedralMesh::TetraArray tet1 = { cn[LBN], cn[LTN], cn[RBN], cn[LBF] };
                tetConnectivity.push_back(tet1);
                imstk::TetrahedralMesh::TetraArray tet2 = { cn[RTN], cn[LTN], cn[LBF], cn[RBN] };
                tetConnectivity.push_back(tet2);
                imstk::TetrahedralMesh::TetraArray tet3 = { cn[RTN], cn[LTN], cn[LTF], cn[LBF] };
                tetConnectivity.push_back(tet3);
                imstk::TetrahedralMesh::TetraArray tet4 = { cn[RTN], cn[RBN], cn[LBF], cn[RBF] };
                tetConnectivity.push_back(tet4);
                imstk::TetrahedralMesh::TetraArray tet5 = { cn[RTN], cn[LBF], cn[LTF], cn[RBF] };
                tetConnectivity.push_back(tet5);
                imstk::TetrahedralMesh::TetraArray tet6 = { cn[RTN], cn[LTF], cn[RTF], cn[RBF] };
                tetConnectivity.push_back(tet6);
            }
        }
    }

    //build the final mesh
    //vector<double> vFlatVertices;
    //vector<U32> vFlatElements;
    //FlattenVec3<double>(vertices, vFlatVertices);
    //FlattenVec4<U32>(elements, vFlatElements);
    
    //physXLink linkVolSur;
    std::vector<physXLink> linksVolSur;
    //bool flag = loadTetFile(DATA_ROOT_PATH"/VESS/colonrectum_s002_SB.tet", vertListTet, tetConnectivity, linksVolSur); // colon_tumor3_3_SB for test   colon_tumor3_SB  cubeTest2_SB
    auto cutTissueMesh = std::make_shared<imstk::CuttingVESSCutMesh>();
    cutTissueMesh->setInitialVertexPositions(vertListTet);
    cutTissueMesh->setVertexPositions(vertListTet);
    cutTissueMesh->setTetrahedraVertices(tetConnectivity);

    cutTissueMesh->initializeCuttingMesh();
    cutTissueMesh->initializeCutting();
    cutTissueMesh->extractSurfaceMesh();
    //auto cutTissueMesh = std::make_shared<imstk::CuttingVESSCutMesh>(tetMesh);
    cutTissueMesh->setFlagSplitMeshAfterCut(true);

    Vec3d min1, max1;
    cutTissueMesh->computeBoundingBox(min1, max1);

    // start cutting
    vector<Vec3d> m_vBladeSegments;
    vector<Vec3d> m_vSweptQuad;
    Vec3d temp1, temp2;
    //temp1.x = -2.7982251644134521; temp1.y = 1.2786393165588379; temp1.z = -2.44757509231567;
    //temp2.x = 3.9769670963287354; temp2.y = 1.2786393165588379; temp2.z = -2.44757509231567;
    temp1.x() = -2.8; temp1.y() = 1.3; temp1.z() = -2.5;
    temp2.x() = 4.0; temp2.y() = 1.3; temp2.z() = -2.5;
    m_vBladeSegments.push_back(temp1);
    m_vBladeSegments.push_back(temp2);

    Vec3d temp3, temp4;
    //temp1.x = -2.7982251644134521; temp1.y = 1.2786393165588379; temp1.z = 1.0033550262451172;
    //temp2.x = -2.7982251644134521; temp2.y = 1.2786393165588379; temp2.z = -2.44757509231567;
    //temp3.x = 3.9769670963287354; temp3.y = 1.2786393165588379; temp3.z = 1.0033550262451172;
    //temp4.x = 3.9769670963287354; temp4.y = 1.2786393165588379; temp4.z = -2.44757509231567;
    temp1.x() = -2.8; temp1.y() = 0.1; temp1.z() = 1.0;  // 2
    temp2.x() = -2.8; temp2.y() = 0.1; temp2.z() = -2.5; // 1
    temp3.x() = 4.0; temp3.y() = 1.3; temp3.z() = 1.0; // 3
    temp4.x() = 4.0; temp4.y() = 1.3; temp4.z() = -2.5; // 4
    m_vSweptQuad.push_back(temp1);
    m_vSweptQuad.push_back(temp2);
    m_vSweptQuad.push_back(temp3);
    m_vSweptQuad.push_back(temp4);

    int res = cutTissueMesh->cut(m_vBladeSegments, m_vSweptQuad, true);

    // c. Extract the surface mesh
    auto surfMesh = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMesh = cutTissueMesh->getSurfaceMesh();
    surfMesh->computeVertexNormals();

    auto surfMeshFront = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMeshFront = cutTissueMesh->getSurfaceCutFrontMesh();
    surfMeshFront->computeVertexNormals();

    auto surfMeshFrontNew = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMeshFrontNew = cutTissueMesh->getSurfaceCutFrontMeshNew();
    surfMeshFrontNew->computeVertexNormals();

    auto surfMeshBack = std::make_shared<imstk::SurfaceMesh>();
    surfMeshBack = cutTissueMesh->getSurfaceCutBackMesh();
    surfMeshBack->computeVertexNormals();

    auto surfMeshBackNew = std::make_shared<imstk::SurfaceMesh>();
    surfMeshBackNew = cutTissueMesh->getSurfaceCutBackMeshNew();
    surfMeshBackNew->computeVertexNormals();

    auto object = std::make_shared<imstk::VisualObject>("visualColonMesh");
    auto material = std::make_shared<RenderMaterial>();
    material->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME); // WIREFRAME SURFACE
    auto tmp = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    material->addTexture(tmp);
    surfMesh->setRenderMaterial(material);
    object->setVisualGeometry(surfMesh); // change to any mesh created above
    //sceneTest->addSceneObject(object);

    auto object4 = std::make_shared<imstk::VisualObject>("visualColonMesh4");
    auto material4 = std::make_shared<RenderMaterial>();
    material4->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME); // WIREFRAME
    auto tmp4 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    material4->addTexture(tmp4);
    surfMeshFront->setRenderMaterial(material4);
    object4->setVisualGeometry(surfMeshFront); // change to any mesh created above
    sceneTest->addSceneObject(object4);

    auto object5 = std::make_shared<imstk::VisualObject>("visualColonMesh5");
    auto material5 = std::make_shared<RenderMaterial>();
    material5->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME
    auto tmp5 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/submucosal/Submucosal_layer_BaseColor.png", Texture::Type::DIFFUSE);
    material5->addTexture(tmp5);
    surfMeshFrontNew->setRenderMaterial(material5);
    object5->setVisualGeometry(surfMeshFrontNew); // change to any mesh created above
    sceneTest->addSceneObject(object5);

    auto object3 = std::make_shared<imstk::VisualObject>("visualColonMesh3");
    auto material3 = std::make_shared<RenderMaterial>();
    material3->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmp3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    material3->addTexture(tmp3);
    surfMeshBack->setRenderMaterial(material3);
    object3->setVisualGeometry(surfMeshBack); // change to any mesh created above
    sceneTest->addSceneObject(object3);

    auto object2 = std::make_shared<imstk::VisualObject>("visualColonMesh2");
    auto material2 = std::make_shared<RenderMaterial>();
    material2->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE);
    auto tmp2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/submucosal/Submucosal_layer_BaseColor.png", Texture::Type::DIFFUSE);
    material2->addTexture(tmp2);
    surfMeshBackNew->setRenderMaterial(material2);
    object2->setVisualGeometry(surfMeshBackNew); // change to any mesh created above
    sceneTest->addSceneObject(object2);

    // Light (white)
    auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
    whiteLight->setPosition(imstk::Vec3d(-15, -1, 0));
    whiteLight->setFocalPoint(imstk::Vec3d(1, -1, 0));
    sceneTest->addLight(whiteLight);
    auto whiteLight2 = std::make_shared<imstk::PointLight>("whiteLight2");
    whiteLight2->setPosition(imstk::Vec3d(1, -1, 0));
    whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    sceneTest->addLight(whiteLight2);

    // Light (red)
    auto colorLight = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight->setPosition(imstk::Vec3d(-3.5, 2, -28)); // - 25, -8, -4.5
    colorLight->setFocalPoint(imstk::Vec3d(-1.5, 22, -18)); // - 3.7, 5.5, 7
    colorLight->setColor(imstk::Color::White); // red
    colorLight->setSpotAngle(15);
    auto colorLight2 = std::make_shared<imstk::SpotLight>("colorLight2");
    colorLight2->setPosition(imstk::Vec3d(-2.5, 17, -23));
    colorLight2->setFocalPoint(imstk::Vec3d(-2.5, 7, -23));
    colorLight2->setColor(imstk::Color::White);
    colorLight2->setSpotAngle(15);
    auto colorLight3 = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight3->setPosition(imstk::Vec3d(-2.5, 7, -23));  //-25, -5, -2.5
    colorLight3->setFocalPoint(imstk::Vec3d(-2.5, 17, -23));  // -3.7, 5.5, 7
    colorLight3->setColor(imstk::Color::White); // red
    colorLight3->setSpotAngle(15);
    sceneTest->addLight(colorLight);
    sceneTest->addLight(colorLight2);
    sceneTest->addLight(colorLight3);

    // Update Camera
    auto cam1 = sceneTest->getCamera();
    cam1->setPosition(imstk::Vec3d(0, 0, 1));
    cam1->setFocalPoint(imstk::Vec3d(0, 0, 0));

    // Run
    sdk->setActiveScene(sceneTest);
    sdk->startSimulation(true);
}

void testNewModel()
{
#ifdef iMSTK_USE_OPENHAPTICS
    // SDK and Scene
    auto sdk = std::make_shared<imstk::SimulationManager>();
    auto scene = sdk->createNewScene("SceneTestDevice");

    // Light (white)
    auto whiteLight = std::make_shared<imstk::SpotLight>("whiteLight");
    whiteLight->setPosition(imstk::Vec3d(-15, -1, 0));
    whiteLight->setFocalPoint(imstk::Vec3d(1, -1, 0));
    scene->addLight(whiteLight);
    auto whiteLight2 = std::make_shared<imstk::SpotLight>("whiteLight2");
    whiteLight2->setPosition(imstk::Vec3d(1, -1, 0));
    whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    scene->addLight(whiteLight2);

    // Light (red)
    auto colorLight = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight->setPosition(imstk::Vec3d(-3.5, 2, -28)); // - 25, -8, -4.5
    colorLight->setFocalPoint(imstk::Vec3d(-1.5, 22, -18)); // - 3.7, 5.5, 7
    colorLight->setColor(imstk::Color::White); // red
    colorLight->setSpotAngle(15);
    auto colorLight2 = std::make_shared<imstk::SpotLight>("colorLight2");
    colorLight2->setPosition(imstk::Vec3d(-2.5, 17, -23));
    colorLight2->setFocalPoint(imstk::Vec3d(-2.5, 7, -23));
    colorLight2->setColor(imstk::Color::White);
    colorLight2->setSpotAngle(15);
    auto colorLight3 = std::make_shared<imstk::SpotLight>("colorLight3");
    colorLight3->setPosition(imstk::Vec3d(-2.5, 7, -23));  //-25, -5, -2.5
    colorLight3->setFocalPoint(imstk::Vec3d(-2.5, 17, -23));  // -3.7, 5.5, 7
    colorLight3->setColor(imstk::Color::White); // red
    colorLight3->setSpotAngle(15);
    scene->addLight(colorLight);
    scene->addLight(colorLight2);
    scene->addLight(colorLight3);

    auto colorLight0 = std::make_shared<imstk::SpotLight>("colorLight0");
    colorLight0->setPosition(imstk::Vec3d(-4, -4, -1)); // - 25, -8, -4.5
    colorLight0->setFocalPoint(imstk::Vec3d(4, 3.5, 2)); // - 3.7, 5.5, 7
    colorLight0->setColor(imstk::Color::White); // red
    colorLight0->setSpotAngle(15);
    auto colorLight02 = std::make_shared<imstk::SpotLight>("colorLight02");
    colorLight02->setPosition(imstk::Vec3d(4, 3.5, 2));
    colorLight02->setFocalPoint(imstk::Vec3d(-4, -4, -1));
    colorLight02->setColor(imstk::Color::White);
    colorLight02->setSpotAngle(15);
    auto colorLight03 = std::make_shared<imstk::SpotLight>("colorLight03");
    colorLight03->setPosition(imstk::Vec3d(-2.5, 7, -23));  //-25, -5, -2.5
    colorLight03->setFocalPoint(imstk::Vec3d(-2.5, 17, -23));  // -3.7, 5.5, 7
    colorLight03->setColor(imstk::Color::White); // red
    colorLight03->setSpotAngle(15);

    scene->addLight(colorLight0);
    scene->addLight(colorLight02);
    scene->addLight(colorLight03);

    // Update Camera
    auto cam1 = scene->getCamera();
    cam1->setPosition(imstk::Vec3d(-12.5, -7, -33)); // -25, -8, -4.5
    cam1->setFocalPoint(imstk::Vec3d(2.5, 17, -13)); // -3.7, 5.5, 7
    cam1->setViewAngle(75);


    // model
    // colon wall
    std::string pathColon = DATA_ROOT_PATH"/VESS/New/latest_low2.obj"; //  colon_new_002_SB   colon_tumor3_SB cubeTest2_SB  colon_s022.dae"; //  colon_s02.dae"; //  colon_s12.dae"; // volume mesh for colliding // key==colonnew_seg4.dae  colonnew_seg0508.dae small segment// good segment  colonnew_seg4.dae   colonnew_seg3.obj colon_move2.DAE"; //  VESS / colonnew_seg3.obj"; // glove.dae"; //  colon_move2.DAE";    // colon_move2.DAE  colon1_part_max.DAE  colon1_part_max.DAE  colon6_max.DAEcloth.DAE  colon_1_c2_max  colon_cylinder2_tri_max
    auto objMesh = imstk::MeshIO::read(pathColon);   // collidingColonMesh; //  imstk::MeshIO::read(pathColon);
    auto visualColonMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh); // physicsColonMesh->getSurfaceMesh(); //  

    Vec3d min1, max1;
    visualColonMesh->computeBoundingBox(min1, max1);

    auto material = std::make_shared<RenderMaterial>();
    auto diffuseTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/New/Polyp_BaseColor.png", Texture::Type::DIFFUSE);
    auto normalTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/New/Polyp_Normal.png", Texture::Type::NORMAL);
    auto roughnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/New/Polyp_Roughness.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    auto sssTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_sss.png", Texture::Type::SUBSURFACE_SCATTERING);
    material->addTexture(diffuseTexture);
    material->addTexture(normalTexture);
    material->addTexture(roughnessTexture);
    //material->setTexture(metalnessTexture);
    material->addTexture(sssTexture);
    material->setTessellated(true);
    visualColonMesh->setRenderMaterial(material);

    // Create object and add to scene  just for scene, no colliding  for the two sides part, middle for operation part
    auto object = std::make_shared<imstk::VisualObject>("visualColonMesh");
    object->setVisualGeometry(visualColonMesh); // change to any mesh created above
    scene->addSceneObject(object);

    // Run
    sdk->setActiveScene(scene);
    sdk->startSimulation(true);
#endif
}

//// 07/18/2017 more dense model merged with tumor with injection knife for  test force feedback with virtual coupling for descending colon part with new colon model based on Mark- doctor split into three segments  use real knife model 2/3 length of artist
void testPbdVirtualCoupling22()
{
#ifdef iMSTK_USE_OPENHAPTICS
    // SDK and Scene
    auto sdk = std::make_shared<imstk::SimulationManager>();
    auto scene = sdk->createNewScene("SceneTestDevice");

    // Device Client
    auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

    // Device Server
    auto server = std::make_shared<imstk::HDAPIDeviceServer>();
    server->addDeviceClient(client);
    sdk->addModule(server);

    std::string path2obj = DATA_ROOT_PATH"/VESS/knife_injection4.obj"; // knife correct ITKnife6 // cube knife knife3.obj";  // //  113.obj"; //  /  blade2.obj  cube.obj  kcube.obj  test cloth using this 
    std::string path2obj_c = DATA_ROOT_PATH"/VESS/knife_injection4.obj"; // knife correct ITKnife6  // cube knife knife3.obj";  //  //  113.obj";  // / VESS / TriangleTipKnife2.obj";  // blade2.obj  cube.obj kcube_s2.obj
    auto readMesh = imstk::MeshIO::read(path2obj_c); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);

    // Line mesh
    auto lineMesh = std::make_shared<imstk::LineMesh>();
    imstk::StdVectorOfVec3d vertList;
    vertList.resize(2);  // using 5 segment
    //vertList[0] = imstk::Vec3d(-13, -2.25, 9.15); // should - volume mesh therold than surface mesh (9.0, -1.9, -0.6) AABB 0: 9.0, -2.23, -0.6   -0.7 ITKnife6.DAE  // knife3.obj 9.5, -0.9, 1.18
    //vertList[1] = imstk::Vec3d(-5.93, -1.825, 9.125); // 9.7 ITKnife6.DAE // knife3.obj 9.5, -0.9, 8.73
    //vertList[2] = imstk::Vec3d(1.14, -1.4, 9.1); // (9.0, -2.55, 9.5)  AABB 1: 9.0, -2.23, 9.6  9.7 ITKnife6.DAE // knife3.obj 9.5, -0.9, 8.73
    vertList[0] = imstk::Vec3d(-16.4, -1.85, 9.15);
    vertList[1] = imstk::Vec3d(-9.3, -1.4, 9.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    std::vector<std::vector<int> > connectivity;
    for (int i = 0; i < 1;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity.push_back(line);
    }

    lineMesh->setInitialVertexPositions(vertList);
    lineMesh->setVertexPositions(vertList);
    lineMesh->setConnectivity(connectivity);

    // using for line mesh display
    auto lineSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertListLine;
    vertListLine.resize(3);
    vertListLine[0] = imstk::Vec3d(-16.4, -1.85, 9.15);
    vertListLine[1] = imstk::Vec3d(-9.3, -1.4, 9.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertListLine[2] = imstk::Vec3d(-16.4, -1.85, 9.15);
    std::vector<std::vector<int> > connectivityLine;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivityLine.push_back(line);
    }

    lineSurfMesh->setInitialVertexPositions(vertListLine);
    lineSurfMesh->setVertexPositions(vertListLine);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> trianglesLine;
    imstk::SurfaceMesh::TriangleArray triLine[1];
    triLine[0] = { { 0, 1, 2 } };
    trianglesLine.push_back(triLine[0]);
    lineSurfMesh->setTrianglesVertices(trianglesLine);
    auto objectLine = std::make_shared<imstk::VisualObject>("visualLineMesh");
    auto materialLine = std::make_shared<RenderMaterial>();
    materialLine->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpLine = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/InjectionNeedle_BaseColor.png", Texture::Type::DIFFUSE);
    materialLine->addTexture(tmpLine);
    lineSurfMesh->setRenderMaterial(materialLine);
    objectLine->setVisualGeometry(lineSurfMesh); // change to any mesh created above
    scene->addSceneObject(objectLine);
    // END

    // using for force normal
    auto tempSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertList2;
    vertList2.resize(3);
    vertList2[0] = imstk::Vec3d(-16.4, -1.85, 9.15);
    vertList2[1] = imstk::Vec3d(-9.3, -1.4, 9.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertList2[2] = imstk::Vec3d(-16.4, -1.85, 9.15);
    std::vector<std::vector<int> > connectivity2;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity2.push_back(line);
    }

    tempSurfMesh->setInitialVertexPositions(vertList2);
    tempSurfMesh->setVertexPositions(vertList2);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> triangles;
    imstk::SurfaceMesh::TriangleArray tri[1];
    tri[0] = { { 0, 1, 2 } };
    triangles.push_back(tri[0]);
    tempSurfMesh->setTrianglesVertices(triangles);
    auto objectTmp = std::make_shared<imstk::VisualObject>("visualNormal0");
    auto materialTmp = std::make_shared<RenderMaterial>();
    materialTmp->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpTT = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialTmp->addTexture(tmpTT);
    tempSurfMesh->setRenderMaterial(materialTmp);
    objectTmp->setVisualGeometry(tempSurfMesh); // change to any mesh created above
    scene->addSceneObject(objectTmp);
    // END

    // XZH using for the central line of colon
    auto centrallineSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertList3;
    vertList3.resize(3);
    vertList3[0] = imstk::Vec3d(-2.5, 7, -23);
    vertList3[1] = imstk::Vec3d(-3.5, 15, -23);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertList3[2] = imstk::Vec3d(-2.5, 7, -23);
    std::vector<std::vector<int> > connectivity3;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity3.push_back(line);
    }

    centrallineSurfMesh->setInitialVertexPositions(vertList3);
    centrallineSurfMesh->setVertexPositions(vertList3);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> triangles1;
    imstk::SurfaceMesh::TriangleArray tri1[1];
    tri1[0] = { { 0, 1, 2 } };
    triangles1.push_back(tri1[0]);
    centrallineSurfMesh->setTrianglesVertices(triangles1);
    auto objectCentral = std::make_shared<imstk::VisualObject>("visualNormal1");
    auto materialCentral = std::make_shared<RenderMaterial>();
    materialCentral->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpCentral = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialCentral->addTexture(tmpCentral);
    centrallineSurfMesh->setRenderMaterial(materialCentral);
    objectCentral->setVisualGeometry(centrallineSurfMesh); // change to any mesh created above
    //scene->addSceneObject(objectCentral);
    // XZH END

    auto visualMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(readMesh);
    auto physicsMesh = lineMesh; //visualMesh; //  lineMesh; //  imstk::MeshIO::read(path2obj_c);  //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
    auto collidingMesh = lineMesh; // visualMesh; //  lineMesh;
    Vec3d min1, max1;
    visualMesh->computeBoundingBox(min1, max1);

    auto material0 = std::make_shared<RenderMaterial>();
    auto diffuseTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto normalTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Normal.png", Texture::Type::NORMAL);
    auto roughnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Roughness.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material0->addTexture(diffuseTexture0);
    material0->addTexture(normalTexture0);
    material0->addTexture(roughnessTexture0);
    //material->setTexture(metalnessTexture);
    visualMesh->setRenderMaterial(material0);

    FILE *outpointtri;
    outpointtri = fopen("i:/AABB.txt", "w");
    fprintf(outpointtri, "V: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    collidingMesh->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "C: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    fclose(outpointtri);

    auto object = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject");

    auto objMapP2V = std::make_shared<imstk::OneToOneMap>();  // std::make_shared<imstk::TriangleLineMap>(); //  std::make_shared<imstk::OneToOneMap>();
    objMapP2V->setMaster(physicsMesh);
    objMapP2V->setSlave(visualMesh);
    objMapP2V->compute();


    auto objMapP2C = std::make_shared<imstk::OneToOneMap>();  //  colliding is physical mesh
    objMapP2C->setMaster(physicsMesh);
    objMapP2C->setSlave(collidingMesh);
    objMapP2C->compute();

    auto objMapC2V = std::make_shared<imstk::OneToOneMap>(); //  std::make_shared<imstk::TriangleLineMap>(); //  s std::make_shared<imstk::OneToOneMap>();
    objMapC2V->setMaster(collidingMesh);
    objMapC2V->setSlave(visualMesh);
    objMapC2V->compute();

    auto objMapC2P = std::make_shared<imstk::OneToOneMap>(); // colliding is physical mesh
    objMapC2P->setMaster(collidingMesh);
    objMapC2P->setSlave(physicsMesh);
    objMapC2P->compute();

    object->setVisualGeometry(visualMesh);
    object->setCollidingGeometry(collidingMesh);
    object->setPhysicsGeometry(physicsMesh);
    object->setPhysicsToCollidingMap(objMapP2C);
    object->setPhysicsToVisualMap(objMapP2V);
    object->setCollidingToVisualMap(objMapC2V);
    object->setColldingToPhysicsMap(objMapC2P);

    auto pbdModel = std::make_shared<PbdModel>();
    pbdModel->setModelGeometry(physicsMesh);
    pbdModel->configure(/*Number of Constraints*/1,
        /*Constraint configuration*/"Distance 0.1",
        /*Mass*/0.0,
        /*Gravity*/"0 0 0",
        /*TimeStep*/0.0003,
        /*FixedPoint*/"",
        /*NumberOfIterationInConstraintSolver*/2, // 5
        /*Proximity*/0.3,  // use linemesh 0.3  // 0.01 use  //  cube 0.1
        /*Contact stiffness*/0.001);
    object->setDynamicalModel(pbdModel);

    //auto pbdSolver = std::make_shared<PbdSolver>();
    //pbdSolver->setPbdObject(object);
    //scene->addNonlinearSolver(pbdSolver);

    scene->addSceneObject(object);

    auto trackCtrl = std::make_shared<imstk::DeviceTracker>(client);
    trackCtrl->setTranslationScaling(0.1);  // 0.1  test 0.1 should use  Scale 0.001 will be better 
    auto controller = std::make_shared<imstk::PBDSceneObjectController>(object, trackCtrl);
    scene->addObjectController(controller);

    // Sphere1
    auto sphere1Obj = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "Sphere1", 0.5, Vec3d(-16.4, -1.85, 9.15)); // -2.23
    sphere1Obj->getVisualGeometry()->setTranslation(Vec3d(-16.4, -1.85, 9.15));
    //auto materialTT = std::make_shared<Material>();
    //materialTT->setTexture(diffuseTexture0);
    //auto suMesh = std::dynamic_pointer_cast<SurfaceMesh>(sphere1Obj->getVisualGeometry());
    //suMesh->addMaterial(materialTT);
    //auto sphere2Obj = apiutils::createCollidingAnalyticalSceneObject(
    //    imstk::Geometry::Type::Sphere, scene, "Sphere2", 0.5, Vec3d(-9.3, -1.4, 9.05)); //-9.3, -1.4, 9.05    new -9.53, - 1.50,    8.6  org  -7.2, -1.35, 8.62
    //sphere2Obj->getVisualGeometry()->setTranslation(Vec3d(-9.3, -1.4, 9.05));
    //sphere2Obj->setCollidingGeometry(sphere2Obj->getVisualGeometry());

    auto visualSphere22 = std::make_shared<imstk::Sphere>();
    visualSphere22->setTranslation(Vec3d(-9.3, -1.4, 9.05));
    visualSphere22->setRadius(0.5);
    Vec3d mm2 = visualSphere22->getPosition();
    auto sphere2Obj = std::make_shared<CollidingObject>("Sphere2");
    sphere2Obj->setCollidingGeometry(visualSphere22);
    sphere2Obj->setVisualGeometry(visualSphere22);
    scene->addSceneObject(sphere2Obj);

    auto visualSphere2 = std::make_shared<imstk::Sphere>();
    visualSphere2->setTranslation(Vec3d(-9.3, -1.4, 9.05));
    visualSphere2->setRadius(0.7);
    Vec3d mmm = visualSphere2->getPosition();
    auto sphere3Obj = std::make_shared<CollidingObject>("Sphere3");
    sphere3Obj->setCollidingGeometry(visualSphere2);
    sphere3Obj->setVisualGeometry(visualSphere2);
    scene->addSceneObject(sphere3Obj);


    auto sphereOrg = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereOrg", 0.1, Vec3d(0, 0, 0));
    sphereOrg->getVisualGeometry()->setTranslation(Vec3d(0, 0, 0));
    auto sphereMaterial = std::make_shared<RenderMaterial>();
    sphereMaterial->setDiffuseColor(Color::Red);
    sphereOrg->getVisualGeometry()->setRenderMaterial(sphereMaterial);

    auto sphereX = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereX", 0.5, Vec3d(10, 0, 0));
    sphereX->getVisualGeometry()->setTranslation(Vec3d(10, 0, 0));

    auto sphereY = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereY", 1.5, Vec3d(0, 10, 0));
    sphereY->getVisualGeometry()->setTranslation(Vec3d(0, 10, 0));

    auto sphereZ = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereZ", 3.0, Vec3d(0, 0, 10));
    sphereZ->getVisualGeometry()->setTranslation(Vec3d(0, 0, 10));
    //scene->addSceneObject(sphere1Obj);
    //scene->addSceneObject(sphere2Obj);

    //auto trackCtrl1 = std::make_shared<imstk::DeviceTracker>(client);
    //trackCtrl1->setTranslationScaling(0.1);
    //auto controller1 = std::make_shared<imstk::SceneObjectController>(sphere1Obj, trackCtrl1);
    //scene->addObjectController(controller1);
    //auto controller2 = std::make_shared<imstk::SceneObjectController>(sphere2Obj, trackCtrl1);
    //scene->addObjectController(controller2);

    //// Light (white)
    //auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
    //whiteLight->setPosition(imstk::Vec3d(-15, -1, 0));
    //whiteLight->setFocalPoint(imstk::Vec3d(1, -1, 0));
    //scene->addLight(whiteLight);
    //auto whiteLight2 = std::make_shared<imstk::PointLight>("whiteLight2");
    //whiteLight2->setPosition(imstk::Vec3d(1, -1, 0));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    ////scene->addLight(whiteLight2);

    //// Light (red)
    //auto colorLight = std::make_shared<imstk::SpotLight>("colorLight");
    //colorLight->setPosition(imstk::Vec3d(-3.5, 2, -28)); // - 25, -8, -4.5
    //colorLight->setFocalPoint(imstk::Vec3d(-1.5, 22, -18)); // - 3.7, 5.5, 7
    //colorLight->setColor(imstk::Color::White); // red
    //colorLight->setSpotAngle(15);
    //auto colorLight2 = std::make_shared<imstk::SpotLight>("colorLight2");
    //colorLight2->setPosition(imstk::Vec3d(-2.5, 17, -23));
    //colorLight2->setFocalPoint(imstk::Vec3d(-2.5, 7, -23));
    //colorLight2->setColor(imstk::Color::White);
    //colorLight2->setSpotAngle(15);
    //auto colorLight3 = std::make_shared<imstk::SpotLight>("colorLight");
    //colorLight3->setPosition(imstk::Vec3d(-2.5, 7, -23));  //-25, -5, -2.5
    //colorLight3->setFocalPoint(imstk::Vec3d(-2.5, 17, -23));  // -3.7, 5.5, 7
    //colorLight3->setColor(imstk::Color::White); // red
    //colorLight3->setSpotAngle(15);
    //scene->addLight(colorLight);
    //scene->addLight(colorLight2);
    //scene->addLight(colorLight3);

    // Light (white)
    auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
    whiteLight->setPosition(imstk::Vec3d(-3.158539, 11.283570, -25.795277));
    //whiteLight->setFocalPoint(imstk::Vec3d(1, -1, 0));
    whiteLight->setIntensity(400);
    scene->addLight(whiteLight);
    auto whiteLight2 = std::make_shared<imstk::PointLight>("whiteLight2");
    whiteLight2->setPosition(imstk::Vec3d(-10, 20, 40));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight2->setIntensity(400);
    scene->addLight(whiteLight2);
    auto whiteLight3 = std::make_shared<imstk::PointLight>("whiteLight3");
    whiteLight3->setPosition(imstk::Vec3d(-60, 20, -5));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight3->setIntensity(400);
    scene->addLight(whiteLight3);
    auto whiteLight4 = std::make_shared<imstk::PointLight>("whiteLight4");
    whiteLight4->setPosition(imstk::Vec3d(35, 20, -5));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight4->setIntensity(400);
    scene->addLight(whiteLight4);
    auto whiteLight5 = std::make_shared<imstk::PointLight>("whiteLight5");
    whiteLight5->setPosition(imstk::Vec3d(-10, -25, -5));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight5->setIntensity(400);
    scene->addLight(whiteLight5);
    auto whiteLight6 = std::make_shared<imstk::PointLight>("whiteLight6");
    whiteLight6->setPosition(imstk::Vec3d(-10, 65, -5));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight6->setIntensity(400);
    scene->addLight(whiteLight6);

    // Light (red)
    auto colorLight = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight->setPosition(imstk::Vec3d(-4.422437, 5.680608, -26.236061)); // - 25, -8, -4.5
    colorLight->setFocalPoint(imstk::Vec3d(0.416648, 18.525057, -13.762144)); // - 3.7, 5.5, 7
    colorLight->setColor(imstk::Color::White); // red
    colorLight->setSpotAngle(70);
    colorLight->setIntensity(200);
    auto colorLight2 = std::make_shared<imstk::SpotLight>("colorLight2");
    colorLight2->setPosition(imstk::Vec3d(-10, 20, -60));
    colorLight2->setFocalPoint(imstk::Vec3d(-10, 20, 15));
    colorLight2->setColor(imstk::Color::White);
    colorLight2->setSpotAngle(70);
    colorLight2->setIntensity(10000);
    auto colorLight3 = std::make_shared<imstk::SpotLight>("colorLight3");
    colorLight3->setPosition(imstk::Vec3d(-2.5, 7, -23));  //-25, -5, -2.5
    colorLight3->setFocalPoint(imstk::Vec3d(-2.5, 17, -23));  // -3.7, 5.5, 7
    colorLight3->setColor(imstk::Color::White); // red
    colorLight3->setSpotAngle(150);
    colorLight3->setIntensity(2000);
    scene->addLight(colorLight);
    //scene->addLight(colorLight2);
    //sceneTest->addLight(colorLight3);

    // Update Camera
    auto cam1 = scene->getCamera();
    cam1->setPosition(imstk::Vec3d(-12.5, -7, -33)); // -25, -8, -4.5
    cam1->setFocalPoint(imstk::Vec3d(2.5, 17, -13)); // -3.7, 5.5, 7
    cam1->setViewAngle(75);


    // model
    // colon wall
    std::string pathColon = DATA_ROOT_PATH"/VESS/colontumor_new_SB.obj"; //  colon_new_002_SB   colon_tumor3_SB cubeTest2_SB  colon_s022.dae"; //  colon_s02.dae"; //  colon_s12.dae"; // volume mesh for colliding // key==colonnew_seg4.dae  colonnew_seg0508.dae small segment// good segment  colonnew_seg4.dae   colonnew_seg3.obj colon_move2.DAE"; //  VESS / colonnew_seg3.obj"; // glove.dae"; //  colon_move2.DAE";    // colon_move2.DAE  colon1_part_max.DAE  colon1_part_max.DAE  colon6_max.DAEcloth.DAE  colon_1_c2_max  colon_cylinder2_tri_max
    std::string pathColon2 = DATA_ROOT_PATH"/VESS/colontumor_s01.obj";  //  colon_s01
    std::string pathColon3 = DATA_ROOT_PATH"/VESS/colonrectum_s003.dae";  // colon_s03
    auto objMesh = imstk::MeshIO::read(pathColon);   // collidingColonMesh; //  imstk::MeshIO::read(pathColon);
    auto objMesh2 = imstk::MeshIO::read(pathColon2);
    auto objMesh3 = imstk::MeshIO::read(pathColon3);
    auto visualColonMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh); // physicsColonMesh->getSurfaceMesh(); //  
    auto visualColonMesh2 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh2);
    auto visualColonMesh3 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh3);
    //visualColonMesh->setTranslation(2.5, -12, 22);
    //visualColonMesh->getVertexPositions();
    //visualColonMesh2->setTranslation(2.5, -12, 22);
    //visualColonMesh3->setTranslation(2.5, -12, 22);
    //visualColonMesh2->getVertexPositions();
    //visualColonMesh3->getVertexPositions();


    //XZH
    FILE *outMesh = fopen("i:/outOBJ.dat", "w");
    //visualColonMesh->getInitialVerticesPositions();
    fprintf(outMesh, "num vertices:%d\n", visualColonMesh->getNumVertices());
    Vec3d tempPos;
    for (int i = 0; i < visualColonMesh->getNumVertices(); ++i){
        tempPos = visualColonMesh->getVertexPosition(i);
        fprintf(outMesh, "%f %f %f\n", tempPos.x(), tempPos.y(), tempPos.z());
    }
    fprintf(outMesh, "\n num tri:%d\n", visualColonMesh->getNumTriangles());
    for (size_t triId = 0; triId < visualColonMesh->getNumTriangles(); ++triId)
    {
        Vec3d center(0, 0, 0);
        auto vert = visualColonMesh->getTrianglesVertices().at(triId); // getTetrahedronVertices(triId);
        fprintf(outMesh, "%d %d %d\n", vert[0], vert[1], vert[2]);
        for (size_t i = 0; i < 3; ++i)
        {
            center = visualColonMesh->getInitialVertexPosition(vert[i]);
            //fprintf(outMesh, "i: %d %f %f %f  ", i, center.x(), center.y(), center.z());
        }
        //fprintf(outMesh, "\n");
    }

    fclose(outMesh);
    // XZH END

    //// Tetrahedral mesh
    imstk::StdVectorOfVec3d vertListTet;
    std::vector<imstk::TetrahedralMesh::TetraArray> tetConnectivity;
    //physXLink linkVolSur;
    std::vector<physXLink> linksVolSur;
    bool flag = loadTetFile(DATA_ROOT_PATH"/VESS/colontumor_new_SB.tet", vertListTet, tetConnectivity, linksVolSur); // colon_new_002_SB colonrectum_s002_SB.tet  colon_tumor3_3_SB for test   colon_tumor3_SB  cubeTest2_SB
    auto tetMesh = std::make_shared<imstk::VESSTetrahedralMesh>();
    //for (int i = 0; i < vertListTet.size();i++)
    //{
    //    vertListTet[i] += Vec3d(2.5, -12, 22);
    //}
    tetMesh->setInitialVertexPositions(vertListTet);
    tetMesh->setVertexPositions(vertListTet);
    tetMesh->setTetrahedraVertices(tetConnectivity);
    tetMesh->setPhysXLink(linksVolSur);
    tetMesh->setInitialPhysVertex(vertListTet);  // add later 06/04/2017
    std::vector<Vec3d> centralLine;
    centralLine.push_back(Vec3d(-2.5, 7, -23));
    centralLine.push_back(Vec3d(-3.5, 15, -23));
    //tetMesh->extractSurfaceMesh(); // both sides of volume mesh
    tetMesh->extractSurfaceMesh(centralLine); // only inside colon of surface of volume mesh


    auto physicsColonMesh = tetMesh; //  collidingColonMesh; //  imstk::MeshIO::read(pathColon);


    auto collidingColonMesh = physicsColonMesh; //  visualColonMesh; //  imstk::MeshIO::read(pathColon);



    auto material = std::make_shared<RenderMaterial>();
    auto diffuseTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    auto normalTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    auto sssTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_sss.png", Texture::Type::SUBSURFACE_SCATTERING);
    material->addTexture(diffuseTexture);
    material->addTexture(normalTexture);
    material->addTexture(roughnessTexture);
    //material->setTexture(metalnessTexture);
    material->addTexture(sssTexture);
    material->setTessellated(true);
    visualColonMesh->setRenderMaterial(material);

    auto material2 = std::make_shared<RenderMaterial>();
    auto diffuseTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    auto normalTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material2->addTexture(diffuseTexture2);
    material2->addTexture(normalTexture2);
    material2->addTexture(roughnessTexture2);
    visualColonMesh2->setRenderMaterial(material2);

    auto material3 = std::make_shared<RenderMaterial>();
    auto diffuseTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    auto normalTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material3->addTexture(diffuseTexture3);
    material3->addTexture(normalTexture3);
    material3->addTexture(roughnessTexture3);
    visualColonMesh3->setRenderMaterial(material3);

    visualColonMesh->computeBoundingBox(min1, max1);
    outpointtri = fopen("i:/AABB.txt", "a");
    fprintf(outpointtri, "V core Colon: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    physicsColonMesh->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "C&P core Colon: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    visualColonMesh2->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "V1 Colon1: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    visualColonMesh3->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "V3 Colon3: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    // floor

    //imstk::StdVectorOfVec3d vertList;
    double width = 60.0;
    double height = 60.0;
    int nRows = 11;
    int nCols = 11;
    int corner[4] = { 1, nRows, nRows*nCols - nCols + 1, nRows*nCols };
    char intStr[33];
    std::string fixed_corner;
    //for (unsigned int i = 0; i < 4; ++i)
    //{
    //	std::sprintf(intStr, "%d", corner[i]);
    //	fixed_corner += std::string(intStr) + ' ';
    //}

    //fixed point
    Vec3d tmpPos;
    std::vector<int> fixedVertice; // add later 06/04/2017
    for (int i = 0; i < physicsColonMesh->getNumVertices(); ++i) // physicsColonMesh  visualColonMesh
    {
        tmpPos = physicsColonMesh->getVertexPosition(i);  // physicsColonMesh  visualColonMesh
        if ((tmpPos.y() < 5) || (tmpPos.y() > 15))  //  || (tmpPos.z() > 3.0   <-1.3  >1.6  smoothed colon segment  (tmpPos.x() < -1.5) || (tmpPos.x() > 1.6)
        {
            std::sprintf(intStr, "%d", i + 1);
            fixed_corner += std::string(intStr) + ' ';
            fixedVertice.push_back(i); // add later 06/04/2017
        }
    }

    fprintf(outpointtri, "V_Colon: Fixed point %s \n ", fixed_corner.c_str());

    // set fixed in the tetmesh add later 06/04/2017
    tetMesh->setFixedVertice(fixedVertice);
    tetMesh->updateFixedVertice();
    tetMesh->initDeformMesh(); // init for deform mesh : injection solution 06/20/2017

    // Plane
    auto geom1 = std::make_shared<imstk::Plane>();
    geom1->setPosition(Vec3d(0, 17, 0));
    //geom1->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    geom1->rotate(imstk::UP_VECTOR, imstk::PI_2);
    geom1->setWidth(30.0);
    geom1->print();
    Vec3d temp = geom1->getNormal();
    fprintf(outpointtri, "plane 1\n norm: %f %f %f\n", temp.x(), temp.y(), temp.z());
    temp = geom1->getPosition();
    fprintf(outpointtri, "position: %f %f %f\n", temp.x(), temp.y(), temp.z());
    auto planeObj1 = std::make_shared<imstk::VisualObject>("VisualPlane1");
    planeObj1->setVisualGeometry(geom1);
    // Plane
    auto geom2 = std::make_shared<imstk::Plane>();
    geom2->setPosition(Vec3d(0, 7, 0));
    //geom2->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    geom2->rotate(imstk::UP_VECTOR, imstk::PI_2);
    geom2->setWidth(30.0);
    geom2->print();
    temp = geom2->getNormal();
    fprintf(outpointtri, "plane 2\n norm: %f %f %f\n", temp.x(), temp.y(), temp.z());
    temp = geom2->getPosition();
    fprintf(outpointtri, "position: %f %f %f\n", temp.x(), temp.y(), temp.z());
    auto planeObj2 = std::make_shared<imstk::VisualObject>("VisualPlane2");
    planeObj2->setVisualGeometry(geom2);
    scene->addSceneObject(planeObj1);
    scene->addSceneObject(planeObj2);

    fclose(outpointtri);

    // Construct a map
    auto colonMapP2V = std::make_shared<imstk::VESSTetraTriangleMap>();  // <imstk::OneToOneMap>();
    colonMapP2V->setMaster(physicsColonMesh);
    colonMapP2V->setSlave(visualColonMesh);
    colonMapP2V->compute();


    auto colonMapP2C = std::make_shared<imstk::OneToOneMap>();
    colonMapP2C->setMaster(physicsColonMesh);
    colonMapP2C->setSlave(collidingColonMesh);
    colonMapP2C->compute();

    auto colonMapC2V = std::make_shared<imstk::VESSTetraTriangleMap>();  //<imstk::OneToOneMap>();
    colonMapC2V->setMaster(collidingColonMesh);
    colonMapC2V->setSlave(visualColonMesh);
    colonMapC2V->compute();

    auto colonObj = std::make_shared<PbdObject>("Colon");
    colonObj->setCollidingGeometry(collidingColonMesh);
    colonObj->setVisualGeometry(visualColonMesh);
    colonObj->setPhysicsGeometry(physicsColonMesh);
    colonObj->setPhysicsToCollidingMap(colonMapP2C);
    colonObj->setPhysicsToVisualMap(colonMapP2V);
    colonObj->setCollidingToVisualMap(colonMapC2V);

    // Create object and add to scene  just for scene, no colliding  for the two sides part, middle for operation part
    auto object2 = std::make_shared<imstk::VisualObject>("visualColonMesh2");
    object2->setVisualGeometry(visualColonMesh2); // change to any mesh created above
    scene->addSceneObject(object2);
    auto object3 = std::make_shared<imstk::VisualObject>("visualColonMesh3");
    object3->setVisualGeometry(visualColonMesh3); // change to any mesh created above
    //scene->addSceneObject(object3);

    // wire frame for surface of volume mesh
    // START
    auto object4 = std::make_shared<imstk::VisualObject>("visualColonMesh4");
    auto material4 = std::make_shared<RenderMaterial>();
    material4->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmp4 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto surfM = tetMesh->getSurfaceMesh();
    material4->addTexture(tmp4);
    surfM->setRenderMaterial(material4);
    object4->setVisualGeometry(surfM); // change to any mesh created above
    //scene->addSceneObject(object4);

    auto object5 = std::make_shared<imstk::VisualObject>("visualColonMesh5");
    auto material5 = std::make_shared<RenderMaterial>();
    material5->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmp5 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto surfM5 = tetMesh->getSurfaceInsideMesh();
    material5->addTexture(tmp5);
    surfM5->setRenderMaterial(material5);
    object5->setVisualGeometry(surfM5); // change to any mesh created above
    //scene->addSceneObject(object5);

    //FILE *outNormal = fopen("G:/surfacenorm.dat","w");
    //fprintf(outNormal, "num:%d\n", surfM->getNumTriangles());
    std::vector<SurfaceMesh::TriangleArray> elements = surfM->getTrianglesVertices();
    for (int i = 0; i < surfM->getNumTriangles() - 1000; i++)
        //if (0)
    {
        //int i = 0;
        Vec3d tmp = surfM->getTriangleNormal(i);
        SurfaceMesh::TriangleArray& e = elements[i];
        const Vec3d v0 = surfM->getVertexPosition(e[0]);
        const Vec3d v1 = surfM->getVertexPosition(e[1]);
        const Vec3d v2 = surfM->getVertexPosition(e[2]);

        Vec3d cenPos = (v0 + v1 + v2) / 3;
        // XZH
        Vec3d p0 = Vec3d(-19.4, -5.85, 1.15);
        Vec3d p1 = Vec3d(-3.3, 2.4, 1.05);
        Vec3d a = cenPos - p0;
        Vec3d b = p1 - p0;
        Vec3d c = ((a.dot(b)) / (b.dot(b)))*b;
        Vec3d ee = a - c;
        if (tmp.dot(-ee) < 0)  // normal indicating the outside colon
        {
            //continue;
            if ((tmp.dot(-ee)) + tmp.norm()*ee.norm() < 0.1)
            {
                printf("cenPos: %f %f %f, -ee: %f %f %f\n", cenPos.x(), cenPos.y(), cenPos.z(), -ee.x(), -ee.y(), -ee.z());
            }
        }
        else
        {
            continue;
            //fprintf(outNormal, "%d\n", i);
        }
        continue;
        // XZH END

        // XZH using for the central line of colon
        auto surfTTT = std::make_shared<imstk::SurfaceMesh>();
        imstk::StdVectorOfVec3d vertListTTT;
        vertListTTT.resize(3);
        vertListTTT[0] = cenPos;
        tmp.normalize();
        vertListTTT[1] = cenPos + tmp;
        vertListTTT[2] = cenPos;
        //fprintf(outNormal, "Pos: %f %f %f 2: %f %f %f\n", cenPos.x(), cenPos.y(), cenPos.z(), cenPos.x() + tmp.x() * 10, cenPos.y() + tmp.y() * 10, cenPos.z() + tmp.z() * 10);

        surfTTT->setInitialVertexPositions(vertListTTT);
        surfTTT->setVertexPositions(vertListTTT);
        // c. Add connectivity data
        std::vector<imstk::SurfaceMesh::TriangleArray> trianglesTTT;
        imstk::SurfaceMesh::TriangleArray triTTT[1];
        triTTT[0] = { { 0, 1, 2 } };
        trianglesTTT.push_back(triTTT[0]);
        surfTTT->setTrianglesVertices(trianglesTTT);
        char str[25];
        sprintf(str, "surfaceNormal %d", i);
        auto objectTTT = std::make_shared<imstk::VisualObject>(str);
        auto materialTTT = std::make_shared<RenderMaterial>();
        materialTTT->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
        auto tmpTTT = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
        materialTTT->addTexture(tmpTTT);
        surfTTT->setRenderMaterial(materialTTT);
        objectTTT->setVisualGeometry(surfTTT); // change to any mesh created above

        scene->addSceneObject(objectTTT);
        continue;

        auto surfTTT1 = std::make_shared<imstk::SurfaceMesh>();
        imstk::StdVectorOfVec3d vertListTTT1;
        vertListTTT1.resize(3);
        vertListTTT1[0] = cenPos;
        Vec3d tmpTTT1 = -ee;
        //tmpTTT1.normalize();
        vertListTTT1[1] = cenPos + tmpTTT1;
        vertListTTT1[2] = cenPos;
        //fprintf(outNormal, "Pos: %f %f %f 2: %f %f %f\n", cenPos.x(), cenPos.y(), cenPos.z(), cenPos.x() + tmp.x() * 10, cenPos.y() + tmp.y() * 10, cenPos.z() + tmp.z() * 10);

        surfTTT1->setInitialVertexPositions(vertListTTT1);
        surfTTT1->setVertexPositions(vertListTTT1);
        // c. Add connectivity data
        std::vector<imstk::SurfaceMesh::TriangleArray> trianglesTTT1;
        imstk::SurfaceMesh::TriangleArray triTTT1[1];
        triTTT1[0] = { { 0, 1, 2 } };
        trianglesTTT1.push_back(triTTT1[0]);
        surfTTT1->setTrianglesVertices(trianglesTTT1);
        sprintf(str, "surfaceLine %d", i);
        auto objectTTT1 = std::make_shared<imstk::VisualObject>(str);
        auto materialTTT1 = std::make_shared<RenderMaterial>();
        materialTTT1->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
        //auto tmpTTTT = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
        //materialTTT1->addTexture(tmpTTTT);
        surfTTT1->setRenderMaterial(materialTTT1);
        objectTTT1->setVisualGeometry(surfTTT1); // change to any mesh created above


        scene->addSceneObject(objectTTT1);
        // XZH END
    }
    //fclose(outNormal);
    FILE *planeInfo = fopen("i:/planenorm.dat", "w");
    // rotate matrix from 2 vectors
    Vec3d nn0 = Vec3d(0, 1, 0);
    Vec3d nn1 = Vec3d(0, 0, 1);   // (-0.95, 1.84, -0.42);
    nn0.normalize();
    nn1.normalize();
    double dot = nn0.dot(nn1);
    if (dot < -1.0) dot = -1.0;
    if (dot > 1.0) dot = 1.0;
    double t = acos(dot); //  acos(nn0.dot(nn1));
    Vec3d nn2 = nn0.cross(nn1);
    nn2.normalize();
    //use angle&angle to make rotate matrix   http://blog.atelier39.org/cg/463.html
    double u = nn2.x();
    double v = nn2.y();
    double w = nn2.z();
    Mat3d rotTmp;
    rotTmp(0, 0) = u*u + (v*v + w*w)*cos(t);  rotTmp(0, 1) = u*v*(1 - cos(t)) - w*sin(t); rotTmp(0, 2) = u*w*(1 - cos(t)) + v*sin(t);
    rotTmp(1, 0) = u*v*(1 - cos(t)) + w*sin(t); rotTmp(1, 1) = v*v + (u*u + w*w)*cos(t); rotTmp(1, 2) = v*w*(1 - cos(t)) - u*sin(t);
    rotTmp(2, 0) = u*w*(1 - cos(t)) - v*sin(t); rotTmp(2, 1) = v*w*(1 - cos(t)) + u*sin(t); rotTmp(2, 2) = w*w + (u*u + v*v)*cos(t);

    // Plane default normal is (0,1,0) position (0,0,0)
    auto plane0 = std::make_shared<imstk::Plane>();
    //plane0->setPosition(Vec3d(0, 10, 0));  // -4.68, -0.63, 1.48
    plane0->translate(Vec3d(-4.68, -0.63, -3.9));  // -4.68, -0.63, 1.48
    //geom1->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    //plane0->rotate(imstk::FORWARD_VECTOR, imstk::PI_2);
    plane0->rotate(rotTmp);
    plane0->setWidth(30.0);
    plane0->print();
    Vec3d ttmp0 = plane0->getNormal();
    fprintf(planeInfo, "plane 1\n norm: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    ttmp0 = plane0->getPosition();
    fprintf(planeInfo, "position: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    auto planeOmni0 = std::make_shared<imstk::CollidingObject>("VisualPlane Omni0");
    planeOmni0->setVisualGeometry(plane0);
    planeOmni0->setCollidingGeometry(plane0);
    //scene->addSceneObject(planeOmni0);

    // Plane default normal is (0,1,0) position (0,0,0)
    auto plane1 = std::make_shared<imstk::Plane>();
    //plane0->setPosition(Vec3d(0, 10, 0));  // -4.68, -0.63, 1.48
    plane1->translate(Vec3d(-4.68, -0.63, 6.8));  // -4.68, -0.63, 1.48
    //geom1->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    //plane0->rotate(imstk::FORWARD_VECTOR, imstk::PI_2);
    plane1->rotate(rotTmp);
    plane1->setWidth(30.0);
    plane1->print();
    ttmp0 = plane1->getNormal();
    fprintf(planeInfo, "plane 2\n norm: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    ttmp0 = plane1->getPosition();
    fprintf(planeInfo, "position: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    auto planeOmni1 = std::make_shared<imstk::CollidingObject>("VisualPlane Omni1");
    planeOmni1->setVisualGeometry(plane1);
    planeOmni1->setCollidingGeometry(plane1);
    //scene->addSceneObject(planeOmni1);
    fclose(planeInfo);
    // END wire frame

    auto pbdColonModel = std::make_shared<PbdModel>();
    pbdColonModel->setModelGeometry(physicsColonMesh);
    pbdColonModel->configure(/*Number of Constraints*/2,
        /*Constraint configuration*/"Volume 0.70",  // 0.7 "Dihedral 0.8", // 0.01 0.8 effect is couhe not very well
        /*Constraint configuration*/"Distance 0.90",  // 0.9 effect is couhe
        /*Mass*/0.1, // 0.1
        /*Gravity*/"0 0 0",  // "0 -9.8 0",
        /*TimeStep*/0.001, // 0.001
        /*FixedPoint*/ fixed_corner.c_str(),
        /*NumberOfIterationInConstraintSolver*/3, //   2 12
        /*Proximity*/0.01,  // 0.1
        /*Contact stiffness*/0.1);
    colonObj->setDynamicalModel(pbdColonModel);

    auto pbdColonSolver = std::make_shared<PbdSolver>();
    pbdColonSolver->setPbdObject(colonObj);
    scene->addNonlinearSolver(pbdColonSolver);

    scene->addSceneObject(colonObj);

    // Collisions
    auto colGraph = scene->getCollisionGraph();
    auto pair = std::make_shared<PbdInteractionPair>(PbdInteractionPair(object, colonObj));
    pair->setNumberOfInterations(3); // 2

    colGraph->addInteractionPair(pair);

    // Sphere-plane
    //auto trackCtrl2 = std::make_shared<imstk::DeviceTracker>(client);
    //trackCtrl2->setTranslationScaling(0.25);  // 0.1  test 0.1 should use  Scale 0.001 will be better 
    ////Create and add virtual coupling object controller in the scene
    //auto objController = std::make_shared<imstk::SceneObjectController>(sphere3Obj, trackCtrl2);
    //scene->addObjectController(objController);
    //colGraph->addInteractionPair(planeOmni0, object,
    //    CollisionDetection::Type::UnidirectionalPlaneToSphere,
    //    CollisionHandling::Type::None,
    //    CollisionHandling::Type::Penalty);

    // Run
    sdk->setActiveScene(scene);
    sdk->startSimulation(true);
#endif
}

//// for viewer and knife visual and colliding mesh is not consistent error 1103 07/18/2017 more dense model merged with tumor for  test force feedback with virtual coupling for descending colon part with new colon model based on Mark- doctor split into three segments  use real knife model 2/3 length of artist
void testPbdVirtualCoupling21_2()
{
#ifdef iMSTK_USE_OPENHAPTICS
    // SDK and Scene
    auto sdk = std::make_shared<imstk::SimulationManager>();
    auto scene = sdk->createNewScene("SceneTestDevice");

    // Device Client
    auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

    // Device Server
    auto server = std::make_shared<imstk::HDAPIDeviceServer>();
    server->addDeviceClient(client);
    sdk->addModule(server);

    std::string path2obj = DATA_ROOT_PATH"/VESS/ITKnife7_6.obj"; // ITKnife7_4.obj knife correct ITKnife6 // cube knife knife3.obj";  // //  113.obj"; //  /  blade2.obj  cube.obj  kcube.obj  test cloth using this 
    std::string path2obj_c = DATA_ROOT_PATH"/VESS/ITKnife7_6.obj"; //  ITKnife7_4.obj  knife correct ITKnife6  // cube knife knife3.obj";  //  //  113.obj";  // / VESS / TriangleTipKnife2.obj";  // blade2.obj  cube.obj kcube_s2.obj
    auto readMesh = imstk::MeshIO::read(path2obj_c); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);

    // Line mesh
    auto lineMesh = std::make_shared<imstk::LineMesh>();
    imstk::StdVectorOfVec3d vertList;
    vertList.resize(2);  // using 5 segment
    //vertList[0] = imstk::Vec3d(-13, -2.25, 9.15); // should - volume mesh therold than surface mesh (9.0, -1.9, -0.6) AABB 0: 9.0, -2.23, -0.6   -0.7 ITKnife6.DAE  // knife3.obj 9.5, -0.9, 1.18
    //vertList[1] = imstk::Vec3d(-5.93, -1.825, 9.125); // 9.7 ITKnife6.DAE // knife3.obj 9.5, -0.9, 8.73
    //vertList[2] = imstk::Vec3d(1.14, -1.4, 9.1); // (9.0, -2.55, 9.5)  AABB 1: 9.0, -2.23, 9.6  9.7 ITKnife6.DAE // knife3.obj 9.5, -0.9, 8.73
    vertList[0] = imstk::Vec3d(-14.4, -1.4, 7.9);
    vertList[1] = imstk::Vec3d(-7.8, -1.2, 7.9);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    std::vector<std::vector<int> > connectivity;
    for (int i = 0; i < 1;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity.push_back(line);
    }

    lineMesh->setInitialVertexPositions(vertList);
    lineMesh->setVertexPositions(vertList);
    lineMesh->setConnectivity(connectivity);

    // using for line mesh display
    auto lineSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertListLine;
    vertListLine.resize(3);
    vertListLine[0] = imstk::Vec3d(-14.4, -1.4, 7.9);
    vertListLine[1] = imstk::Vec3d(-7.8, -1.2, 7.9);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertListLine[2] = imstk::Vec3d(-14.4, -1.4, 7.9);
    std::vector<std::vector<int> > connectivityLine;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivityLine.push_back(line);
    }

    lineSurfMesh->setInitialVertexPositions(vertListLine);
    lineSurfMesh->setVertexPositions(vertListLine);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> trianglesLine;
    imstk::SurfaceMesh::TriangleArray triLine[1];
    triLine[0] = { { 0, 1, 2 } };
    trianglesLine.push_back(triLine[0]);
    lineSurfMesh->setTrianglesVertices(trianglesLine);
    auto objectLine = std::make_shared<imstk::VisualObject>("visualLineMesh");
    auto materialLine = std::make_shared<RenderMaterial>();
    materialLine->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpLine = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialLine->addTexture(tmpLine);
    lineSurfMesh->setRenderMaterial(materialLine);
    objectLine->setVisualGeometry(lineSurfMesh); // change to any mesh created above
    scene->addSceneObject(objectLine);
    // END

    // using for force normal
    auto tempSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertList2;
    vertList2.resize(3);
    vertList2[0] = imstk::Vec3d(-14.4, -1.4, 7.9);
    vertList2[1] = imstk::Vec3d(-7.8, -1.2, 7.9);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertList2[2] = imstk::Vec3d(-14.4, -1.4, 7.9);
    std::vector<std::vector<int> > connectivity2;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity2.push_back(line);
    }

    tempSurfMesh->setInitialVertexPositions(vertList2);
    tempSurfMesh->setVertexPositions(vertList2);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> triangles11;
    imstk::SurfaceMesh::TriangleArray tri[1];
    tri[0] = { { 0, 1, 2 } };
    triangles11.push_back(tri[0]);
    tempSurfMesh->setTrianglesVertices(triangles11);
    auto objectTmp = std::make_shared<imstk::VisualObject>("visualNormal0");
    auto materialTmp = std::make_shared<RenderMaterial>();
    materialTmp->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpTT = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialTmp->addTexture(tmpTT);
    tempSurfMesh->setRenderMaterial(materialTmp);
    objectTmp->setVisualGeometry(tempSurfMesh); // change to any mesh created above
    scene->addSceneObject(objectTmp);
    // END

    // XZH using for the central line of colon
    auto centrallineSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertList3;
    vertList3.resize(3);
    vertList3[0] = imstk::Vec3d(-2.5, 7, -23);
    vertList3[1] = imstk::Vec3d(-3.5, 15, -23);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertList3[2] = imstk::Vec3d(-2.5, 7, -23);
    std::vector<std::vector<int> > connectivity3;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity3.push_back(line);
    }

    centrallineSurfMesh->setInitialVertexPositions(vertList3);
    centrallineSurfMesh->setVertexPositions(vertList3);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> triangles1;
    imstk::SurfaceMesh::TriangleArray tri11[1];
    tri11[0] = { { 0, 1, 2 } };
    triangles1.push_back(tri11[0]);
    centrallineSurfMesh->setTrianglesVertices(triangles1);
    auto objectCentral = std::make_shared<imstk::VisualObject>("visualNormal1");
    auto materialCentral = std::make_shared<RenderMaterial>();
    materialCentral->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpCentral = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialCentral->addTexture(tmpCentral);
    centrallineSurfMesh->setRenderMaterial(materialCentral);
    objectCentral->setVisualGeometry(centrallineSurfMesh); // change to any mesh created above
    //scene->addSceneObject(objectCentral);
    // XZH END

    auto visualMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(readMesh);
    auto physicsMesh = lineMesh; //visualMesh; //  lineMesh; //  imstk::MeshIO::read(path2obj_c);  //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
    auto collidingMesh = lineMesh; // visualMesh; //  lineMesh;
    visualMesh->setRotation(Vec3d(0, 0, 1),PI/4); // move
    Vec3d min1, max1;
    visualMesh->computeBoundingBox(min1, max1);

    auto material0 = std::make_shared<RenderMaterial>();
    auto diffuseTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto normalTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Normal.png", Texture::Type::NORMAL);
    auto roughnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Roughness.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material0->addTexture(diffuseTexture0);
    material0->addTexture(normalTexture0);
    material0->addTexture(roughnessTexture0);
    //material->setTexture(metalnessTexture);
    visualMesh->setRenderMaterial(material0);

    FILE *outpointtri;
    outpointtri = fopen("i:/AABB.txt", "w");
    fprintf(outpointtri, "V: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    collidingMesh->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "C: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    fclose(outpointtri);

    auto object = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject");

    auto objMapP2V = std::make_shared<imstk::OneToOneMap>();  // std::make_shared<imstk::TriangleLineMap>(); //  std::make_shared<imstk::OneToOneMap>();
    objMapP2V->setMaster(physicsMesh);
    objMapP2V->setSlave(visualMesh);
    objMapP2V->compute();


    auto objMapP2C = std::make_shared<imstk::OneToOneMap>();  //  colliding is physical mesh
    objMapP2C->setMaster(physicsMesh);
    objMapP2C->setSlave(collidingMesh);
    objMapP2C->compute();

    auto objMapC2V = std::make_shared<imstk::OneToOneMap>(); //  std::make_shared<imstk::TriangleLineMap>(); //  s std::make_shared<imstk::OneToOneMap>();
    objMapC2V->setMaster(collidingMesh);
    objMapC2V->setSlave(visualMesh);
    objMapC2V->compute();

    auto objMapC2P = std::make_shared<imstk::OneToOneMap>(); // colliding is physical mesh
    objMapC2P->setMaster(collidingMesh);
    objMapC2P->setSlave(physicsMesh);
    objMapC2P->compute();

    object->setVisualGeometry(visualMesh);
    object->setCollidingGeometry(collidingMesh);
    object->setPhysicsGeometry(physicsMesh);
    object->setPhysicsToCollidingMap(objMapP2C);
    object->setPhysicsToVisualMap(objMapP2V);
    object->setCollidingToVisualMap(objMapC2V);
    object->setColldingToPhysicsMap(objMapC2P);

    auto pbdModel = std::make_shared<PbdModel>();
    pbdModel->setModelGeometry(physicsMesh);
    pbdModel->configure(/*Number of Constraints*/1,
        /*Constraint configuration*/"Distance 0.1",
        /*Mass*/0.0,
        /*Gravity*/"0 0 0",
        /*TimeStep*/0.0003,
        /*FixedPoint*/"",
        /*NumberOfIterationInConstraintSolver*/2, // 5
        /*Proximity*/0.3,  // use linemesh 0.3  // 0.01 use  //  cube 0.1
        /*Contact stiffness*/0.001);
    object->setDynamicalModel(pbdModel);

    //auto pbdSolver = std::make_shared<PbdSolver>();
    //pbdSolver->setPbdObject(object);
    //scene->addNonlinearSolver(pbdSolver);

    scene->addSceneObject(object);

    auto trackCtrl = std::make_shared<imstk::DeviceTracker>(client);
    trackCtrl->setTranslationScaling(0.25);  // 0.1  test 0.1 should use  Scale 0.001 will be better 
    auto controller = std::make_shared<imstk::PBDSceneObjectController>(object, trackCtrl);
    scene->addObjectController(controller);

    // Sphere1
    auto sphere1Obj = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "Sphere1", 0.1, Vec3d(-14.4, -1.4, 7.9)); // -2.23
    sphere1Obj->getVisualGeometry()->setTranslation(Vec3d(-14.4, -1.4, 7.9));
    //auto materialTT = std::make_shared<Material>();
    //materialTT->setTexture(diffuseTexture0);
    //auto suMesh = std::dynamic_pointer_cast<SurfaceMesh>(sphere1Obj->getVisualGeometry());
    //suMesh->addMaterial(materialTT);
    //auto sphere2Obj = apiutils::createCollidingAnalyticalSceneObject(
    //    imstk::Geometry::Type::Sphere, scene, "Sphere2", 0.5, Vec3d(-9.3, -1.4, 9.05)); //-9.3, -1.4, 9.05    new -9.53, - 1.50,    8.6  org  -7.2, -1.35, 8.62
    //sphere2Obj->getVisualGeometry()->setTranslation(Vec3d(-9.3, -1.4, 9.05));
    //sphere2Obj->setCollidingGeometry(sphere2Obj->getVisualGeometry());

    auto visualSphere22 = std::make_shared<imstk::Sphere>();
    visualSphere22->setTranslation(Vec3d(-7.8, -1.2, 7.9));
    visualSphere22->setRadius(0.4);
    Vec3d mm2 = visualSphere22->getPosition();
    auto sphere2Obj = std::make_shared<CollidingObject>("Sphere2");
    sphere2Obj->setCollidingGeometry(visualSphere22);
    sphere2Obj->setVisualGeometry(visualSphere22);
    scene->addSceneObject(sphere2Obj);

    auto visualSphere2 = std::make_shared<imstk::Sphere>();
    visualSphere2->setTranslation(Vec3d(-14.4, -1.4, 7.9));
    visualSphere2->setRadius(0.2);
    Vec3d mmm = visualSphere2->getPosition();
    auto sphere3Obj = std::make_shared<CollidingObject>("Sphere3");
    sphere3Obj->setCollidingGeometry(visualSphere2);
    sphere3Obj->setVisualGeometry(visualSphere2);
    //scene->addSceneObject(sphere3Obj);


    auto sphereOrg = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereOrg", 0.2, Vec3d(0, 0, 0));
    sphereOrg->getVisualGeometry()->setTranslation(Vec3d(0, 0, 0));

    auto sphereX = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereX", 0.5, Vec3d(10, 0, 0));
    sphereX->getVisualGeometry()->setTranslation(Vec3d(10, 0, 0));

    auto sphereY = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereY", 1.0, Vec3d(0, 10, 0));
    sphereY->getVisualGeometry()->setTranslation(Vec3d(0, 10, 0));

    auto sphereZ = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereZ", 1.5, Vec3d(0, 0, 10));
    sphereZ->getVisualGeometry()->setTranslation(Vec3d(0, 0, 10));
    //scene->addSceneObject(sphere1Obj);
    //scene->addSceneObject(sphere2Obj);

    //auto trackCtrl1 = std::make_shared<imstk::DeviceTracker>(client);
    //trackCtrl1->setTranslationScaling(0.1);
    //auto controller1 = std::make_shared<imstk::SceneObjectController>(sphere1Obj, trackCtrl1);
    //scene->addObjectController(controller1);
    //auto controller2 = std::make_shared<imstk::SceneObjectController>(sphere2Obj, trackCtrl1);
    //scene->addObjectController(controller2);

    // Light (white)
    auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
    whiteLight->setPosition(imstk::Vec3d(-2.111103, 5.906062, -24.198272));
    whiteLight->setFocalPoint(imstk::Vec3d(-2.111103, 5.906062, -24.198272));
    whiteLight->setIntensity(110);
    scene->addLight(whiteLight);
    auto whiteLight12 = std::make_shared<imstk::PointLight>("whiteLight12");
    whiteLight12->setPosition(imstk::Vec3d(-2.111103, 5.906062, -24.198272));
    whiteLight12->setFocalPoint(imstk::Vec3d(-6.532188, 16.588575, -13.986447));
    whiteLight->setIntensity(200);
    scene->addLight(whiteLight12);

    //// Light (red)
    //auto colorLight = std::make_shared<imstk::SpotLight>("colorLight");
    //colorLight->setPosition(imstk::Vec3d(-3.5, 2, -28)); // - 25, -8, -4.5
    //colorLight->setFocalPoint(imstk::Vec3d(-1.5, 22, -18)); // - 3.7, 5.5, 7
    //colorLight->setColor(imstk::Color::White); // red
    //colorLight->setSpotAngle(15);
    //auto colorLight2 = std::make_shared<imstk::SpotLight>("colorLight2");
    //colorLight2->setPosition(imstk::Vec3d(-2.5, 17, -23));
    //colorLight2->setFocalPoint(imstk::Vec3d(-2.5, 7, -23));
    //colorLight2->setColor(imstk::Color::White);
    //colorLight2->setSpotAngle(15);
    //auto colorLight3 = std::make_shared<imstk::SpotLight>("colorLight");
    //colorLight3->setPosition(imstk::Vec3d(-2.5, 7, -23));  //-25, -5, -2.5
    //colorLight3->setFocalPoint(imstk::Vec3d(-2.5, 17, -23));  // -3.7, 5.5, 7
    //colorLight3->setColor(imstk::Color::White); // red
    //colorLight3->setSpotAngle(15);
    //scene->addLight(colorLight);
    //scene->addLight(colorLight2);
    //scene->addLight(colorLight3);

    // Light (white)
    //auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
    //whiteLight->setPosition(imstk::Vec3d(-3.158539, 11.283570, -25.795277));
    ////whiteLight->setFocalPoint(imstk::Vec3d(1, -1, 0));
    //whiteLight->setIntensity(400);
    //scene->addLight(whiteLight);
    auto whiteLight2 = std::make_shared<imstk::PointLight>("whiteLight2");
    whiteLight2->setPosition(imstk::Vec3d(-10, 20, 40));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight2->setIntensity(200);
    scene->addLight(whiteLight2);
    auto whiteLight3 = std::make_shared<imstk::PointLight>("whiteLight3");
    whiteLight3->setPosition(imstk::Vec3d(-60, 20, -5));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight3->setIntensity(200);
    scene->addLight(whiteLight3);
    auto whiteLight4 = std::make_shared<imstk::PointLight>("whiteLight4");
    whiteLight4->setPosition(imstk::Vec3d(35, 20, -5));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight4->setIntensity(200);
    scene->addLight(whiteLight4);
    auto whiteLight5 = std::make_shared<imstk::PointLight>("whiteLight5");
    whiteLight5->setPosition(imstk::Vec3d(-10, -25, -5));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight5->setIntensity(200);
    scene->addLight(whiteLight5);
    auto whiteLight6 = std::make_shared<imstk::PointLight>("whiteLight6");
    whiteLight6->setPosition(imstk::Vec3d(-10, 65, -5));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight6->setIntensity(200);
    scene->addLight(whiteLight6);

    // Light (red)
    auto colorLight = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight->setPosition(imstk::Vec3d(-2.111103, 5.906062, -24.198272)); // - 25, -8, -4.5
    colorLight->setFocalPoint(imstk::Vec3d(-6.532188, 16.588575, -13.986447)); // - 3.7, 5.5, 7
    colorLight->setColor(imstk::Color::White); // red
    colorLight->setSpotAngle(45);
    colorLight->setIntensity(120);
    auto colorLight2 = std::make_shared<imstk::SpotLight>("colorLight2");
    colorLight2->setPosition(imstk::Vec3d(-10, 20, -60));
    colorLight2->setFocalPoint(imstk::Vec3d(-10, 20, 15));
    colorLight2->setColor(imstk::Color::White);
    colorLight2->setSpotAngle(70);
    colorLight2->setIntensity(10000);
    auto colorLight3 = std::make_shared<imstk::SpotLight>("colorLight3");
    colorLight3->setPosition(imstk::Vec3d(-2.5, 7, -23));  //-25, -5, -2.5
    colorLight3->setFocalPoint(imstk::Vec3d(-2.5, 17, -23));  // -3.7, 5.5, 7
    colorLight3->setColor(imstk::Color::White); // red
    colorLight3->setSpotAngle(70);
    colorLight3->setIntensity(100);
    //scene->addLight(colorLight);
    //scene->addLight(colorLight2);
    //sceneTest->addLight(colorLight3);

    // Update Camera
    // navgation
    vector<Vec3d> vecCamera;
    vector<Vec3d> vecCameraFocal;
    vecCamera.push_back(Vec3d(3.049765, -5.416139, -13.772857));
    vecCamera.push_back(Vec3d(2.066011, -2.092456, -16.283300));
    vecCamera.push_back(Vec3d(2.416454, 0.893009, -18.732140));
    vecCamera.push_back(Vec3d(2.656587, 3.308475, -21.841312));
    vecCamera.push_back(Vec3d(0.744602, 4.685533, -24.712399));
    vecCamera.push_back(Vec3d(-2.111103, 5.906062, -24.198272));

    vecCameraFocal.push_back(Vec3d(-4.065353, 2.550728, -18.045065));
    vecCameraFocal.push_back(Vec3d(-2.318216, 3.125296, -19.496111));
    vecCameraFocal.push_back(Vec3d(-0.275814, 4.097096, -20.704975));
    vecCameraFocal.push_back(Vec3d(-0.668360, 6.501790, -21.953945));
    vecCameraFocal.push_back(Vec3d(-0.777570, 7.189897, -22.593023));
    vecCameraFocal.push_back(Vec3d(-6.532188, 16.588575, -13.986447));
    auto cam1 = scene->getCamera();
    cam1->setPosition(vecCamera[5]); //  (Vec3d(3.049765, -5.416139, -13.772857)); // (2.416454, 0.893009, -18.732140)); // (Vec3d(-2.111103, 5.906062, -24.198272)); // (imstk::Vec3d(-12.5, -7, -33)); // -25, -8, -4.5
    cam1->setFocalPoint(vecCameraFocal[5]); //  (Vec3d(-4.065353, 2.550728, -18.045065)); // -0.275814, 4.097096, -20.704975)); // (-6.532188, 16.588575, -13.986447); //  (imstk::Vec3d(2.5, 17, -13)); // -3.7, 5.5, 7
    cam1->setViewAngle(75);


    // model
    // colon wall
    std::string pathColon = DATA_ROOT_PATH"/VESS/colontumor_new_SB.obj"; //  colon_new_002_SB   colon_tumor3_SB cubeTest2_SB  colon_s022.dae"; //  colon_s02.dae"; //  colon_s12.dae"; // volume mesh for colliding // key==colonnew_seg4.dae  colonnew_seg0508.dae small segment// good segment  colonnew_seg4.dae   colonnew_seg3.obj colon_move2.DAE"; //  VESS / colonnew_seg3.obj"; // glove.dae"; //  colon_move2.DAE";    // colon_move2.DAE  colon1_part_max.DAE  colon1_part_max.DAE  colon6_max.DAEcloth.DAE  colon_1_c2_max  colon_cylinder2_tri_max
    std::string pathColon2 = DATA_ROOT_PATH"/VESS/colontumor_s01.obj";  //  colon_s01
    std::string pathColon3 = DATA_ROOT_PATH"/VESS/colontumor_new_S003.obj";  // colon_s03
    auto objMesh = imstk::MeshIO::read(pathColon);   // collidingColonMesh; //  imstk::MeshIO::read(pathColon);
    auto objMesh2 = imstk::MeshIO::read(pathColon2);
    auto objMesh3 = imstk::MeshIO::read(pathColon3);
    auto visualColonMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh); // physicsColonMesh->getSurfaceMesh(); //  
    auto visualColonMesh2 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh2);
    auto visualColonMesh3 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh3);
    //visualColonMesh->setTranslation(2.5, -12, 22);
    //visualColonMesh->getVertexPositions();
    //visualColonMesh2->setTranslation(2.5, -12, 22);
    //visualColonMesh3->setTranslation(2.5, -12, 22);
    //visualColonMesh2->getVertexPositions();
    //visualColonMesh3->getVertexPositions();


    //XZH
    FILE *outMesh = fopen("i:/outOBJ.dat", "w");
    //visualColonMesh->getInitialVerticesPositions();
    fprintf(outMesh, "num vertices:%d\n", visualColonMesh->getNumVertices());
    Vec3d tempPos;
    for (int i = 0; i < visualColonMesh->getNumVertices(); ++i){
        tempPos = visualColonMesh->getVertexPosition(i);
        fprintf(outMesh, "%f %f %f\n", tempPos.x(), tempPos.y(), tempPos.z());
    }
    fprintf(outMesh, "\n num tri:%d\n", visualColonMesh->getNumTriangles());
    for (size_t triId = 0; triId < visualColonMesh->getNumTriangles(); ++triId)
    {
        Vec3d center(0, 0, 0);
        auto vert = visualColonMesh->getTrianglesVertices().at(triId); // getTetrahedronVertices(triId);
        fprintf(outMesh, "%d %d %d\n", vert[0], vert[1], vert[2]);
        for (size_t i = 0; i < 3; ++i)
        {
            center = visualColonMesh->getInitialVertexPosition(vert[i]);
            //fprintf(outMesh, "i: %d %f %f %f  ", i, center.x(), center.y(), center.z());
        }
        //fprintf(outMesh, "\n");
    }

    fclose(outMesh);
    // XZH END

    //// Tetrahedral mesh
    imstk::StdVectorOfVec3d vertListTet;
    std::vector<imstk::TetrahedralMesh::TetraArray> tetConnectivity;
    //physXLink linkVolSur;
    std::vector<physXLink> linksVolSur;
    bool flag = loadTetFile(DATA_ROOT_PATH"/VESS/colontumor_new_SB.tet", vertListTet, tetConnectivity, linksVolSur); // colon_new_002_SB colonrectum_s002_SB.tet  colon_tumor3_3_SB for test   colon_tumor3_SB  cubeTest2_SB
    auto tetMesh = std::make_shared<imstk::CuttingVESSCutMesh>(); // VESSTetrahedralMesh
    //for (int i = 0; i < vertListTet.size();i++)
    //{
    //    vertListTet[i] += Vec3d(2.5, -12, 22);
    //}
    tetMesh->setInitialVertexPositions(vertListTet);
    tetMesh->setVertexPositions(vertListTet);
    tetMesh->setTetrahedraVertices(tetConnectivity);
    tetMesh->setPhysXLink(linksVolSur);
    tetMesh->setInitialPhysVertex(vertListTet);  // add later 06/04/2017

    tetMesh->initializeCuttingMesh();
    tetMesh->initializeCutting();
    tetMesh->extractSurfaceMesh();
    tetMesh->extractSurfaceMeshVolume();
    //auto cutTissueMesh = std::make_shared<imstk::CuttingVESSCutMesh>(tetMesh);
    tetMesh->setFlagSplitMeshAfterCut(true);

    //Vec3d min1, max1;
    tetMesh->computeBoundingBox(min1, max1);

    std::vector<Vec3d> centralLine;
    centralLine.push_back(Vec3d(-2.5, 7, -23));
    centralLine.push_back(Vec3d(-3.5, 15, -23));
    //tetMesh->extractSurfaceMesh(); // both sides of volume mesh
    tetMesh->extractSurfaceMesh(centralLine); // only inside colon of surface of volume mesh


    auto physicsColonMesh = tetMesh; //  collidingColonMesh; //  imstk::MeshIO::read(pathColon);


    auto collidingColonMesh = physicsColonMesh; //  visualColonMesh; //  imstk::MeshIO::read(pathColon);



    auto material = std::make_shared<RenderMaterial>();
    auto diffuseTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    auto normalTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    auto sssTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_sss.png", Texture::Type::SUBSURFACE_SCATTERING);
    material->addTexture(diffuseTexture);
    material->addTexture(normalTexture);
    material->addTexture(roughnessTexture);
    material->setDisplayMode(RenderMaterial::DisplayMode::SURFACE);
    //material->setTexture(metalnessTexture);
    material->addTexture(sssTexture);
    material->setTessellated(true);
    visualColonMesh->setRenderMaterial(material);

    auto material2 = std::make_shared<RenderMaterial>();
    auto diffuseTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    auto normalTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material2->addTexture(diffuseTexture2);
    material2->addTexture(normalTexture2);
    material2->addTexture(roughnessTexture2);
    visualColonMesh2->setRenderMaterial(material2);

    auto material3 = std::make_shared<RenderMaterial>();
    auto diffuseTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    auto normalTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material3->addTexture(diffuseTexture3);
    material3->addTexture(normalTexture3);
    material3->addTexture(roughnessTexture3);
    visualColonMesh3->setRenderMaterial(material3);

    visualColonMesh->computeBoundingBox(min1, max1);
    outpointtri = fopen("i:/AABB.txt", "a");
    fprintf(outpointtri, "V core Colon: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    physicsColonMesh->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "C&P core Colon: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    visualColonMesh2->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "V1 Colon1: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    visualColonMesh3->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "V3 Colon3: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    // floor

    //imstk::StdVectorOfVec3d vertList;
    double width = 60.0;
    double height = 60.0;
    int nRows = 11;
    int nCols = 11;
    int corner[4] = { 1, nRows, nRows*nCols - nCols + 1, nRows*nCols };
    char intStr[33];
    std::string fixed_corner;
    //for (unsigned int i = 0; i < 4; ++i)
    //{
    //	std::sprintf(intStr, "%d", corner[i]);
    //	fixed_corner += std::string(intStr) + ' ';
    //}

    //fixed point
    Vec3d tmpPos;
    std::vector<int> fixedVertice; // add later 06/04/2017
    for (int i = 0; i < physicsColonMesh->getNumVertices(); ++i) // physicsColonMesh  visualColonMesh
    {
        tmpPos = physicsColonMesh->getVertexPosition(i);  // physicsColonMesh  visualColonMesh
        if ((tmpPos.y() < 5) || (tmpPos.y() > 15))  //  || (tmpPos.z() > 3.0   <-1.3  >1.6  smoothed colon segment  (tmpPos.x() < -1.5) || (tmpPos.x() > 1.6)
        {
            std::sprintf(intStr, "%d", i + 1);
            fixed_corner += std::string(intStr) + ' ';
            fixedVertice.push_back(i); // add later 06/04/2017
        }
    }

    fprintf(outpointtri, "V_Colon: Fixed point %s \n ", fixed_corner.c_str());

    // set fixed in the tetmesh add later 06/04/2017
    tetMesh->setFixedVertice(fixedVertice);
    tetMesh->updateFixedVertice();
    tetMesh->initDeformMesh(); // init for deform mesh : injection solution 06/20/2017

    // Plane
    auto geom1 = std::make_shared<imstk::Plane>();
    geom1->setPosition(Vec3d(0, 17, 0));
    //geom1->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    geom1->rotate(imstk::UP_VECTOR, imstk::PI_2);
    geom1->setWidth(30.0);
    geom1->print();
    Vec3d temp = geom1->getNormal();
    fprintf(outpointtri, "plane 1\n norm: %f %f %f\n", temp.x(), temp.y(), temp.z());
    temp = geom1->getPosition();
    fprintf(outpointtri, "position: %f %f %f\n", temp.x(), temp.y(), temp.z());
    auto planeObj1 = std::make_shared<imstk::VisualObject>("VisualPlane1");
    planeObj1->setVisualGeometry(geom1);
    // Plane
    auto geom2 = std::make_shared<imstk::Plane>();
    geom2->setPosition(Vec3d(0, 7, 0));
    //geom2->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    geom2->rotate(imstk::UP_VECTOR, imstk::PI_2);
    geom2->setWidth(30.0);
    geom2->print();
    temp = geom2->getNormal();
    fprintf(outpointtri, "plane 2\n norm: %f %f %f\n", temp.x(), temp.y(), temp.z());
    temp = geom2->getPosition();
    fprintf(outpointtri, "position: %f %f %f\n", temp.x(), temp.y(), temp.z());
    auto planeObj2 = std::make_shared<imstk::VisualObject>("VisualPlane2");
    planeObj2->setVisualGeometry(geom2);
    scene->addSceneObject(planeObj1);
    scene->addSceneObject(planeObj2);

    fclose(outpointtri);

    // Construct a map
    auto colonMapP2V = std::make_shared<imstk::VESSTetraTriangleMap>();  // <imstk::OneToOneMap>();
    colonMapP2V->setMaster(physicsColonMesh);
    colonMapP2V->setSlave(visualColonMesh);
    colonMapP2V->compute();


    auto colonMapP2C = std::make_shared<imstk::OneToOneMap>();
    colonMapP2C->setMaster(physicsColonMesh);
    colonMapP2C->setSlave(collidingColonMesh);
    colonMapP2C->compute();

    auto colonMapC2V = std::make_shared<imstk::VESSTetraTriangleMap>();  //<imstk::OneToOneMap>();
    colonMapC2V->setMaster(collidingColonMesh);
    colonMapC2V->setSlave(visualColonMesh);
    colonMapC2V->compute();

    auto colonObj = std::make_shared<PbdObject>("Colon");
    colonObj->setCollidingGeometry(collidingColonMesh);
    colonObj->setVisualGeometry(visualColonMesh);
    colonObj->setPhysicsGeometry(physicsColonMesh);
    colonObj->setPhysicsToCollidingMap(colonMapP2C);
    colonObj->setPhysicsToVisualMap(colonMapP2V);
    colonObj->setCollidingToVisualMap(colonMapC2V);

    // Create object and add to scene  just for scene, no colliding  for the two sides part, middle for operation part
    auto object2 = std::make_shared<imstk::VisualObject>("visualColonMesh2");
    object2->setVisualGeometry(visualColonMesh2); // change to any mesh created above
    scene->addSceneObject(object2);
    auto object3 = std::make_shared<imstk::VisualObject>("visualColonMesh3");
    object3->setVisualGeometry(visualColonMesh3); // change to any mesh created above
    scene->addSceneObject(object3);

    // wire frame for surface of volume mesh
    // START
    auto object4 = std::make_shared<imstk::VisualObject>("visualColonMesh4");
    auto material4 = std::make_shared<RenderMaterial>();
    material4->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmp4 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto surfM = tetMesh->getSurfaceMesh();
    material4->addTexture(tmp4);
    surfM->setRenderMaterial(material4);
    object4->setVisualGeometry(surfM); // change to any mesh created above
    //scene->addSceneObject(object4);

    auto object5 = std::make_shared<imstk::VisualObject>("visualColonMesh5");
    auto material5 = std::make_shared<RenderMaterial>();
    material5->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmp5 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto surfM5 = tetMesh->getSurfaceInsideMesh();
    material5->addTexture(tmp5);
    surfM5->setRenderMaterial(material5);
    object5->setVisualGeometry(surfM5); // change to any mesh created above
    //scene->addSceneObject(object5);

    //FILE *outNormal = fopen("G:/surfacenorm.dat","w");
    //fprintf(outNormal, "num:%d\n", surfM->getNumTriangles());
    std::vector<SurfaceMesh::TriangleArray> elements = surfM->getTrianglesVertices();
    for (int i = 0; i < surfM->getNumTriangles() - 1000; i++)
        //if (0)
    {
        //int i = 0;
        Vec3d tmp = surfM->getTriangleNormal(i);
        SurfaceMesh::TriangleArray& e = elements[i];
        const Vec3d v0 = surfM->getVertexPosition(e[0]);
        const Vec3d v1 = surfM->getVertexPosition(e[1]);
        const Vec3d v2 = surfM->getVertexPosition(e[2]);

        Vec3d cenPos = (v0 + v1 + v2) / 3;
        // XZH
        Vec3d p0 = Vec3d(-19.4, -5.85, 1.15);
        Vec3d p1 = Vec3d(-3.3, 2.4, 1.05);
        Vec3d a = cenPos - p0;
        Vec3d b = p1 - p0;
        Vec3d c = ((a.dot(b)) / (b.dot(b)))*b;
        Vec3d ee = a - c;
        if (tmp.dot(-ee) < 0)  // normal indicating the outside colon
        {
            //continue;
            if ((tmp.dot(-ee)) + tmp.norm()*ee.norm() < 0.1)
            {
                printf("cenPos: %f %f %f, -ee: %f %f %f\n", cenPos.x(), cenPos.y(), cenPos.z(), -ee.x(), -ee.y(), -ee.z());
            }
        }
        else
        {
            continue;
            //fprintf(outNormal, "%d\n", i);
        }
        continue;
        // XZH END

        // XZH using for the central line of colon
        auto surfTTT = std::make_shared<imstk::SurfaceMesh>();
        imstk::StdVectorOfVec3d vertListTTT;
        vertListTTT.resize(3);
        vertListTTT[0] = cenPos;
        tmp.normalize();
        vertListTTT[1] = cenPos + tmp;
        vertListTTT[2] = cenPos;
        //fprintf(outNormal, "Pos: %f %f %f 2: %f %f %f\n", cenPos.x(), cenPos.y(), cenPos.z(), cenPos.x() + tmp.x() * 10, cenPos.y() + tmp.y() * 10, cenPos.z() + tmp.z() * 10);

        surfTTT->setInitialVertexPositions(vertListTTT);
        surfTTT->setVertexPositions(vertListTTT);
        // c. Add connectivity data
        std::vector<imstk::SurfaceMesh::TriangleArray> trianglesTTT;
        imstk::SurfaceMesh::TriangleArray triTTT[1];
        triTTT[0] = { { 0, 1, 2 } };
        trianglesTTT.push_back(triTTT[0]);
        surfTTT->setTrianglesVertices(trianglesTTT);
        char str[25];
        sprintf(str, "surfaceNormal %d", i);
        auto objectTTT = std::make_shared<imstk::VisualObject>(str);
        auto materialTTT = std::make_shared<RenderMaterial>();
        materialTTT->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
        auto tmpTTT = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
        materialTTT->addTexture(tmpTTT);
        surfTTT->setRenderMaterial(materialTTT);
        objectTTT->setVisualGeometry(surfTTT); // change to any mesh created above

        scene->addSceneObject(objectTTT);
        continue;

        auto surfTTT1 = std::make_shared<imstk::SurfaceMesh>();
        imstk::StdVectorOfVec3d vertListTTT1;
        vertListTTT1.resize(3);
        vertListTTT1[0] = cenPos;
        Vec3d tmpTTT1 = -ee;
        //tmpTTT1.normalize();
        vertListTTT1[1] = cenPos + tmpTTT1;
        vertListTTT1[2] = cenPos;
        //fprintf(outNormal, "Pos: %f %f %f 2: %f %f %f\n", cenPos.x(), cenPos.y(), cenPos.z(), cenPos.x() + tmp.x() * 10, cenPos.y() + tmp.y() * 10, cenPos.z() + tmp.z() * 10);

        surfTTT1->setInitialVertexPositions(vertListTTT1);
        surfTTT1->setVertexPositions(vertListTTT1);
        // c. Add connectivity data
        std::vector<imstk::SurfaceMesh::TriangleArray> trianglesTTT1;
        imstk::SurfaceMesh::TriangleArray triTTT1[1];
        triTTT1[0] = { { 0, 1, 2 } };
        trianglesTTT1.push_back(triTTT1[0]);
        surfTTT1->setTrianglesVertices(trianglesTTT1);
        sprintf(str, "surfaceLine %d", i);
        auto objectTTT1 = std::make_shared<imstk::VisualObject>(str);
        auto materialTTT1 = std::make_shared<RenderMaterial>();
        materialTTT1->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
        //auto tmpTTTT = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
        //materialTTT1->addTexture(tmpTTTT);
        surfTTT1->setRenderMaterial(materialTTT1);
        objectTTT1->setVisualGeometry(surfTTT1); // change to any mesh created above


        scene->addSceneObject(objectTTT1);
        // XZH END
    }
    //fclose(outNormal);
    FILE *planeInfo = fopen("i:/planenorm.dat", "w");
    // rotate matrix from 2 vectors
    Vec3d nn0 = Vec3d(0, 1, 0);
    Vec3d nn1 = Vec3d(0, 0, 1);   // (-0.95, 1.84, -0.42);
    nn0.normalize();
    nn1.normalize();
    double dot = nn0.dot(nn1);
    if (dot < -1.0) dot = -1.0;
    if (dot > 1.0) dot = 1.0;
    double t = acos(dot); //  acos(nn0.dot(nn1));
    Vec3d nn2 = nn0.cross(nn1);
    nn2.normalize();
    //use angle&angle to make rotate matrix   http://blog.atelier39.org/cg/463.html
    double u = nn2.x();
    double v = nn2.y();
    double w = nn2.z();
    Mat3d rotTmp;
    rotTmp(0, 0) = u*u + (v*v + w*w)*cos(t);  rotTmp(0, 1) = u*v*(1 - cos(t)) - w*sin(t); rotTmp(0, 2) = u*w*(1 - cos(t)) + v*sin(t);
    rotTmp(1, 0) = u*v*(1 - cos(t)) + w*sin(t); rotTmp(1, 1) = v*v + (u*u + w*w)*cos(t); rotTmp(1, 2) = v*w*(1 - cos(t)) - u*sin(t);
    rotTmp(2, 0) = u*w*(1 - cos(t)) - v*sin(t); rotTmp(2, 1) = v*w*(1 - cos(t)) + u*sin(t); rotTmp(2, 2) = w*w + (u*u + v*v)*cos(t);

    // Plane default normal is (0,1,0) position (0,0,0)
    auto plane0 = std::make_shared<imstk::Plane>();
    //plane0->setPosition(Vec3d(0, 10, 0));  // -4.68, -0.63, 1.48
    plane0->translate(Vec3d(-4.68, -0.63, -3.9));  // -4.68, -0.63, 1.48
    //geom1->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    //plane0->rotate(imstk::FORWARD_VECTOR, imstk::PI_2);
    plane0->rotate(rotTmp);
    plane0->setWidth(30.0);
    plane0->print();
    Vec3d ttmp0 = plane0->getNormal();
    fprintf(planeInfo, "plane 1\n norm: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    ttmp0 = plane0->getPosition();
    fprintf(planeInfo, "position: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    auto planeOmni0 = std::make_shared<imstk::CollidingObject>("VisualPlane Omni0");
    planeOmni0->setVisualGeometry(plane0);
    planeOmni0->setCollidingGeometry(plane0);
    //scene->addSceneObject(planeOmni0);

    // Plane default normal is (0,1,0) position (0,0,0)
    auto plane1 = std::make_shared<imstk::Plane>();
    //plane0->setPosition(Vec3d(0, 10, 0));  // -4.68, -0.63, 1.48
    plane1->translate(Vec3d(-4.68, -0.63, 6.8));  // -4.68, -0.63, 1.48
    //geom1->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    //plane0->rotate(imstk::FORWARD_VECTOR, imstk::PI_2);
    plane1->rotate(rotTmp);
    plane1->setWidth(30.0);
    plane1->print();
    ttmp0 = plane1->getNormal();
    fprintf(planeInfo, "plane 2\n norm: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    ttmp0 = plane1->getPosition();
    fprintf(planeInfo, "position: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    auto planeOmni1 = std::make_shared<imstk::CollidingObject>("VisualPlane Omni1");
    planeOmni1->setVisualGeometry(plane1);
    planeOmni1->setCollidingGeometry(plane1);
    //scene->addSceneObject(planeOmni1);
    fclose(planeInfo);
    // END wire frame

    auto pbdColonModel = std::make_shared<PbdModel>();
    pbdColonModel->setModelGeometry(physicsColonMesh);
    pbdColonModel->configure(/*Number of Constraints*/2,
        /*Constraint configuration*/"Volume 0.70",  // 0.7 "Dihedral 0.8", // 0.01 0.8 effect is couhe not very well
        /*Constraint configuration*/"Distance 0.90",  // 0.9 effect is couhe
        /*Mass*/0.1, // 0.1
        /*Gravity*/"0 0 0",  // "0 -9.8 0",
        /*TimeStep*/0.001, // 0.001
        /*FixedPoint*/ fixed_corner.c_str(),
        /*NumberOfIterationInConstraintSolver*/3, //   2 12
        /*Proximity*/0.01,  // 0.1
        /*Contact stiffness*/0.1);
    colonObj->setDynamicalModel(pbdColonModel);

    auto pbdColonSolver = std::make_shared<PbdSolver>();
    pbdColonSolver->setPbdObject(colonObj);
    scene->addNonlinearSolver(pbdColonSolver);

    scene->addSceneObject(colonObj);

    // Collisions
    auto colGraph = scene->getCollisionGraph();
    auto pair = std::make_shared<PbdInteractionPair>(PbdInteractionPair(object, colonObj));
    pair->setNumberOfInterations(3); // 2

    colGraph->addInteractionPair(pair);

    // Sphere-plane
    //auto trackCtrl2 = std::make_shared<imstk::DeviceTracker>(client);
    //trackCtrl2->setTranslationScaling(0.25);  // 0.1  test 0.1 should use  Scale 0.001 will be better 
    ////Create and add virtual coupling object controller in the scene
    //auto objController = std::make_shared<imstk::SceneObjectController>(sphere3Obj, trackCtrl2);
    //scene->addObjectController(objController);
    //colGraph->addInteractionPair(planeOmni0, object,
    //    CollisionDetection::Type::UnidirectionalPlaneToSphere,
    //    CollisionHandling::Type::None,
    //    CollisionHandling::Type::Penalty);

    // Marking Test
    Sleep(100);
    vector<Vec3d> markingPosList;
    markingPosList.push_back(Vec3d(-1.385700, 8.899301, -18.955198));
    markingPosList.push_back(Vec3d(-1.859606, 10.204217, -18.999193));
    markingPosList.push_back(Vec3d(-2.510700, 10.712403, -19.638699));
    markingPosList.push_back(Vec3d(-3.472900, 10.740303, -19.968397));
    markingPosList.push_back(Vec3d(-4.309300, 10.330703, -20.379998));
    markingPosList.push_back(Vec3d(-4.922279, 9.261862, -20.488335));
    markingPosList.push_back(Vec3d(-4.277007, 8.486430, -20.165256));
    markingPosList.push_back(Vec3d(-3.185467, 7.896873, -19.752643));
    markingPosList.push_back(Vec3d(-1.623400, 8.069003, -19.261698));
    vector<int> vertVisualList;
    vertVisualList.push_back(39);
    vertVisualList.push_back(165);
    vertVisualList.push_back(110);
    vertVisualList.push_back(106);
    vertVisualList.push_back(102);
    vertVisualList.push_back(163);
    vertVisualList.push_back(220);
    vertVisualList.push_back(9);
    vertVisualList.push_back(3);
    StdVectorOfVec3d initPoses = visualColonMesh->getInitialVertexPositions();
    for (int numMarking = 0; numMarking < 9; numMarking++)
    {
        Vec3d markingPos = markingPosList[numMarking];

        //for (int k = 0; k < initPoses.size(); k++)
        //{
        //    Vec3d& pos = initPoses[k];
        //    if ((markingPos - pos).norm()<0.20)
        //    {
        //        vertVisualList.push_back(k);
        //        printf("markink index IDX: %d, visual idx %d\n", numMarking, k);
        //    }
        //}

        char name[256];
        sprintf(name, "XZHSphere%d", numMarking);
        auto visualSphere22 = std::make_shared<imstk::Sphere>();
        visualSphere22->setTranslation(markingPos);
        visualSphere22->setRadius(0.2);
        auto sphere2Obj = std::make_shared<SceneObject>(name);
        sphere2Obj->setVisualGeometry(visualSphere22);
        scene->addSceneObject(sphere2Obj);

        auto decalTest = std::make_shared<DecalPool>();
        auto decalMaterial = std::make_shared<RenderMaterial>();
        auto diffuseTexture2 = std::make_shared<Texture>("I:/iMSTK/resources/textures/test4.png", Texture::DIFFUSE);
        decalMaterial->addTexture(diffuseTexture2);
        auto decal1 = decalTest->addDecal();
        decal1->setPosition(markingPos);
        decal1->rotate(RIGHT_VECTOR, PI_4);
        decal1->scale(0.4);
        decalTest->setRenderMaterial(decalMaterial);

        sprintf(name, "DecalObject%d", numMarking);
        auto decalObject = std::make_shared<VisualObject>(name); // "DecalObject"
        decalObject->setVisualGeometry(decalTest);
        scene->addSceneObject(decalObject);
    }

    // Run
    sdk->setActiveScene(scene);
    sdk->startSimulation(true);
#endif
}

//// 07/18/2017 more dense model merged with tumor for  test force feedback with virtual coupling for descending colon part with new colon model based on Mark- doctor split into three segments  use real knife model 2/3 length of artist
void testPbdVirtualCoupling21()
{
#ifdef iMSTK_USE_OPENHAPTICS
    // SDK and Scene
    auto sdk = std::make_shared<imstk::SimulationManager>();
    auto scene = sdk->createNewScene("SceneTestDevice");

    // Device Client
    auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

    // Device Server
    auto server = std::make_shared<imstk::HDAPIDeviceServer>();
    server->addDeviceClient(client);
    sdk->addModule(server);

    std::string path2obj = DATA_ROOT_PATH"/VESS/ITKnife7_4.obj"; // ITKnife7_4.obj knife correct ITKnife6 // cube knife knife3.obj";  // //  113.obj"; //  /  blade2.obj  cube.obj  kcube.obj  test cloth using this 
    std::string path2obj_c = DATA_ROOT_PATH"/VESS/ITKnife7_4.obj"; //  ITKnife7_4.obj  knife correct ITKnife6  // cube knife knife3.obj";  //  //  113.obj";  // / VESS / TriangleTipKnife2.obj";  // blade2.obj  cube.obj kcube_s2.obj
    auto readMesh = imstk::MeshIO::read(path2obj_c); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);

    // Line mesh
    auto lineMesh = std::make_shared<imstk::LineMesh>();
    imstk::StdVectorOfVec3d vertList;
    vertList.resize(2);  // using 5 segment
    //vertList[0] = imstk::Vec3d(-13, -2.25, 9.15); // should - volume mesh therold than surface mesh (9.0, -1.9, -0.6) AABB 0: 9.0, -2.23, -0.6   -0.7 ITKnife6.DAE  // knife3.obj 9.5, -0.9, 1.18
    //vertList[1] = imstk::Vec3d(-5.93, -1.825, 9.125); // 9.7 ITKnife6.DAE // knife3.obj 9.5, -0.9, 8.73
    //vertList[2] = imstk::Vec3d(1.14, -1.4, 9.1); // (9.0, -2.55, 9.5)  AABB 1: 9.0, -2.23, 9.6  9.7 ITKnife6.DAE // knife3.obj 9.5, -0.9, 8.73
    vertList[0] = imstk::Vec3d(-14.4, -1.4, 7.9);
    vertList[1] = imstk::Vec3d(-7.8, -1.2, 7.9);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    std::vector<std::vector<int> > connectivity;
    for (int i = 0; i < 1;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity.push_back(line);
    }

    lineMesh->setInitialVertexPositions(vertList);
    lineMesh->setVertexPositions(vertList);
    lineMesh->setConnectivity(connectivity);

    // using for line mesh display
    auto lineSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertListLine;
    vertListLine.resize(3);
    vertListLine[0] = imstk::Vec3d(-14.4, -1.4, 7.9);
    vertListLine[1] = imstk::Vec3d(-7.8, -1.2, 7.9);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertListLine[2] = imstk::Vec3d(-14.4, -1.4, 7.9);
    std::vector<std::vector<int> > connectivityLine;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivityLine.push_back(line);
    }

    lineSurfMesh->setInitialVertexPositions(vertListLine);
    lineSurfMesh->setVertexPositions(vertListLine);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> trianglesLine;
    imstk::SurfaceMesh::TriangleArray triLine[1];
    triLine[0] = { { 0, 1, 2 } };
    trianglesLine.push_back(triLine[0]);
    lineSurfMesh->setTrianglesVertices(trianglesLine);
    auto objectLine = std::make_shared<imstk::VisualObject>("visualLineMesh");
    auto materialLine = std::make_shared<RenderMaterial>();
    materialLine->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpLine = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialLine->addTexture(tmpLine);
    lineSurfMesh->setRenderMaterial(materialLine);
    objectLine->setVisualGeometry(lineSurfMesh); // change to any mesh created above
    scene->addSceneObject(objectLine);
    // END

    // using for force normal
    auto tempSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertList2;
    vertList2.resize(3);
    vertList2[0] = imstk::Vec3d(-14.4, -1.4, 7.9);
    vertList2[1] = imstk::Vec3d(-7.8, -1.2, 7.9);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertList2[2] = imstk::Vec3d(-14.4, -1.4, 7.9);
    std::vector<std::vector<int> > connectivity2;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity2.push_back(line);
    }

    tempSurfMesh->setInitialVertexPositions(vertList2);
    tempSurfMesh->setVertexPositions(vertList2);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> triangles11;
    imstk::SurfaceMesh::TriangleArray tri[1];
    tri[0] = { { 0, 1, 2 } };
    triangles11.push_back(tri[0]);
    tempSurfMesh->setTrianglesVertices(triangles11);
    auto objectTmp = std::make_shared<imstk::VisualObject>("visualNormal0");
    auto materialTmp = std::make_shared<RenderMaterial>();
    materialTmp->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpTT = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialTmp->addTexture(tmpTT);
    tempSurfMesh->setRenderMaterial(materialTmp);
    objectTmp->setVisualGeometry(tempSurfMesh); // change to any mesh created above
    scene->addSceneObject(objectTmp);
    // END

    // XZH using for the central line of colon
    auto centrallineSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertList3;
    vertList3.resize(3);
    vertList3[0] = imstk::Vec3d(-2.5, 7, -23);
    vertList3[1] = imstk::Vec3d(-3.5, 15, -23);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertList3[2] = imstk::Vec3d(-2.5, 7, -23);
    std::vector<std::vector<int> > connectivity3;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity3.push_back(line);
    }

    centrallineSurfMesh->setInitialVertexPositions(vertList3);
    centrallineSurfMesh->setVertexPositions(vertList3);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> triangles1;
    imstk::SurfaceMesh::TriangleArray tri11[1];
    tri11[0] = { { 0, 1, 2 } };
    triangles1.push_back(tri11[0]);
    centrallineSurfMesh->setTrianglesVertices(triangles1);
    auto objectCentral = std::make_shared<imstk::VisualObject>("visualNormal1");
    auto materialCentral = std::make_shared<RenderMaterial>();
    materialCentral->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpCentral = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialCentral->addTexture(tmpCentral);
    centrallineSurfMesh->setRenderMaterial(materialCentral);
    objectCentral->setVisualGeometry(centrallineSurfMesh); // change to any mesh created above
    //scene->addSceneObject(objectCentral);
    // XZH END

    auto visualMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(readMesh);
    auto physicsMesh = lineMesh; //visualMesh; //  lineMesh; //  imstk::MeshIO::read(path2obj_c);  //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
    auto collidingMesh = lineMesh; // visualMesh; //  lineMesh;
    Vec3d min1, max1;
    visualMesh->computeBoundingBox(min1, max1);

    auto material0 = std::make_shared<RenderMaterial>();
    auto diffuseTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto normalTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Normal.png", Texture::Type::NORMAL);
    auto roughnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Roughness.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material0->addTexture(diffuseTexture0);
    material0->addTexture(normalTexture0);
    material0->addTexture(roughnessTexture0);
    //material->setTexture(metalnessTexture);
    visualMesh->setRenderMaterial(material0);

    FILE *outpointtri;
    outpointtri = fopen("i:/AABB.txt", "w");
    fprintf(outpointtri, "V: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    collidingMesh->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "C: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    fclose(outpointtri);

    auto object = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject");

    auto objMapP2V = std::make_shared<imstk::OneToOneMap>();  // std::make_shared<imstk::TriangleLineMap>(); //  std::make_shared<imstk::OneToOneMap>();
    objMapP2V->setMaster(physicsMesh);
    objMapP2V->setSlave(visualMesh);
    objMapP2V->compute();


    auto objMapP2C = std::make_shared<imstk::OneToOneMap>();  //  colliding is physical mesh
    objMapP2C->setMaster(physicsMesh);
    objMapP2C->setSlave(collidingMesh);
    objMapP2C->compute();

    auto objMapC2V = std::make_shared<imstk::OneToOneMap>(); //  std::make_shared<imstk::TriangleLineMap>(); //  s std::make_shared<imstk::OneToOneMap>();
    objMapC2V->setMaster(collidingMesh);
    objMapC2V->setSlave(visualMesh);
    objMapC2V->compute();

    auto objMapC2P = std::make_shared<imstk::OneToOneMap>(); // colliding is physical mesh
    objMapC2P->setMaster(collidingMesh);
    objMapC2P->setSlave(physicsMesh);
    objMapC2P->compute();

    object->setVisualGeometry(visualMesh);
    object->setCollidingGeometry(collidingMesh);
    object->setPhysicsGeometry(physicsMesh);
    object->setPhysicsToCollidingMap(objMapP2C);
    object->setPhysicsToVisualMap(objMapP2V);
    object->setCollidingToVisualMap(objMapC2V);
    object->setColldingToPhysicsMap(objMapC2P);

    auto pbdModel = std::make_shared<PbdModel>();
    pbdModel->setModelGeometry(physicsMesh);
    pbdModel->configure(/*Number of Constraints*/1,
        /*Constraint configuration*/"Distance 0.1",
        /*Mass*/0.0,
        /*Gravity*/"0 0 0",
        /*TimeStep*/0.0003,
        /*FixedPoint*/"",
        /*NumberOfIterationInConstraintSolver*/2, // 5
        /*Proximity*/0.3,  // use linemesh 0.3  // 0.01 use  //  cube 0.1
        /*Contact stiffness*/0.001);
    object->setDynamicalModel(pbdModel);

    //auto pbdSolver = std::make_shared<PbdSolver>();
    //pbdSolver->setPbdObject(object);
    //scene->addNonlinearSolver(pbdSolver);

    scene->addSceneObject(object);

    auto trackCtrl = std::make_shared<imstk::DeviceTracker>(client);
    trackCtrl->setTranslationScaling(0.25);  // 0.1  test 0.1 should use  Scale 0.001 will be better 
    auto controller = std::make_shared<imstk::PBDSceneObjectController>(object, trackCtrl);
    scene->addObjectController(controller);

    // Sphere1
    auto sphere1Obj = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "Sphere1", 0.1, Vec3d(-14.4, -1.4, 7.9)); // -2.23
    sphere1Obj->getVisualGeometry()->setTranslation(Vec3d(-14.4, -1.4, 7.9));
    //auto materialTT = std::make_shared<Material>();
    //materialTT->setTexture(diffuseTexture0);
    //auto suMesh = std::dynamic_pointer_cast<SurfaceMesh>(sphere1Obj->getVisualGeometry());
    //suMesh->addMaterial(materialTT);
    //auto sphere2Obj = apiutils::createCollidingAnalyticalSceneObject(
    //    imstk::Geometry::Type::Sphere, scene, "Sphere2", 0.5, Vec3d(-9.3, -1.4, 9.05)); //-9.3, -1.4, 9.05    new -9.53, - 1.50,    8.6  org  -7.2, -1.35, 8.62
    //sphere2Obj->getVisualGeometry()->setTranslation(Vec3d(-9.3, -1.4, 9.05));
    //sphere2Obj->setCollidingGeometry(sphere2Obj->getVisualGeometry());

    auto visualSphere22 = std::make_shared<imstk::Sphere>();
    visualSphere22->setTranslation(Vec3d(-7.8, -1.2, 7.9));
    visualSphere22->setRadius(0.4);
    Vec3d mm2 = visualSphere22->getPosition();
    auto sphere2Obj = std::make_shared<CollidingObject>("Sphere2");
    sphere2Obj->setCollidingGeometry(visualSphere22);
    sphere2Obj->setVisualGeometry(visualSphere22);
    scene->addSceneObject(sphere2Obj);

    auto visualSphere2 = std::make_shared<imstk::Sphere>();
    visualSphere2->setTranslation(Vec3d(-14.4, -1.4, 7.9));
    visualSphere2->setRadius(0.2);
    Vec3d mmm = visualSphere2->getPosition();
    auto sphere3Obj = std::make_shared<CollidingObject>("Sphere3");
    sphere3Obj->setCollidingGeometry(visualSphere2);
    sphere3Obj->setVisualGeometry(visualSphere2);
    //scene->addSceneObject(sphere3Obj);


    auto sphereOrg = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereOrg", 0.2, Vec3d(0, 0, 0));
    sphereOrg->getVisualGeometry()->setTranslation(Vec3d(0, 0, 0));

    auto sphereX = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereX", 0.5, Vec3d(10, 0, 0));
    sphereX->getVisualGeometry()->setTranslation(Vec3d(10, 0, 0));

    auto sphereY = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereY", 1.0, Vec3d(0, 10, 0));
    sphereY->getVisualGeometry()->setTranslation(Vec3d(0, 10, 0));

    auto sphereZ = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereZ", 1.5, Vec3d(0, 0, 10));
    sphereZ->getVisualGeometry()->setTranslation(Vec3d(0, 0, 10));
    //scene->addSceneObject(sphere1Obj);
    //scene->addSceneObject(sphere2Obj);

    //auto trackCtrl1 = std::make_shared<imstk::DeviceTracker>(client);
    //trackCtrl1->setTranslationScaling(0.1);
    //auto controller1 = std::make_shared<imstk::SceneObjectController>(sphere1Obj, trackCtrl1);
    //scene->addObjectController(controller1);
    //auto controller2 = std::make_shared<imstk::SceneObjectController>(sphere2Obj, trackCtrl1);
    //scene->addObjectController(controller2);

    //// Light (white)
    //auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
    //whiteLight->setPosition(imstk::Vec3d(-15, -1, 0));
    //whiteLight->setFocalPoint(imstk::Vec3d(1, -1, 0));
    //scene->addLight(whiteLight);
    //auto whiteLight2 = std::make_shared<imstk::PointLight>("whiteLight2");
    //whiteLight2->setPosition(imstk::Vec3d(1, -1, 0));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    ////scene->addLight(whiteLight2);

    //// Light (red)
    //auto colorLight = std::make_shared<imstk::SpotLight>("colorLight");
    //colorLight->setPosition(imstk::Vec3d(-3.5, 2, -28)); // - 25, -8, -4.5
    //colorLight->setFocalPoint(imstk::Vec3d(-1.5, 22, -18)); // - 3.7, 5.5, 7
    //colorLight->setColor(imstk::Color::White); // red
    //colorLight->setSpotAngle(15);
    //auto colorLight2 = std::make_shared<imstk::SpotLight>("colorLight2");
    //colorLight2->setPosition(imstk::Vec3d(-2.5, 17, -23));
    //colorLight2->setFocalPoint(imstk::Vec3d(-2.5, 7, -23));
    //colorLight2->setColor(imstk::Color::White);
    //colorLight2->setSpotAngle(15);
    //auto colorLight3 = std::make_shared<imstk::SpotLight>("colorLight");
    //colorLight3->setPosition(imstk::Vec3d(-2.5, 7, -23));  //-25, -5, -2.5
    //colorLight3->setFocalPoint(imstk::Vec3d(-2.5, 17, -23));  // -3.7, 5.5, 7
    //colorLight3->setColor(imstk::Color::White); // red
    //colorLight3->setSpotAngle(15);
    //scene->addLight(colorLight);
    //scene->addLight(colorLight2);
    //scene->addLight(colorLight3);

    // Light (white)
    auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
    whiteLight->setPosition(imstk::Vec3d(-3.158539, 11.283570, - 25.795277));
    //whiteLight->setFocalPoint(imstk::Vec3d(1, -1, 0));
    whiteLight->setIntensity(400);
    scene->addLight(whiteLight);
    auto whiteLight2 = std::make_shared<imstk::PointLight>("whiteLight2");
    whiteLight2->setPosition(imstk::Vec3d(-10, 20, 40));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight2->setIntensity(400);
    scene->addLight(whiteLight2);
    auto whiteLight3 = std::make_shared<imstk::PointLight>("whiteLight3");
    whiteLight3->setPosition(imstk::Vec3d(-60, 20, -5));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight3->setIntensity(400);
    scene->addLight(whiteLight3);
    auto whiteLight4 = std::make_shared<imstk::PointLight>("whiteLight4");
    whiteLight4->setPosition(imstk::Vec3d(35, 20, -5));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight4->setIntensity(400);
    scene->addLight(whiteLight4);
    auto whiteLight5 = std::make_shared<imstk::PointLight>("whiteLight5");
    whiteLight5->setPosition(imstk::Vec3d(-10, -25, -5));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight5->setIntensity(400);
    scene->addLight(whiteLight5);
    auto whiteLight6 = std::make_shared<imstk::PointLight>("whiteLight6");
    whiteLight6->setPosition(imstk::Vec3d(-10, 65, -5));
    //whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    whiteLight6->setIntensity(400);
    scene->addLight(whiteLight6);

    // Light (red)
    auto colorLight = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight->setPosition(imstk::Vec3d(-4.422437, 5.680608, - 26.236061)); // - 25, -8, -4.5
    colorLight->setFocalPoint(imstk::Vec3d(0.416648, 18.525057, - 13.762144)); // - 3.7, 5.5, 7
    colorLight->setColor(imstk::Color::White); // red
    colorLight->setSpotAngle(70);
    colorLight->setIntensity(200);
    auto colorLight2 = std::make_shared<imstk::SpotLight>("colorLight2");
    colorLight2->setPosition(imstk::Vec3d(-10, 20, -60));
    colorLight2->setFocalPoint(imstk::Vec3d(-10, 20, 15));
    colorLight2->setColor(imstk::Color::White);
    colorLight2->setSpotAngle(70);
    colorLight2->setIntensity(10000);
    auto colorLight3 = std::make_shared<imstk::SpotLight>("colorLight3");
    colorLight3->setPosition(imstk::Vec3d(-2.5, 7, -23));  //-25, -5, -2.5
    colorLight3->setFocalPoint(imstk::Vec3d(-2.5, 17, -23));  // -3.7, 5.5, 7
    colorLight3->setColor(imstk::Color::White); // red
    colorLight3->setSpotAngle(150);
    colorLight3->setIntensity(2000);
    scene->addLight(colorLight);
    //scene->addLight(colorLight2);
    //sceneTest->addLight(colorLight3);

    // Update Camera
    auto cam1 = scene->getCamera();
    cam1->setPosition(Vec3d(-2.111103, 5.906062, -24.198272)); // (imstk::Vec3d(-12.5, -7, -33)); // -25, -8, -4.5
    cam1->setFocalPoint(-6.532188, 16.588575, -13.986447); //  (imstk::Vec3d(2.5, 17, -13)); // -3.7, 5.5, 7
    cam1->setViewAngle(75);


    // model
    // colon wall
    std::string pathColon = DATA_ROOT_PATH"/VESS/colontumor_new_SB.obj"; //  colon_new_002_SB   colon_tumor3_SB cubeTest2_SB  colon_s022.dae"; //  colon_s02.dae"; //  colon_s12.dae"; // volume mesh for colliding // key==colonnew_seg4.dae  colonnew_seg0508.dae small segment// good segment  colonnew_seg4.dae   colonnew_seg3.obj colon_move2.DAE"; //  VESS / colonnew_seg3.obj"; // glove.dae"; //  colon_move2.DAE";    // colon_move2.DAE  colon1_part_max.DAE  colon1_part_max.DAE  colon6_max.DAEcloth.DAE  colon_1_c2_max  colon_cylinder2_tri_max
    std::string pathColon2 = DATA_ROOT_PATH"/VESS/colontumor_s01.obj";  //  colon_s01
    std::string pathColon3 = DATA_ROOT_PATH"/VESS/colonrectum_s003.dae";  // colon_s03
    auto objMesh = imstk::MeshIO::read(pathColon);   // collidingColonMesh; //  imstk::MeshIO::read(pathColon);
    auto objMesh2 = imstk::MeshIO::read(pathColon2);
    auto objMesh3 = imstk::MeshIO::read(pathColon3);
    auto visualColonMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh); // physicsColonMesh->getSurfaceMesh(); //  
    auto visualColonMesh2 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh2);
    auto visualColonMesh3 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh3);
    //visualColonMesh->setTranslation(2.5, -12, 22);
    //visualColonMesh->getVertexPositions();
    //visualColonMesh2->setTranslation(2.5, -12, 22);
    //visualColonMesh3->setTranslation(2.5, -12, 22);
    //visualColonMesh2->getVertexPositions();
    //visualColonMesh3->getVertexPositions();


    //XZH
    FILE *outMesh = fopen("i:/outOBJ.dat", "w");
    //visualColonMesh->getInitialVerticesPositions();
    fprintf(outMesh, "num vertices:%d\n", visualColonMesh->getNumVertices());
    Vec3d tempPos;
    for (int i = 0; i < visualColonMesh->getNumVertices(); ++i){
        tempPos = visualColonMesh->getVertexPosition(i);
        fprintf(outMesh, "%f %f %f\n", tempPos.x(), tempPos.y(), tempPos.z());
    }
    fprintf(outMesh, "\n num tri:%d\n", visualColonMesh->getNumTriangles());
    for (size_t triId = 0; triId < visualColonMesh->getNumTriangles(); ++triId)
    {
        Vec3d center(0, 0, 0);
        auto vert = visualColonMesh->getTrianglesVertices().at(triId); // getTetrahedronVertices(triId);
        fprintf(outMesh, "%d %d %d\n", vert[0], vert[1], vert[2]);
        for (size_t i = 0; i < 3; ++i)
        {
            center = visualColonMesh->getInitialVertexPosition(vert[i]);
            //fprintf(outMesh, "i: %d %f %f %f  ", i, center.x(), center.y(), center.z());
        }
        //fprintf(outMesh, "\n");
    }

    fclose(outMesh);
    // XZH END

    //// Tetrahedral mesh
    imstk::StdVectorOfVec3d vertListTet;
    std::vector<imstk::TetrahedralMesh::TetraArray> tetConnectivity;
    //physXLink linkVolSur;
    std::vector<physXLink> linksVolSur;
    bool flag = loadTetFile(DATA_ROOT_PATH"/VESS/colontumor_new_SB.tet", vertListTet, tetConnectivity, linksVolSur); // colon_new_002_SB colonrectum_s002_SB.tet  colon_tumor3_3_SB for test   colon_tumor3_SB  cubeTest2_SB
    auto tetMesh = std::make_shared<imstk::CuttingVESSCutMesh>(); // VESSTetrahedralMesh
    //for (int i = 0; i < vertListTet.size();i++)
    //{
    //    vertListTet[i] += Vec3d(2.5, -12, 22);
    //}
    tetMesh->setInitialVertexPositions(vertListTet);
    tetMesh->setVertexPositions(vertListTet);
    tetMesh->setTetrahedraVertices(tetConnectivity);
    tetMesh->setPhysXLink(linksVolSur);
    tetMesh->setInitialPhysVertex(vertListTet);  // add later 06/04/2017

    tetMesh->initializeCuttingMesh();
    tetMesh->initializeCutting();
    tetMesh->extractSurfaceMesh();
    tetMesh->extractSurfaceMeshVolume();
    //auto cutTissueMesh = std::make_shared<imstk::CuttingVESSCutMesh>(tetMesh);
    tetMesh->setFlagSplitMeshAfterCut(true);

    //Vec3d min1, max1;
    tetMesh->computeBoundingBox(min1, max1);

    std::vector<Vec3d> centralLine;
    centralLine.push_back(Vec3d(-2.5, 7, -23));
    centralLine.push_back(Vec3d(-3.5, 15, -23));
    //tetMesh->extractSurfaceMesh(); // both sides of volume mesh
    tetMesh->extractSurfaceMesh(centralLine); // only inside colon of surface of volume mesh


    auto physicsColonMesh = tetMesh; //  collidingColonMesh; //  imstk::MeshIO::read(pathColon);


    auto collidingColonMesh = physicsColonMesh; //  visualColonMesh; //  imstk::MeshIO::read(pathColon);



    auto material = std::make_shared<RenderMaterial>();
    auto diffuseTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    auto normalTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    auto sssTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_sss.png", Texture::Type::SUBSURFACE_SCATTERING);
    material->addTexture(diffuseTexture);
    material->addTexture(normalTexture);
    material->addTexture(roughnessTexture);
    material->setDisplayMode(RenderMaterial::DisplayMode::SURFACE);
    //material->setTexture(metalnessTexture);
    material->addTexture(sssTexture);
    material->setTessellated(true);
    visualColonMesh->setRenderMaterial(material);

    auto material2 = std::make_shared<RenderMaterial>();
    auto diffuseTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    auto normalTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material2->addTexture(diffuseTexture2);
    material2->addTexture(normalTexture2);
    material2->addTexture(roughnessTexture2);
    visualColonMesh2->setRenderMaterial(material2);

    auto material3 = std::make_shared<RenderMaterial>();
    auto diffuseTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    auto normalTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material3->addTexture(diffuseTexture3);
    material3->addTexture(normalTexture3);
    material3->addTexture(roughnessTexture3);
    visualColonMesh3->setRenderMaterial(material3);

    visualColonMesh->computeBoundingBox(min1, max1);
    outpointtri = fopen("i:/AABB.txt", "a");
    fprintf(outpointtri, "V core Colon: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    physicsColonMesh->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "C&P core Colon: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    visualColonMesh2->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "V1 Colon1: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    visualColonMesh3->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "V3 Colon3: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    // floor

    //imstk::StdVectorOfVec3d vertList;
    double width = 60.0;
    double height = 60.0;
    int nRows = 11;
    int nCols = 11;
    int corner[4] = { 1, nRows, nRows*nCols - nCols + 1, nRows*nCols };
    char intStr[33];
    std::string fixed_corner;
    //for (unsigned int i = 0; i < 4; ++i)
    //{
    //	std::sprintf(intStr, "%d", corner[i]);
    //	fixed_corner += std::string(intStr) + ' ';
    //}

    //fixed point
    Vec3d tmpPos;
    std::vector<int> fixedVertice; // add later 06/04/2017
    for (int i = 0; i < physicsColonMesh->getNumVertices(); ++i) // physicsColonMesh  visualColonMesh
    {
        tmpPos = physicsColonMesh->getVertexPosition(i);  // physicsColonMesh  visualColonMesh
        if ((tmpPos.y() < 5) || (tmpPos.y() > 15))  //  || (tmpPos.z() > 3.0   <-1.3  >1.6  smoothed colon segment  (tmpPos.x() < -1.5) || (tmpPos.x() > 1.6)
        {
            std::sprintf(intStr, "%d", i + 1);
            fixed_corner += std::string(intStr) + ' ';
            fixedVertice.push_back(i); // add later 06/04/2017
        }
    }

    fprintf(outpointtri, "V_Colon: Fixed point %s \n ", fixed_corner.c_str());

    // set fixed in the tetmesh add later 06/04/2017
    tetMesh->setFixedVertice(fixedVertice);
    tetMesh->updateFixedVertice();
    tetMesh->initDeformMesh(); // init for deform mesh : injection solution 06/20/2017

    // Plane
    auto geom1 = std::make_shared<imstk::Plane>();
    geom1->setPosition(Vec3d(0, 17, 0));
    //geom1->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    geom1->rotate(imstk::UP_VECTOR, imstk::PI_2);
    geom1->setWidth(30.0);
    geom1->print();
    Vec3d temp = geom1->getNormal();
    fprintf(outpointtri, "plane 1\n norm: %f %f %f\n", temp.x(), temp.y(), temp.z());
    temp = geom1->getPosition();
    fprintf(outpointtri, "position: %f %f %f\n", temp.x(), temp.y(), temp.z());
    auto planeObj1 = std::make_shared<imstk::VisualObject>("VisualPlane1");
    planeObj1->setVisualGeometry(geom1);
    // Plane
    auto geom2 = std::make_shared<imstk::Plane>();
    geom2->setPosition(Vec3d(0, 7, 0));
    //geom2->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    geom2->rotate(imstk::UP_VECTOR, imstk::PI_2);
    geom2->setWidth(30.0);
    geom2->print();
    temp = geom2->getNormal();
    fprintf(outpointtri, "plane 2\n norm: %f %f %f\n", temp.x(), temp.y(), temp.z());
    temp = geom2->getPosition();
    fprintf(outpointtri, "position: %f %f %f\n", temp.x(), temp.y(), temp.z());
    auto planeObj2 = std::make_shared<imstk::VisualObject>("VisualPlane2");
    planeObj2->setVisualGeometry(geom2);
    scene->addSceneObject(planeObj1);
    scene->addSceneObject(planeObj2);

    fclose(outpointtri);

    // Construct a map
    auto colonMapP2V = std::make_shared<imstk::VESSTetraTriangleMap>();  // <imstk::OneToOneMap>();
    colonMapP2V->setMaster(physicsColonMesh);
    colonMapP2V->setSlave(visualColonMesh);
    colonMapP2V->compute();


    auto colonMapP2C = std::make_shared<imstk::OneToOneMap>();
    colonMapP2C->setMaster(physicsColonMesh);
    colonMapP2C->setSlave(collidingColonMesh);
    colonMapP2C->compute();

    auto colonMapC2V = std::make_shared<imstk::VESSTetraTriangleMap>();  //<imstk::OneToOneMap>();
    colonMapC2V->setMaster(collidingColonMesh);
    colonMapC2V->setSlave(visualColonMesh);
    colonMapC2V->compute();

    auto colonObj = std::make_shared<PbdObject>("Colon");
    colonObj->setCollidingGeometry(collidingColonMesh);
    colonObj->setVisualGeometry(visualColonMesh);
    colonObj->setPhysicsGeometry(physicsColonMesh);
    colonObj->setPhysicsToCollidingMap(colonMapP2C);
    colonObj->setPhysicsToVisualMap(colonMapP2V);
    colonObj->setCollidingToVisualMap(colonMapC2V);

    // Create object and add to scene  just for scene, no colliding  for the two sides part, middle for operation part
    auto object2 = std::make_shared<imstk::VisualObject>("visualColonMesh2");
    object2->setVisualGeometry(visualColonMesh2); // change to any mesh created above
    scene->addSceneObject(object2);
    auto object3 = std::make_shared<imstk::VisualObject>("visualColonMesh3");
    object3->setVisualGeometry(visualColonMesh3); // change to any mesh created above
    //scene->addSceneObject(object3);

    // wire frame for surface of volume mesh
    // START
    auto object4 = std::make_shared<imstk::VisualObject>("visualColonMesh4");
    auto material4 = std::make_shared<RenderMaterial>();
    material4->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmp4 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto surfM = tetMesh->getSurfaceMesh();
    material4->addTexture(tmp4);
    surfM->setRenderMaterial(material4);
    object4->setVisualGeometry(surfM); // change to any mesh created above
    //scene->addSceneObject(object4);

    auto object5 = std::make_shared<imstk::VisualObject>("visualColonMesh5");
    auto material5 = std::make_shared<RenderMaterial>();
    material5->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmp5 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto surfM5 = tetMesh->getSurfaceInsideMesh();
    material5->addTexture(tmp5);
    surfM5->setRenderMaterial(material5);
    object5->setVisualGeometry(surfM5); // change to any mesh created above
    //scene->addSceneObject(object5);

    //FILE *outNormal = fopen("G:/surfacenorm.dat","w");
    //fprintf(outNormal, "num:%d\n", surfM->getNumTriangles());
    std::vector<SurfaceMesh::TriangleArray> elements = surfM->getTrianglesVertices();
    for (int i = 0; i < surfM->getNumTriangles() - 1000; i++)
        //if (0)
    {
        //int i = 0;
        Vec3d tmp = surfM->getTriangleNormal(i);
        SurfaceMesh::TriangleArray& e = elements[i];
        const Vec3d v0 = surfM->getVertexPosition(e[0]);
        const Vec3d v1 = surfM->getVertexPosition(e[1]);
        const Vec3d v2 = surfM->getVertexPosition(e[2]);

        Vec3d cenPos = (v0 + v1 + v2) / 3;
        // XZH
        Vec3d p0 = Vec3d(-19.4, -5.85, 1.15);
        Vec3d p1 = Vec3d(-3.3, 2.4, 1.05);
        Vec3d a = cenPos - p0;
        Vec3d b = p1 - p0;
        Vec3d c = ((a.dot(b)) / (b.dot(b)))*b;
        Vec3d ee = a - c;
        if (tmp.dot(-ee) < 0)  // normal indicating the outside colon
        {
            //continue;
            if ((tmp.dot(-ee)) + tmp.norm()*ee.norm() < 0.1)
            {
                printf("cenPos: %f %f %f, -ee: %f %f %f\n", cenPos.x(), cenPos.y(), cenPos.z(), -ee.x(), -ee.y(), -ee.z());
            }
        }
        else
        {
            continue;
            //fprintf(outNormal, "%d\n", i);
        }
        continue;
        // XZH END

        // XZH using for the central line of colon
        auto surfTTT = std::make_shared<imstk::SurfaceMesh>();
        imstk::StdVectorOfVec3d vertListTTT;
        vertListTTT.resize(3);
        vertListTTT[0] = cenPos;
        tmp.normalize();
        vertListTTT[1] = cenPos + tmp;
        vertListTTT[2] = cenPos;
        //fprintf(outNormal, "Pos: %f %f %f 2: %f %f %f\n", cenPos.x(), cenPos.y(), cenPos.z(), cenPos.x() + tmp.x() * 10, cenPos.y() + tmp.y() * 10, cenPos.z() + tmp.z() * 10);

        surfTTT->setInitialVertexPositions(vertListTTT);
        surfTTT->setVertexPositions(vertListTTT);
        // c. Add connectivity data
        std::vector<imstk::SurfaceMesh::TriangleArray> trianglesTTT;
        imstk::SurfaceMesh::TriangleArray triTTT[1];
        triTTT[0] = { { 0, 1, 2 } };
        trianglesTTT.push_back(triTTT[0]);
        surfTTT->setTrianglesVertices(trianglesTTT);
        char str[25];
        sprintf(str, "surfaceNormal %d", i);
        auto objectTTT = std::make_shared<imstk::VisualObject>(str);
        auto materialTTT = std::make_shared<RenderMaterial>();
        materialTTT->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
        auto tmpTTT = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
        materialTTT->addTexture(tmpTTT);
        surfTTT->setRenderMaterial(materialTTT);
        objectTTT->setVisualGeometry(surfTTT); // change to any mesh created above

        scene->addSceneObject(objectTTT);
        continue;

        auto surfTTT1 = std::make_shared<imstk::SurfaceMesh>();
        imstk::StdVectorOfVec3d vertListTTT1;
        vertListTTT1.resize(3);
        vertListTTT1[0] = cenPos;
        Vec3d tmpTTT1 = -ee;
        //tmpTTT1.normalize();
        vertListTTT1[1] = cenPos + tmpTTT1;
        vertListTTT1[2] = cenPos;
        //fprintf(outNormal, "Pos: %f %f %f 2: %f %f %f\n", cenPos.x(), cenPos.y(), cenPos.z(), cenPos.x() + tmp.x() * 10, cenPos.y() + tmp.y() * 10, cenPos.z() + tmp.z() * 10);

        surfTTT1->setInitialVertexPositions(vertListTTT1);
        surfTTT1->setVertexPositions(vertListTTT1);
        // c. Add connectivity data
        std::vector<imstk::SurfaceMesh::TriangleArray> trianglesTTT1;
        imstk::SurfaceMesh::TriangleArray triTTT1[1];
        triTTT1[0] = { { 0, 1, 2 } };
        trianglesTTT1.push_back(triTTT1[0]);
        surfTTT1->setTrianglesVertices(trianglesTTT1);
        sprintf(str, "surfaceLine %d", i);
        auto objectTTT1 = std::make_shared<imstk::VisualObject>(str);
        auto materialTTT1 = std::make_shared<RenderMaterial>();
        materialTTT1->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
        //auto tmpTTTT = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
        //materialTTT1->addTexture(tmpTTTT);
        surfTTT1->setRenderMaterial(materialTTT1);
        objectTTT1->setVisualGeometry(surfTTT1); // change to any mesh created above


        scene->addSceneObject(objectTTT1);
        // XZH END
    }
    //fclose(outNormal);
    FILE *planeInfo = fopen("i:/planenorm.dat", "w");
    // rotate matrix from 2 vectors
    Vec3d nn0 = Vec3d(0, 1, 0);
    Vec3d nn1 = Vec3d(0, 0, 1);   // (-0.95, 1.84, -0.42);
    nn0.normalize();
    nn1.normalize();
    double dot = nn0.dot(nn1);
    if (dot < -1.0) dot = -1.0;
    if (dot > 1.0) dot = 1.0;
    double t = acos(dot); //  acos(nn0.dot(nn1));
    Vec3d nn2 = nn0.cross(nn1);
    nn2.normalize();
    //use angle&angle to make rotate matrix   http://blog.atelier39.org/cg/463.html
    double u = nn2.x();
    double v = nn2.y();
    double w = nn2.z();
    Mat3d rotTmp;
    rotTmp(0, 0) = u*u + (v*v + w*w)*cos(t);  rotTmp(0, 1) = u*v*(1 - cos(t)) - w*sin(t); rotTmp(0, 2) = u*w*(1 - cos(t)) + v*sin(t);
    rotTmp(1, 0) = u*v*(1 - cos(t)) + w*sin(t); rotTmp(1, 1) = v*v + (u*u + w*w)*cos(t); rotTmp(1, 2) = v*w*(1 - cos(t)) - u*sin(t);
    rotTmp(2, 0) = u*w*(1 - cos(t)) - v*sin(t); rotTmp(2, 1) = v*w*(1 - cos(t)) + u*sin(t); rotTmp(2, 2) = w*w + (u*u + v*v)*cos(t);

    // Plane default normal is (0,1,0) position (0,0,0)
    auto plane0 = std::make_shared<imstk::Plane>();
    //plane0->setPosition(Vec3d(0, 10, 0));  // -4.68, -0.63, 1.48
    plane0->translate(Vec3d(-4.68, -0.63, -3.9));  // -4.68, -0.63, 1.48
    //geom1->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    //plane0->rotate(imstk::FORWARD_VECTOR, imstk::PI_2);
    plane0->rotate(rotTmp);
    plane0->setWidth(30.0);
    plane0->print();
    Vec3d ttmp0 = plane0->getNormal();
    fprintf(planeInfo, "plane 1\n norm: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    ttmp0 = plane0->getPosition();
    fprintf(planeInfo, "position: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    auto planeOmni0 = std::make_shared<imstk::CollidingObject>("VisualPlane Omni0");
    planeOmni0->setVisualGeometry(plane0);
    planeOmni0->setCollidingGeometry(plane0);
    //scene->addSceneObject(planeOmni0);

    // Plane default normal is (0,1,0) position (0,0,0)
    auto plane1 = std::make_shared<imstk::Plane>();
    //plane0->setPosition(Vec3d(0, 10, 0));  // -4.68, -0.63, 1.48
    plane1->translate(Vec3d(-4.68, -0.63, 6.8));  // -4.68, -0.63, 1.48
    //geom1->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    //plane0->rotate(imstk::FORWARD_VECTOR, imstk::PI_2);
    plane1->rotate(rotTmp);
    plane1->setWidth(30.0);
    plane1->print();
    ttmp0 = plane1->getNormal();
    fprintf(planeInfo, "plane 2\n norm: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    ttmp0 = plane1->getPosition();
    fprintf(planeInfo, "position: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    auto planeOmni1 = std::make_shared<imstk::CollidingObject>("VisualPlane Omni1");
    planeOmni1->setVisualGeometry(plane1);
    planeOmni1->setCollidingGeometry(plane1);
    //scene->addSceneObject(planeOmni1);
    fclose(planeInfo);
    // END wire frame

    auto pbdColonModel = std::make_shared<PbdModel>();
    pbdColonModel->setModelGeometry(physicsColonMesh);
    pbdColonModel->configure(/*Number of Constraints*/2,
        /*Constraint configuration*/"Volume 0.70",  // 0.7 "Dihedral 0.8", // 0.01 0.8 effect is couhe not very well
        /*Constraint configuration*/"Distance 0.90",  // 0.9 effect is couhe
        /*Mass*/0.1, // 0.1
        /*Gravity*/"0 0 0",  // "0 -9.8 0",
        /*TimeStep*/0.001, // 0.001
        /*FixedPoint*/ fixed_corner.c_str(),
        /*NumberOfIterationInConstraintSolver*/3, //   2 12
        /*Proximity*/0.01,  // 0.1
        /*Contact stiffness*/0.1);
    colonObj->setDynamicalModel(pbdColonModel);

    auto pbdColonSolver = std::make_shared<PbdSolver>();
    pbdColonSolver->setPbdObject(colonObj);
    scene->addNonlinearSolver(pbdColonSolver);

    scene->addSceneObject(colonObj);

    // Collisions
    auto colGraph = scene->getCollisionGraph();
    auto pair = std::make_shared<PbdInteractionPair>(PbdInteractionPair(object, colonObj));
    pair->setNumberOfInterations(3); // 2

    colGraph->addInteractionPair(pair);

    // Sphere-plane
    //auto trackCtrl2 = std::make_shared<imstk::DeviceTracker>(client);
    //trackCtrl2->setTranslationScaling(0.25);  // 0.1  test 0.1 should use  Scale 0.001 will be better 
    ////Create and add virtual coupling object controller in the scene
    //auto objController = std::make_shared<imstk::SceneObjectController>(sphere3Obj, trackCtrl2);
    //scene->addObjectController(objController);
    //colGraph->addInteractionPair(planeOmni0, object,
    //    CollisionDetection::Type::UnidirectionalPlaneToSphere,
    //    CollisionHandling::Type::None,
    //    CollisionHandling::Type::Penalty);

    // Marking Test
    Sleep(100);
    vector<Vec3d> markingPosList;
    markingPosList.push_back(Vec3d(-1.385700, 8.899301, -18.955198));
    markingPosList.push_back(Vec3d(-1.859606, 10.204217, - 18.999193));
    markingPosList.push_back(Vec3d(-2.510700, 10.712403, - 19.638699));
    markingPosList.push_back(Vec3d(-3.472900, 10.740303, - 19.968397));
    markingPosList.push_back(Vec3d(-4.309300, 10.330703, -20.379998));
    markingPosList.push_back(Vec3d(-4.922279, 9.261862, -20.488335));
    markingPosList.push_back(Vec3d(-4.277007, 8.486430, -20.165256));
    markingPosList.push_back(Vec3d(-3.185467, 7.896873, -19.752643));
    markingPosList.push_back(Vec3d(-1.623400, 8.069003, -19.261698));
    vector<int> vertVisualList;
    vertVisualList.push_back(39);
    vertVisualList.push_back(165);
    vertVisualList.push_back(110);
    vertVisualList.push_back(106);
    vertVisualList.push_back(102);
    vertVisualList.push_back(163);
    vertVisualList.push_back(220);
    vertVisualList.push_back(9);
    vertVisualList.push_back(3);
    StdVectorOfVec3d initPoses = visualColonMesh->getInitialVertexPositions();
    for (int numMarking = 0; numMarking < 9; numMarking++)
    {
        Vec3d markingPos = markingPosList[numMarking];
        
        //for (int k = 0; k < initPoses.size(); k++)
        //{
        //    Vec3d& pos = initPoses[k];
        //    if ((markingPos - pos).norm()<0.20)
        //    {
        //        vertVisualList.push_back(k);
        //        printf("markink index IDX: %d, visual idx %d\n", numMarking, k);
        //    }
        //}

        char name[256];
        sprintf(name, "XZHSphere%d", numMarking);
        auto visualSphere22 = std::make_shared<imstk::Sphere>();
        visualSphere22->setTranslation(markingPos);
        visualSphere22->setRadius(0.3);
        auto sphere2Obj = std::make_shared<SceneObject>(name);
        sphere2Obj->setVisualGeometry(visualSphere22);
        scene->addSceneObject(sphere2Obj);

        auto decalTest = std::make_shared<DecalPool>();
        auto decalMaterial = std::make_shared<RenderMaterial>();
        auto diffuseTexture2 = std::make_shared<Texture>("I:/iMSTK/resources/textures/test4.png", Texture::DIFFUSE);
        decalMaterial->addTexture(diffuseTexture2);
        auto decal1 = decalTest->addDecal();
        decal1->setPosition(markingPos);
        decal1->rotate(RIGHT_VECTOR, PI_4);
        decal1->scale(0.6);
        decalTest->setRenderMaterial(decalMaterial);

        sprintf(name, "DecalObject%d", numMarking);
        auto decalObject = std::make_shared<VisualObject>(name); // "DecalObject"
        decalObject->setVisualGeometry(decalTest);
        scene->addSceneObject(decalObject);
    }

    // cutting test Start
    DWORD t1, t2;
    t1 = GetTickCount();

    //  start cutting
    vector<Vec3d> m_vBladeSegments;
    vector<Vec3d> m_vSweptQuad;
    Vec3d temp1, temp2;
    Vec3d temp3, temp4;
    //Test 1
    temp1.x() = -1.4; temp1.y() = 8.9; temp1.z() = -18.9;  // 2 x=4
    temp2.x() = -3.5; temp2.y() = 10.7; temp2.z() = -20; // 1
    temp3.x() = -3.2; temp3.y() = 7.9; temp3.z() = -19.8; // 3
    //temp1 = temp1 + (temp1 - temp3)*0.2;
    //temp2 = temp2 + (temp1 - temp3)*0.2;
    //temp3 = temp3 + (temp3 - temp1)*0.2;
    temp4.x() = -4.9; temp4.y() = 9.3; temp4.z() = -20.5; // 4  // partial cutting straight
    temp4 = temp3 + (temp2 - temp1);
    //Vec3d normxzh = (temp2 - temp1).cross(temp3 - temp1).normalized();
    //temp1 += normxzh*0.3;
    //temp2 += normxzh*0.3;
    //temp3 += normxzh*0.3;
    //temp4 += normxzh*0.3;
    m_vBladeSegments.push_back(temp1);
    m_vBladeSegments.push_back(temp2);

    auto visualSphere222 = std::make_shared<imstk::Sphere>();
    visualSphere222->setTranslation(temp1);
    visualSphere222->setRadius(0.5);
    auto sphere22Obj = std::make_shared<SceneObject>("CuttingSphere1");
    sphere22Obj->setVisualGeometry(visualSphere222);
    //scene->addSceneObject(sphere22Obj);

    auto visualSphere23 = std::make_shared<imstk::Sphere>();
    visualSphere23->setTranslation(temp2);
    visualSphere23->setRadius(0.5);
    auto sphere33Obj = std::make_shared<SceneObject>("CuttingSphere2");
    sphere33Obj->setVisualGeometry(visualSphere23);
    //scene->addSceneObject(sphere33Obj);

    auto visualSphere24 = std::make_shared<imstk::Sphere>();
    visualSphere24->setTranslation(temp3);
    visualSphere24->setRadius(0.5);
    auto sphere4Obj = std::make_shared<SceneObject>("CuttingSphere3");
    sphere4Obj->setVisualGeometry(visualSphere24);
    //scene->addSceneObject(sphere4Obj);

    auto visualSphere25 = std::make_shared<imstk::Sphere>();
    visualSphere25->setTranslation(temp4);
    visualSphere25->setRadius(0.5);
    auto sphere5Obj = std::make_shared<SceneObject>("CuttingSphere4");
    sphere5Obj->setVisualGeometry(visualSphere25);
    //scene->addSceneObject(sphere5Obj);

    m_vSweptQuad.push_back(temp1);
    m_vSweptQuad.push_back(temp2);
    m_vSweptQuad.push_back(temp3);
    m_vSweptQuad.push_back(temp4);
    imstk::StdVectorOfVec3d vertListSwept;
    std::vector<imstk::SurfaceMesh::TriangleArray> trianglesSwept;
    vertListSwept.push_back(temp1);
    vertListSwept.push_back(temp2);
    vertListSwept.push_back(temp3);
    vertListSwept.push_back(temp4);
    trianglesSwept.push_back({ { 0, 1, 2 } });
    trianglesSwept.push_back({ { 2, 1, 3 } });
    auto surfMeshSwept = std::make_shared<imstk::SurfaceMesh>();
    surfMeshSwept->initialize(vertListSwept, trianglesSwept);

    //// surfMeshVisualNew
    //auto objectVisualSwept = std::make_shared<imstk::VisualObject>("visualCutFaceSwept");
    //auto materialVisualSwept = std::make_shared<RenderMaterial>();
    //materialVisualSwept->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME_SURFACE); // WIREFRAME SURFACE
    //auto tmpVisualSwept = std::make_shared<Texture>("I:/iMSTK/resources/VESS/testtest.png", Texture::Type::DIFFUSE);
    //materialVisualSwept->addTexture(tmpVisualSwept);
    //surfMeshSwept->setRenderMaterial(materialVisualSwept);
    //surfMeshSwept->computeVertexNormals();
    //objectVisualSwept->setVisualGeometry(surfMeshSwept); // change to any mesh created above
    //m_scene->addSceneObject(objectVisualSwept);

    int res = 0;
    std::shared_ptr<CuttingVESSCutMesh> cutTissueMesh = tetMesh;
    res = cutTissueMesh->cut(m_vBladeSegments, m_vSweptQuad, true);

    //// Tetrahedral mesh
    imstk::StdVectorOfVec3d vertListTetXZH;
    std::vector<imstk::TetrahedralMesh::TetraArray> tetConnectivityXZH;
    //vector< vec3d > vertices;
    //vector< vec4u32 > elements;

    //physXLink linkVolSur;
    std::vector<physXLink> linksVolSurXZH;
    bool flag2 = loadTetFile("I:/iMSTK/resources/VESS/colontumor_new_SB.tet", vertListTetXZH, tetConnectivityXZH, linksVolSurXZH);

    // link surface control points
    imstk::StdVectorOfVec3d vertListLink;
    std::vector<imstk::SurfaceMesh::TriangleArray> trianglesLink;
    // volumetric surface mesh for whole tetrahedrons
    std::list<size_t> uniqueVertIdList2Volume;
    auto& linklist = cutTissueMesh->getPhysXLink();
    auto& verList = cutTissueMesh->getTetrahedraVertices();
    auto& posList = cutTissueMesh->getVertexPositions();
    using triA = imstk::SurfaceMesh::TriangleArray;
    for (int n = 0; n < linklist.size(); n++)
    {
        auto idxList = verList[linklist[n].tetraIndex];
        trianglesLink.push_back(triA{ { idxList[0], idxList[1], idxList[2] } });
        trianglesLink.push_back(triA{ { idxList[0], idxList[1], idxList[3] } });
        trianglesLink.push_back(triA{ { idxList[0], idxList[2], idxList[3] } });
        trianglesLink.push_back(triA{ { idxList[1], idxList[2], idxList[3] } });
    }

    for (const auto &face : trianglesLink)
    {
        uniqueVertIdList2Volume.push_back(face[0]);
        uniqueVertIdList2Volume.push_back(face[1]);
        uniqueVertIdList2Volume.push_back(face[2]);
    }
    uniqueVertIdList2Volume.sort();
    uniqueVertIdList2Volume.unique();

    std::list<size_t>::iterator it;
    size_t vertId;
    for (vertId = 0, it = uniqueVertIdList2Volume.begin(); it != uniqueVertIdList2Volume.end(); ++vertId, it++)
    {
        vertListLink.push_back(posList[*it]);
        for (auto &face : trianglesLink)
        {
            for (size_t i = 0; i < 3; ++i)
            {
                if (face[i] == *it)
                {
                    face[i] = vertId;
                }
            }
        }
    }

    auto surfMeshLink = std::make_shared<imstk::SurfaceMesh>();
    surfMeshLink->initialize(vertListLink, trianglesLink);

    // display the cut points related tet
    imstk::StdVectorOfVec3d vertPosListTet;
    std::vector<imstk::SurfaceMesh::TriangleArray> trianglesTet;
    // volumetric surface mesh for whole tetrahedrons
    std::list<size_t> uniqueVertIdTet;
    auto& tetList = cutTissueMesh->getCutElements();
    auto& verListTet = cutTissueMesh->getTetrahedraVertices();
    auto& posListTet = cutTissueMesh->getVertexPositions();
    using triA = imstk::SurfaceMesh::TriangleArray;
    for (int n = 0; n < tetList.size(); n++)
    {
        auto idxList = verListTet[tetList[n]];
        trianglesTet.push_back(triA{ { idxList[0], idxList[1], idxList[2] } });
        trianglesTet.push_back(triA{ { idxList[0], idxList[1], idxList[3] } });
        trianglesTet.push_back(triA{ { idxList[0], idxList[2], idxList[3] } });
        trianglesTet.push_back(triA{ { idxList[1], idxList[2], idxList[3] } });
    }

    for (const auto &face : trianglesTet)
    {
        uniqueVertIdTet.push_back(face[0]);
        uniqueVertIdTet.push_back(face[1]);
        uniqueVertIdTet.push_back(face[2]);
    }
    uniqueVertIdTet.sort();
    uniqueVertIdTet.unique();

    //std::list<size_t>::iterator it;
    //size_t vertId;
    for (vertId = 0, it = uniqueVertIdTet.begin(); it != uniqueVertIdTet.end(); ++vertId, it++)
    {
        vertPosListTet.push_back(posList[*it]);
        for (auto &face : trianglesTet)
        {
            for (size_t i = 0; i < 3; ++i)
            {
                if (face[i] == *it)
                {
                    face[i] = vertId;
                }
            }
        }
    }

    auto surfMeshTet = std::make_shared<imstk::SurfaceMesh>();
    surfMeshTet->initialize(vertPosListTet, trianglesTet);

    // finding out the related triangles in visual surface mesh from vecCutElements
    std::vector<U32> cutNodesVisual;
    for (int m = 0; m < linksVolSur.size(); m++) // traverse the whole links
    {
        auto link = linksVolSur[m];
        std::vector<U32>::iterator result = find(tetList.begin(), tetList.end(), link.tetraIndex);  // find whether 
        if (result != tetList.end()) // existing
        {
            cutNodesVisual.push_back(m); // related nodes in visual surface mesh
        }
    }
    std::vector<U32> m_trianglesNumCut;
    auto& triLists = visualColonMesh->getTrianglesVertices();
    for (int m = 0; m < triLists.size(); m++)
    {
        auto& tri = triLists[m];
        std::vector<U32>::iterator result = find(cutNodesVisual.begin(), cutNodesVisual.end(), tri[0]);
        if (result != cutNodesVisual.end()) // existing
        {
            m_trianglesNumCut.push_back(m); // related triangle in visual surface mesh
            continue; // next m
        }
        result = find(cutNodesVisual.begin(), cutNodesVisual.end(), tri[1]);
        if (result != cutNodesVisual.end()) // existing
        {
            m_trianglesNumCut.push_back(m); // related triangle in visual surface mesh
            continue;
        }
        result = find(cutNodesVisual.begin(), cutNodesVisual.end(), tri[2]);
        if (result != cutNodesVisual.end()) // existing
        {
            m_trianglesNumCut.push_back(m); // related triangle in visual surface mesh
        }
    }

    // mapping assimp visual mesh TO original mesh
    FILE *fp = fopen("i:/connectNewColonTumor.txt", "r");
    if (!fp) return;
    int n1, n2;
    std::unordered_map<size_t, size_t> assimp2orgMap;
    for (int i = 0; i < 1224; i++)
    {
        fscanf(fp, "%d %d\n", &n1, &n2);
        assimp2orgMap.insert(make_pair(n1, n2));
    }
    fclose(fp);

    // 
    auto& posVisualList = visualColonMesh->getVertexPositions(); // changable 
    //vars
    Vec3d uvw, xyz, ss0, ss1;
    //double t;
    Vec3d tri1[3] = { m_vSweptQuad[0], m_vSweptQuad[2], m_vSweptQuad[1] };
    Vec3d tri2[3] = { m_vSweptQuad[2], m_vSweptQuad[3], m_vSweptQuad[1] };
    res = 0;
    imstk::StdVectorOfVec3d newVertList;
    std::vector<Vec3d> newVertListTriangulation; //
    std::list<size_t> newVertIndex;
    std::vector<imstk::SurfaceMesh::TriangleArray> newTriangles;
    imstk::StdVectorOfVec3d visualVertList = posVisualList;
    for (int m = 0; m < m_trianglesNumCut.size(); m++)
    {
        auto& tri = triLists[m_trianglesNumCut[m]];
        ss0 = posVisualList[tri[0]];
        ss1 = posVisualList[tri[1]];

        res = IntersectSegmentTriangle(ss0, ss1, tri1, t, uvw, xyz); //  6  9: res ← IntersectSegmentTri(edge0, edge1, tri0, p)
        if (res == 0)
        {
            res = IntersectSegmentTriangle(ss0, ss1, tri2, t, uvw, xyz);
            if (res > 0)
            {
                Vec3d temp = ss0 + (ss1 - ss0).normalized() * t;
                if (t <= 0.5) // move to ss0
                {
                    visualVertList[tri[0]] = temp;    // visualColonMesh->setVertexPosition(tri[0], temp);
                    newVertIndex.push_back(tri[0]);
                    for (std::unordered_map< size_t, size_t >::iterator it = assimp2orgMap.begin(); it != assimp2orgMap.end(); it++)
                    {
                        if (it->second == assimp2orgMap.at(tri[0]))  // in the original is the same vertex
                        {
                            visualVertList[it->first] = temp;  // visualColonMesh->setVertexPosition(it->first, temp);
                        }
                    }
                }
                else // move to ss1
                {
                    visualVertList[tri[1]] = temp;  //   visualColonMesh->setVertexPosition(tri[1], temp);
                    newVertIndex.push_back(tri[1]);
                    for (std::unordered_map< size_t, size_t >::iterator it = assimp2orgMap.begin(); it != assimp2orgMap.end(); it++)
                    {
                        if (it->second == assimp2orgMap.at(tri[1]))  // in the original is the same vertex
                        {
                            visualVertList[it->first] = temp;  // visualColonMesh->setVertexPosition(it->first, temp);
                        }
                    }
                }
                newVertList.push_back(temp);
            }
        }
        else // >0
        {
            Vec3d temp = ss0 + (ss1 - ss0).normalized() * t;
            if (t <= 0.5) // move to ss0
            {
                visualVertList[tri[0]] = temp;  // visualColonMesh->setVertexPosition(tri[0], temp);
                newVertIndex.push_back(tri[0]);
                for (std::unordered_map< size_t, size_t >::iterator it = assimp2orgMap.begin(); it != assimp2orgMap.end(); it++)
                {
                    if (it->second == assimp2orgMap.at(tri[0]))  // in the original is the same vertex
                    {
                        visualVertList[it->first] = temp;  // visualColonMesh->setVertexPosition(it->first, temp);
                    }
                }
            }
            else // move to ss1
            {
                visualVertList[tri[1]] = temp;  // visualColonMesh->setVertexPosition(tri[1], temp);
                newVertIndex.push_back(tri[1]);
                for (std::unordered_map< size_t, size_t >::iterator it = assimp2orgMap.begin(); it != assimp2orgMap.end(); it++)
                {
                    if (it->second == assimp2orgMap.at(tri[1]))  // in the original is the same vertex
                    {
                        visualVertList[it->first] = temp;  // visualColonMesh->setVertexPosition(it->first, temp);
                    }
                }
            }
            newVertList.push_back(temp);
        }

        // edge 1
        ss0 = posVisualList[tri[1]];
        ss1 = posVisualList[tri[2]];

        res = IntersectSegmentTriangle(ss0, ss1, tri1, t, uvw, xyz); //  6  9: res ← IntersectSegmentTri(edge0, edge1, tri0, p)
        if (res == 0)
        {
            res = IntersectSegmentTriangle(ss0, ss1, tri2, t, uvw, xyz);
            if (res > 0)
            {
                Vec3d temp = ss0 + (ss1 - ss0).normalized() * t;
                if (t <= 0.5) // move to ss0
                {
                    visualVertList[tri[1]] = temp;  // visualColonMesh->setVertexPosition(tri[1], temp);
                    newVertIndex.push_back(tri[1]);
                    for (std::unordered_map< size_t, size_t >::iterator it = assimp2orgMap.begin(); it != assimp2orgMap.end(); it++)
                    {
                        if (it->second == assimp2orgMap.at(tri[1]))  // in the original is the same vertex
                        {
                            visualVertList[it->first] = temp;  // visualColonMesh->setVertexPosition(it->first, temp);
                        }
                    }
                }
                else // move to ss1
                {
                    visualVertList[tri[2]] = temp;  // visualColonMesh->setVertexPosition(tri[2], temp);
                    newVertIndex.push_back(tri[2]);
                    for (std::unordered_map< size_t, size_t >::iterator it = assimp2orgMap.begin(); it != assimp2orgMap.end(); it++)
                    {
                        if (it->second == assimp2orgMap.at(tri[2]))  // in the original is the same vertex
                        {
                            visualVertList[it->first] = temp;  // visualColonMesh->setVertexPosition(it->first, temp);
                        }
                    }
                }
                newVertList.push_back(temp);
            }
        }
        else // >0
        {
            Vec3d temp = ss0 + (ss1 - ss0).normalized() * t;
            if (t <= 0.5) // move to ss0
            {
                visualVertList[tri[1]] = temp;  // visualColonMesh->setVertexPosition(tri[1], temp);
                newVertIndex.push_back(tri[1]);
                for (std::unordered_map< size_t, size_t >::iterator it = assimp2orgMap.begin(); it != assimp2orgMap.end(); it++)
                {
                    if (it->second == assimp2orgMap.at(tri[1]))  // in the original is the same vertex
                    {
                        visualVertList[it->first] = temp;  // visualColonMesh->setVertexPosition(it->first, temp);
                    }
                }
            }
            else // move to ss1
            {
                visualVertList[tri[2]] = temp;  // visualColonMesh->setVertexPosition(tri[2], temp);
                newVertIndex.push_back(tri[2]);
                for (std::unordered_map< size_t, size_t >::iterator it = assimp2orgMap.begin(); it != assimp2orgMap.end(); it++)
                {
                    if (it->second == assimp2orgMap.at(tri[2]))  // in the original is the same vertex
                    {
                        visualVertList[it->first] = temp;  // visualColonMesh->setVertexPosition(it->first, temp);
                    }
                }
            }
            newVertList.push_back(temp);
        }

        // edge 2
        ss0 = posVisualList[tri[0]];
        ss1 = posVisualList[tri[2]];

        res = IntersectSegmentTriangle(ss0, ss1, tri1, t, uvw, xyz); //  6  9: res ← IntersectSegmentTri(edge0, edge1, tri0, p)
        if (res == 0)
        {
            res = IntersectSegmentTriangle(ss0, ss1, tri2, t, uvw, xyz);
            if (res > 0)
            {
                Vec3d temp = ss0 + (ss1 - ss0).normalized() * t;
                if (t <= 0.5) // move to ss0
                {
                    visualVertList[tri[0]] = temp;  // visualColonMesh->setVertexPosition(tri[0], temp);
                    newVertIndex.push_back(tri[0]);
                    for (std::unordered_map< size_t, size_t >::iterator it = assimp2orgMap.begin(); it != assimp2orgMap.end(); it++)
                    {
                        if (it->second == assimp2orgMap.at(tri[0]))  // in the original is the same vertex
                        {
                            visualVertList[it->first] = temp;  // visualColonMesh->setVertexPosition(it->first, temp);
                        }
                    }
                }
                else // move to ss1
                {
                    visualVertList[tri[2]] = temp;  // visualColonMesh->setVertexPosition(tri[2], temp);
                    newVertIndex.push_back(tri[2]);
                    for (std::unordered_map< size_t, size_t >::iterator it = assimp2orgMap.begin(); it != assimp2orgMap.end(); it++)
                    {
                        if (it->second == assimp2orgMap.at(tri[2]))  // in the original is the same vertex
                        {
                            visualVertList[it->first] = temp;  // visualColonMesh->setVertexPosition(it->first, temp);
                        }
                    }
                }
                newVertList.push_back(temp);
            }
        }
        else // >0
        {
            Vec3d temp = ss0 + (ss1 - ss0).normalized() * t;
            if (t <= 0.5) // move to ss0
            {
                visualVertList[tri[0]] = temp;  // visualColonMesh->setVertexPosition(tri[0], temp);
                newVertIndex.push_back(tri[0]);
                for (std::unordered_map< size_t, size_t >::iterator it = assimp2orgMap.begin(); it != assimp2orgMap.end(); it++)
                {
                    if (it->second == assimp2orgMap.at(tri[0]))  // in the original is the same vertex
                    {
                        visualVertList[it->first] = temp;  // visualColonMesh->setVertexPosition(it->first, temp);
                    }
                }
            }
            else // move to ss1
            {
                visualVertList[tri[2]] = temp;  // visualColonMesh->setVertexPosition(tri[2], temp);
                newVertIndex.push_back(tri[2]);
                for (std::unordered_map< size_t, size_t >::iterator it = assimp2orgMap.begin(); it != assimp2orgMap.end(); it++)
                {
                    if (it->second == assimp2orgMap.at(tri[2]))  // in the original is the same vertex
                    {
                        visualVertList[it->first] = temp;  // visualColonMesh->setVertexPosition(it->first, temp);
                    }
                }
            }
            newVertList.push_back(temp);
        }
    }

    newVertList.clear();
    imstk::StdVectorOfVec3d newVertList2;
    newVertIndex.sort();
    newVertIndex.unique();
    std::unordered_map< size_t, size_t> mapFromG2L; // global to local
    std::unordered_map< size_t, size_t>::iterator itList; // = mesh2->m_mapEdgeTriIndex.find(edgekey);
    size_t vertID;
    std::list<size_t>::iterator itt;
    std::vector<size_t> newVertIndexLocal;
    auto& posVisualListUpdate = visualVertList; //  visualColonMesh->getVertexPositions(); // changable 
    for (vertID = 0, itt = newVertIndex.begin(); itt != newVertIndex.end(); ++vertID, itt++)
    {
        newVertList.push_back(posVisualListUpdate[*itt]);
        newVertList2.push_back(posVisualListUpdate[*itt] + Vec3d(0, 0, 10));
        mapFromG2L.insert(make_pair(*itt, vertID));
        newVertIndexLocal.push_back(vertID);
    }
    //newVertIndexLocal.push_back(vertID);
    //newVertList.push_back(m_vSweptQuad[0]);
    //newVertList2.push_back(m_vSweptQuad[0] + Vec3d(0, 0, 10));
    //vertID++;
    //newVertIndexLocal.push_back(vertID);
    //newVertList.push_back(m_vSweptQuad[2]);
    //newVertList2.push_back(m_vSweptQuad[2] + Vec3d(0, 0, 10));

    std::vector<SurfaceMesh::TriangleArray> surfaceNewTri;
    using triArray = SurfaceMesh::TriangleArray;
    int mCnt = newVertIndexLocal.size();
    //for (int k = 0; k < mCnt - 3;k++)
    //{
    //    surfaceNewTri.push_back(triArray{ { k, k+1, mCnt-2 } });
    //}
    //surfaceNewTri.push_back(triArray{ { mCnt - 3, mCnt - 2, mCnt - 1 } });

    auto delaunayTriangulation = std::make_shared<imstk::Delaunay>();
    for (auto& vert : newVertList)
    {
        newVertListTriangulation.push_back(vert);
    }
    std::vector<Triangle> triangles = delaunayTriangulation->triangulate(newVertListTriangulation);
    std::unordered_map< size_t, Vec3d> mapNewSurface; // global to local
    std::unordered_map< size_t, Vec3d>::iterator itnew; // = mesh2->m_mapEdgeTriIndex.find(edgekey);
    newVertList.clear();
    int cnt = 0;
    Vec3d norm = Vec3d::Zero();
    for (int k = 0; k < triangles.size(); k++)
    {
        int num[3];
        bool isin = false;
        for (int m = 0; m < 3; m++)
        {
            Vec3d temp;
            switch (m)
            {
            case 0:
                temp = triangles[k].p1;
                isin = false;
                for (itnew = mapNewSurface.begin(); itnew != mapNewSurface.end(); itnew++)
                {
                    if (itnew->second == temp)  // in the original is the same vertex
                    {
                        isin = true; // existing
                        num[0] = itnew->first;
                        break;;  // next m
                    }
                }
                if (!isin) // not existing
                {
                    mapNewSurface.insert(make_pair(cnt, temp));
                    newVertList.push_back(temp);
                    num[0] = cnt;
                    cnt++;
                }
                break;
            case 1:
                temp = triangles[k].p2;
                isin = false;
                for (itnew = mapNewSurface.begin(); itnew != mapNewSurface.end(); itnew++)
                {
                    if (itnew->second == temp)  // in the original is the same vertex
                    {
                        isin = true; // existing
                        num[1] = itnew->first;
                        break;;  // next m
                    }
                }
                if (!isin) // not existing
                {
                    mapNewSurface.insert(make_pair(cnt, temp));
                    newVertList.push_back(temp);
                    num[1] = cnt;
                    cnt++;
                }
                break;
            case 2:
                temp = triangles[k].p3;
                isin = false;
                for (itnew = mapNewSurface.begin(); itnew != mapNewSurface.end(); itnew++)
                {
                    if (itnew->second == temp)  // in the original is the same vertex
                    {
                        isin = true; // existing
                        num[2] = itnew->first;
                        break;;  // next m
                    }
                }
                if (!isin) // not existing
                {
                    mapNewSurface.insert(make_pair(cnt, temp));
                    newVertList.push_back(temp);
                    num[2] = cnt;
                    cnt++;
                }
                break;
            }
        }
        // keep normal the same dir as the first one
        if (k == 0)
        {
            norm = (newVertList[num[1]] - newVertList[num[0]]).cross((newVertList[num[2]] - newVertList[num[0]]));
            norm.normalized();
            surfaceNewTri.push_back(triArray{ { num[0], num[2], num[1] } });
        }
        else
        {
            Vec3d norm1 = (newVertList[num[1]] - newVertList[num[0]]).cross((newVertList[num[2]] - newVertList[num[0]]));
            norm1.normalized();
            if (norm1.dot(norm) > 0)
            {
                surfaceNewTri.push_back(triArray{ { num[0], num[1], num[2] } });
            }
            else
            {
                surfaceNewTri.push_back(triArray{ { num[0], num[2], num[1] } });
            }
        }
        // END normal 
        //surfaceNewTri.push_back(triArray{ { num[0], num[1], num[2] } });
    }
    auto surfMeshVisualNew = std::make_shared<imstk::SurfaceMesh>();
    surfMeshVisualNew->initialize(newVertList, surfaceNewTri);

    // surfMeshVisualNew
    auto objectVisualNew = std::make_shared<imstk::VisualObject>("visualCutFace");
    auto materialVisualNew = std::make_shared<RenderMaterial>();
    materialVisualNew->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME SURFACE
    auto tmpVisualNew = std::make_shared<Texture>("I:/iMSTK/resources/VESS/testtest.png", Texture::Type::DIFFUSE);
    materialVisualNew->addTexture(tmpVisualNew);
    surfMeshVisualNew->setRenderMaterial(materialVisualNew);
    surfMeshVisualNew->computeVertexNormals();
    objectVisualNew->setVisualGeometry(surfMeshVisualNew); // change to any mesh created above
    scene->addSceneObject(objectVisualNew);
    t2 = GetTickCount();
    printf("cutting time: %d ms, HZ: %f\n", t2 - t1, 1000 / double(t2 - t1));
    printf("visual cut tris: %d\n", m_trianglesNumCut.size());

    // display
    auto surfMeshFrontVolume = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMeshFrontVolume = cutTissueMesh->getSurfaceCutFrontMeshVolume();
    surfMeshFrontVolume->computeVertexNormals();

    auto surfMeshBackVolume = std::make_shared<imstk::SurfaceMesh>();
    surfMeshBackVolume = cutTissueMesh->getSurfaceCutBackMeshVolume();
    surfMeshBackVolume->computeVertexNormals();

    auto object6 = std::make_shared<imstk::VisualObject>("visualColonMesh6");
    auto material6 = std::make_shared<RenderMaterial>();
    material6->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME); // WIREFRAME
    auto tmp6 = std::make_shared<Texture>("I:/iMSTK/resources/VESS/testtest.png", Texture::Type::DIFFUSE);
    material6->addTexture(tmp6);
    surfMeshFrontVolume->setRenderMaterial(material6);
    object6->setVisualGeometry(surfMeshFrontVolume); // change to any mesh created above
    //m_scene->addSceneObject(object6);

    auto object22 = std::make_shared<imstk::VisualObject>("visualColonMesh22");
    auto material22 = std::make_shared<RenderMaterial>();
    material22->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmp22 = std::make_shared<Texture>("I:/iMSTK/resources/VESS/testtest.png", Texture::Type::DIFFUSE);
    material22->addTexture(tmp22);
    surfMeshBackVolume->setRenderMaterial(material22);
    object22->setVisualGeometry(surfMeshBackVolume); // change to any mesh created above
    //m_scene->addSceneObject(object22);

    auto surfMeshFrontNew = std::make_shared<imstk::SurfaceMesh>();
    //cutTissueMesh->extractSurfaceMesh(surfMesh);
    surfMeshFrontNew = cutTissueMesh->getSurfaceCutFrontMeshNew();
    surfMeshFrontNew->computeVertexNormals();
    auto object55 = std::make_shared<imstk::VisualObject>("visualColonMesh5");
    auto material55 = std::make_shared<RenderMaterial>();
    material55->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME
    auto tmp55 = std::make_shared<Texture>("I:/iMSTK/resources/VESS/testtest.png", Texture::Type::DIFFUSE);
    material55->addTexture(tmp55);
    surfMeshFrontNew->setRenderMaterial(material55);
    object55->setVisualGeometry(surfMeshFrontNew); // change to any mesh created above
    //scene->addSceneObject(object55);

    auto surfMeshBackNew = std::make_shared<imstk::SurfaceMesh>();
    surfMeshBackNew = cutTissueMesh->getSurfaceCutBackMeshNew();
    surfMeshBackNew->computeVertexNormals();
    auto object222 = std::make_shared<imstk::VisualObject>("visualColonMesh2");
    auto material222 = std::make_shared<RenderMaterial>();
    material222->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE);
    auto tmp222 = std::make_shared<Texture>("I:/iMSTK/resources/VESS/testtest.png", Texture::Type::DIFFUSE);
    material222->addTexture(tmp222);
    surfMeshBackNew->setRenderMaterial(material222);
    object222->setVisualGeometry(surfMeshBackNew); // change to any mesh created above
    //scene->addSceneObject(object222);

    auto surfMeshVolume = std::make_shared<imstk::SurfaceMesh>();
    auto surfMesh2 = std::make_shared<imstk::SurfaceMesh>();
    cutTissueMesh->extractSurfaceMesh();
    surfMeshVolume = cutTissueMesh->getSurfaceMesh(); //->getSurfaceMeshVolume();
    surfMeshVolume->computeVertexNormals();
    auto objectVolume = std::make_shared<imstk::VisualObject>("visualColonMeshVolume");
    auto materialVolume = std::make_shared<RenderMaterial>();
    materialVolume->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME); // WIREFRAME SURFACE
    auto tmpVolume = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    materialVolume->addTexture(tmpVolume);
    surfMeshVolume->setRenderMaterial(materialVolume);
    objectVolume->setVisualGeometry(surfMeshVolume); // change to any mesh created above
    //scene->addSceneObject(objectVolume);
    // cutting test END

    // Run
    sdk->setActiveScene(scene);
    sdk->startSimulation(true);
#endif
}

//// 07/18/2017 more dense model for  test force feedback with virtual coupling for descending colon part with new colon model based on Mark- doctor split into three segments  use real knife model 2/3 length of artist
void testPbdVirtualCoupling20()
{
#ifdef iMSTK_USE_OPENHAPTICS
    // SDK and Scene
    auto sdk = std::make_shared<imstk::SimulationManager>();
    auto scene = sdk->createNewScene("SceneTestDevice");

    // Device Client
    auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

    // Device Server
    auto server = std::make_shared<imstk::HDAPIDeviceServer>();
    server->addDeviceClient(client);
    sdk->addModule(server);

    std::string path2obj = DATA_ROOT_PATH"/VESS/ITKnife7_4.obj"; // knife correct ITKnife6 // cube knife knife3.obj";  // //  113.obj"; //  /  blade2.obj  cube.obj  kcube.obj  test cloth using this 
    std::string path2obj_c = DATA_ROOT_PATH"/VESS/ITKnife7_4.obj"; // knife correct ITKnife6  // cube knife knife3.obj";  //  //  113.obj";  // / VESS / TriangleTipKnife2.obj";  // blade2.obj  cube.obj kcube_s2.obj
    auto readMesh = imstk::MeshIO::read(path2obj_c); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);

    // Line mesh
    auto lineMesh = std::make_shared<imstk::LineMesh>();
    imstk::StdVectorOfVec3d vertList;
    vertList.resize(2);  // using 5 segment
    //vertList[0] = imstk::Vec3d(-13, -2.25, 9.15); // should - volume mesh therold than surface mesh (9.0, -1.9, -0.6) AABB 0: 9.0, -2.23, -0.6   -0.7 ITKnife6.DAE  // knife3.obj 9.5, -0.9, 1.18
    //vertList[1] = imstk::Vec3d(-5.93, -1.825, 9.125); // 9.7 ITKnife6.DAE // knife3.obj 9.5, -0.9, 8.73
    //vertList[2] = imstk::Vec3d(1.14, -1.4, 9.1); // (9.0, -2.55, 9.5)  AABB 1: 9.0, -2.23, 9.6  9.7 ITKnife6.DAE // knife3.obj 9.5, -0.9, 8.73
    vertList[0] = imstk::Vec3d(-16.4, -1.85, 9.15);
    vertList[1] = imstk::Vec3d(-9.3, -1.4, 9.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    std::vector<std::vector<int> > connectivity;
    for (int i = 0; i < 1;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity.push_back(line);
    }

    lineMesh->setInitialVertexPositions(vertList);
    lineMesh->setVertexPositions(vertList);
    lineMesh->setConnectivity(connectivity);

    // using for line mesh display
    auto lineSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertListLine;
    vertListLine.resize(3);
    vertListLine[0] = imstk::Vec3d(-16.4, -1.85, 9.15);
    vertListLine[1] = imstk::Vec3d(-9.3, -1.4, 9.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertListLine[2] = imstk::Vec3d(-16.4, -1.85, 9.15);
    std::vector<std::vector<int> > connectivityLine;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivityLine.push_back(line);
    }

    lineSurfMesh->setInitialVertexPositions(vertListLine);
    lineSurfMesh->setVertexPositions(vertListLine);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> trianglesLine;
    imstk::SurfaceMesh::TriangleArray triLine[1];
    triLine[0] = { { 0, 1, 2 } };
    trianglesLine.push_back(triLine[0]);
    lineSurfMesh->setTrianglesVertices(trianglesLine);
    auto objectLine = std::make_shared<imstk::VisualObject>("visualLineMesh");
    auto materialLine = std::make_shared<RenderMaterial>();
    materialLine->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpLine = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialLine->addTexture(tmpLine);
    lineSurfMesh->setRenderMaterial(materialLine);
    objectLine->setVisualGeometry(lineSurfMesh); // change to any mesh created above
    scene->addSceneObject(objectLine);
    // END

    // using for force normal
    auto tempSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertList2;
    vertList2.resize(3);
    vertList2[0] = imstk::Vec3d(-16.4, -1.85, 9.15);
    vertList2[1] = imstk::Vec3d(-9.3, -1.4, 9.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertList2[2] = imstk::Vec3d(-16.4, -1.85, 9.15);
    std::vector<std::vector<int> > connectivity2;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity2.push_back(line);
    }

    tempSurfMesh->setInitialVertexPositions(vertList2);
    tempSurfMesh->setVertexPositions(vertList2);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> triangles;
    imstk::SurfaceMesh::TriangleArray tri[1];
    tri[0] = { { 0, 1, 2 } };
    triangles.push_back(tri[0]);
    tempSurfMesh->setTrianglesVertices(triangles);
    auto objectTmp = std::make_shared<imstk::VisualObject>("visualNormal0");
    auto materialTmp = std::make_shared<RenderMaterial>();
    materialTmp->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpTT = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialTmp->addTexture(tmpTT);
    tempSurfMesh->setRenderMaterial(materialTmp);
    objectTmp->setVisualGeometry(tempSurfMesh); // change to any mesh created above
    scene->addSceneObject(objectTmp);
    // END

    // XZH using for the central line of colon
    auto centrallineSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertList3;
    vertList3.resize(3);
    vertList3[0] = imstk::Vec3d(-2.5, 7, -23);
    vertList3[1] = imstk::Vec3d(-3.5, 15, -23);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertList3[2] = imstk::Vec3d(-2.5, 7, -23);
    std::vector<std::vector<int> > connectivity3;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity3.push_back(line);
    }

    centrallineSurfMesh->setInitialVertexPositions(vertList3);
    centrallineSurfMesh->setVertexPositions(vertList3);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> triangles1;
    imstk::SurfaceMesh::TriangleArray tri1[1];
    tri1[0] = { { 0, 1, 2 } };
    triangles1.push_back(tri1[0]);
    centrallineSurfMesh->setTrianglesVertices(triangles1);
    auto objectCentral = std::make_shared<imstk::VisualObject>("visualNormal1");
    auto materialCentral = std::make_shared<RenderMaterial>();
    materialCentral->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpCentral = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialCentral->addTexture(tmpCentral);
    centrallineSurfMesh->setRenderMaterial(materialCentral);
    objectCentral->setVisualGeometry(centrallineSurfMesh); // change to any mesh created above
    //scene->addSceneObject(objectCentral);
    // XZH END

    auto visualMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(readMesh);
    auto physicsMesh = lineMesh; //visualMesh; //  lineMesh; //  imstk::MeshIO::read(path2obj_c);  //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
    auto collidingMesh = lineMesh; // visualMesh; //  lineMesh;
    Vec3d min1, max1;
    visualMesh->computeBoundingBox(min1, max1);

    auto material0 = std::make_shared<RenderMaterial>();
    auto diffuseTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto normalTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Normal.png", Texture::Type::NORMAL);
    auto roughnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Roughness.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material0->addTexture(diffuseTexture0);
    material0->addTexture(normalTexture0);
    material0->addTexture(roughnessTexture0);
    //material->setTexture(metalnessTexture);
    visualMesh->setRenderMaterial(material0);

    FILE *outpointtri;
    outpointtri = fopen("i:/AABB.txt", "w");
    fprintf(outpointtri, "V: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    collidingMesh->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "C: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    fclose(outpointtri);

    auto object = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject");

    auto objMapP2V = std::make_shared<imstk::OneToOneMap>();  // std::make_shared<imstk::TriangleLineMap>(); //  std::make_shared<imstk::OneToOneMap>();
    objMapP2V->setMaster(physicsMesh);
    objMapP2V->setSlave(visualMesh);
    objMapP2V->compute();


    auto objMapP2C = std::make_shared<imstk::OneToOneMap>();  //  colliding is physical mesh
    objMapP2C->setMaster(physicsMesh);
    objMapP2C->setSlave(collidingMesh);
    objMapP2C->compute();

    auto objMapC2V = std::make_shared<imstk::OneToOneMap>(); //  std::make_shared<imstk::TriangleLineMap>(); //  s std::make_shared<imstk::OneToOneMap>();
    objMapC2V->setMaster(collidingMesh);
    objMapC2V->setSlave(visualMesh);
    objMapC2V->compute();

    auto objMapC2P = std::make_shared<imstk::OneToOneMap>(); // colliding is physical mesh
    objMapC2P->setMaster(collidingMesh);
    objMapC2P->setSlave(physicsMesh);
    objMapC2P->compute();

    object->setVisualGeometry(visualMesh);
    object->setCollidingGeometry(collidingMesh);
    object->setPhysicsGeometry(physicsMesh);
    object->setPhysicsToCollidingMap(objMapP2C);
    object->setPhysicsToVisualMap(objMapP2V);
    object->setCollidingToVisualMap(objMapC2V);
    object->setColldingToPhysicsMap(objMapC2P);

    auto pbdModel = std::make_shared<PbdModel>();
	pbdModel->setModelGeometry(physicsMesh);
    pbdModel->configure(/*Number of Constraints*/1,
        /*Constraint configuration*/"Distance 0.1",
        /*Mass*/0.0,
        /*Gravity*/"0 0 0",
        /*TimeStep*/0.0003,
        /*FixedPoint*/"",
        /*NumberOfIterationInConstraintSolver*/2, // 5
        /*Proximity*/0.3,  // use linemesh 0.3  // 0.01 use  //  cube 0.1
        /*Contact stiffness*/0.001);
    object->setDynamicalModel(pbdModel);

    //auto pbdSolver = std::make_shared<PbdSolver>();
    //pbdSolver->setPbdObject(object);
    //scene->addNonlinearSolver(pbdSolver);

    scene->addSceneObject(object);

    auto trackCtrl = std::make_shared<imstk::DeviceTracker>(client);
    trackCtrl->setTranslationScaling(0.25);  // 0.1  test 0.1 should use  Scale 0.001 will be better 
    auto controller = std::make_shared<imstk::PBDSceneObjectController>(object, trackCtrl);
    scene->addObjectController(controller);

    // Sphere1
    auto sphere1Obj = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "Sphere1", 0.5, Vec3d(-16.4, -1.85, 9.15)); // -2.23
    sphere1Obj->getVisualGeometry()->setTranslation(Vec3d(-16.4, -1.85, 9.15));
    //auto materialTT = std::make_shared<Material>();
    //materialTT->setTexture(diffuseTexture0);
    //auto suMesh = std::dynamic_pointer_cast<SurfaceMesh>(sphere1Obj->getVisualGeometry());
    //suMesh->addMaterial(materialTT);
    //auto sphere2Obj = apiutils::createCollidingAnalyticalSceneObject(
    //    imstk::Geometry::Type::Sphere, scene, "Sphere2", 0.5, Vec3d(-9.3, -1.4, 9.05)); //-9.3, -1.4, 9.05    new -9.53, - 1.50,    8.6  org  -7.2, -1.35, 8.62
    //sphere2Obj->getVisualGeometry()->setTranslation(Vec3d(-9.3, -1.4, 9.05));
    //sphere2Obj->setCollidingGeometry(sphere2Obj->getVisualGeometry());

    auto visualSphere22 = std::make_shared<imstk::Sphere>();
    visualSphere22->setTranslation(Vec3d(-9.3, -1.4, 9.05));
    visualSphere22->setRadius(0.5);
    Vec3d mm2 = visualSphere22->getPosition();
    auto sphere2Obj = std::make_shared<CollidingObject>("Sphere2");
    sphere2Obj->setCollidingGeometry(visualSphere22);
    sphere2Obj->setVisualGeometry(visualSphere22);
    scene->addSceneObject(sphere2Obj);

    auto visualSphere2 = std::make_shared<imstk::Sphere>();
    visualSphere2->setTranslation(Vec3d(-9.3, -1.4, 9.05));
    visualSphere2->setRadius(0.7);
    Vec3d mmm = visualSphere2->getPosition();
    auto sphere3Obj = std::make_shared<CollidingObject>("Sphere3");
    sphere3Obj->setCollidingGeometry(visualSphere2);
    sphere3Obj->setVisualGeometry(visualSphere2);
    scene->addSceneObject(sphere3Obj);


    auto sphereOrg = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereOrg", 0.2, Vec3d(0, 0, 0));
    sphereOrg->getVisualGeometry()->setTranslation(Vec3d(0, 0, 0));

    auto sphereX = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereX", 0.5, Vec3d(10, 0, 0));
    sphereX->getVisualGeometry()->setTranslation(Vec3d(10, 0, 0));

    auto sphereY = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereY", 1.0, Vec3d(0, 10, 0));
    sphereY->getVisualGeometry()->setTranslation(Vec3d(0, 10, 0));

    auto sphereZ = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereZ", 1.5, Vec3d(0, 0, 10));
    sphereZ->getVisualGeometry()->setTranslation(Vec3d(0, 0, 10));
    //scene->addSceneObject(sphere1Obj);
    //scene->addSceneObject(sphere2Obj);

    //auto trackCtrl1 = std::make_shared<imstk::DeviceTracker>(client);
    //trackCtrl1->setTranslationScaling(0.1);
    //auto controller1 = std::make_shared<imstk::SceneObjectController>(sphere1Obj, trackCtrl1);
    //scene->addObjectController(controller1);
    //auto controller2 = std::make_shared<imstk::SceneObjectController>(sphere2Obj, trackCtrl1);
    //scene->addObjectController(controller2);

    // Light (white)
    auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
    whiteLight->setPosition(imstk::Vec3d(-15, -1, 0));
    whiteLight->setFocalPoint(imstk::Vec3d(1, -1, 0));
    scene->addLight(whiteLight);
    auto whiteLight2 = std::make_shared<imstk::PointLight>("whiteLight2");
    whiteLight2->setPosition(imstk::Vec3d(1, -1, 0));
    whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    //scene->addLight(whiteLight2);

    // Light (red)
    auto colorLight = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight->setPosition(imstk::Vec3d(-3.5, 2, -28)); // - 25, -8, -4.5
    colorLight->setFocalPoint(imstk::Vec3d(-1.5, 22, -18)); // - 3.7, 5.5, 7
    colorLight->setColor(imstk::Color::White); // red
    colorLight->setSpotAngle(15);
    auto colorLight2 = std::make_shared<imstk::SpotLight>("colorLight2");
    colorLight2->setPosition(imstk::Vec3d(-2.5, 17, -23));
    colorLight2->setFocalPoint(imstk::Vec3d(-2.5, 7, -23));
    colorLight2->setColor(imstk::Color::White);
    colorLight2->setSpotAngle(15);
    auto colorLight3 = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight3->setPosition(imstk::Vec3d(-2.5, 7, -23));  //-25, -5, -2.5
    colorLight3->setFocalPoint(imstk::Vec3d(-2.5, 17, -23));  // -3.7, 5.5, 7
    colorLight3->setColor(imstk::Color::White); // red
    colorLight3->setSpotAngle(15);
    scene->addLight(colorLight);
    scene->addLight(colorLight2);
    scene->addLight(colorLight3);

    // Update Camera
    auto cam1 = scene->getCamera();
    cam1->setPosition(imstk::Vec3d(-12.5, -7, -33)); // -25, -8, -4.5
    cam1->setFocalPoint(imstk::Vec3d(2.5, 17, -13)); // -3.7, 5.5, 7
    cam1->setViewAngle(75);


    // model
    // colon wall
    std::string pathColon = DATA_ROOT_PATH"/VESS/colon_new_002_SB_2.obj"; //  colon_new_002_SB   colon_tumor3_SB cubeTest2_SB  colon_s022.dae"; //  colon_s02.dae"; //  colon_s12.dae"; // volume mesh for colliding // key==colonnew_seg4.dae  colonnew_seg0508.dae small segment// good segment  colonnew_seg4.dae   colonnew_seg3.obj colon_move2.DAE"; //  VESS / colonnew_seg3.obj"; // glove.dae"; //  colon_move2.DAE";    // colon_move2.DAE  colon1_part_max.DAE  colon1_part_max.DAE  colon6_max.DAEcloth.DAE  colon_1_c2_max  colon_cylinder2_tri_max
    std::string pathColon2 = DATA_ROOT_PATH"/VESS/colon_new_001.obj";  //  colon_s01
    std::string pathColon3 = DATA_ROOT_PATH"/VESS/colonrectum_s003.dae";  // colon_s03
    auto objMesh = imstk::MeshIO::read(pathColon);   // collidingColonMesh; //  imstk::MeshIO::read(pathColon);
    auto objMesh2 = imstk::MeshIO::read(pathColon2);
    auto objMesh3 = imstk::MeshIO::read(pathColon3);
    auto visualColonMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh); // physicsColonMesh->getSurfaceMesh(); //  
    auto visualColonMesh2 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh2);
    auto visualColonMesh3 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh3);
    //visualColonMesh->setTranslation(2.5, -12, 22);
    //visualColonMesh->getVertexPositions();
    //visualColonMesh2->setTranslation(2.5, -12, 22);
    //visualColonMesh3->setTranslation(2.5, -12, 22);
    //visualColonMesh2->getVertexPositions();
    //visualColonMesh3->getVertexPositions();


    //XZH
    FILE *outMesh = fopen("i:/outOBJ.dat", "w");
    //visualColonMesh->getInitialVerticesPositions();
    fprintf(outMesh, "num vertices:%d\n", visualColonMesh->getNumVertices());
    Vec3d tempPos;
    for (int i = 0; i < visualColonMesh->getNumVertices(); ++i){
        tempPos = visualColonMesh->getVertexPosition(i);
        fprintf(outMesh, "%f %f %f\n", tempPos.x(), tempPos.y(), tempPos.z());
    }
    fprintf(outMesh, "\n num tri:%d\n", visualColonMesh->getNumTriangles());
    for (size_t triId = 0; triId < visualColonMesh->getNumTriangles(); ++triId)
    {
        Vec3d center(0, 0, 0);
        auto vert = visualColonMesh->getTrianglesVertices().at(triId); // getTetrahedronVertices(triId);
        fprintf(outMesh, "%d %d %d\n", vert[0], vert[1], vert[2]);
        for (size_t i = 0; i < 3; ++i)
        {
            center = visualColonMesh->getInitialVertexPosition(vert[i]);
            //fprintf(outMesh, "i: %d %f %f %f  ", i, center.x(), center.y(), center.z());
        }
        //fprintf(outMesh, "\n");
    }

    fclose(outMesh);
    // XZH END

    //// Tetrahedral mesh
    imstk::StdVectorOfVec3d vertListTet;
    std::vector<imstk::TetrahedralMesh::TetraArray> tetConnectivity;
    //physXLink linkVolSur;
    std::vector<physXLink> linksVolSur;
    bool flag = loadTetFile(DATA_ROOT_PATH"/VESS/colon_new_002_SB_2.tet", vertListTet, tetConnectivity, linksVolSur); // colon_new_002_SB colonrectum_s002_SB.tet  colon_tumor3_3_SB for test   colon_tumor3_SB  cubeTest2_SB
    auto tetMesh = std::make_shared<imstk::VESSTetrahedralMesh>();
    //for (int i = 0; i < vertListTet.size();i++)
    //{
    //    vertListTet[i] += Vec3d(2.5, -12, 22);
    //}
    tetMesh->setInitialVertexPositions(vertListTet);
    tetMesh->setVertexPositions(vertListTet);
    tetMesh->setTetrahedraVertices(tetConnectivity);
    tetMesh->setPhysXLink(linksVolSur);
    tetMesh->setInitialPhysVertex(vertListTet);  // add later 06/04/2017
    std::vector<Vec3d> centralLine;
    centralLine.push_back(Vec3d(-2.5, 7, -23));
    centralLine.push_back(Vec3d(-3.5, 15, -23));
    //tetMesh->extractSurfaceMesh(); // both sides of volume mesh
    tetMesh->extractSurfaceMesh(centralLine); // only inside colon of surface of volume mesh


    auto physicsColonMesh = tetMesh; //  collidingColonMesh; //  imstk::MeshIO::read(pathColon);


    auto collidingColonMesh = physicsColonMesh; //  visualColonMesh; //  imstk::MeshIO::read(pathColon);



    auto material = std::make_shared<RenderMaterial>();
    auto diffuseTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    auto normalTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    auto sssTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_sss.png", Texture::Type::SUBSURFACE_SCATTERING);
    material->addTexture(diffuseTexture);
    material->addTexture(normalTexture);
    material->addTexture(roughnessTexture);
    //material->setTexture(metalnessTexture);
    material->addTexture(sssTexture);
    material->setTessellated(true);
    visualColonMesh->setRenderMaterial(material);

    auto material2 = std::make_shared<RenderMaterial>();
    auto diffuseTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    auto normalTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material2->addTexture(diffuseTexture2);
    material2->addTexture(normalTexture2);
    material2->addTexture(roughnessTexture2);
    visualColonMesh2->setRenderMaterial(material2);

    auto material3 = std::make_shared<RenderMaterial>();
    auto diffuseTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    auto normalTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material3->addTexture(diffuseTexture3);
    material3->addTexture(normalTexture3);
    material3->addTexture(roughnessTexture3);
    visualColonMesh3->setRenderMaterial(material3);

    visualColonMesh->computeBoundingBox(min1, max1);
    outpointtri = fopen("i:/AABB.txt", "a");
    fprintf(outpointtri, "V core Colon: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    physicsColonMesh->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "C&P core Colon: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    visualColonMesh2->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "V1 Colon1: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    visualColonMesh3->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "V3 Colon3: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    // floor

    //imstk::StdVectorOfVec3d vertList;
    double width = 60.0;
    double height = 60.0;
    int nRows = 11;
    int nCols = 11;
    int corner[4] = { 1, nRows, nRows*nCols - nCols + 1, nRows*nCols };
    char intStr[33];
    std::string fixed_corner;
    //for (unsigned int i = 0; i < 4; ++i)
    //{
    //	std::sprintf(intStr, "%d", corner[i]);
    //	fixed_corner += std::string(intStr) + ' ';
    //}

    //fixed point
    Vec3d tmpPos;
    std::vector<int> fixedVertice; // add later 06/04/2017
    for (int i = 0; i < physicsColonMesh->getNumVertices(); ++i) // physicsColonMesh  visualColonMesh
    {
        tmpPos = physicsColonMesh->getVertexPosition(i);  // physicsColonMesh  visualColonMesh
        if ((tmpPos.y() < 5) || (tmpPos.y() > 15))  //  || (tmpPos.z() > 3.0   <-1.3  >1.6  smoothed colon segment  (tmpPos.x() < -1.5) || (tmpPos.x() > 1.6)
        {
            std::sprintf(intStr, "%d", i + 1);
            fixed_corner += std::string(intStr) + ' ';
            fixedVertice.push_back(i); // add later 06/04/2017
        }
    }

    fprintf(outpointtri, "V_Colon: Fixed point %s \n ", fixed_corner.c_str());

    // set fixed in the tetmesh add later 06/04/2017
    tetMesh->setFixedVertice(fixedVertice);
    tetMesh->updateFixedVertice();
    tetMesh->initDeformMesh(); // init for deform mesh : injection solution 06/20/2017

    // Plane
    auto geom1 = std::make_shared<imstk::Plane>();
    geom1->setPosition(Vec3d(0, 17, 0));
    //geom1->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    geom1->rotate(imstk::UP_VECTOR, imstk::PI_2);
    geom1->setWidth(30.0);
    geom1->print();
    Vec3d temp = geom1->getNormal();
    fprintf(outpointtri, "plane 1\n norm: %f %f %f\n", temp.x(), temp.y(), temp.z());
    temp = geom1->getPosition();
    fprintf(outpointtri, "position: %f %f %f\n", temp.x(), temp.y(), temp.z());
    auto planeObj1 = std::make_shared<imstk::VisualObject>("VisualPlane1");
    planeObj1->setVisualGeometry(geom1);
    // Plane
    auto geom2 = std::make_shared<imstk::Plane>();
    geom2->setPosition(Vec3d(0, 7, 0));
    //geom2->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    geom2->rotate(imstk::UP_VECTOR, imstk::PI_2);
    geom2->setWidth(30.0);
    geom2->print();
    temp = geom2->getNormal();
    fprintf(outpointtri, "plane 2\n norm: %f %f %f\n", temp.x(), temp.y(), temp.z());
    temp = geom2->getPosition();
    fprintf(outpointtri, "position: %f %f %f\n", temp.x(), temp.y(), temp.z());
    auto planeObj2 = std::make_shared<imstk::VisualObject>("VisualPlane2");
    planeObj2->setVisualGeometry(geom2);
    scene->addSceneObject(planeObj1);
    scene->addSceneObject(planeObj2);

    fclose(outpointtri);

    // Construct a map
    auto colonMapP2V = std::make_shared<imstk::VESSTetraTriangleMap>();  // <imstk::OneToOneMap>();
    colonMapP2V->setMaster(physicsColonMesh);
    colonMapP2V->setSlave(visualColonMesh);
    colonMapP2V->compute();


    auto colonMapP2C = std::make_shared<imstk::OneToOneMap>();
    colonMapP2C->setMaster(physicsColonMesh);
    colonMapP2C->setSlave(collidingColonMesh);
    colonMapP2C->compute();

    auto colonMapC2V = std::make_shared<imstk::VESSTetraTriangleMap>();  //<imstk::OneToOneMap>();
    colonMapC2V->setMaster(collidingColonMesh);
    colonMapC2V->setSlave(visualColonMesh);
    colonMapC2V->compute();

    auto colonObj = std::make_shared<PbdObject>("Colon");
    colonObj->setCollidingGeometry(collidingColonMesh);
    colonObj->setVisualGeometry(visualColonMesh);
    colonObj->setPhysicsGeometry(physicsColonMesh);
    colonObj->setPhysicsToCollidingMap(colonMapP2C);
    colonObj->setPhysicsToVisualMap(colonMapP2V);
    colonObj->setCollidingToVisualMap(colonMapC2V);

    // Create object and add to scene  just for scene, no colliding  for the two sides part, middle for operation part
    auto object2 = std::make_shared<imstk::VisualObject>("visualColonMesh2");
    object2->setVisualGeometry(visualColonMesh2); // change to any mesh created above
    scene->addSceneObject(object2);
    auto object3 = std::make_shared<imstk::VisualObject>("visualColonMesh3");
    object3->setVisualGeometry(visualColonMesh3); // change to any mesh created above
    //scene->addSceneObject(object3);

    // wire frame for surface of volume mesh
    // START
    auto object4 = std::make_shared<imstk::VisualObject>("visualColonMesh4");
    auto material4 = std::make_shared<RenderMaterial>();
    material4->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmp4 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto surfM = tetMesh->getSurfaceMesh();
    material4->addTexture(tmp4);
    surfM->setRenderMaterial(material4);
    object4->setVisualGeometry(surfM); // change to any mesh created above
    scene->addSceneObject(object4);

    auto object5 = std::make_shared<imstk::VisualObject>("visualColonMesh5");
    auto material5 = std::make_shared<RenderMaterial>();
    material5->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmp5 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto surfM5 = tetMesh->getSurfaceInsideMesh();
    material5->addTexture(tmp5);
    surfM5->setRenderMaterial(material5);
    object5->setVisualGeometry(surfM5); // change to any mesh created above
    //scene->addSceneObject(object5);

    //FILE *outNormal = fopen("G:/surfacenorm.dat","w");
    //fprintf(outNormal, "num:%d\n", surfM->getNumTriangles());
    std::vector<SurfaceMesh::TriangleArray> elements = surfM->getTrianglesVertices();
    for (int i = 0; i < surfM->getNumTriangles() - 1000; i++)
        //if (0)
    {
        //int i = 0;
        Vec3d tmp = surfM->getTriangleNormal(i);
        SurfaceMesh::TriangleArray& e = elements[i];
        const Vec3d v0 = surfM->getVertexPosition(e[0]);
        const Vec3d v1 = surfM->getVertexPosition(e[1]);
        const Vec3d v2 = surfM->getVertexPosition(e[2]);

        Vec3d cenPos = (v0 + v1 + v2) / 3;
        // XZH
        Vec3d p0 = Vec3d(-19.4, -5.85, 1.15);
        Vec3d p1 = Vec3d(-3.3, 2.4, 1.05);
        Vec3d a = cenPos - p0;
        Vec3d b = p1 - p0;
        Vec3d c = ((a.dot(b)) / (b.dot(b)))*b;
        Vec3d ee = a - c;
        if (tmp.dot(-ee) < 0)  // normal indicating the outside colon
        {
            //continue;
            if ((tmp.dot(-ee)) + tmp.norm()*ee.norm() < 0.1)
            {
                printf("cenPos: %f %f %f, -ee: %f %f %f\n", cenPos.x(), cenPos.y(), cenPos.z(), -ee.x(), -ee.y(), -ee.z());
            }
        }
        else
        {
            continue;
            //fprintf(outNormal, "%d\n", i);
        }
        continue;
        // XZH END

        // XZH using for the central line of colon
        auto surfTTT = std::make_shared<imstk::SurfaceMesh>();
        imstk::StdVectorOfVec3d vertListTTT;
        vertListTTT.resize(3);
        vertListTTT[0] = cenPos;
        tmp.normalize();
        vertListTTT[1] = cenPos + tmp;
        vertListTTT[2] = cenPos;
        //fprintf(outNormal, "Pos: %f %f %f 2: %f %f %f\n", cenPos.x(), cenPos.y(), cenPos.z(), cenPos.x() + tmp.x() * 10, cenPos.y() + tmp.y() * 10, cenPos.z() + tmp.z() * 10);

        surfTTT->setInitialVertexPositions(vertListTTT);
        surfTTT->setVertexPositions(vertListTTT);
        // c. Add connectivity data
        std::vector<imstk::SurfaceMesh::TriangleArray> trianglesTTT;
        imstk::SurfaceMesh::TriangleArray triTTT[1];
        triTTT[0] = { { 0, 1, 2 } };
        trianglesTTT.push_back(triTTT[0]);
        surfTTT->setTrianglesVertices(trianglesTTT);
        char str[25];
        sprintf(str, "surfaceNormal %d", i);
        auto objectTTT = std::make_shared<imstk::VisualObject>(str);
        auto materialTTT = std::make_shared<RenderMaterial>();
        materialTTT->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
        auto tmpTTT = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
        materialTTT->addTexture(tmpTTT);
        surfTTT->setRenderMaterial(materialTTT);
        objectTTT->setVisualGeometry(surfTTT); // change to any mesh created above

        scene->addSceneObject(objectTTT);
        continue;

        auto surfTTT1 = std::make_shared<imstk::SurfaceMesh>();
        imstk::StdVectorOfVec3d vertListTTT1;
        vertListTTT1.resize(3);
        vertListTTT1[0] = cenPos;
        Vec3d tmpTTT1 = -ee;
        //tmpTTT1.normalize();
        vertListTTT1[1] = cenPos + tmpTTT1;
        vertListTTT1[2] = cenPos;
        //fprintf(outNormal, "Pos: %f %f %f 2: %f %f %f\n", cenPos.x(), cenPos.y(), cenPos.z(), cenPos.x() + tmp.x() * 10, cenPos.y() + tmp.y() * 10, cenPos.z() + tmp.z() * 10);

        surfTTT1->setInitialVertexPositions(vertListTTT1);
        surfTTT1->setVertexPositions(vertListTTT1);
        // c. Add connectivity data
        std::vector<imstk::SurfaceMesh::TriangleArray> trianglesTTT1;
        imstk::SurfaceMesh::TriangleArray triTTT1[1];
        triTTT1[0] = { { 0, 1, 2 } };
        trianglesTTT1.push_back(triTTT1[0]);
        surfTTT1->setTrianglesVertices(trianglesTTT1);
        sprintf(str, "surfaceLine %d", i);
        auto objectTTT1 = std::make_shared<imstk::VisualObject>(str);
        auto materialTTT1 = std::make_shared<RenderMaterial>();
        materialTTT1->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
        //auto tmpTTTT = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
        //materialTTT1->addTexture(tmpTTTT);
        surfTTT1->setRenderMaterial(materialTTT1);
        objectTTT1->setVisualGeometry(surfTTT1); // change to any mesh created above


        scene->addSceneObject(objectTTT1);
        // XZH END
    }
    //fclose(outNormal);
    FILE *planeInfo = fopen("i:/planenorm.dat", "w");
    // rotate matrix from 2 vectors
    Vec3d nn0 = Vec3d(0, 1, 0);
    Vec3d nn1 = Vec3d(0, 0, 1);   // (-0.95, 1.84, -0.42);
    nn0.normalize();
    nn1.normalize();
    double dot = nn0.dot(nn1);
    if (dot < -1.0) dot = -1.0;
    if (dot > 1.0) dot = 1.0;
    double t = acos(dot); //  acos(nn0.dot(nn1));
    Vec3d nn2 = nn0.cross(nn1);
    nn2.normalize();
    //use angle&angle to make rotate matrix   http://blog.atelier39.org/cg/463.html
    double u = nn2.x();
    double v = nn2.y();
    double w = nn2.z();
    Mat3d rotTmp;
    rotTmp(0, 0) = u*u + (v*v + w*w)*cos(t);  rotTmp(0, 1) = u*v*(1 - cos(t)) - w*sin(t); rotTmp(0, 2) = u*w*(1 - cos(t)) + v*sin(t);
    rotTmp(1, 0) = u*v*(1 - cos(t)) + w*sin(t); rotTmp(1, 1) = v*v + (u*u + w*w)*cos(t); rotTmp(1, 2) = v*w*(1 - cos(t)) - u*sin(t);
    rotTmp(2, 0) = u*w*(1 - cos(t)) - v*sin(t); rotTmp(2, 1) = v*w*(1 - cos(t)) + u*sin(t); rotTmp(2, 2) = w*w + (u*u + v*v)*cos(t);

    // Plane default normal is (0,1,0) position (0,0,0)
    auto plane0 = std::make_shared<imstk::Plane>();
    //plane0->setPosition(Vec3d(0, 10, 0));  // -4.68, -0.63, 1.48
    plane0->translate(Vec3d(-4.68, -0.63, -3.9));  // -4.68, -0.63, 1.48
    //geom1->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    //plane0->rotate(imstk::FORWARD_VECTOR, imstk::PI_2);
    plane0->rotate(rotTmp);
    plane0->setWidth(30.0);
    plane0->print();
    Vec3d ttmp0 = plane0->getNormal();
    fprintf(planeInfo, "plane 1\n norm: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    ttmp0 = plane0->getPosition();
    fprintf(planeInfo, "position: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    auto planeOmni0 = std::make_shared<imstk::CollidingObject>("VisualPlane Omni0");
    planeOmni0->setVisualGeometry(plane0);
    planeOmni0->setCollidingGeometry(plane0);
    //scene->addSceneObject(planeOmni0);

    // Plane default normal is (0,1,0) position (0,0,0)
    auto plane1 = std::make_shared<imstk::Plane>();
    //plane0->setPosition(Vec3d(0, 10, 0));  // -4.68, -0.63, 1.48
    plane1->translate(Vec3d(-4.68, -0.63, 6.8));  // -4.68, -0.63, 1.48
    //geom1->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    //plane0->rotate(imstk::FORWARD_VECTOR, imstk::PI_2);
    plane1->rotate(rotTmp);
    plane1->setWidth(30.0);
    plane1->print();
    ttmp0 = plane1->getNormal();
    fprintf(planeInfo, "plane 2\n norm: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    ttmp0 = plane1->getPosition();
    fprintf(planeInfo, "position: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    auto planeOmni1 = std::make_shared<imstk::CollidingObject>("VisualPlane Omni1");
    planeOmni1->setVisualGeometry(plane1);
    planeOmni1->setCollidingGeometry(plane1);
    //scene->addSceneObject(planeOmni1);
    fclose(planeInfo);
    // END wire frame

    auto pbdColonModel = std::make_shared<PbdModel>();
	pbdColonModel->setModelGeometry(physicsColonMesh);
    pbdColonModel->configure(/*Number of Constraints*/2,
        /*Constraint configuration*/"Volume 0.70",  // 0.7 "Dihedral 0.8", // 0.01 0.8 effect is couhe not very well
        /*Constraint configuration*/"Distance 0.90",  // 0.9 effect is couhe
        /*Mass*/0.1, // 0.1
        /*Gravity*/"0 0 0",  // "0 -9.8 0",
        /*TimeStep*/0.001, // 0.001
        /*FixedPoint*/ fixed_corner.c_str(),
        /*NumberOfIterationInConstraintSolver*/3, //   2 12
        /*Proximity*/0.01,  // 0.1
        /*Contact stiffness*/0.1);
    colonObj->setDynamicalModel(pbdColonModel);

    auto pbdColonSolver = std::make_shared<PbdSolver>();
    pbdColonSolver->setPbdObject(colonObj);
    scene->addNonlinearSolver(pbdColonSolver);

    scene->addSceneObject(colonObj);

    // Collisions
    auto colGraph = scene->getCollisionGraph();
    auto pair = std::make_shared<PbdInteractionPair>(PbdInteractionPair(object, colonObj));
    pair->setNumberOfInterations(3); // 2

    colGraph->addInteractionPair(pair);

    // Sphere-plane
    //auto trackCtrl2 = std::make_shared<imstk::DeviceTracker>(client);
    //trackCtrl2->setTranslationScaling(0.25);  // 0.1  test 0.1 should use  Scale 0.001 will be better 
    ////Create and add virtual coupling object controller in the scene
    //auto objController = std::make_shared<imstk::SceneObjectController>(sphere3Obj, trackCtrl2);
    //scene->addObjectController(objController);
    //colGraph->addInteractionPair(planeOmni0, object,
    //    CollisionDetection::Type::UnidirectionalPlaneToSphere,
    //    CollisionHandling::Type::None,
    //    CollisionHandling::Type::Penalty);

    // Run
    sdk->setActiveScene(scene);
    sdk->startSimulation(true);
#endif
}

//// test force feedback with virtual coupling for descending colon part with new colon model based on Mark- doctor split into three segments  use real knife model 2/3 length of artist
void testPbdVirtualCoupling19()
{
#ifdef iMSTK_USE_OPENHAPTICS
    // SDK and Scene
    auto sdk = std::make_shared<imstk::SimulationManager>();
    auto scene = sdk->createNewScene("SceneTestDevice");

    // Device Client
    auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

    // Device Server
    auto server = std::make_shared<imstk::HDAPIDeviceServer>();
    server->addDeviceClient(client);
    sdk->addModule(server);

    std::string path2obj = DATA_ROOT_PATH"/VESS/ITKnife7_4.obj"; // knife correct ITKnife6 // cube knife knife3.obj";  // //  113.obj"; //  /  blade2.obj  cube.obj  kcube.obj  test cloth using this 
    std::string path2obj_c = DATA_ROOT_PATH"/VESS/ITKnife7_4.obj"; // knife correct ITKnife6  // cube knife knife3.obj";  //  //  113.obj";  // / VESS / TriangleTipKnife2.obj";  // blade2.obj  cube.obj kcube_s2.obj
    auto readMesh = imstk::MeshIO::read(path2obj_c); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);

    // Line mesh
    auto lineMesh = std::make_shared<imstk::LineMesh>();
    imstk::StdVectorOfVec3d vertList;
    vertList.resize(2);  // using 5 segment
    //vertList[0] = imstk::Vec3d(-13, -2.25, 9.15); // should - volume mesh therold than surface mesh (9.0, -1.9, -0.6) AABB 0: 9.0, -2.23, -0.6   -0.7 ITKnife6.DAE  // knife3.obj 9.5, -0.9, 1.18
    //vertList[1] = imstk::Vec3d(-5.93, -1.825, 9.125); // 9.7 ITKnife6.DAE // knife3.obj 9.5, -0.9, 8.73
    //vertList[2] = imstk::Vec3d(1.14, -1.4, 9.1); // (9.0, -2.55, 9.5)  AABB 1: 9.0, -2.23, 9.6  9.7 ITKnife6.DAE // knife3.obj 9.5, -0.9, 8.73
    vertList[0] = imstk::Vec3d(-16.4, -1.85, 9.15);
    vertList[1] = imstk::Vec3d(-9.3, -1.4, 9.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    std::vector<std::vector<int> > connectivity;
    for (int i = 0; i < 1;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity.push_back(line);
    }

    lineMesh->setInitialVertexPositions(vertList);
    lineMesh->setVertexPositions(vertList);
    lineMesh->setConnectivity(connectivity);

    // using for line mesh display
    auto lineSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertListLine;
    vertListLine.resize(3);
    vertListLine[0] = imstk::Vec3d(-16.4, -1.85, 9.15);
    vertListLine[1] = imstk::Vec3d(-9.3, -1.4, 9.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertListLine[2] = imstk::Vec3d(-16.4, -1.85, 9.15);
    std::vector<std::vector<int> > connectivityLine;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivityLine.push_back(line);
    }

    lineSurfMesh->setInitialVertexPositions(vertListLine);
    lineSurfMesh->setVertexPositions(vertListLine);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> trianglesLine;
    imstk::SurfaceMesh::TriangleArray triLine[1];
    triLine[0] = { { 0, 1, 2 } };
    trianglesLine.push_back(triLine[0]);
    lineSurfMesh->setTrianglesVertices(trianglesLine);
    auto objectLine = std::make_shared<imstk::VisualObject>("visualLineMesh");
    auto materialLine = std::make_shared<RenderMaterial>();
    materialLine->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpLine = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialLine->addTexture(tmpLine);
    lineSurfMesh->setRenderMaterial(materialLine);
    objectLine->setVisualGeometry(lineSurfMesh); // change to any mesh created above
    scene->addSceneObject(objectLine);
    // END

    // using for force normal
    auto tempSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertList2;
    vertList2.resize(3);
    vertList2[0] = imstk::Vec3d(-16.4, -1.85, 9.15);
    vertList2[1] = imstk::Vec3d(-9.3, -1.4, 9.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertList2[2] = imstk::Vec3d(-16.4, -1.85, 9.15);
    std::vector<std::vector<int> > connectivity2;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity2.push_back(line);
    }

    tempSurfMesh->setInitialVertexPositions(vertList2);
    tempSurfMesh->setVertexPositions(vertList2);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> triangles;
    imstk::SurfaceMesh::TriangleArray tri[1];
    tri[0] = { { 0, 1, 2 } };
    triangles.push_back(tri[0]);
    tempSurfMesh->setTrianglesVertices(triangles);
    auto objectTmp = std::make_shared<imstk::VisualObject>("visualNormal0");
    auto materialTmp = std::make_shared<RenderMaterial>();
    materialTmp->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpTT = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialTmp->addTexture(tmpTT);
    tempSurfMesh->setRenderMaterial(materialTmp);
    objectTmp->setVisualGeometry(tempSurfMesh); // change to any mesh created above
    scene->addSceneObject(objectTmp);
    // END

    // XZH using for the central line of colon
    auto centrallineSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertList3;
    vertList3.resize(3);
    vertList3[0] = imstk::Vec3d(-2.5, 7, -23);
    vertList3[1] = imstk::Vec3d(-3.5, 15, -23);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertList3[2] = imstk::Vec3d(-2.5, 7, -23);
    std::vector<std::vector<int> > connectivity3;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity3.push_back(line);
    }

    centrallineSurfMesh->setInitialVertexPositions(vertList3);
    centrallineSurfMesh->setVertexPositions(vertList3);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> triangles1;
    imstk::SurfaceMesh::TriangleArray tri1[1];
    tri1[0] = { { 0, 1, 2 } };
    triangles1.push_back(tri1[0]);
    centrallineSurfMesh->setTrianglesVertices(triangles1);
    auto objectCentral = std::make_shared<imstk::VisualObject>("visualNormal1");
    auto materialCentral = std::make_shared<RenderMaterial>();
    materialCentral->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpCentral = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialCentral->addTexture(tmpCentral);
    centrallineSurfMesh->setRenderMaterial(materialCentral);
    objectCentral->setVisualGeometry(centrallineSurfMesh); // change to any mesh created above
    //scene->addSceneObject(objectCentral);
    // XZH END

    auto visualMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(readMesh);
    auto physicsMesh = lineMesh; //visualMesh; //  lineMesh; //  imstk::MeshIO::read(path2obj_c);  //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
    auto collidingMesh = lineMesh; // visualMesh; //  lineMesh;
    Vec3d min1, max1;
    visualMesh->computeBoundingBox(min1, max1);

    auto material0 = std::make_shared<RenderMaterial>();
    auto diffuseTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto normalTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Normal.png", Texture::Type::NORMAL);
    auto roughnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Roughness.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material0->addTexture(diffuseTexture0);
    material0->addTexture(normalTexture0);
    material0->addTexture(roughnessTexture0);
    //material->setTexture(metalnessTexture);
    visualMesh->setRenderMaterial(material0);

    FILE *outpointtri;
    outpointtri = fopen("G:/AABB.txt", "w");
    fprintf(outpointtri, "V: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    collidingMesh->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "C: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    fclose(outpointtri);

    auto object = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject");

    auto objMapP2V = std::make_shared<imstk::OneToOneMap>();  // std::make_shared<imstk::TriangleLineMap>(); //  std::make_shared<imstk::OneToOneMap>();
    objMapP2V->setMaster(physicsMesh);
    objMapP2V->setSlave(visualMesh);
    objMapP2V->compute();


    auto objMapP2C = std::make_shared<imstk::OneToOneMap>();  //  colliding is physical mesh
    objMapP2C->setMaster(physicsMesh);
    objMapP2C->setSlave(collidingMesh);
    objMapP2C->compute();

    auto objMapC2V = std::make_shared<imstk::OneToOneMap>(); //  std::make_shared<imstk::TriangleLineMap>(); //  s std::make_shared<imstk::OneToOneMap>();
    objMapC2V->setMaster(collidingMesh);
    objMapC2V->setSlave(visualMesh);
    objMapC2V->compute();

    auto objMapC2P = std::make_shared<imstk::OneToOneMap>(); // colliding is physical mesh
    objMapC2P->setMaster(collidingMesh);
    objMapC2P->setSlave(physicsMesh);
    objMapC2P->compute();

    object->setVisualGeometry(visualMesh);
    object->setCollidingGeometry(collidingMesh);
    object->setPhysicsGeometry(physicsMesh);
    object->setPhysicsToCollidingMap(objMapP2C);
    object->setPhysicsToVisualMap(objMapP2V);
    object->setCollidingToVisualMap(objMapC2V);
    object->setColldingToPhysicsMap(objMapC2P);

    auto pbdModel = std::make_shared<PbdModel>();
    pbdModel->configure(/*Number of Constraints*/1,
        /*Constraint configuration*/"Distance 0.1",
        /*Mass*/0.0,
        /*Gravity*/"0 0 0",
        /*TimeStep*/0.0003,
        /*FixedPoint*/"",
        /*NumberOfIterationInConstraintSolver*/2, // 5
        /*Proximity*/0.3,  // use linemesh 0.3  // 0.01 use  //  cube 0.1
        /*Contact stiffness*/0.001);
    object->setDynamicalModel(pbdModel);

    //auto pbdSolver = std::make_shared<PbdSolver>();
    //pbdSolver->setPbdObject(object);
    //scene->addNonlinearSolver(pbdSolver);

    scene->addSceneObject(object);

    auto trackCtrl = std::make_shared<imstk::DeviceTracker>(client);
    trackCtrl->setTranslationScaling(0.25);  // 0.1  test 0.1 should use  Scale 0.001 will be better 
    auto controller = std::make_shared<imstk::PBDSceneObjectController>(object, trackCtrl);
    scene->addObjectController(controller);

    // Sphere1
    auto sphere1Obj = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "Sphere1", 0.5, Vec3d(-16.4, -1.85, 9.15)); // -2.23
    sphere1Obj->getVisualGeometry()->setTranslation(Vec3d(-16.4, -1.85, 9.15));
    //auto materialTT = std::make_shared<Material>();
    //materialTT->setTexture(diffuseTexture0);
    //auto suMesh = std::dynamic_pointer_cast<SurfaceMesh>(sphere1Obj->getVisualGeometry());
    //suMesh->addMaterial(materialTT);
    //auto sphere2Obj = apiutils::createCollidingAnalyticalSceneObject(
    //    imstk::Geometry::Type::Sphere, scene, "Sphere2", 0.5, Vec3d(-9.3, -1.4, 9.05)); //-9.3, -1.4, 9.05    new -9.53, - 1.50,    8.6  org  -7.2, -1.35, 8.62
    //sphere2Obj->getVisualGeometry()->setTranslation(Vec3d(-9.3, -1.4, 9.05));
    //sphere2Obj->setCollidingGeometry(sphere2Obj->getVisualGeometry());

    auto visualSphere22 = std::make_shared<imstk::Sphere>();
    visualSphere22->setTranslation(Vec3d(-9.3, -1.4, 9.05));
    visualSphere22->setRadius(0.5);
    Vec3d mm2 = visualSphere22->getPosition();
    auto sphere2Obj = std::make_shared<CollidingObject>("Sphere2");
    sphere2Obj->setCollidingGeometry(visualSphere22);
    sphere2Obj->setVisualGeometry(visualSphere22);
    scene->addSceneObject(sphere2Obj);

    auto visualSphere2 = std::make_shared<imstk::Sphere>();
    visualSphere2->setTranslation(Vec3d(-9.3, -1.4, 9.05));
    visualSphere2->setRadius(0.7);
    Vec3d mmm = visualSphere2->getPosition();
    auto sphere3Obj = std::make_shared<CollidingObject>("Sphere3");
    sphere3Obj->setCollidingGeometry(visualSphere2);
    sphere3Obj->setVisualGeometry(visualSphere2);
    scene->addSceneObject(sphere3Obj);


    auto sphereOrg = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereOrg", 0.2, Vec3d(0, 0, 0));
    sphereOrg->getVisualGeometry()->setTranslation(Vec3d(0, 0, 0));

    auto sphereX = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereX", 1.5, Vec3d(10, 0, 0));
    sphereX->getVisualGeometry()->setTranslation(Vec3d(10, 0, 0));

    auto sphereY = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereY", 1.2, Vec3d(0, 10, 0));
    sphereY->getVisualGeometry()->setTranslation(Vec3d(0, 10, 0));

    auto sphereZ = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereZ", 0.8, Vec3d(0, 0, 10));
    sphereZ->getVisualGeometry()->setTranslation(Vec3d(0, 0, 10));
    //scene->addSceneObject(sphere1Obj);
    //scene->addSceneObject(sphere2Obj);

    //auto trackCtrl1 = std::make_shared<imstk::DeviceTracker>(client);
    //trackCtrl1->setTranslationScaling(0.1);
    //auto controller1 = std::make_shared<imstk::SceneObjectController>(sphere1Obj, trackCtrl1);
    //scene->addObjectController(controller1);
    //auto controller2 = std::make_shared<imstk::SceneObjectController>(sphere2Obj, trackCtrl1);
    //scene->addObjectController(controller2);

    // Light (white)
    auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
    whiteLight->setPosition(imstk::Vec3d(-15, -1, 0));
    whiteLight->setFocalPoint(imstk::Vec3d(1, -1, 0));
    scene->addLight(whiteLight);
    auto whiteLight2 = std::make_shared<imstk::PointLight>("whiteLight2");
    whiteLight2->setPosition(imstk::Vec3d(1, -1, 0));
    whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    //scene->addLight(whiteLight2);

    // Light (red)
    auto colorLight = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight->setPosition(imstk::Vec3d(-3.5, 2, -28)); // - 25, -8, -4.5
    colorLight->setFocalPoint(imstk::Vec3d(-1.5, 22, -18)); // - 3.7, 5.5, 7
    colorLight->setColor(imstk::Color::White); // red
    colorLight->setSpotAngle(15);
    auto colorLight2 = std::make_shared<imstk::SpotLight>("colorLight2");
    colorLight2->setPosition(imstk::Vec3d(-2.5, 17, -23));
    colorLight2->setFocalPoint(imstk::Vec3d(-2.5, 7, -23));
    colorLight2->setColor(imstk::Color::White);
    colorLight2->setSpotAngle(15);
    auto colorLight3 = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight3->setPosition(imstk::Vec3d(-2.5, 7, -23));  //-25, -5, -2.5
    colorLight3->setFocalPoint(imstk::Vec3d(-2.5, 17, -23));  // -3.7, 5.5, 7
    colorLight3->setColor(imstk::Color::White); // red
    colorLight3->setSpotAngle(15);
    scene->addLight(colorLight);
    scene->addLight(colorLight2);
    scene->addLight(colorLight3);

    // Update Camera
    auto cam1 = scene->getCamera();
    cam1->setPosition(imstk::Vec3d(-12.5, -7, -33)); // -25, -8, -4.5
    cam1->setFocalPoint(imstk::Vec3d(2.5, 17, -13)); // -3.7, 5.5, 7
    cam1->setViewAngle(75);


    // model
    // colon wall
    std::string pathColon = DATA_ROOT_PATH"/VESS/colon_new_002_SB.obj"; // colon_tumor3_SB cubeTest2_SB  colon_s022.dae"; //  colon_s02.dae"; //  colon_s12.dae"; // volume mesh for colliding // key==colonnew_seg4.dae  colonnew_seg0508.dae small segment// good segment  colonnew_seg4.dae   colonnew_seg3.obj colon_move2.DAE"; //  VESS / colonnew_seg3.obj"; // glove.dae"; //  colon_move2.DAE";    // colon_move2.DAE  colon1_part_max.DAE  colon1_part_max.DAE  colon6_max.DAEcloth.DAE  colon_1_c2_max  colon_cylinder2_tri_max
    std::string pathColon2 = DATA_ROOT_PATH"/VESS/colon_new_001.obj";  //  colon_s01
    std::string pathColon3 = DATA_ROOT_PATH"/VESS/colonrectum_s003.dae";  // colon_s03
    auto objMesh = imstk::MeshIO::read(pathColon);   // collidingColonMesh; //  imstk::MeshIO::read(pathColon);
    auto objMesh2 = imstk::MeshIO::read(pathColon2);
    auto objMesh3 = imstk::MeshIO::read(pathColon3);
    auto visualColonMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh); // physicsColonMesh->getSurfaceMesh(); //  
    auto visualColonMesh2 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh2);
    auto visualColonMesh3 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh3);
    //visualColonMesh->setTranslation(2.5, -12, 22);
    //visualColonMesh->getVertexPositions();
    //visualColonMesh2->setTranslation(2.5, -12, 22);
    //visualColonMesh3->setTranslation(2.5, -12, 22);
    //visualColonMesh2->getVertexPositions();
    //visualColonMesh3->getVertexPositions();


    //XZH
    FILE *outMesh = fopen("G:/outOBJ.dat", "w");
    //visualColonMesh->getInitialVerticesPositions();
    fprintf(outMesh, "num vertices:%d\n", visualColonMesh->getNumVertices());
    Vec3d tempPos;
    for (int i = 0; i < visualColonMesh->getNumVertices(); ++i){
        tempPos = visualColonMesh->getVertexPosition(i);
        fprintf(outMesh, "%f %f %f\n", tempPos.x(), tempPos.y(), tempPos.z());
    }
    fprintf(outMesh, "\n num tri:%d\n", visualColonMesh->getNumTriangles());
    for (size_t triId = 0; triId < visualColonMesh->getNumTriangles(); ++triId)
    {
        Vec3d center(0, 0, 0);
        auto vert = visualColonMesh->getTrianglesVertices().at(triId); // getTetrahedronVertices(triId);
        fprintf(outMesh, "%d %d %d\n", vert[0], vert[1], vert[2]);
        for (size_t i = 0; i < 3; ++i)
        {
            center = visualColonMesh->getInitialVertexPosition(vert[i]);
            //fprintf(outMesh, "i: %d %f %f %f  ", i, center.x(), center.y(), center.z());
        }
        //fprintf(outMesh, "\n");
    }

    fclose(outMesh);
    // XZH END

    //// Tetrahedral mesh
    imstk::StdVectorOfVec3d vertListTet;
    std::vector<imstk::TetrahedralMesh::TetraArray> tetConnectivity;
    //physXLink linkVolSur;
    std::vector<physXLink> linksVolSur;
    bool flag = loadTetFile(DATA_ROOT_PATH"/VESS/colon_new_002_SB.tet", vertListTet, tetConnectivity, linksVolSur); // colonrectum_s002_SB.tet  colon_tumor3_3_SB for test   colon_tumor3_SB  cubeTest2_SB
    auto tetMesh = std::make_shared<imstk::VESSTetrahedralMesh>();
    //for (int i = 0; i < vertListTet.size();i++)
    //{
    //    vertListTet[i] += Vec3d(2.5, -12, 22);
    //}
    tetMesh->setInitialVertexPositions(vertListTet);
    tetMesh->setVertexPositions(vertListTet);
    tetMesh->setTetrahedraVertices(tetConnectivity);
    tetMesh->setPhysXLink(linksVolSur);
    tetMesh->setInitialPhysVertex(vertListTet);  // add later 06/04/2017
    std::vector<Vec3d> centralLine;
    centralLine.push_back(Vec3d(-2.5, 7, -23));
    centralLine.push_back(Vec3d(-3.5, 15, -23));
    //tetMesh->extractSurfaceMesh(); // both sides of volume mesh
    tetMesh->extractSurfaceMesh(centralLine); // only inside colon of surface of volume mesh

    
    auto physicsColonMesh = tetMesh; //  collidingColonMesh; //  imstk::MeshIO::read(pathColon);

    
    auto collidingColonMesh = physicsColonMesh; //  visualColonMesh; //  imstk::MeshIO::read(pathColon);

    

    auto material = std::make_shared<RenderMaterial>();
    auto diffuseTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    auto normalTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material->addTexture(diffuseTexture);
    material->addTexture(normalTexture);
    material->addTexture(roughnessTexture);
    //material->setTexture(metalnessTexture);
    visualColonMesh->setRenderMaterial(material);

    auto material2 = std::make_shared<RenderMaterial>();
    auto diffuseTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    auto normalTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material2->addTexture(diffuseTexture2);
    material2->addTexture(normalTexture2);
    material2->addTexture(roughnessTexture2);
    visualColonMesh2->setRenderMaterial(material2);

    auto material3 = std::make_shared<RenderMaterial>();
    auto diffuseTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    auto normalTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material3->addTexture(diffuseTexture3);
    material3->addTexture(normalTexture3);
    material3->addTexture(roughnessTexture3);
    visualColonMesh3->setRenderMaterial(material3);

    visualColonMesh->computeBoundingBox(min1, max1);
    outpointtri = fopen("G:/AABB.txt", "a");
    fprintf(outpointtri, "V core Colon: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    physicsColonMesh->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "C&P core Colon: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    visualColonMesh2->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "V1 Colon1: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    visualColonMesh3->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "V3 Colon3: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    // floor

    //imstk::StdVectorOfVec3d vertList;
    double width = 60.0;
    double height = 60.0;
    int nRows = 11;
    int nCols = 11;
    int corner[4] = { 1, nRows, nRows*nCols - nCols + 1, nRows*nCols };
    char intStr[33];
    std::string fixed_corner;
    //for (unsigned int i = 0; i < 4; ++i)
    //{
    //	std::sprintf(intStr, "%d", corner[i]);
    //	fixed_corner += std::string(intStr) + ' ';
    //}

    //fixed point
    Vec3d tmpPos;
    std::vector<int> fixedVertice; // add later 06/04/2017
    for (int i = 0; i < physicsColonMesh->getNumVertices(); ++i) // physicsColonMesh  visualColonMesh
    {
        tmpPos = physicsColonMesh->getVertexPosition(i);  // physicsColonMesh  visualColonMesh
        if ((tmpPos.y() < 5) || (tmpPos.y() > 15))  //  || (tmpPos.z() > 3.0   <-1.3  >1.6  smoothed colon segment  (tmpPos.x() < -1.5) || (tmpPos.x() > 1.6)
        {
            std::sprintf(intStr, "%d", i + 1);
            fixed_corner += std::string(intStr) + ' ';
            fixedVertice.push_back(i); // add later 06/04/2017
        }
    }

    fprintf(outpointtri, "V_Colon: Fixed point %s \n ", fixed_corner.c_str());

    // set fixed in the tetmesh add later 06/04/2017
    tetMesh->setFixedVertice(fixedVertice);
    tetMesh->updateFixedVertice();
    tetMesh->initDeformMesh(); // init for deform mesh : injection solution 06/20/2017

    // Plane
    auto geom1 = std::make_shared<imstk::Plane>();
    geom1->setPosition(Vec3d(0, 17, 0));
    //geom1->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    geom1->rotate(imstk::UP_VECTOR, imstk::PI_2);
    geom1->setWidth(30.0);
    geom1->print();
    Vec3d temp = geom1->getNormal();
    fprintf(outpointtri, "plane 1\n norm: %f %f %f\n", temp.x(), temp.y(), temp.z());
    temp = geom1->getPosition();
    fprintf(outpointtri, "position: %f %f %f\n", temp.x(), temp.y(), temp.z());
    auto planeObj1 = std::make_shared<imstk::VisualObject>("VisualPlane1");
    planeObj1->setVisualGeometry(geom1);
    // Plane
    auto geom2 = std::make_shared<imstk::Plane>();
    geom2->setPosition(Vec3d(0, 7, 0));
    //geom2->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    geom2->rotate(imstk::UP_VECTOR, imstk::PI_2);
    geom2->setWidth(30.0);
    geom2->print();
    temp = geom2->getNormal();
    fprintf(outpointtri, "plane 2\n norm: %f %f %f\n", temp.x(), temp.y(), temp.z());
    temp = geom2->getPosition();
    fprintf(outpointtri, "position: %f %f %f\n", temp.x(), temp.y(), temp.z());
    auto planeObj2 = std::make_shared<imstk::VisualObject>("VisualPlane2");
    planeObj2->setVisualGeometry(geom2);
    scene->addSceneObject(planeObj1);
    scene->addSceneObject(planeObj2);

    fclose(outpointtri);

    // Construct a map
    auto colonMapP2V = std::make_shared<imstk::VESSTetraTriangleMap>();  // <imstk::OneToOneMap>();
    colonMapP2V->setMaster(physicsColonMesh);
    colonMapP2V->setSlave(visualColonMesh);
    colonMapP2V->compute();


    auto colonMapP2C = std::make_shared<imstk::OneToOneMap>();
    colonMapP2C->setMaster(physicsColonMesh);
    colonMapP2C->setSlave(collidingColonMesh);
    colonMapP2C->compute();

    auto colonMapC2V = std::make_shared<imstk::VESSTetraTriangleMap>();  //<imstk::OneToOneMap>();
    colonMapC2V->setMaster(collidingColonMesh);
    colonMapC2V->setSlave(visualColonMesh);
    colonMapC2V->compute();

    auto colonObj = std::make_shared<PbdObject>("Colon");
    colonObj->setCollidingGeometry(collidingColonMesh);
    colonObj->setVisualGeometry(visualColonMesh);
    colonObj->setPhysicsGeometry(physicsColonMesh);
    colonObj->setPhysicsToCollidingMap(colonMapP2C);
    colonObj->setPhysicsToVisualMap(colonMapP2V);
    colonObj->setCollidingToVisualMap(colonMapC2V);

    // Create object and add to scene  just for scene, no colliding  for the two sides part, middle for operation part
    auto object2 = std::make_shared<imstk::VisualObject>("visualColonMesh2");
    object2->setVisualGeometry(visualColonMesh2); // change to any mesh created above
    scene->addSceneObject(object2);
    auto object3 = std::make_shared<imstk::VisualObject>("visualColonMesh3");
    object3->setVisualGeometry(visualColonMesh3); // change to any mesh created above
    //scene->addSceneObject(object3);

    // wire frame for surface of volume mesh
    // START
    auto object4 = std::make_shared<imstk::VisualObject>("visualColonMesh4");
    auto material4 = std::make_shared<RenderMaterial>();
    material4->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmp4 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto surfM = tetMesh->getSurfaceMesh();
    material4->addTexture(tmp4);
    surfM->setRenderMaterial(material4);
    object4->setVisualGeometry(surfM); // change to any mesh created above
    scene->addSceneObject(object4);

    //FILE *outNormal = fopen("G:/surfacenorm.dat","w");
    //fprintf(outNormal, "num:%d\n", surfM->getNumTriangles());
    std::vector<SurfaceMesh::TriangleArray> elements = surfM->getTrianglesVertices();
    for (int i = 0; i < surfM->getNumTriangles() - 1000; i++)
        //if (0)
    {
        //int i = 0;
        Vec3d tmp = surfM->getTriangleNormal(i);
        SurfaceMesh::TriangleArray& e = elements[i];
        const Vec3d v0 = surfM->getVertexPosition(e[0]);
        const Vec3d v1 = surfM->getVertexPosition(e[1]);
        const Vec3d v2 = surfM->getVertexPosition(e[2]);

        Vec3d cenPos = (v0 + v1 + v2) / 3;
        // XZH
        Vec3d p0 = Vec3d(-19.4, -5.85, 1.15);
        Vec3d p1 = Vec3d(-3.3, 2.4, 1.05);
        Vec3d a = cenPos - p0;
        Vec3d b = p1 - p0;
        Vec3d c = ((a.dot(b)) / (b.dot(b)))*b;
        Vec3d ee = a - c;
        if (tmp.dot(-ee) < 0)  // normal indicating the outside colon
        {
            //continue;
            if ((tmp.dot(-ee)) + tmp.norm()*ee.norm() < 0.1)
            {
                printf("cenPos: %f %f %f, -ee: %f %f %f\n", cenPos.x(), cenPos.y(), cenPos.z(), -ee.x(), -ee.y(), -ee.z());
            }
        }
        else
        {
            continue;
            //fprintf(outNormal, "%d\n", i);
        }
        continue;
        // XZH END

        // XZH using for the central line of colon
        auto surfTTT = std::make_shared<imstk::SurfaceMesh>();
        imstk::StdVectorOfVec3d vertListTTT;
        vertListTTT.resize(3);
        vertListTTT[0] = cenPos;
        tmp.normalize();
        vertListTTT[1] = cenPos + tmp;
        vertListTTT[2] = cenPos;
        //fprintf(outNormal, "Pos: %f %f %f 2: %f %f %f\n", cenPos.x(), cenPos.y(), cenPos.z(), cenPos.x() + tmp.x() * 10, cenPos.y() + tmp.y() * 10, cenPos.z() + tmp.z() * 10);

        surfTTT->setInitialVertexPositions(vertListTTT);
        surfTTT->setVertexPositions(vertListTTT);
        // c. Add connectivity data
        std::vector<imstk::SurfaceMesh::TriangleArray> trianglesTTT;
        imstk::SurfaceMesh::TriangleArray triTTT[1];
        triTTT[0] = { { 0, 1, 2 } };
        trianglesTTT.push_back(triTTT[0]);
        surfTTT->setTrianglesVertices(trianglesTTT);
        char str[25];
        sprintf(str, "surfaceNormal %d", i);
        auto objectTTT = std::make_shared<imstk::VisualObject>(str);
        auto materialTTT = std::make_shared<RenderMaterial>();
        materialTTT->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
        auto tmpTTT = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
        materialTTT->addTexture(tmpTTT);
        surfTTT->setRenderMaterial(materialTTT);
        objectTTT->setVisualGeometry(surfTTT); // change to any mesh created above

        scene->addSceneObject(objectTTT);
        continue;

        auto surfTTT1 = std::make_shared<imstk::SurfaceMesh>();
        imstk::StdVectorOfVec3d vertListTTT1;
        vertListTTT1.resize(3);
        vertListTTT1[0] = cenPos;
        Vec3d tmpTTT1 = -ee;
        //tmpTTT1.normalize();
        vertListTTT1[1] = cenPos + tmpTTT1;
        vertListTTT1[2] = cenPos;
        //fprintf(outNormal, "Pos: %f %f %f 2: %f %f %f\n", cenPos.x(), cenPos.y(), cenPos.z(), cenPos.x() + tmp.x() * 10, cenPos.y() + tmp.y() * 10, cenPos.z() + tmp.z() * 10);

        surfTTT1->setInitialVertexPositions(vertListTTT1);
        surfTTT1->setVertexPositions(vertListTTT1);
        // c. Add connectivity data
        std::vector<imstk::SurfaceMesh::TriangleArray> trianglesTTT1;
        imstk::SurfaceMesh::TriangleArray triTTT1[1];
        triTTT1[0] = { { 0, 1, 2 } };
        trianglesTTT1.push_back(triTTT1[0]);
        surfTTT1->setTrianglesVertices(trianglesTTT1);
        sprintf(str, "surfaceLine %d", i);
        auto objectTTT1 = std::make_shared<imstk::VisualObject>(str);
        auto materialTTT1 = std::make_shared<RenderMaterial>();
        materialTTT1->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
        //auto tmpTTTT = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
        //materialTTT1->addTexture(tmpTTTT);
        surfTTT1->setRenderMaterial(materialTTT1);
        objectTTT1->setVisualGeometry(surfTTT1); // change to any mesh created above


        scene->addSceneObject(objectTTT1);
        // XZH END
    }
    //fclose(outNormal);
    FILE *planeInfo = fopen("G:/planenorm.dat", "w");
    // rotate matrix from 2 vectors
    Vec3d nn0 = Vec3d(0, 1, 0);
    Vec3d nn1 = Vec3d(0, 0, 1);   // (-0.95, 1.84, -0.42);
    nn0.normalize();
    nn1.normalize();
    double dot = nn0.dot(nn1);
    if (dot < -1.0) dot = -1.0;
    if (dot > 1.0) dot = 1.0;
    double t = acos(dot); //  acos(nn0.dot(nn1));
    Vec3d nn2 = nn0.cross(nn1);
    nn2.normalize();
    //use angle&angle to make rotate matrix   http://blog.atelier39.org/cg/463.html
    double u = nn2.x();
    double v = nn2.y();
    double w = nn2.z();
    Mat3d rotTmp;
    rotTmp(0, 0) = u*u + (v*v + w*w)*cos(t);  rotTmp(0, 1) = u*v*(1 - cos(t)) - w*sin(t); rotTmp(0, 2) = u*w*(1 - cos(t)) + v*sin(t);
    rotTmp(1, 0) = u*v*(1 - cos(t)) + w*sin(t); rotTmp(1, 1) = v*v + (u*u + w*w)*cos(t); rotTmp(1, 2) = v*w*(1 - cos(t)) - u*sin(t);
    rotTmp(2, 0) = u*w*(1 - cos(t)) - v*sin(t); rotTmp(2, 1) = v*w*(1 - cos(t)) + u*sin(t); rotTmp(2, 2) = w*w + (u*u + v*v)*cos(t);

    // Plane default normal is (0,1,0) position (0,0,0)
    auto plane0 = std::make_shared<imstk::Plane>();
    //plane0->setPosition(Vec3d(0, 10, 0));  // -4.68, -0.63, 1.48
    plane0->translate(Vec3d(-4.68, -0.63, -3.9));  // -4.68, -0.63, 1.48
    //geom1->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    //plane0->rotate(imstk::FORWARD_VECTOR, imstk::PI_2);
    plane0->rotate(rotTmp);
    plane0->setWidth(30.0);
    plane0->print();
    Vec3d ttmp0 = plane0->getNormal();
    fprintf(planeInfo, "plane 1\n norm: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    ttmp0 = plane0->getPosition();
    fprintf(planeInfo, "position: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    auto planeOmni0 = std::make_shared<imstk::CollidingObject>("VisualPlane Omni0");
    planeOmni0->setVisualGeometry(plane0);
    planeOmni0->setCollidingGeometry(plane0);
    //scene->addSceneObject(planeOmni0);

    // Plane default normal is (0,1,0) position (0,0,0)
    auto plane1 = std::make_shared<imstk::Plane>();
    //plane0->setPosition(Vec3d(0, 10, 0));  // -4.68, -0.63, 1.48
    plane1->translate(Vec3d(-4.68, -0.63, 6.8));  // -4.68, -0.63, 1.48
    //geom1->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    //plane0->rotate(imstk::FORWARD_VECTOR, imstk::PI_2);
    plane1->rotate(rotTmp);
    plane1->setWidth(30.0);
    plane1->print();
    ttmp0 = plane1->getNormal();
    fprintf(planeInfo, "plane 2\n norm: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    ttmp0 = plane1->getPosition();
    fprintf(planeInfo, "position: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    auto planeOmni1 = std::make_shared<imstk::CollidingObject>("VisualPlane Omni1");
    planeOmni1->setVisualGeometry(plane1);
    planeOmni1->setCollidingGeometry(plane1);
    //scene->addSceneObject(planeOmni1);
    fclose(planeInfo);
    // END wire frame

    auto pbdColonModel = std::make_shared<PbdModel>();
    pbdColonModel->configure(/*Number of Constraints*/2,
        /*Constraint configuration*/"Volume 0.7",  // 0.9 "Dihedral 0.8", // 0.01 0.8 effect is couhe not very well
        /*Constraint configuration*/"Distance 0.9",  // 0.8 effect is couhe
        /*Mass*/0.1, // 0.1
        /*Gravity*/"0 0 0",  // "0 -9.8 0",
        /*TimeStep*/0.001, // 0.001
        /*FixedPoint*/ fixed_corner.c_str(),
        /*NumberOfIterationInConstraintSolver*/3, //   2 12
        /*Proximity*/0.01,  // 0.1
        /*Contact stiffness*/0.1);
    colonObj->setDynamicalModel(pbdColonModel);

    auto pbdColonSolver = std::make_shared<PbdSolver>();
    pbdColonSolver->setPbdObject(colonObj);
    scene->addNonlinearSolver(pbdColonSolver);

    scene->addSceneObject(colonObj);

    // Collisions
    auto colGraph = scene->getCollisionGraph();
    auto pair = std::make_shared<PbdInteractionPair>(PbdInteractionPair(object, colonObj));
    pair->setNumberOfInterations(3); // 2

    colGraph->addInteractionPair(pair);

    // Sphere-plane
    //auto trackCtrl2 = std::make_shared<imstk::DeviceTracker>(client);
    //trackCtrl2->setTranslationScaling(0.25);  // 0.1  test 0.1 should use  Scale 0.001 will be better 
    ////Create and add virtual coupling object controller in the scene
    //auto objController = std::make_shared<imstk::SceneObjectController>(sphere3Obj, trackCtrl2);
    //scene->addObjectController(objController);
    //colGraph->addInteractionPair(planeOmni0, object,
    //    CollisionDetection::Type::UnidirectionalPlaneToSphere,
    //    CollisionHandling::Type::None,
    //    CollisionHandling::Type::Penalty);

    // Run
    sdk->setActiveScene(scene);
    sdk->startSimulation(true);
#endif
}

// test force feedback with virtual coupling for rect with 2 triangles and 2 tetrahedrons for debug
void testPbdVirtualCoupling18()
{
    //Vec3d temp00 = Vec3d(0, 12, 1);
    //Vec3d temp01 = temp00.normalized();  //  has return value
    //return;
#ifdef iMSTK_USE_OPENHAPTICS
    // SDK and Scene
    auto sdk = std::make_shared<imstk::SimulationManager>();
    auto scene = sdk->createNewScene("SceneTestDevice");

    // Device Client
    auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

    // Device Server
    auto server = std::make_shared<imstk::HDAPIDeviceServer>();
    server->addDeviceClient(client);
    sdk->addModule(server);

    std::string path2obj = DATA_ROOT_PATH"/VESS/ITKnife7_4.obj"; // knife correct ITKnife6 // cube knife knife3.obj";  // //  113.obj"; //  /  blade2.obj  cube.obj  kcube.obj  test cloth using this 
    std::string path2obj_c = DATA_ROOT_PATH"/VESS/ITKnife7_4.obj"; // knife correct ITKnife6  // cube knife knife3.obj";  //  //  113.obj";  // / VESS / TriangleTipKnife2.obj";  // blade2.obj  cube.obj kcube_s2.obj
    auto readMesh = imstk::MeshIO::read(path2obj_c); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);

    // Line mesh
    auto lineMesh = std::make_shared<imstk::LineMesh>();
    imstk::StdVectorOfVec3d vertList;
    vertList.resize(2);  // using 5 segment
    //vertList[0] = imstk::Vec3d(-13, -2.25, 9.15); // should - volume mesh therold than surface mesh (9.0, -1.9, -0.6) AABB 0: 9.0, -2.23, -0.6   -0.7 ITKnife6.DAE  // knife3.obj 9.5, -0.9, 1.18
    //vertList[1] = imstk::Vec3d(-5.93, -1.825, 9.125); // 9.7 ITKnife6.DAE // knife3.obj 9.5, -0.9, 8.73
    //vertList[2] = imstk::Vec3d(1.14, -1.4, 9.1); // (9.0, -2.55, 9.5)  AABB 1: 9.0, -2.23, 9.6  9.7 ITKnife6.DAE // knife3.obj 9.5, -0.9, 8.73
    vertList[0] = imstk::Vec3d(-16.4, -1.85, 9.15);
    vertList[1] = imstk::Vec3d(-9.3, -1.4, 9.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    std::vector<std::vector<int> > connectivity;
    for (int i = 0; i < 1;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity.push_back(line);
    }

    lineMesh->setInitialVertexPositions(vertList);
    lineMesh->setVertexPositions(vertList);
    lineMesh->setConnectivity(connectivity);

    // using for line mesh display
    auto lineSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertListLine;
    vertListLine.resize(3);
    vertListLine[0] = imstk::Vec3d(-16.4, -1.85, 9.15);
    vertListLine[1] = imstk::Vec3d(-9.3, -1.4, 9.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertListLine[2] = imstk::Vec3d(-16.4, -1.85, 9.15);
    std::vector<std::vector<int> > connectivityLine;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivityLine.push_back(line);
    }

    lineSurfMesh->setInitialVertexPositions(vertListLine);
    lineSurfMesh->setVertexPositions(vertListLine);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> trianglesLine;
    imstk::SurfaceMesh::TriangleArray triLine[1];
    triLine[0] = { { 0, 1, 2 } };
    trianglesLine.push_back(triLine[0]);
    lineSurfMesh->setTrianglesVertices(trianglesLine);
    auto objectLine = std::make_shared<imstk::VisualObject>("visualLineMesh");
    auto materialLine = std::make_shared<RenderMaterial>();
    materialLine->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpLine = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialLine->addTexture(tmpLine);
    lineSurfMesh->setRenderMaterial(materialLine);
    objectLine->setVisualGeometry(lineSurfMesh); // change to any mesh created above
    scene->addSceneObject(objectLine);
    // END

    // using for force normal
    auto tempSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertList2;
    vertList2.resize(3);
    vertList2[0] = imstk::Vec3d(-16.4, -1.85, 9.15);
    vertList2[1] = imstk::Vec3d(-9.3, -1.4, 9.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertList2[2] = imstk::Vec3d(-16.4, -1.85, 9.15);
    std::vector<std::vector<int> > connectivity2;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity2.push_back(line);
    }

    tempSurfMesh->setInitialVertexPositions(vertList2);
    tempSurfMesh->setVertexPositions(vertList2);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> triangles;
    imstk::SurfaceMesh::TriangleArray tri[1];
    tri[0] = { { 0, 1, 2 } };
    triangles.push_back(tri[0]);
    tempSurfMesh->setTrianglesVertices(triangles);
    auto objectTmp = std::make_shared<imstk::VisualObject>("visualNormal0");
    auto materialTmp = std::make_shared<RenderMaterial>();
    materialTmp->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpTT = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialTmp->addTexture(tmpTT);
    tempSurfMesh->setRenderMaterial(materialTmp);
    objectTmp->setVisualGeometry(tempSurfMesh); // change to any mesh created above
    scene->addSceneObject(objectTmp);
    // END

    // XZH using for the central line of colon
    auto centrallineSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertList3;
    vertList3.resize(3);
    vertList3[0] = imstk::Vec3d(-19.4, -5, 1.15);
    vertList3[1] = imstk::Vec3d(13.3, -5, 1.15);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertList3[2] = imstk::Vec3d(-19.4, -5, 1.15);
    std::vector<std::vector<int> > connectivity3;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity3.push_back(line);
    }

    centrallineSurfMesh->setInitialVertexPositions(vertList3);
    centrallineSurfMesh->setVertexPositions(vertList3);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> triangles1;
    imstk::SurfaceMesh::TriangleArray tri1[1];
    tri1[0] = { { 0, 1, 2 } };
    triangles1.push_back(tri1[0]);
    centrallineSurfMesh->setTrianglesVertices(triangles1);
    auto objectCentral = std::make_shared<imstk::VisualObject>("visualNormal1");
    auto materialCentral = std::make_shared<RenderMaterial>();
    materialCentral->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpCentral = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialCentral->addTexture(tmpCentral);
    centrallineSurfMesh->setRenderMaterial(materialCentral);
    objectCentral->setVisualGeometry(centrallineSurfMesh); // change to any mesh created above
    scene->addSceneObject(objectCentral);
    // XZH END

    auto visualMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(readMesh);
    auto physicsMesh = lineMesh; //visualMesh; //  lineMesh; //  imstk::MeshIO::read(path2obj_c);  //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
    auto collidingMesh = lineMesh; // visualMesh; //  lineMesh;
    Vec3d min1, max1;
    visualMesh->computeBoundingBox(min1, max1);

    auto material0 = std::make_shared<RenderMaterial>();
    auto diffuseTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto normalTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Normal.png", Texture::Type::NORMAL);
    auto roughnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Roughness.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material0->addTexture(diffuseTexture0);
    material0->addTexture(normalTexture0);
    material0->addTexture(roughnessTexture0);
    //material->setTexture(metalnessTexture);
    visualMesh->setRenderMaterial(material0);

    FILE *outpointtri;
    outpointtri = fopen("G:/AABB.txt", "w");
    fprintf(outpointtri, "V: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    collidingMesh->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "C: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    fclose(outpointtri);

    auto object = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject");

    auto objMapP2V = std::make_shared<imstk::OneToOneMap>();  // std::make_shared<imstk::TriangleLineMap>(); //  std::make_shared<imstk::OneToOneMap>();
    objMapP2V->setMaster(physicsMesh);
    objMapP2V->setSlave(visualMesh);
    objMapP2V->compute();


    auto objMapP2C = std::make_shared<imstk::OneToOneMap>();  //  colliding is physical mesh
    objMapP2C->setMaster(physicsMesh);
    objMapP2C->setSlave(collidingMesh);
    objMapP2C->compute();

    auto objMapC2V = std::make_shared<imstk::OneToOneMap>(); //  std::make_shared<imstk::TriangleLineMap>(); //  s std::make_shared<imstk::OneToOneMap>();
    objMapC2V->setMaster(collidingMesh);
    objMapC2V->setSlave(visualMesh);
    objMapC2V->compute();

    auto objMapC2P = std::make_shared<imstk::OneToOneMap>(); // colliding is physical mesh
    objMapC2P->setMaster(collidingMesh);
    objMapC2P->setSlave(physicsMesh);
    objMapC2P->compute();

    object->setVisualGeometry(visualMesh);
    object->setCollidingGeometry(collidingMesh);
    object->setPhysicsGeometry(physicsMesh);
    object->setPhysicsToCollidingMap(objMapP2C);
    object->setPhysicsToVisualMap(objMapP2V);
    object->setCollidingToVisualMap(objMapC2V);
    object->setColldingToPhysicsMap(objMapC2P);

    auto pbdModel = std::make_shared<PbdModel>();
    pbdModel->configure(/*Number of Constraints*/1,
        /*Constraint configuration*/"Distance 0.1",
        /*Mass*/0.0,
        /*Gravity*/"0 0 0",
        /*TimeStep*/0.0003,
        /*FixedPoint*/"",
        /*NumberOfIterationInConstraintSolver*/2, // 5
        /*Proximity*/0.3,  // use linemesh 0.3  // 0.01 use  //  cube 0.1
        /*Contact stiffness*/0.001);
    object->setDynamicalModel(pbdModel);

    //auto pbdSolver = std::make_shared<PbdSolver>();
    //pbdSolver->setPbdObject(object);
    //scene->addNonlinearSolver(pbdSolver);

    scene->addSceneObject(object);

    auto trackCtrl = std::make_shared<imstk::DeviceTracker>(client);
    trackCtrl->setTranslationScaling(0.25);  // 0.1  test 0.1 should use  Scale 0.001 will be better 
    auto controller = std::make_shared<imstk::PBDSceneObjectController>(object, trackCtrl);
    scene->addObjectController(controller);

    // Sphere1
    auto sphere1Obj = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "Sphere1", 0.5, Vec3d(-16.4, -1.85, 9.15)); // -2.23
    sphere1Obj->getVisualGeometry()->setTranslation(Vec3d(-16.4, -1.85, 9.15));
    //auto materialTT = std::make_shared<Material>();
    //materialTT->setTexture(diffuseTexture0);
    //auto suMesh = std::dynamic_pointer_cast<SurfaceMesh>(sphere1Obj->getVisualGeometry());
    //suMesh->addMaterial(materialTT);
    //auto sphere2Obj = apiutils::createCollidingAnalyticalSceneObject(
    //    imstk::Geometry::Type::Sphere, scene, "Sphere2", 0.5, Vec3d(-9.3, -1.4, 9.05)); //-9.3, -1.4, 9.05    new -9.53, - 1.50,    8.6  org  -7.2, -1.35, 8.62
    //sphere2Obj->getVisualGeometry()->setTranslation(Vec3d(-9.3, -1.4, 9.05));
    //sphere2Obj->setCollidingGeometry(sphere2Obj->getVisualGeometry());

    auto visualSphere22 = std::make_shared<imstk::Sphere>();
    visualSphere22->setTranslation(Vec3d(-9.3, -1.4, 9.05));
    visualSphere22->setRadius(0.5);
    Vec3d mm2 = visualSphere22->getPosition();
    auto sphere2Obj = std::make_shared<CollidingObject>("Sphere2");
    sphere2Obj->setCollidingGeometry(visualSphere22);
    sphere2Obj->setVisualGeometry(visualSphere22);
    scene->addSceneObject(sphere2Obj);

    auto visualSphere2 = std::make_shared<imstk::Sphere>();
    visualSphere2->setTranslation(Vec3d(-9.3, -1.4, 9.05));
    visualSphere2->setRadius(0.9);
    Vec3d mmm = visualSphere2->getPosition();
    auto sphere3Obj = std::make_shared<CollidingObject>("Sphere3");
    sphere3Obj->setCollidingGeometry(visualSphere2);
    sphere3Obj->setVisualGeometry(visualSphere2);
    scene->addSceneObject(sphere3Obj);


    auto sphereOrg = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereOrg", 0.2, Vec3d(0, 0, 0));
    sphereOrg->getVisualGeometry()->setTranslation(Vec3d(0, 0, 0));

    auto sphereX = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereX", 1.5, Vec3d(10, 0, 0));
    sphereX->getVisualGeometry()->setTranslation(Vec3d(10, 0, 0));

    auto sphereY = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereY", 1.2, Vec3d(0, 10, 0));
    sphereY->getVisualGeometry()->setTranslation(Vec3d(0, 10, 0));

    auto sphereZ = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereZ", 0.8, Vec3d(0, 0, 10));
    sphereZ->getVisualGeometry()->setTranslation(Vec3d(0, 0, 10));
    //scene->addSceneObject(sphere1Obj);
    //scene->addSceneObject(sphere2Obj);

    //auto trackCtrl1 = std::make_shared<imstk::DeviceTracker>(client);
    //trackCtrl1->setTranslationScaling(0.1);
    //auto controller1 = std::make_shared<imstk::SceneObjectController>(sphere1Obj, trackCtrl1);
    //scene->addObjectController(controller1);
    //auto controller2 = std::make_shared<imstk::SceneObjectController>(sphere2Obj, trackCtrl1);
    //scene->addObjectController(controller2);

    // Light (white)
    auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
    whiteLight->setPosition(imstk::Vec3d(-15, -1, 0));
    whiteLight->setFocalPoint(imstk::Vec3d(1, -1, 0));
    scene->addLight(whiteLight);
    auto whiteLight2 = std::make_shared<imstk::PointLight>("whiteLight2");
    whiteLight2->setPosition(imstk::Vec3d(0, 2, 0));
    whiteLight2->setFocalPoint(imstk::Vec3d(0, -20, 0));
    scene->addLight(whiteLight2);

    // Light (red)
    auto colorLight = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight->setPosition(imstk::Vec3d(-25, -8, -4.5));
    colorLight->setFocalPoint(imstk::Vec3d(-3.7, 5.5, 7));
    colorLight->setColor(imstk::Color::White); // red
    colorLight->setSpotAngle(15);
    auto colorLight2 = std::make_shared<imstk::SpotLight>("colorLight2");
    colorLight2->setPosition(imstk::Vec3d(-25, -8, -4.5));
    colorLight2->setFocalPoint(imstk::Vec3d(-3.7, 5.5, 7));
    colorLight2->setColor(imstk::Color::White);
    colorLight2->setSpotAngle(15);
    auto colorLight3 = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight3->setPosition(imstk::Vec3d(-25, -5, -2.5));  //-25, -5, -2.5
    colorLight3->setFocalPoint(imstk::Vec3d(-3.7, 5.5, 7));
    colorLight3->setColor(imstk::Color::White); // red
    colorLight3->setSpotAngle(15);
    scene->addLight(colorLight);
    //scene->addLight(colorLight2);
    scene->addLight(colorLight3);

    // Update Camera
    auto cam1 = scene->getCamera();
    cam1->setPosition(imstk::Vec3d(-20, -15, -4.5));
    cam1->setFocalPoint(imstk::Vec3d(-4, 12, 7));
    cam1->setViewAngle(75);

    //// Tetrahedral mesh
    imstk::StdVectorOfVec3d vertListTet;
    std::vector<imstk::TetrahedralMesh::TetraArray> tetConnectivity;
    //physXLink linkVolSur;
    std::vector<physXLink> linksVolSur;
    bool flag = loadTetFile(DATA_ROOT_PATH"/VESS/rect_0602_SB.tet", vertListTet, tetConnectivity, linksVolSur); //  rect_new_SB  colon_tumor3_3_SB for test   colon_tumor3_SB  cubeTest2_SB
    auto tetMesh = std::make_shared<imstk::VESSTetrahedralMesh>();
    tetMesh->setInitialVertexPositions(vertListTet);
    tetMesh->setVertexPositions(vertListTet);
    tetMesh->setTetrahedraVertices(tetConnectivity);
    tetMesh->setPhysXLink(linksVolSur);
    tetMesh->setInitialPhysVertex(vertListTet);
    std::vector<Vec3d> centralLine;
    centralLine.push_back(Vec3d(-19.4, -5, 1.15));
    centralLine.push_back(Vec3d(13.3, -5, 1.15));
    //tetMesh->extractSurfaceMesh(); // both sides of volume mesh
    tetMesh->extractSurfaceMesh(centralLine); // only inside colon of surface of volume mesh


    // colon wall
    std::string pathColon = DATA_ROOT_PATH"/VESS/rect_0602_SB.obj"; // colon_tumor3_SB cubeTest2_SB  colon_s022.dae"; //  colon_s02.dae"; //  colon_s12.dae"; // volume mesh for colliding // key==colonnew_seg4.dae  colonnew_seg0508.dae small segment// good segment  colonnew_seg4.dae   colonnew_seg3.obj colon_move2.DAE"; //  VESS / colonnew_seg3.obj"; // glove.dae"; //  colon_move2.DAE";    // colon_move2.DAE  colon1_part_max.DAE  colon1_part_max.DAE  colon6_max.DAEcloth.DAE  colon_1_c2_max  colon_cylinder2_tri_max
    std::string pathColon2 = DATA_ROOT_PATH"/VESS/colonrectum_s001.obj";  //  colon_s01
    std::string pathColon3 = DATA_ROOT_PATH"/VESS/colonrectum_s003.dae";  // colon_s03

    auto objMesh = imstk::MeshIO::read(pathColon);   // collidingColonMesh; //  imstk::MeshIO::read(pathColon);
    auto physicsColonMesh = tetMesh; //  collidingColonMesh; //  imstk::MeshIO::read(pathColon);

    auto objMesh2 = imstk::MeshIO::read(pathColon2);
    auto objMesh3 = imstk::MeshIO::read(pathColon3);
    auto visualColonMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh); // physicsColonMesh->getSurfaceMesh(); //  
    auto visualColonMesh2 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh2);
    auto visualColonMesh3 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh3);
    auto collidingColonMesh = physicsColonMesh; //  visualColonMesh; //  imstk::MeshIO::read(pathColon);

    //XZH
    FILE *outMesh = fopen("G:/outOBJRect2Tri.dat", "w");
    //visualColonMesh->getInitialVerticesPositions();
    fprintf(outMesh, "num vertices:%d\n", visualColonMesh->getNumVertices());
    Vec3d tempPos;
    for (int i = 0; i < visualColonMesh->getNumVertices(); ++i){
        tempPos = visualColonMesh->getVertexPosition(i);
        fprintf(outMesh, "%f %f %f\n", tempPos.x(), tempPos.y(), tempPos.z());
    }
    fprintf(outMesh, "\n num tri:%d\n", visualColonMesh->getNumTriangles());
    for (size_t triId = 0; triId < visualColonMesh->getNumTriangles(); ++triId)
    {
        Vec3d center(0, 0, 0);
        auto vert = visualColonMesh->getTrianglesVertices().at(triId); // getTetrahedronVertices(triId);
        fprintf(outMesh, "%d %d %d\n", vert[0], vert[1], vert[2]);
        for (size_t i = 0; i < 3; ++i)
        {
            center = visualColonMesh->getInitialVertexPosition(vert[i]);
            //fprintf(outMesh, "i: %d %f %f %f  ", i, center.x(), center.y(), center.z());
        }
        //fprintf(outMesh, "\n");
    }

    fclose(outMesh);
    // XZH END

    auto material = std::make_shared<RenderMaterial>();
    auto diffuseTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/rectTest.png", Texture::Type::DIFFUSE);
    auto normalTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material->addTexture(diffuseTexture);
    //material->addTexture(normalTexture);
    //material->addTexture(roughnessTexture);
    //material->setTexture(metalnessTexture);
    visualColonMesh->setRenderMaterial(material);

    auto material2 = std::make_shared<RenderMaterial>();
    auto diffuseTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    auto normalTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material2->addTexture(diffuseTexture2);
    material2->addTexture(normalTexture2);
    material2->addTexture(roughnessTexture2);
    visualColonMesh2->setRenderMaterial(material2);

    auto material3 = std::make_shared<RenderMaterial>();
    auto diffuseTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    auto normalTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material3->addTexture(diffuseTexture3);
    material3->addTexture(normalTexture3);
    material3->addTexture(roughnessTexture3);
    visualColonMesh3->setRenderMaterial(material3);

    visualColonMesh->computeBoundingBox(min1, max1);
    outpointtri = fopen("G:/AABB.txt", "a");
    fprintf(outpointtri, "V core Colon: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    physicsColonMesh->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "C&P core Colon: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    visualColonMesh2->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "V1 Colon1: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    visualColonMesh3->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "V3 Colon3: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    // floor

    //imstk::StdVectorOfVec3d vertList;
    double width = 60.0;
    double height = 60.0;
    int nRows = 11;
    int nCols = 11;
    int corner[4] = { 1, nRows, nRows*nCols - nCols + 1, nRows*nCols };
    char intStr[33];
    std::string fixed_corner;
    //for (unsigned int i = 0; i < 4; ++i)
    //{
    //	std::sprintf(intStr, "%d", corner[i]);
    //	fixed_corner += std::string(intStr) + ' ';
    //}

    //fixed point
    Vec3d tmpPos;
    std::vector<int> fixedVertice;
    //for (int i = 0; i < physicsColonMesh->getNumVertices(); ++i) // physicsColonMesh  visualColonMesh
    //{
    //    tmpPos = physicsColonMesh->getVertexPosition(i);  // physicsColonMesh  visualColonMesh
    //    if ((tmpPos.x() < -9) || (tmpPos.x() > 9) || (tmpPos.z() < -9) || (tmpPos.z() > 9))  //  || (tmpPos.z() > 3.0   <-1.3  >1.6  smoothed colon segment  (tmpPos.x() < -1.5) || (tmpPos.x() > 1.6)
    //    {
    //        std::sprintf(intStr, "%d", i + 1);
    //        fixed_corner += std::string(intStr) + ' ';
    //        fixedVertice.push_back(i);
    //    }
    //}
    int tmp;
    tmp = 0;
    std::sprintf(intStr, "%d", tmp + 1);
    fixed_corner += std::string(intStr) + ' ';
    fixedVertice.push_back(tmp);
    tmp = 1;
    std::sprintf(intStr, "%d", tmp + 1);
    fixed_corner += std::string(intStr) + ' ';
    fixedVertice.push_back(tmp);
    tmp = 3;
    std::sprintf(intStr, "%d", tmp + 1);
    fixed_corner += std::string(intStr) + ' ';
    fixedVertice.push_back(tmp);
    //tmp = 4;
    //std::sprintf(intStr, "%d", tmp + 1);
    //fixed_corner += std::string(intStr) + ' ';
    //fixedVertice.push_back(tmp);

    fprintf(outpointtri, "V_Colon: Fixed point %s \n ", fixed_corner.c_str());

    // set fixed in the tetmesh
    tetMesh->setFixedVertice(fixedVertice);
    tetMesh->updateFixedVertice();

    // Plane
    auto geom1 = std::make_shared<imstk::Plane>();
    geom1->setPosition(Vec3d(-17, 0, 0));
    //geom1->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    geom1->rotate(imstk::FORWARD_VECTOR, imstk::PI_2);
    geom1->setWidth(30.0);
    geom1->print();
    Vec3d temp = geom1->getNormal();
    fprintf(outpointtri, "plane 1\n norm: %f %f %f\n", temp.x(), temp.y(), temp.z());
    temp = geom1->getPosition();
    fprintf(outpointtri, "position: %f %f %f\n", temp.x(), temp.y(), temp.z());
    auto planeObj1 = std::make_shared<imstk::VisualObject>("VisualPlane1");
    planeObj1->setVisualGeometry(geom1);
    // Plane
    auto geom2 = std::make_shared<imstk::Plane>();
    geom2->setPosition(Vec3d(-5.5, 0, 0));
    //geom2->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    geom2->rotate(imstk::FORWARD_VECTOR, imstk::PI_2);
    geom2->setWidth(30.0);
    geom2->print();
    temp = geom2->getNormal();
    fprintf(outpointtri, "plane 2\n norm: %f %f %f\n", temp.x(), temp.y(), temp.z());
    temp = geom2->getPosition();
    fprintf(outpointtri, "position: %f %f %f\n", temp.x(), temp.y(), temp.z());
    auto planeObj2 = std::make_shared<imstk::VisualObject>("VisualPlane2");
    planeObj2->setVisualGeometry(geom2);
    //scene->addSceneObject(planeObj1);
    //scene->addSceneObject(planeObj2);

    fclose(outpointtri);

    // Construct a map
    auto colonMapP2V = std::make_shared<imstk::VESSTetraTriangleMap>();  // <imstk::OneToOneMap>();
    colonMapP2V->setMaster(physicsColonMesh);
    colonMapP2V->setSlave(visualColonMesh);
    colonMapP2V->compute();


    auto colonMapP2C = std::make_shared<imstk::OneToOneMap>();
    colonMapP2C->setMaster(physicsColonMesh);
    colonMapP2C->setSlave(collidingColonMesh);
    colonMapP2C->compute();

    auto colonMapC2V = std::make_shared<imstk::VESSTetraTriangleMap>();  //<imstk::OneToOneMap>();
    colonMapC2V->setMaster(collidingColonMesh);
    colonMapC2V->setSlave(visualColonMesh);
    colonMapC2V->compute();

    auto colonObj = std::make_shared<PbdObject>("Colon");
    colonObj->setCollidingGeometry(collidingColonMesh);
    colonObj->setVisualGeometry(visualColonMesh);
    colonObj->setPhysicsGeometry(physicsColonMesh);
    colonObj->setPhysicsToCollidingMap(colonMapP2C);
    colonObj->setPhysicsToVisualMap(colonMapP2V);
    colonObj->setCollidingToVisualMap(colonMapC2V);

    // Create object and add to scene  just for scene, no colliding  for the two sides part, middle for operation part
    auto object2 = std::make_shared<imstk::VisualObject>("visualColonMesh2");
    object2->setVisualGeometry(visualColonMesh2); // change to any mesh created above
    //scene->addSceneObject(object2);
    auto object3 = std::make_shared<imstk::VisualObject>("visualColonMesh3");
    object3->setVisualGeometry(visualColonMesh3); // change to any mesh created above
    //scene->addSceneObject(object3);

    // wire frame for surface of volume mesh
    // START
    auto object4 = std::make_shared<imstk::VisualObject>("visualColonMesh4");
    auto material4 = std::make_shared<RenderMaterial>();
    material4->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmp4 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto surfM = tetMesh->getSurfaceMesh();
    material4->addTexture(tmp4);
    surfM->setRenderMaterial(material4);
    object4->setVisualGeometry(surfM); // change to any mesh created above
    scene->addSceneObject(object4);

    //FILE *outNormal = fopen("G:/surfacenorm.dat","w");
    //fprintf(outNormal, "num:%d\n", surfM->getNumTriangles());
    std::vector<SurfaceMesh::TriangleArray> elements = surfM->getTrianglesVertices();
    for (int i = 0; i < surfM->getNumTriangles(); i++)
        //if (0)
    {
        //int i = 0;
        Vec3d tmp = surfM->getTriangleNormal(i);
        SurfaceMesh::TriangleArray& e = elements[i];
        const Vec3d v0 = surfM->getVertexPosition(e[0]);
        const Vec3d v1 = surfM->getVertexPosition(e[1]);
        const Vec3d v2 = surfM->getVertexPosition(e[2]);

        Vec3d cenPos = (v0 + v1 + v2) / 3;
        // XZH
        Vec3d p0 = Vec3d(-19.4, -5.85, 1.15);
        Vec3d p1 = Vec3d(-3.3, 2.4, 1.05);
        Vec3d a = cenPos - p0;
        Vec3d b = p1 - p0;
        Vec3d c = ((a.dot(b)) / (b.dot(b)))*b;
        Vec3d ee = a - c;
        if (tmp.dot(-ee) < 0)  // normal indicating the outside colon
        {
            //continue;
            if ((tmp.dot(-ee)) + tmp.norm()*ee.norm() < 0.1)
            {
                printf("cenPos: %f %f %f, -ee: %f %f %f\n", cenPos.x(), cenPos.y(), cenPos.z(), -ee.x(), -ee.y(), -ee.z());
            }
        }
        else
        {
            continue;
            //fprintf(outNormal, "%d\n", i);
        }
        continue;
        // XZH END

        // XZH using for the central line of colon
        auto surfTTT = std::make_shared<imstk::SurfaceMesh>();
        imstk::StdVectorOfVec3d vertListTTT;
        vertListTTT.resize(3);
        vertListTTT[0] = cenPos;
        tmp.normalize();
        vertListTTT[1] = cenPos + tmp;
        vertListTTT[2] = cenPos;
        //fprintf(outNormal, "Pos: %f %f %f 2: %f %f %f\n", cenPos.x(), cenPos.y(), cenPos.z(), cenPos.x() + tmp.x() * 10, cenPos.y() + tmp.y() * 10, cenPos.z() + tmp.z() * 10);

        surfTTT->setInitialVertexPositions(vertListTTT);
        surfTTT->setVertexPositions(vertListTTT);
        // c. Add connectivity data
        std::vector<imstk::SurfaceMesh::TriangleArray> trianglesTTT;
        imstk::SurfaceMesh::TriangleArray triTTT[1];
        triTTT[0] = { { 0, 1, 2 } };
        trianglesTTT.push_back(triTTT[0]);
        surfTTT->setTrianglesVertices(trianglesTTT);
        char str[25];
        sprintf(str, "surfaceNormal %d", i);
        auto objectTTT = std::make_shared<imstk::VisualObject>(str);
        auto materialTTT = std::make_shared<RenderMaterial>();
        materialTTT->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
        auto tmpTTT = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
        materialTTT->addTexture(tmpTTT);
        surfTTT->setRenderMaterial(materialTTT);
        objectTTT->setVisualGeometry(surfTTT); // change to any mesh created above

        scene->addSceneObject(objectTTT);
        continue;

        auto surfTTT1 = std::make_shared<imstk::SurfaceMesh>();
        imstk::StdVectorOfVec3d vertListTTT1;
        vertListTTT1.resize(3);
        vertListTTT1[0] = cenPos;
        Vec3d tmpTTT1 = -ee;
        //tmpTTT1.normalize();
        vertListTTT1[1] = cenPos + tmpTTT1;
        vertListTTT1[2] = cenPos;
        //fprintf(outNormal, "Pos: %f %f %f 2: %f %f %f\n", cenPos.x(), cenPos.y(), cenPos.z(), cenPos.x() + tmp.x() * 10, cenPos.y() + tmp.y() * 10, cenPos.z() + tmp.z() * 10);

        surfTTT1->setInitialVertexPositions(vertListTTT1);
        surfTTT1->setVertexPositions(vertListTTT1);
        // c. Add connectivity data
        std::vector<imstk::SurfaceMesh::TriangleArray> trianglesTTT1;
        imstk::SurfaceMesh::TriangleArray triTTT1[1];
        triTTT1[0] = { { 0, 1, 2 } };
        trianglesTTT1.push_back(triTTT1[0]);
        surfTTT1->setTrianglesVertices(trianglesTTT1);
        sprintf(str, "surfaceLine %d", i);
        auto objectTTT1 = std::make_shared<imstk::VisualObject>(str);
        auto materialTTT1 = std::make_shared<RenderMaterial>();
        materialTTT1->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
        //auto tmpTTTT = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
        //materialTTT1->addTexture(tmpTTTT);
        surfTTT1->setRenderMaterial(materialTTT1);
        objectTTT1->setVisualGeometry(surfTTT1); // change to any mesh created above


        scene->addSceneObject(objectTTT1);
        // XZH END
    }
    //fclose(outNormal);
    FILE *planeInfo = fopen("G:/planenorm.dat", "w");
    // rotate matrix from 2 vectors
    Vec3d nn0 = Vec3d(0, 1, 0);
    Vec3d nn1 = Vec3d(0, 0, 1);   // (-0.95, 1.84, -0.42);
    nn0.normalize();
    nn1.normalize();
    double dot = nn0.dot(nn1);
    if (dot < -1.0) dot = -1.0;
    if (dot > 1.0) dot = 1.0;
    double t = acos(dot); //  acos(nn0.dot(nn1));
    Vec3d nn2 = nn0.cross(nn1);
    nn2.normalize();
    //use angle&angle to make rotate matrix   http://blog.atelier39.org/cg/463.html
    double u = nn2.x();
    double v = nn2.y();
    double w = nn2.z();
    Mat3d rotTmp;
    rotTmp(0, 0) = u*u + (v*v + w*w)*cos(t);  rotTmp(0, 1) = u*v*(1 - cos(t)) - w*sin(t); rotTmp(0, 2) = u*w*(1 - cos(t)) + v*sin(t);
    rotTmp(1, 0) = u*v*(1 - cos(t)) + w*sin(t); rotTmp(1, 1) = v*v + (u*u + w*w)*cos(t); rotTmp(1, 2) = v*w*(1 - cos(t)) - u*sin(t);
    rotTmp(2, 0) = u*w*(1 - cos(t)) - v*sin(t); rotTmp(2, 1) = v*w*(1 - cos(t)) + u*sin(t); rotTmp(2, 2) = w*w + (u*u + v*v)*cos(t);

    // Plane default normal is (0,1,0) position (0,0,0)
    auto plane0 = std::make_shared<imstk::Plane>();
    //plane0->setPosition(Vec3d(0, 10, 0));  // -4.68, -0.63, 1.48
    plane0->translate(Vec3d(-4.68, -0.63, -3.9));  // -4.68, -0.63, 1.48
    //geom1->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    //plane0->rotate(imstk::FORWARD_VECTOR, imstk::PI_2);
    plane0->rotate(rotTmp);
    plane0->setWidth(30.0);
    plane0->print();
    Vec3d ttmp0 = plane0->getNormal();
    fprintf(planeInfo, "plane 1\n norm: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    ttmp0 = plane0->getPosition();
    fprintf(planeInfo, "position: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    auto planeOmni0 = std::make_shared<imstk::CollidingObject>("VisualPlane Omni0");
    planeOmni0->setVisualGeometry(plane0);
    planeOmni0->setCollidingGeometry(plane0);
    //scene->addSceneObject(planeOmni0);

    // Plane default normal is (0,1,0) position (0,0,0)
    auto plane1 = std::make_shared<imstk::Plane>();
    //plane0->setPosition(Vec3d(0, 10, 0));  // -4.68, -0.63, 1.48
    plane1->translate(Vec3d(-4.68, -0.63, 6.8));  // -4.68, -0.63, 1.48
    //geom1->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    //plane0->rotate(imstk::FORWARD_VECTOR, imstk::PI_2);
    plane1->rotate(rotTmp);
    plane1->setWidth(30.0);
    plane1->print();
    ttmp0 = plane1->getNormal();
    fprintf(planeInfo, "plane 2\n norm: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    ttmp0 = plane1->getPosition();
    fprintf(planeInfo, "position: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    auto planeOmni1 = std::make_shared<imstk::CollidingObject>("VisualPlane Omni1");
    planeOmni1->setVisualGeometry(plane1);
    planeOmni1->setCollidingGeometry(plane1);
    //scene->addSceneObject(planeOmni1);
    fclose(planeInfo);
    // END wire frame

    auto pbdColonModel = std::make_shared<PbdModel>();
    pbdColonModel->configure(/*Number of Constraints*/2,
        /*Constraint configuration*/"Volume 0.05",  // 0.9 "Dihedral 0.8", // 0.01 0.8 effect is couhe not very well
        /*Constraint configuration*/"Distance 0.05",  // 0.8 effect is couhe
        /*Mass*/0.1, // 0.1
        /*Gravity*/"0 0 0",  // "0 -9.8 0",
        /*TimeStep*/0.001, // 0.001
        /*FixedPoint*/ fixed_corner.c_str(),
        /*NumberOfIterationInConstraintSolver*/3, //   2 12
        /*Proximity*/0.01,  // 0.1
        /*Contact stiffness*/0.1);
    colonObj->setDynamicalModel(pbdColonModel);

    auto pbdColonSolver = std::make_shared<PbdSolver>();
    pbdColonSolver->setPbdObject(colonObj);
    scene->addNonlinearSolver(pbdColonSolver);

    scene->addSceneObject(colonObj);

    // Collisions
    auto colGraph = scene->getCollisionGraph();
    auto pair = std::make_shared<PbdInteractionPair>(PbdInteractionPair(object, colonObj));
    pair->setNumberOfInterations(3); // 2

    colGraph->addInteractionPair(pair);

    // Sphere-plane
    //auto trackCtrl2 = std::make_shared<imstk::DeviceTracker>(client);
    //trackCtrl2->setTranslationScaling(0.25);  // 0.1  test 0.1 should use  Scale 0.001 will be better 
    ////Create and add virtual coupling object controller in the scene
    //auto objController = std::make_shared<imstk::SceneObjectController>(sphere3Obj, trackCtrl2);
    //scene->addObjectController(objController);
    //colGraph->addInteractionPair(planeOmni0, object,
    //    CollisionDetection::Type::UnidirectionalPlaneToSphere,
    //    CollisionHandling::Type::None,
    //    CollisionHandling::Type::Penalty);

    // Run
    sdk->setActiveScene(scene);
    sdk->startSimulation(true);
#endif
}

// test force feedback with virtual coupling for rect small mesh for debug
void testPbdVirtualCoupling17()
{
    //Vec3d temp00 = Vec3d(0, 12, 1);
    //Vec3d temp01 = temp00.normalized();  //  has return value
    //return;
#ifdef iMSTK_USE_OPENHAPTICS
    // SDK and Scene
    auto sdk = std::make_shared<imstk::SimulationManager>();
    auto scene = sdk->createNewScene("SceneTestDevice");

    // Device Client
    auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

    // Device Server
    auto server = std::make_shared<imstk::HDAPIDeviceServer>();
    server->addDeviceClient(client);
    sdk->addModule(server);

    std::string path2obj = DATA_ROOT_PATH"/VESS/ITKnife7_4.obj"; // knife correct ITKnife6 // cube knife knife3.obj";  // //  113.obj"; //  /  blade2.obj  cube.obj  kcube.obj  test cloth using this 
    std::string path2obj_c = DATA_ROOT_PATH"/VESS/ITKnife7_4.obj"; // knife correct ITKnife6  // cube knife knife3.obj";  //  //  113.obj";  // / VESS / TriangleTipKnife2.obj";  // blade2.obj  cube.obj kcube_s2.obj
    auto readMesh = imstk::MeshIO::read(path2obj_c); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);

    // Line mesh
    auto lineMesh = std::make_shared<imstk::LineMesh>();
    imstk::StdVectorOfVec3d vertList;
    vertList.resize(2);  // using 5 segment
    //vertList[0] = imstk::Vec3d(-13, -2.25, 9.15); // should - volume mesh therold than surface mesh (9.0, -1.9, -0.6) AABB 0: 9.0, -2.23, -0.6   -0.7 ITKnife6.DAE  // knife3.obj 9.5, -0.9, 1.18
    //vertList[1] = imstk::Vec3d(-5.93, -1.825, 9.125); // 9.7 ITKnife6.DAE // knife3.obj 9.5, -0.9, 8.73
    //vertList[2] = imstk::Vec3d(1.14, -1.4, 9.1); // (9.0, -2.55, 9.5)  AABB 1: 9.0, -2.23, 9.6  9.7 ITKnife6.DAE // knife3.obj 9.5, -0.9, 8.73
    vertList[0] = imstk::Vec3d(-16.4, -1.85, 9.15);
    vertList[1] = imstk::Vec3d(-9.3, -1.4, 9.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    std::vector<std::vector<int> > connectivity;
    for (int i = 0; i < 1;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity.push_back(line);
    }

    lineMesh->setInitialVertexPositions(vertList);
    lineMesh->setVertexPositions(vertList);
    lineMesh->setConnectivity(connectivity);

    // using for line mesh display
    auto lineSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertListLine;
    vertListLine.resize(3);
    vertListLine[0] = imstk::Vec3d(-16.4, -1.85, 9.15);
    vertListLine[1] = imstk::Vec3d(-9.3, -1.4, 9.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertListLine[2] = imstk::Vec3d(-16.4, -1.85, 9.15);
    std::vector<std::vector<int> > connectivityLine;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivityLine.push_back(line);
    }

    lineSurfMesh->setInitialVertexPositions(vertListLine);
    lineSurfMesh->setVertexPositions(vertListLine);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> trianglesLine;
    imstk::SurfaceMesh::TriangleArray triLine[1];
    triLine[0] = { { 0, 1, 2 } };
    trianglesLine.push_back(triLine[0]);
    lineSurfMesh->setTrianglesVertices(trianglesLine);
    auto objectLine = std::make_shared<imstk::VisualObject>("visualLineMesh");
    auto materialLine = std::make_shared<RenderMaterial>();
    materialLine->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpLine = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialLine->addTexture(tmpLine);
    lineSurfMesh->setRenderMaterial(materialLine);
    objectLine->setVisualGeometry(lineSurfMesh); // change to any mesh created above
    scene->addSceneObject(objectLine);
    // END

    // using for force normal
    auto tempSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertList2;
    vertList2.resize(3);
    vertList2[0] = imstk::Vec3d(-16.4, -1.85, 9.15);
    vertList2[1] = imstk::Vec3d(-9.3, -1.4, 9.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertList2[2] = imstk::Vec3d(-16.4, -1.85, 9.15);
    std::vector<std::vector<int> > connectivity2;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity2.push_back(line);
    }

    tempSurfMesh->setInitialVertexPositions(vertList2);
    tempSurfMesh->setVertexPositions(vertList2);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> triangles;
    imstk::SurfaceMesh::TriangleArray tri[1];
    tri[0] = { { 0, 1, 2 } };
    triangles.push_back(tri[0]);
    tempSurfMesh->setTrianglesVertices(triangles);
    auto objectTmp = std::make_shared<imstk::VisualObject>("visualNormal0");
    auto materialTmp = std::make_shared<RenderMaterial>();
    materialTmp->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpTT = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialTmp->addTexture(tmpTT);
    tempSurfMesh->setRenderMaterial(materialTmp);
    objectTmp->setVisualGeometry(tempSurfMesh); // change to any mesh created above
    scene->addSceneObject(objectTmp);
    // END

    // XZH using for the central line of colon
    auto centrallineSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertList3;
    vertList3.resize(3);
    vertList3[0] = imstk::Vec3d(-19.4, -5, 1.15);
    vertList3[1] = imstk::Vec3d(13.3, -5, 1.15);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertList3[2] = imstk::Vec3d(-19.4, -5, 1.15);
    std::vector<std::vector<int> > connectivity3;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity3.push_back(line);
    }

    centrallineSurfMesh->setInitialVertexPositions(vertList3);
    centrallineSurfMesh->setVertexPositions(vertList3);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> triangles1;
    imstk::SurfaceMesh::TriangleArray tri1[1];
    tri1[0] = { { 0, 1, 2 } };
    triangles1.push_back(tri1[0]);
    centrallineSurfMesh->setTrianglesVertices(triangles1);
    auto objectCentral = std::make_shared<imstk::VisualObject>("visualNormal1");
    auto materialCentral = std::make_shared<RenderMaterial>();
    materialCentral->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpCentral = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialCentral->addTexture(tmpCentral);
    centrallineSurfMesh->setRenderMaterial(materialCentral);
    objectCentral->setVisualGeometry(centrallineSurfMesh); // change to any mesh created above
    scene->addSceneObject(objectCentral);
    // XZH END

    auto visualMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(readMesh);
    auto physicsMesh = lineMesh; //visualMesh; //  lineMesh; //  imstk::MeshIO::read(path2obj_c);  //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
    auto collidingMesh = lineMesh; // visualMesh; //  lineMesh;
    Vec3d min1, max1;
    visualMesh->computeBoundingBox(min1, max1);

    auto material0 = std::make_shared<RenderMaterial>();
    auto diffuseTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto normalTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Normal.png", Texture::Type::NORMAL);
    auto roughnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Roughness.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material0->addTexture(diffuseTexture0);
    material0->addTexture(normalTexture0);
    material0->addTexture(roughnessTexture0);
    //material->setTexture(metalnessTexture);
    visualMesh->setRenderMaterial(material0);

    FILE *outpointtri;
    outpointtri = fopen("G:/AABB.txt", "w");
    fprintf(outpointtri, "V: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    collidingMesh->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "C: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    fclose(outpointtri);

    auto object = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject");

    auto objMapP2V = std::make_shared<imstk::OneToOneMap>();  // std::make_shared<imstk::TriangleLineMap>(); //  std::make_shared<imstk::OneToOneMap>();
    objMapP2V->setMaster(physicsMesh);
    objMapP2V->setSlave(visualMesh);
    objMapP2V->compute();


    auto objMapP2C = std::make_shared<imstk::OneToOneMap>();  //  colliding is physical mesh
    objMapP2C->setMaster(physicsMesh);
    objMapP2C->setSlave(collidingMesh);
    objMapP2C->compute();

    auto objMapC2V = std::make_shared<imstk::OneToOneMap>(); //  std::make_shared<imstk::TriangleLineMap>(); //  s std::make_shared<imstk::OneToOneMap>();
    objMapC2V->setMaster(collidingMesh);
    objMapC2V->setSlave(visualMesh);
    objMapC2V->compute();

    auto objMapC2P = std::make_shared<imstk::OneToOneMap>(); // colliding is physical mesh
    objMapC2P->setMaster(collidingMesh);
    objMapC2P->setSlave(physicsMesh);
    objMapC2P->compute();

    object->setVisualGeometry(visualMesh);
    object->setCollidingGeometry(collidingMesh);
    object->setPhysicsGeometry(physicsMesh);
    object->setPhysicsToCollidingMap(objMapP2C);
    object->setPhysicsToVisualMap(objMapP2V);
    object->setCollidingToVisualMap(objMapC2V);
    object->setColldingToPhysicsMap(objMapC2P);

    auto pbdModel = std::make_shared<PbdModel>();
    pbdModel->configure(/*Number of Constraints*/1,
        /*Constraint configuration*/"Distance 0.1",
        /*Mass*/0.0,
        /*Gravity*/"0 0 0",
        /*TimeStep*/0.0003,
        /*FixedPoint*/"",
        /*NumberOfIterationInConstraintSolver*/2, // 5
        /*Proximity*/0.3,  // use linemesh 0.3  // 0.01 use  //  cube 0.1
        /*Contact stiffness*/0.001);
    object->setDynamicalModel(pbdModel);

    //auto pbdSolver = std::make_shared<PbdSolver>();
    //pbdSolver->setPbdObject(object);
    //scene->addNonlinearSolver(pbdSolver);

    scene->addSceneObject(object);

    auto trackCtrl = std::make_shared<imstk::DeviceTracker>(client);
    trackCtrl->setTranslationScaling(0.25);  // 0.1  test 0.1 should use  Scale 0.001 will be better 
    auto controller = std::make_shared<imstk::PBDSceneObjectController>(object, trackCtrl);
    scene->addObjectController(controller);

    // Sphere1
    auto sphere1Obj = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "Sphere1", 0.5, Vec3d(-16.4, -1.85, 9.15)); // -2.23
    sphere1Obj->getVisualGeometry()->setTranslation(Vec3d(-16.4, -1.85, 9.15));
    //auto materialTT = std::make_shared<Material>();
    //materialTT->setTexture(diffuseTexture0);
    //auto suMesh = std::dynamic_pointer_cast<SurfaceMesh>(sphere1Obj->getVisualGeometry());
    //suMesh->addMaterial(materialTT);
    //auto sphere2Obj = apiutils::createCollidingAnalyticalSceneObject(
    //    imstk::Geometry::Type::Sphere, scene, "Sphere2", 0.5, Vec3d(-9.3, -1.4, 9.05)); //-9.3, -1.4, 9.05    new -9.53, - 1.50,    8.6  org  -7.2, -1.35, 8.62
    //sphere2Obj->getVisualGeometry()->setTranslation(Vec3d(-9.3, -1.4, 9.05));
    //sphere2Obj->setCollidingGeometry(sphere2Obj->getVisualGeometry());

    auto visualSphere22 = std::make_shared<imstk::Sphere>();
    visualSphere22->setTranslation(Vec3d(-9.3, -1.4, 9.05));
    visualSphere22->setRadius(0.5);
    Vec3d mm2 = visualSphere22->getPosition();
    auto sphere2Obj = std::make_shared<CollidingObject>("Sphere2");
    sphere2Obj->setCollidingGeometry(visualSphere22);
    sphere2Obj->setVisualGeometry(visualSphere22);
    scene->addSceneObject(sphere2Obj);

    auto visualSphere2 = std::make_shared<imstk::Sphere>();
    visualSphere2->setTranslation(Vec3d(-9.3, -1.4, 9.05));
    visualSphere2->setRadius(0.9);
    Vec3d mmm = visualSphere2->getPosition();
    auto sphere3Obj = std::make_shared<CollidingObject>("Sphere3");
    sphere3Obj->setCollidingGeometry(visualSphere2);
    sphere3Obj->setVisualGeometry(visualSphere2);
    scene->addSceneObject(sphere3Obj);


    auto sphereOrg = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereOrg", 0.2, Vec3d(0, 0, 0));
    sphereOrg->getVisualGeometry()->setTranslation(Vec3d(0, 0, 0));

    auto sphereX = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereX", 1.5, Vec3d(10, 0, 0));
    sphereX->getVisualGeometry()->setTranslation(Vec3d(10, 0, 0));

    auto sphereY = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereY", 1.2, Vec3d(0, 10, 0));
    sphereY->getVisualGeometry()->setTranslation(Vec3d(0, 10, 0));

    auto sphereZ = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereZ", 0.8, Vec3d(0, 0, 10));
    sphereZ->getVisualGeometry()->setTranslation(Vec3d(0, 0, 10));
    //scene->addSceneObject(sphere1Obj);
    //scene->addSceneObject(sphere2Obj);

    //auto trackCtrl1 = std::make_shared<imstk::DeviceTracker>(client);
    //trackCtrl1->setTranslationScaling(0.1);
    //auto controller1 = std::make_shared<imstk::SceneObjectController>(sphere1Obj, trackCtrl1);
    //scene->addObjectController(controller1);
    //auto controller2 = std::make_shared<imstk::SceneObjectController>(sphere2Obj, trackCtrl1);
    //scene->addObjectController(controller2);

    // Light (white)
    auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
    whiteLight->setPosition(imstk::Vec3d(-15, -1, 0));
    whiteLight->setFocalPoint(imstk::Vec3d(1, -1, 0));
    scene->addLight(whiteLight);
    auto whiteLight2 = std::make_shared<imstk::PointLight>("whiteLight2");
    whiteLight2->setPosition(imstk::Vec3d(0, 2, 0));
    whiteLight2->setFocalPoint(imstk::Vec3d(0, -20, 0));
    scene->addLight(whiteLight2);

    // Light (red)
    auto colorLight = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight->setPosition(imstk::Vec3d(-25, -8, -4.5));
    colorLight->setFocalPoint(imstk::Vec3d(-3.7, 5.5, 7));
    colorLight->setColor(imstk::Color::White); // red
    colorLight->setSpotAngle(15);
    auto colorLight2 = std::make_shared<imstk::SpotLight>("colorLight2");
    colorLight2->setPosition(imstk::Vec3d(-25, -8, -4.5));
    colorLight2->setFocalPoint(imstk::Vec3d(-3.7, 5.5, 7));
    colorLight2->setColor(imstk::Color::White);
    colorLight2->setSpotAngle(15);
    auto colorLight3 = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight3->setPosition(imstk::Vec3d(-25, -5, -2.5));  //-25, -5, -2.5
    colorLight3->setFocalPoint(imstk::Vec3d(-3.7, 5.5, 7));
    colorLight3->setColor(imstk::Color::White); // red
    colorLight3->setSpotAngle(15);
    scene->addLight(colorLight);
    //scene->addLight(colorLight2);
    scene->addLight(colorLight3);

    // Update Camera
    auto cam1 = scene->getCamera();
    cam1->setPosition(imstk::Vec3d(-20, -15, -4.5));
    cam1->setFocalPoint(imstk::Vec3d(-4, 12, 7));
    cam1->setViewAngle(75);

    //// Tetrahedral mesh
    imstk::StdVectorOfVec3d vertListTet;
    std::vector<imstk::TetrahedralMesh::TetraArray> tetConnectivity;
    //physXLink linkVolSur;
    std::vector<physXLink> linksVolSur;
    bool flag = loadTetFile(DATA_ROOT_PATH"/VESS/rect_new_SB.tet", vertListTet, tetConnectivity, linksVolSur); // colon_tumor3_3_SB for test   colon_tumor3_SB  cubeTest2_SB
    auto tetMesh = std::make_shared<imstk::VESSTetrahedralMesh>();
    tetMesh->setInitialVertexPositions(vertListTet);
    tetMesh->setVertexPositions(vertListTet);
    tetMesh->setTetrahedraVertices(tetConnectivity);
    tetMesh->setPhysXLink(linksVolSur);
    tetMesh->setInitialPhysVertex(vertListTet);
    std::vector<Vec3d> centralLine;
    centralLine.push_back(Vec3d(-19.4, -5, 1.15));
    centralLine.push_back(Vec3d(13.3, -5, 1.15));
    //tetMesh->extractSurfaceMesh(); // both sides of volume mesh
    tetMesh->extractSurfaceMesh(centralLine); // only inside colon of surface of volume mesh


    // colon wall
    std::string pathColon = DATA_ROOT_PATH"/VESS/rect_new_SB.obj"; // colon_tumor3_SB cubeTest2_SB  colon_s022.dae"; //  colon_s02.dae"; //  colon_s12.dae"; // volume mesh for colliding // key==colonnew_seg4.dae  colonnew_seg0508.dae small segment// good segment  colonnew_seg4.dae   colonnew_seg3.obj colon_move2.DAE"; //  VESS / colonnew_seg3.obj"; // glove.dae"; //  colon_move2.DAE";    // colon_move2.DAE  colon1_part_max.DAE  colon1_part_max.DAE  colon6_max.DAEcloth.DAE  colon_1_c2_max  colon_cylinder2_tri_max
    std::string pathColon2 = DATA_ROOT_PATH"/VESS/colonrectum_s001.obj";  //  colon_s01
    std::string pathColon3 = DATA_ROOT_PATH"/VESS/colonrectum_s003.dae";  // colon_s03

    auto objMesh = imstk::MeshIO::read(pathColon);   // collidingColonMesh; //  imstk::MeshIO::read(pathColon);
    auto physicsColonMesh = tetMesh; //  collidingColonMesh; //  imstk::MeshIO::read(pathColon);

    auto objMesh2 = imstk::MeshIO::read(pathColon2);
    auto objMesh3 = imstk::MeshIO::read(pathColon3);
    auto visualColonMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh); // physicsColonMesh->getSurfaceMesh(); //  
    auto visualColonMesh2 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh2);
    auto visualColonMesh3 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh3);
    auto collidingColonMesh = physicsColonMesh; //  visualColonMesh; //  imstk::MeshIO::read(pathColon);

    //XZH
    FILE *outMesh = fopen("G:/outOBJRect.dat", "w");
    //visualColonMesh->getInitialVerticesPositions();
    fprintf(outMesh, "num vertices:%d\n", visualColonMesh->getNumVertices());
    Vec3d tempPos;
    for (int i = 0; i < visualColonMesh->getNumVertices(); ++i){
        tempPos = visualColonMesh->getVertexPosition(i);
        fprintf(outMesh, "%f %f %f\n", tempPos.x(), tempPos.y(), tempPos.z());
    }
    fprintf(outMesh, "\n num tri:%d\n", visualColonMesh->getNumTriangles());
    for (size_t triId = 0; triId < visualColonMesh->getNumTriangles(); ++triId)
    {
        Vec3d center(0, 0, 0);
        auto vert = visualColonMesh->getTrianglesVertices().at(triId); // getTetrahedronVertices(triId);
        fprintf(outMesh, "%d %d %d\n", vert[0], vert[1], vert[2]);
        for (size_t i = 0; i < 3; ++i)
        {
            center = visualColonMesh->getInitialVertexPosition(vert[i]);
            //fprintf(outMesh, "i: %d %f %f %f  ", i, center.x(), center.y(), center.z());
        }
        //fprintf(outMesh, "\n");
    }

    fclose(outMesh);
    // XZH END

    auto material = std::make_shared<RenderMaterial>();
    auto diffuseTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/rectTest.png", Texture::Type::DIFFUSE);
    auto normalTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material->addTexture(diffuseTexture);
    //material->addTexture(normalTexture);
    //material->addTexture(roughnessTexture);
    //material->setTexture(metalnessTexture);
    visualColonMesh->setRenderMaterial(material);

    auto material2 = std::make_shared<RenderMaterial>();
    auto diffuseTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    auto normalTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material2->addTexture(diffuseTexture2);
    material2->addTexture(normalTexture2);
    material2->addTexture(roughnessTexture2);
    visualColonMesh2->setRenderMaterial(material2);

    auto material3 = std::make_shared<RenderMaterial>();
    auto diffuseTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    auto normalTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material3->addTexture(diffuseTexture3);
    material3->addTexture(normalTexture3);
    material3->addTexture(roughnessTexture3);
    visualColonMesh3->setRenderMaterial(material3);

    visualColonMesh->computeBoundingBox(min1, max1);
    outpointtri = fopen("G:/AABB.txt", "a");
    fprintf(outpointtri, "V core Colon: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    physicsColonMesh->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "C&P core Colon: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    visualColonMesh2->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "V1 Colon1: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    visualColonMesh3->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "V3 Colon3: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    // floor

    //imstk::StdVectorOfVec3d vertList;
    double width = 60.0;
    double height = 60.0;
    int nRows = 11;
    int nCols = 11;
    int corner[4] = { 1, nRows, nRows*nCols - nCols + 1, nRows*nCols };
    char intStr[33];
    std::string fixed_corner;
    //for (unsigned int i = 0; i < 4; ++i)
    //{
    //	std::sprintf(intStr, "%d", corner[i]);
    //	fixed_corner += std::string(intStr) + ' ';
    //}

    //fixed point
    Vec3d tmpPos;
    std::vector<int> fixedVertice;
    for (int i = 0; i < physicsColonMesh->getNumVertices(); ++i) // physicsColonMesh  visualColonMesh
    {
        tmpPos = physicsColonMesh->getVertexPosition(i);  // physicsColonMesh  visualColonMesh
        if ((tmpPos.x() < -9) || (tmpPos.x() >9) || (tmpPos.z() < -9) || (tmpPos.z() >9))  //  || (tmpPos.z() > 3.0   <-1.3  >1.6  smoothed colon segment  (tmpPos.x() < -1.5) || (tmpPos.x() > 1.6)
        {
            std::sprintf(intStr, "%d", i + 1);
            fixed_corner += std::string(intStr) + ' ';
            fixedVertice.push_back(i);
        }
    }

    fprintf(outpointtri, "V_Colon: Fixed point %s \n ", fixed_corner.c_str());

    // set fixed in the tetmesh
    tetMesh->setFixedVertice(fixedVertice);
    tetMesh->updateFixedVertice();
    tetMesh->initDeformMesh(); // init for deform mesh : injection solution 06/20/2017

    // Plane
    auto geom1 = std::make_shared<imstk::Plane>();
    geom1->setPosition(Vec3d(-17, 0, 0));
    //geom1->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    geom1->rotate(imstk::FORWARD_VECTOR, imstk::PI_2);
    geom1->setWidth(30.0);
    geom1->print();
    Vec3d temp = geom1->getNormal();
    fprintf(outpointtri, "plane 1\n norm: %f %f %f\n", temp.x(), temp.y(), temp.z());
    temp = geom1->getPosition();
    fprintf(outpointtri, "position: %f %f %f\n", temp.x(), temp.y(), temp.z());
    auto planeObj1 = std::make_shared<imstk::VisualObject>("VisualPlane1");
    planeObj1->setVisualGeometry(geom1);
    // Plane
    auto geom2 = std::make_shared<imstk::Plane>();
    geom2->setPosition(Vec3d(-5.5, 0, 0));
    //geom2->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    geom2->rotate(imstk::FORWARD_VECTOR, imstk::PI_2);
    geom2->setWidth(30.0);
    geom2->print();
    temp = geom2->getNormal();
    fprintf(outpointtri, "plane 2\n norm: %f %f %f\n", temp.x(), temp.y(), temp.z());
    temp = geom2->getPosition();
    fprintf(outpointtri, "position: %f %f %f\n", temp.x(), temp.y(), temp.z());
    auto planeObj2 = std::make_shared<imstk::VisualObject>("VisualPlane2");
    planeObj2->setVisualGeometry(geom2);
    //scene->addSceneObject(planeObj1);
    //scene->addSceneObject(planeObj2);

    fclose(outpointtri);

    // Construct a map
    auto colonMapP2V = std::make_shared<imstk::VESSTetraTriangleMap>();  // <imstk::OneToOneMap>();
    colonMapP2V->setMaster(physicsColonMesh);
    colonMapP2V->setSlave(visualColonMesh);
    colonMapP2V->compute();


    auto colonMapP2C = std::make_shared<imstk::OneToOneMap>();
    colonMapP2C->setMaster(physicsColonMesh);
    colonMapP2C->setSlave(collidingColonMesh);
    colonMapP2C->compute();

    auto colonMapC2V = std::make_shared<imstk::VESSTetraTriangleMap>();  //<imstk::OneToOneMap>();
    colonMapC2V->setMaster(collidingColonMesh);
    colonMapC2V->setSlave(visualColonMesh);
    colonMapC2V->compute();

    auto colonObj = std::make_shared<PbdObject>("Colon");
    colonObj->setCollidingGeometry(collidingColonMesh);
    colonObj->setVisualGeometry(visualColonMesh);
    colonObj->setPhysicsGeometry(physicsColonMesh);
    colonObj->setPhysicsToCollidingMap(colonMapP2C);
    colonObj->setPhysicsToVisualMap(colonMapP2V);
    colonObj->setCollidingToVisualMap(colonMapC2V);

    // Create object and add to scene  just for scene, no colliding  for the two sides part, middle for operation part
    auto object2 = std::make_shared<imstk::VisualObject>("visualColonMesh2");
    object2->setVisualGeometry(visualColonMesh2); // change to any mesh created above
    //scene->addSceneObject(object2);
    auto object3 = std::make_shared<imstk::VisualObject>("visualColonMesh3");
    object3->setVisualGeometry(visualColonMesh3); // change to any mesh created above
    //scene->addSceneObject(object3);

    // wire frame for surface of volume mesh
    // START
    auto object4 = std::make_shared<imstk::VisualObject>("visualColonMesh4");
    auto material4 = std::make_shared<RenderMaterial>();
    material4->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmp4 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto surfM = tetMesh->getSurfaceMesh();
    material4->addTexture(tmp4);
    surfM->setRenderMaterial(material4);
    object4->setVisualGeometry(surfM); // change to any mesh created above
    scene->addSceneObject(object4);

    //FILE *outNormal = fopen("G:/surfacenorm.dat","w");
    //fprintf(outNormal, "num:%d\n", surfM->getNumTriangles());
    std::vector<SurfaceMesh::TriangleArray> elements = surfM->getTrianglesVertices();
    for (int i = 0; i < surfM->getNumTriangles() ; i++)
        //if (0)
    {
        //int i = 0;
        Vec3d tmp = surfM->getTriangleNormal(i);
        SurfaceMesh::TriangleArray& e = elements[i];
        const Vec3d v0 = surfM->getVertexPosition(e[0]);
        const Vec3d v1 = surfM->getVertexPosition(e[1]);
        const Vec3d v2 = surfM->getVertexPosition(e[2]);

        Vec3d cenPos = (v0 + v1 + v2) / 3;
        // XZH
        Vec3d p0 = Vec3d(-19.4, -5.85, 1.15);
        Vec3d p1 = Vec3d(-3.3, 2.4, 1.05);
        Vec3d a = cenPos - p0;
        Vec3d b = p1 - p0;
        Vec3d c = ((a.dot(b)) / (b.dot(b)))*b;
        Vec3d ee = a - c;
        if (tmp.dot(-ee) < 0)  // normal indicating the outside colon
        {
            //continue;
            if ((tmp.dot(-ee)) + tmp.norm()*ee.norm() < 0.1)
            {
                printf("cenPos: %f %f %f, -ee: %f %f %f\n", cenPos.x(), cenPos.y(), cenPos.z(), -ee.x(), -ee.y(), -ee.z());
            }
        }
        else
        {
            continue;
            //fprintf(outNormal, "%d\n", i);
        }
        continue;
        // XZH END

        // XZH using for the central line of colon
        auto surfTTT = std::make_shared<imstk::SurfaceMesh>();
        imstk::StdVectorOfVec3d vertListTTT;
        vertListTTT.resize(3);
        vertListTTT[0] = cenPos;
        tmp.normalize();
        vertListTTT[1] = cenPos + tmp;
        vertListTTT[2] = cenPos;
        //fprintf(outNormal, "Pos: %f %f %f 2: %f %f %f\n", cenPos.x(), cenPos.y(), cenPos.z(), cenPos.x() + tmp.x() * 10, cenPos.y() + tmp.y() * 10, cenPos.z() + tmp.z() * 10);

        surfTTT->setInitialVertexPositions(vertListTTT);
        surfTTT->setVertexPositions(vertListTTT);
        // c. Add connectivity data
        std::vector<imstk::SurfaceMesh::TriangleArray> trianglesTTT;
        imstk::SurfaceMesh::TriangleArray triTTT[1];
        triTTT[0] = { { 0, 1, 2 } };
        trianglesTTT.push_back(triTTT[0]);
        surfTTT->setTrianglesVertices(trianglesTTT);
        char str[25];
        sprintf(str, "surfaceNormal %d", i);
        auto objectTTT = std::make_shared<imstk::VisualObject>(str);
        auto materialTTT = std::make_shared<RenderMaterial>();
        materialTTT->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
        auto tmpTTT = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
        materialTTT->addTexture(tmpTTT);
        surfTTT->setRenderMaterial(materialTTT);
        objectTTT->setVisualGeometry(surfTTT); // change to any mesh created above

        scene->addSceneObject(objectTTT);
        continue;

        auto surfTTT1 = std::make_shared<imstk::SurfaceMesh>();
        imstk::StdVectorOfVec3d vertListTTT1;
        vertListTTT1.resize(3);
        vertListTTT1[0] = cenPos;
        Vec3d tmpTTT1 = -ee;
        //tmpTTT1.normalize();
        vertListTTT1[1] = cenPos + tmpTTT1;
        vertListTTT1[2] = cenPos;
        //fprintf(outNormal, "Pos: %f %f %f 2: %f %f %f\n", cenPos.x(), cenPos.y(), cenPos.z(), cenPos.x() + tmp.x() * 10, cenPos.y() + tmp.y() * 10, cenPos.z() + tmp.z() * 10);

        surfTTT1->setInitialVertexPositions(vertListTTT1);
        surfTTT1->setVertexPositions(vertListTTT1);
        // c. Add connectivity data
        std::vector<imstk::SurfaceMesh::TriangleArray> trianglesTTT1;
        imstk::SurfaceMesh::TriangleArray triTTT1[1];
        triTTT1[0] = { { 0, 1, 2 } };
        trianglesTTT1.push_back(triTTT1[0]);
        surfTTT1->setTrianglesVertices(trianglesTTT1);
        sprintf(str, "surfaceLine %d", i);
        auto objectTTT1 = std::make_shared<imstk::VisualObject>(str);
        auto materialTTT1 = std::make_shared<RenderMaterial>();
        materialTTT1->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
        //auto tmpTTTT = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
        //materialTTT1->addTexture(tmpTTTT);
        surfTTT1->setRenderMaterial(materialTTT1);
        objectTTT1->setVisualGeometry(surfTTT1); // change to any mesh created above


        scene->addSceneObject(objectTTT1);
        // XZH END
    }
    //fclose(outNormal);
    FILE *planeInfo = fopen("G:/planenorm.dat", "w");
    // rotate matrix from 2 vectors
    Vec3d nn0 = Vec3d(0, 1, 0);
    Vec3d nn1 = Vec3d(0, 0, 1);   // (-0.95, 1.84, -0.42);
    nn0.normalize();
    nn1.normalize();
    double dot = nn0.dot(nn1);
    if (dot < -1.0) dot = -1.0;
    if (dot > 1.0) dot = 1.0;
    double t = acos(dot); //  acos(nn0.dot(nn1));
    Vec3d nn2 = nn0.cross(nn1);
    nn2.normalize();
    //use angle&angle to make rotate matrix   http://blog.atelier39.org/cg/463.html
    double u = nn2.x();
    double v = nn2.y();
    double w = nn2.z();
    Mat3d rotTmp;
    rotTmp(0, 0) = u*u + (v*v + w*w)*cos(t);  rotTmp(0, 1) = u*v*(1 - cos(t)) - w*sin(t); rotTmp(0, 2) = u*w*(1 - cos(t)) + v*sin(t);
    rotTmp(1, 0) = u*v*(1 - cos(t)) + w*sin(t); rotTmp(1, 1) = v*v + (u*u + w*w)*cos(t); rotTmp(1, 2) = v*w*(1 - cos(t)) - u*sin(t);
    rotTmp(2, 0) = u*w*(1 - cos(t)) - v*sin(t); rotTmp(2, 1) = v*w*(1 - cos(t)) + u*sin(t); rotTmp(2, 2) = w*w + (u*u + v*v)*cos(t);

    // Plane default normal is (0,1,0) position (0,0,0)
    auto plane0 = std::make_shared<imstk::Plane>();
    //plane0->setPosition(Vec3d(0, 10, 0));  // -4.68, -0.63, 1.48
    plane0->translate(Vec3d(-4.68, -0.63, -3.9));  // -4.68, -0.63, 1.48
    //geom1->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    //plane0->rotate(imstk::FORWARD_VECTOR, imstk::PI_2);
    plane0->rotate(rotTmp);
    plane0->setWidth(30.0);
    plane0->print();
    Vec3d ttmp0 = plane0->getNormal();
    fprintf(planeInfo, "plane 1\n norm: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    ttmp0 = plane0->getPosition();
    fprintf(planeInfo, "position: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    auto planeOmni0 = std::make_shared<imstk::CollidingObject>("VisualPlane Omni0");
    planeOmni0->setVisualGeometry(plane0);
    planeOmni0->setCollidingGeometry(plane0);
    //scene->addSceneObject(planeOmni0);

    // Plane default normal is (0,1,0) position (0,0,0)
    auto plane1 = std::make_shared<imstk::Plane>();
    //plane0->setPosition(Vec3d(0, 10, 0));  // -4.68, -0.63, 1.48
    plane1->translate(Vec3d(-4.68, -0.63, 6.8));  // -4.68, -0.63, 1.48
    //geom1->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    //plane0->rotate(imstk::FORWARD_VECTOR, imstk::PI_2);
    plane1->rotate(rotTmp);
    plane1->setWidth(30.0);
    plane1->print();
    ttmp0 = plane1->getNormal();
    fprintf(planeInfo, "plane 2\n norm: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    ttmp0 = plane1->getPosition();
    fprintf(planeInfo, "position: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    auto planeOmni1 = std::make_shared<imstk::CollidingObject>("VisualPlane Omni1");
    planeOmni1->setVisualGeometry(plane1);
    planeOmni1->setCollidingGeometry(plane1);
    //scene->addSceneObject(planeOmni1);
    fclose(planeInfo);
    // END wire frame

    auto pbdColonModel = std::make_shared<PbdModel>();
    pbdColonModel->configure(/*Number of Constraints*/2,
        /*Constraint configuration*/"Volume 0.6",  // 0.9 "Dihedral 0.8", // 0.01 0.8 effect is couhe not very well
        /*Constraint configuration*/"Distance 0.8",  // 0.8 effect is couhe
        /*Mass*/0.1, // 0.1
        /*Gravity*/"0 0 0",  // "0 -9.8 0",
        /*TimeStep*/0.001, // 0.001
        /*FixedPoint*/ fixed_corner.c_str(),
        /*NumberOfIterationInConstraintSolver*/3, //   2 12
        /*Proximity*/0.01,  // 0.1
        /*Contact stiffness*/0.1);
    colonObj->setDynamicalModel(pbdColonModel);

    auto pbdColonSolver = std::make_shared<PbdSolver>();
    pbdColonSolver->setPbdObject(colonObj);
    scene->addNonlinearSolver(pbdColonSolver);

    scene->addSceneObject(colonObj);

    // Collisions
    auto colGraph = scene->getCollisionGraph();
    auto pair = std::make_shared<PbdInteractionPair>(PbdInteractionPair(object, colonObj));
    pair->setNumberOfInterations(3); // 2

    colGraph->addInteractionPair(pair);

    // Sphere-plane
    //auto trackCtrl2 = std::make_shared<imstk::DeviceTracker>(client);
    //trackCtrl2->setTranslationScaling(0.25);  // 0.1  test 0.1 should use  Scale 0.001 will be better 
    ////Create and add virtual coupling object controller in the scene
    //auto objController = std::make_shared<imstk::SceneObjectController>(sphere3Obj, trackCtrl2);
    //scene->addObjectController(objController);
    //colGraph->addInteractionPair(planeOmni0, object,
    //    CollisionDetection::Type::UnidirectionalPlaneToSphere,
    //    CollisionHandling::Type::None,
    //    CollisionHandling::Type::Penalty);

    // Run
    sdk->setActiveScene(scene);
    sdk->startSimulation(true);
#endif
}

//// test force feedback with virtual coupling for rectum (not colon part, which has lumen) split into three segments  use real knife model 2/3 length of artist
void testPbdVirtualCoupling16()
{
    Mat4d matXzh;
    //matXzh << 1, 0, 0, 2,
    //    0, 1, 0, 3,
    //    0, 0, 1, 4,
    //    7, 8, 9, 5;
    matXzh(0, 0) = 1; matXzh(0, 1) = 0; matXzh(0, 2) = 0; matXzh(0, 3) = 2;
    matXzh(1, 0) = 0; matXzh(1, 1) = 1; matXzh(1, 2) = 0; matXzh(1, 3) = 3;
    matXzh(2, 0) = 0; matXzh(2, 1) = 0; matXzh(2, 2) = 1; matXzh(2, 3) = 4;
    matXzh(3, 0) = 7; matXzh(3, 1) = 8; matXzh(3, 2) = 9; matXzh(3, 3) = 5;
    double x1 = matXzh.coeffRef(3, 2);
    double x2 = matXzh.coeffRef(2, 3);
#ifdef iMSTK_USE_OPENHAPTICS
    // SDK and Scene
    auto sdk = std::make_shared<imstk::SimulationManager>();
    auto scene = sdk->createNewScene("SceneTestDevice");

    // Device Client
    auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

    // Device Server
    auto server = std::make_shared<imstk::HDAPIDeviceServer>();
    server->addDeviceClient(client);
    sdk->addModule(server);

    std::string path2obj = DATA_ROOT_PATH"/VESS/ITKnife7_4.obj"; // knife correct ITKnife6 // cube knife knife3.obj";  // //  113.obj"; //  /  blade2.obj  cube.obj  kcube.obj  test cloth using this 
    std::string path2obj_c = DATA_ROOT_PATH"/VESS/ITKnife7_4.obj"; // knife correct ITKnife6  // cube knife knife3.obj";  //  //  113.obj";  // / VESS / TriangleTipKnife2.obj";  // blade2.obj  cube.obj kcube_s2.obj
    auto readMesh = imstk::MeshIO::read(path2obj_c); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);

    // Line mesh
    auto lineMesh = std::make_shared<imstk::LineMesh>();
    imstk::StdVectorOfVec3d vertList;
    vertList.resize(2);  // using 5 segment
    //vertList[0] = imstk::Vec3d(-13, -2.25, 9.15); // should - volume mesh therold than surface mesh (9.0, -1.9, -0.6) AABB 0: 9.0, -2.23, -0.6   -0.7 ITKnife6.DAE  // knife3.obj 9.5, -0.9, 1.18
    //vertList[1] = imstk::Vec3d(-5.93, -1.825, 9.125); // 9.7 ITKnife6.DAE // knife3.obj 9.5, -0.9, 8.73
    //vertList[2] = imstk::Vec3d(1.14, -1.4, 9.1); // (9.0, -2.55, 9.5)  AABB 1: 9.0, -2.23, 9.6  9.7 ITKnife6.DAE // knife3.obj 9.5, -0.9, 8.73
    vertList[0] = imstk::Vec3d(-16.4, -1.85, 9.15);
    vertList[1] = imstk::Vec3d(-9.3, -1.4, 9.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    std::vector<std::vector<int> > connectivity;
    for (int i = 0; i < 1;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity.push_back(line);
    }

    lineMesh->setInitialVertexPositions(vertList);
    lineMesh->setVertexPositions(vertList);
    lineMesh->setConnectivity(connectivity);

    // using for line mesh display
    auto lineSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertListLine;
    vertListLine.resize(3);
    vertListLine[0] = imstk::Vec3d(-16.4, -1.85, 9.15);
    vertListLine[1] = imstk::Vec3d(-9.3, -1.4, 9.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertListLine[2] = imstk::Vec3d(-16.4, -1.85, 9.15);
    std::vector<std::vector<int> > connectivityLine;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivityLine.push_back(line);
    }

    lineSurfMesh->setInitialVertexPositions(vertListLine);
    lineSurfMesh->setVertexPositions(vertListLine);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> trianglesLine;
    imstk::SurfaceMesh::TriangleArray triLine[1];
    triLine[0] = { { 0, 1, 2 } };
    trianglesLine.push_back(triLine[0]);
    lineSurfMesh->setTrianglesVertices(trianglesLine);
    auto objectLine = std::make_shared<imstk::VisualObject>("visualLineMesh");
    auto materialLine = std::make_shared<RenderMaterial>();
    materialLine->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpLine = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialLine->addTexture(tmpLine);
    lineSurfMesh->setRenderMaterial(materialLine);
    objectLine->setVisualGeometry(lineSurfMesh); // change to any mesh created above
    scene->addSceneObject(objectLine);
    // END

    // using for force normal
    auto tempSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertList2;
    vertList2.resize(3);
    vertList2[0] = imstk::Vec3d(-16.4, -1.85, 9.15);
    vertList2[1] = imstk::Vec3d(-9.3, -1.4, 9.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertList2[2] = imstk::Vec3d(-16.4, -1.85, 9.15);
    std::vector<std::vector<int> > connectivity2;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity2.push_back(line);
    }

    tempSurfMesh->setInitialVertexPositions(vertList2);
    tempSurfMesh->setVertexPositions(vertList2);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> triangles;
    imstk::SurfaceMesh::TriangleArray tri[1];
    tri[0] = { { 0, 1, 2 } };
    triangles.push_back(tri[0]);
    tempSurfMesh->setTrianglesVertices(triangles);
    auto objectTmp = std::make_shared<imstk::VisualObject>("visualNormal0");
    auto materialTmp = std::make_shared<RenderMaterial>();
    materialTmp->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpTT = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialTmp->addTexture(tmpTT);
    tempSurfMesh->setRenderMaterial(materialTmp);
    objectTmp->setVisualGeometry(tempSurfMesh); // change to any mesh created above
    scene->addSceneObject(objectTmp);
    // END

    // XZH using for the central line of colon
    auto centrallineSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertList3;
    vertList3.resize(3);
    vertList3[0] = imstk::Vec3d(-19.4, -5.85, 1.15);
    vertList3[1] = imstk::Vec3d(-3.3, 2.4, 1.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertList3[2] = imstk::Vec3d(-19.4, -5.85, 1.15);
    std::vector<std::vector<int> > connectivity3;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity3.push_back(line);
    }

    centrallineSurfMesh->setInitialVertexPositions(vertList3);
    centrallineSurfMesh->setVertexPositions(vertList3);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> triangles1;
    imstk::SurfaceMesh::TriangleArray tri1[1];
    tri1[0] = { { 0, 1, 2 } };
    triangles1.push_back(tri1[0]);
    centrallineSurfMesh->setTrianglesVertices(triangles1);
    auto objectCentral = std::make_shared<imstk::VisualObject>("visualNormal1");
    auto materialCentral = std::make_shared<RenderMaterial>();
    materialCentral->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpCentral = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialCentral->addTexture(tmpCentral);
    centrallineSurfMesh->setRenderMaterial(materialCentral);
    objectCentral->setVisualGeometry(centrallineSurfMesh); // change to any mesh created above
    scene->addSceneObject(objectCentral);
    // XZH END

    auto visualMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(readMesh);
    auto physicsMesh = lineMesh; //visualMesh; //  lineMesh; //  imstk::MeshIO::read(path2obj_c);  //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
    auto collidingMesh = lineMesh; // visualMesh; //  lineMesh;
    Vec3d min1, max1;
    visualMesh->computeBoundingBox(min1, max1);

    auto material0 = std::make_shared<RenderMaterial>();
    auto diffuseTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto normalTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Normal.png", Texture::Type::NORMAL);
    auto roughnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Roughness.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material0->addTexture(diffuseTexture0);
    material0->addTexture(normalTexture0);
    material0->addTexture(roughnessTexture0);
    //material->setTexture(metalnessTexture);
    visualMesh->setRenderMaterial(material0);

    FILE *outpointtri;
    outpointtri = fopen("G:/AABB.txt", "w");
    fprintf(outpointtri, "V: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    collidingMesh->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "C: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    fclose(outpointtri);

    auto object = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject");

    auto objMapP2V = std::make_shared<imstk::OneToOneMap>();  // std::make_shared<imstk::TriangleLineMap>(); //  std::make_shared<imstk::OneToOneMap>();
    objMapP2V->setMaster(physicsMesh);
    objMapP2V->setSlave(visualMesh);
    objMapP2V->compute();


    auto objMapP2C = std::make_shared<imstk::OneToOneMap>();  //  colliding is physical mesh
    objMapP2C->setMaster(physicsMesh);
    objMapP2C->setSlave(collidingMesh);
    objMapP2C->compute();

    auto objMapC2V = std::make_shared<imstk::OneToOneMap>(); //  std::make_shared<imstk::TriangleLineMap>(); //  s std::make_shared<imstk::OneToOneMap>();
    objMapC2V->setMaster(collidingMesh);
    objMapC2V->setSlave(visualMesh);
    objMapC2V->compute();

    auto objMapC2P = std::make_shared<imstk::OneToOneMap>(); // colliding is physical mesh
    objMapC2P->setMaster(collidingMesh);
    objMapC2P->setSlave(physicsMesh);
    objMapC2P->compute();

    object->setVisualGeometry(visualMesh);
    object->setCollidingGeometry(collidingMesh);
    object->setPhysicsGeometry(physicsMesh);
    object->setPhysicsToCollidingMap(objMapP2C);
    object->setPhysicsToVisualMap(objMapP2V);
    object->setCollidingToVisualMap(objMapC2V);
    object->setColldingToPhysicsMap(objMapC2P);

    auto pbdModel = std::make_shared<PbdModel>();
    pbdModel->setModelGeometry(physicsMesh);
    pbdModel->configure(/*Number of Constraints*/1,
        /*Constraint configuration*/"Distance 0.1",
        /*Mass*/0.0,
        /*Gravity*/"0 0 0",
        /*TimeStep*/0.0003,
        /*FixedPoint*/"",
        /*NumberOfIterationInConstraintSolver*/2, // 5
        /*Proximity*/0.3,  // use linemesh 0.3  // 0.01 use  //  cube 0.1
        /*Contact stiffness*/0.001);
    object->setDynamicalModel(pbdModel);


    //auto pbdSolver = std::make_shared<PbdSolver>();
    //pbdSolver->setPbdObject(object);
    //scene->addNonlinearSolver(pbdSolver);

    scene->addSceneObject(object);

    auto trackCtrl = std::make_shared<imstk::DeviceTracker>(client);
    trackCtrl->setTranslationScaling(0.25);  // 0.1  test 0.1 should use  Scale 0.001 will be better 
    auto controller = std::make_shared<imstk::PBDSceneObjectController>(object, trackCtrl);
    scene->addObjectController(controller);

    // Sphere1
    auto sphere1Obj = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "Sphere1", 0.5, Vec3d(-16.4, -1.85, 9.15)); // -2.23
    sphere1Obj->getVisualGeometry()->setTranslation(Vec3d(-16.4, -1.85, 9.15));
    //auto materialTT = std::make_shared<Material>();
    //materialTT->setTexture(diffuseTexture0);
    //auto suMesh = std::dynamic_pointer_cast<SurfaceMesh>(sphere1Obj->getVisualGeometry());
    //suMesh->addMaterial(materialTT);
    //auto sphere2Obj = apiutils::createCollidingAnalyticalSceneObject(
    //    imstk::Geometry::Type::Sphere, scene, "Sphere2", 0.5, Vec3d(-9.3, -1.4, 9.05)); //-9.3, -1.4, 9.05    new -9.53, - 1.50,    8.6  org  -7.2, -1.35, 8.62
    //sphere2Obj->getVisualGeometry()->setTranslation(Vec3d(-9.3, -1.4, 9.05));
    //sphere2Obj->setCollidingGeometry(sphere2Obj->getVisualGeometry());

    auto visualSphere22 = std::make_shared<imstk::Sphere>();
    visualSphere22->setTranslation(Vec3d(-9.3, -1.4, 9.05));
    visualSphere22->setRadius(0.5);
    Vec3d mm2 = visualSphere22->getPosition();
    auto sphere2Obj = std::make_shared<CollidingObject>("Sphere2");
    sphere2Obj->setCollidingGeometry(visualSphere22);
    sphere2Obj->setVisualGeometry(visualSphere22);
    scene->addSceneObject(sphere2Obj);

    auto visualSphere2 = std::make_shared<imstk::Sphere>();
    visualSphere2->setTranslation(Vec3d(-9.3, -1.4, 9.05));
    visualSphere2->setRadius(0.7);
    Vec3d mmm=visualSphere2->getPosition();
    auto sphere3Obj = std::make_shared<CollidingObject>("Sphere3");
    sphere3Obj->setCollidingGeometry(visualSphere2);
    sphere3Obj->setVisualGeometry(visualSphere2);
    scene->addSceneObject(sphere3Obj);


    auto sphereOrg = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereOrg", 0.2, Vec3d(0, 0, 0));
    sphereOrg->getVisualGeometry()->setTranslation(Vec3d(0, 0, 0));

    auto sphereX = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereX", 1.5, Vec3d(10, 0, 0));
    sphereX->getVisualGeometry()->setTranslation(Vec3d(10, 0, 0));

    auto sphereY = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereY", 1.2, Vec3d(0, 10, 0));
    sphereY->getVisualGeometry()->setTranslation(Vec3d(0, 10, 0));

    auto sphereZ = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereZ", 0.8, Vec3d(0, 0, 10));
    sphereZ->getVisualGeometry()->setTranslation(Vec3d(0, 0, 10));
    //scene->addSceneObject(sphere1Obj);
    //scene->addSceneObject(sphere2Obj);

    //auto trackCtrl1 = std::make_shared<imstk::DeviceTracker>(client);
    //trackCtrl1->setTranslationScaling(0.1);
    //auto controller1 = std::make_shared<imstk::SceneObjectController>(sphere1Obj, trackCtrl1);
    //scene->addObjectController(controller1);
    //auto controller2 = std::make_shared<imstk::SceneObjectController>(sphere2Obj, trackCtrl1);
    //scene->addObjectController(controller2);

    // Light (white)
    auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
    whiteLight->setPosition(imstk::Vec3d(-15, -1, 0));
    whiteLight->setFocalPoint(imstk::Vec3d(1, -1, 0));
    scene->addLight(whiteLight);
    auto whiteLight2 = std::make_shared<imstk::PointLight>("whiteLight2");
    whiteLight2->setPosition(imstk::Vec3d(1, -1, 0));
    whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    //scene->addLight(whiteLight2);

    // Light (red)
    auto colorLight = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight->setPosition(imstk::Vec3d(-25, -8, -4.5));
    colorLight->setFocalPoint(imstk::Vec3d(-3.7, 5.5, 7));
    colorLight->setColor(imstk::Color::White); // red
    colorLight->setSpotAngle(15);
    auto colorLight2 = std::make_shared<imstk::SpotLight>("colorLight2");
    colorLight2->setPosition(imstk::Vec3d(-25, -8, -4.5));
    colorLight2->setFocalPoint(imstk::Vec3d(-3.7, 5.5, 7));
    colorLight2->setColor(imstk::Color::White);
    colorLight2->setSpotAngle(15);
    auto colorLight3 = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight3->setPosition(imstk::Vec3d(-25, -5, -2.5));  //-25, -5, -2.5
    colorLight3->setFocalPoint(imstk::Vec3d(-3.7, 5.5, 7));
    colorLight3->setColor(imstk::Color::White); // red
    colorLight3->setSpotAngle(15);
    scene->addLight(colorLight);
    //scene->addLight(colorLight2);
    scene->addLight(colorLight3);

    // Update Camera
    auto cam1 = scene->getCamera();
    cam1->setPosition(imstk::Vec3d(-25, -8, -4.5));
    cam1->setFocalPoint(imstk::Vec3d(-3.7, 5.5, 7));
    cam1->setViewAngle(75);

    //// Tetrahedral mesh
    imstk::StdVectorOfVec3d vertListTet;
    std::vector<imstk::TetrahedralMesh::TetraArray> tetConnectivity;
    //physXLink linkVolSur;
    std::vector<physXLink> linksVolSur;
    bool flag = loadTetFile(DATA_ROOT_PATH"/VESS/colonrectum_s002_SB.tet", vertListTet, tetConnectivity, linksVolSur); // colon_tumor3_3_SB for test   colon_tumor3_SB  cubeTest2_SB
    auto tetMesh = std::make_shared<imstk::VESSTetrahedralMesh>();
    tetMesh->setInitialVertexPositions(vertListTet);
    tetMesh->setVertexPositions(vertListTet);
    tetMesh->setTetrahedraVertices(tetConnectivity);
    tetMesh->setPhysXLink(linksVolSur);
    tetMesh->setInitialPhysVertex(vertListTet);  // add later 06/04/2017
    std::vector<Vec3d> centralLine;
    centralLine.push_back(Vec3d(-19.4, -5.85, 1.15));
    centralLine.push_back(Vec3d(-3.3, 2.4, 1.05));
    //tetMesh->extractSurfaceMesh(); // both sides of volume mesh
    tetMesh->extractSurfaceMesh(centralLine); // only inside colon of surface of volume mesh


    // colon wall
    std::string pathColon = DATA_ROOT_PATH"/VESS/colonrectum_s002_SB.obj"; // colon_tumor3_SB cubeTest2_SB  colon_s022.dae"; //  colon_s02.dae"; //  colon_s12.dae"; // volume mesh for colliding // key==colonnew_seg4.dae  colonnew_seg0508.dae small segment// good segment  colonnew_seg4.dae   colonnew_seg3.obj colon_move2.DAE"; //  VESS / colonnew_seg3.obj"; // glove.dae"; //  colon_move2.DAE";    // colon_move2.DAE  colon1_part_max.DAE  colon1_part_max.DAE  colon6_max.DAEcloth.DAE  colon_1_c2_max  colon_cylinder2_tri_max
    std::string pathColon2 = DATA_ROOT_PATH"/VESS/colonrectum_s001.obj";  //  colon_s01
    std::string pathColon3 = DATA_ROOT_PATH"/VESS/colonrectum_s003.dae";  // colon_s03

    auto objMesh = imstk::MeshIO::read(pathColon);   // collidingColonMesh; //  imstk::MeshIO::read(pathColon);
    auto physicsColonMesh = tetMesh; //  collidingColonMesh; //  imstk::MeshIO::read(pathColon);

    auto objMesh2 = imstk::MeshIO::read(pathColon2);
    auto objMesh3 = imstk::MeshIO::read(pathColon3);
    auto visualColonMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh); // physicsColonMesh->getSurfaceMesh(); //  
    auto visualColonMesh2 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh2);
    auto visualColonMesh3 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh3);
    auto collidingColonMesh = physicsColonMesh; //  visualColonMesh; //  imstk::MeshIO::read(pathColon);

    //XZH
    FILE *outMesh = fopen("G:/outOBJ.dat", "w");
    //visualColonMesh->getInitialVerticesPositions();
    fprintf(outMesh, "num vertices:%d\n", visualColonMesh->getNumVertices());
    Vec3d tempPos;
    for (int i = 0; i < visualColonMesh->getNumVertices(); ++i){
        tempPos = visualColonMesh->getVertexPosition(i);
        fprintf(outMesh, "%f %f %f\n", tempPos.x(), tempPos.y(), tempPos.z());
    }
    fprintf(outMesh, "\n num tri:%d\n", visualColonMesh->getNumTriangles());
    for (size_t triId = 0; triId < visualColonMesh->getNumTriangles(); ++triId)
    {
        Vec3d center(0, 0, 0);
        auto vert = visualColonMesh->getTrianglesVertices().at(triId); // getTetrahedronVertices(triId);
        fprintf(outMesh, "%d %d %d\n", vert[0], vert[1], vert[2]);
        for (size_t i = 0; i < 3; ++i)
        {
            center = visualColonMesh->getInitialVertexPosition(vert[i]);
            //fprintf(outMesh, "i: %d %f %f %f  ", i, center.x(), center.y(), center.z());
        }
        //fprintf(outMesh, "\n");
    }

    fclose(outMesh);
    // XZH END

    auto material = std::make_shared<RenderMaterial>();
    auto diffuseTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    auto normalTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    auto sssTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_sss.png", Texture::Type::SUBSURFACE_SCATTERING);
    material->addTexture(diffuseTexture);
    material->addTexture(normalTexture);
    material->addTexture(roughnessTexture);
    material->addTexture(sssTexture);
    material->setTessellated(true);
    //material->setTexture(metalnessTexture);
    visualColonMesh->setRenderMaterial(material);

    auto material2 = std::make_shared<RenderMaterial>();
    auto diffuseTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    auto normalTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material2->addTexture(diffuseTexture2);
    material2->addTexture(normalTexture2);
    material2->addTexture(roughnessTexture2);
    visualColonMesh2->setRenderMaterial(material2);

    auto material3 = std::make_shared<RenderMaterial>();
    auto diffuseTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    auto normalTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material3->addTexture(diffuseTexture3);
    material3->addTexture(normalTexture3);
    material3->addTexture(roughnessTexture3);
    visualColonMesh3->setRenderMaterial(material3);

    visualColonMesh->computeBoundingBox(min1, max1);
    outpointtri = fopen("G:/AABB.txt", "a");
    fprintf(outpointtri, "V core Colon: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    physicsColonMesh->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "C&P core Colon: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    visualColonMesh2->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "V1 Colon1: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    visualColonMesh3->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "V3 Colon3: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    // floor

    //imstk::StdVectorOfVec3d vertList;
    double width = 60.0;
    double height = 60.0;
    int nRows = 11;
    int nCols = 11;
    int corner[4] = { 1, nRows, nRows*nCols - nCols + 1, nRows*nCols };
    char intStr[33];
    std::string fixed_corner;
    //for (unsigned int i = 0; i < 4; ++i)
    //{
    //	std::sprintf(intStr, "%d", corner[i]);
    //	fixed_corner += std::string(intStr) + ' ';
    //}

    //fixed point
    Vec3d tmpPos;
    std::vector<int> fixedVertice; // add later 06/04/2017
    for (int i = 0; i < physicsColonMesh->getNumVertices(); ++i) // physicsColonMesh  visualColonMesh
    {
        tmpPos = physicsColonMesh->getVertexPosition(i);  // physicsColonMesh  visualColonMesh
        if ((tmpPos.x() < -17) || (tmpPos.x() > -5.5))  //  || (tmpPos.z() > 3.0   <-1.3  >1.6  smoothed colon segment  (tmpPos.x() < -1.5) || (tmpPos.x() > 1.6)
        {
            std::sprintf(intStr, "%d", i + 1);
            fixed_corner += std::string(intStr) + ' ';
            fixedVertice.push_back(i); // add later 06/04/2017
        }
    }

    fprintf(outpointtri, "V_Colon: Fixed point %s \n ", fixed_corner.c_str());

    // set fixed in the tetmesh add later 06/04/2017
    tetMesh->setFixedVertice(fixedVertice);
    tetMesh->updateFixedVertice();
    tetMesh->initDeformMesh(); // init for deform mesh : injection solution 06/20/2017

    // Plane
    auto geom1 = std::make_shared<imstk::Plane>();
    geom1->setPosition(Vec3d(-17, 0, 0));
    //geom1->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    geom1->rotate(imstk::FORWARD_VECTOR, imstk::PI_2);
    geom1->setWidth(30.0);
    geom1->print();
    Vec3d temp = geom1->getNormal();
    fprintf(outpointtri, "plane 1\n norm: %f %f %f\n", temp.x(), temp.y(), temp.z());
    temp = geom1->getPosition();
    fprintf(outpointtri, "position: %f %f %f\n", temp.x(), temp.y(), temp.z());
    auto planeObj1 = std::make_shared<imstk::VisualObject>("VisualPlane1");
    planeObj1->setVisualGeometry(geom1);
    // Plane
    auto geom2 = std::make_shared<imstk::Plane>();
    geom2->setPosition(Vec3d(-5.5, 0, 0));
    //geom2->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    geom2->rotate(imstk::FORWARD_VECTOR, imstk::PI_2);
    geom2->setWidth(30.0);
    geom2->print();
    temp = geom2->getNormal();
    fprintf(outpointtri, "plane 2\n norm: %f %f %f\n", temp.x(), temp.y(), temp.z());
    temp = geom2->getPosition();
    fprintf(outpointtri, "position: %f %f %f\n", temp.x(), temp.y(), temp.z());
    auto planeObj2 = std::make_shared<imstk::VisualObject>("VisualPlane2");
    planeObj2->setVisualGeometry(geom2);
    //scene->addSceneObject(planeObj1);
    //scene->addSceneObject(planeObj2);

    fclose(outpointtri);

    // Construct a map
    auto colonMapP2V = std::make_shared<imstk::VESSTetraTriangleMap>();  // <imstk::OneToOneMap>();
    colonMapP2V->setMaster(physicsColonMesh);
    colonMapP2V->setSlave(visualColonMesh);
    colonMapP2V->compute();


    auto colonMapP2C = std::make_shared<imstk::OneToOneMap>();
    colonMapP2C->setMaster(physicsColonMesh);
    colonMapP2C->setSlave(collidingColonMesh);
    colonMapP2C->compute();

    auto colonMapC2V = std::make_shared<imstk::VESSTetraTriangleMap>();  //<imstk::OneToOneMap>();
    colonMapC2V->setMaster(collidingColonMesh);
    colonMapC2V->setSlave(visualColonMesh);
    colonMapC2V->compute();

    auto colonObj = std::make_shared<PbdObject>("Colon");
    colonObj->setCollidingGeometry(collidingColonMesh);
    colonObj->setVisualGeometry(visualColonMesh);
    colonObj->setPhysicsGeometry(physicsColonMesh);
    colonObj->setPhysicsToCollidingMap(colonMapP2C);
    colonObj->setPhysicsToVisualMap(colonMapP2V);
    colonObj->setCollidingToVisualMap(colonMapC2V);

    // Create object and add to scene  just for scene, no colliding  for the two sides part, middle for operation part
    auto object2 = std::make_shared<imstk::VisualObject>("visualColonMesh2");
    object2->setVisualGeometry(visualColonMesh2); // change to any mesh created above
    //scene->addSceneObject(object2);
    auto object3 = std::make_shared<imstk::VisualObject>("visualColonMesh3");
    object3->setVisualGeometry(visualColonMesh3); // change to any mesh created above
    //scene->addSceneObject(object3);

    // wire frame for surface of volume mesh
    // START
    auto object4 = std::make_shared<imstk::VisualObject>("visualColonMesh4");
    auto material4 = std::make_shared<RenderMaterial>();
    material4->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmp4 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto surfM = tetMesh->getSurfaceMesh();
    material4->addTexture(tmp4);
    surfM->setRenderMaterial(material4);
    object4->setVisualGeometry(surfM); // change to any mesh created above
    scene->addSceneObject(object4);

    //FILE *outNormal = fopen("G:/surfacenorm.dat","w");
    //fprintf(outNormal, "num:%d\n", surfM->getNumTriangles());
    std::vector<SurfaceMesh::TriangleArray> elements = surfM->getTrianglesVertices();
    for (int i = 0; i < surfM->getNumTriangles()-1000; i++)
    //if (0)
    {
        //int i = 0;
        Vec3d tmp = surfM->getTriangleNormal(i);
        SurfaceMesh::TriangleArray& e = elements[i];
        const Vec3d v0 = surfM->getVertexPosition(e[0]);
        const Vec3d v1 = surfM->getVertexPosition(e[1]);
        const Vec3d v2 = surfM->getVertexPosition(e[2]);

        Vec3d cenPos = (v0 + v1 + v2) / 3;
        // XZH
        Vec3d p0 = Vec3d(-19.4, -5.85, 1.15);
        Vec3d p1 = Vec3d(-3.3, 2.4, 1.05);
        Vec3d a = cenPos - p0;
        Vec3d b = p1 - p0;
        Vec3d c = ((a.dot(b)) / (b.dot(b)))*b;
        Vec3d ee = a - c;
        if (tmp.dot(-ee) < 0)  // normal indicating the outside colon
        {
            //continue;
            if ((tmp.dot(-ee))+tmp.norm()*ee.norm()<0.1)
            {
                printf("cenPos: %f %f %f, -ee: %f %f %f\n", cenPos.x(), cenPos.y(), cenPos.z(), -ee.x(), -ee.y(), -ee.z());
            }
        }
        else
        {
            continue;
            //fprintf(outNormal, "%d\n", i);
        }
        continue;
        // XZH END

        // XZH using for the central line of colon
        auto surfTTT = std::make_shared<imstk::SurfaceMesh>();
        imstk::StdVectorOfVec3d vertListTTT;
        vertListTTT.resize(3);
        vertListTTT[0] = cenPos;  
        tmp.normalize();
        vertListTTT[1] = cenPos + tmp;
        vertListTTT[2] = cenPos;
        //fprintf(outNormal, "Pos: %f %f %f 2: %f %f %f\n", cenPos.x(), cenPos.y(), cenPos.z(), cenPos.x() + tmp.x() * 10, cenPos.y() + tmp.y() * 10, cenPos.z() + tmp.z() * 10);

        surfTTT->setInitialVertexPositions(vertListTTT);
        surfTTT->setVertexPositions(vertListTTT);
        // c. Add connectivity data
        std::vector<imstk::SurfaceMesh::TriangleArray> trianglesTTT;
        imstk::SurfaceMesh::TriangleArray triTTT[1];
        triTTT[0] = { { 0, 1, 2 } };
        trianglesTTT.push_back(triTTT[0]);
        surfTTT->setTrianglesVertices(trianglesTTT);
        char str[25];
        sprintf(str, "surfaceNormal %d", i);
        auto objectTTT = std::make_shared<imstk::VisualObject>(str);
        auto materialTTT = std::make_shared<RenderMaterial>();
        materialTTT->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
        auto tmpTTT = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
        materialTTT->addTexture(tmpTTT);
        surfTTT->setRenderMaterial(materialTTT);
        objectTTT->setVisualGeometry(surfTTT); // change to any mesh created above
         
        scene->addSceneObject(objectTTT);
        continue;

        auto surfTTT1 = std::make_shared<imstk::SurfaceMesh>();
        imstk::StdVectorOfVec3d vertListTTT1;
        vertListTTT1.resize(3);
        vertListTTT1[0] = cenPos;
        Vec3d tmpTTT1 = -ee;
        //tmpTTT1.normalize();
        vertListTTT1[1] = cenPos + tmpTTT1;
        vertListTTT1[2] = cenPos;
        //fprintf(outNormal, "Pos: %f %f %f 2: %f %f %f\n", cenPos.x(), cenPos.y(), cenPos.z(), cenPos.x() + tmp.x() * 10, cenPos.y() + tmp.y() * 10, cenPos.z() + tmp.z() * 10);

        surfTTT1->setInitialVertexPositions(vertListTTT1);
        surfTTT1->setVertexPositions(vertListTTT1);
        // c. Add connectivity data
        std::vector<imstk::SurfaceMesh::TriangleArray> trianglesTTT1;
        imstk::SurfaceMesh::TriangleArray triTTT1[1];
        triTTT1[0] = { { 0, 1, 2 } };
        trianglesTTT1.push_back(triTTT1[0]);
        surfTTT1->setTrianglesVertices(trianglesTTT1);
        sprintf(str, "surfaceLine %d", i);
        auto objectTTT1 = std::make_shared<imstk::VisualObject>(str);
        auto materialTTT1 = std::make_shared<RenderMaterial>();
        materialTTT1->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
        //auto tmpTTTT = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
        //materialTTT1->addTexture(tmpTTTT);
        surfTTT1->setRenderMaterial(materialTTT1);
        objectTTT1->setVisualGeometry(surfTTT1); // change to any mesh created above

        
        scene->addSceneObject(objectTTT1);
        // XZH END
    }
    //fclose(outNormal);
    FILE *planeInfo = fopen("G:/planenorm.dat", "w");
    // rotate matrix from 2 vectors
    Vec3d nn0 = Vec3d(0, 1, 0);
    Vec3d nn1 = Vec3d(0, 0, 1);   // (-0.95, 1.84, -0.42);
    nn0.normalize();
    nn1.normalize();
    double dot = nn0.dot(nn1);
    if (dot < -1.0) dot = -1.0;
    if (dot > 1.0) dot = 1.0;
    double t = acos(dot); //  acos(nn0.dot(nn1));
    Vec3d nn2 = nn0.cross(nn1);
    nn2.normalize();
    //use angle&angle to make rotate matrix   http://blog.atelier39.org/cg/463.html
    double u = nn2.x();
    double v = nn2.y();
    double w = nn2.z();
    Mat3d rotTmp;
    rotTmp(0, 0) = u*u + (v*v + w*w)*cos(t);  rotTmp(0, 1) = u*v*(1 - cos(t)) - w*sin(t); rotTmp(0, 2) = u*w*(1 - cos(t)) + v*sin(t);
    rotTmp(1, 0) = u*v*(1 - cos(t)) + w*sin(t); rotTmp(1, 1) = v*v + (u*u + w*w)*cos(t); rotTmp(1, 2) = v*w*(1 - cos(t)) - u*sin(t);
    rotTmp(2, 0) = u*w*(1 - cos(t)) - v*sin(t); rotTmp(2, 1) = v*w*(1 - cos(t)) + u*sin(t); rotTmp(2, 2) = w*w + (u*u + v*v)*cos(t);

    // Plane default normal is (0,1,0) position (0,0,0)
    auto plane0 = std::make_shared<imstk::Plane>();
    //plane0->setPosition(Vec3d(0, 10, 0));  // -4.68, -0.63, 1.48
    plane0->translate(Vec3d(-4.68, -0.63, -3.9));  // -4.68, -0.63, 1.48
    //geom1->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    //plane0->rotate(imstk::FORWARD_VECTOR, imstk::PI_2);
    plane0->rotate(rotTmp);
    plane0->setWidth(30.0);
    plane0->print();
    Vec3d ttmp0 = plane0->getNormal();
    fprintf(planeInfo, "plane 1\n norm: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    ttmp0 = plane0->getPosition();
    fprintf(planeInfo, "position: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    auto planeOmni0 = std::make_shared<imstk::CollidingObject>("VisualPlane Omni0");
    planeOmni0->setVisualGeometry(plane0);
    planeOmni0->setCollidingGeometry(plane0);
    //scene->addSceneObject(planeOmni0);

    // Plane default normal is (0,1,0) position (0,0,0)
    auto plane1 = std::make_shared<imstk::Plane>();
    //plane0->setPosition(Vec3d(0, 10, 0));  // -4.68, -0.63, 1.48
    plane1->translate(Vec3d(-4.68, -0.63, 6.8));  // -4.68, -0.63, 1.48
    //geom1->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    //plane0->rotate(imstk::FORWARD_VECTOR, imstk::PI_2);
    plane1->rotate(rotTmp);
    plane1->setWidth(30.0);
    plane1->print();
    ttmp0 = plane1->getNormal();
    fprintf(planeInfo, "plane 2\n norm: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    ttmp0 = plane1->getPosition();
    fprintf(planeInfo, "position: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    auto planeOmni1 = std::make_shared<imstk::CollidingObject>("VisualPlane Omni1");
    planeOmni1->setVisualGeometry(plane1);
    planeOmni1->setCollidingGeometry(plane1);
    //scene->addSceneObject(planeOmni1);
    fclose(planeInfo);
    // END wire frame

    auto pbdColonModel = std::make_shared<PbdModel>();
    pbdColonModel->setModelGeometry(physicsColonMesh);
    pbdColonModel->configure(/*Number of Constraints*/2,
        /*Constraint configuration*/"Volume 0.7",  // 0.9 "Dihedral 0.8", // 0.01 0.8 effect is couhe not very well
        /*Constraint configuration*/"Distance 0.9",  // 0.8 effect is couhe
        /*Mass*/0.1, // 0.1
        /*Gravity*/"0 0 0",  // "0 -9.8 0",
        /*TimeStep*/0.001, // 0.001
        /*FixedPoint*/ fixed_corner.c_str(),
        /*NumberOfIterationInConstraintSolver*/3, //   2 12
        /*Proximity*/0.01,  // 0.1
        /*Contact stiffness*/0.1);
    colonObj->setDynamicalModel(pbdColonModel);

    auto pbdColonSolver = std::make_shared<PbdSolver>();
    pbdColonSolver->setPbdObject(colonObj);
    scene->addNonlinearSolver(pbdColonSolver);

    scene->addSceneObject(colonObj);

    // Collisions
    auto colGraph = scene->getCollisionGraph();
    auto pair = std::make_shared<PbdInteractionPair>(PbdInteractionPair(object, colonObj));
    pair->setNumberOfInterations(3); // 2

    colGraph->addInteractionPair(pair);

    // Sphere-plane
    //auto trackCtrl2 = std::make_shared<imstk::DeviceTracker>(client);
    //trackCtrl2->setTranslationScaling(0.25);  // 0.1  test 0.1 should use  Scale 0.001 will be better 
    ////Create and add virtual coupling object controller in the scene
    //auto objController = std::make_shared<imstk::SceneObjectController>(sphere3Obj, trackCtrl2);
    //scene->addObjectController(objController);
    //colGraph->addInteractionPair(planeOmni0, object,
    //    CollisionDetection::Type::UnidirectionalPlaneToSphere,
    //    CollisionHandling::Type::None,
    //    CollisionHandling::Type::Penalty);

    // Run
    sdk->setActiveScene(scene);
    sdk->startSimulation(true);
#endif
}

void testPbdVirtualCoupling16_2()
{
#ifdef iMSTK_USE_OPENHAPTICS
    // SDK and Scene
    auto sdk = std::make_shared<imstk::SimulationManager>();
    auto scene = sdk->createNewScene("SceneTestDevice");

    // Device Client
    auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

    // Device Server
    auto server = std::make_shared<imstk::HDAPIDeviceServer>();
    server->addDeviceClient(client);
    sdk->addModule(server);

    std::string path2obj = DATA_ROOT_PATH"/VESS/ITKnife7_4.obj"; // knife correct ITKnife6 // cube knife knife3.obj";  // //  113.obj"; //  /  blade2.obj  cube.obj  kcube.obj  test cloth using this 
    std::string path2obj_c = DATA_ROOT_PATH"/VESS/ITKnife7_4.obj"; // knife correct ITKnife6  // cube knife knife3.obj";  //  //  113.obj";  // / VESS / TriangleTipKnife2.obj";  // blade2.obj  cube.obj kcube_s2.obj
    auto readMesh = imstk::MeshIO::read(path2obj_c); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);

    // Line mesh
    auto lineMesh = std::make_shared<imstk::LineMesh>();
    imstk::StdVectorOfVec3d vertList;
    vertList.resize(2);  // using 5 segment
    //vertList[0] = imstk::Vec3d(-13, -2.25, 9.15); // should - volume mesh therold than surface mesh (9.0, -1.9, -0.6) AABB 0: 9.0, -2.23, -0.6   -0.7 ITKnife6.DAE  // knife3.obj 9.5, -0.9, 1.18
    //vertList[1] = imstk::Vec3d(-5.93, -1.825, 9.125); // 9.7 ITKnife6.DAE // knife3.obj 9.5, -0.9, 8.73
    //vertList[2] = imstk::Vec3d(1.14, -1.4, 9.1); // (9.0, -2.55, 9.5)  AABB 1: 9.0, -2.23, 9.6  9.7 ITKnife6.DAE // knife3.obj 9.5, -0.9, 8.73
    vertList[0] = imstk::Vec3d(-16.4, -1.85, 9.15);
    vertList[1] = imstk::Vec3d(-9.3, -1.4, 9.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    std::vector<std::vector<int> > connectivity;
    for (int i = 0; i < 1;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity.push_back(line);
    }

    lineMesh->setInitialVertexPositions(vertList);
    lineMesh->setVertexPositions(vertList);
    lineMesh->setConnectivity(connectivity);

    // using for line mesh display
    auto lineSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertListLine;
    vertListLine.resize(3);
    vertListLine[0] = imstk::Vec3d(-16.4, -1.85, 9.15);
    vertListLine[1] = imstk::Vec3d(-9.3, -1.4, 9.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertListLine[2] = imstk::Vec3d(-16.4, -1.85, 9.15);
    std::vector<std::vector<int> > connectivityLine;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivityLine.push_back(line);
    }

    lineSurfMesh->setInitialVertexPositions(vertListLine);
    lineSurfMesh->setVertexPositions(vertListLine);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> trianglesLine;
    imstk::SurfaceMesh::TriangleArray triLine[1];
    triLine[0] = { { 0, 1, 2 } };
    trianglesLine.push_back(triLine[0]);
    lineSurfMesh->setTrianglesVertices(trianglesLine);
    auto objectLine = std::make_shared<imstk::VisualObject>("visualLineMesh");
    auto materialLine = std::make_shared<RenderMaterial>();
    materialLine->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpLine = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialLine->addTexture(tmpLine);
    lineSurfMesh->setRenderMaterial(materialLine);
    objectLine->setVisualGeometry(lineSurfMesh); // change to any mesh created above
    scene->addSceneObject(objectLine);
    // END

    // using for force normal
    auto tempSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertList2;
    vertList2.resize(3);
    vertList2[0] = imstk::Vec3d(-16.4, -1.85, 9.15);
    vertList2[1] = imstk::Vec3d(-9.3, -1.4, 9.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertList2[2] = imstk::Vec3d(-16.4, -1.85, 9.15);
    std::vector<std::vector<int> > connectivity2;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity2.push_back(line);
    }

    tempSurfMesh->setInitialVertexPositions(vertList2);
    tempSurfMesh->setVertexPositions(vertList2);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> triangles;
    imstk::SurfaceMesh::TriangleArray tri[1];
    tri[0] = { { 0, 1, 2 } };
    triangles.push_back(tri[0]);
    tempSurfMesh->setTrianglesVertices(triangles);
    auto objectTmp = std::make_shared<imstk::VisualObject>("visualNormal0");
    auto materialTmp = std::make_shared<RenderMaterial>();
    materialTmp->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpTT = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialTmp->addTexture(tmpTT);
    tempSurfMesh->setRenderMaterial(materialTmp);
    objectTmp->setVisualGeometry(tempSurfMesh); // change to any mesh created above
    scene->addSceneObject(objectTmp);
    // END

    // XZH using for the central line of colon
    auto centrallineSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertList3;
    vertList3.resize(3);
    vertList3[0] = imstk::Vec3d(-19.4, -5.85, 1.15);
    vertList3[1] = imstk::Vec3d(-3.3, 2.4, 1.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertList3[2] = imstk::Vec3d(-19.4, -5.85, 1.15);
    std::vector<std::vector<int> > connectivity3;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity3.push_back(line);
    }

    centrallineSurfMesh->setInitialVertexPositions(vertList3);
    centrallineSurfMesh->setVertexPositions(vertList3);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> triangles1;
    imstk::SurfaceMesh::TriangleArray tri1[1];
    tri1[0] = { { 0, 1, 2 } };
    triangles1.push_back(tri1[0]);
    centrallineSurfMesh->setTrianglesVertices(triangles1);
    auto objectCentral = std::make_shared<imstk::VisualObject>("visualNormal1");
    auto materialCentral = std::make_shared<RenderMaterial>();
    materialCentral->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpCentral = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialCentral->addTexture(tmpCentral);
    centrallineSurfMesh->setRenderMaterial(materialCentral);
    objectCentral->setVisualGeometry(centrallineSurfMesh); // change to any mesh created above
    scene->addSceneObject(objectCentral);
    // XZH END

    auto visualMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(readMesh);
    auto physicsMesh = lineMesh; //visualMesh; //  lineMesh; //  imstk::MeshIO::read(path2obj_c);  //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
    auto collidingMesh = lineMesh; // visualMesh; //  lineMesh;
    Vec3d min1, max1;
    visualMesh->computeBoundingBox(min1, max1);

    auto material0 = std::make_shared<RenderMaterial>();
    auto diffuseTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto normalTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Normal.png", Texture::Type::NORMAL);
    auto roughnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Roughness.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material0->addTexture(diffuseTexture0);
    material0->addTexture(normalTexture0);
    material0->addTexture(roughnessTexture0);
    //material->setTexture(metalnessTexture);
    visualMesh->setRenderMaterial(material0);

    FILE *outpointtri;
    outpointtri = fopen("G:/AABB.txt", "w");
    fprintf(outpointtri, "V: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    collidingMesh->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "C: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    fclose(outpointtri);

    auto object = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject");

    auto objMapP2V = std::make_shared<imstk::OneToOneMap>();  // std::make_shared<imstk::TriangleLineMap>(); //  std::make_shared<imstk::OneToOneMap>();
    objMapP2V->setMaster(physicsMesh);
    objMapP2V->setSlave(visualMesh);
    objMapP2V->compute();


    auto objMapP2C = std::make_shared<imstk::OneToOneMap>();  //  colliding is physical mesh
    objMapP2C->setMaster(physicsMesh);
    objMapP2C->setSlave(collidingMesh);
    objMapP2C->compute();

    auto objMapC2V = std::make_shared<imstk::OneToOneMap>(); //  std::make_shared<imstk::TriangleLineMap>(); //  s std::make_shared<imstk::OneToOneMap>();
    objMapC2V->setMaster(collidingMesh);
    objMapC2V->setSlave(visualMesh);
    objMapC2V->compute();

    auto objMapC2P = std::make_shared<imstk::OneToOneMap>(); // colliding is physical mesh
    objMapC2P->setMaster(collidingMesh);
    objMapC2P->setSlave(physicsMesh);
    objMapC2P->compute();

    object->setVisualGeometry(visualMesh);
    object->setCollidingGeometry(collidingMesh);
    object->setPhysicsGeometry(physicsMesh);
    object->setPhysicsToCollidingMap(objMapP2C);
    object->setPhysicsToVisualMap(objMapP2V);
    object->setCollidingToVisualMap(objMapC2V);
    object->setColldingToPhysicsMap(objMapC2P);

    auto pbdModel = std::make_shared<PbdModel>();
    pbdModel->configure(/*Number of Constraints*/1,
        /*Constraint configuration*/"Distance 0.1",
        /*Mass*/0.0,
        /*Gravity*/"0 0 0",
        /*TimeStep*/0.0003,
        /*FixedPoint*/"",
        /*NumberOfIterationInConstraintSolver*/2, // 5
        /*Proximity*/0.3,  // use linemesh 0.3  // 0.01 use  //  cube 0.1
        /*Contact stiffness*/0.001);
    object->setDynamicalModel(pbdModel);

    //auto pbdSolver = std::make_shared<PbdSolver>();
    //pbdSolver->setPbdObject(object);
    //scene->addNonlinearSolver(pbdSolver);

    scene->addSceneObject(object);

    auto trackCtrl = std::make_shared<imstk::DeviceTracker>(client);
    trackCtrl->setTranslationScaling(0.25);  // 0.1  test 0.1 should use  Scale 0.001 will be better 
    auto controller = std::make_shared<imstk::PBDSceneObjectController>(object, trackCtrl);
    scene->addObjectController(controller);

    // Sphere1
    auto sphere1Obj = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "Sphere1", 0.5, Vec3d(-16.4, -1.85, 9.15)); // -2.23
    sphere1Obj->getVisualGeometry()->setTranslation(Vec3d(-16.4, -1.85, 9.15));
    //auto materialTT = std::make_shared<Material>();
    //materialTT->setTexture(diffuseTexture0);
    //auto suMesh = std::dynamic_pointer_cast<SurfaceMesh>(sphere1Obj->getVisualGeometry());
    //suMesh->addMaterial(materialTT);
    //auto sphere2Obj = apiutils::createCollidingAnalyticalSceneObject(
    //    imstk::Geometry::Type::Sphere, scene, "Sphere2", 0.5, Vec3d(-9.3, -1.4, 9.05)); //-9.3, -1.4, 9.05    new -9.53, - 1.50,    8.6  org  -7.2, -1.35, 8.62
    //sphere2Obj->getVisualGeometry()->setTranslation(Vec3d(-9.3, -1.4, 9.05));
    //sphere2Obj->setCollidingGeometry(sphere2Obj->getVisualGeometry());

    auto visualSphere22 = std::make_shared<imstk::Sphere>();
    visualSphere22->setTranslation(Vec3d(-9.3, -1.4, 9.05));
    visualSphere22->setRadius(0.5);
    Vec3d mm2 = visualSphere22->getPosition();
    auto sphere2Obj = std::make_shared<CollidingObject>("Sphere2");
    sphere2Obj->setCollidingGeometry(visualSphere22);
    sphere2Obj->setVisualGeometry(visualSphere22);
    scene->addSceneObject(sphere2Obj);

    auto visualSphere2 = std::make_shared<imstk::Sphere>();
    visualSphere2->setTranslation(Vec3d(-9.3, -1.4, 9.05));
    visualSphere2->setRadius(0.7);
    Vec3d mmm = visualSphere2->getPosition();
    auto sphere3Obj = std::make_shared<CollidingObject>("Sphere3");
    sphere3Obj->setCollidingGeometry(visualSphere2);
    sphere3Obj->setVisualGeometry(visualSphere2);
    scene->addSceneObject(sphere3Obj);


    auto sphereOrg = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereOrg", 0.2, Vec3d(0, 0, 0));
    sphereOrg->getVisualGeometry()->setTranslation(Vec3d(0, 0, 0));

    auto sphereX = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereX", 1.5, Vec3d(10, 0, 0));
    sphereX->getVisualGeometry()->setTranslation(Vec3d(10, 0, 0));

    auto sphereY = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereY", 1.2, Vec3d(0, 10, 0));
    sphereY->getVisualGeometry()->setTranslation(Vec3d(0, 10, 0));

    auto sphereZ = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereZ", 0.8, Vec3d(0, 0, 10));
    sphereZ->getVisualGeometry()->setTranslation(Vec3d(0, 0, 10));
    //scene->addSceneObject(sphere1Obj);
    //scene->addSceneObject(sphere2Obj);

    //auto trackCtrl1 = std::make_shared<imstk::DeviceTracker>(client);
    //trackCtrl1->setTranslationScaling(0.1);
    //auto controller1 = std::make_shared<imstk::SceneObjectController>(sphere1Obj, trackCtrl1);
    //scene->addObjectController(controller1);
    //auto controller2 = std::make_shared<imstk::SceneObjectController>(sphere2Obj, trackCtrl1);
    //scene->addObjectController(controller2);

    // Light (white)
    auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
    whiteLight->setPosition(imstk::Vec3d(-15, -1, 0));
    whiteLight->setFocalPoint(imstk::Vec3d(1, -1, 0));
    scene->addLight(whiteLight);
    auto whiteLight2 = std::make_shared<imstk::PointLight>("whiteLight2");
    whiteLight2->setPosition(imstk::Vec3d(1, -1, 0));
    whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    //scene->addLight(whiteLight2);

    // Light (red)
    auto colorLight = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight->setPosition(imstk::Vec3d(-25, -8, -4.5));
    colorLight->setFocalPoint(imstk::Vec3d(-3.7, 5.5, 7));
    colorLight->setColor(imstk::Color::White); // red
    colorLight->setSpotAngle(15);
    auto colorLight2 = std::make_shared<imstk::SpotLight>("colorLight2");
    colorLight2->setPosition(imstk::Vec3d(-25, -8, -4.5));
    colorLight2->setFocalPoint(imstk::Vec3d(-3.7, 5.5, 7));
    colorLight2->setColor(imstk::Color::White);
    colorLight2->setSpotAngle(15);
    auto colorLight3 = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight3->setPosition(imstk::Vec3d(-25, -5, -2.5));  //-25, -5, -2.5
    colorLight3->setFocalPoint(imstk::Vec3d(-3.7, 5.5, 7));
    colorLight3->setColor(imstk::Color::White); // red
    colorLight3->setSpotAngle(15);
    scene->addLight(colorLight);
    //scene->addLight(colorLight2);
    scene->addLight(colorLight3);

    // Update Camera
    auto cam1 = scene->getCamera();
    cam1->setPosition(imstk::Vec3d(-25, -8, -4.5));
    cam1->setFocalPoint(imstk::Vec3d(-3.7, 5.5, 7));
    cam1->setViewAngle(75);

    //// Tetrahedral mesh
    imstk::StdVectorOfVec3d vertListTet;
    std::vector<imstk::TetrahedralMesh::TetraArray> tetConnectivity;
    //physXLink linkVolSur;
    std::vector<physXLink> linksVolSur;
    bool flag = loadTetFile(DATA_ROOT_PATH"/VESS/colonrectum_s002_SB.tet", vertListTet, tetConnectivity, linksVolSur); // colon_tumor3_3_SB for test   colon_tumor3_SB  cubeTest2_SB
    auto tetMesh = std::make_shared<imstk::VESSTetrahedralMesh>();
    tetMesh->setInitialVertexPositions(vertListTet);
    tetMesh->setVertexPositions(vertListTet);
    tetMesh->setTetrahedraVertices(tetConnectivity);
    tetMesh->setPhysXLink(linksVolSur);
    tetMesh->setInitialPhysVertex(vertListTet);  // add later 06/04/2017
    std::vector<Vec3d> centralLine;
    centralLine.push_back(Vec3d(-19.4, -5.85, 1.15));
    centralLine.push_back(Vec3d(-3.3, 2.4, 1.05));
    //tetMesh->extractSurfaceMesh(); // both sides of volume mesh
    tetMesh->extractSurfaceMesh(centralLine); // only inside colon of surface of volume mesh


    // colon wall
    std::string pathColon = DATA_ROOT_PATH"/VESS/colonrectum_s002_SB.obj"; // colon_tumor3_SB cubeTest2_SB  colon_s022.dae"; //  colon_s02.dae"; //  colon_s12.dae"; // volume mesh for colliding // key==colonnew_seg4.dae  colonnew_seg0508.dae small segment// good segment  colonnew_seg4.dae   colonnew_seg3.obj colon_move2.DAE"; //  VESS / colonnew_seg3.obj"; // glove.dae"; //  colon_move2.DAE";    // colon_move2.DAE  colon1_part_max.DAE  colon1_part_max.DAE  colon6_max.DAEcloth.DAE  colon_1_c2_max  colon_cylinder2_tri_max
    std::string pathColon2 = DATA_ROOT_PATH"/VESS/colonrectum_s001.obj";  //  colon_s01
    std::string pathColon3 = DATA_ROOT_PATH"/VESS/colonrectum_s003.dae";  // colon_s03

    auto objMesh = imstk::MeshIO::read(pathColon);   // collidingColonMesh; //  imstk::MeshIO::read(pathColon);
    auto physicsColonMesh = tetMesh; //  collidingColonMesh; //  imstk::MeshIO::read(pathColon);

    auto objMesh2 = imstk::MeshIO::read(pathColon2);
    auto objMesh3 = imstk::MeshIO::read(pathColon3);
    auto visualColonMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh); // physicsColonMesh->getSurfaceMesh(); //  
    auto visualColonMesh2 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh2);
    auto visualColonMesh3 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh3);
    auto collidingColonMesh = physicsColonMesh; //  visualColonMesh; //  imstk::MeshIO::read(pathColon);

    //XZH
    FILE *outMesh = fopen("G:/outOBJ.dat", "w");
    //visualColonMesh->getInitialVerticesPositions();
    fprintf(outMesh, "num vertices:%d\n", visualColonMesh->getNumVertices());
    Vec3d tempPos;
    for (int i = 0; i < visualColonMesh->getNumVertices(); ++i){
        tempPos = visualColonMesh->getVertexPosition(i);
        fprintf(outMesh, "%f %f %f\n", tempPos.x(), tempPos.y(), tempPos.z());
    }
    fprintf(outMesh, "\n num tri:%d\n", visualColonMesh->getNumTriangles());
    for (size_t triId = 0; triId < visualColonMesh->getNumTriangles(); ++triId)
    {
        Vec3d center(0, 0, 0);
        auto vert = visualColonMesh->getTrianglesVertices().at(triId); // getTetrahedronVertices(triId);
        fprintf(outMesh, "%d %d %d\n", vert[0], vert[1], vert[2]);
        for (size_t i = 0; i < 3; ++i)
        {
            center = visualColonMesh->getInitialVertexPosition(vert[i]);
            //fprintf(outMesh, "i: %d %f %f %f  ", i, center.x(), center.y(), center.z());
        }
        //fprintf(outMesh, "\n");
    }

    fclose(outMesh);
    // XZH END

    auto material = std::make_shared<RenderMaterial>();
    auto diffuseTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    auto normalTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    auto sssTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_sss.png", Texture::Type::SUBSURFACE_SCATTERING);
    material->addTexture(diffuseTexture);
    material->addTexture(normalTexture);
    material->addTexture(roughnessTexture);
    material->addTexture(sssTexture);
    material->setTessellated(true);
    //material->setTexture(metalnessTexture);
    visualColonMesh->setRenderMaterial(material);

    auto material2 = std::make_shared<RenderMaterial>();
    auto diffuseTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    auto normalTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material2->addTexture(diffuseTexture2);
    material2->addTexture(normalTexture2);
    material2->addTexture(roughnessTexture2);
    visualColonMesh2->setRenderMaterial(material2);

    auto material3 = std::make_shared<RenderMaterial>();
    auto diffuseTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    auto normalTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material3->addTexture(diffuseTexture3);
    material3->addTexture(normalTexture3);
    material3->addTexture(roughnessTexture3);
    visualColonMesh3->setRenderMaterial(material3);

    visualColonMesh->computeBoundingBox(min1, max1);
    outpointtri = fopen("G:/AABB.txt", "a");
    fprintf(outpointtri, "V core Colon: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    physicsColonMesh->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "C&P core Colon: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    visualColonMesh2->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "V1 Colon1: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    visualColonMesh3->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "V3 Colon3: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    // floor

    //imstk::StdVectorOfVec3d vertList;
    double width = 60.0;
    double height = 60.0;
    int nRows = 11;
    int nCols = 11;
    int corner[4] = { 1, nRows, nRows*nCols - nCols + 1, nRows*nCols };
    char intStr[33];
    std::string fixed_corner;
    //for (unsigned int i = 0; i < 4; ++i)
    //{
    //	std::sprintf(intStr, "%d", corner[i]);
    //	fixed_corner += std::string(intStr) + ' ';
    //}

    //fixed point
    Vec3d tmpPos;
    std::vector<int> fixedVertice; // add later 06/04/2017
    for (int i = 0; i < physicsColonMesh->getNumVertices(); ++i) // physicsColonMesh  visualColonMesh
    {
        tmpPos = physicsColonMesh->getVertexPosition(i);  // physicsColonMesh  visualColonMesh
        if ((tmpPos.x() < -17) || (tmpPos.x() > -5.5))  //  || (tmpPos.z() > 3.0   <-1.3  >1.6  smoothed colon segment  (tmpPos.x() < -1.5) || (tmpPos.x() > 1.6)
        {
            std::sprintf(intStr, "%d", i + 1);
            fixed_corner += std::string(intStr) + ' ';
            fixedVertice.push_back(i); // add later 06/04/2017
        }
    }

    fprintf(outpointtri, "V_Colon: Fixed point %s \n ", fixed_corner.c_str());

    // set fixed in the tetmesh add later 06/04/2017
    tetMesh->setFixedVertice(fixedVertice);
    tetMesh->updateFixedVertice();
    tetMesh->initDeformMesh(); // init for deform mesh : injection solution 06/20/2017

    // Plane
    auto geom1 = std::make_shared<imstk::Plane>();
    geom1->setPosition(Vec3d(-17, 0, 0));
    //geom1->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    geom1->rotate(imstk::FORWARD_VECTOR, imstk::PI_2);
    geom1->setWidth(30.0);
    geom1->print();
    Vec3d temp = geom1->getNormal();
    fprintf(outpointtri, "plane 1\n norm: %f %f %f\n", temp.x(), temp.y(), temp.z());
    temp = geom1->getPosition();
    fprintf(outpointtri, "position: %f %f %f\n", temp.x(), temp.y(), temp.z());
    auto planeObj1 = std::make_shared<imstk::VisualObject>("VisualPlane1");
    planeObj1->setVisualGeometry(geom1);
    // Plane
    auto geom2 = std::make_shared<imstk::Plane>();
    geom2->setPosition(Vec3d(-5.5, 0, 0));
    //geom2->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    geom2->rotate(imstk::FORWARD_VECTOR, imstk::PI_2);
    geom2->setWidth(30.0);
    geom2->print();
    temp = geom2->getNormal();
    fprintf(outpointtri, "plane 2\n norm: %f %f %f\n", temp.x(), temp.y(), temp.z());
    temp = geom2->getPosition();
    fprintf(outpointtri, "position: %f %f %f\n", temp.x(), temp.y(), temp.z());
    auto planeObj2 = std::make_shared<imstk::VisualObject>("VisualPlane2");
    planeObj2->setVisualGeometry(geom2);
    //scene->addSceneObject(planeObj1);
    //scene->addSceneObject(planeObj2);

    fclose(outpointtri);

    // Construct a map
    auto colonMapP2V = std::make_shared<imstk::VESSTetraTriangleMap>();  // <imstk::OneToOneMap>();
    colonMapP2V->setMaster(physicsColonMesh);
    colonMapP2V->setSlave(visualColonMesh);
    colonMapP2V->compute();


    auto colonMapP2C = std::make_shared<imstk::OneToOneMap>();
    colonMapP2C->setMaster(physicsColonMesh);
    colonMapP2C->setSlave(collidingColonMesh);
    colonMapP2C->compute();

    auto colonMapC2V = std::make_shared<imstk::VESSTetraTriangleMap>();  //<imstk::OneToOneMap>();
    colonMapC2V->setMaster(collidingColonMesh);
    colonMapC2V->setSlave(visualColonMesh);
    colonMapC2V->compute();

    auto colonObj = std::make_shared<PbdObject>("Colon");
    colonObj->setCollidingGeometry(collidingColonMesh);
    colonObj->setVisualGeometry(visualColonMesh);
    colonObj->setPhysicsGeometry(physicsColonMesh);
    colonObj->setPhysicsToCollidingMap(colonMapP2C);
    colonObj->setPhysicsToVisualMap(colonMapP2V);
    colonObj->setCollidingToVisualMap(colonMapC2V);

    // Create object and add to scene  just for scene, no colliding  for the two sides part, middle for operation part
    auto object2 = std::make_shared<imstk::VisualObject>("visualColonMesh2");
    object2->setVisualGeometry(visualColonMesh2); // change to any mesh created above
    //scene->addSceneObject(object2);
    auto object3 = std::make_shared<imstk::VisualObject>("visualColonMesh3");
    object3->setVisualGeometry(visualColonMesh3); // change to any mesh created above
    //scene->addSceneObject(object3);

    // wire frame for surface of volume mesh
    // START
    auto object4 = std::make_shared<imstk::VisualObject>("visualColonMesh4");
    auto material4 = std::make_shared<RenderMaterial>();
    material4->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmp4 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto surfM = tetMesh->getSurfaceMesh();
    material4->addTexture(tmp4);
    surfM->setRenderMaterial(material4);
    object4->setVisualGeometry(surfM); // change to any mesh created above
    scene->addSceneObject(object4);

    // Nick test
    auto objMeshNick = imstk::MeshIO::read(DATA_ROOT_PATH"/VESS/head/head2.dae");
    auto surfaceMeshNick = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMeshNick);
    surfaceMeshNick->scale(1);

    auto diffuseTextureNick = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/head/diffuse_test.png", Texture::Type::DIFFUSE);
    auto normalTextureNick = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/head/normal.png", Texture::Type::NORMAL);
    auto roughnessTextureNick = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/head/roughness.png", Texture::Type::ROUGHNESS);
    auto subsurfaceScatteringTextureNick = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/head/subsurfaceScattering.png", Texture::Type::SUBSURFACE_SCATTERING);
    auto materialNick = std::make_shared<RenderMaterial>();
    materialNick->addTexture(diffuseTextureNick);
    materialNick->addTexture(normalTextureNick);
    materialNick->addTexture(roughnessTextureNick);
    materialNick->addTexture(subsurfaceScatteringTextureNick);

    surfaceMeshNick->setRenderMaterial(materialNick);

    // Create object and add to scene
    auto objectNick = std::make_shared<imstk::VisualObject>("head");
    objectNick->setVisualGeometry(surfaceMeshNick); // change to any mesh created above
    scene->addSceneObject(objectNick);

    // Nick END

    //FILE *outNormal = fopen("G:/surfacenorm.dat","w");
    //fprintf(outNormal, "num:%d\n", surfM->getNumTriangles());
    std::vector<SurfaceMesh::TriangleArray> elements = surfM->getTrianglesVertices();
    for (int i = 0; i < surfM->getNumTriangles() - 1000; i++)
        //if (0)
    {
        //int i = 0;
        Vec3d tmp = surfM->getTriangleNormal(i);
        SurfaceMesh::TriangleArray& e = elements[i];
        const Vec3d v0 = surfM->getVertexPosition(e[0]);
        const Vec3d v1 = surfM->getVertexPosition(e[1]);
        const Vec3d v2 = surfM->getVertexPosition(e[2]);

        Vec3d cenPos = (v0 + v1 + v2) / 3;
        // XZH
        Vec3d p0 = Vec3d(-19.4, -5.85, 1.15);
        Vec3d p1 = Vec3d(-3.3, 2.4, 1.05);
        Vec3d a = cenPos - p0;
        Vec3d b = p1 - p0;
        Vec3d c = ((a.dot(b)) / (b.dot(b)))*b;
        Vec3d ee = a - c;
        if (tmp.dot(-ee) < 0)  // normal indicating the outside colon
        {
            //continue;
            if ((tmp.dot(-ee)) + tmp.norm()*ee.norm() < 0.1)
            {
                printf("cenPos: %f %f %f, -ee: %f %f %f\n", cenPos.x(), cenPos.y(), cenPos.z(), -ee.x(), -ee.y(), -ee.z());
            }
        }
        else
        {
            continue;
            //fprintf(outNormal, "%d\n", i);
        }
        continue;
        // XZH END

        // XZH using for the central line of colon
        auto surfTTT = std::make_shared<imstk::SurfaceMesh>();
        imstk::StdVectorOfVec3d vertListTTT;
        vertListTTT.resize(3);
        vertListTTT[0] = cenPos;
        tmp.normalize();
        vertListTTT[1] = cenPos + tmp;
        vertListTTT[2] = cenPos;
        //fprintf(outNormal, "Pos: %f %f %f 2: %f %f %f\n", cenPos.x(), cenPos.y(), cenPos.z(), cenPos.x() + tmp.x() * 10, cenPos.y() + tmp.y() * 10, cenPos.z() + tmp.z() * 10);

        surfTTT->setInitialVertexPositions(vertListTTT);
        surfTTT->setVertexPositions(vertListTTT);
        // c. Add connectivity data
        std::vector<imstk::SurfaceMesh::TriangleArray> trianglesTTT;
        imstk::SurfaceMesh::TriangleArray triTTT[1];
        triTTT[0] = { { 0, 1, 2 } };
        trianglesTTT.push_back(triTTT[0]);
        surfTTT->setTrianglesVertices(trianglesTTT);
        char str[25];
        sprintf(str, "surfaceNormal %d", i);
        auto objectTTT = std::make_shared<imstk::VisualObject>(str);
        auto materialTTT = std::make_shared<RenderMaterial>();
        materialTTT->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
        auto tmpTTT = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
        materialTTT->addTexture(tmpTTT);
        surfTTT->setRenderMaterial(materialTTT);
        objectTTT->setVisualGeometry(surfTTT); // change to any mesh created above

        scene->addSceneObject(objectTTT);
        continue;

        auto surfTTT1 = std::make_shared<imstk::SurfaceMesh>();
        imstk::StdVectorOfVec3d vertListTTT1;
        vertListTTT1.resize(3);
        vertListTTT1[0] = cenPos;
        Vec3d tmpTTT1 = -ee;
        //tmpTTT1.normalize();
        vertListTTT1[1] = cenPos + tmpTTT1;
        vertListTTT1[2] = cenPos;
        //fprintf(outNormal, "Pos: %f %f %f 2: %f %f %f\n", cenPos.x(), cenPos.y(), cenPos.z(), cenPos.x() + tmp.x() * 10, cenPos.y() + tmp.y() * 10, cenPos.z() + tmp.z() * 10);

        surfTTT1->setInitialVertexPositions(vertListTTT1);
        surfTTT1->setVertexPositions(vertListTTT1);
        // c. Add connectivity data
        std::vector<imstk::SurfaceMesh::TriangleArray> trianglesTTT1;
        imstk::SurfaceMesh::TriangleArray triTTT1[1];
        triTTT1[0] = { { 0, 1, 2 } };
        trianglesTTT1.push_back(triTTT1[0]);
        surfTTT1->setTrianglesVertices(trianglesTTT1);
        sprintf(str, "surfaceLine %d", i);
        auto objectTTT1 = std::make_shared<imstk::VisualObject>(str);
        auto materialTTT1 = std::make_shared<RenderMaterial>();
        materialTTT1->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
        //auto tmpTTTT = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
        //materialTTT1->addTexture(tmpTTTT);
        surfTTT1->setRenderMaterial(materialTTT1);
        objectTTT1->setVisualGeometry(surfTTT1); // change to any mesh created above


        scene->addSceneObject(objectTTT1);
        // XZH END
    }
    //fclose(outNormal);
    FILE *planeInfo = fopen("G:/planenorm.dat", "w");
    // rotate matrix from 2 vectors
    Vec3d nn0 = Vec3d(0, 1, 0);
    Vec3d nn1 = Vec3d(0, 0, 1);   // (-0.95, 1.84, -0.42);
    nn0.normalize();
    nn1.normalize();
    double dot = nn0.dot(nn1);
    if (dot < -1.0) dot = -1.0;
    if (dot > 1.0) dot = 1.0;
    double t = acos(dot); //  acos(nn0.dot(nn1));
    Vec3d nn2 = nn0.cross(nn1);
    nn2.normalize();
    //use angle&angle to make rotate matrix   http://blog.atelier39.org/cg/463.html
    double u = nn2.x();
    double v = nn2.y();
    double w = nn2.z();
    Mat3d rotTmp;
    rotTmp(0, 0) = u*u + (v*v + w*w)*cos(t);  rotTmp(0, 1) = u*v*(1 - cos(t)) - w*sin(t); rotTmp(0, 2) = u*w*(1 - cos(t)) + v*sin(t);
    rotTmp(1, 0) = u*v*(1 - cos(t)) + w*sin(t); rotTmp(1, 1) = v*v + (u*u + w*w)*cos(t); rotTmp(1, 2) = v*w*(1 - cos(t)) - u*sin(t);
    rotTmp(2, 0) = u*w*(1 - cos(t)) - v*sin(t); rotTmp(2, 1) = v*w*(1 - cos(t)) + u*sin(t); rotTmp(2, 2) = w*w + (u*u + v*v)*cos(t);

    // Plane default normal is (0,1,0) position (0,0,0)
    auto plane0 = std::make_shared<imstk::Plane>();
    //plane0->setPosition(Vec3d(0, 10, 0));  // -4.68, -0.63, 1.48
    plane0->translate(Vec3d(-4.68, -0.63, -3.9));  // -4.68, -0.63, 1.48
    //geom1->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    //plane0->rotate(imstk::FORWARD_VECTOR, imstk::PI_2);
    plane0->rotate(rotTmp);
    plane0->setWidth(30.0);
    plane0->print();
    Vec3d ttmp0 = plane0->getNormal();
    fprintf(planeInfo, "plane 1\n norm: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    ttmp0 = plane0->getPosition();
    fprintf(planeInfo, "position: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    auto planeOmni0 = std::make_shared<imstk::CollidingObject>("VisualPlane Omni0");
    planeOmni0->setVisualGeometry(plane0);
    planeOmni0->setCollidingGeometry(plane0);
    //scene->addSceneObject(planeOmni0);

    // Plane default normal is (0,1,0) position (0,0,0)
    auto plane1 = std::make_shared<imstk::Plane>();
    //plane0->setPosition(Vec3d(0, 10, 0));  // -4.68, -0.63, 1.48
    plane1->translate(Vec3d(-4.68, -0.63, 6.8));  // -4.68, -0.63, 1.48
    //geom1->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    //plane0->rotate(imstk::FORWARD_VECTOR, imstk::PI_2);
    plane1->rotate(rotTmp);
    plane1->setWidth(30.0);
    plane1->print();
    ttmp0 = plane1->getNormal();
    fprintf(planeInfo, "plane 2\n norm: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    ttmp0 = plane1->getPosition();
    fprintf(planeInfo, "position: %f %f %f\n", ttmp0.x(), ttmp0.y(), ttmp0.z());
    auto planeOmni1 = std::make_shared<imstk::CollidingObject>("VisualPlane Omni1");
    planeOmni1->setVisualGeometry(plane1);
    planeOmni1->setCollidingGeometry(plane1);
    //scene->addSceneObject(planeOmni1);
    fclose(planeInfo);
    // END wire frame

    auto pbdColonModel = std::make_shared<PbdModel>();
    pbdColonModel->configure(/*Number of Constraints*/2,
        /*Constraint configuration*/"Volume 0.7",  // 0.9 "Dihedral 0.8", // 0.01 0.8 effect is couhe not very well
        /*Constraint configuration*/"Distance 0.9",  // 0.8 effect is couhe
        /*Mass*/0.1, // 0.1
        /*Gravity*/"0 0 0",  // "0 -9.8 0",
        /*TimeStep*/0.001, // 0.001
        /*FixedPoint*/ fixed_corner.c_str(),
        /*NumberOfIterationInConstraintSolver*/3, //   2 12
        /*Proximity*/0.01,  // 0.1
        /*Contact stiffness*/0.1);
    colonObj->setDynamicalModel(pbdColonModel);

    auto pbdColonSolver = std::make_shared<PbdSolver>();
    pbdColonSolver->setPbdObject(colonObj);
    scene->addNonlinearSolver(pbdColonSolver);

    scene->addSceneObject(colonObj);

    // Collisions
    auto colGraph = scene->getCollisionGraph();
    auto pair = std::make_shared<PbdInteractionPair>(PbdInteractionPair(object, colonObj));
    pair->setNumberOfInterations(3); // 2

    colGraph->addInteractionPair(pair);

    // Sphere-plane
    //auto trackCtrl2 = std::make_shared<imstk::DeviceTracker>(client);
    //trackCtrl2->setTranslationScaling(0.25);  // 0.1  test 0.1 should use  Scale 0.001 will be better 
    ////Create and add virtual coupling object controller in the scene
    //auto objController = std::make_shared<imstk::SceneObjectController>(sphere3Obj, trackCtrl2);
    //scene->addObjectController(objController);
    //colGraph->addInteractionPair(planeOmni0, object,
    //    CollisionDetection::Type::UnidirectionalPlaneToSphere,
    //    CollisionHandling::Type::None,
    //    CollisionHandling::Type::Penalty);

    // Run
    sdk->setActiveScene(scene);
    sdk->startSimulation(true);
#endif
}

//// test force feedback for rectum (not colon part, which has lumen) split into three segments  use real knife model 2/3 length of artist
//void testPbdVirtualCoupling15()
//{
//#ifdef iMSTK_USE_OPENHAPTICS
//    // SDK and Scene
//    auto sdk = std::make_shared<imstk::SimulationManager>();
//    auto scene = sdk->createNewScene("SceneTestDevice");
//
//    // Device Client
//    auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");
//
//    // Device Server
//    auto server = std::make_shared<imstk::HDAPIDeviceServer>();
//    server->addDeviceClient(client);
//    sdk->addModule(server);
//
//    std::string path2obj = DATA_ROOT_PATH"/VESS/ITKnife7_4.obj"; // knife correct ITKnife6 // cube knife knife3.obj";  // //  113.obj"; //  /  blade2.obj  cube.obj  kcube.obj  test cloth using this 
//    std::string path2obj_c = DATA_ROOT_PATH"/VESS/ITKnife7_4.obj"; // knife correct ITKnife6  // cube knife knife3.obj";  //  //  113.obj";  // / VESS / TriangleTipKnife2.obj";  // blade2.obj  cube.obj kcube_s2.obj
//    auto readMesh = imstk::MeshIO::read(path2obj_c); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
//
//    // Line mesh
//    auto lineMesh = std::make_shared<imstk::LineMesh>();
//    imstk::StdVectorOfVec3d vertList;
//    vertList.resize(2);  // using 5 segment
//    //vertList[0] = imstk::Vec3d(-13, -2.25, 9.15); // should - volume mesh therold than surface mesh (9.0, -1.9, -0.6) AABB 0: 9.0, -2.23, -0.6   -0.7 ITKnife6.DAE  // knife3.obj 9.5, -0.9, 1.18
//    //vertList[1] = imstk::Vec3d(-5.93, -1.825, 9.125); // 9.7 ITKnife6.DAE // knife3.obj 9.5, -0.9, 8.73
//    //vertList[2] = imstk::Vec3d(1.14, -1.4, 9.1); // (9.0, -2.55, 9.5)  AABB 1: 9.0, -2.23, 9.6  9.7 ITKnife6.DAE // knife3.obj 9.5, -0.9, 8.73
//    vertList[0] = imstk::Vec3d(-16.4, -1.85, 9.15);
//    vertList[1] = imstk::Vec3d(-9.3, -1.4, 9.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
//    std::vector<std::vector<int> > connectivity;
//    for (int i = 0; i < 1;){
//        std::vector<int> line;
//        line.push_back(i);
//        i++;
//        line.push_back(i);
//
//        connectivity.push_back(line);
//    }
//
//    lineMesh->setInitialVertexPositions(vertList);
//    lineMesh->setVertexPositions(vertList);
//    lineMesh->setConnectivity(connectivity);
//    lineMesh->print();
//
//    auto visualMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(readMesh);
//    auto physicsMesh = lineMesh; //visualMesh; //  lineMesh; //  imstk::MeshIO::read(path2obj_c);  //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
//    auto collidingMesh = lineMesh; // visualMesh; //  lineMesh;
//    Vec3d min1, max1;
//    visualMesh->computeBoundingBox(min1, max1);
//    visualMesh->print();
//
//    auto material0 = std::make_shared<RenderMaterial>();
//    auto diffuseTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
//    auto normalTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Normal.png", Texture::Type::NORMAL);
//    auto roughnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Roughness.png", Texture::Type::ROUGHNESS);
//    auto metalnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
//    material0->addTexture(diffuseTexture0);
//    material0->addTexture(normalTexture0);
//    material0->addTexture(roughnessTexture0);
//    //material->addTexture(metalnessTexture);
//    visualMesh->setRenderMaterial(material0);
//
//    FILE *outpointtri;
//    outpointtri = fopen("G:/AABB.txt", "w");
//    fprintf(outpointtri, "V: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
//
//    collidingMesh->computeBoundingBox(min1, max1);
//    fprintf(outpointtri, "C: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
//    fclose(outpointtri);
//
//    auto object = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject");
//
//    auto objMapP2V = std::make_shared<imstk::OneToOneMap>();  // std::make_shared<imstk::TriangleLineMap>(); //  std::make_shared<imstk::OneToOneMap>();
//    objMapP2V->setMaster(physicsMesh);
//    objMapP2V->setSlave(visualMesh);
//    objMapP2V->compute();
//
//
//    auto objMapP2C = std::make_shared<imstk::OneToOneMap>();  //  colliding is physical mesh
//    objMapP2C->setMaster(physicsMesh);
//    objMapP2C->setSlave(collidingMesh);
//    objMapP2C->compute();
//
//    auto objMapC2V = std::make_shared<imstk::OneToOneMap>(); //  std::make_shared<imstk::TriangleLineMap>(); //  s std::make_shared<imstk::OneToOneMap>();
//    objMapC2V->setMaster(collidingMesh);
//    objMapC2V->setSlave(visualMesh);
//    objMapC2V->compute();
//
//    auto objMapC2P = std::make_shared<imstk::OneToOneMap>(); // colliding is physical mesh
//    objMapC2P->setMaster(collidingMesh);
//    objMapC2P->setSlave(physicsMesh);
//    objMapC2P->compute();
//
//    object->setVisualGeometry(visualMesh);
//    object->setCollidingGeometry(collidingMesh);
//    object->setPhysicsGeometry(physicsMesh);
//    object->setPhysicsToCollidingMap(objMapP2C);
//    object->setPhysicsToVisualMap(objMapP2V);
//    object->setCollidingToVisualMap(objMapC2V);
//    object->setColldingToPhysicsMap(objMapC2P);
//
//    auto pbdModel = std::make_shared<PbdModel>();
//    object->setDynamicalModel(pbdModel);
//    object->initialize(/*Number of Constraints*/1,
//        /*Constraint configuration*/"Distance 0.1",
//        /*Mass*/0.0,
//        /*Gravity*/"0 0 0",
//        /*TimeStep*/0.0003,
//        /*FixedPoint*/"",
//        /*NumberOfIterationInConstraintSolver*/2, // 5
//        /*Proximity*/0.5,  // use linemesh 0.3  // 0.01 use  //  cube 0.1
//        /*Contact stiffness*/0.001);
//
//    //auto pbdSolver = std::make_shared<PbdSolver>();
//    //pbdSolver->setPbdObject(object);
//    //scene->addNonlinearSolver(pbdSolver);
//
//    scene->addSceneObject(object);
//
//    auto trackCtrl = std::make_shared<imstk::DeviceTracker>(client);
//    trackCtrl->setTranslationScaling(0.25);  // 0.1  test 0.1 should use  Scale 0.001 will be better 
//    auto controller = std::make_shared<imstk::PBDSceneObjectController>(object, trackCtrl);
//    scene->addObjectController(controller);
//
//    // Sphere1
//    //auto sphere1Obj = apiutils::createCollidingAnalyticalSceneObject(
//    //    imstk::Geometry::Type::Sphere, scene, "Sphere1",1.0, Vec3d(-16.4, -1.85, 9.15)); // -2.23
//    //auto materialTT = std::make_shared<Material>();
//    //materialTT->setTexture(diffuseTexture0);
//    //auto suMesh = std::dynamic_pointer_cast<SurfaceMesh>(sphere1Obj->getVisualGeometry());
//    //suMesh->addMaterial(materialTT);
//    // Create a  
//    auto sphere1 = std::make_shared<imstk::Sphere>();
//    sphere1->setRadius(0.5);
//    sphere1->setTranslation(Vec3d(-16.4, -1.85, 9.15));
//
//    auto sphere1Obj = std::make_shared<SceneObject>("Sphere1");
//    sphere1Obj->setVisualGeometry(sphere1);
//    scene->addSceneObject(sphere1Obj);
//
//    auto sphere2 = std::make_shared<imstk::Sphere>();
//    sphere2->setRadius(0.5);
//    sphere2->setTranslation(Vec3d(-9.3, -1.4, 9.05));
//
//    auto sphere2Obj = std::make_shared<SceneObject>("Sphere2");
//    sphere2Obj->setVisualGeometry(sphere2);
//    scene->addSceneObject(sphere2Obj);
//
//    //auto sphere2Obj = apiutils::createCollidingAnalyticalSceneObject(
//    //    imstk::Geometry::Type::Sphere, scene, "Sphere2", 1.0, Vec3d(-9.3, -1.4, 9.05)); //    new -9.53, - 1.50,    8.6  org  -7.2, -1.35, 8.62
//
//    auto sphereOrg = apiutils::createCollidingAnalyticalSceneObject(
//        imstk::Geometry::Type::Sphere, scene, "SphereOrg", 0.2, Vec3d(0, 0, 0));
//
//    auto sphereX = apiutils::createCollidingAnalyticalSceneObject(
//        imstk::Geometry::Type::Sphere, scene, "SphereX", 2.5, Vec3d(10, 0, 0));
//
//    auto sphereY = apiutils::createCollidingAnalyticalSceneObject(
//        imstk::Geometry::Type::Sphere, scene, "SphereY", 1.5, Vec3d(0, 10, 0));
//
//    auto sphereZ = apiutils::createCollidingAnalyticalSceneObject(
//        imstk::Geometry::Type::Sphere, scene, "SphereZ", 0.8, Vec3d(0, 0, 10));
//    //scene->addSceneObject(sphere1Obj);
//    //scene->addSceneObject(sphere2Obj);
//
//    //auto trackCtrl1 = std::make_shared<imstk::DeviceTracker>(client);
//    //trackCtrl1->setTranslationScaling(0.1);
//    //auto controller1 = std::make_shared<imstk::SceneObjectController>(sphere1Obj, trackCtrl1);
//    //scene->addObjectController(controller1);
//    //auto controller2 = std::make_shared<imstk::SceneObjectController>(sphere2Obj, trackCtrl1);
//    //scene->addObjectController(controller2);
//
//    // Light (white)
//    auto whiteLight = std::make_shared<imstk::Light>("whiteLight");
//    whiteLight->setPosition(imstk::Vec3d(-15, -1, 0));
//    whiteLight->setFocalPoint(imstk::Vec3d(1, -1, 0));
//    whiteLight->setPositional();
//    //scene->addLight(whiteLight);
//    auto whiteLight2 = std::make_shared<imstk::Light>("whiteLight2");
//    whiteLight2->setPosition(imstk::Vec3d(1, -1, 0));
//    whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
//    whiteLight2->setPositional();
//    scene->addLight(whiteLight2);
//
//    // Light (red)
//    auto colorLight = std::make_shared<imstk::Light>("colorLight");
//    colorLight->setPosition(imstk::Vec3d(-25, -8, -4.5 ));
//    colorLight->setFocalPoint(imstk::Vec3d(-3.7, 5.5, 7));
//    colorLight->setColor(imstk::Color::White); // red
//    colorLight->setPositional();
//    colorLight->setSpotAngle(15);
//    auto colorLight2 = std::make_shared<imstk::Light>("colorLight2");
//    colorLight2->setPosition(imstk::Vec3d(-25, -8, -4.5));
//    colorLight2->setFocalPoint(imstk::Vec3d(-3.7, 5.5, 7));
//    colorLight2->setColor(imstk::Color::White);
//    colorLight2->setPositional();
//    colorLight2->setSpotAngle(15);
//    auto colorLight3 = std::make_shared<imstk::Light>("colorLight");
//    colorLight3->setPosition(imstk::Vec3d(-25, -5, -2.5  ));
//    colorLight3->setFocalPoint(imstk::Vec3d(-3.7, 5.5, 7));
//    colorLight3->setColor(imstk::Color::White); // red
//    colorLight3->setPositional();
//    colorLight3->setSpotAngle(15);
//    scene->addLight(colorLight);
//    //scene->addLight(colorLight2);
//    scene->addLight(colorLight3);
//
//    // Update Camera
//    auto cam1 = scene->getCamera();
//    cam1->setPosition(imstk::Vec3d(-25, -8, -4.5));
//    cam1->setFocalPoint(imstk::Vec3d(-3.7, 5.5, 7));
//    cam1->setViewAngle(75);
//
//    //// Tetrahedral mesh
//    imstk::StdVectorOfVec3d vertListTet;
//    std::vector<imstk::TetrahedralMesh::TetraArray> tetConnectivity;
//    //physXLink linkVolSur;
//    std::vector<physXLink> linksVolSur;
//    bool flag = loadTetFile(DATA_ROOT_PATH"/VESS/colonrectum_s002_SB.tet", vertListTet, tetConnectivity, linksVolSur); // colon_tumor3_3_SB for test   colon_tumor3_SB  cubeTest2_SB
//    auto tetMesh = std::make_shared<imstk::VESSTetrahedralMesh>();
//    tetMesh->setInitialVertexPositions(vertListTet);
//    tetMesh->setVertexPositions(vertListTet);
//    tetMesh->setTetrahedraVertices(tetConnectivity);
//    tetMesh->setPhysXLink(linksVolSur);
//    tetMesh->extractSurfaceMesh();
//    tetMesh->print();
//
//    // colon wall
//    std::string pathColon = DATA_ROOT_PATH"/VESS/colonrectum_s002_SB.obj"; // colon_tumor3_SB cubeTest2_SB  colon_s022.dae"; //  colon_s02.dae"; //  colon_s12.dae"; // volume mesh for colliding // key==colonnew_seg4.dae  colonnew_seg0508.dae small segment// good segment  colonnew_seg4.dae   colonnew_seg3.obj colon_move2.DAE"; //  VESS / colonnew_seg3.obj"; // glove.dae"; //  colon_move2.DAE";    // colon_move2.DAE  colon1_part_max.DAE  colon1_part_max.DAE  colon6_max.DAEcloth.DAE  colon_1_c2_max  colon_cylinder2_tri_max
//    std::string pathColon2 = DATA_ROOT_PATH"/VESS/colonrectum_s001.obj";  //  colon_s01
//    std::string pathColon3 = DATA_ROOT_PATH"/VESS/colonrectum_s003.dae";  // colon_s03
//
//    auto objMesh = imstk::MeshIO::read(pathColon);   // collidingColonMesh; //  imstk::MeshIO::read(pathColon);
//    auto physicsColonMesh = tetMesh; //  collidingColonMesh; //  imstk::MeshIO::read(pathColon);
//
//    auto objMesh2 = imstk::MeshIO::read(pathColon2);
//    auto objMesh3 = imstk::MeshIO::read(pathColon3);
//    auto visualColonMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh); // physicsColonMesh->getSurfaceMesh(); //  
//    auto visualColonMesh2 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh2);
//    auto visualColonMesh3 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh3);
//    auto collidingColonMesh = physicsColonMesh; //  visualColonMesh; //  imstk::MeshIO::read(pathColon);
//    visualColonMesh->print();
//
//    //XZH
//    FILE *outMesh = fopen("G:/outOBJ.dat", "w");
//    //visualColonMesh->getInitialVerticesPositions();
//    fprintf(outMesh, "num vertices:%d\n", visualColonMesh->getNumVertices());
//    Vec3d tempPos;
//    for (int i = 0; i < visualColonMesh->getNumVertices(); ++i){
//        tempPos = visualColonMesh->getVertexPosition(i);
//        fprintf(outMesh, "%f %f %f\n", tempPos.x(), tempPos.y(), tempPos.z());
//    }
//    fprintf(outMesh, "\n num tri:%d\n", visualColonMesh->getNumTriangles());
//    for (size_t triId = 0; triId < visualColonMesh->getNumTriangles(); ++triId)
//    {
//        Vec3d center(0, 0, 0);
//        auto vert = visualColonMesh->getTrianglesVertices().at(triId); // getTetrahedronVertices(triId);
//        fprintf(outMesh, "%d %d %d\n", vert[0], vert[1], vert[2]);
//        for (size_t i = 0; i < 3; ++i)
//        {
//            center = visualColonMesh->getInitialVertexPosition(vert[i]);
//            //fprintf(outMesh, "i: %d %f %f %f  ", i, center.x(), center.y(), center.z());
//        }
//        //fprintf(outMesh, "\n");
//    }
//
//    fclose(outMesh);
//    // XZH END
//
//    auto material = std::make_shared<RenderMaterial>();
//    auto diffuseTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
//    auto normalTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
//    auto roughnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
//    auto metalnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
//    material->addTexture(diffuseTexture);
//    material->addTexture(normalTexture);
//    material->addTexture(roughnessTexture);
//    //material->addTexture(metalnessTexture);
//    visualColonMesh->setRenderMaterial(material);
//
//    auto material2 = std::make_shared<RenderMaterial>();
//    auto diffuseTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
//    auto normalTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
//    auto roughnessTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
//    auto metalnessTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
//    material2->addTexture(diffuseTexture2);
//    material2->addTexture(normalTexture2);
//    material2->addTexture(roughnessTexture2);
//    visualColonMesh2->setRenderMaterial(material2);
//
//    auto material3 = std::make_shared<RenderMaterial>();
//    auto diffuseTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
//    auto normalTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
//    auto roughnessTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
//    auto metalnessTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
//    material3->addTexture(diffuseTexture3);
//    material3->addTexture(normalTexture3);
//    material3->addTexture(roughnessTexture3);
//    visualColonMesh3->setRenderMaterial(material3);
//
//    visualColonMesh->computeBoundingBox(min1, max1);
//    outpointtri = fopen("G:/AABB.txt", "a");
//    fprintf(outpointtri, "V core Colon: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
//
//    physicsColonMesh->computeBoundingBox(min1, max1);
//    fprintf(outpointtri, "C&P core Colon: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
//
//    visualColonMesh2->computeBoundingBox(min1, max1);
//    fprintf(outpointtri, "V1 Colon1: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
//    visualColonMesh3->computeBoundingBox(min1, max1);
//    fprintf(outpointtri, "V3 Colon3: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
//    // floor
//
//    //imstk::StdVectorOfVec3d vertList;
//    double width = 60.0;
//    double height = 60.0;
//    int nRows = 11;
//    int nCols = 11;
//    int corner[4] = { 1, nRows, nRows*nCols - nCols + 1, nRows*nCols };
//    char intStr[33];
//    std::string fixed_corner;
//    //for (unsigned int i = 0; i < 4; ++i)
//    //{
//    //	std::sprintf(intStr, "%d", corner[i]);
//    //	fixed_corner += std::string(intStr) + ' ';
//    //}
//
//    //fixed point
//    Vec3d tmpPos;
//    for (int i = 0; i < physicsColonMesh->getNumVertices(); ++i) // physicsColonMesh  visualColonMesh
//    {
//        tmpPos = physicsColonMesh->getVertexPosition(i);  // physicsColonMesh  visualColonMesh
//        if ((tmpPos.x() < -17) || (tmpPos.x() > -5.5))  //  || (tmpPos.z() > 3.0   <-1.3  >1.6  smoothed colon segment  (tmpPos.x() < -1.5) || (tmpPos.x() > 1.6)
//        {
//            std::sprintf(intStr, "%d", i + 1);
//            fixed_corner += std::string(intStr) + ' ';
//        }
//    }
//
//    fprintf(outpointtri, "V_Colon: Fixed point %s \n ", fixed_corner.c_str());
//
//
//    // Plane
//    auto geom1 = std::make_shared<imstk::Plane>();
//    geom1->setPosition(Vec3d(-17, 0, 0));
//    //geom1->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
//    geom1->rotate(imstk::FORWARD_VECTOR, imstk::PI_2);
//    geom1->setWidth(30.0);
//    geom1->print();
//    Vec3d temp = geom1->getNormal();
//    fprintf(outpointtri, "plane 1\n norm: %f %f %f\n", temp.x(), temp.y(), temp.z());
//    temp = geom1->getPosition();
//    fprintf(outpointtri, "position: %f %f %f\n", temp.x(), temp.y(), temp.z());
//    auto planeObj1 = std::make_shared<imstk::VisualObject>("VisualPlane1");
//    planeObj1->setVisualGeometry(geom1);
//    // Plane
//    auto geom2 = std::make_shared<imstk::Plane>();
//    geom2->setPosition(Vec3d(-5.5, 0, 0));
//    //geom2->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
//    geom2->rotate(imstk::FORWARD_VECTOR, imstk::PI_2);
//    geom2->setWidth(30.0);
//    geom2->print();
//    temp = geom2->getNormal();
//    fprintf(outpointtri, "plane 2\n norm: %f %f %f\n", temp.x(), temp.y(), temp.z());
//    temp = geom2->getPosition();
//    fprintf(outpointtri, "position: %f %f %f\n", temp.x(), temp.y(), temp.z());
//    auto planeObj2 = std::make_shared<imstk::VisualObject>("VisualPlane2");
//    planeObj2->setVisualGeometry(geom2);
//    //scene->addSceneObject(planeObj1);
//    //scene->addSceneObject(planeObj2);
//
//    fclose(outpointtri);
//
//    // Construct a map
//    auto colonMapP2V = std::make_shared<imstk::VESSTetraTriangleMap>();  // <imstk::OneToOneMap>();
//    colonMapP2V->setMaster(physicsColonMesh);
//    colonMapP2V->setSlave(visualColonMesh);
//    colonMapP2V->compute();
//
//
//    auto colonMapP2C = std::make_shared<imstk::OneToOneMap>();
//    colonMapP2C->setMaster(physicsColonMesh);
//    colonMapP2C->setSlave(collidingColonMesh);
//    colonMapP2C->compute();
//
//    auto colonMapC2V = std::make_shared<imstk::VESSTetraTriangleMap>();  //<imstk::OneToOneMap>();
//    colonMapC2V->setMaster(collidingColonMesh);
//    colonMapC2V->setSlave(visualColonMesh);
//    colonMapC2V->compute();
//
//    auto colonObj = std::make_shared<PbdObject>("Colon");
//    colonObj->setCollidingGeometry(collidingColonMesh);
//    colonObj->setVisualGeometry(visualColonMesh);
//    colonObj->setPhysicsGeometry(physicsColonMesh);
//    colonObj->setPhysicsToCollidingMap(colonMapP2C);
//    colonObj->setPhysicsToVisualMap(colonMapP2V);
//    colonObj->setCollidingToVisualMap(colonMapC2V);
//
//    // Create object and add to scene  just for scene, no colliding  for the two sides part, middle for operation part
//    auto object2 = std::make_shared<imstk::VisualObject>("visualColonMesh2");
//    object2->setVisualGeometry(visualColonMesh2); // change to any mesh created above
//    //scene->addSceneObject(object2);
//    auto object3 = std::make_shared<imstk::VisualObject>("visualColonMesh3");
//    object3->setVisualGeometry(visualColonMesh3); // change to any mesh created above
//    scene->addSceneObject(object3);
//
//    auto object4 = std::make_shared<imstk::VisualObject>("visualColonMesh4");
//    auto material4 = std::make_shared<RenderMaterial>();
//    material4->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
//    auto tmp4 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
//    auto surfM = tetMesh->getSurfaceMesh();
//    material4->addTexture(tmp4);
//    surfM->setRenderMaterial(material4);
//    object4->setVisualGeometry(surfM); // change to any mesh created above
//    //scene->addSceneObject(object4);
//
//    auto pbdColonModel = std::make_shared<PbdModel>();
//    colonObj->setDynamicalModel(pbdColonModel);
//
//    colonObj->initialize(/*Number of Constraints*/2,
//        /*Constraint configuration*/"Volume 0.1",  // 0.9 "Dihedral 0.8", // 0.01 0.8 effect is couhe not very well
//        /*Constraint configuration*/"Distance 0.7",  // 0.8 effect is couhe
//        /*Mass*/0.1, // 0.1
//        /*Gravity*/"0 0 0",  // "0 -9.8 0",
//        /*TimeStep*/0.001, // 0.001
//        /*FixedPoint*/ fixed_corner.c_str(),
//        /*NumberOfIterationInConstraintSolver*/2, //   2 12
//        /*Proximity*/0.02,  // 0.1
//        /*Contact stiffness*/0.1);
//
//    auto pbdColonSolver = std::make_shared<PbdSolver>();
//    pbdColonSolver->setPbdObject(colonObj);
//    scene->addNonlinearSolver(pbdColonSolver);
//
//    scene->addSceneObject(colonObj);
//
//    // Collisions
//    auto colGraph = scene->getCollisionGraph();
//    auto pair = std::make_shared<PbdInteractionPair>(PbdInteractionPair(object, colonObj));
//    pair->setNumberOfInterations(2); // 2
//
//    colGraph->addInteractionPair(pair);
//
//    // Run
//    sdk->setActiveScene(scene);
//    sdk->startSimulation(true);
//#endif
//}
// test force feedback for rectum (not colon part, which has lumen) split into three segments  use real knife model 2/3 length of artist
void testPbdVirtualCoupling15()
{
#ifdef iMSTK_USE_OPENHAPTICS
    // SDK and Scene
    auto sdk = std::make_shared<imstk::SimulationManager>();
    auto scene = sdk->createNewScene("SceneTestDevice");

    // Device Client
    auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

    // Device Server
    auto server = std::make_shared<imstk::HDAPIDeviceServer>();
    server->addDeviceClient(client);
    sdk->addModule(server);

    std::string path2obj = DATA_ROOT_PATH"/VESS/ITKnife7_4.obj"; // knife correct ITKnife6 // cube knife knife3.obj";  // //  113.obj"; //  /  blade2.obj  cube.obj  kcube.obj  test cloth using this 
    std::string path2obj_c = DATA_ROOT_PATH"/VESS/ITKnife7_4.obj"; // knife correct ITKnife6  // cube knife knife3.obj";  //  //  113.obj";  // / VESS / TriangleTipKnife2.obj";  // blade2.obj  cube.obj kcube_s2.obj
    auto readMesh = imstk::MeshIO::read(path2obj_c); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);

    // Line mesh
    auto lineMesh = std::make_shared<imstk::LineMesh>();
    imstk::StdVectorOfVec3d vertList;
    vertList.resize(2);  // using 5 segment
    //vertList[0] = imstk::Vec3d(-13, -2.25, 9.15); // should - volume mesh therold than surface mesh (9.0, -1.9, -0.6) AABB 0: 9.0, -2.23, -0.6   -0.7 ITKnife6.DAE  // knife3.obj 9.5, -0.9, 1.18
    //vertList[1] = imstk::Vec3d(-5.93, -1.825, 9.125); // 9.7 ITKnife6.DAE // knife3.obj 9.5, -0.9, 8.73
    //vertList[2] = imstk::Vec3d(1.14, -1.4, 9.1); // (9.0, -2.55, 9.5)  AABB 1: 9.0, -2.23, 9.6  9.7 ITKnife6.DAE // knife3.obj 9.5, -0.9, 8.73
    vertList[0] = imstk::Vec3d(-16.4, -1.85, 9.15);
    vertList[1] = imstk::Vec3d(-9.3, -1.4, 9.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    std::vector<std::vector<int> > connectivity;
    for (int i = 0; i < 1;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity.push_back(line);
    }

    lineMesh->setInitialVertexPositions(vertList);
    lineMesh->setVertexPositions(vertList);
    lineMesh->setConnectivity(connectivity);


    // using for force normal
    auto tempSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertList2;
    vertList2.resize(3);
    vertList2[0] = imstk::Vec3d(-16.4, -1.85, 9.15);
    vertList2[1] = imstk::Vec3d(-9.3, -1.4, 9.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertList2[2] = imstk::Vec3d(-16.4, -1.85, 9.15);
    std::vector<std::vector<int> > connectivity2;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity2.push_back(line);
    }

    tempSurfMesh->setInitialVertexPositions(vertList2);
    tempSurfMesh->setVertexPositions(vertList2);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> triangles;
    imstk::SurfaceMesh::TriangleArray tri[1];
    tri[0] = { { 0, 1, 2 } };
    triangles.push_back(tri[0]);
    tempSurfMesh->setTrianglesVertices(triangles);
    auto objectTmp = std::make_shared<imstk::VisualObject>("visualNormal0");
    auto materialTmp = std::make_shared<RenderMaterial>();
    materialTmp->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpTT = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialTmp->addTexture(tmpTT);
    tempSurfMesh->setRenderMaterial(materialTmp);
    objectTmp->setVisualGeometry(tempSurfMesh); // change to any mesh created above
    scene->addSceneObject(objectTmp);
    // END

    // XZH using for the central line of colon
    auto centrallineSurfMesh = std::make_shared<imstk::SurfaceMesh>();
    imstk::StdVectorOfVec3d vertList3;
    vertList3.resize(3);
    vertList3[0] = imstk::Vec3d(-19.4, -5.85, 1.15);
    vertList3[1] = imstk::Vec3d(-3.3, 2.4, 1.05);  //  -9.5369   -1.5169    8.5977  orginal -7.2, -1.35, 8.62
    vertList3[2] = imstk::Vec3d(-19.4, -5.85, 1.15);
    std::vector<std::vector<int> > connectivity3;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity3.push_back(line);
    }

    centrallineSurfMesh->setInitialVertexPositions(vertList3);
    centrallineSurfMesh->setVertexPositions(vertList3);
    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> triangles1;
    imstk::SurfaceMesh::TriangleArray tri1[1];
    tri1[0] = { { 0, 1, 2 } };
    triangles1.push_back(tri1[0]);
    centrallineSurfMesh->setTrianglesVertices(triangles1);
    auto objectCentral = std::make_shared<imstk::VisualObject>("visualNormal1");
    auto materialCentral = std::make_shared<RenderMaterial>();
    materialCentral->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmpCentral = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    materialCentral->addTexture(tmpCentral);
    centrallineSurfMesh->setRenderMaterial(materialCentral);
    objectCentral->setVisualGeometry(centrallineSurfMesh); // change to any mesh created above
    scene->addSceneObject(objectCentral);
    // XZH END

    auto visualMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(readMesh);
    auto physicsMesh = lineMesh; //visualMesh; //  lineMesh; //  imstk::MeshIO::read(path2obj_c);  //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
    auto collidingMesh = lineMesh; // visualMesh; //  lineMesh;
    Vec3d min1, max1;
    visualMesh->computeBoundingBox(min1, max1);

    auto material0 = std::make_shared<RenderMaterial>();
    auto diffuseTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto normalTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Normal.png", Texture::Type::NORMAL);
    auto roughnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Roughness.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material0->addTexture(diffuseTexture0);
    material0->addTexture(normalTexture0);
    material0->addTexture(roughnessTexture0);
    //material->setTexture(metalnessTexture);
    visualMesh->setRenderMaterial(material0);

    FILE *outpointtri;
    outpointtri = fopen("G:/AABB.txt", "w");
    fprintf(outpointtri, "V: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    collidingMesh->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "C: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    fclose(outpointtri);

    auto object = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject");

    auto objMapP2V = std::make_shared<imstk::OneToOneMap>();  // std::make_shared<imstk::TriangleLineMap>(); //  std::make_shared<imstk::OneToOneMap>();
    objMapP2V->setMaster(physicsMesh);
    objMapP2V->setSlave(visualMesh);
    objMapP2V->compute();


    auto objMapP2C = std::make_shared<imstk::OneToOneMap>();  //  colliding is physical mesh
    objMapP2C->setMaster(physicsMesh);
    objMapP2C->setSlave(collidingMesh);
    objMapP2C->compute();

    auto objMapC2V = std::make_shared<imstk::OneToOneMap>(); //  std::make_shared<imstk::TriangleLineMap>(); //  s std::make_shared<imstk::OneToOneMap>();
    objMapC2V->setMaster(collidingMesh);
    objMapC2V->setSlave(visualMesh);
    objMapC2V->compute();

    auto objMapC2P = std::make_shared<imstk::OneToOneMap>(); // colliding is physical mesh
    objMapC2P->setMaster(collidingMesh);
    objMapC2P->setSlave(physicsMesh);
    objMapC2P->compute();

    object->setVisualGeometry(visualMesh);
    object->setCollidingGeometry(collidingMesh);
    object->setPhysicsGeometry(physicsMesh);
    object->setPhysicsToCollidingMap(objMapP2C);
    object->setPhysicsToVisualMap(objMapP2V);
    object->setCollidingToVisualMap(objMapC2V);
    object->setColldingToPhysicsMap(objMapC2P);

    auto pbdModel = std::make_shared<PbdModel>();
    pbdModel->configure(/*Number of Constraints*/1,
        /*Constraint configuration*/"Distance 0.1",
        /*Mass*/0.0,
        /*Gravity*/"0 0 0",
        /*TimeStep*/0.0003,
        /*FixedPoint*/"",
        /*NumberOfIterationInConstraintSolver*/2, // 5
        /*Proximity*/0.1,  // use linemesh 0.3  // 0.01 use  //  cube 0.1
        /*Contact stiffness*/0.001);
    object->setDynamicalModel(pbdModel);

    //auto pbdSolver = std::make_shared<PbdSolver>();
    //pbdSolver->setPbdObject(object);
    //scene->addNonlinearSolver(pbdSolver);

    scene->addSceneObject(object);

    auto trackCtrl = std::make_shared<imstk::DeviceTracker>(client);
    trackCtrl->setTranslationScaling(0.25);  // 0.1  test 0.1 should use  Scale 0.001 will be better 
    auto controller = std::make_shared<imstk::PBDSceneObjectController>(object, trackCtrl);
    scene->addObjectController(controller);

    // Sphere1
    auto sphere1Obj = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "Sphere1", 0.5, Vec3d(-16.4, -1.85, 9.15)); // -2.23
    sphere1Obj->getVisualGeometry()->setTranslation(Vec3d(-16.4, -1.85, 9.15));
    //auto materialTT = std::make_shared<Material>();
    //materialTT->setTexture(diffuseTexture0);
    //auto suMesh = std::dynamic_pointer_cast<SurfaceMesh>(sphere1Obj->getVisualGeometry());
    //suMesh->addMaterial(materialTT);
    auto sphere2Obj = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "Sphere2", 0.5, Vec3d(-9.3, -1.4, 9.05)); //-9.3, -1.4, 9.05    new -9.53, - 1.50,    8.6  org  -7.2, -1.35, 8.62
    sphere2Obj->getVisualGeometry()->setTranslation(Vec3d(-9.3, -1.4, 9.05));
    

    auto sphereOrg = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereOrg", 0.2, Vec3d(0, 0, 0));
    sphereOrg->getVisualGeometry()->setTranslation(Vec3d(0, 0, 0));

    auto sphereX = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereX", 1.5, Vec3d(10, 0, 0));
    sphereX->getVisualGeometry()->setTranslation(Vec3d(10, 0, 0));

    auto sphereY = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereY", 1.2, Vec3d(0, 10, 0));
    sphereY->getVisualGeometry()->setTranslation(Vec3d(0, 10, 0));

    auto sphereZ = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereZ", 0.8, Vec3d(0, 0, 10));
    sphereZ->getVisualGeometry()->setTranslation(Vec3d(0, 0, 10));
    //scene->addSceneObject(sphere1Obj);
    //scene->addSceneObject(sphere2Obj);

    //auto trackCtrl1 = std::make_shared<imstk::DeviceTracker>(client);
    //trackCtrl1->setTranslationScaling(0.1);
    //auto controller1 = std::make_shared<imstk::SceneObjectController>(sphere1Obj, trackCtrl1);
    //scene->addObjectController(controller1);
    //auto controller2 = std::make_shared<imstk::SceneObjectController>(sphere2Obj, trackCtrl1);
    //scene->addObjectController(controller2);

    // Light (white)
    auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
    whiteLight->setPosition(imstk::Vec3d(-15, -1, 0));
    whiteLight->setFocalPoint(imstk::Vec3d(1, -1, 0));
    scene->addLight(whiteLight);
    auto whiteLight2 = std::make_shared<imstk::PointLight>("whiteLight2");
    whiteLight2->setPosition(imstk::Vec3d(1, -1, 0));
    whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    //scene->addLight(whiteLight2);

    // Light (red)
    auto colorLight = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight->setPosition(imstk::Vec3d(-25, -8, -4.5));
    colorLight->setFocalPoint(imstk::Vec3d(-3.7, 5.5, 7));
    colorLight->setColor(imstk::Color::White); // red
    colorLight->setSpotAngle(15);
    auto colorLight2 = std::make_shared<imstk::SpotLight>("colorLight2");
    colorLight2->setPosition(imstk::Vec3d(-25, -8, -4.5));
    colorLight2->setFocalPoint(imstk::Vec3d(-3.7, 5.5, 7));
    colorLight2->setColor(imstk::Color::White);
    colorLight2->setSpotAngle(15);
    auto colorLight3 = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight3->setPosition(imstk::Vec3d(-25, -5, -2.5));  //-25, -5, -2.5
    colorLight3->setFocalPoint(imstk::Vec3d(-3.7, 5.5, 7));
    colorLight3->setColor(imstk::Color::White); // red
    colorLight3->setSpotAngle(15);
    scene->addLight(colorLight);
    //scene->addLight(colorLight2);
    scene->addLight(colorLight3);

    // Update Camera
    auto cam1 = scene->getCamera();
    cam1->setPosition(imstk::Vec3d(-25, -8, -4.5));
    cam1->setFocalPoint(imstk::Vec3d(-3.7, 5.5, 7));
    cam1->setViewAngle(75);

    //// Tetrahedral mesh
    imstk::StdVectorOfVec3d vertListTet;
    std::vector<imstk::TetrahedralMesh::TetraArray> tetConnectivity;
    //physXLink linkVolSur;
    std::vector<physXLink> linksVolSur;
    bool flag = loadTetFile(DATA_ROOT_PATH"/VESS/colonrectum_s002_SB.tet", vertListTet, tetConnectivity, linksVolSur); // colon_tumor3_3_SB for test   colon_tumor3_SB  cubeTest2_SB
    auto tetMesh = std::make_shared<imstk::VESSTetrahedralMesh>();
    tetMesh->setInitialVertexPositions(vertListTet);
    tetMesh->setVertexPositions(vertListTet);
    tetMesh->setTetrahedraVertices(tetConnectivity);
    tetMesh->setPhysXLink(linksVolSur);
    tetMesh->extractSurfaceMesh();


    // colon wall
    std::string pathColon = DATA_ROOT_PATH"/VESS/colonrectum_s002_SB.obj"; // colon_tumor3_SB cubeTest2_SB  colon_s022.dae"; //  colon_s02.dae"; //  colon_s12.dae"; // volume mesh for colliding // key==colonnew_seg4.dae  colonnew_seg0508.dae small segment// good segment  colonnew_seg4.dae   colonnew_seg3.obj colon_move2.DAE"; //  VESS / colonnew_seg3.obj"; // glove.dae"; //  colon_move2.DAE";    // colon_move2.DAE  colon1_part_max.DAE  colon1_part_max.DAE  colon6_max.DAEcloth.DAE  colon_1_c2_max  colon_cylinder2_tri_max
    std::string pathColon2 = DATA_ROOT_PATH"/VESS/colonrectum_s001.obj";  //  colon_s01
    std::string pathColon3 = DATA_ROOT_PATH"/VESS/colonrectum_s003.dae";  // colon_s03

    auto objMesh = imstk::MeshIO::read(pathColon);   // collidingColonMesh; //  imstk::MeshIO::read(pathColon);
    auto physicsColonMesh = tetMesh; //  collidingColonMesh; //  imstk::MeshIO::read(pathColon);

    auto objMesh2 = imstk::MeshIO::read(pathColon2);
    auto objMesh3 = imstk::MeshIO::read(pathColon3);
    auto visualColonMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh); // physicsColonMesh->getSurfaceMesh(); //  
    auto visualColonMesh2 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh2);
    auto visualColonMesh3 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh3);
    auto collidingColonMesh = physicsColonMesh; //  visualColonMesh; //  imstk::MeshIO::read(pathColon);

    //XZH
    FILE *outMesh = fopen("G:/outOBJ.dat", "w");
    //visualColonMesh->getInitialVerticesPositions();
    fprintf(outMesh, "num vertices:%d\n", visualColonMesh->getNumVertices());
    Vec3d tempPos;
    for (int i = 0; i < visualColonMesh->getNumVertices(); ++i){
        tempPos = visualColonMesh->getVertexPosition(i);
        fprintf(outMesh, "%f %f %f\n", tempPos.x(), tempPos.y(), tempPos.z());
    }
    fprintf(outMesh, "\n num tri:%d\n", visualColonMesh->getNumTriangles());
    for (size_t triId = 0; triId < visualColonMesh->getNumTriangles(); ++triId)
    {
        Vec3d center(0, 0, 0);
        auto vert = visualColonMesh->getTrianglesVertices().at(triId); // getTetrahedronVertices(triId);
        fprintf(outMesh, "%d %d %d\n", vert[0], vert[1], vert[2]);
        for (size_t i = 0; i < 3; ++i)
        {
            center = visualColonMesh->getInitialVertexPosition(vert[i]);
            //fprintf(outMesh, "i: %d %f %f %f  ", i, center.x(), center.y(), center.z());
        }
        //fprintf(outMesh, "\n");
    }

    fclose(outMesh);
    // XZH END

    auto material = std::make_shared<RenderMaterial>();
    auto diffuseTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    auto normalTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material->addTexture(diffuseTexture);
    material->addTexture(normalTexture);
    material->addTexture(roughnessTexture);
    //material->setTexture(metalnessTexture);
    visualColonMesh->setRenderMaterial(material);

    auto material2 = std::make_shared<RenderMaterial>();
    auto diffuseTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    auto normalTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture2 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material2->addTexture(diffuseTexture2);
    material2->addTexture(normalTexture2);
    material2->addTexture(roughnessTexture2);
    visualColonMesh2->setRenderMaterial(material2);

    auto material3 = std::make_shared<RenderMaterial>();
    auto diffuseTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    auto normalTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture3 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material3->addTexture(diffuseTexture3);
    material3->addTexture(normalTexture3);
    material3->addTexture(roughnessTexture3);
    visualColonMesh3->setRenderMaterial(material3);

    visualColonMesh->computeBoundingBox(min1, max1);
    outpointtri = fopen("G:/AABB.txt", "a");
    fprintf(outpointtri, "V core Colon: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    physicsColonMesh->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "C&P core Colon: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    visualColonMesh2->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "V1 Colon1: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    visualColonMesh3->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "V3 Colon3: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    // floor

    //imstk::StdVectorOfVec3d vertList;
    double width = 60.0;
    double height = 60.0;
    int nRows = 11;
    int nCols = 11;
    int corner[4] = { 1, nRows, nRows*nCols - nCols + 1, nRows*nCols };
    char intStr[33];
    std::string fixed_corner;
    //for (unsigned int i = 0; i < 4; ++i)
    //{
    //	std::sprintf(intStr, "%d", corner[i]);
    //	fixed_corner += std::string(intStr) + ' ';
    //}

    //fixed point
    Vec3d tmpPos;
    for (int i = 0; i < physicsColonMesh->getNumVertices(); ++i) // physicsColonMesh  visualColonMesh
    {
        tmpPos = physicsColonMesh->getVertexPosition(i);  // physicsColonMesh  visualColonMesh
        if ((tmpPos.x() < -17) || (tmpPos.x() > -5.5))  //  || (tmpPos.z() > 3.0   <-1.3  >1.6  smoothed colon segment  (tmpPos.x() < -1.5) || (tmpPos.x() > 1.6)
        {
            std::sprintf(intStr, "%d", i + 1);
            fixed_corner += std::string(intStr) + ' ';
        }
    }

    fprintf(outpointtri, "V_Colon: Fixed point %s \n ", fixed_corner.c_str());


    // Plane
    auto geom1 = std::make_shared<imstk::Plane>();
    geom1->setPosition(Vec3d(-17, 0, 0));
    //geom1->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    geom1->rotate(imstk::FORWARD_VECTOR, imstk::PI_2);
    geom1->setWidth(30.0);
    geom1->print();
    Vec3d temp = geom1->getNormal();
    fprintf(outpointtri, "plane 1\n norm: %f %f %f\n", temp.x(), temp.y(), temp.z());
    temp = geom1->getPosition();
    fprintf(outpointtri, "position: %f %f %f\n", temp.x(), temp.y(), temp.z());
    auto planeObj1 = std::make_shared<imstk::VisualObject>("VisualPlane1");
    planeObj1->setVisualGeometry(geom1);
    // Plane
    auto geom2 = std::make_shared<imstk::Plane>();
    geom2->setPosition(Vec3d(-5.5, 0, 0));
    //geom2->setNormal(Vec3d(0, -3, 0)); // 0, -0.325, 4.85
    geom2->rotate(imstk::FORWARD_VECTOR, imstk::PI_2);
    geom2->setWidth(30.0);
    geom2->print();
    temp = geom2->getNormal();
    fprintf(outpointtri, "plane 2\n norm: %f %f %f\n", temp.x(), temp.y(), temp.z());
    temp = geom2->getPosition();
    fprintf(outpointtri, "position: %f %f %f\n", temp.x(), temp.y(), temp.z());
    auto planeObj2 = std::make_shared<imstk::VisualObject>("VisualPlane2");
    planeObj2->setVisualGeometry(geom2);
    //scene->addSceneObject(planeObj1);
    //scene->addSceneObject(planeObj2);

    fclose(outpointtri);

    // Construct a map
    auto colonMapP2V = std::make_shared<imstk::VESSTetraTriangleMap>();  // <imstk::OneToOneMap>();
    colonMapP2V->setMaster(physicsColonMesh);
    colonMapP2V->setSlave(visualColonMesh);
    colonMapP2V->compute();


    auto colonMapP2C = std::make_shared<imstk::OneToOneMap>();
    colonMapP2C->setMaster(physicsColonMesh);
    colonMapP2C->setSlave(collidingColonMesh);
    colonMapP2C->compute();

    auto colonMapC2V = std::make_shared<imstk::VESSTetraTriangleMap>();  //<imstk::OneToOneMap>();
    colonMapC2V->setMaster(collidingColonMesh);
    colonMapC2V->setSlave(visualColonMesh);
    colonMapC2V->compute();

    auto colonObj = std::make_shared<PbdObject>("Colon");
    colonObj->setCollidingGeometry(collidingColonMesh);
    colonObj->setVisualGeometry(visualColonMesh);
    colonObj->setPhysicsGeometry(physicsColonMesh);
    colonObj->setPhysicsToCollidingMap(colonMapP2C);
    colonObj->setPhysicsToVisualMap(colonMapP2V);
    colonObj->setCollidingToVisualMap(colonMapC2V);

    // Create object and add to scene  just for scene, no colliding  for the two sides part, middle for operation part
    auto object2 = std::make_shared<imstk::VisualObject>("visualColonMesh2");
    object2->setVisualGeometry(visualColonMesh2); // change to any mesh created above
    //scene->addSceneObject(object2);
    auto object3 = std::make_shared<imstk::VisualObject>("visualColonMesh3");
    object3->setVisualGeometry(visualColonMesh3); // change to any mesh created above
    //scene->addSceneObject(object3);

    auto object4 = std::make_shared<imstk::VisualObject>("visualColonMesh4");
    auto material4 = std::make_shared<RenderMaterial>();
    material4->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmp4 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto surfM = tetMesh->getSurfaceMesh();
    material4->addTexture(tmp4);
    surfM->setRenderMaterial(material4);
    object4->setVisualGeometry(surfM); // change to any mesh created above
    scene->addSceneObject(object4);

    auto pbdColonModel = std::make_shared<PbdModel>();
    pbdColonModel->configure(/*Number of Constraints*/2,
        /*Constraint configuration*/"Volume 0.05",  // 0.9 "Dihedral 0.8", // 0.01 0.8 effect is couhe not very well
        /*Constraint configuration*/"Distance 0.05",  // 0.8 effect is couhe
        /*Mass*/0.1, // 0.1
        /*Gravity*/"0 0 0",  // "0 -9.8 0",
        /*TimeStep*/0.001, // 0.001
        /*FixedPoint*/ fixed_corner.c_str(),
        /*NumberOfIterationInConstraintSolver*/3, //   2 12
        /*Proximity*/0.01,  // 0.1
        /*Contact stiffness*/0.1);
    colonObj->setDynamicalModel(pbdColonModel);

    auto pbdColonSolver = std::make_shared<PbdSolver>();
    pbdColonSolver->setPbdObject(colonObj);
    scene->addNonlinearSolver(pbdColonSolver);

    scene->addSceneObject(colonObj);

    // Collisions
    auto colGraph = scene->getCollisionGraph();
    auto pair = std::make_shared<PbdInteractionPair>(PbdInteractionPair(object, colonObj));
    pair->setNumberOfInterations(3); // 2

    colGraph->addInteractionPair(pair);

    // Run
    sdk->setActiveScene(scene);
    sdk->startSimulation(true);
#endif
}

// test force feedback for rectum (not colon part, which has lumen) split into three segments
void testPbdVirtualCoupling13()
{
#ifdef iMSTK_USE_OPENHAPTICS
    // SDK and Scene
    auto sdk = std::make_shared<imstk::SimulationManager>();
    auto scene = sdk->createNewScene("SceneTestDevice");

    // Device Client
    auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

    // Device Server
    auto server = std::make_shared<imstk::HDAPIDeviceServer>();
    server->addDeviceClient(client);
    sdk->addModule(server);

    std::string path2obj = DATA_ROOT_PATH"/VESS/ITKnife7_1.obj"; // knife correct ITKnife6 // cube knife knife3.obj";  // //  113.obj"; //  /  blade2.obj  cube.obj  kcube.obj  test cloth using this 
    std::string path2obj_c = DATA_ROOT_PATH"/VESS/ITKnife7_1.obj"; // knife correct ITKnife6  // cube knife knife3.obj";  //  //  113.obj";  // / VESS / TriangleTipKnife2.obj";  // blade2.obj  cube.obj kcube_s2.obj
    auto readMesh = imstk::MeshIO::read(path2obj_c); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);

    // Line mesh
    auto lineMesh = std::make_shared<imstk::LineMesh>();
    imstk::StdVectorOfVec3d vertList;
    vertList.resize(3);  // using 5 segment
    vertList[0] = imstk::Vec3d(-13, -2.25, 9.15); // should - volume mesh therold than surface mesh (9.0, -1.9, -0.6) AABB 0: 9.0, -2.23, -0.6   -0.7 ITKnife6.DAE  // knife3.obj 9.5, -0.9, 1.18
    vertList[1] = imstk::Vec3d(-5.93, -1.825, 9.125); // 9.7 ITKnife6.DAE // knife3.obj 9.5, -0.9, 8.73
    vertList[2] = imstk::Vec3d(1.14, -1.4, 9.1); // (9.0, -2.55, 9.5)  AABB 1: 9.0, -2.23, 9.6  9.7 ITKnife6.DAE // knife3.obj 9.5, -0.9, 8.73
    std::vector<std::vector<int> > connectivity;
    for (int i = 0; i < 2;){
        std::vector<int> line;
        line.push_back(i);
        i++;
        line.push_back(i);

        connectivity.push_back(line);
    }

    lineMesh->setInitialVertexPositions(vertList);
    lineMesh->setVertexPositions(vertList);
    lineMesh->setConnectivity(connectivity);

    auto visualMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(readMesh);
    auto physicsMesh = lineMesh; //visualMesh; //  lineMesh; //  imstk::MeshIO::read(path2obj_c);  //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
    auto collidingMesh = lineMesh; // visualMesh; //  lineMesh;
    Vec3d min1, max1;
    visualMesh->computeBoundingBox(min1, max1);

    auto material0 = std::make_shared<RenderMaterial>();
    auto diffuseTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto normalTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Normal.png", Texture::Type::NORMAL);
    auto roughnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Roughness.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material0->addTexture(diffuseTexture0);
    material0->addTexture(normalTexture0);
    material0->addTexture(roughnessTexture0);
    //material->setTexture(metalnessTexture);
    visualMesh->setRenderMaterial(material0);

    auto object = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject");

    auto objMapP2V = std::make_shared<imstk::OneToOneMap>();  // std::make_shared<imstk::TriangleLineMap>(); //  std::make_shared<imstk::OneToOneMap>();
    objMapP2V->setMaster(physicsMesh);
    objMapP2V->setSlave(visualMesh);
    objMapP2V->compute();


    auto objMapP2C = std::make_shared<imstk::OneToOneMap>();  //  colliding is physical mesh
    objMapP2C->setMaster(physicsMesh);
    objMapP2C->setSlave(collidingMesh);
    objMapP2C->compute();

    auto objMapC2V = std::make_shared<imstk::OneToOneMap>(); //  std::make_shared<imstk::TriangleLineMap>(); //  s std::make_shared<imstk::OneToOneMap>();
    objMapC2V->setMaster(collidingMesh);
    objMapC2V->setSlave(visualMesh);
    objMapC2V->compute();

    auto objMapC2P = std::make_shared<imstk::OneToOneMap>(); // colliding is physical mesh
    objMapC2P->setMaster(collidingMesh);
    objMapC2P->setSlave(physicsMesh);
    objMapC2P->compute();

    object->setVisualGeometry(visualMesh);
    object->setCollidingGeometry(collidingMesh);
    object->setPhysicsGeometry(physicsMesh);
    object->setPhysicsToCollidingMap(objMapP2C);
    object->setPhysicsToVisualMap(objMapP2V);
    object->setCollidingToVisualMap(objMapC2V);
    object->setColldingToPhysicsMap(objMapC2P);

    auto pbdModel = std::make_shared<PbdModel>();
    pbdModel->configure(/*Number of Constraints*/1,
        /*Constraint configuration*/"Distance 0.1",
        /*Mass*/0.0,
        /*Gravity*/"0 0 0",
        /*TimeStep*/0.0003,
        /*FixedPoint*/"",
        /*NumberOfIterationInConstraintSolver*/2, // 5
        /*Proximity*/0.42,  // use linemesh 0.3  // 0.01 use  //  cube 0.1
        /*Contact stiffness*/0.001);
    object->setDynamicalModel(pbdModel);

    //auto pbdSolver = std::make_shared<PbdSolver>();
    //pbdSolver->setPbdObject(object);
    //scene->addNonlinearSolver(pbdSolver);

    scene->addSceneObject(object);

    auto trackCtrl = std::make_shared<imstk::DeviceTracker>(client);
    trackCtrl->setTranslationScaling(0.15);  // 0.1  test 0.1 should use  Scale 0.001 will be better 
    auto controller = std::make_shared<imstk::PBDSceneObjectController>(object, trackCtrl);
    scene->addObjectController(controller);

    // Sphere1
    auto sphere1Obj = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "Sphere1", 0.42, Vec3d(-13, -2.25, 9.15)); // -2.23
    //auto materialTT = std::make_shared<Material>();
    //materialTT->setTexture(diffuseTexture0);
    //auto suMesh = std::dynamic_pointer_cast<SurfaceMesh>(sphere1Obj->getVisualGeometry());
    //suMesh->addMaterial(materialTT);
    auto sphere2Obj = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "Sphere2", 0.42, Vec3d(1.14, -1.4, 9.1)); // -2.23

    auto sphereOrg = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereOrg", 3, Vec3d(0, 0, 0));

    auto sphereX = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereX", 2.5, Vec3d(10, 0, 0));

    auto sphereY = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereY", 1.5, Vec3d(0, 10, 0));

    auto sphereZ = apiutils::createCollidingAnalyticalSceneObject(
        imstk::Geometry::Type::Sphere, scene, "SphereZ", 0.8, Vec3d(0, 0, 10));
    //scene->addSceneObject(sphere1Obj);
    //scene->addSceneObject(sphere2Obj);

    //auto trackCtrl1 = std::make_shared<imstk::DeviceTracker>(client);
    //trackCtrl1->setTranslationScaling(0.1);
    //auto controller1 = std::make_shared<imstk::SceneObjectController>(sphere1Obj, trackCtrl1);
    //scene->addObjectController(controller1);
    //auto controller2 = std::make_shared<imstk::SceneObjectController>(sphere2Obj, trackCtrl1);
    //scene->addObjectController(controller2);

    // Light (white)
    auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
    whiteLight->setPosition(imstk::Vec3d(-15, -1, 0));
    whiteLight->setFocalPoint(imstk::Vec3d(1, -1, 0));
    scene->addLight(whiteLight);
    auto whiteLight2 = std::make_shared<imstk::PointLight>("whiteLight2");
    whiteLight2->setPosition(imstk::Vec3d(1, -1, 0));
    whiteLight2->setFocalPoint(imstk::Vec3d(-15, -1, 0));
    scene->addLight(whiteLight2);

    // Light (red)
    auto colorLight = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight->setPosition(imstk::Vec3d(-3.7, 5.5, 7));
    colorLight->setFocalPoint(imstk::Vec3d(-25, -8, -4.5));
    colorLight->setColor(imstk::Color::White); // red
    colorLight->setSpotAngle(15);
    auto colorLight2 = std::make_shared<imstk::SpotLight>("colorLight2");
    colorLight2->setPosition(imstk::Vec3d(-25, -8, -4.5));
    colorLight2->setFocalPoint(imstk::Vec3d(-3.7, 5.5, 7));
    colorLight2->setColor(imstk::Color::White);
    colorLight2->setSpotAngle(15);
    scene->addLight(colorLight);
    scene->addLight(colorLight2);

    // Update Camera
    auto cam1 = scene->getCamera();
    cam1->setPosition(imstk::Vec3d(-25, -8, -4.5));
    cam1->setFocalPoint(imstk::Vec3d(-3.7, 5.5, 7));
    cam1->setViewAngle(75);

    //// Tetrahedral mesh
    imstk::StdVectorOfVec3d vertListTet;
    std::vector<imstk::TetrahedralMesh::TetraArray> tetConnectivity;
    //physXLink linkVolSur;
    std::vector<physXLink> linksVolSur;
    bool flag = loadTetFile(DATA_ROOT_PATH"/VESS/colonrectum_s002_SB.tet", vertListTet, tetConnectivity, linksVolSur); // colon_tumor3_3_SB for test   colon_tumor3_SB  cubeTest2_SB
    auto tetMesh = std::make_shared<imstk::VESSTetrahedralMesh>();
    tetMesh->setInitialVertexPositions(vertListTet);
    tetMesh->setVertexPositions(vertListTet);
    tetMesh->setTetrahedraVertices(tetConnectivity);
    tetMesh->setPhysXLink(linksVolSur);
    tetMesh->extractSurfaceMesh();


    // colon wall
    std::string pathColon = DATA_ROOT_PATH"/VESS/colonrectum_s002_SB.obj"; // colon_tumor3_SB cubeTest2_SB  colon_s022.dae"; //  colon_s02.dae"; //  colon_s12.dae"; // volume mesh for colliding // key==colonnew_seg4.dae  colonnew_seg0508.dae small segment// good segment  colonnew_seg4.dae   colonnew_seg3.obj colon_move2.DAE"; //  VESS / colonnew_seg3.obj"; // glove.dae"; //  colon_move2.DAE";    // colon_move2.DAE  colon1_part_max.DAE  colon1_part_max.DAE  colon6_max.DAEcloth.DAE  colon_1_c2_max  colon_cylinder2_tri_max
    std::string pathColon2 = DATA_ROOT_PATH"/VESS/colonrectum_s001.obj";  //  colon_s01
    std::string pathColon3 = DATA_ROOT_PATH"/VESS/colonrectum_s003.dae";  // colon_s03

    auto objMesh = imstk::MeshIO::read(pathColon);   // collidingColonMesh; //  imstk::MeshIO::read(pathColon);
    auto physicsColonMesh = tetMesh; //  collidingColonMesh; //  imstk::MeshIO::read(pathColon);

    auto objMesh2 = imstk::MeshIO::read(pathColon2);
    auto objMesh3 = imstk::MeshIO::read(pathColon3);
    auto visualColonMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh); // physicsColonMesh->getSurfaceMesh(); //  
    auto visualColonMesh2 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh2);
    auto visualColonMesh3 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh3);
    auto collidingColonMesh = physicsColonMesh; //  visualColonMesh; //  imstk::MeshIO::read(pathColon);

    auto material = std::make_shared<RenderMaterial>();
    auto diffuseTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    auto normalTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material->addTexture(diffuseTexture);
    material->addTexture(normalTexture);
    material->addTexture(roughnessTexture);
    //material->setTexture(metalnessTexture);
    visualColonMesh->setRenderMaterial(material);

    auto colonObj = std::make_shared<SceneObject>("Colon");
    colonObj->setVisualGeometry(visualColonMesh);
    scene->addSceneObject(colonObj);

    auto object4 = std::make_shared<imstk::VisualObject>("visualColonMesh4");
    auto material4 = std::make_shared<RenderMaterial>();
    material4->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME);
    auto tmp4 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto surfM = tetMesh->getSurfaceMesh();
    //material4->addTexture(tmp4);
    surfM->setRenderMaterial(material4);
    object4->setVisualGeometry(surfM); // change to any mesh created above
    scene->addSceneObject(object4);


    // Run
    sdk->setActiveScene(scene);
    sdk->startSimulation(true);
#endif
}

//void testViewer()
//{
//    // SDK and Scene
//    auto sdk = std::make_shared<imstk::SimulationManager>();
//    auto sceneTest = sdk->createNewScene("SceneTest");
//
//    // Plane
//    /*auto planeObj = apiutils::createVisualAnalyticalSceneObject(imstk::Geometry::Type::Plane, sceneTest, "VisualPlane", 10);
//
//    // Cube
//    auto cubeObj = apiutils::createVisualAnalyticalSceneObject(
//    imstk::Geometry::Type::Cube, sceneTest, "VisualCube", 0.5, Vec3d(1.0, -1.0, 0.5));
//    auto cubeGeom = cubeObj->getVisualGeometry();
//    // rotates could be replaced by cubeGeom->setOrientationAxis(1,1,1) (normalized inside)
//    cubeGeom->rotate(imstk::UP_VECTOR, imstk::PI_4, Geometry::TransformType::ApplyToData);
//    cubeGeom->rotate(imstk::RIGHT_VECTOR, imstk::PI_4, Geometry::TransformType::ApplyToData);
//
//    // Sphere
//    auto sphereObj = apiutils::createVisualAnalyticalSceneObject(
//    imstk::Geometry::Type::Sphere, sceneTest, "VisualSphere", 0.3, Vec3d(0, 2., 0));*/
//
//    // Light (white)
//    auto whiteLight = std::make_shared<imstk::Light>("whiteLight");
//    whiteLight->setPosition(imstk::Vec3d(5, 8, 5));
//    whiteLight->setPositional();
//    sceneTest->addLight(whiteLight);
//
//    // Light (red)
//    auto colorLight = std::make_shared<imstk::Light>("colorLight");
//    colorLight->setPosition(imstk::Vec3d(4, -3, 1));
//    colorLight->setFocalPoint(imstk::Vec3d(0, 0, 0));
//    colorLight->setColor(imstk::Color::Red);
//    colorLight->setPositional();
//    colorLight->setSpotAngle(15);
//
//    // Read surface mesh
//    auto objMesh = imstk::MeshIO::read(DATA_ROOT_PATH"/VESS/colon_new.obj"); //  "I:/iMSTK/resources/VESS/colonnew_seg3.obj"); //  colon_new2.dae");
//    auto surfaceMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh);
//
//    auto diffuseTexture = std::make_shared<Texture>("I:/iMSTK/resources/VESS/colon_texture_mat_Albedo2.png", Texture::Type::DIFFUSE);
//    auto normalTexture = std::make_shared<Texture>("I:/iMSTK/resources/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
//    auto metalTexture = std::make_shared<Texture>("I:/iMSTK/resources/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::METALNESS);
//    auto roughnessTexture = std::make_shared<Texture>("I:/iMSTK/resources/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
//    auto cubemapTexture = std::make_shared<Texture>("F:/assets/HDR cubemaps/irradiance1.dds", Texture::Type::IRRADIANCE_CUBEMAP);
//    auto material = std::make_shared<RenderMaterial>();
//    material->addTexture(diffuseTexture);
//    material->addTexture(normalTexture);
//    material->addTexture(roughnessTexture);
//    //material->setTexture(metalTexture);
//    //material->setTexture(cubemapTexture);
//    surfaceMesh->setRenderMaterial(material);
//
//    // Create object and add to scene
//    auto object = std::make_shared<imstk::VisualObject>("meshObject");
//    object->setVisualGeometry(surfaceMesh); // change to any mesh created above
//    sceneTest->addSceneObject(object);
//
//    // Add in scene
//    //sceneTest->addSceneObject(planeObj);
//    //sceneTest->addSceneObject(cubeObj);
//    //sceneTest->addSceneObject(sphereObj);
//    sceneTest->addLight(whiteLight);
//    //sceneTest->addLight(colorLight);
//
//    // Update Camera
//    auto cam1 = sceneTest->getCamera();
//    cam1->setPosition(imstk::Vec3d(-5.5, 2.5, 32));
//    cam1->setFocalPoint(imstk::Vec3d(1, 1, 0));
//    cam1->setViewAngle(75);
//
//    // Run
//    sdk->setActiveScene(sceneTest);
//    sdk->startSimulation(true);
//}

void testViewer()
{
    // SDK and Scene
    auto sdk = std::make_shared<imstk::SimulationManager>();
    auto sceneTest = sdk->createNewScene("SceneTest");

    // Plane
    /*auto planeObj = apiutils::createVisualAnalyticalSceneObject(imstk::Geometry::Type::Plane, sceneTest, "VisualPlane", 10);

    // Cube
    auto cubeObj = apiutils::createVisualAnalyticalSceneObject(
    imstk::Geometry::Type::Cube, sceneTest, "VisualCube", 0.5, Vec3d(1.0, -1.0, 0.5));
    auto cubeGeom = cubeObj->getVisualGeometry();
    // rotates could be replaced by cubeGeom->setOrientationAxis(1,1,1) (normalized inside)
    cubeGeom->rotate(imstk::UP_VECTOR, imstk::PI_4, Geometry::TransformType::ApplyToData);
    cubeGeom->rotate(imstk::RIGHT_VECTOR, imstk::PI_4, Geometry::TransformType::ApplyToData);

    // Sphere
    auto sphereObj = apiutils::createVisualAnalyticalSceneObject(
    imstk::Geometry::Type::Sphere, sceneTest, "VisualSphere", 0.3, Vec3d(0, 2., 0));*/

    //// Light (white)
    //auto whiteLight = std::make_shared<imstk::Light>("whiteLight");
    //whiteLight->setPosition(imstk::Vec3d(8, 1, -15));
    //whiteLight->setFocalPoint(imstk::Vec3d(8, 1, 15));
    //whiteLight->setPositional();
    //sceneTest->addLight(whiteLight);
    //auto whiteLight2 = std::make_shared<imstk::Light>("whiteLight2");
    //whiteLight2->setPosition(imstk::Vec3d(8, 1, -15));
    //whiteLight2->setFocalPoint(imstk::Vec3d(8, 1, 15));
    //whiteLight2->setPositional();
    //sceneTest->addLight(whiteLight2);

    // Light (white)
    auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
    whiteLight->setPosition(imstk::Vec3d(9, 0, 13));
    whiteLight->setFocalPoint(imstk::Vec3d(-8, -11, -15));
    sceneTest->addLight(whiteLight);
    //auto whiteLight2 = std::make_shared<imstk::Light>("whiteLight2");
    //whiteLight2->setPosition(imstk::Vec3d(-8, -11, -15));
    //whiteLight2->setFocalPoint(imstk::Vec3d(9, 0, 13));
    //whiteLight2->setPositional();
    //sceneTest->addLight(whiteLight2);

    // Light (red)
    auto colorLight = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight->setPosition(imstk::Vec3d(9, 0, 13));
    colorLight->setFocalPoint(imstk::Vec3d(8, -11, -15));
    colorLight->setColor(imstk::Color::White); // red
    colorLight->setSpotAngle(15);
    //auto colorLight2 = std::make_shared<imstk::Light>("colorLight2");
    //colorLight2->setPosition(imstk::Vec3d(-8, -11, -15));
    //colorLight2->setFocalPoint(imstk::Vec3d(9, 0, 13));
    //colorLight2->setColor(imstk::Color::LightGray);
    //colorLight2->setPositional();
    //colorLight2->setSpotAngle(15);
    sceneTest->addLight(colorLight);
    //sceneTest->addLight(colorLight2);


    // Read surface mesh
    auto objMesh = imstk::MeshIO::read(DATA_ROOT_PATH"/VESS/colon_new.obj");  //  colonnew_seg3.obj  colonnew_seg3.DAE colonnew_seg2  colon_new2.dae
    auto surfaceMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh);

    Vec3d min1, max1;
    surfaceMesh->computeBoundingBox(min1, max1);

    //FILE *outpointtri;
    //outpointtri = fopen("G:/AABB.txt", "w");
    //fprintf(outpointtri, "V: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    printf("V: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    auto diffuseTexture = std::make_shared<Texture>("I:/iMSTK/resources/VESS/colon_texture_mat_Albedo2.png", Texture::Type::DIFFUSE);
    auto normalTexture = std::make_shared<Texture>("I:/iMSTK/resources/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto metalTexture = std::make_shared<Texture>("I:/iMSTK/resources/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::METALNESS);
    auto roughnessTexture = std::make_shared<Texture>("I:/iMSTK/resources/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto cubemapTexture = std::make_shared<Texture>("F:/assets/HDR cubemaps/irradiance1.dds", Texture::Type::IRRADIANCE_CUBEMAP);
    auto material = std::make_shared<RenderMaterial>();
    material->addTexture(diffuseTexture);
    material->addTexture(normalTexture);
    material->addTexture(roughnessTexture);
    //material->setTexture(metalTexture);
    //material->setTexture(cubemapTexture);
    surfaceMesh->setRenderMaterial(material);

    // Create object and add to scene
    auto object = std::make_shared<imstk::VisualObject>("meshObject");
    object->setVisualGeometry(surfaceMesh); // change to any mesh created above
    sceneTest->addSceneObject(object);

    // Add in scene
    //sceneTest->addSceneObject(planeObj);
    //sceneTest->addSceneObject(cubeObj);
    //sceneTest->addSceneObject(sphereObj);
    //sceneTest->addLight(whiteLight);
    //sceneTest->addLight(colorLight);

    // Update Camera
    auto cam1 = sceneTest->getCamera();
    cam1->setPosition(imstk::Vec3d(9, 0, 13));
    cam1->setFocalPoint(imstk::Vec3d(-8, -11, -15));
    cam1->setViewAngle(75);

    // Run
    sdk->setActiveScene(sceneTest);
    sdk->startSimulation(true);
}

void testPbdVirtualCoupling7()
{
#ifdef iMSTK_USE_OPENHAPTICS
    // SDK and Scene
    auto sdk = std::make_shared<imstk::SimulationManager>();
    auto scene = sdk->createNewScene("SceneTestDevice");

    // Device Client
    auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

    // Device Server
    auto server = std::make_shared<imstk::HDAPIDeviceServer>();
    server->addDeviceClient(client);
    sdk->addModule(server);

    /*std::string path2objtest = DATA_ROOT_PATH"/test.dae";
    auto testMesh = imstk::MeshIO::read(path2objtest);*/

    std::string path2obj = DATA_ROOT_PATH"/VESS/knife3.obj"; //  ITKnife6.DAE" 6尺寸正好 ; // // 113.obj"; //  /  blade2.obj  cube.obj  kcube.obj  test cloth using this 
    std::string path2obj_c = DATA_ROOT_PATH"/VESS/knife3.obj"; // ITKnife6.DAE";  // knife3.obj";  // 113.obj";  // / VESS / TriangleTipKnife2.obj";  // blade2.obj  cube.obj kcube_s2.obj
    auto collidingMesh = imstk::MeshIO::read(path2obj_c); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
    //auto visualMesh = collidingMesh; // imstk::MeshIO::read(path2obj);
    auto physicsMesh = collidingMesh; //  imstk::MeshIO::read(path2obj_c);  //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
    //objmesh->translate(-270, 0, 70);
    //auto geom = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objmesh);
    //auto viusalMesh = imstk::MeshIO::read(path2obj);


    auto visualMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(collidingMesh);
    Vec3d min1, max1;
    visualMesh->computeBoundingBox(min1, max1);

    auto material0 = std::make_shared<RenderMaterial>();
    auto diffuseTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_BaseColor.png", Texture::Type::DIFFUSE);
    auto normalTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Normal.png", Texture::Type::NORMAL);
    auto roughnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/OpaqueTools_Roughness.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture0 = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material0->addTexture(diffuseTexture0);
    material0->addTexture(normalTexture0);
    material0->addTexture(roughnessTexture0);
    //material->setTexture(metalnessTexture);
    visualMesh->setRenderMaterial(material0);

    FILE *outpointtri;
    outpointtri = fopen("G:/AABB.txt", "w");
    fprintf(outpointtri, "V: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

    collidingMesh->computeBoundingBox(min1, max1);
    fprintf(outpointtri, "C: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    fclose(outpointtri);

    /*imstk::StdVectorOfVec3d vertListLine;
    vertListLine.resize(2);
    vertListLine[0] = Vec3d(0.0, 0.0, 0.0);
    vertListLine[1] = Vec3d(0.0, 40, 0.0);
    std::vector<std::vector<int> > connectivityLine;
    for (int i = 0; i < 1;){
    std::vector<int> line;
    line.push_back(i);
    i++;
    line.push_back(i);

    connectivityLine.push_back(line);
    }*/

    //collidingMesh->initialize(vertListLine);
    //collidingMesh->setConnectivity(connectivityLine);

    //physicsMesh->initialize(vertListLine);
    //physicsMesh->setConnectivity(connectivityLine);

    auto object = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject");

    auto objMapP2V = std::make_shared<imstk::OneToOneMap>();
    objMapP2V->setMaster(physicsMesh);
    objMapP2V->setSlave(visualMesh);
    objMapP2V->compute();


    auto objMapP2C = std::make_shared<imstk::OneToOneMap>();
    objMapP2C->setMaster(physicsMesh);
    objMapP2C->setSlave(collidingMesh);
    objMapP2C->compute();

    auto objMapC2V = std::make_shared<imstk::OneToOneMap>();
    objMapC2V->setMaster(collidingMesh);
    objMapC2V->setSlave(visualMesh);
    objMapC2V->compute();

    auto objMapC2P = std::make_shared<imstk::OneToOneMap>();
    objMapC2P->setMaster(collidingMesh);
    objMapC2P->setSlave(physicsMesh);
    objMapC2P->compute();

    object->setVisualGeometry(visualMesh);
    object->setCollidingGeometry(collidingMesh);
    object->setPhysicsGeometry(physicsMesh);
    object->setPhysicsToCollidingMap(objMapP2C);
    object->setPhysicsToVisualMap(objMapP2V);
    object->setCollidingToVisualMap(objMapC2V);
    object->setColldingToPhysicsMap(objMapC2P);

    auto pbdModel = std::make_shared<PbdModel>();
    pbdModel->configure(/*Number of Constraints*/1,
        /*Constraint configuration*/"Distance 0.1",
        /*Mass*/0.0,
        /*Gravity*/"0 0 0",
        /*TimeStep*/0.0003,
        /*FixedPoint*/"",
        /*NumberOfIterationInConstraintSolver*/2, // 5
        /*Proximity*/0.01,  // 0.1
        /*Contact stiffness*/0.001);
    object->setDynamicalModel(pbdModel);

    //auto pbdSolver = std::make_shared<PbdSolver>();
    //pbdSolver->setPbdObject(object);
    //scene->addNonlinearSolver(pbdSolver);

    scene->addSceneObject(object);

    auto trackCtrl = std::make_shared<imstk::DeviceTracker>(client);
    trackCtrl->setTranslationScaling(0.001);  // Scale 0.001
    auto controller = std::make_shared<imstk::PBDSceneObjectController>(object, trackCtrl);
    scene->addObjectController(controller);

    // Light (white)
    auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
    whiteLight->setPosition(imstk::Vec3d(8, 1, -15));
    whiteLight->setFocalPoint(imstk::Vec3d(8, 1, 15));
    scene->addLight(whiteLight);
    auto whiteLight2 = std::make_shared<imstk::PointLight>("whiteLight2");
    whiteLight2->setPosition(imstk::Vec3d(8, 1, -15));
    whiteLight2->setFocalPoint(imstk::Vec3d(8, 1, 15));
    scene->addLight(whiteLight2);

    // Light (red)
    auto colorLight = std::make_shared<imstk::SpotLight>("colorLight");
    colorLight->setPosition(imstk::Vec3d(9, 0, 13));
    colorLight->setFocalPoint(imstk::Vec3d(-8, -11, -15));
    colorLight->setColor(imstk::Color::White); // red
    colorLight->setSpotAngle(15);
    auto colorLight2 = std::make_shared<imstk::SpotLight>("colorLight2");
    colorLight2->setPosition(imstk::Vec3d(-8, -11, -15));
    colorLight2->setFocalPoint(imstk::Vec3d(9, 0, 13));
    colorLight2->setColor(imstk::Color::White);
    colorLight2->setSpotAngle(15);
    scene->addLight(colorLight);
    scene->addLight(colorLight2);

    // Update Camera
    auto cam1 = scene->getCamera();
    cam1->setPosition(imstk::Vec3d(9, 0, 13));
    cam1->setFocalPoint(imstk::Vec3d(-8, -11, -15));
    cam1->setViewAngle(75);


    // colon wall
    std::string pathColon = DATA_ROOT_PATH"/VESS/colonnew_seg4.dae"; //    colonnew_seg3.obj colon_move2.DAE"; //  VESS / colonnew_seg3.obj"; // glove.dae"; //  colon_move2.DAE";    // colon_move2.DAE  colon1_part_max.DAE  colon1_part_max.DAE  colon6_max.DAEcloth.DAE  colon_1_c2_max  colon_cylinder2_tri_max
    auto collidingColonMesh = imstk::MeshIO::read(pathColon);
    auto objMesh = collidingColonMesh; //  imstk::MeshIO::read(pathColon);
    auto physicsColonMesh = collidingColonMesh; //  imstk::MeshIO::read(pathColon);

    auto visualColonMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh);
    auto material = std::make_shared<RenderMaterial>();
    auto diffuseTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE);
    auto normalTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);
    auto roughnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colontex5r.png", Texture::Type::METALNESS);
    material->addTexture(diffuseTexture);
    material->addTexture(normalTexture);
    material->addTexture(roughnessTexture);
    material->setDisplayMode(RenderMaterial::DisplayMode::WIREFRAME);
    //material->setTexture(metalnessTexture);
    visualColonMesh->setRenderMaterial(material);

    visualColonMesh->computeBoundingBox(min1, max1);
    outpointtri = fopen("G:/AABB.txt", "a");
    fprintf(outpointtri, "V Colon: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
    // floor

    imstk::StdVectorOfVec3d vertList;
    double width = 60.0;
    double height = 60.0;
    int nRows = 11;
    int nCols = 11;
    int corner[4] = { 1, nRows, nRows*nCols - nCols + 1, nRows*nCols };
    char intStr[33];
    std::string fixed_corner;
    //for (unsigned int i = 0; i < 4; ++i)
    //{
    //	std::sprintf(intStr, "%d", corner[i]);
    //	fixed_corner += std::string(intStr) + ' ';
    //}

    //fixed point
    Vec3d tmpPos;
    for (int i = 0; i < visualColonMesh->getNumVertices(); ++i)
    {
        tmpPos = visualColonMesh->getVertexPosition(i);
        if ((tmpPos.z() <-12) || (tmpPos.z() > 12))  // <-1.3  >1.6  smoothed colon segment  (tmpPos.x() < -1.5) || (tmpPos.x() > 1.6)
        {
            std::sprintf(intStr, "%d", i);
            fixed_corner += std::string(intStr) + ' ';
        }
    }

    fprintf(outpointtri, "V_Colon: Fixed point %s \n ", fixed_corner.c_str());
    fclose(outpointtri);

    // 
    vertList.resize(nRows*nCols);
    const double dy = width / (double)(nCols - 1);
    const double dx = height / (double)(nRows - 1);
    for (int i = 0; i < nRows; ++i)
    {
        for (int j = 0; j < nCols; j++)
        {
            const double y = (double)dy*j;
            const double x = (double)dx*i;
            vertList[i*nCols + j] = Vec3d(x - 50, 0.0, y - 50);

        }
    }

    // c. Add connectivity data
    std::vector<imstk::SurfaceMesh::TriangleArray> triangles;
    for (std::size_t i = 0; i < nRows - 1; ++i)
    {
        for (std::size_t j = 0; j < nCols - 1; j++)
        {
            imstk::SurfaceMesh::TriangleArray tri[2];
            tri[0] = { { i*nCols + j, i*nCols + j + 1, (i + 1)*nCols + j } };
            tri[1] = { { (i + 1)*nCols + j + 1, (i + 1)*nCols + j, i*nCols + j + 1 } };
            triangles.push_back(tri[0]);
            triangles.push_back(tri[1]);
        }
    }
    //auto collidingColonMesh = std::make_shared<imstk::SurfaceMesh>();
    //collidingColonMesh->initialize(vertList, triangles);
    //auto visualColonMesh = std::make_shared<imstk::SurfaceMesh>();
    //visualColonMesh->initialize(vertList, triangles);
    //auto physicsColonMesh = std::make_shared<imstk::SurfaceMesh>();
    //physicsColonMesh->initialize(vertList, triangles);


    auto colonMapP2V = std::make_shared<imstk::OneToOneMap>();
    colonMapP2V->setMaster(physicsColonMesh);
    colonMapP2V->setSlave(visualColonMesh);
    colonMapP2V->compute();


    auto colonMapP2C = std::make_shared<imstk::OneToOneMap>();
    colonMapP2C->setMaster(physicsColonMesh);
    colonMapP2C->setSlave(collidingColonMesh);
    colonMapP2C->compute();

    auto colonMapC2V = std::make_shared<imstk::OneToOneMap>();
    colonMapC2V->setMaster(collidingColonMesh);
    colonMapC2V->setSlave(visualColonMesh);
    colonMapC2V->compute();

    auto colonObj = std::make_shared<PbdObject>("Floor");
    colonObj->setCollidingGeometry(collidingColonMesh);
    colonObj->setVisualGeometry(visualColonMesh);
    colonObj->setPhysicsGeometry(physicsColonMesh);
    colonObj->setPhysicsToCollidingMap(colonMapP2C);
    colonObj->setPhysicsToVisualMap(colonMapP2V);
    colonObj->setCollidingToVisualMap(colonMapC2V);

    auto pbdColonModel = std::make_shared<PbdModel>();
    pbdColonModel->configure(/*Number of Constraints*/2,
        /*Constraint configuration*/"Dihedral 0.8", // 0.01
        /*Constraint configuration*/"Distance 0.8",
        /*Mass*/0.1,
        /*Gravity*/"0 0 0",  // "0 -9.8 0",
        /*TimeStep*/0.001, // 0.001
        /*FixedPoint*/fixed_corner.c_str(),
        /*NumberOfIterationInConstraintSolver*/3, // 12
        /*Proximity*/0.02,  // 0.1
        /*Contact stiffness*/0.1);
    colonObj->setDynamicalModel(pbdColonModel);

    auto pbdColonSolver = std::make_shared<PbdSolver>();
    pbdColonSolver->setPbdObject(colonObj);
    scene->addNonlinearSolver(pbdColonSolver);

    scene->addSceneObject(colonObj);

    // Collisions
    auto colGraph = scene->getCollisionGraph();
    auto pair = std::make_shared<PbdInteractionPair>(PbdInteractionPair(object, colonObj));
    pair->setNumberOfInterations(2);

    colGraph->addInteractionPair(pair);

    // Run
    sdk->setActiveScene(scene);
    sdk->startSimulation(true);
#endif
}

void testVolumeMeshCutting()
{
    //auto sdk = std::make_shared<SimulationManager>();
    //auto scene = sdk->createNewScene("PositionBasedDynamicsTest");
    //scene->getCamera()->setPosition(0, 2.0, 15.0);

    //// b. Load a tetrahedral mesh
    //auto tetMesh = imstk::MeshIO::read(iMSTK_DATA_ROOT"/asianDragon/asianDragon.veg");
    //if (!tetMesh)
    //{
    //    LOG(WARNING) << "Could not read mesh from file.";
    //    return;
    //}

    //// c. Extract the surface mesh
    //auto surfMesh = std::make_shared<imstk::SurfaceMesh>();
    //auto volTetMesh = std::dynamic_pointer_cast<imstk::TetrahedralMesh>(tetMesh);
    //if (!volTetMesh)
    //{
    //    LOG(WARNING) << "Dynamic pointer cast from imstk::Mesh to imstk::TetrahedralMesh failed!";
    //    return;
    //}
    //volTetMesh->extractSurfaceMesh(surfMesh);

    ////auto temp = std::make_shared<imstk::VolMesh>();
    //auto temp = std::static_pointer_cast<imstk::VolMesh>(volTetMesh); //  new VolMesh(volTetMesh);
    ////temp->setFlagFilterOutFlatCells(false);
    //temp->parseMesh();
    ////temp->setVerbose(g_parser.value_to_int("verbose"));

    //sdk->setActiveScene(scene);
    //sdk->startSimulation(true);
}

void testVTKVESSViewer()
{
	// SDK and Scene
	auto sdk = std::make_shared<imstk::SimulationManager>();
	auto scene = sdk->createNewScene("SceneTestDevice");

	// Device clients
	auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");
	//auto client1 = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 2");

	// Device Server
	auto server = std::make_shared<imstk::HDAPIDeviceServer>();
	server->addDeviceClient(client);
	//server->addDeviceClient(client1);
	sdk->addModule(server);

	// Update Camera position
	auto cam = scene->getCamera();
	cam->setPosition(imstk::Vec3d(-11, -1.5, 5.5));  //  put it in the middle of colon model  colon (-10, -1.5, 6)  cube (-15, 20, -5)
	cam->setFocalPoint(imstk::Vec3d(-15, -1.5, 5.5)); // colon (-14, -1.5, 6)  cube (-19, 20, -5)
	cam->setViewUp(0, 1, 0);

	// Set camera controller
	//std::shared_ptr<ESDCameraController> 
    auto camController = std::make_shared<CameraController>(*cam.get(), client);
	camController->setTranslationScaling(0.2);
	//camController->setCollisionMesh(surfaceMeshCD);
	//LOG(INFO) << camController->getTranslationOffset(); // should be the same than initial cam position
	//camController->setInversionFlags(imstk::CameraController::InvertFlag::rotY |
	// imstk::CameraController::InvertFlag::rotZ);

	// Light (white)
	auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
	whiteLight->setPosition(imstk::Vec3d(-11, -1.5, 5.5)); // colon  (-10, -1.5, 6));   cube (-15, 20, -5)
	whiteLight->setFocalPoint(imstk::Vec3d(-15, -1.5, 5.5)); // colon (-14, -1.5, 6));  (-19, 20, -5)
    auto whiteLight2 = std::make_shared<imstk::PointLight>("whiteLight");
    whiteLight2->setPosition(imstk::Vec3d(-15, -1.5, 5.5)); // colon  (-10, -1.5, 6));   cube (-15, 20, -5)
    whiteLight2->setFocalPoint(imstk::Vec3d(-11, -1.5, 5.5)); // colon (-14, -1.5, 6));  (-19, 20, -5)

	// Light (red)
	auto colorLight = std::make_shared<imstk::SpotLight>("colorLight");
	colorLight->setPosition(imstk::Vec3d(5.2361 ,   7.9564 ,   3.1235));
	colorLight->setFocalPoint(imstk::Vec3d(1.2361  ,  7.9564 ,   3.1235));
	colorLight->setColor(imstk::Color::Red);
	colorLight->setSpotAngle(15);
	scene->addLight(whiteLight);
    scene->addLight(whiteLight2);
	scene->addLight(colorLight); // not add red right

	auto sphere0Geom = std::make_shared<imstk::Sphere>();
	sphere0Geom->setPosition(imstk::Vec3d(-11, -1.5, 5.5)); // colon  (-10, -1.5, 6));  -10, -1.5, 6  cube -15, 20, -5
	sphere0Geom->setRadius(0.2);
	//sphere0Geom->scale(0);

	auto sphere0Obj = std::make_shared<imstk::CollidingObject>("Sphere0");
	sphere0Obj->setVisualGeometry(sphere0Geom);
	sphere0Obj->setCollidingGeometry(sphere0Geom);
	//scene->addSceneObject(sphere0Obj);

	auto sphereStartGeom = std::make_shared<imstk::Sphere>();
	sphereStartGeom->setPosition(imstk::Vec3d(-13, -2.0, 6.0)); // colon  (-10, -1.5, 6));  -10, -1.5, 6  cube -15, 20, -5
	sphereStartGeom->setRadius(0.2);

	auto sphereStartObj = std::make_shared<imstk::CollidingObject>("SphereStart");
	sphereStartObj->setVisualGeometry(sphereStartGeom);
	sphereStartObj->setCollidingGeometry(sphereStartGeom);
	scene->addSceneObject(sphereStartObj);

	//auto trackCtrl0 = std::make_shared<imstk::DeviceTracker>(client);
	//trackCtrl0->setTranslationScaling(0.05);
	//auto sphere0Controller = std::make_shared<imstk::SceneObjectController>(sphere0Obj, trackCtrl0);
	//scene->addObjectController(sphere0Controller);

	// Read surface mesh
	auto objMesh = imstk::MeshIO::read(DATA_ROOT_PATH"/VESS/colon_new.dae"); //  colon_new.dae  colon4.dae cube_den.dae rect_den.dae colon3_c2 coarse mesh colon3.dae colon dense mesh
	auto surfaceMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh);
	//surfaceMesh->setScaling(5);
	auto material = std::make_shared<RenderMaterial>();
    auto diffuseTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo.png", Texture::Type::DIFFUSE); //  auto diffuseTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/textures/colontex5.png", Texture::Type::DIFFUSE); // colon_texture_mat_Albedo.png
    auto normalTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_N.png", Texture::Type::NORMAL);  // textures/colontex5n.png
    auto roughnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/VESS/colon_texture_mat_Albedo_R.png", Texture::Type::ROUGHNESS); // textures/colontex5r.png
    auto metalnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/textures/colontex5r.png", Texture::Type::METALNESS);
    material->addTexture(diffuseTexture);
    material->addTexture(normalTexture);
    material->addTexture(roughnessTexture);
    //material->setTexture(metalnessTexture);
    surfaceMesh->setRenderMaterial(material);

	//auto objMesh2 = imstk::MeshIO::read(DATA_ROOT_PATH"/colon_s1.dae"); // cube.dae rect.dae colon3_c2 coarse mesh colon3_c2.dae// colon coarse mesh
	//auto surfaceMesh2 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh2);

	auto objectColon = std::make_shared<imstk::CollidingObject>("Colon");
	objectColon->setVisualGeometry(surfaceMesh);
	//objectColon->setCollidingGeometry(surfaceMesh2);
	scene->addSceneObject(objectColon);

	auto controller0 = std::make_shared<imstk::SceneObjectController>(sphere0Obj, camController);  // SceneObjectController
	scene->addObjectController(controller0);

	// Run
	sdk->setActiveScene(scene);
	sdk->startSimulation(true);
}
// 
void testPbdVisutralCouplingObject2()
{
#ifdef iMSTK_USE_OPENHAPTICS
	// SDK and Scene
	auto sdk = std::make_shared<imstk::SimulationManager>();
	auto scene = sdk->createNewScene("SceneTestDevice");

	// Device Client
	auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

	// Device Server
	auto server = std::make_shared<imstk::HDAPIDeviceServer>();
	server->addDeviceClient(client);
	sdk->addModule(server);

	//auto trackingCtrl = std::make_shared<imstk::DeviceTracker>(client0);
	////trackingCtrl->setTranslationScaling(100);
	//auto lapToolController = std::make_shared<imstk::SceneObjectController>(pbdObj, trackingCtrl);
	///*auto lapToolController = std::make_shared<imstk::LaparoscopicToolController>(pivot, upperJaw, lowerJaw, trackingCtrl);
	//lapToolController->setJawRotationAxis(imstk::Vec3d(1.0, 0, 0));*/
	//scene->addObjectController(lapToolController);

	// Object
	//auto geom = std::make_shared<imstk::Cube>();
	//geom->setPosition(imstk::UP_VECTOR);
	//geom->scale(2);

	std::string path2obj = DATA_ROOT_PATH"/knife3.obj";  // blade2.obj  cube.obj
	auto collidingMesh = imstk::MeshIO::read(path2obj);
	auto visualMesh = imstk::MeshIO::read(path2obj);
	auto physicsMesh = imstk::MeshIO::read(path2obj);
	//objmesh->translate(-270, 0, 70);
	//auto geom = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objmesh);
	//auto viusalMesh = imstk::MeshIO::read(path2obj);
	//Vec3d min1, max1;
	//geom->computeBoundingBox(min1, max1);

	//FILE *outpointtri;
	//outpointtri = fopen("G:/AABB.txt", "w");
	//fprintf(outpointtri, "AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
	//fclose(outpointtri);

	auto object = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject");

	auto objMapP2V = std::make_shared<imstk::OneToOneMap>();
	objMapP2V->setMaster(physicsMesh);
	objMapP2V->setSlave(visualMesh);
	objMapP2V->compute();


	auto objMapP2C = std::make_shared<imstk::OneToOneMap>();
	objMapP2C->setMaster(physicsMesh);
	objMapP2C->setSlave(collidingMesh);
	objMapP2C->compute();

	auto objMapC2V = std::make_shared<imstk::OneToOneMap>();
	objMapC2V->setMaster(collidingMesh);
	objMapC2V->setSlave(visualMesh);
	objMapC2V->compute();

	auto objMapC2P = std::make_shared<imstk::OneToOneMap>();
	objMapC2P->setMaster(collidingMesh);
	objMapC2P->setSlave(physicsMesh);
	objMapC2P->compute();

	object->setVisualGeometry(visualMesh);
	object->setCollidingGeometry(collidingMesh);
	object->setPhysicsGeometry(physicsMesh);
	object->setPhysicsToCollidingMap(objMapP2C);
	object->setPhysicsToVisualMap(objMapP2V);
	object->setCollidingToVisualMap(objMapC2V);
	object->setColldingToPhysicsMap(objMapC2P);

	auto pbdModel = std::make_shared<PbdModel>();
    pbdModel->configure(/*Number of Constraints*/1,
		/*Constraint configuration*/"Distance 0.1",
		/*Mass*/0.0,
		/*Gravity*/"0 0 0",
		/*TimeStep*/0.001,
		/*FixedPoint*/"",
		/*NumberOfIterationInConstraintSolver*/5,
		/*Proximity*/0.1,
		/*Contact stiffness*/0.01);
    object->setDynamicalModel(pbdModel);

	//auto pbdSolver = std::make_shared<PbdSolver>();
	//pbdSolver->setPbdObject(object);
	//scene->addNonlinearSolver(pbdSolver);

	scene->addSceneObject(object);

	auto trackCtrl = std::make_shared<imstk::DeviceTracker>(client);
	trackCtrl->setTranslationScaling(0.5);
	auto controller = std::make_shared<imstk::PBDSceneObjectController>(object, trackCtrl);
	scene->addObjectController(controller);

	// Update Camera position
	auto cam = scene->getCamera();
	cam->setPosition(imstk::Vec3d(0, 10, -150));
	cam->setFocalPoint(imstk::Vec3d(0, -40, 0));
	cam->setViewAngle(60);

	// Run
	sdk->setActiveScene(scene);
	sdk->startSimulation(true);
#endif
}
// this example can run normally moving with device
void testPbdVisutralCouplingObject()
{
#ifdef iMSTK_USE_OPENHAPTICS
	// SDK and Scene
	auto sdk = std::make_shared<imstk::SimulationManager>();
	auto scene = sdk->createNewScene("SceneTestDevice");

	// Device Client
	auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

	// Device Server
	auto server = std::make_shared<imstk::HDAPIDeviceServer>();
	server->addDeviceClient(client);
	sdk->addModule(server);

	//auto trackingCtrl = std::make_shared<imstk::DeviceTracker>(client0);
	////trackingCtrl->setTranslationScaling(100);
	//auto lapToolController = std::make_shared<imstk::SceneObjectController>(pbdObj, trackingCtrl);
	///*auto lapToolController = std::make_shared<imstk::LaparoscopicToolController>(pivot, upperJaw, lowerJaw, trackingCtrl);
	//lapToolController->setJawRotationAxis(imstk::Vec3d(1.0, 0, 0));*/
	//scene->addObjectController(lapToolController);

	// Object
	//auto geom = std::make_shared<imstk::Cube>();
	//geom->setPosition(imstk::UP_VECTOR);
	//geom->scale(2);

	std::string path2obj = DATA_ROOT_PATH"/knife3.obj";  // blade2.obj  cube.obj
	auto collidingMesh = imstk::MeshIO::read(path2obj);
	auto visualMesh = imstk::MeshIO::read(path2obj);
	auto physicsMesh = imstk::MeshIO::read(path2obj);
	//objmesh->translate(-270, 0, 70);
	//auto geom = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objmesh);
	//auto viusalMesh = imstk::MeshIO::read(path2obj);
	//Vec3d min1, max1;
	//geom->computeBoundingBox(min1, max1);

	//FILE *outpointtri;
	//outpointtri = fopen("G:/AABB.txt", "w");
	//fprintf(outpointtri, "AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
	//fclose(outpointtri);

	auto object = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject");

	/*auto objMapP2V = std::make_shared<imstk::OneToOneMap>();
	objMapP2V->setMaster(physicsMesh);
	objMapP2V->setSlave(visualMesh);
	objMapP2V->compute();


	auto objMapP2C = std::make_shared<imstk::OneToOneMap>();
	objMapP2C->setMaster(physicsMesh);
	objMapP2C->setSlave(collidingMesh);
	objMapP2C->compute();

	auto objMapC2V = std::make_shared<imstk::OneToOneMap>();
	objMapC2V->setMaster(collidingMesh);
	objMapC2V->setSlave(visualMesh);
	objMapC2V->compute();

	auto objMapC2P = std::make_shared<imstk::OneToOneMap>();
	objMapC2P->setMaster(collidingMesh);
	objMapC2P->setSlave(physicsMesh);
	objMapC2P->compute();*/

	object->setVisualGeometry(visualMesh);
	object->setCollidingGeometry(collidingMesh);
	object->setPhysicsGeometry(physicsMesh);
	//object->setPhysicsToCollidingMap(objMapP2C);
	//object->setPhysicsToVisualMap(objMapP2V);
	//object->setCollidingToVisualMap(objMapC2V);
	//object->setColldingToPhysicsMap(objMapC2P);

	auto pbdModel = std::make_shared<PbdModel>();
    pbdModel->configure(/*Number of Constraints*/1,
		/*Constraint configuration*/"Distance 0.1",
		/*Mass*/0.0,
		/*Gravity*/"0 0 0",
		/*TimeStep*/0.001,
		/*FixedPoint*/"",
		/*NumberOfIterationInConstraintSolver*/5,
		/*Proximity*/0.1,
		/*Contact stiffness*/0.01);
    object->setDynamicalModel(pbdModel);

	//auto pbdSolver = std::make_shared<PbdSolver>();
	//pbdSolver->setPbdObject(object);
	//scene->addNonlinearSolver(pbdSolver);

	scene->addSceneObject(object);

	auto trackCtrl = std::make_shared<imstk::DeviceTracker>(client);
	trackCtrl->setTranslationScaling(0.5);
	auto controller = std::make_shared<imstk::PBDSceneObjectController>(object, trackCtrl);
	scene->addObjectController(controller);

	// Update Camera position
	auto cam = scene->getCamera();
	cam->setPosition(imstk::Vec3d(0, 10, -150));
	cam->setFocalPoint(imstk::Vec3d(0, -40, 0));
	cam->setViewAngle(60);

	// Run
	sdk->setActiveScene(scene);
	sdk->startSimulation(true);
#endif
}

// test different angle view 
void testPbdVirtualCoupling6()
{
#ifdef iMSTK_USE_OPENHAPTICS
	// SDK and Scene
	auto sdk = std::make_shared<imstk::SimulationManager>();
	auto scene = sdk->createNewScene("SceneTestDevice");

	// Device Client
	auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

	// Device Server
	auto server = std::make_shared<imstk::HDAPIDeviceServer>();
	server->addDeviceClient(client);
	sdk->addModule(server);

    /*std::string path2objtest = DATA_ROOT_PATH"/test.dae";
    auto testMesh = imstk::MeshIO::read(path2objtest);*/

	std::string path2obj = DATA_ROOT_PATH"/113.obj";  // blade2.obj  cube.obj  kcube.obj  test cloth using this 
	std::string path2obj_c = DATA_ROOT_PATH"/113.obj";  // blade2.obj  cube.obj kcube_s2.obj
	auto collidingMesh = imstk::MeshIO::read(path2obj_c); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
	auto visualMesh = collidingMesh; // imstk::MeshIO::read(path2obj);
	auto physicsMesh = collidingMesh; //  imstk::MeshIO::read(path2obj_c);  //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
	//objmesh->translate(-270, 0, 70);
	//auto geom = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objmesh);
	//auto viusalMesh = imstk::MeshIO::read(path2obj);
	Vec3d min1, max1;
	visualMesh->computeBoundingBox(min1, max1);

	FILE *outpointtri;
	outpointtri = fopen("G:/AABB.txt", "w");
	fprintf(outpointtri, "V: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

	collidingMesh->computeBoundingBox(min1, max1);
	fprintf(outpointtri, "C: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
	fclose(outpointtri);

	/*imstk::StdVectorOfVec3d vertListLine;
	vertListLine.resize(2);
	vertListLine[0] = Vec3d(0.0, 0.0, 0.0);
	vertListLine[1] = Vec3d(0.0, 40, 0.0);
	std::vector<std::vector<int> > connectivityLine;
	for (int i = 0; i < 1;){
	std::vector<int> line;
	line.push_back(i);
	i++;
	line.push_back(i);

	connectivityLine.push_back(line);
	}*/

	//collidingMesh->initialize(vertListLine);
	//collidingMesh->setConnectivity(connectivityLine);

	//physicsMesh->initialize(vertListLine);
	//physicsMesh->setConnectivity(connectivityLine);

	auto object = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject");

	auto objMapP2V = std::make_shared<imstk::OneToOneMap>();
	objMapP2V->setMaster(physicsMesh);
	objMapP2V->setSlave(visualMesh);
	objMapP2V->compute();


	auto objMapP2C = std::make_shared<imstk::OneToOneMap>();
	objMapP2C->setMaster(physicsMesh);
	objMapP2C->setSlave(collidingMesh);
	objMapP2C->compute();

	auto objMapC2V = std::make_shared<imstk::OneToOneMap>();
	objMapC2V->setMaster(collidingMesh);
	objMapC2V->setSlave(visualMesh);
	objMapC2V->compute();

	auto objMapC2P = std::make_shared<imstk::OneToOneMap>();
	objMapC2P->setMaster(collidingMesh);
	objMapC2P->setSlave(physicsMesh);
	objMapC2P->compute();

	object->setVisualGeometry(visualMesh);
	object->setCollidingGeometry(collidingMesh);
	object->setPhysicsGeometry(physicsMesh);
	object->setPhysicsToCollidingMap(objMapP2C);
	object->setPhysicsToVisualMap(objMapP2V);
	object->setCollidingToVisualMap(objMapC2V);
	object->setColldingToPhysicsMap(objMapC2P);

	auto pbdModel = std::make_shared<PbdModel>();
    pbdModel->configure(/*Number of Constraints*/1,
		/*Constraint configuration*/"Distance 0.1",
		/*Mass*/0.0,
		/*Gravity*/"0 0 0",
		/*TimeStep*/0.001,
		/*FixedPoint*/"",
		/*NumberOfIterationInConstraintSolver*/5,
		/*Proximity*/0.01,  // 0.1
		/*Contact stiffness*/0.001);
    object->setDynamicalModel(pbdModel);

	//auto pbdSolver = std::make_shared<PbdSolver>();
	//pbdSolver->setPbdObject(object);
	//scene->addNonlinearSolver(pbdSolver);

	scene->addSceneObject(object);

	auto trackCtrl = std::make_shared<imstk::DeviceTracker>(client);
	trackCtrl->setTranslationScaling(0.02);  // Scale 0.001
	auto controller = std::make_shared<imstk::PBDSceneObjectController>(object, trackCtrl);
	scene->addObjectController(controller);

	// Update Camera position
	auto cam = scene->getCamera();
	//cam->setPosition(imstk::Vec3d(1.9, 0, -0.50));  // 60  1.8, -2, 2
	//cam->setFocalPoint(imstk::Vec3d(-1.8, 0.8, 0.5));
    cam->setPosition(imstk::Vec3d(-1.9, 0.0, -0.6));  // 60  1.8, -2, 2
    cam->setFocalPoint(imstk::Vec3d(1.9, 0.8, 0.50));
	cam->setViewAngle(70);

    // Light (white)
    auto whiteLight = std::make_shared<imstk::PointLight>("whiteLight");
    whiteLight->setPosition(imstk::Vec3d(1.5, 0, 0));
    whiteLight->setColor(Color(1,1,1));

    auto whiteLight2 = std::make_shared<imstk::PointLight>("whiteLight2");
    whiteLight2->setPosition(imstk::Vec3d(-1.5, 0, 0));
    whiteLight2->setColor(Color(1, 1, 1));

    scene->addLight(whiteLight);
    scene->addLight(whiteLight2);


	// colon wall
    std::string pathColon = DATA_ROOT_PATH"/glove.dae"; //  colon_move2.DAE";    // colon_move2.DAE  colon1_part_max.DAE  colon1_part_max.DAE  colon6_max.DAEcloth.DAE  colon_1_c2_max  colon_cylinder2_tri_max
	auto collidingColonMesh = imstk::MeshIO::read(pathColon);
    auto objMesh = collidingColonMesh; //  imstk::MeshIO::read(pathColon);
	auto physicsColonMesh = collidingColonMesh; //  imstk::MeshIO::read(pathColon);

    auto visualColonMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh);
    auto material = std::make_shared<RenderMaterial>();
    auto diffuseTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/textures/colontex5.png", Texture::Type::DIFFUSE);
    auto normalTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/textures/colontex5n.png", Texture::Type::NORMAL);
    auto roughnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/textures/colontex5r.png", Texture::Type::ROUGHNESS);
    auto metalnessTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/textures/colontex5r.png", Texture::Type::METALNESS);
    material->addTexture(diffuseTexture);
    material->addTexture(normalTexture);
    material->addTexture(roughnessTexture);
    //material->setTexture(metalnessTexture);
    visualColonMesh->setRenderMaterial(material);

	visualColonMesh->computeBoundingBox(min1, max1);
	outpointtri = fopen("G:/AABB.txt", "a");
	fprintf(outpointtri, "V Colon: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
	// floor

	imstk::StdVectorOfVec3d vertList;
	double width = 60.0;
	double height = 60.0;
	int nRows = 11;
	int nCols = 11;
	int corner[4] = { 1, nRows, nRows*nCols - nCols + 1, nRows*nCols };
	char intStr[33];
	std::string fixed_corner;
	//for (unsigned int i = 0; i < 4; ++i)
	//{
	//	std::sprintf(intStr, "%d", corner[i]);
	//	fixed_corner += std::string(intStr) + ' ';
	//}

	//fixed point
	Vec3d tmpPos;
	for (int i = 0; i < visualColonMesh->getNumVertices(); ++i)
	{
		tmpPos = visualColonMesh->getVertexPosition(i);
		if ((tmpPos.x() < -1.5) || (tmpPos.x() > 1.6))  // <-1.3  >1.6  smoothed colon segment  (tmpPos.x() < -1.5) || (tmpPos.x() > 1.6)
		{
			std::sprintf(intStr, "%d", i);
			fixed_corner += std::string(intStr) + ' ';
		}
	}

	fprintf(outpointtri, "V_Colon: Fixed point %s \n ", fixed_corner.c_str());
	fclose(outpointtri);

	// 
	vertList.resize(nRows*nCols);
	const double dy = width / (double)(nCols - 1);
	const double dx = height / (double)(nRows - 1);
	for (int i = 0; i < nRows; ++i)
	{
		for (int j = 0; j < nCols; j++)
		{
			const double y = (double)dy*j;
			const double x = (double)dx*i;
			vertList[i*nCols + j] = Vec3d(x - 50, 0.0, y - 50);

		}
	}

	// c. Add connectivity data
	std::vector<imstk::SurfaceMesh::TriangleArray> triangles;
	for (std::size_t i = 0; i < nRows - 1; ++i)
	{
		for (std::size_t j = 0; j < nCols - 1; j++)
		{
			imstk::SurfaceMesh::TriangleArray tri[2];
			tri[0] = { { i*nCols + j, i*nCols + j + 1, (i + 1)*nCols + j } };
			tri[1] = { { (i + 1)*nCols + j + 1, (i + 1)*nCols + j, i*nCols + j + 1 } };
			triangles.push_back(tri[0]);
			triangles.push_back(tri[1]);
		}
	}
	//auto collidingColonMesh = std::make_shared<imstk::SurfaceMesh>();
	//collidingColonMesh->initialize(vertList, triangles);
	//auto visualColonMesh = std::make_shared<imstk::SurfaceMesh>();
	//visualColonMesh->initialize(vertList, triangles);
	//auto physicsColonMesh = std::make_shared<imstk::SurfaceMesh>();
	//physicsColonMesh->initialize(vertList, triangles);


	auto colonMapP2V = std::make_shared<imstk::OneToOneMap>();
	colonMapP2V->setMaster(physicsColonMesh);
	colonMapP2V->setSlave(visualColonMesh);
	colonMapP2V->compute();


	auto colonMapP2C = std::make_shared<imstk::OneToOneMap>();
	colonMapP2C->setMaster(physicsColonMesh);
	colonMapP2C->setSlave(collidingColonMesh);
	colonMapP2C->compute();

	auto colonMapC2V = std::make_shared<imstk::OneToOneMap>();
	colonMapC2V->setMaster(collidingColonMesh);
	colonMapC2V->setSlave(visualColonMesh);
	colonMapC2V->compute();

	auto colonObj = std::make_shared<PbdObject>("Floor");
	colonObj->setCollidingGeometry(collidingColonMesh);
	colonObj->setVisualGeometry(visualColonMesh);
	colonObj->setPhysicsGeometry(physicsColonMesh);
	colonObj->setPhysicsToCollidingMap(colonMapP2C);
	colonObj->setPhysicsToVisualMap(colonMapP2V);
	colonObj->setCollidingToVisualMap(colonMapC2V);

	auto pbdColonModel = std::make_shared<PbdModel>();
    pbdColonModel->configure(/*Number of Constraints*/2,
		/*Constraint configuration*/"Dihedral 0.9", // 0.01
		/*Constraint configuration*/"Distance 1",
		/*Mass*/0.1,
		/*Gravity*/"0 0 0",  // "0 -9.8 0",
		/*TimeStep*/0.001, // 0.001
		/*FixedPoint*/fixed_corner.c_str(),
		/*NumberOfIterationInConstraintSolver*/12,
		/*Proximity*/0.02,  // 0.1
		/*Contact stiffness*/0.1);
    colonObj->setDynamicalModel(pbdColonModel);

    auto pbdColonSolver = std::make_shared<PbdSolver>();
	pbdColonSolver->setPbdObject(colonObj);
	scene->addNonlinearSolver(pbdColonSolver);

	scene->addSceneObject(colonObj);

	// Collisions
	auto colGraph = scene->getCollisionGraph();
	auto pair = std::make_shared<PbdInteractionPair>(PbdInteractionPair(object, colonObj));
	pair->setNumberOfInterations(2);

	colGraph->addInteractionPair(pair);

	// Run
	sdk->setActiveScene(scene);
	sdk->startSimulation(false);
#endif
}

// with smooth colon wall (original segment colon) run normally
void testPbdVirtualCoupling5()
{
#ifdef iMSTK_USE_OPENHAPTICS
	// SDK and Scene
	auto sdk = std::make_shared<imstk::SimulationManager>();
	auto scene = sdk->createNewScene("SceneTestDevice");

	// Device Client
	auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

	// Device Server
	auto server = std::make_shared<imstk::HDAPIDeviceServer>();
	server->addDeviceClient(client);
	sdk->addModule(server);

	std::string path2obj = DATA_ROOT_PATH"/kcube2.DAE";  // blade2.obj  cube.obj  kcube.obj  test cloth using this 
	std::string path2obj_c = DATA_ROOT_PATH"/kcube2.DAE";  // blade2.obj  cube.obj kcube_s2.obj
	auto collidingMesh = imstk::MeshIO::read(path2obj_c); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
	auto visualMesh = collidingMesh; // imstk::MeshIO::read(path2obj);
	auto physicsMesh = collidingMesh; //  imstk::MeshIO::read(path2obj_c);  //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
	//objmesh->translate(-270, 0, 70);
	//auto geom = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objmesh);
	//auto viusalMesh = imstk::MeshIO::read(path2obj);
	Vec3d min1, max1;
	visualMesh->computeBoundingBox(min1, max1);

	FILE *outpointtri;
	outpointtri = fopen("G:/AABB.txt", "w");
	fprintf(outpointtri, "V: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

	collidingMesh->computeBoundingBox(min1, max1);
	fprintf(outpointtri, "C: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
	fclose(outpointtri);

	/*imstk::StdVectorOfVec3d vertListLine;
	vertListLine.resize(2);
	vertListLine[0] = Vec3d(0.0, 0.0, 0.0);
	vertListLine[1] = Vec3d(0.0, 40, 0.0);
	std::vector<std::vector<int> > connectivityLine;
	for (int i = 0; i < 1;){
	std::vector<int> line;
	line.push_back(i);
	i++;
	line.push_back(i);

	connectivityLine.push_back(line);
	}*/

	//collidingMesh->initialize(vertListLine);
	//collidingMesh->setConnectivity(connectivityLine);

	//physicsMesh->initialize(vertListLine);
	//physicsMesh->setConnectivity(connectivityLine);

	auto object = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject");

	auto objMapP2V = std::make_shared<imstk::OneToOneMap>();
	objMapP2V->setMaster(physicsMesh);
	objMapP2V->setSlave(visualMesh);
	objMapP2V->compute();


	auto objMapP2C = std::make_shared<imstk::OneToOneMap>();
	objMapP2C->setMaster(physicsMesh);
	objMapP2C->setSlave(collidingMesh);
	objMapP2C->compute();

	auto objMapC2V = std::make_shared<imstk::OneToOneMap>();
	objMapC2V->setMaster(collidingMesh);
	objMapC2V->setSlave(visualMesh);
	objMapC2V->compute();

	auto objMapC2P = std::make_shared<imstk::OneToOneMap>();
	objMapC2P->setMaster(collidingMesh);
	objMapC2P->setSlave(physicsMesh);
	objMapC2P->compute();

	object->setVisualGeometry(visualMesh);
	object->setCollidingGeometry(collidingMesh);
	object->setPhysicsGeometry(physicsMesh);
	object->setPhysicsToCollidingMap(objMapP2C);
	object->setPhysicsToVisualMap(objMapP2V);
	object->setCollidingToVisualMap(objMapC2V);
	object->setColldingToPhysicsMap(objMapC2P);

	auto pbdModel = std::make_shared<PbdModel>();
    pbdModel->configure(/*Number of Constraints*/1,
		/*Constraint configuration*/"Distance 0.1",
		/*Mass*/0.0,
		/*Gravity*/"0 0 0",
		/*TimeStep*/0.001,
		/*FixedPoint*/"",
		/*NumberOfIterationInConstraintSolver*/5,
		/*Proximity*/0.1,
		/*Contact stiffness*/0.001);
    object->setDynamicalModel(pbdModel);

	//auto pbdSolver = std::make_shared<PbdSolver>();
	//pbdSolver->setPbdObject(object);
	//scene->addNonlinearSolver(pbdSolver);

	scene->addSceneObject(object);

	auto trackCtrl = std::make_shared<imstk::DeviceTracker>(client);
	trackCtrl->setTranslationScaling(0.3);
	auto controller = std::make_shared<imstk::PBDSceneObjectController>(object, trackCtrl);
	scene->addObjectController(controller);

	// Update Camera position
	auto cam = scene->getCamera();
	cam->setPosition(imstk::Vec3d(0, 3, 10));  // 60
	cam->setFocalPoint(imstk::Vec3d(0, -4, -5));
	cam->setViewAngle(70);

	// colon wall
	std::string pathColon = DATA_ROOT_PATH"/colon1_part_max.DAE";    // colon1_part_max.DAE  colon6_max.DAEcloth.DAE  colon_1_c2_max  colon_cylinder2_tri_max
	auto collidingColonMesh = imstk::MeshIO::read(pathColon);
	auto visualColonMesh = collidingColonMesh; //  imstk::MeshIO::read(pathColon);
	auto physicsColonMesh = collidingColonMesh; //  imstk::MeshIO::read(pathColon);

	visualColonMesh->computeBoundingBox(min1, max1);	
	outpointtri = fopen("G:/AABB.txt", "a");
	fprintf(outpointtri, "V Colon: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
	// floor

	imstk::StdVectorOfVec3d vertList;
	double width = 60.0;
	double height = 60.0;
	int nRows = 11;
	int nCols = 11;
	int corner[4] = { 1, nRows, nRows*nCols - nCols + 1, nRows*nCols };
	char intStr[33];
	std::string fixed_corner;
	//for (unsigned int i = 0; i < 4; ++i)
	//{
	//	std::sprintf(intStr, "%d", corner[i]);
	//	fixed_corner += std::string(intStr) + ' ';
	//}

	//fixed point
	Vec3d tmpPos;
	for (int i = 0; i < visualColonMesh->getNumVertices(); ++i)
	{
		tmpPos = visualColonMesh->getVertexPosition(i);
		if ((tmpPos.x() < -1.3) || (tmpPos.x() > 1.6))  // <-1.3  >1.6  smoothed colon segment  (tmpPos.x() < -1.5) || (tmpPos.x() > 1.6)
		{
			std::sprintf(intStr, "%d", i);
			fixed_corner += std::string(intStr) + ' ';
		}
	}
	
	fprintf(outpointtri, "V_Colon: Fixed point %s \n ", fixed_corner.c_str());
	fclose(outpointtri);

	// 
	vertList.resize(nRows*nCols);
	const double dy = width / (double)(nCols - 1);
	const double dx = height / (double)(nRows - 1);
	for (int i = 0; i < nRows; ++i)
	{
		for (int j = 0; j < nCols; j++)
		{
			const double y = (double)dy*j;
			const double x = (double)dx*i;
			vertList[i*nCols + j] = Vec3d(x - 50, 0.0, y - 50);

		}
	}

	// c. Add connectivity data
	std::vector<imstk::SurfaceMesh::TriangleArray> triangles;
	for (std::size_t i = 0; i < nRows - 1; ++i)
	{
		for (std::size_t j = 0; j < nCols - 1; j++)
		{
			imstk::SurfaceMesh::TriangleArray tri[2];
			tri[0] = { { i*nCols + j, i*nCols + j + 1, (i + 1)*nCols + j } };
			tri[1] = { { (i + 1)*nCols + j + 1, (i + 1)*nCols + j, i*nCols + j + 1 } };
			triangles.push_back(tri[0]);
			triangles.push_back(tri[1]);
		}
	}
	//auto collidingColonMesh = std::make_shared<imstk::SurfaceMesh>();
	//collidingColonMesh->initialize(vertList, triangles);
	//auto visualColonMesh = std::make_shared<imstk::SurfaceMesh>();
	//visualColonMesh->initialize(vertList, triangles);
	//auto physicsColonMesh = std::make_shared<imstk::SurfaceMesh>();
	//physicsColonMesh->initialize(vertList, triangles);


	auto colonMapP2V = std::make_shared<imstk::OneToOneMap>();
	colonMapP2V->setMaster(physicsColonMesh);
	colonMapP2V->setSlave(visualColonMesh);
	colonMapP2V->compute();


	auto colonMapP2C = std::make_shared<imstk::OneToOneMap>();
	colonMapP2C->setMaster(physicsColonMesh);
	colonMapP2C->setSlave(collidingColonMesh);
	colonMapP2C->compute();

	auto colonMapC2V = std::make_shared<imstk::OneToOneMap>();
	colonMapC2V->setMaster(collidingColonMesh);
	colonMapC2V->setSlave(visualColonMesh);
	colonMapC2V->compute();

	auto colonObj = std::make_shared<PbdObject>("Floor");
	colonObj->setCollidingGeometry(collidingColonMesh);
	colonObj->setVisualGeometry(visualColonMesh);
	colonObj->setPhysicsGeometry(physicsColonMesh);
	colonObj->setPhysicsToCollidingMap(colonMapP2C);
	colonObj->setPhysicsToVisualMap(colonMapP2V);
	colonObj->setCollidingToVisualMap(colonMapC2V);

	auto pbdColonModel = std::make_shared<PbdModel>();
    pbdColonModel->configure(/*Number of Constraints*/2,
		/*Constraint configuration*/"Dihedral 0.9", // 0.01
		/*Constraint configuration*/"Distance 1",
		/*Mass*/0.1,
		/*Gravity*/"0 0 0",  // "0 -9.8 0",
		/*TimeStep*/0.2, // 0.001
		/*FixedPoint*/fixed_corner.c_str(),
		/*NumberOfIterationInConstraintSolver*/5,
		/*Proximity*/0.1,
		/*Contact stiffness*/0.1);
    colonObj->setDynamicalModel(pbdColonModel);

	auto pbdColonSolver = std::make_shared<PbdSolver>();
	pbdColonSolver->setPbdObject(colonObj);
	scene->addNonlinearSolver(pbdColonSolver);

	scene->addSceneObject(colonObj);

	// Collisions
	auto colGraph = scene->getCollisionGraph();
	auto pair = std::make_shared<PbdInteractionPair>(PbdInteractionPair(object, colonObj));
	pair->setNumberOfInterations(2);

	colGraph->addInteractionPair(pair);

	// Run
	sdk->setActiveScene(scene);
	sdk->startSimulation(true);
#endif
}

// with colon wall
void testPbdVirtualCoupling4()
{
#ifdef iMSTK_USE_OPENHAPTICS
	// SDK and Scene
	auto sdk = std::make_shared<imstk::SimulationManager>();
	auto scene = sdk->createNewScene("SceneTestDevice");

	// Device Client
	auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

	// Device Server
	auto server = std::make_shared<imstk::HDAPIDeviceServer>();
	server->addDeviceClient(client);
	sdk->addModule(server);

	std::string path2obj = DATA_ROOT_PATH"/kcube_s2.obj";  // blade2.obj  cube.obj  kcube.obj  test cloth using this 
	std::string path2obj_c = DATA_ROOT_PATH"/kcube_s2.obj";  // blade2.obj  cube.obj
	auto collidingMesh = imstk::MeshIO::read(path2obj_c); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
	auto visualMesh = collidingMesh; // imstk::MeshIO::read(path2obj);
	auto physicsMesh = collidingMesh; //  imstk::MeshIO::read(path2obj_c);  //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
	//objmesh->translate(-270, 0, 70);
	//auto geom = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objmesh);
	//auto viusalMesh = imstk::MeshIO::read(path2obj);
	Vec3d min1, max1;
	visualMesh->computeBoundingBox(min1, max1);

	FILE *outpointtri;
	outpointtri = fopen("G:/AABB.txt", "w");
	fprintf(outpointtri, "V: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

	collidingMesh->computeBoundingBox(min1, max1);
	fprintf(outpointtri, "C: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
	fclose(outpointtri);

	/*imstk::StdVectorOfVec3d vertListLine;
	vertListLine.resize(2);
	vertListLine[0] = Vec3d(0.0, 0.0, 0.0);
	vertListLine[1] = Vec3d(0.0, 40, 0.0);
	std::vector<std::vector<int> > connectivityLine;
	for (int i = 0; i < 1;){
	std::vector<int> line;
	line.push_back(i);
	i++;
	line.push_back(i);

	connectivityLine.push_back(line);
	}*/

	//collidingMesh->initialize(vertListLine);
	//collidingMesh->setConnectivity(connectivityLine);

	//physicsMesh->initialize(vertListLine);
	//physicsMesh->setConnectivity(connectivityLine);

	auto object = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject");

	auto objMapP2V = std::make_shared<imstk::OneToOneMap>();
	objMapP2V->setMaster(physicsMesh);
	objMapP2V->setSlave(visualMesh);
	objMapP2V->compute();


	auto objMapP2C = std::make_shared<imstk::OneToOneMap>();
	objMapP2C->setMaster(physicsMesh);
	objMapP2C->setSlave(collidingMesh);
	objMapP2C->compute();

	auto objMapC2V = std::make_shared<imstk::OneToOneMap>();
	objMapC2V->setMaster(collidingMesh);
	objMapC2V->setSlave(visualMesh);
	objMapC2V->compute();

	auto objMapC2P = std::make_shared<imstk::OneToOneMap>();
	objMapC2P->setMaster(collidingMesh);
	objMapC2P->setSlave(physicsMesh);
	objMapC2P->compute();

	object->setVisualGeometry(visualMesh);
	object->setCollidingGeometry(collidingMesh);
	object->setPhysicsGeometry(physicsMesh);
	object->setPhysicsToCollidingMap(objMapP2C);
	object->setPhysicsToVisualMap(objMapP2V);
	object->setCollidingToVisualMap(objMapC2V);
	object->setColldingToPhysicsMap(objMapC2P);

	auto pbdModel = std::make_shared<PbdModel>();
    pbdModel->configure(/*Number of Constraints*/1,
		/*Constraint configuration*/"Distance 0.1",
		/*Mass*/0.0,
		/*Gravity*/"0 0 0",
		/*TimeStep*/0.001,
		/*FixedPoint*/"",
		/*NumberOfIterationInConstraintSolver*/5,
		/*Proximity*/0.1,
		/*Contact stiffness*/0.001);
    object->setDynamicalModel(pbdModel);

	//auto pbdSolver = std::make_shared<PbdSolver>();
	//pbdSolver->setPbdObject(object);
	//scene->addNonlinearSolver(pbdSolver);

	scene->addSceneObject(object);

	auto trackCtrl = std::make_shared<imstk::DeviceTracker>(client);
	trackCtrl->setTranslationScaling(0.5);
	auto controller = std::make_shared<imstk::PBDSceneObjectController>(object, trackCtrl);
	scene->addObjectController(controller);

	// Update Camera position
	auto cam = scene->getCamera();
	cam->setPosition(imstk::Vec3d(0, 3, 60));
	cam->setFocalPoint(imstk::Vec3d(0, -4, -10));
	cam->setViewAngle(60);

	// colon wall
	std::string pathColon = DATA_ROOT_PATH"/cloth2m.obj";    // colon_1_c2_max  colon_cylinder2_tri_max
	auto collidingColonMesh = imstk::MeshIO::read(pathColon);
	auto visualColonMesh = collidingColonMesh; //  imstk::MeshIO::read(pathColon);
	auto physicsColonMesh = collidingColonMesh; //  imstk::MeshIO::read(pathColon);

	// floor

	imstk::StdVectorOfVec3d vertList;
	double width = 60.0;
	double height = 60.0;
	int nRows = 11;
	int nCols = 11;
	int corner[4] = { 1, nRows, nRows*nCols - nCols + 1, nRows*nCols };
	char intStr[33];
	std::string fixed_corner;
	for (unsigned int i = 0; i < 4; ++i)
	{
		std::sprintf(intStr, "%d", corner[i]);
		fixed_corner += std::string(intStr) + ' ';
	}
	vertList.resize(nRows*nCols);
	const double dy = width / (double)(nCols - 1);
	const double dx = height / (double)(nRows - 1);
	for (int i = 0; i < nRows; ++i)
	{
		for (int j = 0; j < nCols; j++)
		{
			const double y = (double)dy*j;
			const double x = (double)dx*i;
			vertList[i*nCols + j] = Vec3d(x - 50, 0.0, y - 50);

		}
	}

	// c. Add connectivity data
	std::vector<imstk::SurfaceMesh::TriangleArray> triangles;
	for (std::size_t i = 0; i < nRows - 1; ++i)
	{
		for (std::size_t j = 0; j < nCols - 1; j++)
		{
			imstk::SurfaceMesh::TriangleArray tri[2];
			tri[0] = { { i*nCols + j, i*nCols + j + 1, (i + 1)*nCols + j } };
			tri[1] = { { (i + 1)*nCols + j + 1, (i + 1)*nCols + j, i*nCols + j + 1 } };
			triangles.push_back(tri[0]);
			triangles.push_back(tri[1]);
		}
	}
	//auto collidingColonMesh = std::make_shared<imstk::SurfaceMesh>();
	//collidingColonMesh->initialize(vertList, triangles);
	//auto visualColonMesh = std::make_shared<imstk::SurfaceMesh>();
	//visualColonMesh->initialize(vertList, triangles);
	//auto physicsColonMesh = std::make_shared<imstk::SurfaceMesh>();
	//physicsColonMesh->initialize(vertList, triangles);


	auto colonMapP2V = std::make_shared<imstk::OneToOneMap>();
	colonMapP2V->setMaster(physicsColonMesh);
	colonMapP2V->setSlave(visualColonMesh);
	colonMapP2V->compute();


	auto colonMapP2C = std::make_shared<imstk::OneToOneMap>();
	colonMapP2C->setMaster(physicsColonMesh);
	colonMapP2C->setSlave(collidingColonMesh);
	colonMapP2C->compute();

	auto colonMapC2V = std::make_shared<imstk::OneToOneMap>();
	colonMapC2V->setMaster(collidingColonMesh);
	colonMapC2V->setSlave(visualColonMesh);
	colonMapC2V->compute();

	auto colonObj = std::make_shared<PbdObject>("Floor");
	colonObj->setCollidingGeometry(collidingColonMesh);
	colonObj->setVisualGeometry(visualColonMesh);
	colonObj->setPhysicsGeometry(physicsColonMesh);
	colonObj->setPhysicsToCollidingMap(colonMapP2C);
	colonObj->setPhysicsToVisualMap(colonMapP2V);
	colonObj->setCollidingToVisualMap(colonMapC2V);

	auto pbdColonModel = std::make_shared<PbdModel>();
    pbdColonModel->configure(/*Number of Constraints*/1,
		/*Constraint configuration*/"Distance 1",
		///*Constraint configuration*/"Dihedral 0.01",
		/*Mass*/0.1,
		/*Gravity*/"0 0 0",  // "0 -9.8 0",
		/*TimeStep*/0.001,
		/*FixedPoint*/fixed_corner.c_str(),
		/*NumberOfIterationInConstraintSolver*/5,
		/*Proximity*/0.1,
		/*Contact stiffness*/0.1);
    colonObj->setDynamicalModel(pbdColonModel);

    auto pbdColonSolver = std::make_shared<PbdSolver>();
	pbdColonSolver->setPbdObject(colonObj);
	scene->addNonlinearSolver(pbdColonSolver);

	scene->addSceneObject(colonObj);

	// Collisions
	auto colGraph = scene->getCollisionGraph();
	auto pair = std::make_shared<PbdInteractionPair>(PbdInteractionPair(object, colonObj));
	pair->setNumberOfInterations(2);

	colGraph->addInteractionPair(pair);

	// Run
	sdk->setActiveScene(scene);
	sdk->startSimulation(true);
#endif
}

// this example can run normally moving knife having collision with cloth using line mesh for blade colliding
void testPbdVirtualCoupling3()
{
#ifdef iMSTK_USE_OPENHAPTICS
	// SDK and Scene
	auto sdk = std::make_shared<imstk::SimulationManager>();
	auto scene = sdk->createNewScene("SceneTestDevice");

	// Device Client
	auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

	// Device Server
	auto server = std::make_shared<imstk::HDAPIDeviceServer>();
	server->addDeviceClient(client);
	sdk->addModule(server);

	//auto trackingCtrl = std::make_shared<imstk::DeviceTracker>(client0);
	////trackingCtrl->setTranslationScaling(100);
	//auto lapToolController = std::make_shared<imstk::SceneObjectController>(pbdObj, trackingCtrl);
	///*auto lapToolController = std::make_shared<imstk::LaparoscopicToolController>(pivot, upperJaw, lowerJaw, trackingCtrl);
	//lapToolController->setJawRotationAxis(imstk::Vec3d(1.0, 0, 0));*/
	//scene->addObjectController(lapToolController);

	// Object
	//auto geom = std::make_shared<imstk::Cube>();
	//geom->setPosition(imstk::UP_VECTOR);
	//geom->scale(2);

	std::string path2obj = DATA_ROOT_PATH"/kcube.obj";  // blade2.obj  cube.obj
	std::string path2obj_c = DATA_ROOT_PATH"/kcube.obj";  // blade2.obj  cube.obj
	auto collidingMesh = imstk::MeshIO::read(path2obj_c); //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
	auto visualMesh = imstk::MeshIO::read(path2obj);
	auto physicsMesh = imstk::MeshIO::read(path2obj_c);  //  std::make_shared<imstk::LineMesh>(); //  imstk::MeshIO::read(path2obj);
	//objmesh->translate(-270, 0, 70);
	//auto geom = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objmesh);
	//auto viusalMesh = imstk::MeshIO::read(path2obj);
	Vec3d min1, max1;
	visualMesh->computeBoundingBox(min1, max1);

	FILE *outpointtri;
	outpointtri = fopen("G:/AABB.txt", "w");
	fprintf(outpointtri, "V: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());

	collidingMesh->computeBoundingBox(min1, max1);
	fprintf(outpointtri, "C: AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
	fclose(outpointtri);

	/*imstk::StdVectorOfVec3d vertListLine;
	vertListLine.resize(2);
	vertListLine[0] = Vec3d(0.0, 0.0, 0.0);
	vertListLine[1] = Vec3d(0.0, 40, 0.0);
	std::vector<std::vector<int> > connectivityLine;
	for (int i = 0; i < 1;){
	std::vector<int> line;
	line.push_back(i);
	i++;
	line.push_back(i);

	connectivityLine.push_back(line);
	}*/

	//collidingMesh->initialize(vertListLine);
	//collidingMesh->setConnectivity(connectivityLine);

	//physicsMesh->initialize(vertListLine);
	//physicsMesh->setConnectivity(connectivityLine);

	auto object = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject");

	auto objMapP2V = std::make_shared<imstk::OneToOneMap>();
	objMapP2V->setMaster(physicsMesh);
	objMapP2V->setSlave(visualMesh);
	objMapP2V->compute();


	auto objMapP2C = std::make_shared<imstk::OneToOneMap>();
	objMapP2C->setMaster(physicsMesh);
	objMapP2C->setSlave(collidingMesh);
	objMapP2C->compute();

	auto objMapC2V = std::make_shared<imstk::OneToOneMap>();
	objMapC2V->setMaster(collidingMesh);
	objMapC2V->setSlave(visualMesh);
	objMapC2V->compute();

	auto objMapC2P = std::make_shared<imstk::OneToOneMap>();
	objMapC2P->setMaster(collidingMesh);
	objMapC2P->setSlave(physicsMesh);
	objMapC2P->compute();

	object->setVisualGeometry(visualMesh);
	object->setCollidingGeometry(collidingMesh);
	object->setPhysicsGeometry(physicsMesh);
	object->setPhysicsToCollidingMap(objMapP2C);
	object->setPhysicsToVisualMap(objMapP2V);
	object->setCollidingToVisualMap(objMapC2V);
	object->setColldingToPhysicsMap(objMapC2P);

	auto pbdModel = std::make_shared<PbdModel>();
    pbdModel->configure(/*Number of Constraints*/1,
		/*Constraint configuration*/"Distance 0.1",
		/*Mass*/0.0,
		/*Gravity*/"0 0 0",
		/*TimeStep*/0.001,
		/*FixedPoint*/"",
		/*NumberOfIterationInConstraintSolver*/5,
		/*Proximity*/0.1,
		/*Contact stiffness*/0.01);
    object->setDynamicalModel(pbdModel);

	//auto pbdSolver = std::make_shared<PbdSolver>();
	//pbdSolver->setPbdObject(object);
	//scene->addNonlinearSolver(pbdSolver);

	scene->addSceneObject(object);

	auto trackCtrl = std::make_shared<imstk::DeviceTracker>(client);
	trackCtrl->setTranslationScaling(0.5);
	auto controller = std::make_shared<imstk::PBDSceneObjectController>(object, trackCtrl);
	scene->addObjectController(controller);

	// Update Camera position
	auto cam = scene->getCamera();
	cam->setPosition(imstk::Vec3d(0, 10, -150));
	cam->setFocalPoint(imstk::Vec3d(0, -40, 0));
	cam->setViewAngle(60);

	// floor

	imstk::StdVectorOfVec3d vertList;
	double width = 100.0;
	double height = 100.0;
	int nRows = 10; // 30
	int nCols = 10;
	int corner[4] = { 1, nRows, nRows*nCols - nCols + 1, nRows*nCols };
	char intStr[33];
	std::string fixed_corner;
	for (unsigned int i = 0; i < 4; ++i)
	{
		std::sprintf(intStr, "%d", corner[i]);
		fixed_corner += std::string(intStr) + ' ';
	}
	vertList.resize(nRows*nCols);
	const double dy = width / (double)(nCols - 1);
	const double dx = height / (double)(nRows - 1);
	for (int i = 0; i < nRows; ++i)
	{
		for (int j = 0; j < nCols; j++)
		{
			const double y = (double)dy*j;
			const double x = (double)dx*i;
			vertList[i*nCols + j] = Vec3d(x - 50, -30.0, y - 50);

		}
	}

	// c. Add connectivity data
	std::vector<imstk::SurfaceMesh::TriangleArray> triangles;
	for (std::size_t i = 0; i < nRows - 1; ++i)
	{
		for (std::size_t j = 0; j < nCols - 1; j++)
		{
			imstk::SurfaceMesh::TriangleArray tri[2];
			tri[0] = { { i*nCols + j, i*nCols + j + 1, (i + 1)*nCols + j } };
			tri[1] = { { (i + 1)*nCols + j + 1, (i + 1)*nCols + j, i*nCols + j + 1 } };
			triangles.push_back(tri[0]);
			triangles.push_back(tri[1]);
		}
	}
	auto floorMeshColliding = std::make_shared<imstk::SurfaceMesh>();
	floorMeshColliding->initialize(vertList, triangles);
	auto floorMeshVisual = std::make_shared<imstk::SurfaceMesh>();
	floorMeshVisual->initialize(vertList, triangles);
	auto floorMeshPhysics = std::make_shared<imstk::SurfaceMesh>();
	floorMeshPhysics->initialize(vertList, triangles);


	auto floorMapP2V = std::make_shared<imstk::OneToOneMap>();
	floorMapP2V->setMaster(floorMeshPhysics);
	floorMapP2V->setSlave(floorMeshVisual);
	floorMapP2V->compute();


	auto floorMapP2C = std::make_shared<imstk::OneToOneMap>();
	floorMapP2C->setMaster(floorMeshPhysics);
	floorMapP2C->setSlave(floorMeshColliding);
	floorMapP2C->compute();

	auto floorMapC2V = std::make_shared<imstk::OneToOneMap>();
	floorMapC2V->setMaster(floorMeshColliding);
	floorMapC2V->setSlave(floorMeshVisual);
	floorMapC2V->compute();

	auto floor = std::make_shared<PbdObject>("Floor");
	floor->setCollidingGeometry(floorMeshColliding);
	floor->setVisualGeometry(floorMeshVisual);
	floor->setPhysicsGeometry(floorMeshPhysics);
	floor->setPhysicsToCollidingMap(floorMapP2C);
	floor->setPhysicsToVisualMap(floorMapP2V);
	floor->setCollidingToVisualMap(floorMapC2V);

	auto pbdModel2 = std::make_shared<PbdModel>();
    pbdModel2->configure(/*Number of Constraints*/2,
		/*Constraint configuration*/"Distance 0.1",
		/*Constraint configuration*/"Dihedral 0.001",
		/*Mass*/0.1,
		/*Gravity*/"0 0 0",  // "0 -9.8 0",
		/*TimeStep*/0.001,
		/*FixedPoint*/fixed_corner.c_str(),
		/*NumberOfIterationInConstraintSolver*/5,
		/*Proximity*/0.1,
		/*Contact stiffness*/0.1);
    floor->setDynamicalModel(pbdModel2);

    auto pbdSolverfloor = std::make_shared<PbdSolver>();
	pbdSolverfloor->setPbdObject(floor);
	scene->addNonlinearSolver(pbdSolverfloor);

	scene->addSceneObject(floor);

	// Collisions
	auto colGraph = scene->getCollisionGraph();
	auto pair = std::make_shared<PbdInteractionPair>(PbdInteractionPair(object, floor));
	pair->setNumberOfInterations(2);

	colGraph->addInteractionPair(pair);

	// Run
	sdk->setActiveScene(scene);
	sdk->startSimulation(true);
#endif
}

// this example can run normally moving knife having collision with cloth using surface mesh for blade
void testPbdVirtualCoupling2()
{
#ifdef iMSTK_USE_OPENHAPTICS
	// SDK and Scene
	auto sdk = std::make_shared<imstk::SimulationManager>();
	auto scene = sdk->createNewScene("SceneTestDevice");

	// Device Client
	auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

	// Device Server
	auto server = std::make_shared<imstk::HDAPIDeviceServer>();
	server->addDeviceClient(client);
	sdk->addModule(server);

	//auto trackingCtrl = std::make_shared<imstk::DeviceTracker>(client0);
	////trackingCtrl->setTranslationScaling(100);
	//auto lapToolController = std::make_shared<imstk::SceneObjectController>(pbdObj, trackingCtrl);
	///*auto lapToolController = std::make_shared<imstk::LaparoscopicToolController>(pivot, upperJaw, lowerJaw, trackingCtrl);
	//lapToolController->setJawRotationAxis(imstk::Vec3d(1.0, 0, 0));*/
	//scene->addObjectController(lapToolController);

	// Object
	//auto geom = std::make_shared<imstk::Cube>();
	//geom->setPosition(imstk::UP_VECTOR);
	//geom->scale(2);

	std::string path2obj = DATA_ROOT_PATH"/knife3.obj";  // blade2.obj  cube.obj
	auto collidingMesh = imstk::MeshIO::read(path2obj);
	auto visualMesh = imstk::MeshIO::read(path2obj);
	auto physicsMesh = imstk::MeshIO::read(path2obj);
	//objmesh->translate(-270, 0, 70);
	//auto geom = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objmesh);
	//auto viusalMesh = imstk::MeshIO::read(path2obj);
	//Vec3d min1, max1;
	//geom->computeBoundingBox(min1, max1);

	//FILE *outpointtri;
	//outpointtri = fopen("G:/AABB.txt", "w");
	//fprintf(outpointtri, "AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
	//fclose(outpointtri);

	auto object = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject");

	auto objMapP2V = std::make_shared<imstk::OneToOneMap>();
	objMapP2V->setMaster(physicsMesh);
	objMapP2V->setSlave(visualMesh);
	objMapP2V->compute();


	auto objMapP2C = std::make_shared<imstk::OneToOneMap>();
	objMapP2C->setMaster(physicsMesh);
	objMapP2C->setSlave(collidingMesh);
	objMapP2C->compute();

	auto objMapC2V = std::make_shared<imstk::OneToOneMap>();
	objMapC2V->setMaster(collidingMesh);
	objMapC2V->setSlave(visualMesh);
	objMapC2V->compute();

	auto objMapC2P = std::make_shared<imstk::OneToOneMap>();
	objMapC2P->setMaster(collidingMesh);
	objMapC2P->setSlave(physicsMesh);
	objMapC2P->compute();

	object->setVisualGeometry(visualMesh);
	object->setCollidingGeometry(collidingMesh);
	object->setPhysicsGeometry(physicsMesh);
	object->setPhysicsToCollidingMap(objMapP2C);
	object->setPhysicsToVisualMap(objMapP2V);
	object->setCollidingToVisualMap(objMapC2V);
	object->setColldingToPhysicsMap(objMapC2P);

	auto pbdModel = std::make_shared<PbdModel>();
    pbdModel->configure(/*Number of Constraints*/1,
		/*Constraint configuration*/"Distance 0.1",
		/*Mass*/0.0,
		/*Gravity*/"0 0 0",
		/*TimeStep*/0.001,
		/*FixedPoint*/"",
		/*NumberOfIterationInConstraintSolver*/5,
		/*Proximity*/0.1,
		/*Contact stiffness*/0.01);
    object->setDynamicalModel(pbdModel);

	//auto pbdSolver = std::make_shared<PbdSolver>();
	//pbdSolver->setPbdObject(object);
	//scene->addNonlinearSolver(pbdSolver);

	scene->addSceneObject(object);

	auto trackCtrl = std::make_shared<imstk::DeviceTracker>(client);
	trackCtrl->setTranslationScaling(0.5);
	auto controller = std::make_shared<imstk::PBDSceneObjectController>(object, trackCtrl);
	scene->addObjectController(controller);

	// Update Camera position
	auto cam = scene->getCamera();
	cam->setPosition(imstk::Vec3d(0, 10, -150));
	cam->setFocalPoint(imstk::Vec3d(0, -40, 0));
	cam->setViewAngle(60);

	// floor

	imstk::StdVectorOfVec3d vertList;
	double width = 100.0;
	double height = 100.0;
	int nRows = 10;
	int nCols = 10;
	int corner[4] = { 1, nRows, nRows*nCols - nCols + 1, nRows*nCols };
	char intStr[33];
	std::string fixed_corner;
	for (unsigned int i = 0; i < 4; ++i)
	{
		std::sprintf(intStr, "%d", corner[i]);
		fixed_corner += std::string(intStr) + ' ';
	}
	vertList.resize(nRows*nCols);
	const double dy = width / (double)(nCols - 1);
	const double dx = height / (double)(nRows - 1);
	for (int i = 0; i < nRows; ++i)
	{
		for (int j = 0; j < nCols; j++)
		{
			const double y = (double)dy*j;
			const double x = (double)dx*i;
			vertList[i*nCols + j] = Vec3d(x - 50, -30.0, y - 50);

		}
	}

	// c. Add connectivity data
	std::vector<imstk::SurfaceMesh::TriangleArray> triangles;
	for (std::size_t i = 0; i < nRows - 1; ++i)
	{
		for (std::size_t j = 0; j < nCols - 1; j++)
		{
			imstk::SurfaceMesh::TriangleArray tri[2];
			tri[0] = { { i*nCols + j, i*nCols + j + 1, (i + 1)*nCols + j } };
			tri[1] = { { (i + 1)*nCols + j + 1, (i + 1)*nCols + j, i*nCols + j + 1 } };
			triangles.push_back(tri[0]);
			triangles.push_back(tri[1]);
		}
	}
	auto floorMeshColliding = std::make_shared<imstk::SurfaceMesh>();
	floorMeshColliding->initialize(vertList, triangles);
	auto floorMeshVisual = std::make_shared<imstk::SurfaceMesh>();
	floorMeshVisual->initialize(vertList, triangles);
	auto floorMeshPhysics = std::make_shared<imstk::SurfaceMesh>();
	floorMeshPhysics->initialize(vertList, triangles);


	auto floorMapP2V = std::make_shared<imstk::OneToOneMap>();
	floorMapP2V->setMaster(floorMeshPhysics);
	floorMapP2V->setSlave(floorMeshVisual);
	floorMapP2V->compute();


	auto floorMapP2C = std::make_shared<imstk::OneToOneMap>();
	floorMapP2C->setMaster(floorMeshPhysics);
	floorMapP2C->setSlave(floorMeshColliding);
	floorMapP2C->compute();

	auto floorMapC2V = std::make_shared<imstk::OneToOneMap>();
	floorMapC2V->setMaster(floorMeshColliding);
	floorMapC2V->setSlave(floorMeshVisual);
	floorMapC2V->compute();

	auto floor = std::make_shared<PbdObject>("Floor");
	floor->setCollidingGeometry(floorMeshColliding);
	floor->setVisualGeometry(floorMeshVisual);
	floor->setPhysicsGeometry(floorMeshPhysics);
	floor->setPhysicsToCollidingMap(floorMapP2C);
	floor->setPhysicsToVisualMap(floorMapP2V);
	floor->setCollidingToVisualMap(floorMapC2V);

	auto pbdModel2 = std::make_shared<PbdModel>();
    pbdModel2->configure(/*Number of Constraints*/2,
		/*Constraint configuration*/"Distance 0.1",
		/*Constraint configuration*/"Dihedral 0.001",
		/*Mass*/0.1,
		/*Gravity*/"0 0 0",  // "0 -9.8 0",
		/*TimeStep*/0.001,
		/*FixedPoint*/fixed_corner.c_str(),
		/*NumberOfIterationInConstraintSolver*/5,
		/*Proximity*/0.1,
		/*Contact stiffness*/0.1);
    floor->setDynamicalModel(pbdModel2);

	auto pbdSolverfloor = std::make_shared<PbdSolver>();
	pbdSolverfloor->setPbdObject(floor);
	scene->addNonlinearSolver(pbdSolverfloor);

	scene->addSceneObject(floor);

	// Collisions
	auto colGraph = scene->getCollisionGraph();
	auto pair = std::make_shared<PbdInteractionPair>(PbdInteractionPair(object, floor));
	pair->setNumberOfInterations(2);

	colGraph->addInteractionPair(pair);

	// Run
	sdk->setActiveScene(scene);
	sdk->startSimulation(true);
#endif
}

// this example can run normally moving knife having collision with cloth
void testPbdVirtualCoupling()
{
#ifdef iMSTK_USE_OPENHAPTICS
	// SDK and Scene
	auto sdk = std::make_shared<imstk::SimulationManager>();
	auto scene = sdk->createNewScene("SceneTestDevice");

	// Device Client
	auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

	// Device Server
	auto server = std::make_shared<imstk::HDAPIDeviceServer>();
	server->addDeviceClient(client);
	sdk->addModule(server);

	//auto trackingCtrl = std::make_shared<imstk::DeviceTracker>(client0);
	////trackingCtrl->setTranslationScaling(100);
	//auto lapToolController = std::make_shared<imstk::SceneObjectController>(pbdObj, trackingCtrl);
	///*auto lapToolController = std::make_shared<imstk::LaparoscopicToolController>(pivot, upperJaw, lowerJaw, trackingCtrl);
	//lapToolController->setJawRotationAxis(imstk::Vec3d(1.0, 0, 0));*/
	//scene->addObjectController(lapToolController);

	// Object
	//auto geom = std::make_shared<imstk::Cube>();
	//geom->setPosition(imstk::UP_VECTOR);
	//geom->scale(2);

	std::string path2obj = DATA_ROOT_PATH"/knife3.obj";  // blade2.obj  cube.obj
	auto collidingMesh = imstk::MeshIO::read(path2obj);
	auto visualMesh = imstk::MeshIO::read(path2obj);
	auto physicsMesh = imstk::MeshIO::read(path2obj);
	//objmesh->translate(-270, 0, 70);
	//auto geom = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objmesh);
	//auto viusalMesh = imstk::MeshIO::read(path2obj);
	//Vec3d min1, max1;
	//geom->computeBoundingBox(min1, max1);

	//FILE *outpointtri;
	//outpointtri = fopen("G:/AABB.txt", "w");
	//fprintf(outpointtri, "AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
	//fclose(outpointtri);

	auto object = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject");

	auto objMapP2V = std::make_shared<imstk::OneToOneMap>();
	objMapP2V->setMaster(physicsMesh);
	objMapP2V->setSlave(visualMesh);
	objMapP2V->compute();


	auto objMapP2C = std::make_shared<imstk::OneToOneMap>();
	objMapP2C->setMaster(physicsMesh);
	objMapP2C->setSlave(collidingMesh);
	objMapP2C->compute();

	auto objMapC2V = std::make_shared<imstk::OneToOneMap>();
	objMapC2V->setMaster(collidingMesh);
	objMapC2V->setSlave(visualMesh);
	objMapC2V->compute();

	auto objMapC2P = std::make_shared<imstk::OneToOneMap>();
	objMapC2P->setMaster(collidingMesh);
	objMapC2P->setSlave(physicsMesh);
	objMapC2P->compute();

	object->setVisualGeometry(visualMesh);
	object->setCollidingGeometry(collidingMesh);
	object->setPhysicsGeometry(physicsMesh);
	object->setPhysicsToCollidingMap(objMapP2C);
	object->setPhysicsToVisualMap(objMapP2V);
	object->setCollidingToVisualMap(objMapC2V);
	object->setColldingToPhysicsMap(objMapC2P);

	auto pbdModel = std::make_shared<PbdModel>();
    pbdModel->configure(/*Number of Constraints*/1,
		/*Constraint configuration*/"Distance 0.1",  
		/*Mass*/0.0,  
		/*Gravity*/"0 0 0",
		/*TimeStep*/0.001,
		/*FixedPoint*/"",
		/*NumberOfIterationInConstraintSolver*/5,
		/*Proximity*/0.1,
		/*Contact stiffness*/0.01);   
    object->setDynamicalModel(pbdModel);

	//auto pbdSolver = std::make_shared<PbdSolver>();
	//pbdSolver->setPbdObject(object);
	//scene->addNonlinearSolver(pbdSolver);

	scene->addSceneObject(object);

	auto trackCtrl = std::make_shared<imstk::DeviceTracker>(client);
	trackCtrl->setTranslationScaling(0.5);
	auto controller = std::make_shared<imstk::PBDSceneObjectController>(object, trackCtrl);
	scene->addObjectController(controller);

	// Update Camera position
	auto cam = scene->getCamera();
	cam->setPosition(imstk::Vec3d(0, 10, -150));
	cam->setFocalPoint(imstk::Vec3d(0, -40, 0));
	cam->setViewAngle(60);

	// floor

	imstk::StdVectorOfVec3d vertList;
	double width = 100.0;
	double height = 100.0;
	int nRows = 10;
	int nCols =10;
	int corner[4] = { 1, nRows, nRows*nCols - nCols + 1, nRows*nCols };
	char intStr[33];
	std::string fixed_corner;
	for (unsigned int i = 0; i < 4; ++i)
	{
		std::sprintf(intStr, "%d", corner[i]);
		fixed_corner += std::string(intStr) + ' ';
	}
	vertList.resize(nRows*nCols);
	const double dy = width / (double)(nCols - 1);
	const double dx = height / (double)(nRows - 1);
	for (int i = 0; i < nRows; ++i)
	{
		for (int j = 0; j < nCols; j++)
		{
			const double y = (double)dy*j;
			const double x = (double)dx*i;
			vertList[i*nCols + j] = Vec3d(x - 50, -10.0, y - 50);

		}
	}

	// c. Add connectivity data
	std::vector<imstk::SurfaceMesh::TriangleArray> triangles;
	for (std::size_t i = 0; i < nRows - 1; ++i)
	{
		for (std::size_t j = 0; j < nCols - 1; j++)
		{
			imstk::SurfaceMesh::TriangleArray tri[2];
			tri[0] = { { i*nCols + j, i*nCols + j + 1, (i + 1)*nCols + j } };
			tri[1] = { { (i + 1)*nCols + j + 1, (i + 1)*nCols + j, i*nCols + j + 1 } };
			triangles.push_back(tri[0]);
			triangles.push_back(tri[1]);
		}
	}
	auto floorMeshColliding = std::make_shared<imstk::SurfaceMesh>();
	floorMeshColliding->initialize(vertList, triangles);
	auto floorMeshVisual = std::make_shared<imstk::SurfaceMesh>();
	floorMeshVisual->initialize(vertList, triangles);
	auto floorMeshPhysics = std::make_shared<imstk::SurfaceMesh>();
	floorMeshPhysics->initialize(vertList, triangles);


	auto floorMapP2V = std::make_shared<imstk::OneToOneMap>();
	floorMapP2V->setMaster(floorMeshPhysics);
	floorMapP2V->setSlave(floorMeshVisual);
	floorMapP2V->compute();


	auto floorMapP2C = std::make_shared<imstk::OneToOneMap>();
	floorMapP2C->setMaster(floorMeshPhysics);
	floorMapP2C->setSlave(floorMeshColliding);
	floorMapP2C->compute();

	auto floorMapC2V = std::make_shared<imstk::OneToOneMap>();
	floorMapC2V->setMaster(floorMeshColliding);
	floorMapC2V->setSlave(floorMeshVisual);
	floorMapC2V->compute();

	auto floor = std::make_shared<PbdObject>("Floor");
	floor->setCollidingGeometry(floorMeshColliding);
	floor->setVisualGeometry(floorMeshVisual);
	floor->setPhysicsGeometry(floorMeshPhysics);
	floor->setPhysicsToCollidingMap(floorMapP2C);
	floor->setPhysicsToVisualMap(floorMapP2V);
	floor->setCollidingToVisualMap(floorMapC2V);

	auto pbdModel2 = std::make_shared<PbdModel>();
    pbdModel2->configure(/*Number of Constraints*/2,
		/*Constraint configuration*/"Distance 0.1",
		/*Constraint configuration*/"Dihedral 0.001",
		/*Mass*/0.1,
		/*Gravity*/"0 -9.8 0",
		/*TimeStep*/0.001,
		/*FixedPoint*/fixed_corner.c_str(),
		/*NumberOfIterationInConstraintSolver*/5,
		/*Proximity*/0.1,
		/*Contact stiffness*/0.1);
    floor->setDynamicalModel(pbdModel2);

	auto pbdSolverfloor = std::make_shared<PbdSolver>();
	pbdSolverfloor->setPbdObject(floor);
	scene->addNonlinearSolver(pbdSolverfloor);

	scene->addSceneObject(floor);

	// Collisions
	auto colGraph = scene->getCollisionGraph();
	auto pair = std::make_shared<PbdInteractionPair>(PbdInteractionPair(object, floor));
	pair->setNumberOfInterations(2);

	colGraph->addInteractionPair(pair);

	// Run
	sdk->setActiveScene(scene);
	sdk->startSimulation(true);
#endif
}

void testPBDVCController()
{
#ifdef iMSTK_USE_OPENHAPTICS
	// SDK and Scene
	auto sdk = std::make_shared<imstk::SimulationManager>();
	auto scene = sdk->createNewScene("SceneTestDevice");

	// Device Client
	auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

	// Device Server
	auto server = std::make_shared<imstk::HDAPIDeviceServer>();
	server->addDeviceClient(client);
	sdk->addModule(server);

	//auto trackingCtrl = std::make_shared<imstk::DeviceTracker>(client0);
	////trackingCtrl->setTranslationScaling(100);
	//auto lapToolController = std::make_shared<imstk::SceneObjectController>(pbdObj, trackingCtrl);
	///*auto lapToolController = std::make_shared<imstk::LaparoscopicToolController>(pivot, upperJaw, lowerJaw, trackingCtrl);
	//lapToolController->setJawRotationAxis(imstk::Vec3d(1.0, 0, 0));*/
	//scene->addObjectController(lapToolController);

	// Object
	//auto geom = std::make_shared<imstk::Cube>();
	//geom->setPosition(imstk::UP_VECTOR);
	//geom->scale(2);

	std::string path2obj = DATA_ROOT_PATH"/knife3.obj";  // blade2.obj  cube.obj
	auto geom = imstk::MeshIO::read(path2obj);
	//objmesh->translate(-270, 0, 70);
	//auto geom = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objmesh);
	//auto viusalMesh = imstk::MeshIO::read(path2obj);
	Vec3d min1, max1;
	geom->computeBoundingBox(min1, max1);

	FILE *outpointtri;
	outpointtri = fopen("G:/AABB.txt", "w");
	fprintf(outpointtri, "AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
	fclose(outpointtri);

	auto object = std::make_shared<imstk::PbdVirtualCouplingObject>("VirtualObject");
	
	auto objMapP2V = std::make_shared<imstk::OneToOneMap>();
	objMapP2V->setMaster(geom);
	objMapP2V->setSlave(geom);
	objMapP2V->compute();


	auto objMapP2C = std::make_shared<imstk::OneToOneMap>();
	objMapP2C->setMaster(geom);
	objMapP2C->setSlave(geom);
	objMapP2C->compute();

	auto objMapC2V = std::make_shared<imstk::OneToOneMap>();
	objMapC2V->setMaster(geom);
	objMapC2V->setSlave(geom);
	objMapC2V->compute();

	object->setVisualGeometry(geom);
	object->setCollidingGeometry(geom);
	object->setPhysicsGeometry(geom);
	object->setPhysicsToCollidingMap(objMapP2C);
	object->setPhysicsToVisualMap(objMapP2V);
	object->setCollidingToVisualMap(objMapC2V);

	auto pbdModel = std::make_shared<PbdModel>();
    pbdModel->configure(/*Number of Constraints*/2,
		/*Constraint configuration*/"Area 0.9",  //"Distance 0.1",
		/*Constraint configuration*/"Distance 1.0",  // "Dihedral 0.001",
		/*Mass*/1.0,  // org 1.0
		/*Gravity*/"0 -9.8 0",
		/*TimeStep*/0.001,
		/*FixedPoint*/"",
		/*NumberOfIterationInConstraintSolver*/10,
		/*Proximity*/0.1,
		/*Contact stiffness*/0.01);  // org 0.01
    object->setDynamicalModel(pbdModel);

	auto pbdSolver = std::make_shared<PbdSolver>();
	pbdSolver->setPbdObject(object);
	scene->addNonlinearSolver(pbdSolver);

	scene->addSceneObject(object);

    auto trackCtrl = std::make_shared<imstk::DeviceTracker>(client);
    trackCtrl->setTranslationScaling(0.5);
    auto controller = std::make_shared<imstk::PBDSceneObjectController>(object, trackCtrl);
    scene->addObjectController(controller);

	// Update Camera position
	auto cam = scene->getCamera();
	cam->setPosition(imstk::Vec3d(0, 10, -150));
	cam->setFocalPoint(imstk::Vec3d(0, -40, 0));
	cam->setViewAngle(60);

	// floor

	imstk::StdVectorOfVec3d vertList;
	double width = 100.0;
	double height = 100.0;
	int nRows = 2;
	int nCols = 2;
	vertList.resize(nRows*nCols);
	const double dy = width / (double)(nCols - 1);
	const double dx = height / (double)(nRows - 1);
	for (int i = 0; i < nRows; ++i)
	{
		for (int j = 0; j < nCols; j++)
		{
			const double y = (double)dy*j;
			const double x = (double)dx*i;
			vertList[i*nCols + j] = Vec3d(x - 50, -10.0, y - 50);

		}
	}

	// c. Add connectivity data
	std::vector<imstk::SurfaceMesh::TriangleArray> triangles;
	for (std::size_t i = 0; i < nRows - 1; ++i)
	{
		for (std::size_t j = 0; j < nCols - 1; j++)
		{
			imstk::SurfaceMesh::TriangleArray tri[2];
			tri[0] = { { i*nCols + j, i*nCols + j + 1, (i + 1)*nCols + j } };
			tri[1] = { { (i + 1)*nCols + j + 1, (i + 1)*nCols + j, i*nCols + j + 1 } };
			triangles.push_back(tri[0]);
			triangles.push_back(tri[1]);
		}
	}
	auto floorMeshColliding = std::make_shared<imstk::SurfaceMesh>();
	floorMeshColliding->initialize(vertList, triangles);
	auto floorMeshVisual = std::make_shared<imstk::SurfaceMesh>();
	floorMeshVisual->initialize(vertList, triangles);
	auto floorMeshPhysics = std::make_shared<imstk::SurfaceMesh>();
	floorMeshPhysics->initialize(vertList, triangles);


	auto floorMapP2V = std::make_shared<imstk::OneToOneMap>();
	floorMapP2V->setMaster(floorMeshPhysics);
	floorMapP2V->setSlave(floorMeshVisual);
	floorMapP2V->compute();


	auto floorMapP2C = std::make_shared<imstk::OneToOneMap>();
	floorMapP2C->setMaster(floorMeshPhysics);
	floorMapP2C->setSlave(floorMeshColliding);
	floorMapP2C->compute();

	auto floorMapC2V = std::make_shared<imstk::OneToOneMap>();
	floorMapC2V->setMaster(floorMeshColliding);
	floorMapC2V->setSlave(floorMeshVisual);
	floorMapC2V->compute();

	auto floor = std::make_shared<PbdObject>("Floor");
	floor->setCollidingGeometry(floorMeshColliding);
	floor->setVisualGeometry(floorMeshVisual);
	floor->setPhysicsGeometry(floorMeshPhysics);
	floor->setPhysicsToCollidingMap(floorMapP2C);
	floor->setPhysicsToVisualMap(floorMapP2V);
	floor->setCollidingToVisualMap(floorMapC2V);

	auto pbdModel2 = std::make_shared<PbdModel>();
    pbdModel2->configure(/*Number of Constraints*/0,
		/*Mass*/0.0,
		/*Proximity*/0.1,
		/*Contact stiffness*/1.0);
    floor->setDynamicalModel(pbdModel2);

	auto pbdSolverfloor = std::make_shared<PbdSolver>();
	pbdSolverfloor->setPbdObject(floor);
	scene->addNonlinearSolver(pbdSolverfloor);

	scene->addSceneObject(floor);

	// Collisions
	auto colGraph = scene->getCollisionGraph();
	auto pair = std::make_shared<PbdInteractionPair>(PbdInteractionPair(object, floor));
	pair->setNumberOfInterations(2);

	colGraph->addInteractionPair(pair);

	// Run
	sdk->setActiveScene(scene);
	sdk->startSimulation(false);
#endif
}

void testPBDObj()
{
#ifdef iMSTK_USE_OPENHAPTICS
	// SDK and Scene
	auto sdk = std::make_shared<imstk::SimulationManager>();
	auto scene = sdk->createNewScene("PBDObjectTest");

	// Device clients
	auto client0 = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

	// Device Server
	auto server = std::make_shared<imstk::HDAPIDeviceServer>();
	server->addDeviceClient(client0);
	sdk->addModule(server);

	/*auto trackingCtrl = std::make_shared<imstk::DeviceTracker>(client0);
	auto blade = std::make_shared<imstk::PBDVirtualCouplingObject>("blade", trackingCtrl);*/

	auto pbdObj = std::make_shared<PbdVirtualCouplingObject>("Tool"); // <PbdRigidObject>("Tool");


	std::string path2obj = DATA_ROOT_PATH"/blade2.obj";

	//auto collidingMesh = imstk::MeshIO::read(path2obj);
	
	//auto viusalMesh = imstk::MeshIO::read(path2obj);
	auto collidingMesh=std::make_shared<imstk::LineMesh>();
	auto viusalMesh = std::make_shared<imstk::LineMesh>();
	auto physicsMesh = std::make_shared<imstk::LineMesh>(); // imstk::MeshIO::read(path2obj);

	Vec3d min1, max1;
	collidingMesh->computeBoundingBox(min1, max1);

	imstk::StdVectorOfVec3d vertList;
	vertList.resize(2);
	/*vertList[0] = Vec3d(-10.0, 0.0, -0.5);
	vertList[1] = Vec3d(0.0, 0.0, -0.5);*/ // blade AABB
	vertList[0] = Vec3d(0.0, -26, -30);
	vertList[1] = Vec3d(0.0, -36, -30);
	std::vector<std::vector<int> > connectivity;
	for (int i = 0; i < 1;){
		std::vector<int> line;
		line.push_back(i);
		i++;
		line.push_back(i);

		connectivity.push_back(line);
	}

	physicsMesh->initialize(vertList);
	physicsMesh->setConnectivity(connectivity);

	collidingMesh->initialize(vertList);
	collidingMesh->setConnectivity(connectivity);

	viusalMesh->initialize(vertList); //  ->setInitialVerticesPositions(vertList);
	viusalMesh->setConnectivity(connectivity);

	auto bladeMapP2V = std::make_shared<imstk::OneToOneMap>();
	bladeMapP2V->setMaster(physicsMesh);
	bladeMapP2V->setSlave(viusalMesh);
	bladeMapP2V->compute();

	auto bladeMapP2C = std::make_shared<imstk::OneToOneMap>();
	bladeMapP2C->setMaster(physicsMesh);
	bladeMapP2C->setSlave(collidingMesh);
	bladeMapP2C->compute();

	auto bladeMapC2V = std::make_shared<imstk::OneToOneMap>();
	bladeMapC2V->setMaster(collidingMesh);
	bladeMapC2V->setSlave(viusalMesh);
	bladeMapC2V->compute();

	auto bladeMapC2P = std::make_shared<imstk::OneToOneMap>();
	bladeMapC2P->setMaster(collidingMesh);
	bladeMapC2P->setSlave(physicsMesh);
	bladeMapC2P->compute();

	pbdObj->setCollidingGeometry(collidingMesh);
	pbdObj->setVisualGeometry(viusalMesh);
	pbdObj->setPhysicsGeometry(physicsMesh);
	pbdObj->setPhysicsToCollidingMap(bladeMapP2C);
	pbdObj->setCollidingToVisualMap(bladeMapC2V);
	pbdObj->setPhysicsToVisualMap(bladeMapP2V);
	pbdObj->setColldingToPhysicsMap(bladeMapC2P);

	auto pbdModel = std::make_shared<PbdModel>();
    pbdModel->configure(/*Number of constraints*/1,
		/*Constraint configuration*/"Distance 0.1",
		/*Mass*/0.1,
		/*Gravity*/"0 0 0",
		/*TimeStep*/0.001,
		/*FixedPoint*/"",
		/*NumberOfIterationInConstraintSolver*/5,
		/*Proximity*/0.1,
		/*Contact stiffness*/0.01);
    pbdObj->setDynamicalModel(pbdModel);

	auto pbdSolver = std::make_shared<PbdSolver>();
	pbdSolver->setPbdObject(pbdObj);
	scene->addNonlinearSolver(pbdSolver);

	scene->addSceneObject(pbdObj);

	 //Update Camera position
	auto cam = scene->getCamera();
	cam->setPosition(imstk::Vec3d(0, -20, -100));  //  (-11, -1.5, 5.5)put it in the middle of colon model  colon (-10, -1.5, 6)  cube (-15, 20, -5)
	cam->setFocalPoint(imstk::Vec3d(0, -20, 0)); //  (-15, -1.5, 5.5) colon (-14, -1.5, 6)  cube (-19, 20, -5)
	//cam->setViewUp(0, 1, 0);

	//auto camController = cam->setupController(client0); // auto
	//camController->setTranslationScaling(0.3); // 0.2 colon

	//auto controller0 = std::make_shared<imstk::PBDSceneObjectController>(pbdObj, camController);  // SceneObjectController
	//scene->addObjectController(controller0);

	

	//auto sphereStartGeom = std::make_shared<imstk::Sphere>();
	//sphereStartGeom->setPosition(imstk::Vec3d(-15, 20, -5)); // (-13, -2.0, 6.0) colon  (-10, -1.5, 6));  -10, -1.5, 6  cube -15, 20, -5
	//sphereStartGeom->setRadius(1.0); // colon 0.2 cube 1.0

	////auto sphereStartObj = std::make_shared<imstk::CollidingObject>("SphereStart");
	//auto sphereStartObj = std::make_shared<PbdVirtualCouplingObject>("SphereStart");
	//sphereStartObj->setVisualGeometry(sphereStartGeom);
	//sphereStartObj->setCollidingGeometry(sphereStartGeom);
	//sphereStartObj->setPhysicsGeometry(sphereStartGeom);
	////scene->addSceneObject(sphereStartObj);

	//// Object
	//auto geom = std::make_shared<imstk::Cube>();
	//geom->setPosition(imstk::UP_VECTOR);
	//geom->scale(2);

	//auto object = std::make_shared<imstk::CollidingObject>("VirtualObject");
	//object->setVisualGeometry(geom);
	//object->setCollidingGeometry(geom);
	////scene->addSceneObject(object);

	auto trackingCtrl = std::make_shared<imstk::DeviceTracker>(client0);
	//trackingCtrl->setTranslationScaling(100);
	auto lapToolController = std::make_shared<imstk::SceneObjectController>(pbdObj, trackingCtrl);
	/*auto lapToolController = std::make_shared<imstk::LaparoscopicToolController>(pivot, upperJaw, lowerJaw, trackingCtrl);
	lapToolController->setJawRotationAxis(imstk::Vec3d(1.0, 0, 0));*/
	scene->addObjectController(lapToolController);

	//imstk::StdVectorOfVec3d vertList;
	double width = 60.0;
	double height = 60.0;
	int nRows = 5; // 20;
	int nCols = 5; //  20;
	int corner[4] = { 1, nRows, nRows*nCols - nCols + 1, nRows*nCols };
	char intStr[33];
	std::string fixed_corner;
	for (unsigned int i = 0; i < 4; ++i)
	{
		std::sprintf(intStr, "%d", corner[i]);
		fixed_corner += std::string(intStr) + ' ';
	}

	FILE *output;
	output = fopen("G:/vert.txt", "w");

	vertList.resize(nRows*nCols);
	const double dy = width / (double)(nCols - 1);
	const double dx = height / (double)(nRows - 1);
	for (int i = 0; i < nRows; ++i)
	{
		for (int j = 0; j < nCols; j++)
		{
			const double y = (double)dy*j;
			const double x = (double)dx*i;
			vertList[i*nCols + j] = Vec3d(x - 30, -25, y - 60);
			fprintf(output, "%f -25 %f\n", x - 30,  y - 60);
		}
		fprintf(output, "\n");
	}
	fclose(output);
	// c. Add connectivity data
	std::vector<imstk::SurfaceMesh::TriangleArray> triangles;
	for (size_t i = 0; i < nRows - 1; ++i)
	{
		for (size_t j = 0; j < nCols - 1; j++)
		{
			imstk::SurfaceMesh::TriangleArray tri[2];
			tri[0] = { { i*nCols + j, i*nCols + j + 1, (i + 1)*nCols + j } };
			tri[1] = { { (i + 1)*nCols + j + 1, (i + 1)*nCols + j, i*nCols + j + 1 } };
			triangles.push_back(tri[0]);
			triangles.push_back(tri[1]);
		}
	}

	auto clothMeshVisual = std::make_shared<imstk::SurfaceMesh>();
	clothMeshVisual->initialize(vertList, triangles);
	auto clothMeshColliding = std::make_shared<imstk::SurfaceMesh>();
	clothMeshColliding->initialize(vertList, triangles);
	auto clothMeshPhysics = std::make_shared<imstk::SurfaceMesh>();
	clothMeshPhysics->initialize(vertList, triangles);

	auto clothMapP2V = std::make_shared<imstk::OneToOneMap>();
	clothMapP2V->setMaster(clothMeshPhysics);
	clothMapP2V->setSlave(clothMeshVisual);
	clothMapP2V->compute();

	auto clothMapC2V = std::make_shared<imstk::OneToOneMap>();
	clothMapC2V->setMaster(clothMeshColliding);
	clothMapC2V->setSlave(clothMeshVisual);
	clothMapC2V->compute();

	auto clothMapP2C = std::make_shared<imstk::OneToOneMap>();
	clothMapP2C->setMaster(clothMeshPhysics);
	clothMapP2C->setSlave(clothMeshColliding);
	clothMapP2C->compute();

	//auto clothMapC2P = std::make_shared<imstk::OneToOneMap>();
	//clothMapC2P->setMaster(clothMeshColliding);
	//clothMapC2P->setSlave(clothMeshPhysics);
	//clothMapC2P->compute();


	auto floor = std::make_shared<PbdObject>("cloth");
	floor->setCollidingGeometry(clothMeshColliding);
	floor->setVisualGeometry(clothMeshVisual);
	floor->setPhysicsGeometry(clothMeshPhysics);
	floor->setPhysicsToCollidingMap(clothMapP2C);
	floor->setPhysicsToVisualMap(clothMapP2V);
	floor->setCollidingToVisualMap(clothMapC2V);
	//floor->setcoll

	auto pbdModel2 = std::make_shared<PbdModel>();
    pbdModel2->configure(/*Number of constraints*/2,
		/*Constraint configuration*/"Distance 0.1",
		/*Constraint configuration*/"Dihedral 0.001",
		/*Mass*/0.1,
		/*Gravity*/"0 -9.8 0",
		/*TimeStep*/0.001,
		/*FixedPoint*/fixed_corner.c_str(),
		/*NumberOfIterationInConstraintSolver*/5,
		/*Proximity*/0.1,
		/*Contact stiffness*/0.1);
    floor->setDynamicalModel(pbdModel2);

	auto pbdSolver2 = std::make_shared<PbdSolver>();
	pbdSolver2->setPbdObject(floor);
	scene->addNonlinearSolver(pbdSolver2);

	scene->addSceneObject(floor);

	std::cout << "nbr of vertices in cloth mesh" << clothMeshVisual->getNumVertices() << std::endl;

	// Collisions
	auto clothTestcolGraph = scene->getCollisionGraph();
	auto pair1 = std::make_shared<PbdInteractionPair>(PbdInteractionPair(pbdObj, floor));  // sphereStartObj  pbdObj
	pair1->setNumberOfInterations(5);

	clothTestcolGraph->addInteractionPair(pair1);

	//scene->getCamera()->setPosition(0, 0, 50);

	// Run
	sdk->setActiveScene(scene);
	sdk->startSimulation(true);
#endif
}

//void testPBDVirtualCoupling()
//{
//#ifdef iMSTK_USE_OPENHAPTICS
//	// SDK and Scene
//	auto sdk = std::make_shared<imstk::SimulationManager>();
//	auto scene = sdk->createNewScene("LiverToolInteraction");
//
//	// Device clients
//	auto client0 = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");
//
//	// Device Server
//	auto server = std::make_shared<imstk::HDAPIDeviceServer>();
//	server->addDeviceClient(client0);
//	sdk->addModule(server);
//
//	auto trackingCtrl = std::make_shared<imstk::DeviceTracker>(client0);
//	//trackingCtrl->setTranslationScaling(100);
//	auto tool = std::make_shared<imstk::PBDVirtualCouplingObject>("tool", trackingCtrl);
//
//	// Sphere0
//	//auto sphere0Obj = apiutils::createCollidingAnalyticalSceneObject(
//	//	imstk::Geometry::Type::Sphere, scene, "Sphere0", 3, Vec3d(1, 0.5, 0));
//
//	auto lineMeshColliding = std::make_shared<imstk::LineMesh>();
//	auto lineMeshVisual = std::make_shared < imstk::LineMesh >();
//	auto lineMeshPhysics = std::make_shared < imstk::LineMesh >();
//
//	imstk::StdVectorOfVec3d vertList;
//	vertList.resize(2);
//	vertList[0] = Vec3d(0.0, 0.0, 0.0);
//	vertList[1] = Vec3d(0.0, 0.0, 20.0);
//	std::vector<std::vector<int>> connectivity;
//	for (int i = 0; i < 1;){
//		std::vector<int> line;
//		line.push_back(i);
//		i++;
//		line.push_back(i);
//
//		connectivity.push_back(line);
//	}
//
//	lineMeshColliding->initialize(vertList);
//	lineMeshColliding->setConnectivity(connectivity);
//
//	lineMeshPhysics->initialize(vertList);
//	lineMeshPhysics->setConnectivity(connectivity);
//
//	lineMeshVisual->initialize(vertList);
//	lineMeshVisual->setConnectivity(connectivity);
//
//	auto mapC2P = std::make_shared<imstk::OneToOneMap>();
//	mapC2P->setMaster(lineMeshColliding);
//	mapC2P->setSlave(lineMeshPhysics);
//	mapC2P->compute();
//
//	auto mapC2V = std::make_shared<imstk::OneToOneMap>();
//	mapC2V->setMaster(lineMeshColliding);
//	mapC2V->setSlave(lineMeshVisual);
//
//	auto mapP2C = std::make_shared<imstk::OneToOneMap>();
//	mapP2C->setMaster(lineMeshPhysics);
//	mapP2C->setSlave(lineMeshColliding);
//	mapP2C->compute();
//
//	auto mapP2V = std::make_shared<imstk::OneToOneMap>();
//	mapP2V->setMaster(lineMeshPhysics);
//	mapP2V->setSlave(lineMeshVisual);
//	mapP2V->compute();
//
//	auto sphere0Geom = std::make_shared<imstk::Sphere>();
//	sphere0Geom->setPosition(imstk::Vec3d(1, 0.5, 0)); // (-11, -1.5, 5.5) colon  (-10, -1.5, 6));  -10, -1.5, 6  cube -15, 20, -5
//	sphere0Geom->setRadius(1.0); // colon 0.2 cube 1.0
//
//	tool->setCollidingGeometry(lineMeshColliding);
//	tool->setVisualGeometry(lineMeshVisual);
//	tool->setPhysicsGeometry(lineMeshPhysics);
//	tool->setPhysicsToCollidingMap(mapP2C);
//	tool->setColldingToPhysicsMap(mapC2P);
//	tool->setPhysicsToVisualMap(mapP2V);
//	tool->setCollidingToVisualMap(mapC2V);
//	/*tool->setVisualGeometry(sphere0Geom);
//	tool->setCollidingGeometry(sphere0Geom);*/
//	scene->addSceneObject(tool);
//
//
//
//	//auto trackingCtrl = std::make_shared<imstk::DeviceTracker>(client0);
//	////trackingCtrl->setTranslationScaling(100);
//	//auto lapToolController = std::make_shared<imstk::PBDVirtualCouplingObject>("SphereTest", sphere0Obj, trackingCtrl);
//	///*auto lapToolController = std::make_shared<imstk::LaparoscopicToolController>(pivot, upperJaw, lowerJaw, trackingCtrl);
//	//lapToolController->setJawRotationAxis(imstk::Vec3d(1.0, 0, 0));*/
//	//scene->addObjectController(lapToolController);
//
//	// Set Camera configuration
//	auto cam = scene->getCamera();
//	cam->setPosition(imstk::Vec3d(0, 20, 20));
//	cam->setFocalPoint(imstk::Vec3d(0, 0, 0));
//
//	// Run
//	sdk->setActiveScene(scene);
//	sdk->startSimulation(true);
//#endif
//}

void testController()
{
#ifdef iMSTK_USE_OPENHAPTICS
	// SDK and Scene
	auto sdk = std::make_shared<imstk::SimulationManager>();
	auto scene = sdk->createNewScene("SceneTestDevice");

	// Device Client
	auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

	// Device Server
	auto server = std::make_shared<imstk::HDAPIDeviceServer>();
	server->addDeviceClient(client);
	sdk->addModule(server);

	// Object
	std::string path2obj = DATA_ROOT_PATH"/cube.obj";  // blade2.obj
	auto objmesh = imstk::MeshIO::read(path2obj);
	auto geom = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objmesh);
	//auto viusalMesh = imstk::MeshIO::read(path2obj);
	Vec3d min1, max1;
	geom->computeBoundingBox(min1, max1);

	FILE *outpointtri;
	outpointtri = fopen("G:/AABB.txt", "a");
	fprintf(outpointtri, "AABB: min %f %f %f max %f %f %f \n ", min1.x(), min1.y(), min1.z(), max1.x(), max1.y(), max1.z());
	fclose(outpointtri);

	auto object = std::make_shared<imstk::CollidingObject>("VirtualObject");
	object->setVisualGeometry(geom);
	object->setCollidingGeometry(geom);
	scene->addSceneObject(object);

	auto trackCtrl = std::make_shared<imstk::DeviceTracker>(client);
	trackCtrl->setTranslationScaling(0.1);
	auto controller = std::make_shared<imstk::SceneObjectController>(object, trackCtrl);
	scene->addObjectController(controller);

	// Update Camera position
	auto cam = scene->getCamera();
	cam->setPosition(imstk::Vec3d(0, 0, 10));
	cam->setFocalPoint(geom->getTranslation());

	// Run
	sdk->setActiveScene(scene);
	sdk->startSimulation(true);
#endif
}

void testPBDLineMesh()
{
#ifdef iMSTK_USE_OPENHAPTICS
	vtkSmartPointer<vtkActor> actor =
		vtkSmartPointer<vtkActor>::New();

	// SDK and Scene
	auto sdk = std::make_shared<imstk::SimulationManager>();
	auto scene = sdk->createNewScene("testLineMeshVESS");

	// Device clients
	auto client0 = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");

	// Device Server
	auto server = std::make_shared<imstk::HDAPIDeviceServer>();
	server->addDeviceClient(client0);
	sdk->addModule(server);

	// Update Camera position
	/*	auto cam = scene->getCamera();
		cam->setPosition(imstk::Vec3d(0, 0, 0));*/  //  (-11, -1.5, 5.5)put it in the middle of colon model  colon (-10, -1.5, 6)  cube (-15, 20, -5)

	//auto camController = cam->setupController(client0); // auto
	//camController->setTranslationScaling(0.3); // 0.2 colon

	auto blade = std::make_shared<imstk::VirtualCouplingPBDObject>("blade", client0);
	bool line = true;
	//auto tool = std::make_shared<imstk::VirtualCouplingPBDObject>("tool", client0);

	std::string path2obj = DATA_ROOT_PATH"/blade2.obj";

	auto collidingMesh = imstk::MeshIO::read(path2obj);
	auto viusalMesh = imstk::MeshIO::read(path2obj);
	auto physicsMesh = imstk::MeshIO::read(path2obj);

	auto bladeMapP2V = std::make_shared<imstk::OneToOneMap>();
	bladeMapP2V->setMaster(physicsMesh);
	bladeMapP2V->setSlave(viusalMesh);
	bladeMapP2V->compute();

	auto bladeMapP2C = std::make_shared<imstk::OneToOneMap>();
	bladeMapP2C->setMaster(physicsMesh);
	bladeMapP2C->setSlave(collidingMesh);
	bladeMapP2C->compute();

	auto bladeMapC2V = std::make_shared<imstk::OneToOneMap>();
	bladeMapC2V->setMaster(collidingMesh);
	bladeMapC2V->setSlave(viusalMesh);
	bladeMapC2V->compute();

	auto bladeMapC2P = std::make_shared<imstk::OneToOneMap>();
	bladeMapC2P->setMaster(collidingMesh);
	bladeMapC2P->setSlave(physicsMesh);
	bladeMapC2P->compute();

	auto pbdModel = std::make_shared<PbdModel>();
    pbdModel->configure(/*Number of constraints*/1,
        /*Constraint configuration*/"Distance 0.1",
        /*Mass*/0.0,
        /*Gravity*/"0 0 0",
        /*TimeStep*/0.001,
        /*FixedPoint*/"",
        /*NumberOfIterationInConstraintSolver*/5,
        /*Proximity*/0.1,
        /*Contact stiffness*/0.01);

    blade->setDynamicalModel(pbdModel);
	blade->setCollidingGeometry(collidingMesh);
	blade->setVisualGeometry(viusalMesh);
	blade->setPhysicsGeometry(physicsMesh);
	blade->setPhysicsToCollidingMap(bladeMapP2C);
	blade->setCollidingToVisualMap(bladeMapC2V);
	blade->setPhysicsToVisualMap(bladeMapP2V);
	blade->setColldingToPhysicsMap(bladeMapC2P);

	scene->addSceneObject(blade);

	//auto controller0 = std::make_shared<imstk::SceneObjectController>(blade, camController);  // SceneObjectController
	//scene->addObjectController(controller0);


	//// Read surface mesh
	////auto floorMesh = imstk::MeshIO::read(DATA_ROOT_PATH"/rect5_4.obj"); //  cube_den.dae rect_den.dae colon3_c2 coarse mesh colon3.dae colon dense mesh
	//auto clothMeshVisual = imstk::MeshIO::read(DATA_ROOT_PATH"/cube2IN.obj"); //  std::dynamic_pointer_cast<imstk::SurfaceMesh>(floorMesh);
	//auto clothMeshColliding = imstk::MeshIO::read(DATA_ROOT_PATH"/cube2IN.obj"); // std::make_shared<imstk::SurfaceMesh>(floorMesh);
	//auto clothMeshPhysics = imstk::MeshIO::read(DATA_ROOT_PATH"/cube2IN.obj"); // std::make_shared<imstk::SurfaceMesh>(floorMesh);

	//auto clothMapP2V = std::make_shared<imstk::OneToOneMap>();
	//clothMapP2V->setMaster(clothMeshPhysics);
	//clothMapP2V->setSlave(clothMeshVisual);
	//clothMapP2V->compute();

	//auto clothMapC2V = std::make_shared<imstk::OneToOneMap>();
	//clothMapC2V->setMaster(clothMeshColliding);
	//clothMapC2V->setSlave(clothMeshVisual);
	//clothMapC2V->compute();

	//auto clothMapP2C = std::make_shared<imstk::OneToOneMap>();
	//clothMapP2C->setMaster(clothMeshPhysics);
	//clothMapP2C->setSlave(clothMeshColliding);
	//clothMapP2C->compute();


	//auto floor = std::make_shared<PbdObject>("cloth");

	//auto pbdModel2 = std::make_shared<PbdModel>();
	//floor->setDynamicalModel(pbdModel2);

	//floor->setCollidingGeometry(clothMeshColliding);
	//floor->setVisualGeometry(clothMeshVisual);
	//floor->setPhysicsGeometry(clothMeshPhysics);
	//floor->setPhysicsToCollidingMap(clothMapP2C);
	//floor->setPhysicsToVisualMap(clothMapP2V);
	//floor->setCollidingToVisualMap(clothMapC2V);
	//floor->initialize(/*Number of constraints*/2,
	//	/*Constraint configuration*/"Distance 0.1",
	//	/*Constraint configuration*/"Dihedral 0.001",
	//	/*Mass*/0.1,
	//	/*Gravity*/"0 -9.8 0",
	//	/*TimeStep*/0.001,
	//	/*FixedPoint*/"1 2 3 4 5",  // fixed_corner.c_str(),
	//	/*NumberOfIterationInConstraintSolver*/5,
	//	/*Proximity*/0.1,
	//	/*Contact stiffness*/0.1);
	//scene->addSceneObject(floor);

	//std::cout << "nbr of vertices in cloth mesh %d" << clothMeshVisual->getNumVertices() << std::endl;

	//// Collisions
	//auto clothTestcolGraph = scene->getCollisionGraph();

	//auto pair1 = std::make_shared<PbdInteractionPair>(PbdInteractionPair(blade, floor));
	//pair1->setNumberOfInterations(5);

	//clothTestcolGraph->addInteractionPair(pair1);
	//clothTestcolGraph->addInteractionPair(linesTool, floor,
	//	CollisionDetection::Type::SphereToMesh,
	//	CollisionHandling::Type::Penalty,
	//	CollisionHandling::Type::None);

	//scene->getCamera()->setPosition(0, 0, 50);

	// Run
	sdk->setActiveScene(scene);
	//auto renderer = std::dynamic_pointer_cast<VTKRenderer>(sdk->getViewer()->getCurrentRenderer())->getVtkRenderer();  //  sdk->getViewer()->getCurrentRenderer()->getVtkRenderer();
	//renderer->AddActor(actor);
	sdk->startSimulation(true);

#endif
}


void testVESSPBDCD()
{
	//auto sdk = std::make_shared<SimulationManager>();
	//auto scene = sdk->createNewScene("PbdCollisionTest");

	//scene->getCamera()->setPosition(0, 10.0, 25.0);

	//auto clothMeshVisual = imstk::MeshIO::read(DATA_ROOT_PATH"/rect5_3.obj"); //  std::dynamic_pointer_cast<imstk::SurfaceMesh>(floorMesh);
	//auto clothMeshColliding = imstk::MeshIO::read(DATA_ROOT_PATH"/rect5_3.obj"); // std::make_shared<imstk::SurfaceMesh>(floorMesh);
	//auto clothMeshPhysics = imstk::MeshIO::read(DATA_ROOT_PATH"/rect5_3.obj"); // std::make_shared<imstk::SurfaceMesh>(floorMesh);

	//auto clothMapP2V = std::make_shared<imstk::OneToOneMap>();
	//clothMapP2V->setMaster(clothMeshPhysics);
	//clothMapP2V->setSlave(clothMeshVisual);
	//clothMapP2V->compute();

	//auto clothMapC2V = std::make_shared<imstk::OneToOneMap>();
	//clothMapC2V->setMaster(clothMeshColliding);
	//clothMapC2V->setSlave(clothMeshVisual);
	//clothMapC2V->compute();

	//auto clothMapP2C = std::make_shared<imstk::OneToOneMap>();
	//clothMapP2C->setMaster(clothMeshPhysics);
	//clothMapP2C->setSlave(clothMeshColliding);
	//clothMapP2C->compute();

	//auto floor = std::make_shared<PbdObject>("cloth");
	//floor->setCollidingGeometry(clothMeshColliding);
	//floor->setVisualGeometry(clothMeshVisual);
	//floor->setPhysicsGeometry(clothMeshPhysics);
	//floor->setPhysicsToCollidingMap(clothMapP2C);
	//floor->setPhysicsToVisualMap(clothMapP2V);
	//floor->setCollidingToVisualMap(clothMapC2V);
	//floor->initialize(/*Number of constraints*/2,
	//	/*Constraint configuration*/"Distance 0.1",
	//	/*Constraint configuration*/"Dihedral 0.001",
	//	/*Mass*/0.1,
	//	/*Gravity*/"0 -9.8 0",
	//	/*TimeStep*/0.001,
	//	/*FixedPoint*/"1 2 3 4 5",  // fixed_corner.c_str(),
	//	/*NumberOfIterationInConstraintSolver*/5,
	//	/*Proximity*/0.1,
	//	/*Contact stiffness*/0.1);
	//scene->addSceneObject(floor);

	//std::cout << "nbr of vertices in cloth mesh %d" << clothMeshVisual->getNumVertices() << std::endl;

	//// Collisions
	//auto clothTestcolGraph = scene->getCollisionGraph();

	//auto pair1 = std::make_shared<PbdInteractionPair>(PbdInteractionPair(linesTool, floor));
	//pair1->setNumberOfInterations(5);

	//clothTestcolGraph->addInteractionPair(pair1);

	//scene->getCamera()->setPosition(0, 0, 50);

	//// Run
	//sdk->setActiveScene(scene);
	//sdk->startSimulation(true);
}

//void testLineMeshVESS()
//{
//#ifdef iMSTK_USE_OPENHAPTICS
//	vtkSmartPointer<vtkActor> actor =
//		vtkSmartPointer<vtkActor>::New();
//
//	// SDK and Scene
//	auto sdk = std::make_shared<imstk::SimulationManager>();
//	auto scene = sdk->createNewScene("testLineMeshVESS");
//
//	// Device clients
//	auto client0 = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");
//
//	// Device Server
//	auto server = std::make_shared<imstk::HDAPIDeviceServer>();
//	server->addDeviceClient(client0);
//	sdk->addModule(server);
//
//	auto linesTool = std::make_shared<imstk::VirtualCouplingPBDObject>("linesTool", client0);
//	bool line=true;
//	//auto tool = std::make_shared<imstk::VirtualCouplingPBDObject>("tool", client0);
//
//	// Read LineMesh
//	auto lineMeshColliding = std::make_shared<imstk::LineMesh>();
//	auto lineMeshVisual = std::make_shared<imstk::LineMesh>();
//	auto lineMeshPhysics = std::make_shared<imstk::LineMesh>();
//
//	imstk::StdVectorOfVec3d vertList;
//	vertList.resize(3);
//	vertList[0] = Vec3d(0.0, -10.0, -10.0);
//	vertList[1] = Vec3d(0.0, 0.0, -10.0);
//	vertList[2] = Vec3d(0.0, 0.0, -30.0);
//	std::vector<std::vector<int> > connectivity;
//	for (int i = 0; i < 2;){
//		std::vector<int> line;
//		line.push_back(i);
//		i++;
//		line.push_back(i);
//
//		connectivity.push_back(line);
//	}
//
//	lineMeshColliding->setInitialVerticesPositions(vertList);
//	lineMeshColliding->setVerticesPositions(vertList);
//	lineMeshColliding->setConnectivity(connectivity);
//
//	lineMeshPhysics->setInitialVerticesPositions(vertList);
//	lineMeshPhysics->setVerticesPositions(vertList);
//	lineMeshPhysics->setConnectivity(connectivity);
//
//	lineMeshVisual->setInitialVerticesPositions(vertList);
//	lineMeshVisual->setVerticesPositions(vertList);
//	lineMeshVisual->setConnectivity(connectivity);
//
//
//	auto mapC2P = std::make_shared<imstk::OneToOneMap>();
//	mapC2P->setMaster(lineMeshColliding);
//	mapC2P->setSlave(lineMeshPhysics);
//	mapC2P->compute();
//
//	auto mapC2V = std::make_shared<imstk::OneToOneMap>();
//	mapC2V->setMaster(lineMeshColliding);
//	mapC2V->setSlave(lineMeshVisual);
//	mapC2V->compute();
//
//	auto mapP2C = std::make_shared<imstk::OneToOneMap>();
//	mapP2C->setMaster(lineMeshPhysics);
//	mapP2C->setSlave(lineMeshColliding);
//	mapP2C->compute();
//
//	auto mapP2V = std::make_shared<imstk::OneToOneMap>();
//	mapP2V->setMaster(lineMeshPhysics);
//	mapP2V->setSlave(lineMeshVisual);
//	mapP2V->compute();
//
//	auto pbdModel = std::make_shared<PbdModel>();
//	linesTool->setDynamicalModel(pbdModel);
//
//	linesTool->setCollidingGeometry(lineMeshColliding);
//	linesTool->setVisualGeometry(lineMeshVisual);
//	linesTool->setPhysicsGeometry(lineMeshPhysics);
//	linesTool->setPhysicsToCollidingMap(mapP2C);
//	linesTool->setCollidingToVisualMap(mapC2V);
//	linesTool->setPhysicsToVisualMap(mapP2V);
//	linesTool->setColldingToPhysicsMap(mapC2P);
//	linesTool->initialize(/*Number of constraints*/1,
//		/*Constraint configuration*/"Distance 100",
//		/*Mass*/0.0,
//		/*Gravity*/"0 -9.8 0",
//		/*TimeStep*/0.002,
//		/*FixedPoint*/"0 1 2",
//		/*NumberOfIterationInConstraintSolver*/5,
//		/*Proximity*/0.1,
//		/*Contact stiffness*/0.1);
//	scene->addSceneObject(linesTool);
//
//
//	// Read surface mesh
//	//auto floorMesh = imstk::MeshIO::read(DATA_ROOT_PATH"/rect5_4.obj"); //  cube_den.dae rect_den.dae colon3_c2 coarse mesh colon3.dae colon dense mesh
//	auto clothMeshVisual = imstk::MeshIO::read(DATA_ROOT_PATH"/cube2IN.obj"); //  std::dynamic_pointer_cast<imstk::SurfaceMesh>(floorMesh);
//	auto clothMeshColliding = imstk::MeshIO::read(DATA_ROOT_PATH"/cube2IN.obj"); // std::make_shared<imstk::SurfaceMesh>(floorMesh);
//	auto clothMeshPhysics = imstk::MeshIO::read(DATA_ROOT_PATH"/cube2IN.obj"); // std::make_shared<imstk::SurfaceMesh>(floorMesh);
//
//	auto clothMapP2V = std::make_shared<imstk::OneToOneMap>();
//	clothMapP2V->setMaster(clothMeshPhysics);
//	clothMapP2V->setSlave(clothMeshVisual);
//	clothMapP2V->compute();
//
//	auto clothMapC2V = std::make_shared<imstk::OneToOneMap>();
//	clothMapC2V->setMaster(clothMeshColliding);
//	clothMapC2V->setSlave(clothMeshVisual);
//	clothMapC2V->compute();
//
//	auto clothMapP2C = std::make_shared<imstk::OneToOneMap>();
//	clothMapP2C->setMaster(clothMeshPhysics);
//	clothMapP2C->setSlave(clothMeshColliding);
//	clothMapP2C->compute();
//
//
//	auto floor = std::make_shared<PbdObject>("cloth");
//
//	auto pbdModel2 = std::make_shared<PbdModel>();
//	floor->setDynamicalModel(pbdModel2);
//
//	floor->setCollidingGeometry(clothMeshColliding);
//	floor->setVisualGeometry(clothMeshVisual);
//	floor->setPhysicsGeometry(clothMeshPhysics);
//	floor->setPhysicsToCollidingMap(clothMapP2C);
//	floor->setPhysicsToVisualMap(clothMapP2V);
//	floor->setCollidingToVisualMap(clothMapC2V);
//	floor->initialize(/*Number of constraints*/2,
//		/*Constraint configuration*/"Distance 0.1",
//		/*Constraint configuration*/"Dihedral 0.001",
//		/*Mass*/0.1,
//		/*Gravity*/"0 -9.8 0",
//		/*TimeStep*/0.001,
//		/*FixedPoint*/"1 2 3 4 5",  // fixed_corner.c_str(),
//		/*NumberOfIterationInConstraintSolver*/5,
//		/*Proximity*/0.1,
//		/*Contact stiffness*/0.1);
//	scene->addSceneObject(floor);
//
//	std::cout << "nbr of vertices in cloth mesh %d" << clothMeshVisual->getNumVertices() << std::endl;
//
//	// Collisions
//	auto clothTestcolGraph = scene->getCollisionGraph();
//
//	auto pair1 = std::make_shared<PbdInteractionPair>(PbdInteractionPair(linesTool, floor));
//	pair1->setNumberOfInterations(5);
//
//	clothTestcolGraph->addInteractionPair(pair1);
//	//clothTestcolGraph->addInteractionPair(linesTool, floor,
//	//	CollisionDetection::Type::SphereToMesh,
//	//	CollisionHandling::Type::Penalty,
//	//	CollisionHandling::Type::None);
//
//	scene->getCamera()->setPosition(0, 0, 50);
//
//	// Run
//	sdk->setActiveScene(scene);
//	auto renderer = std::dynamic_pointer_cast<VTKRenderer>(sdk->getViewer()->getCurrentRenderer())->getVtkRenderer();  //  sdk->getViewer()->getCurrentRenderer()->getVtkRenderer();
//	renderer->AddActor(actor);
//	sdk->startSimulation(true);
//
//#endif
//}

void testVESS()
{
	//// SDK and Scene
	//auto sdk = std::make_shared<imstk::SimulationManager>();
	//auto scene = sdk->createNewScene("VESSTest");

	//// Device clients
	//auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");
	////auto client1 = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 2");

	//// Device Server
	//auto server = std::make_shared<imstk::HDAPIDeviceServer>();
	//server->addDeviceClient(client);
	////server->addDeviceClient(client1);
	//sdk->addModule(server);

	//// Update Camera position
	//auto cam = scene->getCamera();
	//cam->setPosition(imstk::Vec3d(-15, 20, -5));  //  (-11, -1.5, 5.5)put it in the middle of colon model  colon (-10, -1.5, 6)  cube (-15, 20, -5)
	//cam->setFocalPoint(imstk::Vec3d(-19, 20, -5)); //  (-15, -1.5, 5.5) colon (-14, -1.5, 6)  cube (-19, 20, -5)
	//cam->setViewUp(0, 1, 0);

	//auto camController = cam->setupController(client); // auto
	//camController->setTranslationScaling(0.3); // 0.2 colon

	//// Light (white)
	//auto whiteLight = std::make_shared<imstk::Light>("whiteLight");
	//whiteLight->setPosition(imstk::Vec3d(-15, 20, -5)); // (-11, -1.5, 5.5) colon  (-10, -1.5, 6));   cube (-15, 20, -5)
	//whiteLight->setFocalPoint(imstk::Vec3d(-19, 20, -5)); //(-15, -1.5, 5.5) colon (-14, -1.5, 6));  (-19, 20, -5)
	//whiteLight->setPositional();

	//// Light (red)
	//auto colorLight = std::make_shared<imstk::Light>("colorLight");
	//colorLight->setPosition(imstk::Vec3d(5.2361, 7.9564, 3.1235));
	//colorLight->setFocalPoint(imstk::Vec3d(1.2361, 7.9564, 3.1235));
	//colorLight->setColor(imstk::Color::Red);
	//colorLight->setPositional();
	//colorLight->setSpotAngle(15);

	//// Light (point)
	//auto pointLight = std::make_shared<imstk::Light>("pointLight");
	//pointLight->setPosition(imstk::Vec3d(-15, 20, -5));
	//pointLight->setType(imstk::LightType::SCENE_LIGHT);
	//

	//scene->addLight(whiteLight);
	//scene->addLight(colorLight); // not add red right
	//scene->addLight(pointLight);

	//auto sphere0Geom = std::make_shared<imstk::Sphere>();
	//sphere0Geom->setPosition(imstk::Vec3d(-15, 20, -5)); // (-11, -1.5, 5.5) colon  (-10, -1.5, 6));  -10, -1.5, 6  cube -15, 20, -5
	//sphere0Geom->setRadius(1.0); // colon 0.2 cube 1.0

	//auto sphere0Obj = std::make_shared<imstk::CollidingObject>("Sphere0");
	//sphere0Obj->setVisualGeometry(sphere0Geom);
	//sphere0Obj->setCollidingGeometry(sphere0Geom);

	//auto sphereStartGeom = std::make_shared<imstk::Sphere>();
	//sphereStartGeom->setPosition(imstk::Vec3d(-15, 20, -5)); // (-13, -2.0, 6.0) colon  (-10, -1.5, 6));  -10, -1.5, 6  cube -15, 20, -5
	//sphereStartGeom->setRadius(1.0); // colon 0.2 cube 1.0

	//auto sphereStartObj = std::make_shared<imstk::CollidingObject>("SphereStart");
	//sphereStartObj->setVisualGeometry(sphereStartGeom);
	//sphereStartObj->setCollidingGeometry(sphereStartGeom);
	//scene->addSceneObject(sphereStartObj);

	//// Parse command line arguments

	////std::string inputFilename = DATA_ROOT_PATH"/colon_7_max3.obj";
	////std::string texturename = DATA_ROOT_PATH"/textures/colontex5.jpg";

	////std::string inputFilename1 = DATA_ROOT_PATH"/colon_s1.obj";
	//////std::string texturename1 = DATA_ROOT_PATH"/textures/colontex5n.png";

	////vtkSmartPointer<vtkOBJReader> reader =
	////	vtkSmartPointer<vtkOBJReader>::New();
	////reader->SetFileName(inputFilename.c_str());
	////reader->Update();

	////vtkSmartPointer<vtkOBJReader> reader1 =
	////	vtkSmartPointer<vtkOBJReader>::New();
	////reader1->SetFileName(inputFilename1.c_str());
	////reader1->Update();

	////// Visualize
	////vtkSmartPointer<vtkPolyDataMapper> mapper =
	////	vtkSmartPointer<vtkPolyDataMapper>::New();
	////mapper->SetInputConnection(reader->GetOutputPort());

	////vtkSmartPointer<vtkPolyDataMapper> mapper1 =
	////	vtkSmartPointer<vtkPolyDataMapper>::New();
	////mapper1->SetInputConnection(reader1->GetOutputPort());

	//vtkSmartPointer<vtkActor> actor =
	//	vtkSmartPointer<vtkActor>::New();
	////actor->SetMapper(mapper);

	////vtkSmartPointer<vtkActor> actor1 =
	////	vtkSmartPointer<vtkActor>::New();
	////actor1->SetMapper(mapper1);

	////vtkSmartPointer<vtkJPEGReader> jpgReader =
	////	vtkSmartPointer<vtkJPEGReader>::New();
	////jpgReader->SetFileName(texturename.c_str());
	////jpgReader->Update();
	////vtkSmartPointer<vtkTexture> texture = vtkSmartPointer<vtkTexture>::New();
	////texture->SetInputConnection(jpgReader->GetOutputPort());
	////texture->InterpolateOn();
	////actor->SetTexture(texture);

	///*vtkSmartPointer<vtkJPEGReader> jpgReader1 =
	//	vtkSmartPointer<vtkJPEGReader>::New();
	//	jpgReader1->SetFileName(texturename1.c_str());
	//	jpgReader1->Update();*/
	///*vtkSmartPointer<vtkTexture> texture1 = vtkSmartPointer<vtkTexture>::New();
	//texture1->SetInputConnection(jpgReader1->GetOutputPort());
	//texture1->InterpolateOn();
	//actor->SetTexture(texture1);*/


	//// Read surface mesh
	//auto objMesh = imstk::MeshIO::read(DATA_ROOT_PATH"/cube2IN.obj"); // colon_s1.obj  colon_7_max3
	//auto surfaceMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh);
	////surfaceMesh->addTexture(DATA_ROOT_PATH"/textures/colontex5.png",
	////	"material_0");
	////surfaceMesh->addTexture(DATA_ROOT_PATH"/textures/colontex5n.png",
	////	"material_1");

	//auto objMesh2 = imstk::MeshIO::read(DATA_ROOT_PATH"/cube2IN.obj"); // cube.dae rect.dae colon3_c2 coarse mesh colon3_c2.dae// colon coarse mesh
	//auto surfaceMesh2 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh2);

	//auto objectColon = std::make_shared<imstk::CollidingObject>("Colon");
	//objectColon->setVisualGeometry(surfaceMesh);
	//objectColon->setCollidingGeometry(surfaceMesh2);
	//scene->addSceneObject(objectColon);

	//auto controller0 = std::make_shared<imstk::SceneObjectController>(sphere0Obj, camController);  // SceneObjectController
	//scene->addObjectController(controller0);

	//// Collisions
	//auto colGraph = scene->getCollisionGraph();
	//colGraph->addInteractionPair(sphere0Obj, objectColon,
	//	CollisionDetection::Type::SphereToMesh,
	//	CollisionHandling::Type::Penalty,
	//	CollisionHandling::Type::None);

	//// Run
	//sdk->setActiveScene(scene);
	//auto renderer = std::dynamic_pointer_cast<VTKRenderer>(sdk->getViewer()->getCurrentRenderer())->getVtkRenderer();  //  sdk->getViewer()->getCurrentRenderer()->getVtkRenderer();
	//renderer->AddActor(actor);
	//sdk->startSimulation(true);
}

//void testVTKVESS()
//{
//	// SDK and Scene
//	auto sdk = std::make_shared<imstk::SimulationManager>();
//	auto scene = sdk->createNewScene("SceneTestDevice");
//
//	// Device clients
//	auto client = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 1");
//	//auto client1 = std::make_shared<imstk::HDAPIDeviceClient>("PHANToM 2");
//
//	// Device Server
//	auto server = std::make_shared<imstk::HDAPIDeviceServer>();
//	server->addDeviceClient(client);
//	//server->addDeviceClient(client1);
//	sdk->addModule(server);
//
//	//// Device server
//	//auto server = std::make_shared<imstk::VRPNDeviceServer>("127.0.0.1");  
//	//server->addDevice("device0", imstk::DeviceType::OSVR_HDK);
//	//sdk->addModule(server);
//
//	//// Device Client
//	//auto client = std::make_shared<imstk::VRPNDeviceClient>("device0", "localhost"); // localhost = 127.0.0.1
//	////client->setLoopDelay(1000);
//	//sdk->addModule(client);
//
//	// Read surface mesh
//	//auto objMesh = imstk::MeshIO::read(DATA_ROOT_PATH"/colon_7_max.obj"); // colon_1.obj  ColonRectum.obj ColonRectum_r1.obj
//	//auto surfaceMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh);
//	//surfaceMesh->addTexture(DATA_ROOT_PATH"/textures/organs_large_intestine_BUMP.jpg",
//	//	"");
//	//surfaceMesh->addTexture(DATA_ROOT_PATH"/textures/organs_large_intestine.jpg",
//	//	"");
//	//surfaceMesh->scale(1);
//	//// coarse surface mesh for CD
//	//auto objMeshCD = imstk::MeshIO::read(DATA_ROOT_PATH"/ColonRectum_2.obj"); // colon_1.obj  ColonRectum.obj
//	//auto surfaceMeshCD = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMeshCD);
//
//	vtkSmartPointer<vtkOBJReader> reader =
//		vtkSmartPointer<vtkOBJReader>::New();
//	reader->SetFileName(DATA_ROOT_PATH"/colon_7_max3.obj");
//	reader->Update();
//
//	// Visualize
//	vtkSmartPointer<vtkPolyDataMapper> mapper =
//		vtkSmartPointer<vtkPolyDataMapper>::New();
//	//mapper->SetInputConnection(reader->GetOutputPort());
//
//	vtkSmartPointer<vtkActor> actor =
//		vtkSmartPointer<vtkActor>::New();
//	actor->SetMapper(mapper);
//
//	vtkSmartPointer<vtkJPEGReader> jpgReader =
//		vtkSmartPointer<vtkJPEGReader>::New();
//	jpgReader->SetFileName(DATA_ROOT_PATH"/textures/colontex6.jpg"); // colontex6.jpg
//	jpgReader->Update();
//	vtkSmartPointer<vtkTexture> texture = vtkSmartPointer<vtkTexture>::New();
//	texture->SetInputConnection(jpgReader->GetOutputPort());
//	texture->InterpolateOn();
//	actor->SetTexture(texture);
//
//
//	//Vec3d min1, max1;
//	//surfaceMesh->computeBoundingBox(min1, max1);
//
//	//// Create object and add to scene
//	//auto object = std::make_shared<imstk::VisualObject>("meshObject");
//	//object->setVisualGeometry(surfaceMesh); // change to any mesh created above
//
//	//scene->addSceneObject(object);
//
//	// Update Camera position
//	auto cam = scene->getCamera();
//	cam->setPosition(imstk::Vec3d(-11, -1.5, 5.5));  //  put it in the middle of colon model  colon (-10, -1.5, 6)  cube (-15, 20, -5)
//	cam->setFocalPoint(imstk::Vec3d(-15, -1.5, 5.5)); // colon (-14, -1.5, 6)  cube (-19, 20, -5)
//	cam->setViewUp(0, 1, 0);
//
//	// Set camera controller
//	//std::shared_ptr<ESDCameraController> 
//	auto camController = cam->setupController(client); // auto
//	camController->setTranslationScaling(0.2);
//	//camController->setCollisionMesh(surfaceMeshCD);
//	//LOG(INFO) << camController->getTranslationOffset(); // should be the same than initial cam position
//	//camController->setInversionFlags(imstk::CameraController::InvertFlag::rotY |
//	// imstk::CameraController::InvertFlag::rotZ);
//
//	// Light (white)
//	auto whiteLight = std::make_shared<imstk::Light>("whiteLight");
//	whiteLight->setPosition(imstk::Vec3d(-11, -1.5, 5.5)); // colon  (-10, -1.5, 6));   cube (-15, 20, -5)
//	whiteLight->setFocalPoint(imstk::Vec3d(-15, -1.5, 5.5)); // colon (-14, -1.5, 6));  (-19, 20, -5)
//	whiteLight->setPositional();
//
//	// Light (red)
//	auto colorLight = std::make_shared<imstk::Light>("colorLight");
//	colorLight->setPosition(imstk::Vec3d(5.2361 ,   7.9564 ,   3.1235));
//	colorLight->setFocalPoint(imstk::Vec3d(1.2361  ,  7.9564 ,   3.1235));
//	colorLight->setColor(imstk::Color::Red);
//	colorLight->setPositional();
//	colorLight->setSpotAngle(15);
//	//scene->addLight(whiteLight);
//	//scene->addLight(colorLight); // not add red right
//
//	auto sphere0Geom = std::make_shared<imstk::Sphere>();
//	sphere0Geom->setPosition(imstk::Vec3d(-11, -1.5, 5.5)); // colon  (-10, -1.5, 6));  -10, -1.5, 6  cube -15, 20, -5
//	sphere0Geom->setRadius(0.2);
//	//sphere0Geom->scale(0);
//
//	auto sphere0Obj = std::make_shared<imstk::CollidingObject>("Sphere0");
//	sphere0Obj->setVisualGeometry(sphere0Geom);
//	sphere0Obj->setCollidingGeometry(sphere0Geom);
//	//scene->addSceneObject(sphere0Obj);
//
//	auto sphereStartGeom = std::make_shared<imstk::Sphere>();
//	sphereStartGeom->setPosition(imstk::Vec3d(-13, -2.0, 6.0)); // colon  (-10, -1.5, 6));  -10, -1.5, 6  cube -15, 20, -5
//	sphereStartGeom->setRadius(0.2);
//
//	auto sphereStartObj = std::make_shared<imstk::CollidingObject>("SphereStart");
//	sphereStartObj->setVisualGeometry(sphereStartGeom);
//	sphereStartObj->setCollidingGeometry(sphereStartGeom);
//	scene->addSceneObject(sphereStartObj);
//
//	//auto trackCtrl0 = std::make_shared<imstk::DeviceTracker>(client);
//	//trackCtrl0->setTranslationScaling(0.05);
//	//auto sphere0Controller = std::make_shared<imstk::SceneObjectController>(sphere0Obj, trackCtrl0);
//	//scene->addObjectController(sphere0Controller);
//
//	// Read surface mesh
//	auto objMesh = imstk::MeshIO::read(DATA_ROOT_PATH"/colon3.dae"); //  cube_den.dae rect_den.dae colon3_c2 coarse mesh colon3.dae colon dense mesh
//	auto surfaceMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh);
//	//surfaceMesh->setScaling(5);
//	auto material = std::make_shared<Material>();
//	auto diffuseTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/textures/colontex5.png", Texture::Type::DIFFUSE);
//	auto normalTexture = std::make_shared<Texture>(DATA_ROOT_PATH"/textures/colontex5n.png", Texture::Type::NORMAL);
//	material->setTexture(diffuseTexture);
//	material->setTexture(normalTexture);
//	surfaceMesh->addMaterial(material);
//
//	auto objMesh2 = imstk::MeshIO::read(DATA_ROOT_PATH"/colon_s1.dae"); // cube.dae rect.dae colon3_c2 coarse mesh colon3_c2.dae// colon coarse mesh
//	auto surfaceMesh2 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(objMesh2);
//	//surfaceMesh2->setScaling(5);
//	// Create object and add to scene
//	//auto object = std::make_shared<imstk::VisualObject>("meshObject");
//	//object->setVisualGeometry(surfaceMesh); // change to any mesh created above
//	//scene->addSceneObject(object);
//
//	auto objectColon = std::make_shared<imstk::CollidingObject>("Colon");
//	objectColon->setVisualGeometry(surfaceMesh);
//	objectColon->setCollidingGeometry(surfaceMesh2);
//	scene->addSceneObject(objectColon);
//
//	auto controller0 = std::make_shared<imstk::SceneObjectController>(sphere0Obj, camController);  // SceneObjectController
//	scene->addObjectController(controller0);
//
//	// Collisions
//	auto colGraph = scene->getCollisionGraph();
//	colGraph->addInteractionPair(sphere0Obj, objectColon,
//		CollisionDetection::Type::SphereToMesh,
//		CollisionHandling::Type::Penalty,
//	CollisionHandling::Type::None);
//
//	// Run
//	sdk->setActiveScene(scene);
//	//<< << << < HEAD
//
//	
//
//	//== == == =
//	//vtkNew<vtkPolyDataNormals> norms;
//	//norms->SetInputConnection(reader->GetOutputPort());
//	//norms->Update();
//	
//	auto mapperShader = (vtkOpenGLPolyDataMapper*)(actor->GetMapper());
//	// mapper->SetInputConnection(norms->GetOutputPort());
//	actor->SetMapper(mapperShader);
//	actor->GetProperty()->SetAmbientColor(0.2, 0.2, 1.0);
//	actor->GetProperty()->SetDiffuseColor(1.0, 0.65, 0.7);
//	actor->GetProperty()->SetSpecularColor(1.0, 1.0, 1.0);
//	actor->GetProperty()->SetSpecular(0.5);
//	actor->GetProperty()->SetDiffuse(0.7);
//	actor->GetProperty()->SetAmbient(0.5);
//	actor->GetProperty()->SetSpecularPower(20.0);
//	actor->GetProperty()->SetOpacity(1.0);
//
//	mapperShader->SetVertexShaderCode(
//		"//VTK::System::Dec\n"  // always start with this line
//		"attribute vec4 vertexMC;\n"
//		// use the default normal decl as the mapper
//		// will then provide the normalMatrix uniform
//		// which we use later on
//		"attribute vec2 tcoordMC;\n"
//		"varying vec3 normalVCVSOutput;\n"
//
//		"uniform mat3 normalMatrix;\n"
//		"uniform mat4 MCVCMatrix;\n" // MCVC  model view
//		"uniform mat4 MCDCMatrix;\n"
//		"attribute vec3 normalMC;\n"   
//
//		//"varying vec3 normalInterp;\n"
//		//"varying vec3 vertPos;\n"
//
//		"varying vec2 tcoordVCVSOutput;\n"
//
//		"varying vec4 vertexVCVSOutput;\n"
//
//		"void main () {\n"
//		"tcoordVCVSOutput=tcoordMC;\n"
//		"gl_Position = MCDCMatrix * vertexMC;\n"  // model-
//		"vertexVCVSOutput=MCVCMatrix * vertexMC;\n"
//		"normalVCVSOutput = normalMatrix * normalMC;\n"
//
//		//"vec4 vertPos4 = MCVCMatrix * vertexMC;\n"
//		//"vertPos = vec3(vertPos4) / vertPos4.w;"
//		//"normalInterp =  normalMatrix * normalVCVSOutput; \n"
//		"}\n"
//		);
//
//
//	mapperShader->SetFragmentShaderCode(
//		"//VTK::System::Dec\n"  // always start with this line
//		"//VTK::Output::Dec\n"  // always have this line in your FS
//
//		//"precision mediump float;\n"
//
//		//"varying vec3 normalInterp;\n"
//		//"varying vec3 vertPos;\n"
//
//		"const vec3 lightPos = vec3(1.0, 1.0, 1.0);\n"
//		"const vec3 ambientColor = vec3(0.00, 0.00, 0.00);\n"
//		"vec3 diffuseColor = vec3(1.0, 0.65, 0.7);\n"
//		"const vec3 specColor = vec3(0.3, 0.3, 0.3);\n" // higher when texture is dark
//		"const float shininess = 10.0;\n"
//		"const float screenGamma = 2.2;\n" // Assume the monitor is calibrated to the sRGB color space
//
//		"uniform sampler2D texture_0;\n"  // texture
//		"varying vec3 normalVCVSOutput;\n"
//		"varying vec2 tcoordVCVSOutput;\n"
//		"varying vec4 vertexVCVSOutput;\n"
//
//		"void main () {\n"
//
//		"vec3 normal = normalize(normalVCVSOutput);\n"
//		"vec3 lightDir =-vertexVCVSOutput.xyz;\n"  // normalize(lightPos - vertPos); \n"
//
//		"float lambertian = max(dot(lightDir, normalVCVSOutput), 0.0);\n"
//		"float specular = 0.0;\n"
//
//		"if (lambertian > 0.0) {\n"
//
//		"	vec3 viewDir = normalize(-vertexVCVSOutput.xyz);\n"
//
//		// this is blinn phong
//		"	vec3 halfDir = normalize(lightDir + viewDir);\n"
//		"	float specAngle = max(dot(halfDir, normal), 0.0);\n"
//		"	specular = pow(specAngle, shininess);\n"
//
//		"}\n" // if END
//		"diffuseColor=pow(texture2D(texture_0, tcoordVCVSOutput).xyz,vec3(screenGamma));\n"
//	"vec3 colorLinear = ambientColor +lambertian * diffuseColor +specular * specColor;\n"
//		// apply gamma correction (assume ambientColor, diffuseColor and specColor
//		// have been linearized, i.e. have no gamma correction in them)
//		"vec3 colorGammaCorrected = pow(colorLinear, vec3(1.0 / screenGamma));\n"
//		// use the gamma corrected color in the fragment
//		"gl_FragData[0] = vec4(colorGammaCorrected, 1.0);\n"  // +texture2D(texture_0, tcoordVCVSOutput); \n" // gl_FragColor 120之后弃用
//
//		"}\n"  // main END
//		);
//
//
//	//mapperShader->SetVertexShaderCode(
//	//	"//VTK::System::Dec\n"  // always start with this line
//	//	"attribute vec4 vertexMC;\n"
//	//	// use the default normal decl as the mapper
//	//	// will then provide the normalMatrix uniform
//	//	// which we use later on
//	//	"//VTK::Normal::Dec\n"
//	//	"uniform mat4 MCDCMatrix;\n"
//	//	"varying vec2 tcoordVCVSOutput;\n"
//	//	"attribute vec2 tcoordMC;\n"
//
//	//	"void main () {\n"
//	//	"normalVCVSOutput = normalMatrix * normalMC;\n"
//	//	"tcoordVCVSOutput=tcoordMC;\n"
//	//	"gl_Position = MCDCMatrix * vertexMC;\n"
//	//	//"tcoordVC = tcoordMC;\n"
//	//	//"gl_Position = vertexMC;\n"
//	//	"}\n"
//	//	);
//	//mapperShader->SetFragmentShaderCode(
//	//	"//VTK::System::Dec\n"  // always start with this line
//	//	"//VTK::Output::Dec\n"  // always have this line in your FS
//
//	//	"varying vec3 normalVCVSOutput;\n"
//	//	"uniform sampler2D texture_0;\n"
//	//	"varying vec2 tcoordVCVSOutput;\n"
//	//	"uniform sampler2D source;\n"
//	//	"void main () {\n"
//	//	"gl_FragData[0] = texture2D(texture_0,tcoordVCVSOutput);\n"
//	//	//"vec4 texColor = texture2D(texture_0, tcoordVCVSOutput);\n"
//	//	//"gl_FragData[0] =vec4(0.0,1.0,0.0,1.0); \n" //texColor;\n"  // vec4(abs(normalVCVSOutput), 1.0) + texColor; \n"
//	//	"}\n"
//	//	);
//	
//	//mapperShader->SetVertexShaderCode(
//	//	//"#version 120 \n"
//	//	"//VTK::System::Dec\n"  // always start with this line
//
//	//	//"attribute vec4 vertexMC;\n"
//	//	//"uniform vec3 fvLightPosition;\n"
//	//	//"uniform vec3 fvEyePosition; \n"
//	//	//"varying vec2 Texcoord; \n"
//	//	//"varying vec3 ViewDirection; \n"
//	//	//"varying vec3 LightDirection; \n"
//	//	//"varying vec3 Normal; \n"
//
//	//	// OpenGLSL 4.5
//
//	//	//"layout(location = 0) in vec3 VertexPosition; \n"
//	//	//"layout(location = 1) in vec3 VertexNormal; \n"
//	//	//"layout(location = 2) in vec2 VertexTexCoord; \n"
//	//	//"out vec3 Position; \n"
//	//	//"out vec3 Normal; \n"
//	//	//"out vec2 vTexCoord; \n"
//
//	//	"uniform vec3  LightPosition;\n"
//	//	"uniform vec3  LightColor;\n"
//	//	"uniform vec3  EyePosition;\n"
//	//	"uniform vec3  Specular;\n"
//	//	"uniform vec3  Ambient;\n"
//	//	"uniform float Kd;\n"
//
//	//	"in  vec2  TexCoord0;\n"
//	//	"out float TexCoord;\n"
//	//	"uniform mat4 MVMatrix;\n"
//	//	"uniform mat4 MVPMatrix;\n"
//	//	"uniform mat3 NormalMatrix;\n"
//	//	"in  vec4  MCVertex;\n"
//	//	"in  vec3  MCNormal;\n"
//
//	//	"out vec3  DiffuseColor; \n"
//	//	"out vec3  SpecularColor; \n"
//
//	//	//"uniform mat4 ModelViewMatrix; \n"
//	//	//"uniform mat3 NormalMatrix; \n"
//	//	//"uniform mat4 ProjectionMatrix; \n"
//	//	//"uniform mat4 MVP; \n"
//	//	// END 4.5
//	//	// use the default normal decl as the mapper
//	//	// will then provide the normalMatrix uniform
//	//	// which we use later on
//	//	"//VTK::Normal::Dec\n"
//	//	//"uniform mat4 MCDCMatrix;\n"
//	//	"void main () {\n"
//
//	//	"vec3 ecPosition = vec3(MVMatrix * MCVertex); \n"
//	//	"vec3 tnorm = normalize(NormalMatrix * MCNormal); \n"
//	//	"vec3 lightVec = normalize(LightPosition - ecPosition); \n"
//	//	"vec3 viewVec = normalize(EyePosition - ecPosition); \n"
//	//	"vec3 hvec = normalize(viewVec + lightVec); \n"
//	//	"float spec = clamp(dot(hvec, tnorm), 0.0, 1.0); \n"
//	//	"spec = pow(spec, 16.0); \n"
//	//	"DiffuseColor = LightColor * vec3(Kd * dot(lightVec, tnorm)); \n"
//	//	"DiffuseColor = clamp(Ambient + DiffuseColor, 0.0, 1.0); \n"
//	//	"SpecularColor = clamp((LightColor * Specular * spec), 0.0, 1.0); \n"
//	//	"TexCoord  = TexCoord0.t; \n"
//
//	//	//"Normal = normalize(NormalMatrix * VertexNormal); \n"
//	//	//"Position = vec3(ModelViewMatrix *vec4(VertexPosition, 1.0));\n"
//	//	//"gl_Position = MVP * vec4(VertexPosition, 1.0); \n"
//	//	//"normalVCVSOutput = normalMatrix * normalMC;\n"
//	//	// do something weird with the vertex positions
//	//	// this will mess up your head if you keep
//	//	// rotating and looking at it, very trippy
//	//	//"vec4 tmpPos = MCDCMatrix * vertexMC;\n"
//	//	//"  gl_Position = tmpPos*vec4(0.2+0.8*abs(tmpPos.x),0.2+0.8*abs(tmpPos.y),1.0,1.0);\n"
//	//	//"gl_Position = vec4(tmpPos.x,tmpPos.y,tmpPos.z,1.0); \n"
//	//	//"gl_Position = gl_ModelViewProjectionMatrix*gl_Vertex; \n"
//	//	//"Texcoord = gl_MultiTexCoord0.xy; \n"
//	//	//"vec4 fvObjectPosition = gl_ModelViewMatrix * gl_Vertex; \n"
//	//	//"ViewDirection = fvEyePosition - fvObjectPosition.xyz; \n"
//	//	//"LightDirection = fvLightPosition - fvObjectPosition.xyz; \n"
//	//	//"Normal = gl_NormalMatrix * gl_Normal; \n"
//	//	"}\n"
//	//	);
//	//mapperShader->SetFragmentShaderCode(
//	//	"//VTK::System::Dec\n"  // always start with this line
//	//	"//VTK::Output::Dec\n"  // always have this line in your FS
//	//	/*"varying vec3 normalVCVSOutput;\n"
//	//	"uniform vec3 diffuseColorUniform;\n"*/
//	//	//"uniform vec4 fvAmbient; \n"
//	//	//"uniform vec4 fvSpecular; \n"
//	//	//"uniform vec4 fvDiffuse; \n"
//	//	//"uniform float fSpecularPower; \n"
//	//	//"uniform sampler2D baseMap; \n"
//
//	//	//"varying vec2 Texcoord; \n"
//	//	//"varying vec3 ViewDirection; \n"
//	//	//"varying vec3 LightDirection; \n"
//	//	//"varying vec3 Normal; \n"
//
//	//	// 140 version
//	//	"uniform vec3  StripeColor; \n"
//	//	"uniform vec3  BackColor; \n"
//	//	"uniform float Width; \n"
//	//	"uniform float Fuzz; \n"
//	//	"uniform float Scale; \n"
//	//	"in vec3  DiffuseColor; \n"
//	//	"in vec3  SpecularColor; \n"
//	//	"in float TexCoord; \n"
//	//	"out vec4 FragColor; \n"
//	//	// 140 END
//	//	"in vec3 Position; \n"
//	//	"in vec3 Normal; \n"
//	//	"in vec2 TexCoord; \n"
//	//	"uniform sampler2D Tex1; \n"
//
//	//	"struct LightInfo {\n"
//	//	"vec4 Position; \n" // Light position in eye coords.
//	//	"vec3 Intensity; \n"// A,D,S intensity
//	//	"};\n"
//	//	"uniform LightInfo Light;\n"
//
//	//	"struct MaterialInfo { \n"
//	//	"vec3 Ka; \n"            // Ambient reflectivity
//	//	"vec3 Kd; \n"            // Diffuse reflectivity
//	//	"vec3 Ks; \n"            // Specular reflectivity
//	//	"float Shininess; \n"    // Specular shininess factor
//	//	"}; \n"
//	//	"uniform MaterialInfo Material; \n"
//
//	//	//"layout(location = 0) out vec4 FragColor;\n"
//
//	//	"void phongModel(vec3 pos, vec3 norm,\n"
//	//	"out vec3 ambAndDiff, out vec3 spec) {\n"
//	//	// Compute the ADS shading model here, return ambient
//	//	// and diffuse color in ambAndDiff, and return specular
//	//	// color in spec
//
//	//	"}\n"
//
//	//	"void main () {\n"
//
//	//	// 140 
//	//	"float scaledT = fract(TexCoord * Scale); \n"
//	//	"float frac1 = clamp(scaledT / Fuzz, 0.0, 1.0); \n"
//	//	"float frac2 = clamp((scaledT - Width) / Fuzz, 0.0, 1.0); \n"
//	//	"frac1 = frac1 * (1.0 - frac2); \n"
//	//	"frac1 = frac1 * frac1 * (3.0 - (2.0 * frac1)); \n"
//	//	"vec3 finalColor = mix(BackColor, StripeColor, frac1); \n"
//	//	"finalColor = finalColor * DiffuseColor + SpecularColor; \n"
//	//	"FragColor = vec4(finalColor, 1.0); \n"
//	//	// 140 END
//	//	//"vec3 ambAndDiff, spec;\n"
//	//	//"vec4 texColor = texture(Tex1, TexCoord);\n"
//	//	//"phongModel(Position, Normal, ambAndDiff, spec);\n"
//	//	//"FragColor = vec4(ambAndDiff, 1.0) * texColor +vec4(spec, 1.0);\n"
//
//	//	//"  float df = max(0.0, normalVCVSOutput.z);\n"
//	//	//"  float sf = pow(df, 20.0);\n"
//	//	//"  vec3 diffuse = df * diffuseColorUniform;\n"
//	//	//"  vec3 specular = sf * vec3(0.4,0.4,0.4);\n"
//	//	//"  gl_FragData[0] = vec4(0.3*abs(normalVCVSOutput) + 0.7*diffuse + specular, 1.0);\n"
//	//	/*"vec3  fvLightDirection = normalize(LightDirection); \n"
//	//	"vec3  fvNormal = normalize(Normal); \n"
//	//	"float fNDotL = dot(fvNormal, fvLightDirection); \n"
//
//	//	"vec3  fvReflection = normalize(((2.0 * fvNormal) * fNDotL) - fvLightDirection); \n"
//	//	"vec3  fvViewDirection = normalize(ViewDirection); \n"
//	//	"float fRDotV = max(0.0, dot(fvReflection, fvViewDirection)); \n"
//
//	//	"vec4  fvBaseColor = texture2D(baseMap, Texcoord); \n"
//
//	//	"vec4  fvTotalAmbient = fvAmbient * fvBaseColor; \n"
//	//	"vec4  fvTotalDiffuse = fvDiffuse * fNDotL * fvBaseColor; \n"
//	//	"vec4  fvTotalSpecular = fvSpecular * (pow(fRDotV, fSpecularPower)); \n"
//
//	//	"gl_FragColor = (fvTotalAmbient + fvTotalDiffuse + fvTotalSpecular); \n"*/
//	//	"}\n"
//	//	);
//
//	//auto renderer = sdk->getViewer()->getCurrentRenderer()->getVtkRenderer();
//	//renderer->AddActor(actor);
////auto renderer = std::dynamic_pointer_cast<VTKRenderer>(sdk->getViewer()->getCurrentRenderer())->getVtkRenderer();  //  sdk->getViewer()->getCurrentRenderer()->getVtkRenderer();
////renderer->AddActor(actor);
//	sdk->startSimulation(false);
//}

void testVESSPBD()
{
	auto sdk = std::make_shared<SimulationManager>();
	auto scene = sdk->createNewScene("PbdCollisionTest");

	scene->getCamera()->setPosition(0, 10.0, 25.0);

	// dragon
	auto tetMesh = imstk::MeshIO::read(iMSTK_DATA_ROOT"/asianDragon/asianDragon.veg");
	if (!tetMesh)
	{
		LOG(WARNING) << "Could not read mesh from file.";
		return;
	}

	auto surfMesh = std::make_shared<imstk::SurfaceMesh>();
	auto surfMeshVisual = std::make_shared<imstk::SurfaceMesh>();
	auto volTetMesh = std::dynamic_pointer_cast<imstk::TetrahedralMesh>(tetMesh);
	if (!volTetMesh)
	{
		LOG(WARNING) << "Dynamic pointer cast from imstk::Mesh to imstk::TetrahedralMesh failed!";
		return;
	}
	volTetMesh->extractSurfaceMesh(surfMesh);
	volTetMesh->extractSurfaceMesh(surfMeshVisual);

	auto deformMapP2V = std::make_shared<imstk::OneToOneMap>();
	deformMapP2V->setMaster(tetMesh);
	deformMapP2V->setSlave(surfMeshVisual);
	deformMapP2V->compute();

	auto deformMapC2V = std::make_shared<imstk::OneToOneMap>();
	deformMapC2V->setMaster(surfMesh);
	deformMapC2V->setSlave(surfMeshVisual);
	deformMapC2V->compute();

	auto deformMapP2C = std::make_shared<imstk::OneToOneMap>();
	deformMapP2C->setMaster(tetMesh);
	deformMapP2C->setSlave(surfMesh);
	deformMapP2C->compute();

	auto deformableObj = std::make_shared<PbdObject>("Dragon");
	deformableObj->setVisualGeometry(surfMeshVisual);
	deformableObj->setCollidingGeometry(surfMesh);
	deformableObj->setPhysicsGeometry(volTetMesh);
	deformableObj->setPhysicsToCollidingMap(deformMapP2C);
	deformableObj->setPhysicsToVisualMap(deformMapP2V);
	deformableObj->setCollidingToVisualMap(deformMapC2V);

	auto pbdModel = std::make_shared<PbdModel>();
    pbdModel->configure(/*Number of Constraints*/1,
		/*Constraint configuration*/"FEM NeoHookean 1.0 0.3", // "Volume 0.7",   // "FEM NeoHookean 1.0 0.3", //  "Volume 0.5",  //  "FEM NeoHookean 1.0 0.3", // "Volume 0.5",  // 
		/*Mass*/1.0,  // org 1.0
		/*Gravity*/"0 -9.8 0", // "0 -9.8 0",
		/*TimeStep*/0.001,
		/*FixedPoint*/"",
		/*NumberOfIterationInConstraintSolver*/2, // org 2
		/*Proximity*/0.1,
		/*Contact stiffness*/0.01);  // org 0.01
    deformableObj->setDynamicalModel(pbdModel);

	auto pbdSolver = std::make_shared<PbdSolver>();
	pbdSolver->setPbdObject(deformableObj);
	scene->addNonlinearSolver(pbdSolver);

	scene->addSceneObject(deformableObj);

	bool clothTest = 0; //  1; // origin 0;
	bool volumetric = !clothTest;
	if (clothTest){
		auto clothMesh = std::make_shared<imstk::SurfaceMesh>();
		imstk::StdVectorOfVec3d vertList;
		double width = 60.0;
		double height = 60.0;
		int nRows = 10;
		int nCols = 10;
		int corner[4] = { 1, nRows, nRows*nCols - nCols + 1, nRows*nCols };
		char intStr[33];
		std::string fixed_corner;
		for (unsigned int i = 0; i < 4; ++i)
		{
			std::sprintf(intStr, "%d", corner[i]);
			fixed_corner += std::string(intStr) + ' ';
		}
		vertList.resize(nRows*nCols);
		const double dy = width / (double)(nCols - 1);
		const double dx = height / (double)(nRows - 1);
		for (int i = 0; i < nRows; ++i)
		{
			for (int j = 0; j < nCols; j++)
			{
				const double y = (double)dy*j;
				const double x = (double)dx*i;
				vertList[i*nCols + j] = Vec3d(x - 30, -10, y - 30);

			}
		}
		clothMesh->setInitialVertexPositions(vertList);
		clothMesh->setVertexPositions(vertList);

		// c. Add connectivity data
		std::vector<imstk::SurfaceMesh::TriangleArray> triangles;
		for (std::size_t i = 0; i < nRows - 1; ++i)
		{
			for (std::size_t j = 0; j < nCols - 1; j++)
			{
				imstk::SurfaceMesh::TriangleArray tri[2];
				tri[0] = { { i*nCols + j, i*nCols + j + 1, (i + 1)*nCols + j } };
				tri[1] = { { (i + 1)*nCols + j + 1, (i + 1)*nCols + j, i*nCols + j + 1 } };
				triangles.push_back(tri[0]);
				triangles.push_back(tri[1]);
			}
		}
		clothMesh->setTrianglesVertices(triangles);

		auto oneToOneFloor = std::make_shared<imstk::OneToOneMap>();
		oneToOneFloor->setMaster(clothMesh);
		oneToOneFloor->setSlave(clothMesh);
		oneToOneFloor->compute();

		auto floor = std::make_shared<PbdObject>("Floor");
		floor->setCollidingGeometry(clothMesh);
		floor->setVisualGeometry(clothMesh);
		floor->setPhysicsGeometry(clothMesh);
		floor->setPhysicsToCollidingMap(oneToOneFloor);
		floor->setPhysicsToVisualMap(oneToOneFloor);
		//floor->setCollidingToVisualMap(oneToOneFloor);

		auto pbdModel2 = std::make_shared<PbdModel>();
        pbdModel2->configure(/*Number of constraints*/2,
			/*Constraint configuration*/"Distance 0.1",
			/*Constraint configuration*/"Dihedral 0.001",
			/*Mass*/0.0,
			/*Gravity*/"0 9.8 0",
			/*TimeStep*/0.001,
			/*FixedPoint*/fixed_corner.c_str(),
			/*NumberOfIterationInConstraintSolver*/5,
			/*Proximity*/0.1,
			/*Contact stiffness*/0.95); // org 0.95
        floor->setDynamicalModel(pbdModel2);

		//auto pbdSolver1 = std::make_shared<PbdSolver>();
		//pbdSolver1->setPbdObject(floor);
		//scene->addNonlinearSolver(pbdSolver1);

		scene->addSceneObject(floor);

		std::cout << "nbr of vertices in cloth mesh" << clothMesh->getNumVertices() << std::endl;

		// Collisions
		auto clothTestcolGraph = scene->getCollisionGraph();
		auto pair1 = std::make_shared<PbdInteractionPair>(PbdInteractionPair(deformableObj, floor));
		pair1->setNumberOfInterations(5);

		clothTestcolGraph->addInteractionPair(pair1);

		scene->getCamera()->setPosition(0, 0, 50);
	}
	else if (0){
		auto tetMesh1 = imstk::MeshIO::read(iMSTK_DATA_ROOT"/asianDragon/asianDragon.veg");
		if (!tetMesh1)
		{
			LOG(WARNING) << "Could not read mesh from file.";
			return;
		}

		auto surfMesh1 = std::make_shared<imstk::SurfaceMesh>();
		auto surfMeshVisual1 = std::make_shared<imstk::SurfaceMesh>();
		auto volTetMesh1 = std::dynamic_pointer_cast<imstk::TetrahedralMesh>(tetMesh1);
		if (!volTetMesh1)
		{
			LOG(WARNING) << "Dynamic pointer cast from imstk::Mesh to imstk::TetrahedralMesh failed!";
			return;
		}

		auto vs = volTetMesh1->getInitialVertexPositions();
		Vec3d tmpPos;
		for (int i = 0; i < volTetMesh1->getNumVertices(); ++i){
			tmpPos = volTetMesh1->getVertexPosition(i);
			tmpPos[1] -= 6;
			volTetMesh1->setVertexPosition(i, tmpPos);
		}
		volTetMesh1->setInitialVertexPositions(volTetMesh1->getVertexPositions());

		volTetMesh1->extractSurfaceMesh(surfMesh1);
		volTetMesh1->extractSurfaceMesh(surfMeshVisual1);


		auto deformMapP2V1 = std::make_shared<imstk::OneToOneMap>();
		deformMapP2V1->setMaster(volTetMesh1);
		deformMapP2V1->setSlave(surfMeshVisual1);
		deformMapP2V1->compute();

		auto deformMapC2V1 = std::make_shared<imstk::OneToOneMap>();
		deformMapC2V1->setMaster(surfMesh1);
		deformMapC2V1->setSlave(surfMeshVisual1);
		deformMapC2V1->compute();

		auto deformMapP2C1 = std::make_shared<imstk::OneToOneMap>();
		deformMapP2C1->setMaster(volTetMesh1);
		deformMapP2C1->setSlave(surfMesh1);
		deformMapP2C1->compute();

        auto deformableModel1 = std::make_shared<PbdModel>();
        auto deformableObj1 = std::make_shared<PbdObject>("Dragon2");
		deformableObj1->setVisualGeometry(surfMeshVisual1);
		deformableObj1->setCollidingGeometry(surfMesh1);
		deformableObj1->setPhysicsGeometry(volTetMesh1);
		deformableObj1->setPhysicsToCollidingMap(deformMapP2C1);
		deformableObj1->setPhysicsToVisualMap(deformMapP2V1);
		deformableObj1->setCollidingToVisualMap(deformMapC2V1);
        deformableModel1->configure(/*Number of Constraints*/1,
			/*Constraint configuration*/"FEM NeoHookean 10.0 0.5",
			/*Mass*/0.0,
			/*Gravity*/"0 -9.8 0",
			/*TimeStep*/0.002,
			/*FixedPoint*/"",
			/*NumberOfIterationInConstraintSolver*/4, // 2
			/*Proximity*/0.1,
			/*Contact stiffness*/0.05);  // 0.01
        deformableObj1->setDynamicalModel(deformableModel1);

        scene->addSceneObject(deformableObj1);

		// Collisions
		auto colGraph = scene->getCollisionGraph();
		auto pair = std::make_shared<PbdInteractionPair>(PbdInteractionPair(deformableObj, deformableObj1));
		pair->setNumberOfInterations(2);

		colGraph->addInteractionPair(pair);
	}
	else{
		// floor

		imstk::StdVectorOfVec3d vertList;
		double width = 100.0;
		double height = 100.0;
		int nRows = 2;
		int nCols = 2;
		vertList.resize(nRows*nCols);
		const double dy = width / (double)(nCols - 1);
		const double dx = height / (double)(nRows - 1);
		for (int i = 0; i < nRows; ++i)
		{
			for (int j = 0; j < nCols; j++)
			{
				const double y = (double)dy*j;
				const double x = (double)dx*i;
				vertList[i*nCols + j] = Vec3d(x - 50, -10.0, y - 50);

			}
		}

		// c. Add connectivity data
		std::vector<imstk::SurfaceMesh::TriangleArray> triangles;
		for (std::size_t i = 0; i < nRows - 1; ++i)
		{
			for (std::size_t j = 0; j < nCols - 1; j++)
			{
				imstk::SurfaceMesh::TriangleArray tri[2];
				tri[0] = { { i*nCols + j, i*nCols + j + 1, (i + 1)*nCols + j } };
				tri[1] = { { (i + 1)*nCols + j + 1, (i + 1)*nCols + j, i*nCols + j + 1 } };
				triangles.push_back(tri[0]);
				triangles.push_back(tri[1]);
			}
		}
		auto floorMeshColliding = std::make_shared<imstk::SurfaceMesh>();
		floorMeshColliding->initialize(vertList, triangles);
		auto floorMeshVisual = std::make_shared<imstk::SurfaceMesh>();
		floorMeshVisual->initialize(vertList, triangles);
		auto floorMeshPhysics = std::make_shared<imstk::SurfaceMesh>();
		floorMeshPhysics->initialize(vertList, triangles);


		auto floorMapP2V = std::make_shared<imstk::OneToOneMap>();
		floorMapP2V->setMaster(floorMeshPhysics);
		floorMapP2V->setSlave(floorMeshVisual);
		floorMapP2V->compute();


		auto floorMapP2C = std::make_shared<imstk::OneToOneMap>();
		floorMapP2C->setMaster(floorMeshPhysics);
		floorMapP2C->setSlave(floorMeshColliding);
		floorMapP2C->compute();

		auto floorMapC2V = std::make_shared<imstk::OneToOneMap>();
		floorMapC2V->setMaster(floorMeshColliding);
		floorMapC2V->setSlave(floorMeshVisual);
		floorMapC2V->compute();

		auto floor = std::make_shared<PbdObject>("Floor");
		floor->setCollidingGeometry(floorMeshColliding);
		floor->setVisualGeometry(floorMeshVisual);
		floor->setPhysicsGeometry(floorMeshPhysics);
		floor->setPhysicsToCollidingMap(floorMapP2C);
		floor->setPhysicsToVisualMap(floorMapP2V);
		floor->setCollidingToVisualMap(floorMapC2V);

		auto pbdModel2 = std::make_shared<PbdModel>();
        pbdModel2->configure(/*Number of Constraints*/0,
			/*Mass*/0.0,
			/*Proximity*/0.1,
			/*Contact stiffness*/1.0);
        floor->setDynamicalModel(pbdModel2);

		//auto pbdSolverfloor = std::make_shared<PbdSolver>();
		//pbdSolverfloor->setPbdObject(floor);
		//scene->addNonlinearSolver(pbdSolverfloor);

		scene->addSceneObject(floor);

		// Collisions
		auto colGraph = scene->getCollisionGraph();
		auto pair = std::make_shared<PbdInteractionPair>(PbdInteractionPair(deformableObj, floor));
		pair->setNumberOfInterations(2); // irg 2

		colGraph->addInteractionPair(pair);
	}
	sdk->setActiveScene(scene);
	sdk->startSimulation(true);
}

/************************************************************************/
/* test distance xzh                                                                     */
struct VECTOR
{
	double x, y, z;
};

void setVector(VECTOR & v, const double & x, const double & y, const double & z); // set value of VECTOR

VECTOR sub(const VECTOR & v1, const VECTOR & v2); // sub

double DotProduct(const VECTOR & v1, const VECTOR & v2); // dot product

double Point2Triangle(const VECTOR & point, const VECTOR & v0,
	const VECTOR & v1, const VECTOR & v2);  // distance
double Point2Triangle_LenSqr(const VECTOR & point, const VECTOR & v0,
	const VECTOR & v1, const VECTOR & v2);  // square distance
// xzh modify
void Point2TriangleXzh(const VECTOR & point, const VECTOR & v0,
	const VECTOR & v1, const VECTOR & v2, double &distance, VECTOR &cdPos);  // distance the last two are output parameter
void Point2Triangle_LenSqrXzh(const VECTOR & point, const VECTOR & v0,
	const VECTOR & v1, const VECTOR & v2, double &distance, VECTOR &cdPos);  // square distance
//double Point2TriangleXzh(const VECTOR & point, const VECTOR & v0,
//	const VECTOR & v1, const VECTOR & v2, VECTOR &cdPos);  // distance the last two are output parameter
//double Point2Triangle_LenSqrXzh(const VECTOR & point, const VECTOR & v0,
//	const VECTOR & v1, const VECTOR & v2, VECTOR &cdPos);  // square distance

//////////////////////////////////////////////////////////////////////////
// set value of VECTOR
void setVector(VECTOR & v, const double & x, const double & y, const double & z)
{
	v.x = x;
	v.y = y;
	v.z = z;
}

//////////////////////////////////////////////////////////////////////////
// v1-v2
VECTOR sub(const VECTOR & v1, const VECTOR &  v2)
{
	VECTOR v;
	v.x = v1.x - v2.x;
	v.y = v1.y - v2.y;
	v.z = v1.z - v2.z;

	return v;
}

//////////////////////////////////////////////////////////////////////////
// v1.v2
double DotProduct(const VECTOR & v1, const VECTOR & v2)
{
	return v1.x*v2.x + v1.y*v2.y + v1.z*v2.z;;
}

//////////////////////////////////////////////////////////////////////////
// point to triangle
// from <<Geometric tools for computer graphics>>
// return  distance
double Point2Triangle(const VECTOR & point, const VECTOR & v0, const VECTOR & v1, const VECTOR & v2)
{
	return sqrt(Point2Triangle_LenSqr(point, v0, v1, v2));
}


//////////////////////////////////////////////////////////////////////////
// point to triangle
// from <<Geometric tools for computer graphics>>
// return Square distance
double Point2Triangle_LenSqr(const VECTOR & point, const VECTOR & v0, const VECTOR & v1, const VECTOR & v2)
{
	// basic variable
	VECTOR dd = sub(v0, point);
	VECTOR e0 = sub(v1, v0);
	VECTOR e1 = sub(v2, v0);

	double a, b, c, d, e, f;
	a = DotProduct(e0, e0);
	b = DotProduct(e0, e1);
	c = DotProduct(e1, e1);
	d = DotProduct(e0, dd);
	e = DotProduct(e1, dd);
	f = DotProduct(dd, dd);

	// region
	double det = abs(a*c - b*b);
	double s = b*e - c*d;
	double t = b*d - a*e;

	// region 0
	if (s >= 0 && s <= det && t >= 0 && t <= det  && s + t <= det + 1e-6)
	{
		double invDet = 1 / det;
		s *= invDet;
		t *= invDet;
		return a*s*s + 2 * b*s*t + c*t*t + 2 * d*s + 2 * e*t + f;
	}

	// determine the region
	int region;
	if (s + t <= det)
	{
		if (s < 0)
		{
			if (t < 0)
			{
				region = 4;
			}
			else
			{
				region = 3;
			}
		}
		else
		{
			region = 5;
		}
	}
	else
	{
		if (s < 0)
		{
			region = 2;
		}
		else if (t < 0)
		{
			region = 6;
		}
		else
		{
			region = 1;
		}
	}

	// region 1
	if (region == 1)
	{
		double numer = c + e - b - d;
		if (numer <= 0)
		{
			s = 0;
		}
		else
		{
			double denom = a - 2 * b + c;
			s = (numer >= denom ? 1 : numer / denom);
		}
		t = 1 - s;
	}

	// region 3
	if (region == 3)
	{
		s = 0;
		t = (e >= 0 ? 0 : (-e > c ? 1 : -e / c));
	}

	// region 5
	if (region == 5)
	{
		t = 0;
		s = (d >= 0 ? 0 : (-d > a ? 1 : -d / a));
	}

	// region 2
	if (region == 2)
	{
		double tmp0 = b + d;
		double tmp1 = c + e;
		if (tmp1 > tmp0)
		{
			double numer = tmp1 - tmp0;
			double denom = a - 2 * b + c;
			s = (numer >= denom ? 1 : numer / denom);
			t = 1 - s;
		}
		else
		{
			s = 0;
			t = (e >= 0 ? 0 : (-e > c ? 1 : -e / c));
		}
	}
	// region 4
	if (region == 4)
	{
		if (e < 0)
		{
			s = 0;
			t = t = (e >= 0 ? 0 : (-e > c ? 1 : -e / c));
		}
		else
		{
			t = 0;
			s = (d >= 0 ? 0 : (-d > a ? 1 : -d / a));
		}
	}
	// region 6
	if (region == 6)
	{
		if (a + d > 0)
		{
			t = 0;
			s = (d >= 0 ? 0 : (-d > a ? 1 : -d / a));
		}
		else
		{
			double numer = c + e - b - d;
			double denom = a - 2 * b + c;
			s = (numer >= denom ? 1 : numer / denom);
			t = 1 - s;
		}
	}


	return a*s*s + 2 * b*s*t + c*t*t + 2 * d*s + 2 * e*t + f;
}

// xzh modify
//////////////////////////////////////////////////////////////////////////
// point to triangle
// from <<Geometric tools for computer graphics>>
// return  distance
void Point2TriangleXzh(const VECTOR & point, const VECTOR & v0, const VECTOR & v1, const VECTOR & v2, double &distance, VECTOR &cdPos)
//double Point2TriangleXzh(const VECTOR & point, const VECTOR & v0, const VECTOR & v1, const VECTOR & v2, VECTOR &cdPos)
{
	Point2Triangle_LenSqrXzh(point, v0, v1, v2, distance, cdPos);
	distance = sqrt(distance); // (Point2Triangle_LenSqrXzh(point, v0, v1, v2, distance, cdPos));   // return sqrt(Point2Triangle_LenSqrXzh(point, v0, v1, v2, cdPos));  // (distance); //  (Point2Triangle_LenSqrXzh(point, v0, v1, v2));
}


//////////////////////////////////////////////////////////////////////////
// point to triangle
// from <<Geometric tools for computer graphics>>
// return Square distance
void Point2Triangle_LenSqrXzh(const VECTOR & point, const VECTOR & v0, const VECTOR & v1, const VECTOR & v2, double &distance, VECTOR &cdPos)
//double Point2Triangle_LenSqrXzh(const VECTOR & point, const VECTOR & v0, const VECTOR & v1, const VECTOR & v2, VECTOR &cdPos)
{
	// basic variable
	VECTOR dd = sub(v0, point);
	VECTOR e0 = sub(v1, v0);
	VECTOR e1 = sub(v2, v0);

	double a, b, c, d, e, f;
	a = DotProduct(e0, e0);
	b = DotProduct(e0, e1);
	c = DotProduct(e1, e1);
	d = DotProduct(e0, dd);
	e = DotProduct(e1, dd);
	f = DotProduct(dd, dd);

	// region
	double det = abs(a*c - b*b);
	double s = b*e - c*d;
	double t = b*d - a*e;

	// region 0
	if (s >= 0 && s <= det && t >= 0 && t <= det  && s + t <= det + 1e-6)
	{
		double invDet = 1 / det;
		s *= invDet;
		t *= invDet;
		//cdPos = v0 + s*e0 + t*e1;
		setVector(cdPos, v0.x + s*e0.x + t*e1.x, v0.y + s*e0.y + t*e1.y, v0.z + s*e0.z + t*e1.z);
		distance= a*s*s + 2 * b*s*t + c*t*t + 2 * d*s + 2 * e*t + f;
		return;
	}

	// determine the region
	int region;
	if (s + t <= det)
	{
		if (s < 0)
		{
			if (t < 0)
			{
				region = 4;
			}
			else
			{
				region = 3;
			}
		}
		else
		{
			region = 5;
		}
	}
	else
	{
		if (s < 0)
		{
			region = 2;
		}
		else if (t < 0)
		{
			region = 6;
		}
		else
		{
			region = 1;
		}
	}

	// region 1
	if (region == 1)
	{
		double numer = c + e - b - d;
		if (numer <= 0)
		{
			s = 0;
		}
		else
		{
			double denom = a - 2 * b + c;
			s = (numer >= denom ? 1 : numer / denom);
		}
		t = 1 - s;
	}

	// region 3
	if (region == 3)
	{
		s = 0;
		t = (e >= 0 ? 0 : (-e > c ? 1 : -e / c));
	}

	// region 5
	if (region == 5)
	{
		t = 0;
		s = (d >= 0 ? 0 : (-d > a ? 1 : -d / a));
	}

	// region 2
	if (region == 2)
	{
		double tmp0 = b + d;
		double tmp1 = c + e;
		if (tmp1 > tmp0)
		{
			double numer = tmp1 - tmp0;
			double denom = a - 2 * b + c;
			s = (numer >= denom ? 1 : numer / denom);
			t = 1 - s;
		}
		else
		{
			s = 0;
			t = (e >= 0 ? 0 : (-e > c ? 1 : -e / c));
		}
	}
	// region 4
	if (region == 4)
	{
		if (e < 0)
		{
			s = 0;
			t = t = (e >= 0 ? 0 : (-e > c ? 1 : -e / c));
		}
		else
		{
			t = 0;
			s = (d >= 0 ? 0 : (-d > a ? 1 : -d / a));
		}
	}
	// region 6
	if (region == 6)
	{
		if (a + d > 0)
		{
			t = 0;
			s = (d >= 0 ? 0 : (-d > a ? 1 : -d / a));
		}
		else
		{
			double numer = c + e - b - d;
			double denom = a - 2 * b + c;
			s = (numer >= denom ? 1 : numer / denom);
			t = 1 - s;
		}
	}

	setVector(cdPos, v0.x + s*e0.x + t*e1.x, v0.y + s*e0.y + t*e1.y, v0.z + s*e0.z + t*e1.z);
	distance= a*s*s + 2 * b*s*t + c*t*t + 2 * d*s + 2 * e*t + f;
	return;
}

void testDistance()
{
	// triangle data
	VECTOR v0, v1, v2;
	setVector(v0, 0, 0, 0);
	setVector(v1, 1, 0, 0);
	setVector(v2, 0, 1, 0);

	// point
	VECTOR p;
	setVector(p, 0.0, 1.75, 1.0);

	// distance between point and triangle
	DWORD t1 = GetTickCount();
	double dist = 0.0;
	VECTOR cdPos;
	setVector(cdPos, 0.0, 0.0, 0.0);
	Point2TriangleXzh(p, v0, v1, v2, dist, cdPos);
	//dist=Point2TriangleXzh(p, v0, v1, v2, cdPos);
	//dist=Point2Triangle(p, v0, v1, v2);
	for (unsigned int i = 0; i < 100000;i++)
	{
		//dist = Point2TriangleXzh(p, v0, v1, v2, cdPos);  // 
		Point2TriangleXzh(p, v0, v1, v2, dist, cdPos);
		//dist = Point2Triangle(p, v0, v1, v2);
	}
	DWORD t2 = GetTickCount() - t1;
	std::cout << "time consuming is %f ms" << t2 << "\n";

	// output
	std::cout << "triangle vertex:\n";
	std::cout << "0, 0, 0\n" << "2, 0, 0\n" << "0,1.2, 0\n";
	std::cout << "\npoint: 1.0,-0.6,1\n";
	std::cout << "dist: " << dist << "\n";
	std::cout << "cdPos is:" << cdPos.x << "," << cdPos.y << "," << cdPos.z << "\n";
}
/************************************************************************/