/*=========================================================================

   Library: iMSTK

   Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
   & Imaging in Medicine, Rensselaer Polytechnic Institute.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0.txt

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

=========================================================================*/

#include "imstkSimulationManager.h"
#include "imstkPbdObject.h"
#include "imstkPbdSolver.h"
#include "imstkAPIUtilities.h"

using namespace imstk;

const bool runSimWithoutRendering = false;

///
/// \brief This example demonstrates the cloth simulation
/// using Position based dynamics
///
int main()
{
    auto sdk = std::make_shared<SimulationManager>(runSimWithoutRendering);
    auto scene = sdk->createNewScene("PBDCloth");

    // Light (white)
    auto whiteLight = std::make_shared<DirectionalLight>("whiteLight");
    whiteLight->setFocalPoint(Vec3d(5, -8, -5));
    whiteLight->setIntensity(7);

    // Light (red)
    auto colorLight = std::make_shared<SpotLight>("colorLight");
    colorLight->setPosition(Vec3d(-5, -3, 5));
    colorLight->setFocalPoint(Vec3d(0, -5, 5));
    colorLight->setIntensity(100);
    colorLight->setColor(Color::Red);
    colorLight->setSpotAngle(30);

    // Add in scene
    scene->addLight(whiteLight);
    scene->addLight(colorLight);

    // print UPS
    auto ups = std::make_shared<UPSCounter>();
    apiutils::printUPS(sdk->getSceneManager(scene), ups);

    scene->getCamera()->setFocalPoint(0, -5, 5);
    scene->getCamera()->setPosition(-15., -5.0, 15.0);

	sdk->getViewer()->setOnCharFunction('b', [&](InteractorStyle* c) -> bool
	{
		// Create Object & Model
		auto deformableObj = std::make_shared<VisualDeformableObject>("Cloth");

		// Create surface mesh
		auto surfMesh = std::make_shared<SurfaceMesh>();
		StdVectorOfVec3d vertList;
		const double width = 10.0;
		const double height = 10.0;
		const int nRows = 11;
		const int nCols = 11;
		vertList.resize(nRows*nCols);
		const double dy = width / (double)(nCols - 1);
		const double dx = height / (double)(nRows - 1);
		for (int i = 0; i < nRows; ++i)
		{
			for (int j = 0; j < nCols; j++)
			{
				vertList[i*nCols + j] = Vec3d((double)dx*i, 1.0, (double)dy*j);
			}
		}

		for (int i = 0; i < vertList.size(); i++)
		{
			//surfMesh->setVertexColor(i, Color::Red);
		}

		auto material = std::make_shared<RenderMaterial>();
		material->setBackFaceCulling(false);
		material->setColor(Color::LightGray);
		material->setDisplayMode(RenderMaterial::DisplayMode::WIREFRAME_SURFACE);
		auto surfMeshModel = std::make_shared<VisualModel>(surfMesh);
		surfMeshModel->setRenderMaterial(material);
		deformableObj->addVisualModel(surfMeshModel);

		// Add connectivity data
		std::vector<SurfaceMesh::TriangleArray> triangles;
		for (std::size_t i = 0; i < nRows - 1; ++i)
		{
			for (std::size_t j = 0; j < nCols - 1; j++)
			{
				SurfaceMesh::TriangleArray tri[2];
				tri[0] = { { i*nCols + j, (i + 1)*nCols + j, i*nCols + j + 1 } };
				tri[1] = { { (i + 1)*nCols + j + 1, i*nCols + j + 1, (i + 1)*nCols + j } };
				triangles.push_back(tri[0]);
				triangles.push_back(tri[1]);
			}
		}
		surfMesh->initialize(vertList, triangles);
		////surfMesh->setInitialVertexPositions(vertList);
		////surfMesh->setVertexPositions(vertList);
		//surfMesh->setTrianglesVertices(triangles);

		scene->addSceneObject(deformableObj);
		return false;
	});

	// Start
    sdk->setActiveScene(scene);
    sdk->startSimulation(SimulationStatus::RUNNING);

    // Perform an infinite loop if there is no rendering enabled
    if (runSimWithoutRendering)
    {
        LOG(INFO) << "simulation is starting. PRESS any key to exit";
        while (sdk->getStatus() == SimulationStatus::RUNNING && !getchar()) {}
        sdk->endSimulation();
    }

    return 0;
}
