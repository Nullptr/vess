#-----------------------------------------------------------------------------
# Vulkan renderer
#-----------------------------------------------------------------------------
set(VulkanSDK_ROOT_DIR "$ENV{VULKAN_SDK}")
if(NOT EXISTS ${VulkanSDK_ROOT_DIR})
  message(FATAL_ERROR "\nCan not support Vulkan renderer without Vulkan SDK.\nSet VulkanSDK_ROOT_DIR to Vulkan SDK installation directory.\n\n")
endif()

#-----------------------------------------------------------------------------
# Find path
#-----------------------------------------------------------------------------
unset(VulkanSDK_INCLUDE_DIR CACHE)

find_path(VulkanSDK_INCLUDE_DIR
  NAMES
    vulkan/vulkan.h
  PATHS
    ${VulkanSDK_ROOT_DIR}/Include
  )
mark_as_advanced(FORCE VulkanSDK_INCLUDE_DIR)

#-----------------------------------------------------------------------------
# Find library
#-----------------------------------------------------------------------------
unset(VulkanSDK_LIBRARY CACHE)

find_library(VulkanSDK_LIBRARY
  NAMES
    vulkan-1
  PATHS
    ${VulkanSDK_ROOT_DIR}/Lib
  )
mark_as_advanced(FORCE VulkanSDK_LIBRARY)

set(VulkanSDK_LIBRARIES ${VulkanSDK_LIBRARY})