/*=========================================================================

   Library: iMSTK

   Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
   & Imaging in Medicine, Rensselaer Polytechnic Institute.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0.txt

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

=========================================================================*/

#ifndef imstkPbdInteractionPair_h
#define imstkPbdInteractionPair_h

  ////< XZH 
#define INJECTIONMAX 180
#define STIFFDIST0 0.90  //  0.95
#define STIFFDIST1 0.80 // .95 // 0.90
#define STIFFVOL0 0.60
#define STIFFVOL1 0.55  // 0.85

#include "imstkInteractionPair.h"
#include "imstkPbdEdgeEdgeCollisionConstraint.h"
#include "imstkPbdPointTriCollisionConstraint.h"
#include "imstkPbdObject.h"
// XZH
#include "imstkCylinder.h"
#include "imstkVESSTetrahedralMesh.h"  
#include "imstkPbdVirtualCouplingObject.h"  
#include "imstkPbdPointTriCntCollisionConstraint.h"
#include "imstkPbdEdgeEdgeCntCollisionConstraint.h"
//#include "imstkCuttingVESSCutMesh.h"


namespace imstk
{
///
/// \class PbdInteractionPair
///
/// \brief
///
class PbdInteractionPair
{
public:
    ///
    /// \brief Constructor
    ///
    PbdInteractionPair(std::shared_ptr<PbdObject> A, std::shared_ptr<PbdObject> B) :
        first(A), second(B) {}
	PbdInteractionPair(std::shared_ptr<PbdObject> A, std::shared_ptr<PbdObject> A1, std::shared_ptr<PbdObject> A2, std::shared_ptr<PbdObject> B) :
		first(A), first1(A1), first2(A2), second(B) {}

    ///
    /// \brief get the second object
    ///
    std::shared_ptr<PbdObject> getSecondObj()  { return second; }
    std::shared_ptr<PbdObject> getFirstObj()  { return first; }

    ///
    /// \brief Clear the collisions from previous step
    ///
    inline void resetConstraints()
    {
        m_collisionConstraints.clear();
        // XZH
        m_continuousColVertices.clear();
        m_continuousColPair.clear();
        m_continuousColNode.clear();
        bHasCD = false;
    }

    ///
    /// \brief
    ///
    inline void setNumberOfInterations(const unsigned int& n)
    {
        maxIter = n;
    }

    ///
    /// \brief Broad phase collision detection using AABB
    ///
    bool doBroadPhaseCollision();

    ///
    /// \brief Narrow phase collision detection
    ///
    void doNarrowPhaseCollision();

    ///
    /// \brief Resolves the collision by solving pbd collision constraints
    ///
    void resolveCollision();

    ///
    /// \brief XZH VESS Narrow phase collision detection
    ///
    void doNarrowPhaseCollisionVESS();

    ///
    /// \brief XZH VESS Resolves the collision by solving pbd collision constraints
    ///
    void resolveCollisionVESS();

    ///
    /// \brief introduce the function  xzh
    ///
    void computeContactForce();

    ///
    /// \ For continuous collisition detection XZH
    ///
    void doNarrowPhaseContinuousCollisionVESS();
    void doNarrowPhaseContinuousCollisionVESS2();  ////< XZH no edge-edge CCD
    void doNarrowPhaseContinuousCollisionVESS3();
	void doNarrowPhaseContinuousCollisionVESS4();

    ///
    /// \brief XZH VESS Resolves the collision by solving pbd collision constraints
    ///
    void resolveContinuousCollisionVESS();

    ///
    /// \ when no collision should update finalPos of tool
    ///
    void updateToolPos();

    ///
    /// \ force feedback based on virtual coupling and continuous CD  xzh
    ///
    void computeContactForceContinuous();
    void computeContactForceInjection(); // for continus pressing buttor in Procedure injection

    const Vec3d& getForce() const { return force; }
    const Vec3d& getCentralPos() const { return centralPos; }
    const Vec3d& getMoveDir() const { return moveDir; }

    ///
    /// \ intersection between segment and triangle
    ///
    int intersectSegmentTriangle(Vec3d s0, Vec3d s1, Vec3d p[3], double& t, Vec3d& uvw, Vec3d& xyz);
    int intersectRayTriangle(Vec3d ro, Vec3d rd, Vec3d p[3], Vec3d& uvt);

    ///
    /// \ for continuous collision detection
    ///
    bool findCollisionCandidate(std::shared_ptr<VESSTetrahedralMesh> tetMesh, ToolInfo& tool, double threshold);
    bool findCollisionCandidateInjection(std::shared_ptr<VESSTetrahedralMesh> tetMesh, ToolInfo& tool, double threshold);
    bool findCollisionCandidate2(std::shared_ptr<VESSTetrahedralMesh> tetMesh, ToolInfo& tool, double threshold);  ////< XZH no edge-edge CD
    bool continuousCollisionDetectionWithSurfaceMesh(std::shared_ptr<VESSTetrahedralMesh> tetMesh, ToolInfo& tool, double dt);

    ///
    /// \ for calculation intersection
    ///
    double cubeRoot(double x);
    int solveCubic(double a, double b, double c, double d, double* result);
    bool solvePolynomialEquation(double a, double b, double c, double d, double& root);
    void simpleDistTestBetweenLineSegments(Vec3d x1, Vec3d x2, Vec3d x3, Vec3d x4, double& s, double& t);
    bool distTestBetweenTriAndPoint(Vec3d x1, Vec3d x2, Vec3d x3, Vec3d x4, double& u, double& v, double& signedDist, Vec3d& norm);

    ///
    /// \brief Procedure 2 for injection solution part, including broad phase collision to find the closest vertex in the inside surface of physical mesh (using for collision)
    ///
    //void doNarrowPhaseCollisionInjection();
	void doNarrowPhaseCollisionInjection2();   ////< XZH updated from 05/28/2019

    ///
    /// \brief  addition collision between tools-cylinder
    ///
    void doSimpleCollision(int type);
    void setSimpleObject(std::shared_ptr<CollidingObject> obj, std::shared_ptr<SceneObject> lineScene) { cylinderObj = obj; lineCylinderObj = lineScene; }

    ///
    /// \brief choose CD or injection solution via Omni button 0
    ///
    void chooseProcedure();

    ///
    /// \brief  status for button
    ///
    const bool& getInjectionState() const { return bIsInjection; }
    Vec3d& getInjectPos() { return vecGlobalVertex0; }
    Vec3d& getTrianglePos0() { return injTriPos0; }
    Vec3d& getTrianglePos1() { return injTriPos1; }
    Vec3d& getTrianglePos2() { return injTriPos2; }
    Vec3d& getInjectTouchPos() { return injTouchPos; }

    ///
    /// \brief Marking procedure XZH
    ///
    void doMarking();
    const Vec3d& getMarkingPos() const { return markingPos; }
    void getMarkingPosIndex(Vec3d& pos, int& idx)  { pos = markingPos; idx = markingIndex; }
    const Vec3d& getSphereTipPos() const { return sphereTipPos; }
    bool& getMarkState() { return bIsMark; }
	std::vector<int>& getInjectList() { return injectList; }
    double& getInjectIncrement() { return injectIncrement; }
    double& getMaxInjectIncrement() { return maxInjectIncrement; }
    int& getAdjLevels() { return adjLevels; }
    bool& getInflationDir() { return bInflationDir; }
    int getProcType() { return mProcType; }
    void setProcType(int tp) { mProcType = tp; }
    bool& getOpenPBD() { return bOpenPBD; }

private:
    std::vector<std::shared_ptr<PbdCollisionConstraint>> m_collisionConstraints;
    std::shared_ptr<PbdObject> first;   ////< XZH 0 marking
    std::shared_ptr<PbdObject> second;
    unsigned int maxIter;
	////< XZH 
	std::shared_ptr<PbdObject> first1; // inj
	std::shared_ptr<PbdObject> first2; // cut

      ////< XZH cylinder
    std::shared_ptr<CollidingObject> cylinderObj; 
    std::shared_ptr<SceneObject> lineCylinderObj;

    // XZH
    Vec3d force = Vec3d(0.0, 0.0, 0.0); // xzh
    bool m_initialStep = true;
    Vec3d m_prevPos;
    double m_stiffness = 5e-01;                 ///> Stiffness coefficient use to compute spring force
    double m_damping = 0.005;                   ///> Damping coefficient used to compute damping force
    Vec3d centralPos = Vec3d(0.0, 0.0, 0.0); // central position of all the collision detection vertices
    Vec3d moveDir = Vec3d(0.0, 0.0, 0.0); // collding pos indicate visual pos

    // CCD
    std::vector<int> m_continuousColVertices;  // save the colliding vertex with unique not repeat
    std::vector<continuousCollisionPair> m_continuousColPair;
    std::vector<continuousCollisionNode> m_continuousColNode;
	std::vector<continuousCollisionPairXZH> m_continuousColPairXZH;  // 09/25/2019 for surrounding issue CD

    // flag
    bool bHasCD=false;
    const double MaxOutputForce = 20;

    // Injection solution
    bool bIsFirstInjected = false; // first inject , sometimes will continuous, but we just pick up the first time's position as injecte pos
    bool bIsInjection = false; // just record the button status, if pressed, it is ture
    bool bIsPreInjection = false; // previous injection status
    bool bIsCurInjection = false; // current inject status
    Vec3d injectPos = Vec3d::Zero(); // record the pos when injection, will not change if press button continuous
    int nVertex = -1;
    int nSurfTri = -1; // record the closest vertex and triangle
    int nVertex0 = -1, nVertex1 = -1, nVertex2 = -1; // the 3 vertex local index of closest triangle
    int nGlobalVertex0 = -1, nGlobalVertex1 = -1, nGlobalVertex2 = -1;  // the 3 vertex global index of closest triangle
    std::list<int> uniqueVertIdList; // find its neighbors vertices with different level
    std::shared_ptr<VESSDeformMesh> deformMesh;
    int nGlobalClosestVertex0 = -1, nGlobalClosestVertex1 = -1, nGlobalClosestVertex2 = -1;
    Vec3d centerGlobalPos = Vec3d::Zero(); // the center of the cloeset vertex center by its neighbors
    Vec3d vecGlobalVertex0 = Vec3d::Zero(); // initial position of the closest vertex
    bool bIsMaximum0 = false, bIsMaximum1 = false, bIsMaximum2 = false; // check whether the inject solution up to the max
    int numInjection = 0;
    Vec3d injTriPos0 = Vec3d::Zero(), injTriPos1 = Vec3d::Zero(), injTriPos2 = Vec3d::Zero();   ////< XZH injecting triangle vertices
    Vec3d injTouchPos = Vec3d::Zero();
      ////< XZH 20190226
    int noinjCount = 0;

    // Marking procedure
    bool bIsMark = false; // if this is ture then mark, otherwise, no
    bool bIsFirstMarked = false; // first marking , sometimes will continuous, but we just pick up the first time's position as marking pos
    bool bIsMarking = false; // just record the button status, if pressed, it is ture
    bool bIsPreMarking = false; // previous marking status
    bool bIsCurMarking = false; // current marking status
    int markingIndex = -1;  ////< XZH the marking procedure related to colon visual mesh vertex index, it matchs marking pos
    Vec3d markingPos = Vec3d::Zero(); // record the pos when marking, will not change if press button continuous
    Vec3d sphereTipPos = Vec3d::Zero();

	  ////< XZH 03/19/2019
	std::vector<int> injectList;
    bool bUpdatePBD = false;
    double injectIncrement = 0.05; // 0.0005;  // each time add
    double maxInjectIncrement = 0.01; // max add inj
    int adjLevels = 30; // 10; // default is 3;
    bool bInflationDir = true;  // true for normal, false for Z

    int mProcType = 0;  ////< XZH 0 marking, 1 injecting, 2 cutting/dissecting  3 switch camera

      ////< XZH 08/19/2019
    bool bOpenPBD = true; // when CCD, set it to false, no PBD
};
}

#endif // imstkPbdInteractionPair_h
