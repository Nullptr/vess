/*=========================================================================

   Library: iMSTK

   Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
   & Imaging in Medicine, Rensselaer Polytechnic Institute.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0.txt

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

=========================================================================*/

#include "imstkPbdInteractionPair.h"
#include "imstkSurfaceMesh.h"
#include "imstkVESSTetrahedralMesh.h" // XZH
#include "imstkLineMesh.h"
#include "imstkGeometryMap.h"
#include "imstkIntersectionTestUtils.h"

namespace imstk
{
bool
PbdInteractionPair::doBroadPhaseCollision()
{
    auto g1 = first->getCollidingGeometry();
    auto g2 = second->getCollidingGeometry();
    auto mesh1 = std::static_pointer_cast<PointSet>(g1);
    auto mesh2 = std::static_pointer_cast<PointSet>(g2);

    Vec3d min1, max1;
    mesh1->computeBoundingBox(min1, max1);

    Vec3d min2, max2;
    mesh2->computeBoundingBox(min2, max2);

    auto dynaModel1 = std::static_pointer_cast<PbdModel>(first->getDynamicalModel());
    auto dynaModel2 = std::static_pointer_cast<PbdModel>(second->getDynamicalModel());

    auto prox1 = dynaModel1->getProximity();
    auto prox2 = dynaModel2->getProximity();

    return testAABBToAABB(min1[0] - prox1, max1[0] + prox1, min1[1] - prox1, max1[1] + prox1,
                          min1[2] - prox1, max1[2] + prox1, min2[0] - prox2, max2[0] + prox2,
                          min2[1] - prox2, max2[1] + prox2, min2[2] - prox2, max2[2] + prox2);
}

void
PbdInteractionPair::doNarrowPhaseCollision()
{
    auto g1 = first->getCollidingGeometry();
    auto g2 = second->getCollidingGeometry();

    auto map1 = first->getPhysicsToCollidingMap();
    auto map2 = second->getPhysicsToCollidingMap();

    auto dynaModel1 = std::static_pointer_cast<PbdModel>(first->getDynamicalModel());
    auto dynaModel2 = std::static_pointer_cast<PbdModel>(second->getDynamicalModel());

    auto prox1 = dynaModel1->getProximity();
    auto prox2 = dynaModel2->getProximity();

    auto mesh2 = std::static_pointer_cast<SurfaceMesh>(g2);

    if (g1->getType() == Geometry::Type::LineMesh)
    {
        auto mesh1 = std::static_pointer_cast<LineMesh>(g1);

        // brute force, use BVH or spatial grid would be much better
        // point
        for (size_t i = 0; i < mesh1->getNumVertices(); ++i)
        {
            Vec3d p = mesh1->getVertexPosition(i);
            std::vector<SurfaceMesh::TriangleArray> elements = mesh2->getTrianglesVertices();

            for (size_t j = 0; j < elements.size(); ++j)
            {
                SurfaceMesh::TriangleArray& e = elements[j];
                const Vec3d p0 = mesh2->getVertexPosition(e[0]);
                const Vec3d p1 = mesh2->getVertexPosition(e[1]);
                const Vec3d p2 = mesh2->getVertexPosition(e[2]);

                if (testPointToTriAABB(p[0], p[1], p[2],
                    p0[0], p0[1], p0[2],
                    p1[0], p1[1], p1[2],
                    p2[0], p2[1], p2[2], prox1, prox2))
                {
                    auto c = std::make_shared<PbdPointTriangleConstraint>();
                    c->initConstraint(dynaModel1, i,
                        dynaModel2, map2->getMapIdx(e[0]), map2->getMapIdx(e[1]), map2->getMapIdx(e[2]));
                    m_collisionConstraints.push_back(c);
                }
            }
        }

        const auto nL1 = mesh1->getNumLines();
        const auto nV2 = mesh2->getNumVertices();
        std::vector<std::vector<bool>> E2(nV2, std::vector<bool>(nV2, 1));
        std::vector<SurfaceMesh::TriangleArray> elements2 = mesh2->getTrianglesVertices();

        for (int k = 0; k < nL1; ++k)
        {
            auto nodes = mesh1->getLinesVertices()[k];
            unsigned int i1 = nodes[0];
            unsigned int i2 = nodes[1];

            const Vec3d P = mesh1->getVertexPosition(i1);
            const Vec3d Q = mesh1->getVertexPosition(i2);

            for (size_t j = 0; j < elements2.size(); ++j)
            {
                SurfaceMesh::TriangleArray& e = elements2[j];
                const Vec3d p0 = mesh2->getVertexPosition(e[0]);
                const Vec3d p1 = mesh2->getVertexPosition(e[1]);
                const Vec3d p2 = mesh2->getVertexPosition(e[2]);

                if (E2[e[0]][e[1]] && E2[e[1]][e[0]])
                {
                    if (testLineToLineAABB(P[0], P[1], P[2],
                        Q[0], Q[1], Q[2],
                        p0[0], p0[1], p0[2],
                        p1[0], p1[1], p1[2], prox1, prox2))
                    {
                        auto c = std::make_shared<PbdEdgeEdgeConstraint>();
                        c->initConstraint(dynaModel1, map1->getMapIdx(i1), map1->getMapIdx(i2),
                            dynaModel2, map2->getMapIdx(e[0]), map1->getMapIdx(e[1]));
                        m_collisionConstraints.push_back(c);
                    }
                    E2[e[0]][e[1]] = 0;
                }
                if (E2[e[1]][e[2]] && E2[e[2]][e[1]])
                {
                    if (testLineToLineAABB(P[0], P[1], P[2],
                        Q[0], Q[1], Q[2],
                        p1[0], p1[1], p1[2],
                        p2[0], p2[1], p2[2], prox1, prox2))
                    {
                        auto c = std::make_shared<PbdEdgeEdgeConstraint>();
                        c->initConstraint(dynaModel1, map1->getMapIdx(i1), map1->getMapIdx(i2),
                            dynaModel2, map2->getMapIdx(e[1]), map1->getMapIdx(e[2]));
                        m_collisionConstraints.push_back(c);
                    }
                    E2[e[1]][e[2]] = 0;
                }
                if (E2[e[2]][e[0]] && E2[e[0]][e[2]])
                {
                    if (testLineToLineAABB(P[0], P[1], P[2],
                        Q[0], Q[1], Q[2],
                        p2[0], p2[1], p2[2],
                        p0[0], p0[1], p0[2], prox1, prox2))
                    {
                        auto c = std::make_shared<PbdEdgeEdgeConstraint>();
                        c->initConstraint(dynaModel1, map1->getMapIdx(i1), map1->getMapIdx(i2),
                            dynaModel2, map2->getMapIdx(e[2]), map1->getMapIdx(e[0]));
                        m_collisionConstraints.push_back(c);
                    }
                    E2[e[2]][e[0]] = 0;
                }
            }
        }
    }

	  ////< XZH surface-surface col  deleted by XZH
}

void
PbdInteractionPair::resolveCollision()
{
    if (!m_collisionConstraints.empty())
    {
        unsigned int i = 0;
        while (++i < maxIter)
        {
            for (size_t k = 0; k < m_collisionConstraints.size(); ++k)
            {
                m_collisionConstraints[k]->solvePositionConstraint();
            }
        }
    }
}

void
PbdInteractionPair::resolveCollisionVESS()
{
    force = Vec3d(0, 0, 0); // xzh each time should reset the force
    Vec3d tmp = Vec3d::Zero();
    if (!m_collisionConstraints.empty())
    {
        unsigned int i = 0;
        while (++i <= maxIter)  // XZH original is  ++i < maxIter
        {
            Vec3d forceTmp = Vec3d(0, 0, 0);
            int num1, num2;
            num1=num2= 0;
            
            for (size_t k = 0; k < m_collisionConstraints.size(); ++k)
            {
                //m_collisionConstraints[k]->solvePositionConstraint();
                //Vec3d& forceTmp = tmp;
                m_collisionConstraints[k]->solvePositionConstraint(forceTmp);
                              
                auto temp = std::dynamic_pointer_cast<PbdPointTriangleConstraint>(m_collisionConstraints[k]);
                if (temp)
                {
                    num1++;
                    //printf("PT: %d ", num1);
                }
                auto temp2 = std::dynamic_pointer_cast<PbdEdgeEdgeConstraint>(m_collisionConstraints[k]);
                if (temp2)
                {
                    num2++;
                    //printf(" EE: %d ", num2);
                }
                ////if (i==1)
                //{
                //    force += forceTmp; // just calculte the first time for solver
                //}
                //printf("force EE PT: %f %f %f %f\n", force.x(), force.y(), force.z(), force.norm());
            }
            
            //force /= m_collisionConstraints.size();   
            //printf("Total: %d PT %d  EE %d\n ", m_collisionConstraints.size(), num1, num2);
            if (num1>0)
            {
                forceTmp /= num1;
            }
            //printf("force PT forTmp out: num1= %d, %f %f %f %f\n", num1,  forceTmp.x(), forceTmp.y(), forceTmp.z(), forceTmp.norm());
            force += forceTmp;
            //printf("force PT for out: %f %f %f %f\n", force.x(), force.y(), force.z(), force.norm());
        }      
        //printf("force PT while out: %f %f %f %f\n", force.x(), force.y(), force.z(), force.norm());
        computeContactForce(); // xzh
    }
    else
    {
        centralPos = Vec3d::Zero();
        // new way XZH
        if (force.norm() < 0.00001)
        {
            Vec3d temp = first->getForce();
            //printf("force CH from CD: %f %f %f %f\n", force.x(), force.y(), force.z(), force.norm());
            if (temp.norm() < 0.000001)
            {
                first->clearForceList();
            }
            first->setForce(temp);
            const auto visualGeometry = std::static_pointer_cast<SurfaceMesh>(first->getVisualGeometry());
            visualGeometry->setTranslation(Vec3d(0, 0, 0));
        }
    }  
}

// xzh
void
PbdInteractionPair::computeContactForce()
{
    centralPos = Vec3d::Zero();
    moveDir = Vec3d::Zero();
    const auto collidingGeometry = std::static_pointer_cast<LineMesh>(first->getCollidingGeometry());
    const auto visualGeometry = std::static_pointer_cast<SurfaceMesh>(first->getVisualGeometry());
    // new way XZH
    if (force.norm() < 0.00001)
    {
        Vec3d temp = first->getForce();
        //printf("force CH from CD: %f %f %f %f\n", force.x(), force.y(), force.z(), force.norm());
        if (temp.norm()<0.000001)
        {
            first->clearForceList();
        }      
        first->setForce(temp);
        //first->setForce(Vec3d::Zero());  // no force
        visualGeometry->setTranslation(Vec3d(0,0,0));
    }
    else
    {
        // XZH move the visual of tool to the central position of all the collision detection vertices
        std::vector<Vec3d> posListEdge;
        std::vector<double> distanceListEdge;
        std::vector<Vec3d> posListTri;
        std::vector<double> distanceListTri;
        Vec3d p0 = collidingGeometry->getVertexPosition(0);
        Vec3d p1 = collidingGeometry->getVertexPosition(1);
        Vec3d a, b, c, e;
        double disTotalEdge = 0.0;
        double disTotalTri = 0.0;
        bool isPointTri = false;
        using EdgeArray = std::array<size_t, 2>;
        std::vector<EdgeArray> edgeList;  // node index
        for (size_t k = 0; k < m_collisionConstraints.size(); ++k)
        {
            int num = m_collisionConstraints.size();
            auto temp = std::dynamic_pointer_cast<PbdPointTriangleConstraint>(m_collisionConstraints[k]);
            if (temp)
            {
                isPointTri = true;
                const auto i1 = m_collisionConstraints[k]->m_bodiesSecond[0];
                const auto i2 = m_collisionConstraints[k]->m_bodiesSecond[1];  // deformable object
                const auto i3 = m_collisionConstraints[k]->m_bodiesSecond[2];

                auto state2 = m_collisionConstraints[k]->m_model2->getCurrentState();

                Vec3d& x1 = state2->getVertexPosition(i1);
                Vec3d& x2 = state2->getVertexPosition(i2);
                Vec3d& x3 = state2->getVertexPosition(i3);

                // calculate the distance from the vertice to the tool X1
                Vec3d v1 = x1;
                Vec3d v2 = x2;
                Vec3d v3 = x3;
                a = v1 - p0;
                b = p1 - p0;
                c = ((a.dot(b)) / (b.dot(b)))*b;
                e = a - c;
                posListTri.push_back(v1);
                distanceListTri.push_back(e.norm());
                disTotalTri += e.norm();
                // x2
                a = v2 - p0;
                b = p1 - p0;
                c = ((a.dot(b)) / (b.dot(b)))*b;
                e = a - c;
                posListTri.push_back(v2);
                distanceListTri.push_back(e.norm());
                disTotalTri += e.norm();
                // x3
                a = v3 - p0;
                b = p1 - p0;
                c = ((a.dot(b)) / (b.dot(b)))*b;
                e = a - c;
                posListTri.push_back(v3);
                distanceListTri.push_back(e.norm());
                disTotalTri += e.norm();
                // END calcluation

                //centralPos += (x1 + x2 + x3) / (3 * num);
            }
            auto temp2 = std::dynamic_pointer_cast<PbdEdgeEdgeConstraint>(m_collisionConstraints[k]);
            if (temp2)
            {
                const auto i2 = m_collisionConstraints[k]->m_bodiesSecond[0];  // deformable object
                const auto i3 = m_collisionConstraints[k]->m_bodiesSecond[1];

                auto state2 = m_collisionConstraints[k]->m_model2->getCurrentState();

                Vec3d& x2 = state2->getVertexPosition(i2);
                Vec3d& x3 = state2->getVertexPosition(i3);
                EdgeArray eetmp = { { i2, i3 } };
                edgeList.push_back(eetmp);
                // calculate the distance from the vertice to the tool X1
                Vec3d v2 = x2;
                Vec3d v3 = x3;
                // x2
                a = v2 - p0;
                b = p1 - p0;
                c = ((a.dot(b)) / (b.dot(b)))*b;
                e = a - c;
                posListEdge.push_back(v2);
                distanceListEdge.push_back(e.norm());
                disTotalEdge += e.norm();
                // x3
                a = v3 - p0;
                b = p1 - p0;
                c = ((a.dot(b)) / (b.dot(b)))*b;
                e = a - c;
                posListEdge.push_back(v3);
                distanceListEdge.push_back(e.norm());
                disTotalEdge += e.norm();
                // END calcluation
                //centralPos += 0.5*(x2 + x3) / num;
            }
        }
        //// average all the vertices  method 1
        //if (disTotal>0.0)
        //{
        //    for (int z = 0; z < posList.size(); z++)
        //    {
        //        centralPos += posList[z] * distanceList[z] / disTotal;
        //    }
        //}
        // find the closest point  method 2
        //double disTmp = distanceList[0];
        //int disNum = 0;
        //for (int m = 0; m < distanceList.size(); m++)
        //{
        //    if (disTmp>distanceList[m])
        //    {
        //        disTmp = distanceList[m];
        //        disNum = m;
        //    }
        //}
        //centralPos = posList[disNum];
        // seperate two Edge-Edge  Point-Triangle  Method3
        auto g2 = second->getCollidingGeometry();
        auto mesh2 = std::static_pointer_cast<VESSTetrahedralMesh>(g2);
        double disTmp = 0; //  distanceListTri[0];
        int disNum = 0;
        std::vector<SurfaceMesh::TriangleArray> elements = mesh2->getTetrahedraSurfaceVertices();
        if (isPointTri)  // using point-triangle
        {
            disTmp =distanceListTri[0];
            for (int m = 0; m < distanceListTri.size(); m++)
            {
                if (disTmp>distanceListTri[m])
                {
                    disTmp = distanceListTri[m];
                    disNum = m;
                }
            }
            centralPos = posListTri[disNum];
        }
        else  // edge-edge   traverse the related triangle to intersection
        {
            std::vector<int> vecSurTri;
            FILE *edgeInter = fopen("G:/edgeInter.dat", "a");
            int mInter = 0;
            for (int m = 0; m < edgeList.size(); m++)
            {
                EdgeArray& edgearray = edgeList[m];
                int m0 = edgearray[0];
                int m1 = edgearray[1];
                if (m0>m1)
                {
                    std::swap(m0, m1);
                }
                U64 edgekey = HASHEDGEID_FROM_IDX(m0, m1);
                //std::unordered_map< U64, int >::iterator it = mesh2->m_mapEdgeTriIndex.find(edgekey);
                for (std::unordered_map< U64, int >::iterator it = mesh2->m_mapEdgeTriIndex.begin(); it != mesh2->m_mapEdgeTriIndex.end(); it++) {
                    if (it->first==edgekey)
                    {
                        vecSurTri.push_back(it->second); // triangle number
                    }             
                }
                //vars
                Vec3d uvw, xyz, ss0, ss1;
                double t;
                Vec3d centralIntersection = Vec3d::Zero();
                int numInter = 0;
                for (int k = 0; k < vecSurTri.size();k++)
                {
                    SurfaceMesh::TriangleArray& e = elements[vecSurTri[k]];
                    const Vec3d pp0 = mesh2->getVertexPosition(e[0]);
                    const Vec3d pp1 = mesh2->getVertexPosition(e[1]);
                    const Vec3d pp2 = mesh2->getVertexPosition(e[2]);
                    Vec3d tri1[3];
                    tri1[0] = pp0;
                    tri1[1] = pp1;
                    tri1[2] = pp2;
                    int res = IntersectSegmentTriangle(p0, p1, tri1, t, uvw, xyz);  // (posListEdge[m*2], posListEdge[m*2+1], tri1, t, uvw, xyz);
                    if (res>0)
                    {
                        centralIntersection += xyz; //  / vecSurTri.size();
                        numInter++;
                        fprintf(edgeInter, "e: %d %d %d, xyz: %f %f %f p0: %f %f %f p1: %f %f %f \n", e[0], e[1], e[2], xyz.x(), xyz.y(), xyz.z(), p0.x(), p0.y(), p0.z(), p1.x(), p1.y(), p1.z());
                    }
                    else
                    {
                        
                    }
                }
                if (numInter > 0)
                {
                    centralIntersection /= numInter;
                    fprintf(edgeInter, "numInter: %d, centralIntersection: %f %f %f \n\n", numInter, centralIntersection.x(), centralIntersection.y(), centralIntersection.z());
                }
                //if (centralIntersection.norm()>0.0001)
                //{
                //    mInter++;
                //    centralPos += centralIntersection;
                //}
                centralPos = centralIntersection;
                fprintf(edgeInter, "mInter: %d, centralPos: %f %f %f \n\n", mInter, centralPos.x(), centralPos.y(), centralPos.z());
            }
            fclose(edgeInter);
            //if (mInter > 0)
            //{
            //    centralPos = centralPos / mInter;
            //}
        }
        // XZH END
        force /= maxIter;
        // Check if any collisions
        const auto collidingObjPos = collidingGeometry->getVertexPosition(1); // Omni contact point  1 not 2 collidingGeometry->getTranslation();
        // Update the visual object position
        Vec3d visualObjPos = centralPos-collidingObjPos ; //  collidingObjPos + force; //  *m_collisionConstraints.size();
        //centralPos -= collidingObjPos;      
        moveDir = centralPos - collidingObjPos;
        //centralPos /= 3;
        visualGeometry->setTranslation(moveDir);  // visualGeometry->setTranslation(force);

        // Spring force
        Vec3d  forceMain = m_stiffness*moveDir; //  m_stiffness * force; //  (visualObjPos - collidingObjPos);

        // Damping force
        const double dt = 0.1; // Time step size to calculate the object velocity
        forceMain += m_initialStep ? Vec3d(0.0, 0.0, 0.0) : m_damping * (collidingObjPos - m_prevPos) / dt;
        // Update object contact force
        forceMain += first->getForce();
        first->setForce(forceMain);
        //first->setForce(force * 30);  // just the first (tool) will get force, colon just deformation
        // Housekeeping
        m_initialStep = false;
        m_prevPos = collidingObjPos;
    }
    // old way
    //if (force.norm() < 0.0000001)
    //{
    //    first->clearForceList();
    //}
    //first->setForce(force * 1);  // no force
}

int 
PbdInteractionPair::intersectSegmentTriangle(Vec3d s0, Vec3d s1, Vec3d p[3], double& t, Vec3d& uvw, Vec3d& xyz) 
{

    Vec3d delta = s1 - s0;
    Vec3d rd = delta;
    rd.normalize();
    double deltaLen = delta.norm();
    Vec3d uvt;
    int res = intersectRayTriangle(s0, rd, p, uvt);
    if (res > 0) {
        if (uvt.z() >= 0.0 && uvt.z() <= deltaLen) {
            //Barycentric coordinate
            uvw = Vec3d(uvt.x(), uvt.y(), 1.0 - uvt.x() - uvt.y());

            //Cartesian Coordinate
            xyz = s0 + rd * uvt.z();

            //return the t distance on the segment
            t = uvt.z();

            return res;
        }
    }
    return 0;
}
//  edge triangle intersection
int 
PbdInteractionPair::intersectRayTriangle(Vec3d ro, Vec3d rd, Vec3d p[3], Vec3d& uvt) 
{

    Vec3d e1 = p[1] - p[0];
    Vec3d e2 = p[2] - p[0];
    Vec3d q = rd.cross(e2);

    //Test Determinant
    double a = e1.dot(q);
    if (fabs(a) < 0.0001f)  // const float EPSILON = 0.0001f
        return 0;

    //Test U
    double f = 1.0 / a;
    Vec3d s = ro - p[0];
    uvt.x() = f * (s.dot(q));
    if (uvt.x() < 0.0)
        return 0;

    //Test V
    Vec3d r = s.cross(e1);
    uvt.y() = f * (rd.dot(r));
    if ((uvt.y() < 0.0) || ((uvt.x() + uvt.y()) > 1.0))
        return 0;

    //Test T
    uvt.z() = f * (e2.dot(r));

    return 1;
}

//////////////////////////////////////////////////////////////////////////  this function will make VS compile iMSTK very slow, 6-8 minutes
void
PbdInteractionPair::doNarrowPhaseContinuousCollisionVESS3()
{
    //return;
    bool isTool = false;
    auto toolObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(first1);
    if (!toolObj) return;  // not pbdvcobj

    auto g1 = first->getCollidingGeometry();
    auto g2 = second->getCollidingGeometry();
    auto g1visual = first->getVisualGeometry();

    auto map1 = first->getPhysicsToCollidingMap();
    auto map2 = second->getPhysicsToCollidingMap();

    auto dynaModel1 = std::static_pointer_cast<PbdModel>(first->getDynamicalModel());
    auto dynaModel2 = std::static_pointer_cast<PbdModel>(second->getDynamicalModel());
    auto state1 = dynaModel1->getCurrentState();
    auto state2 = dynaModel2->getCurrentState();

    auto prox1 = dynaModel1->getProximity();
    auto prox2 = dynaModel2->getProximity();
    auto contactstiff1 = dynaModel1->getContactStiffness(); // 0.001
    auto contactstiff2 = dynaModel2->getContactStiffness(); // 0.1
		////< XZH change stiffness
	double& stiffDis = dynaModel2->getStiffDistance();
	double& stiffVolume = dynaModel2->getStiffVolume();

    auto mesh1 = std::static_pointer_cast<LineMesh>(g1);
    auto mesh2 = std::static_pointer_cast<VESSTetrahedralMesh>(g2);
    auto mesh1Visual = std::static_pointer_cast<SurfaceMesh>(g1visual);

	if ((!mesh1) || (!mesh2))
	{
		stiffDis = STIFFDIST0;
		stiffVolume = STIFFVOL0;
		return; // convertion failed
	}
    //dynaModel2->resetFixedPoints(); // if not injection solution, should reset these fixedpoints 07/19/2017

    auto& tool = toolObj->getControllerTool();
    bool res = findCollisionCandidate2(mesh2, tool, tool.cullingThickness[0] * 1.0);  //  4.5 4.5 *20  currentState=0 should use cullingthickness (20) not prox1 (0.2)
    if (!res)   //  no collision
    {
        bHasCD = false;
        bOpenPBD = false;
        tool.finalPos = tool.drawPos; // if no, should update finalP
        mesh1Visual->setRotation(tool.rotArr);
        mesh1Visual->setTranslation(tool.finalPos);
		stiffDis = STIFFDIST0;
		stiffVolume = STIFFVOL0;
        return;
    }
    bHasCD = true;

    auto& physVertice = mesh2->getPhysVertex();
    auto& physEdge = mesh2->getPhysEdge();
    auto& physSurfTris = mesh2->getPhysSurfTris();
    // Hash Table
    auto& hashtableVertices = mesh2->getHashTableVess();

    // after pbdSolver should update the position
    for (int i = 0; i < physVertice.size(); i++)
    {
        Vec3d& curpos = state2->getVertexPosition(physVertice[i].globalIdx);   // pbdSolver might change it
        physVertice[i].p = curpos;
    }

    // tool w.r.t Omni
    Vec3d end0 = tool.pDrawPos + tool.pGloVec[0];
    Vec3d end1 = tool.pDrawPos + tool.pGloVec[1];

    bool isCCD = false;
    tool.ccdDistance = 10.0;

    // currentState=1
    // continuous collision detection
    Vec3d x1, x2, x3, x4, v1, v2, v3, v4, x21, v21, x31, v31, x41, v41, ia, ib, ic, uN;
    Vec3d px1, px2, px3, px4;
    double a, b, c, d, root;
    double s, t;

    bool col = false;
    if ((tool.pDrawPos - tool.drawPos).norm() < tool.ccdDistance)
        tool.ccdPos = tool.drawPos;
    else
        tool.ccdPos = tool.pDrawPos + tool.ccdDistance * (tool.drawPos - tool.pDrawPos).normalized();

    px1 = tool.pDrawPos + tool.pGloVec[0]; //  first point of tool ROOT
    px2 = tool.pDrawPos + tool.pGloVec[1]; // second TIP
    x1 = tool.ccdPos + tool.gloVec[0];
    x2 = tool.ccdPos + tool.gloVec[1];

    auto nodes = mesh1->getLinesVertices()[0];
    unsigned int i1 = nodes[0];
    unsigned int i2 = nodes[1];

    m_continuousColVertices.clear();

	int nCntEdge = 0, nCntTri = 0;
    // line-line
    for (int i = 0; i < physEdge.size(); i++) // mesh2->getPhysEdge().size()
	{
        auto& eg = physEdge[i]; // mesh2->getPhysEdge()[i];
        if (eg.closeTool && (physVertice[eg.nodeId[0]].fixed == false) && (physVertice[eg.nodeId[1]].fixed == false))
		{
            px3 = physVertice[eg.nodeId[0]].pos;  // previous 
            px4 = physVertice[eg.nodeId[1]].pos;
            x3 = physVertice[eg.nodeId[0]].p;
            x4 = physVertice[eg.nodeId[1]].p;

			v1 = x1 - px1; // printf("diff v1: %.8f %.8f %.8f\n", v1.x(), v1.y(), v1.z());
            v2 = x2 - px2;
            v3 = x3 - px3;
            v4 = x4 - px4;

            x21 = px2 - px1;
            x31 = px3 - px1;
            x41 = px4 - px1;

            v21 = v2 - v1; 
            v31 = v3 - v1;  
            v41 = v4 - v1;

            ia.x() = v21.y()*v31.z() - v21.z()*v31.y();
            ib.x() = x21.y()*v31.z() + v21.y()*x31.z() - x21.z()*v31.y() - v21.z()*x31.y();
            ic.x() = x21.y()*x31.z() - x21.z()*x31.y();

            ia.y() = v21.z()*v31.x() - v21.x()*v31.z();
            ib.y() = x21.z()*v31.x() + v21.z()*x31.x() - x21.x()*v31.z() - v21.x()*x31.z();
            ic.y() = x21.z()*x31.x() - x21.x()*x31.z();

            ia.z() = v21.x()*v31.y() - v21.y()*v31.x();
            ib.z() = x21.x()*v31.y() + v21.x()*x31.y() - x21.y()*v31.x() - v21.y()*x31.x();
            ic.z() = x21.x()*x31.y() - x21.y()*x31.x();

            Vec3d tmp1 = v21.cross(v31);
            Vec3d tmp2 = x21.cross(v31) + v21.cross(x31);
            Vec3d tmp3 = x21.cross(x31);

            a = ia.dot(v41);
            b = ia.dot(x41) + ib.dot(v41);
            c = ib.dot(x41) + ic.dot(v41);
            d = ic.dot(x41);

            bool exist = solvePolynomialEquation(a, b, c, d, root);

            if (exist)
			{
				nCntEdge++;
                simpleDistTestBetweenLineSegments(px1 + root * v1, px2 + root * v2, px3 + root * v3, px4 + root * v4, s, t);
                if (s >= 0.0 && s <= 1.0 && t >= 0.0 && t <= 1.0)
                {
                    col = true;

                    physVertice[eg.nodeId[0]].contact = true;
                    physVertice[eg.nodeId[1]].contact = true;

                    continuousCollisionPair cntpair;
                    cntpair.type = 0;
                    cntpair.toolID = 0;
                    cntpair.toolSegID = 0;
                    cntpair.toolSegPara = s;

                    cntpair.edgeIdx = i;
                    cntpair.edgePara = t;

                    //colNormal
                    Vec3d cv1, cv2, cv3, cv4;

                    cv1 = px1 + root * v1;
                    cv2 = px2 + root * v2;
                    cv3 = px3 + root * v3;
                    cv4 = px4 + root * v4;
                    Vec3d colNorm = ((cv2 - cv1).cross(cv3 - cv1)).normalized();

                    cntpair.colNorm = colNorm;
                    cntpair.colTime = root;
                    m_continuousColPair.push_back(cntpair); // if not use PbdEdgeEdgeContinuousConstraint

                    auto c = std::make_shared<PbdEdgeEdgeContinuousConstraint>();
                    c->initConstraint(dynaModel1, map1->getMapIdx(i1), map1->getMapIdx(i2),
                        dynaModel2, map2->getMapIdx(eg.nodeId[0]), map2->getMapIdx(eg.nodeId[1]),
                        0, s, i, t, colNorm, root);
                    m_collisionConstraints.push_back(c);

                    continuousCollisionNode cntnode0;
                    continuousCollisionNode cntnode1;

                    std::vector<int>::iterator result = find(m_continuousColVertices.begin(), m_continuousColVertices.end(), eg.nodeId[0]);  // find whether 
                    if (result == m_continuousColVertices.end()) // can not find, so it not exists before ,should add
                    {
                        m_continuousColVertices.push_back(eg.nodeId[0]);  // cannot find in vector, so add
                        cntnode0.idx = eg.nodeId[0];
                        cntnode0.nbrCol++;
                        m_continuousColNode.push_back(cntnode0);
                    }
                    else  // repeat
                    {
                        for (int i = 0; i < m_continuousColNode.size(); i++)
                        {
                            if (eg.nodeId[0] == m_continuousColNode[i].idx)
                            {
                                m_continuousColNode[i].nbrCol++;
                                break;  // go outside of for
                            }
                        } // END for vertor
                    } // END if else
                    result = find(m_continuousColVertices.begin(), m_continuousColVertices.end(), eg.nodeId[1]);
                    if (result == m_continuousColVertices.end())
                    {
                        m_continuousColVertices.push_back(eg.nodeId[1]);
                        cntnode1.idx = eg.nodeId[1];
                        cntnode1.nbrCol++;
                        m_continuousColNode.push_back(cntnode1);
                    }
                    else // repeat
                    {
                        for (int i = 0; i < m_continuousColNode.size(); i++)
                        {
                            if (eg.nodeId[1] == m_continuousColNode[i].idx)
                            {
                                m_continuousColNode[i].nbrCol++;
                                break;  // go outside of for
                            }
                        } // END for vertor
                    } // END if else
                }
                else
                {
                    physVertice[eg.nodeId[0]].contact = false;
                    physVertice[eg.nodeId[1]].contact = false;
                }
            }
        } // END if
    } // END for physEdge  

    // point-triangle 
    px4 = px2; // TIP // ROOT is  px1;  // tool.node[0] is root, [1] is tip, we should use tip, not root, so this place shoud change?~~!!!??????***** 
    x4 = x2; // TIP // ROOT is x1; // for tool
    for (int i = 0; i < physSurfTris.size(); i++) //  mesh2->getPhysSurfTris().size()
	{
        auto tris = physSurfTris[i]; //  mesh2->getPhysSurfTris()[i];
        if (tris.closeTool && physEdge[tris.edgeIdx[0]].colDetOn && physEdge[tris.edgeIdx[1]].colDetOn && physEdge[tris.edgeIdx[2]].colDetOn)
        {
            px1 = physVertice[tris.nodeIdx[0]].pos;
            px2 = physVertice[tris.nodeIdx[1]].pos;
            px3 = physVertice[tris.nodeIdx[2]].pos;

            x1 = physVertice[tris.nodeIdx[0]].p;
            x2 = physVertice[tris.nodeIdx[1]].p;
            x3 = physVertice[tris.nodeIdx[2]].p;

            v1 = x1 - px1;
            v2 = x2 - px2;
            v3 = x3 - px3;
            v4 = x4 - px4;

            x21 = px2 - px1;
            x31 = px3 - px1;
            x41 = px4 - px1;

            v21 = v2 - v1;
            v31 = v3 - v1;
            v41 = v4 - v1;

            ia.x() = v21.y()*v31.z() - v21.z()*v31.y();
            ib.x() = x21.y()*v31.z() + v21.y()*x31.z() - x21.z()*v31.y() - v21.z()*x31.y();
            ic.x() = x21.y()*x31.z() - x21.z()*x31.y();

            ia.y() = v21.z()*v31.x() - v21.x()*v31.z();
            ib.y() = x21.z()*v31.x() + v21.z()*x31.x() - x21.x()*v31.z() - v21.x()*x31.z();
            ic.y() = x21.z()*x31.x() - x21.x()*x31.z();

            ia.z() = v21.x()*v31.y() - v21.y()*v31.x();
            ib.z() = x21.x()*v31.y() + v21.x()*x31.y() - x21.y()*v31.x() - v21.y()*x31.x();
            ic.z() = x21.x()*x31.y() - x21.y()*x31.x();

            a = ia.dot(v41);
            b = ia.dot(x41) + ib.dot(v41);
            c = ib.dot(x41) + ic.dot(v41);
            d = ic.dot(x41);

            bool exist = solvePolynomialEquation(a, b, c, d, root);

            if (exist)
			{
				nCntTri++;
                Vec3d c1, c2, c3, c4, E1, E2, D;
                double det, u, v, y1, y2;

                c1 = px1 + root * v1;
                c2 = px2 + root * v2;
                c3 = px3 + root * v3;
                c4 = px4 + root * v4;

                E1 = c2 - c1;
                E2 = c3 - c1;

                a = E1.dot(E1);
                b = E1.dot(E2);
                c = b;
                d = E2.dot(E2);

                D = c4 - c1;
                y1 = (D).dot(E1);
                y2 = (D).dot(E2);

                det = a*d - b*c;
                if (fabs(det) > (0.000001*0.00000001))   ////< XZH too small ,careful   *0.00000001
                {
                    det = 1.0 / det;

                    u = det * (d * y1 - b * y2);
                    v = det * (-c * y1 + a * y2);
                    if (u >= 0.0&&v >= 0.0&&u + v < 1.0)
                    {
                        uN = (E1.cross(E2)).normalized();
                        if ((px4 - c1).dot(uN) >= 0.0)
                        {
                            physVertice[tris.nodeIdx[0]].contact = true;
                            physVertice[tris.nodeIdx[1]].contact = true;
                            physVertice[tris.nodeIdx[2]].contact = true;

                            col = true;

                            continuousCollisionPair cntpair;
                            cntpair.type = 1; // point-triangle CCD
                            cntpair.toolID = 0;
                            cntpair.toolSegID = 0;
                            cntpair.toolSegPara = 0.0;

                            cntpair.triSurfIdx = i;
                            cntpair.u = u;
                            cntpair.v = v;
                            cntpair.colNorm = uN;
                            cntpair.colTime = root;
                            m_continuousColPair.push_back(cntpair); // if not use PbdPointTriangleContinuousConstraint

                            auto c = std::make_shared<PbdPointTriangleContinuousConstraint>();
                            c->initConstraint(dynaModel1, map1->getMapIdx(i2),   ////< XZH (i1),
                                dynaModel2, tris.nodeIdx[0], tris.nodeIdx[1], tris.nodeIdx[2],
                                0, 0.0, i, u, v, uN, root);
                            //c->initConstraint(dynaModel1, map1->getMapIdx(i1),
                            //    dynaModel2, map2->getMapIdx(tris.nodeIdx[0]), map2->getMapIdx(tris.nodeIdx[1]), map2->getMapIdx(tris.nodeIdx[2]),
                            //    0, 0.0, i, u, v, uN, root);
                            m_collisionConstraints.push_back(c);

                            continuousCollisionNode cntnode0;
                            continuousCollisionNode cntnode1;
                            continuousCollisionNode cntnode2;

                            std::vector<int>::iterator result = find(m_continuousColVertices.begin(), m_continuousColVertices.end(), tris.nodeIdx[0]);  // find whether 
                            if (result == m_continuousColVertices.end())
                            {
                                m_continuousColVertices.push_back(tris.nodeIdx[0]);  // cannot find in vector, so add
                                cntnode0.idx = tris.nodeIdx[0];
                                cntnode0.nbrCol++;
                                m_continuousColNode.push_back(cntnode0);
                            }
                            else
                            {
                                for (int i = 0; i < m_continuousColNode.size(); i++)
                                {
                                    if (tris.nodeIdx[0] == m_continuousColNode[i].idx)
                                    {
                                        m_continuousColNode[i].nbrCol++;
                                        break;  // go outside of for
                                    }
                                } // END for vertor
                            }

                            result = find(m_continuousColVertices.begin(), m_continuousColVertices.end(), tris.nodeIdx[1]);
                            if (result == m_continuousColVertices.end())
                            {
                                m_continuousColVertices.push_back(tris.nodeIdx[1]);
                                cntnode1.idx = tris.nodeIdx[1];
                                cntnode1.nbrCol++;
                                m_continuousColNode.push_back(cntnode1);
                            }
                            else
                            {
                                for (int i = 0; i < m_continuousColNode.size(); i++)
                                {
                                    if (tris.nodeIdx[1] == m_continuousColNode[i].idx)
                                    {
                                        m_continuousColNode[i].nbrCol++;
                                        break;  // go outside of for
                                    }
                                } // END for vertor
                            }

                            result = find(m_continuousColVertices.begin(), m_continuousColVertices.end(), tris.nodeIdx[2]);
                            if (result == m_continuousColVertices.end())
                            {
                                m_continuousColVertices.push_back(tris.nodeIdx[2]);
                                cntnode2.idx = tris.nodeIdx[2];
                                cntnode2.nbrCol++;
                                m_continuousColNode.push_back(cntnode2);
                            }
                            else
                            {
                                for (int i = 0; i < m_continuousColNode.size(); i++)
                                {
                                    if (tris.nodeIdx[2] == m_continuousColNode[i].idx)
                                    {
                                        m_continuousColNode[i].nbrCol++;
                                        break;  // go outside of for
                                    }
                                } // END for vertor
                            }
                        }
                        else
                        {
                            physVertice[tris.nodeIdx[0]].contact = false;
                            physVertice[tris.nodeIdx[1]].contact = false;
                            physVertice[tris.nodeIdx[2]].contact = false;
                        }
                    }
                }
            }
        } // end if
    } // END for point-triangle

	// adding 09/20/2019 adding extra collision among the surrounding of the tool tip and tissue, using 2 triangles for only pushing the tissue as ETI did
	for (int k = 0; k <2; k++) // 2 4
	{
		switch (k)
		{
		default:
		case 0:
			px1 = tool.pDrawPos + tool.pGloVec[2];
			px2 = tool.pDrawPos + tool.pGloVec[3];
			px3 = tool.pDrawPos + tool.pGloVec[4];

			x1 = tool.ccdPos + tool.gloVec[2];
			x2 = tool.ccdPos + tool.gloVec[3];
			x3 = tool.ccdPos + tool.gloVec[4];
			//printf("S2: %.5f %.5f %.5f, %.5f %.5f %.5f\n", x1.x(), x1.y(), x1.z(), x2.x(), x2.y(), x2.z());
			break;
		case 1:
			px1 = tool.pDrawPos + tool.pGloVec[2];
			px2 = tool.pDrawPos + tool.pGloVec[4];
			px3 = tool.pDrawPos + tool.pGloVec[5];

			x1 = tool.ccdPos + tool.gloVec[2];
			x2 = tool.ccdPos + tool.gloVec[4];
			x3 = tool.ccdPos + tool.gloVec[5];
			break;
		}

		  ////< XZH 
		for (int t=0;t<physVertice.size();t++)
		{
			if (!physVertice[t].onSurfSurrounding) continue;

			px4 = physVertice[t].pos;
			x4 = physVertice[t].p;

			v1 = x1 - px1;
			v2 = x2 - px2;
			v3 = x3 - px3;
			v4 = x4 - px4;

			x21 = px2 - px1;
			x31 = px3 - px1;
			x41 = px4 - px1;

			v21 = v2 - v1;
			v31 = v3 - v1;
			v41 = v4 - v1;

			ia.x() = v21.y()*v31.z() - v21.z()*v31.y();
			ib.x() = x21.y()*v31.z() + v21.y()*x31.z() - x21.z()*v31.y() - v21.z()*x31.y();
			ic.x() = x21.y()*x31.z() - x21.z()*x31.y();

			ia.y() = v21.z()*v31.x() - v21.x()*v31.z();
			ib.y() = x21.z()*v31.x() + v21.z()*x31.x() - x21.x()*v31.z() - v21.x()*x31.z();
			ic.y() = x21.z()*x31.x() - x21.x()*x31.z();

			ia.z() = v21.x()*v31.y() - v21.y()*v31.x();
			ib.z() = x21.x()*v31.y() + v21.x()*x31.y() - x21.y()*v31.x() - v21.y()*x31.x();
			ic.z() = x21.x()*x31.y() - x21.y()*x31.x();

			a = ia.dot(v41);
			b = ia.dot(x41) + ib.dot(v41);
			c = ib.dot(x41) + ic.dot(v41);
			d = ic.dot(x41);

			bool exist = solvePolynomialEquation(a, b, c, d, root);

			if (exist)
			{
				Vec3d c1, c2, c3, c4, E1, E2, D;
				double det, u, v, y1, y2;

				c1 = px1 + root * v1;
				c2 = px2 + root * v2;
				c3 = px3 + root * v3;
				c4 = px4 + root * v4;

				E1 = c2 - c1;
				E2 = c3 - c1;

				a = E1.dot(E1);
				b = E1.dot(E2);
				c = b;
				d = E2.dot(E2);

				D = c4 - c1;
				y1 = (D).dot(E1);
				y2 = (D).dot(E2);

				det = a * d - b * c;

				if (fabs(det) >( 0.000001*0.00000001) )
				{
					det = 1.0 / det;

					u = det * (d * y1 - b * y2);
					v = det * (-c * y1 + a * y2);

					if (u >= 0.0&&v >= 0.0 && ((u + v) < 1.0))
					{

						uN = (E1.cross(E2)).normalized();
						if ((px4 - c1).dot(uN) >= 0.0)
						{
							physVertice[t].contactXZH = true;

							col = true;

							continuousCollisionPairXZH cntpairXZH;

							cntpairXZH.type = 3;	// tool triangle- node collision
							cntpairXZH.toolID = 0;
							cntpairXZH.toolTriID = k;

							cntpairXZH.nodeIdx = t;

							cntpairXZH.u = u;
							cntpairXZH.v = v;

							cntpairXZH.colNorm = uN;
							cntpairXZH.colTime = root;

							m_continuousColPairXZH.push_back(cntpairXZH);
						}
						else
						{
							physVertice[t].contactXZH = false;
						}
					}
				} // END fabs
			} // END exist
		}
	}
	  ////< XZH END for extra CCD

    // if no collision detection should update finalPos of tool
    if (m_collisionConstraints.empty())
    {
        tool.finalPos = tool.drawPos;
        isCCD = false;
        bOpenPBD = true;
    }
    else
    {
        isCCD = true;
        //bHasCD = true;
        //resolveContinuousCollisionVESS(); // solve CCD
        bOpenPBD = false;
    }

	//printf("broad col, %d %d, col size: %d %d\n", nCntEdge, nCntTri, m_continuousColPair.size(), m_continuousColPairXZH.size());
	// hanlding, for the extra first
	double ratio;
	//algorithm step3  
	// compute impulse based on the collision depth
	double fS, fT, magImpsNorm, magImpsTang, colWeight;
	Vec3d toolColPt, objColPt, impsVec, impsTang, impsNorm;
	if (m_continuousColPairXZH.size()>0)
	{
		////< XZH 09/25/2019 handling the surrounding tool tip CD with the tissue
        ratio = 0.03; // 0.15; // 1.0; // 1.0 * 5; // 0.03; // 0.08; // 0.15;
		for (int n = 0; n < m_continuousColPairXZH.size(); n++)
		{
			int m = m_continuousColPairXZH[n].toolTriID;
			auto cntcolPair = m_continuousColPairXZH[n];
			double bary[3];
			switch (m)
			{
			default:
			case 0:
				x1 = tool.ccdPos + tool.gloVec[2];
				x2 = tool.ccdPos + tool.gloVec[3];
				x3 = tool.ccdPos + tool.gloVec[4];
				break;
			case 1:
				x1 = tool.ccdPos + tool.gloVec[2];
				x2 = tool.ccdPos + tool.gloVec[4];
				x3 = tool.ccdPos + tool.gloVec[5];
				break;
			}

			bary[0] = 1.0 - cntcolPair.u - cntcolPair.v;
			bary[1] = cntcolPair.u;
			bary[2] = cntcolPair.v;

			toolColPt = bary[0] * x1 + bary[1] * x2 + bary[2] * x3;

			objColPt = physVertice[cntcolPair.nodeIdx].p;

			impsVec = toolColPt - objColPt;
			Vec3d normTmp = (x2 - x1).cross(x3 - x1).normalized();
			physVertice[cntcolPair.nodeIdx].p = physVertice[cntcolPair.nodeIdx].p + ratio * impsVec.norm()*normTmp; // new
		}
	}

    // when it ture
    //if (!isCCD) // correct
    if (isCCD)  // uncorrect (isCCD) test  isCCD should run, otherwise ,no   this part is the major reason making the compiler this file very slow 6-8 minutes
    {
        ratio = 0.10; // 0.15; // 1.0; // 1.0 * 5; // 0.07;   ////< XZH  0.1;
        // currentState=2   continuousCollisionHandlingWithSurfaceMesh
        for (int n = 0; n < m_continuousColPair.size(); n++)
		{
            auto cntcolPair = m_continuousColPair[n];
            // line-line  edge-edge
            if (cntcolPair.type == 0)
            {
                x1 = tool.ccdPos + tool.gloVec[0];
                x2 = tool.ccdPos + tool.gloVec[1];

                fS = cntcolPair.toolSegPara;
                toolColPt = (1.0 - fS) * x1 + fS * x2;

                int i3 = physEdge[cntcolPair.edgeIdx].nodeId[0];
                int i4 = physEdge[cntcolPair.edgeIdx].nodeId[1];
                x3 = physVertice[i3].p;
                x4 = physVertice[i4].p;
                Vec3d& pbdx3 = state2->getVertexPosition(i3);
                Vec3d& pbdx4 = state2->getVertexPosition(i4);
                const auto im3 = dynaModel2->getInvMass(i3);
                const auto im4 = dynaModel2->getInvMass(i4);

                fT = cntcolPair.edgePara;
                objColPt = (1.0 - fT) * x3 + fT * x4;

                int nbrCol3, nbrCol4;
                nbrCol3 = nbrCol4 = 1;
                for (int i = 0; i < m_continuousColNode.size(); i++)
                {
                    if (i3 == m_continuousColNode[i].idx)
                    {
                        nbrCol3 = m_continuousColNode[i].nbrCol;  // may be can use continue , entry next loop
                    }
                    if (i4 == m_continuousColNode[i].idx)
                    {
                        nbrCol4 = m_continuousColNode[i].nbrCol;
                    }
                } // END for vertor

                impsVec = toolColPt - objColPt;
                colWeight = 1.0 / double(nbrCol3); // should computer XZH smFloat(simObj->ctnColNode[simObj->ctnColPair[n].colLineNodeID[0]].nbrCol);
                if (im3 > 0)
                {
                    pbdx3 += colWeight * ratio  *(1.0 - fT) * impsVec;
                    physVertice[i3].p = pbdx3; //  += colWeight * ratio  *(1.0 - fT) * impsVec;
                }

                colWeight = 1.0 / double(nbrCol4); // XZH smFloat(simObj->ctnColNode[simObj->ctnColPair[n].colLineNodeID[1]].nbrCol);
                if (im4 > 0)
                {
                    pbdx4 += colWeight * ratio  *fT * impsVec;
                    physVertice[i4].p = pbdx4; //  += colWeight * ratio  *(1.0 - fT) * impsVec;
                }
            } // END if line-line

            // point-triangle
            double bary[3];
            if (cntcolPair.type == 1)
            {
                toolColPt = tool.ccdPos + tool.gloVec[1];  // [0] is Root, [1] is Tip, should use Tip

                int i1 = physSurfTris[cntcolPair.triSurfIdx].nodeIdx[0];
                int i2 = physSurfTris[cntcolPair.triSurfIdx].nodeIdx[1];
                int i3 = physSurfTris[cntcolPair.triSurfIdx].nodeIdx[2];
                x1 = physVertice[i1].p;
                x2 = physVertice[i2].p;
                x3 = physVertice[i3].p;

                Vec3d& pbdx1 = state2->getVertexPosition(i1);
                Vec3d& pbdx2 = state2->getVertexPosition(i2);
                Vec3d& pbdx3 = state2->getVertexPosition(i3);
                const auto im1 = dynaModel2->getInvMass(i1);
                const auto im2 = dynaModel2->getInvMass(i2);
                const auto im3 = dynaModel2->getInvMass(i3);

                bary[0] = 1.0 - cntcolPair.u - cntcolPair.v;
                bary[1] = cntcolPair.u;
                bary[2] = cntcolPair.v;

                objColPt = bary[0] * x1 + bary[1] * x2 + bary[2] * x3;
                impsVec = toolColPt - objColPt;

                int nbrCol1, nbrCol2, nbrCol3;
                nbrCol1 = nbrCol2 = nbrCol3 = 1;
                for (int i = 0; i < m_continuousColNode.size(); i++)
                {
                    if (i1 == m_continuousColNode[i].idx)
                    {
                        nbrCol1 = m_continuousColNode[i].nbrCol;
                    }
                    if (i2 == m_continuousColNode[i].idx)
                    {
                        nbrCol2 = m_continuousColNode[i].nbrCol;
                    }
                    if (i3 == m_continuousColNode[i].idx)
                    {
                        nbrCol3 = m_continuousColNode[i].nbrCol;
                    }
                } // END for vertor

                colWeight = 1.0 / double(nbrCol1);  // XZH ,should calculate smFloat(simObj->ctnColNode[simObj->ctnColPair[n].colTriNodeID[k]].nbrCol);
                if (im1 > 0)
                {
                    pbdx1 += ratio*colWeight * bary[0] * impsVec;
                    physVertice[i1].p = pbdx1; // += ratio*colWeight * bary[0] * impsVec;
                }

                colWeight = 1.0 / double(nbrCol2);  // XZH ,should calculate smFloat(simObj->ctnColNode[simObj->ctnColPair[n].colTriNodeID[k]].nbrCol);
                if (im2 > 0)
                {
                    pbdx2 += ratio*colWeight * bary[1] * impsVec;
                    physVertice[i2].p = pbdx2; //  += ratio*colWeight * bary[1] * impsVec;
                }

                colWeight = 1.0 / double(nbrCol3);  // XZH ,should calculate smFloat(simObj->ctnColNode[simObj->ctnColPair[n].colTriNodeID[k]].nbrCol);
                if (im3 > 0)
                {
                    pbdx3 += ratio*colWeight * bary[2] * impsVec;
                    physVertice[i3].p = pbdx3; //  += ratio*colWeight * bary[2] * impsVec;
                }
            } // END if point-triangle
        } // END for CCD colPair
        //return; // test the compile bug 07/28/2017

		

        // algorithm step4: local correction     virtual coulpling
        // pre collision group center
        //-------------------------------------------------------------------------------------------//
        // mass center of collision nodes at t=t0
        // previous and current mass center of collided nodes
        Vec3d preColCen, curColCen;

        preColCen = Vec3d::Zero();
        for (int n = 0; n < m_continuousColVertices.size(); n++)
        {
            int idx = m_continuousColVertices[n];
            preColCen = preColCen + physVertice[idx].pos;
        }
        preColCen = 1.0 / double(m_continuousColVertices.size()) * preColCen;

        // ~#@@@ lack of nbrCtnColNode  I think it is not necessary~~

        // position difference of tool[toolID].pDrawPos, wrt previos mass center, in local coordinates
        Vec3d locColCenVec;
        Vec3d dV;
        // position difference of tool[toolID].pDrawPos, wrt previos mass center, in world coordinates
        dV = preColCen - tool.pDrawPos;
        //locColCenVec.x = tool.pRotArr[0] * dV.x + tool.pRotArr[1] * dV.y + tool.pRotArr[2] * dV.z;
        //locColCenVec.y = tool.pRotArr[3] * dV.x + tool.pRotArr[4] * dV.y + tool.pRotArr[5] * dV.z;
        //locColCenVec.z = tool.pRotArr[6] * dV.x + tool.pRotArr[7] * dV.y + tool.pRotArr[8] * dV.z;
        locColCenVec = tool.pRotArr.inverse() *dV; //  tool.pRotArr*dV; should use inverse matrix to go back tool's coordinate

        //current mass center of collided nodes
        curColCen = Vec3d::Zero();
        for (int n = 0; n < m_continuousColVertices.size(); n++)
        {
            int idx = m_continuousColVertices[n];
            curColCen = curColCen + physVertice[idx].p;
        }
        curColCen = 1.0 / double(m_continuousColVertices.size()) * curColCen;
        // tool[toolID] position correction, local to world, assume that relative position of tool[toolID] and collided nodes center is unchanged
        //dV.x = tool.rotArr[0] * locColCenVec.x + tool.rotArr[3] * locColCenVec.y + tool.rotArr[6] * locColCenVec.z;
        //dV.y = tool.rotArr[1] * locColCenVec.x + tool.rotArr[4] * locColCenVec.y + tool.rotArr[7] * locColCenVec.z;
        //dV.z = tool.rotArr[2] * locColCenVec.x + tool.rotArr[5] * locColCenVec.y + tool.rotArr[8] * locColCenVec.z;
        dV = tool.rotArr*locColCenVec;

        tool.drawPos = curColCen - dV;

        // proximityTestForCloseContactWithSurfaceMesh(ETIObject *simObj, smInt toolID, smBool isRigid)
        int n, nbrIter;
        Vec3d c1, c2, norm; // dV,  x1, x2, x3, x4, 
        double dist, depth, scale, ds, thick, u, v; // s, t, 

        n = 0;
        nbrIter = 10;
        scale = 0.0;
        ds = 1.0 / double(nbrIter);
        double pullCloser = 0.02;  // DOUBLE double is this a BUG!!!~~~!!! XZH  this should be doulbe?!!????

        while (n < nbrIter)
        {
            scale = ds * double(n + 1);
            //if (nbrIter >= 5)
            //{
            //    tool.drawPos = tool.drawPos + pullCloser * (tool.jointPos - tool.drawPos).normalized();
            //    tool.drawPos.z() = tool.drawPos.z() + pullCloser * (tool.jointPos - tool.drawPos).z();  /// why should use this? not the first line?!!?
            //}

            // line--line
            x1 = tool.drawPos + tool.gloVec[0];
            x2 = tool.drawPos + tool.gloVec[1];
            for (int i = 0; i < physEdge.size(); i++)
            {
                auto& eg = physEdge[i];
                if (eg.closeTool&&eg.colDetOn)
                {
                    x3 = physVertice[eg.nodeId[0]].p;
                    x4 = physVertice[eg.nodeId[1]].p;

                    simpleDistTestBetweenLineSegments(x1, x2, x3, x4, s, t);
                    if (s<0.0) s = 0.0; if (s>1.0) s = 1.0;
                    if (t<0.0) t = 0.0; if (t>1.0) t = 1.0;

                    c1 = x1 + s * (x2 - x1);
                    c2 = x3 + t * (x4 - x3);

                    dist = (c1 - c2).norm();
                    depth = dist - prox1 - prox2; // -prox2??!!~~!!! replace tool[toolID].thickness[k][0][0];
                    if (depth < 0.0)
                    {
                        physVertice[eg.nodeId[0]].contact = true;
                        physVertice[eg.nodeId[1]].contact = true;

                        dV = (c1 - c2).normalized();
                        tool.drawPos = tool.drawPos - (depth * scale) * dV;
                    }
                    else
                    {
                        physVertice[eg.nodeId[0]].contact = false;
                        physVertice[eg.nodeId[1]].contact = false;
                    }
                }
            }

            // point -triangle
            for (int i = 0; i < physSurfTris.size(); i++)
            {
                auto& tris = physSurfTris[i];
                if (tris.closeTool&&physEdge[tris.edgeIdx[0]].colDetOn&&physEdge[tris.edgeIdx[1]].colDetOn&&physEdge[tris.edgeIdx[2]].colDetOn)
                {
                    x1 = physVertice[tris.nodeIdx[0]].p;
                    x2 = physVertice[tris.nodeIdx[1]].p;
                    x3 = physVertice[tris.nodeIdx[2]].p;

                    x4 = tool.drawPos + tool.gloVec[1];  // [0] is ROOT,  [1] is TIP

                    if (distTestBetweenTriAndPoint(x1, x2, x3, x4, u, v, dist, norm))
                    {
                        if (fabs(dist) < prox1)   ////< XZH  (prox1 + prox2))  // replace cullingthickness  by prox1   tool[toolID].thickness[0][1][k]
                        {
                            physVertice[tris.nodeIdx[0]].contact = true;
                            physVertice[tris.nodeIdx[1]].contact = true;
                            physVertice[tris.nodeIdx[2]].contact = true;

                            if (dist < 0.0)
                            {
                                depth = -prox1 - dist;   ////< XZH  +dist; 03/05/2018 changed // Hong Li dist;  // tool[toolID].thickness[0][1][k]  prox1
                                tool.drawPos = tool.drawPos - (depth * scale) * norm;  // " -" In Hong Li's method its  - , but I think it should be + 06/05/2017
                            }
                            else
                            {
                                depth = prox1 - dist; // prox1 tool[toolID].thickness[0][1][k]
                                tool.drawPos = tool.drawPos + (depth * scale) * norm;
                            }
                        }
                        else
                        {
                            physVertice[tris.nodeIdx[0]].contact = false;
                            physVertice[tris.nodeIdx[1]].contact = false;
                            physVertice[tris.nodeIdx[2]].contact = false;
                        }
                    }
                }
            }
            n++;
        } // END while handling CCD
    } // END if isCCD

    // above is continuous collision detection  below is static CD
    if (isCCD)  // should update .pos from .p  p is temp pos  
    {
        //bHasCD = true;
        tool.finalPos = tool.drawPos; //
        for (int i = 0; i < physVertice.size(); i++)
        {
            physVertice[i].pos = physVertice[i].p;
        }
        // HashTable 
        hashtableVertices->clear();
        hashtableVertices->insertPoints(physVertice);
        mesh1Visual->setRotation(tool.rotArr);
        mesh1Visual->setTranslation(tool.finalPos);
		stiffDis = STIFFDIST1;
		stiffVolume = STIFFVOL1;
        return;
    }

    // collision detection state =4 static CD
    col = false;
	bool isStaticCol = false;
    int n = 0;
    int nbrIter = 5;
    double scale = 0.0;
    double ds = 1.0 / double(nbrIter);
    ratio = 0.5; // 1.0; // 20190311  1.0  1;
    Vec3d norm, c1, c2, dV;
    c1 = c2 = dV = norm = Vec3d::Zero();
    double u, v, dist, depth;
    const auto im00 = dynaModel1->getInvMass(0);
    const auto im11 = dynaModel1->getInvMass(1);
	const auto im22 = dynaModel1->getInvMass(2);
	const auto im33 = dynaModel1->getInvMass(3);
	const auto im44 = dynaModel1->getInvMass(4);
	const auto im55 = dynaModel1->getInvMass(5);
    double im0, im1, im2, im3; // edge-edge 0-1  2-3  point-triangle 0  1-2-3

    //nbrIter = 0; // test  close static CD
    while (n< nbrIter)  // (n < nbrIter)
    {
        scale = ds * double(n + 1);

        x1 = tool.drawPos + tool.gloVec[0]; //  first point of tool  use current not previous, just for static
        x2 = tool.drawPos + tool.gloVec[1]; // second  

        // ----------------- line-line proximity test ----------------- //
        for (int i = 0; i < physEdge.size(); i++)
        {
            im0 = im00;
            im1 = im11;
            auto& eg = physEdge[i];
            if (eg.closeTool&&eg.colDetOn)
            {
                x3 = physVertice[eg.nodeId[0]].p;
                x4 = physVertice[eg.nodeId[1]].p;

                Vec3d& pbdx3 = state2->getVertexPosition(eg.nodeId[0]);
                Vec3d& pbdx4 = state2->getVertexPosition(eg.nodeId[1]); // reference in PbdModel
                //printf("Edge-Edge static: x3: %f %f %f pdbx3: %f %f %f x4: %f %f %f pbdx4: %f %f %f\n",
                //    x3.x(), x3.y(), x3.z(), pbdx3.x(), pbdx3.y(), pbdx3.z(), x4.x(), x4.y(), x4.z(), pbdx4.x(), pbdx4.y(), pbdx4.z()
                //    );

                const auto im2 = dynaModel2->getInvMass(eg.nodeId[0]);
                const auto im3 = dynaModel2->getInvMass(eg.nodeId[1]);

                simpleDistTestBetweenLineSegments(x1, x2, x3, x4, s, t);
                if (s < 0.0)
                    s = 0.0;
                if (s > 1.0)
                    s = 1.0;
                if (t < 0.0)
                    t = 0.0;
                if (t > 1.0)
                    t = 1.0;  // whether this is needed??! as iMSTK return directly

                c1 = x1 + s * (x2 - x1);  // P
                c2 = x3 + t * (x4 - x3); // Q

                dist = (c1 - c2).norm(); // (-n).norm()

                depth = dist - prox1; // dist-  tool.thickness[0][0][1];  // could use prox to replace thickness
                if (depth < 0.0)
                {
                    dV = (c1 - c2).normalized(); // (-n).normalized()
                    col = true;
                    physVertice[eg.nodeId[0]].contact = true;
                    physVertice[eg.nodeId[1]].contact = true;

                    Vec3d grad0 = (1 - s)*dV;
                    Vec3d grad1 = s*dV;
                    Vec3d grad2 = -(1 - t)*dV;
                    Vec3d grad3 = -(t)*dV;
                    auto lambda = im0*grad0.squaredNorm() +   // iMSTK method squaredNorm is equal to dot(grad0, grad0) 
                        im1*grad1.squaredNorm() +
                        im2*grad2.squaredNorm() +
                        im3*grad3.squaredNorm();
                    lambda = depth / lambda;

                    tool.drawPos = tool.drawPos - (0.75 * depth * scale) *ratio* dV;  //  0.25   dV is n might be changed in the future  using lambda~~@#@#&&

                    if (im2 > 0)  // >0 is not fixed point
                    {
                        pbdx3 += (0.25 *depth * scale)*ratio * dV;  // 0.75
                        physVertice[eg.nodeId[0]].p = pbdx3;
                    }
                    if (im3 > 0)
                    {
                        pbdx4 += (0.25 *depth * scale)*ratio * dV; // 0.75
                        physVertice[eg.nodeId[1]].p = pbdx4;
                    }
                    //physVertice[eg.nodeId[0]].p = physVertice[eg.nodeId[0]].p + (0.75 *depth * scale)*ratio * dV;
                    //physVertice[eg.nodeId[1]].p = physVertice[eg.nodeId[1]].p + (0.75 *depth * scale)*ratio * dV;
                }
                else
                {
					col = false;
                    physVertice[eg.nodeId[0]].contact = false;
                    physVertice[eg.nodeId[1]].contact = false;
                }

            } // END if eg
			isStaticCol = isStaticCol || col;
        } // END for  physEdge

        // ----------------- triangle-vertex proximity ----------------- //		
        for (int i = 0; i < physSurfTris.size(); i++)
        {
            im0 = im00;
            auto tris = physSurfTris[i]; //  mesh2->getPhysSurfTris()[i];
            if (tris.closeTool && physEdge[tris.edgeIdx[0]].colDetOn && physEdge[tris.edgeIdx[1]].colDetOn && physEdge[tris.edgeIdx[2]].colDetOn)
            {
                x1 = physVertice[tris.nodeIdx[0]].p;
                x2 = physVertice[tris.nodeIdx[1]].p;
                x3 = physVertice[tris.nodeIdx[2]].p;

                x4 = tool.drawPos + tool.gloVec[1]; // [1] is TIP,  [0] is ROOT, we should use tip ~~**&&&&&????? so maybe need two times for point-triangle?

                Vec3d& pbdx1 = state2->getVertexPosition(tris.nodeIdx[0]);
                Vec3d& pbdx2 = state2->getVertexPosition(tris.nodeIdx[1]); // reference in PbdModel
                Vec3d& pbdx3 = state2->getVertexPosition(tris.nodeIdx[2]); // reference in PbdModel

                const auto im1 = dynaModel2->getInvMass(tris.nodeIdx[0]);
                const auto im2 = dynaModel2->getInvMass(tris.nodeIdx[1]);
                const auto im3 = dynaModel2->getInvMass(tris.nodeIdx[2]);

                if (distTestBetweenTriAndPoint(x1, x2, x3, x4, u, v, dist, norm))//signed distance (triangle normal)
                {
                    if (fabs(dist) < prox1)  // should use prox1 for replacing tool[toolID].thickness[0][1][1])
                    {
                        col = true;

                        physVertice[tris.nodeIdx[0]].contact = true;
                        physVertice[tris.nodeIdx[1]].contact = true;
                        physVertice[tris.nodeIdx[2]].contact = true;

                        if (dist < 0.0)  // is in the inverse direction of normal should use prox1 +fabs(dist)
                        {
                            depth = -prox1 - dist;   ////< XZH  +dist; 03/05/2018 //  prox1 + fabs(dist); // Hong Li dist; // tool[toolID].thickness[0][1][1] + dist; 06/05/2017  I think Hong Li's method here is uncorrect,
                            tool.drawPos = tool.drawPos - (0.65 * depth * scale) *ratio* norm; //  0.35   is  "-" in  Hong Li's method, I think here is uncorrect, should use +

                            if (im1 > 0)
                            {
                                pbdx1 += (1.0 - u - v) * (0.35 * depth * scale) *ratio* norm;  // 0.65
                                physVertice[tris.nodeIdx[0]].p = pbdx1;
                            }
                            if (im2 > 0)
                            {
                                pbdx2 += u * (0.35 * depth * scale) *ratio* norm; // 0.65
                                physVertice[tris.nodeIdx[1]].p = pbdx2;
                            }
                            if (im3 > 0)
                            {
                                pbdx3 += v * (0.35 * depth * scale)*ratio * norm;  // 0.65
                                physVertice[tris.nodeIdx[2]].p = pbdx3;
                            }

                            //physVertice[tris.nodeIdx[0]].p = physVertice[tris.nodeIdx[0]].p + (1.0 - u - v) * (0.75 * depth * scale) * norm;
                            //physVertice[tris.nodeIdx[1]].p = physVertice[tris.nodeIdx[1]].p + u * (0.75 * depth * scale) * norm;
                            //physVertice[tris.nodeIdx[2]].p = physVertice[tris.nodeIdx[2]].p + v * (0.75 * depth * scale) * norm;
                        }
                        else
                        {
                            depth = prox1 - dist; // tool[toolID].thickness[0][1][1] - dist; // 0 for lary blade tip
                            tool.drawPos = tool.drawPos + (0.65 * depth * scale) *ratio* norm;  //  0.35

                            if (im1 > 0)
                            {
                                pbdx1 += -(1.0 - u - v) * (0.35 * depth * scale) *ratio* norm;  //  0.65
                                physVertice[tris.nodeIdx[0]].p = pbdx1;
                            }
                            if (im2 > 0)
                            {
                                pbdx2 += -u * (0.35 * depth * scale) *ratio* norm; //  0.65
                                physVertice[tris.nodeIdx[1]].p = pbdx2;
                            }
                            if (im3 > 0)
                            {
                                pbdx3 += -v * (0.35 * depth * scale) *ratio* norm; //  0.65
                                physVertice[tris.nodeIdx[2]].p = pbdx3;
                            }

                            //physVertice[tris.nodeIdx[0]].p = physVertice[tris.nodeIdx[0]].p - (1.0 - u - v) * (0.75 * depth * scale) * norm;
                            //physVertice[tris.nodeIdx[1]].p = physVertice[tris.nodeIdx[1]].p - u * (0.75 * depth * scale) * norm;
                            //physVertice[tris.nodeIdx[2]].p = physVertice[tris.nodeIdx[2]].p - v * (0.75 * depth * scale) * norm;
                        }

                    } // END if fabs dist
                    else
                    {
						col = false;
                        physVertice[tris.nodeIdx[0]].contact = false;
                        physVertice[tris.nodeIdx[1]].contact = false;
                        physVertice[tris.nodeIdx[2]].contact = false;
                    } // END else fabs dist
                } // END if distTest

            } // END if
			isStaticCol = isStaticCol || col;
        } // END for physSurfTris

		// ----------------- vertex-triangle proximity for extra collision among the surrounding of the tool tip 09302019----------------- //	
		for (int k=0;k<2;k++)
		{
			switch (k)
			{
			default:
			case 0:
				px1 = tool.pDrawPos + tool.pGloVec[2];
				px2 = tool.pDrawPos + tool.pGloVec[3];
				px3 = tool.pDrawPos + tool.pGloVec[4];

				x1 = tool.ccdPos + tool.gloVec[2];
				x2 = tool.ccdPos + tool.gloVec[3];
				x3 = tool.ccdPos + tool.gloVec[4];
				//printf("S2: %.5f %.5f %.5f, %.5f %.5f %.5f\n", x1.x(), x1.y(), x1.z(), x2.x(), x2.y(), x2.z());
				break;
			case 1:
				px1 = tool.pDrawPos + tool.pGloVec[2];
				px2 = tool.pDrawPos + tool.pGloVec[4];
				px3 = tool.pDrawPos + tool.pGloVec[5];

				x1 = tool.ccdPos + tool.gloVec[2];
				x2 = tool.ccdPos + tool.gloVec[4];
				x3 = tool.ccdPos + tool.gloVec[5];
				break;
			}
		}
		x1 = tool.drawPos + tool.gloVec[2];
		x2 = tool.drawPos + tool.gloVec[4];
		x3 = tool.drawPos + tool.gloVec[5];
		double imm0 = im00; 
		double imm1 = im11;
		double imm2 = im22;
		double imm3 = im33;
		double imm4 = im44;
		double imm5 = im55;

		for (int t=0;t<physVertice.size();t++)
		{
			auto& vvt = physVertice[t];

			const auto imm = dynaModel2->getInvMass(vvt.globalIdx);
			x4 = vvt.p;
			Vec3d& pbdx = state2->getVertexPosition(vvt.globalIdx); // reference in PbdModel

			if (distTestBetweenTriAndPoint(x1, x2, x3, x4, u, v, dist, norm)) //signed distance (triangle normal)
			{
				if (fabs(dist) < prox1)  // should use prox1 for replacing tool[toolID].thickness[0][1][1])
				{
					col = true;

					if (dist<0.0)
					{
						depth = -prox1 - dist;
						if (imm > 0)
						{
							pbdx += v * (0.35 * depth * scale)*ratio * norm;  // 0.65
							physVertice[t].p = pbdx;
						}
					}
					else
					{
						depth = -prox1 - dist;
						pbdx += -v * (0.35 * depth * scale) *ratio* norm; //  0.65
						physVertice[t].p = pbdx;
					}
				}
				else
				{

				} // END if
			} // END if
		} // END for

        n++;
    } // END while
    // UPDATE pos as p
    //bHasCD = col;
    tool.finalPos = tool.drawPos; //
    for (int i = 0; i < physVertice.size(); i++)
    {
        physVertice[i].pos = physVertice[i].p;
    }
    // HashTable 
    hashtableVertices->clear();
    hashtableVertices->insertPoints(physVertice);
    mesh1Visual->setRotation(tool.rotArr);
    mesh1Visual->setTranslation(tool.finalPos);
	if (isStaticCol)
	{
		stiffDis = STIFFDIST1;
		stiffVolume = STIFFVOL1;
	}
	else
	{
		stiffDis = STIFFDIST0;
		stiffVolume = STIFFVOL0;
	}
}

  ////< XZH including dissected surface
void
PbdInteractionPair::doNarrowPhaseContinuousCollisionVESS4()
{

}

void
PbdInteractionPair::resolveContinuousCollisionVESS()
{
    if (!m_collisionConstraints.empty())
    {
        // algorithm step4: local correction    tool's position virtual coulpling
        // pre collision group center
        //-------------------------------------------------------------------------------------------//
        // mass center of collision nodes at t=t0
        // previous and current mass center of collided nodes
        auto toolObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(first);
        if (!toolObj) return;  // not pbdvcobj
        auto g1 = first->getCollidingGeometry();
        auto g2 = second->getCollidingGeometry();
        auto dynaModel1 = std::static_pointer_cast<PbdModel>(first->getDynamicalModel());
        auto dynaModel2 = std::static_pointer_cast<PbdModel>(second->getDynamicalModel());
        auto prox1 = dynaModel1->getProximity();
        auto prox2 = dynaModel2->getProximity();
        auto mesh1 = std::static_pointer_cast<LineMesh>(g1);
        auto mesh2 = std::static_pointer_cast<VESSTetrahedralMesh>(g2);
        if ((!mesh1) || (!mesh2)) return; // convertion failed

        auto& tool = toolObj->getControllerTool();


        // solve for one time in this part
        for (size_t k = 0; k < m_collisionConstraints.size(); ++k)
        {
            m_collisionConstraints[k]->solvePositionConstraint(tool);
        }
        

        auto& physVertice = mesh2->getPhysVertex();
        auto& physEdge = mesh2->getPhysEdge();
        auto& physSurfTris = mesh2->getPhysSurfTris();

        Vec3d preColCen, curColCen;

        preColCen = Vec3d::Zero();
        for (int n = 0; n < m_continuousColVertices.size(); n++)
        {
            int idx = m_continuousColVertices[n];
            preColCen = preColCen + physVertice[idx].pos;
        }
        preColCen = 1.0 / double(m_continuousColVertices.size()) * preColCen;

        // position difference of tool[toolID].pDrawPos, wrt previos mass center, in local coordinates
        Vec3d locColCenVec;
        Vec3d dV;
        // position difference of tool[toolID].pDrawPos, wrt previos mass center, in world coordinates
        dV = preColCen - tool.pDrawPos;
        //locColCenVec.x = tool.pRotArr[0] * dV.x + tool.pRotArr[1] * dV.y + tool.pRotArr[2] * dV.z;
        //locColCenVec.y = tool.pRotArr[3] * dV.x + tool.pRotArr[4] * dV.y + tool.pRotArr[5] * dV.z;
        //locColCenVec.z = tool.pRotArr[6] * dV.x + tool.pRotArr[7] * dV.y + tool.pRotArr[8] * dV.z;
        locColCenVec = tool.pRotArr*dV;

        //current mass center of collided nodes
        curColCen = Vec3d::Zero();
        for (int n = 0; n < m_continuousColVertices.size(); n++)
        {
            int idx = m_continuousColVertices[n];
            curColCen = curColCen + physVertice[idx].p;
        }
        curColCen = 1.0 / double(m_continuousColVertices.size()) * curColCen;
        // tool[toolID] position correction, local to world, assume that relative position of tool[toolID] and collided nodes center is unchanged
        //dV.x = tool.rotArr[0] * locColCenVec.x + tool.rotArr[3] * locColCenVec.y + tool.rotArr[6] * locColCenVec.z;
        //dV.y = tool.rotArr[1] * locColCenVec.x + tool.rotArr[4] * locColCenVec.y + tool.rotArr[7] * locColCenVec.z;
        //dV.z = tool.rotArr[2] * locColCenVec.x + tool.rotArr[5] * locColCenVec.y + tool.rotArr[8] * locColCenVec.z;
        dV = tool.rotArr*locColCenVec;

        tool.drawPos = curColCen - dV;

        // proximityTestForCloseContactWithSurfaceMesh(ETIObject *simObj, smInt toolID, smBool isRigid)
        int n, nbrIter;
        Vec3d x1, x2, x3, x4, c1, c2, norm; // dV, 
        double s, t, dist, depth, scale, ds, thick, u, v;

        n = 0;
        nbrIter = 10;
        scale = 0.0;
        ds = 1.0 / double(nbrIter);
        int pullCloser = 0.05;  // DOUBLE double is this a BUG!!!~~~!!! XZH  this should be doulbe?!!????

        while (n < nbrIter)
        {
            scale = ds * double(n + 1);
            if (nbrIter >= 5){
                tool.drawPos = tool.drawPos + pullCloser * (tool.jointPos - tool.drawPos).normalized();
                tool.drawPos.z() = tool.drawPos.z() + pullCloser * (tool.jointPos - tool.drawPos).z();  /// why should use this? not the first line?!!?
            }

            // line--line
            x1 = tool.drawPos + tool.gloVec[0];
            x2 = tool.drawPos + tool.gloVec[1];
            for (int i = 0; i < physEdge.size();i++)
            {
                auto& eg = physEdge[i];
                if (eg.closeTool&&eg.colDetOn)
                {
                    x3 = physVertice[eg.nodeId[0]].p;
                    x4 = physVertice[eg.nodeId[1]].p;

                    simpleDistTestBetweenLineSegments(x1, x2, x3, x4, s, t);
                    if (s<0.0) s = 0.0; if (s>1.0) s = 1.0;
                    if (t<0.0) t = 0.0; if (t>1.0) t = 1.0;

                    c1 = x1 + s * (x2 - x1);
                    c2 = x3 + t * (x4 - x3);

                    dist = (c1 - c2).norm();
                    depth = dist - prox1; // -prox2??!!~~!!! replace tool[toolID].thickness[k][0][0];
                    if (depth < 0.0)
                    {
                        physVertice[eg.nodeId[0]].contact = true;
                        physVertice[eg.nodeId[1]].contact = true;

                        dV = (c1 - c2).normalized();
                        tool.drawPos = tool.drawPos - (depth * scale) * dV;
                    }
                    else
                    {
                        physVertice[eg.nodeId[0]].contact = false;
                        physVertice[eg.nodeId[1]].contact = false;
                    }
                }
            }

            // point -triangle
            for (int i = 0; i < physSurfTris.size();i++)
            {
                auto& tris = physSurfTris[i];
                if (tris.closeTool&&physEdge[tris.edgeIdx[0]].colDetOn&&physEdge[tris.edgeIdx[1]].colDetOn&&physEdge[tris.edgeIdx[2]].colDetOn)
                {
                    x1 = physVertice[tris.nodeIdx[0]].p;
                    x2 = physVertice[tris.nodeIdx[1]].p;
                    x3 = physVertice[tris.nodeIdx[2]].p;

                    x4 = tool.drawPos + tool.gloVec[0];

                    if (distTestBetweenTriAndPoint(x1, x2, x3, x4, u, v, dist, norm))
                    {
                        if (fabs(dist)<prox1)  // replace cullingthickness  by prox1   tool[toolID].thickness[0][1][k]
                        {
                            physVertice[tris.nodeIdx[0]].contact = true;
                            physVertice[tris.nodeIdx[1]].contact = true;
                            physVertice[tris.nodeIdx[2]].contact = true;

                            if (dist<0.0)
                            {
                                depth = prox1 + dist;  // tool[toolID].thickness[0][1][k]  prox1
                                tool.drawPos = tool.drawPos - (depth * scale) * norm;
                            }
                            else
                            {
                                depth = prox1 - dist; // prox1 tool[toolID].thickness[0][1][k]
                                tool.drawPos = tool.drawPos + (depth * scale) * norm;
                            }
                        }
                        else
                        {
                            physVertice[tris.nodeIdx[0]].contact = false;
                            physVertice[tris.nodeIdx[1]].contact = false;
                            physVertice[tris.nodeIdx[2]].contact = false;
                        }
                    }
                }
            }
            n++;
        }
        tool.finalPos = tool.drawPos; //
    }
}

void
PbdInteractionPair::computeContactForceContinuous()
{
    if (mProcType == 0)
    {
        auto toolObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(first);
        auto& tool = toolObj->getControllerTool();
        first->setForceFeedbackParams(tool.finalPos, bHasCD, false);
    }
    else if (mProcType == 1)
    {
        auto toolObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(first1);
        auto& tool = toolObj->getControllerTool();
        first1->setForceFeedbackParams(tool.finalPos, bHasCD, false);
    }
    else if (mProcType==2)
    {
        auto toolObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(first2);
        auto& tool = toolObj->getControllerTool();
        first2->setForceFeedbackParams(tool.finalPos, bHasCD, false);
    }
    else if (mProcType==3)
    {

    }
    //auto toolObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(first);
    //auto& tool = toolObj->getControllerTool();
    ////tool.diffPos = (tool.finalPos - tool.hapticPos);
    ////Vec3d ddiffPos = tool.diffPos - tool.diffPosPre;

    ////// set Parameter
    //first->setForceFeedbackParams(tool.finalPos, bHasCD, false);
    return;

    //Vec3d kx;
    //if (ddiffPos.norm()>2 * tool.diffPosPre.norm())
    ////    kx = 0.25*tool.springConstant *tool.diffPos;
    ////else if (ddiffPos.norm()>tool.diffPosPre.norm())
    //    kx = tool.springConstant *tool.diffPos;
    //else
    //    kx = tool.springConstant *tool.diffPos; // tool.springConstant *tool.diffPos;

    ////if (kx.norm()>MaxOutputForce)
    ////    kx = MaxOutputForce*kx.normalized();

    //tool.diffPosPre = tool.diffPos;

    //Vec3d velocity = tool.hapticPos - tool.pHapticPos;

    //Vec3d hapticResistance;
    //double HRcoeff = tool.hapticVelResistance;

    //double hapticResisFactor = velocity.norm() / 0.1; // 0.1
    //if (hapticResisFactor > 1)
    //    HRcoeff *= hapticResisFactor;


    ////hapticResistance.setValue(HRcoeff*velocity.x, HRcoeff*velocity.y, HRcoeff*velocity.z);
    //hapticResistance = Vec3d(HRcoeff*velocity.x(), HRcoeff*velocity.y(), HRcoeff*velocity.z());

    //double resistCoeff = tool.velocityResistance;
    //Vec3d resistance;
    ////resistance.setValue(resistCoeff * ddiffPos.x, resistCoeff * ddiffPos.y, resistCoeff * ddiffPos.z);
    //resistance = Vec3d(resistCoeff * ddiffPos.x(), resistCoeff * ddiffPos.y(), resistCoeff * ddiffPos.z());

    //if (resistance.norm() > 1.4*kx.norm())  // 1.5
    //{
    //    resistance = Vec3d::Zero(); // resistance.setValue(0, 0, 0);
    //}

    ////curForce = hapticResistance;
    ////curForce = kx - hapticResistance;
    ////curForce = kx;
    ////tool.force = kx - resistance;
    //tool.force = kx - resistance; //  -hapticResistance;

    //if (tool.force.norm()>MaxOutputForce)
    //    tool.force = MaxOutputForce*tool.force.normalized();


    //tool.numberOfForceInFilter++;
    //if (!bHasCD) // if no collision
    //{
    //    tool.force = Vec3d::Zero(); //  .setValue(0.0, 0.0, 0.0);
    //    for (int i = 0; i < tool.filterSize; i++)
    //    {
    //        tool.arrForce[i] = Vec3d::Zero();  // setValue(0.0, 0.0, 0.0);  //  set to 0
    //    }
    //    tool.numberOfForceInFilter = 0;
    //}

    //for (int i = tool.filterSize - 2; i >= 0; i--)
    //{
    //    tool.arrForce[i + 1] = tool.arrForce[i];
    //}
    //tool.arrForce[0] = tool.force;

    //double sizecoeff = 1.0;
    //if (tool.numberOfForceInFilter < (tool.filterSize / 4.0))
    //    sizecoeff = 1.0 / 4.0;
    //else if (tool.numberOfForceInFilter < (tool.filterSize / 3.0))
    //    sizecoeff = 1.0 / 3.0;
    //else if (tool.numberOfForceInFilter < (tool.filterSize / 2.0))
    //    sizecoeff = 1.0 / 2.0;

    //tool.force = Vec3d::Zero(); //  .setValue(0.0, 0.0, 0.0);
    //for (int i = 0; i<tool.filterSize; i++)
    //{
    //    tool.force = tool.force + tool.arrForce[i] * std::sqrtf(float(tool.filterSize - i) / (sizecoeff*tool.filterSize));
    //    //	tool.curForce = tool.curForce + tool.arrForce[i];
    //}

    //tool.force = (1.0 / double(tool.filterSize)*(1.5 / (1 + 1.5 / double(tool.filterSize)))) * tool.force;

    //if (tool.force.norm()>MaxOutputForce)
    //    tool.force = MaxOutputForce*tool.force.normalized();

    //tool.curForce = tool.force;

    //first->setForce(tool.forceCoeff*tool.curForce);
    //FILE *outForceFilter = fopen("G:/outforcefilter.dat", "a");
    //Vec3d temp = tool.forceCoeff*tool.curForce;
    ////if (temp.norm()>0.0001)
    ////{
    //fprintf(outForceFilter, "%f\n",temp.norm());
    //    //fprintf(outForceFilter, "%f %f %f %f\n", temp.x(), temp.y(), temp.z(), temp.norm());
    ////}
    //fclose(outForceFilter);
}

bool
PbdInteractionPair::continuousCollisionDetectionWithSurfaceMesh(std::shared_ptr<VESSTetrahedralMesh> tetMesh, ToolInfo& tool, double dt)
{
    

    return true;
}

bool
PbdInteractionPair::findCollisionCandidateInjection(std::shared_ptr<VESSTetrahedralMesh> tetMesh, ToolInfo& tool, double threshold)
{
    auto& physEdges = tetMesh->getPhysEdge();
    for (int i = 0; i < physEdges.size(); i++) // for (int i = 0; i < tetMesh->getPhysEdge().size(); i++)
    {
        physEdges[i].closeTool = false;  // tetMesh->getPhysEdge()[i].closeTool = false;
    }
    auto& physSurfTris = tetMesh->getPhysSurfTris();
    for (int i = 0; i < physSurfTris.size(); i++) // for (int i = 0; i < tetMesh->getPhysSurfTris().size(); i++)
    {
        physSurfTris[i].closeTool = false; //tetMesh->getPhysSurfTris()[i].closeTool = false;
    }
    int j, cn = -1;
    double length;
    double dist, minDist = 1000000000.0;
    Vec3d aveNodePos;
    Vec3d end0, end1;
    Vec3d line;
    double para;
    threshold = 0.006;
    std::vector<size_t> verticeList;

    auto g1 = first1->getCollidingGeometry();
    auto mesh1 = std::static_pointer_cast<LineMesh>(g1);
    Vec3d vert0 = mesh1->getVertexPosition(0);
    Vec3d vert1 = mesh1->getVertexPosition(1);
    auto m_model1 = std::static_pointer_cast<PbdModel>(first1->getDynamicalModel());
    auto state1 = m_model1->getPreviousState();
    Vec3d vert00 = state1->getVertexPosition(0);
    Vec3d vert11 = state1->getVertexPosition(1);
    double lencod = (vert1 - vert0).norm();

    end0 = tool.pDrawPos + tool.pGloVec[0]; // ROOT
    end1 = tool.pDrawPos + tool.pGloVec[1];  // Tip  touch colon
    length = (end1 - end0).norm();
    if (length < 0.0001f) return false; // not start 
    line = (end1 - end0).normalized();

    auto& physVertices = tetMesh->getPhysVertex();
    for (int i = 0; i < physVertices.size(); i++)
    {
        auto &vertx = physVertices[i]; //  tetMesh->getPhysVertex()[i];
        auto &verPos = physVertices[i].pos; //  tetMesh->getPhysVertex()[i].pos;  // use pos or pPos or oPos??
        dist = (verPos - end1).norm();
        if (dist < minDist)
        {
            cn = physVertices[i].globalIdx; //  i;  // i is a local index in physVertices, should use global
            minDist = dist;
        }
        if (dist < threshold)
            vertx.closeTool = true;
        else
            vertx.closeTool = false;
        if (vertx.closeTool)
        {
            vertx.contact = true;
            verticeList.push_back(i);
        }
        else
        {
            vertx.contact = false;
        }
    }

    //// Method 3 using the distance between the vertex to the tip of tool to do broad phase
    ////threshold = 3.0; // add the near vertex to the candidates
    //for (int i = 0; i < physVertices.size(); i++) // for (int i = 0; i < tetMesh->getPhysVertex().size(); i++)
    //{
    //    auto &vertx = physVertices[i]; //  tetMesh->getPhysVertex()[i];
    //    auto &verPos = physVertices[i].pos; //  tetMesh->getPhysVertex()[i].pos;  // use pos or pPos or oPos??
    //    dist = (verPos - end1).norm();
    //    //printf("dist %d %f  lencod=%f, length=%f\n", vertx.globalIdx, dist, lencod, length);
    //    if (dist < minDist)
    //    {
    //        cn = physVertices[i].globalIdx; //  i;  // i is a local index in physVertices, should use global
    //        minDist = dist;
    //    }
    //    if (dist < threshold)
    //        vertx.closeTool = true;
    //    else
    //        vertx.closeTool = false;

    //    if (vertx.closeTool)
    //    {
    //        vertx.contact = true;
    //    }
    //    else
    //    {
    //        vertx.contact = false;
    //    }

    //    if (vertx.closeTool)
    //    {
    //        // neighbors surface triangle
    //        for (int k = 0; k < vertx.nbrNeiSurfTri; k++)
    //        {
    //            int mTmp = vertx.neiSurfTri[k]; // tri number
    //            if (physSurfTris[mTmp].colDetOn)
    //            {
    //                physSurfTris[mTmp].closeTool = true;
    //            }
    //        }
    //        // neighbors edge
    //        for (int k = 0; k < vertx.nbrNeiSurfEdge; k++)
    //        {
    //            int mTmp = vertx.neiSurfEdge[k];  // value is index in m_edgeCollision
    //            if (physEdges[mTmp].colDetOn)
    //            {
    //                physEdges[mTmp].closeTool = true;
    //            }
    //        }
    //    }
    //}
    //tetMesh->setClosestVertTool(cn);
    //if (minDist > threshold)
    //{
    //    //printf("find collision false\n");
    //    return false;
    //}
    //else
    //{
    //    //printf("find collision true\n");
    //    return true;
    //}
    //// Method 3 END
    //// Method 2 Hash Table 
    //auto& hashtableVertices = tetMesh->getHashTableVess();
    //Vec3d min1, max1;
    //auto g1visual = first1->getVisualGeometry();
    //auto mesh1Visual = std::static_pointer_cast<SurfaceMesh>(g1visual);
    //mesh1Visual->computeBoundingBox(min1, max1);
    //// replacing Tip of tool's AABB (point's AABB) to surface AABB, since the knife is too long
    //
    //double proxtool = threshold; //  0.8;
    //min1.x() = end1.x() - proxtool;
    //max1.x() = end1.x() + proxtool;
    //min1.y() = end1.y() - proxtool;
    //max1.y() = end1.y() + proxtool;
    //min1.z() = end1.z() - proxtool;
    //max1.z() = end1.z() + proxtool;

    //
    //verticeList = hashtableVertices->getPointsInAABB(min1, max1);
      ////< XZH Method 2 END

    if (verticeList.size() < 1)
    {
        //FILE *outForceFilter = fopen("G:/outInformation.dat", "a");
        //fprintf(outForceFilter, "broad phase CD false\n");
        //fclose(outForceFilter);
        return false;
    }
    // spatial hash table
    for (int i = 0; i < verticeList.size(); i++)
    {
        int num = verticeList[i]; // global index of vertices
        auto &vertx = physVertices[num];
        vertx.closeTool = true;
        vertx.contact = true;
        if (vertx.closeTool)
        {
            // neighbors surface triangle
            for (int k = 0; k < vertx.nbrNeiSurfTri; k++)
            {
                int mTmp = vertx.neiSurfTri[k]; // tri number
                if (physSurfTris[mTmp].colDetOn)
                {
                    physSurfTris[mTmp].closeTool = true;
                }
            }
            // neighbors edge
            for (int k = 0; k < vertx.nbrNeiSurfEdge; k++)
            {
                int mTmp = vertx.neiSurfEdge[k];  // value is index in m_edgeCollision
                if (physEdges[mTmp].colDetOn)
                {
                    physEdges[mTmp].closeTool = true;
                }
            }
        }
    }
    //FILE *outForceFilter = fopen("G:/outInformation.dat", "a");
    //fprintf(outForceFilter, "broad phase CD ture\n");
    //fclose(outForceFilter);
    return true;
    // Hash Table END
    //// Method 1 old way XZH 06/13/2017 // tranditional way  
    //for (int i = 0; i < physVertices.size(); i++) // for (int i = 0; i < tetMesh->getPhysVertex().size(); i++)
    //{
    //    auto &vertx = physVertices[i]; //  tetMesh->getPhysVertex()[i];
    //    auto &verPos = physVertices[i].pos; //  tetMesh->getPhysVertex()[i].pos;  // use pos or pPos or oPos??
    //    para = line.dot(verPos - end0) / length;  // 
    //    if (para < 0) para = 0;
    //    if (para > 1) para = 1;
    //    dist = (verPos - (end0 + para*(end1 - end0))).norm();
    //    //printf("dist %d %f  lencod=%f, length=%f\n", vertx.globalIdx, dist, lencod, length);
    //    if (dist < minDist)
    //    {
    //        cn = physVertices[i].globalIdx; //  i;  // i is a local index in physVertices, should use global
    //        minDist = dist;
    //    }
    //    if (dist < threshold)
    //        vertx.closeTool = true;
    //    else
    //        vertx.closeTool = false;

    //    if (vertx.closeTool)
    //    {
    //        vertx.contact = true;
    //    }
    //    else
    //    {
    //        vertx.contact = false;
    //    }

    //    if (vertx.closeTool)
    //    {
    //        // neighbors surface triangle
    //        for (int k = 0; k < vertx.nbrNeiSurfTri; k++)
    //        {
    //            int mTmp = vertx.neiSurfTri[k]; // tri number
    //            if (physSurfTris[mTmp].colDetOn)
    //            {
    //                physSurfTris[mTmp].closeTool = true;
    //            }
    //        }
    //        // neighbors edge
    //        for (int k = 0; k < vertx.nbrNeiSurfEdge; k++)
    //        {
    //            int mTmp = vertx.neiSurfEdge[k];  // value is index in m_edgeCollision
    //            if (physEdges[mTmp].colDetOn)
    //            {
    //                physEdges[mTmp].closeTool = true;
    //            }
    //        }
    //    }
    //}
    //tetMesh->setClosestVertTool(cn);
    //if (minDist > threshold)
    //{
    //    //printf("find collision false\n");
    //    return false;
    //}
    //else
    //{
    //    //printf("find collision true\n");
    //    return true;
    //}
    //// old way XZH 06/13/2017 END
}


// using vertex-point, traverse all the vertices in the triangles, and choose the tip of tool to check the distance
bool
PbdInteractionPair::findCollisionCandidate(std::shared_ptr<VESSTetrahedralMesh> tetMesh, ToolInfo& tool, double threshold)
{
    auto& physEdges = tetMesh->getPhysEdge();
    for (int i = 0; i < physEdges.size(); i++) // for (int i = 0; i < tetMesh->getPhysEdge().size(); i++)
    {
        physEdges[i].closeTool = false;  // tetMesh->getPhysEdge()[i].closeTool = false;
    }
    auto& physSurfTris = tetMesh->getPhysSurfTris();
    for (int i = 0; i < physSurfTris.size(); i++) // for (int i = 0; i < tetMesh->getPhysSurfTris().size(); i++)
    {
        physSurfTris[i].closeTool = false; //tetMesh->getPhysSurfTris()[i].closeTool = false;
    }
    int j, cn = -1;
    double length;
    double dist, minDist = 1000000000.0;
    Vec3d aveNodePos;
    Vec3d end0, end1;
    Vec3d line;
    double para;

    auto g1 = first->getCollidingGeometry();
    auto mesh1 = std::static_pointer_cast<LineMesh>(g1);
    Vec3d vert0 = mesh1->getVertexPosition(0);
    Vec3d vert1 = mesh1->getVertexPosition(1);
    auto m_model1 = std::static_pointer_cast<PbdModel>(first->getDynamicalModel());
    auto state1 = m_model1->getPreviousState();
    Vec3d vert00 = state1->getVertexPosition(0);
    Vec3d vert11 = state1->getVertexPosition(1);
    double lencod = (vert1 - vert0).norm();

    end0 = tool.pDrawPos + tool.pGloVec[0]; // ROOT
    end1 = tool.pDrawPos + tool.pGloVec[1];  // Tip  touch colon
    length = (end1 - end0).norm();
    if (length < 0.0001f) return false; // not start 
    line = (end1 - end0).normalized();

    auto& physVertices = tetMesh->getPhysVertex();
    //// Method 3 using the distance between the vertex to the tip of tool to do broad phase
    ////threshold = 3.0; // add the near vertex to the candidates
    //for (int i = 0; i < physVertices.size(); i++) // for (int i = 0; i < tetMesh->getPhysVertex().size(); i++)
    //{
    //    auto &vertx = physVertices[i]; //  tetMesh->getPhysVertex()[i];
    //    auto &verPos = physVertices[i].pos; //  tetMesh->getPhysVertex()[i].pos;  // use pos or pPos or oPos??
    //    dist = (verPos - end1).norm();
    //    //printf("dist %d %f  lencod=%f, length=%f\n", vertx.globalIdx, dist, lencod, length);
    //    if (dist < minDist)
    //    {
    //        cn = physVertices[i].globalIdx; //  i;  // i is a local index in physVertices, should use global
    //        minDist = dist;
    //    }
    //    if (dist < threshold)
    //        vertx.closeTool = true;
    //    else
    //        vertx.closeTool = false;

    //    if (vertx.closeTool)
    //    {
    //        vertx.contact = true;
    //    }
    //    else
    //    {
    //        vertx.contact = false;
    //    }

    //    if (vertx.closeTool)
    //    {
    //        // neighbors surface triangle
    //        for (int k = 0; k < vertx.nbrNeiSurfTri; k++)
    //        {
    //            int mTmp = vertx.neiSurfTri[k]; // tri number
    //            if (physSurfTris[mTmp].colDetOn)
    //            {
    //                physSurfTris[mTmp].closeTool = true;
    //            }
    //        }
    //        // neighbors edge
    //        for (int k = 0; k < vertx.nbrNeiSurfEdge; k++)
    //        {
    //            int mTmp = vertx.neiSurfEdge[k];  // value is index in m_edgeCollision
    //            if (physEdges[mTmp].colDetOn)
    //            {
    //                physEdges[mTmp].closeTool = true;
    //            }
    //        }
    //    }
    //}
    //tetMesh->setClosestVertTool(cn);
    //if (minDist > threshold)
    //{
    //    //printf("find collision false\n");
    //    return false;
    //}
    //else
    //{
    //    //printf("find collision true\n");
    //    return true;
    //}
    //// Method 3 END
    // Method 2 Hash Table 
    auto& hashtableVertices = tetMesh->getHashTableVess();
    Vec3d min1, max1;
    auto g1visual = first->getVisualGeometry();
    auto mesh1Visual = std::static_pointer_cast<SurfaceMesh>(g1visual);
    mesh1Visual->computeBoundingBox(min1, max1);
    // replacing Tip of tool's AABB (point's AABB) to surface AABB, since the knife is too long
    double proxtool = threshold; //  0.8;
    min1.x() = end1.x() - proxtool;
    max1.x() = end1.x() + proxtool;
    min1.y() = end1.y() - proxtool;
    max1.y() = end1.y() + proxtool;
    min1.z() = end1.z() - proxtool;
    max1.z() = end1.z() + proxtool;

    std::vector<size_t> verticeList;
    verticeList=hashtableVertices->getPointsInAABB(min1, max1);
    if (verticeList.size() < 1)
    {
        //FILE *outForceFilter = fopen("G:/outInformation.dat", "a");
        //fprintf(outForceFilter, "broad phase CD false\n");
        //fclose(outForceFilter);
        return false;
    }
    // spatial hash table
    for (int i = 0; i < verticeList.size(); i++)
    {
        int num = verticeList[i]; // global index of vertices
        auto &vertx = physVertices[num];
        vertx.closeTool = true;
        vertx.contact = true;
        if (vertx.closeTool)
        {
            // neighbors surface triangle
            for (int k = 0; k < vertx.nbrNeiSurfTri; k++)
            {
                int mTmp = vertx.neiSurfTri[k]; // tri number
                if (physSurfTris[mTmp].colDetOn)
                {
                    physSurfTris[mTmp].closeTool = true;
                }
            }
            // neighbors edge
            for (int k = 0; k < vertx.nbrNeiSurfEdge; k++)
            {
                int mTmp = vertx.neiSurfEdge[k];  // value is index in m_edgeCollision
                if (physEdges[mTmp].colDetOn)
                {
                    physEdges[mTmp].closeTool = true;
                }
            }
        }
    }
    //FILE *outForceFilter = fopen("G:/outInformation.dat", "a");
    //fprintf(outForceFilter, "broad phase CD ture\n");
    //fclose(outForceFilter);
    return true;
    // Hash Table END
    //// Method 1 old way XZH 06/13/2017 // tranditional way  
    //for (int i = 0; i < physVertices.size(); i++) // for (int i = 0; i < tetMesh->getPhysVertex().size(); i++)
    //{
    //    auto &vertx = physVertices[i]; //  tetMesh->getPhysVertex()[i];
    //    auto &verPos = physVertices[i].pos; //  tetMesh->getPhysVertex()[i].pos;  // use pos or pPos or oPos??
    //    para = line.dot(verPos - end0) / length;  // 
    //    if (para < 0) para = 0;
    //    if (para > 1) para = 1;
    //    dist = (verPos - (end0 + para*(end1 - end0))).norm();
    //    //printf("dist %d %f  lencod=%f, length=%f\n", vertx.globalIdx, dist, lencod, length);
    //    if (dist < minDist)
    //    {
    //        cn = physVertices[i].globalIdx; //  i;  // i is a local index in physVertices, should use global
    //        minDist = dist;
    //    }
    //    if (dist < threshold)
    //        vertx.closeTool = true;
    //    else
    //        vertx.closeTool = false;

    //    if (vertx.closeTool)
    //    {
    //        vertx.contact = true;
    //    }
    //    else
    //    {
    //        vertx.contact = false;
    //    }

    //    if (vertx.closeTool)
    //    {
    //        // neighbors surface triangle
    //        for (int k = 0; k < vertx.nbrNeiSurfTri; k++)
    //        {
    //            int mTmp = vertx.neiSurfTri[k]; // tri number
    //            if (physSurfTris[mTmp].colDetOn)
    //            {
    //                physSurfTris[mTmp].closeTool = true;
    //            }
    //        }
    //        // neighbors edge
    //        for (int k = 0; k < vertx.nbrNeiSurfEdge; k++)
    //        {
    //            int mTmp = vertx.neiSurfEdge[k];  // value is index in m_edgeCollision
    //            if (physEdges[mTmp].colDetOn)
    //            {
    //                physEdges[mTmp].closeTool = true;
    //            }
    //        }
    //    }
    //}
    //tetMesh->setClosestVertTool(cn);
    //if (minDist > threshold)
    //{
    //    //printf("find collision false\n");
    //    return false;
    //}
    //else
    //{
    //    //printf("find collision true\n");
    //    return true;
    //}
    //// old way XZH 06/13/2017 END
}

bool
PbdInteractionPair::findCollisionCandidate2(std::shared_ptr<VESSTetrahedralMesh> tetMesh, ToolInfo& tool, double threshold)
{
    auto& physEdges = tetMesh->getPhysEdge();
    for (int i = 0; i < physEdges.size(); i++) // for (int i = 0; i < tetMesh->getPhysEdge().size(); i++)
    {
        physEdges[i].closeTool = false;  // tetMesh->getPhysEdge()[i].closeTool = false;
    }
    auto& physSurfTris = tetMesh->getPhysSurfTris();
    for (int i = 0; i < physSurfTris.size(); i++) // for (int i = 0; i < tetMesh->getPhysSurfTris().size(); i++)
    {
        physSurfTris[i].closeTool = false; //tetMesh->getPhysSurfTris()[i].closeTool = false;
    }
    int j, cn = -1;
    double length;
    double dist, minDist = 1000000000.0;
    Vec3d aveNodePos;
    Vec3d end0, end1;
    Vec3d line;
    double para;

    auto g1 = first->getCollidingGeometry();
    auto mesh1 = std::static_pointer_cast<LineMesh>(g1);
    Vec3d vert0 = mesh1->getVertexPosition(0);
    Vec3d vert1 = mesh1->getVertexPosition(1);
    auto m_model1 = std::static_pointer_cast<PbdModel>(first->getDynamicalModel());
    auto state1 = m_model1->getPreviousState();
    auto state2 = m_model1->getCurrentState();
    Vec3d vert00 = state2->getVertexPosition(0);
    Vec3d vert11 = state2->getVertexPosition(1);
    double lencod = (vert1 - vert0).norm();

    end0 = tool.pDrawPos + tool.pGloVec[0]; // ROOT
    end1 = tool.pDrawPos + tool.pGloVec[1];  // Tip  touch colon
    length = (end1 - end0).norm();
    if (length < 0.0001f) return false; // not start 
    line = (end1 - end0).normalized();
    //printf("tool pos 0: %f %f %f, pos 1: %f %f %f     colliding pos 0: %f %f %f, pos 1: %f %f %f\n", end0.x(), end0.y(), end0.z(), end1.x(), end1.y(), end1.z(), vert00.x(), vert00.y(), vert00.z(), vert11.x(), vert11.y(), vert11.z());
    auto& physVertices = tetMesh->getPhysVertex();
    // Method 3 using the distance between the vertex to the tip of tool to do broad phase
    //threshold = 3.0; // add the near vertex to the candidates
	int nCnt = 0;
    for (int i = 0; i < physVertices.size(); i++) // for (int i = 0; i < tetMesh->getPhysVertex().size(); i++)
    {
        auto &vertx = physVertices[i]; //  tetMesh->getPhysVertex()[i];
        auto &verPos = physVertices[i].pos; //  tetMesh->getPhysVertex()[i].pos;  // use pos or pPos or oPos??
        dist = (verPos - end1).norm();
        //printf("dist %d %f  lencod=%f, length=%f\n", vertx.globalIdx, dist, lencod, length);
        if (dist < minDist)
        {
            cn = physVertices[i].globalIdx; //  i;  // i is a local index in physVertices, should use global
            minDist = dist;
        }
        if (dist < threshold)
            vertx.closeTool = true;
        else
            vertx.closeTool = false;

        if (vertx.closeTool)
        {
			vertx.contact = true; nCnt++;
        }
        else
        {
            vertx.contact = false;
        }

        if (vertx.closeTool)
        {
            // neighbors surface triangle
            for (int k = 0; k < vertx.nbrNeiSurfTri; k++)
            {
                int mTmp = vertx.neiSurfTri[k]; // tri number
                if (physSurfTris[mTmp].colDetOn)
                {
                    physSurfTris[mTmp].closeTool = true;
                }
            }
            // neighbors edge
            for (int k = 0; k < vertx.nbrNeiSurfEdge; k++)
            {
                int mTmp = vertx.neiSurfEdge[k];  // value is index in m_edgeCollision
                if (physEdges[mTmp].colDetOn)
                {
                    physEdges[mTmp].closeTool = true;
                }
            }
        }
    }
	//printf("close vert %d\n", nCnt);
    tetMesh->setClosestVertTool(cn);
    if (minDist > threshold)
    {
        //printf("find collision false\n");
        return false;
    }
    else
    {
        //printf("find collision true\n");
        return true;
    }
    // Method 3 END
    //// Method 2 Hash Table 
    //auto& hashtableVertices = tetMesh->getHashTableVess();
    //Vec3d min1, max1;
    //auto g1visual = first->getVisualGeometry();
    //auto mesh1Visual = std::static_pointer_cast<SurfaceMesh>(g1visual);
    //mesh1Visual->computeBoundingBox(min1, max1);
    //// replacing Tip of tool's AABB (point's AABB) to surface AABB, since the knife is too long
    //double proxtool = threshold; //  0.8;
    //min1.x() = end1.x() - proxtool;
    //max1.x() = end1.x() + proxtool;
    //min1.y() = end1.y() - proxtool;
    //max1.y() = end1.y() + proxtool;
    //min1.z() = end1.z() - proxtool;
    //max1.z() = end1.z() + proxtool;

    //std::vector<size_t> verticeList;
    //verticeList = hashtableVertices->getPointsInAABB(min1, max1);
    //if (verticeList.size() < 1)
    //{
    //    return false;
    //}
    //// spatial hash table
    //for (int i = 0; i < verticeList.size(); i++)
    //{
    //    int num = verticeList[i]; // global index of vertices
    //    auto &vertx = physVertices[num];
    //    vertx.closeTool = true;
    //    vertx.contact = true;
    //    if (vertx.closeTool)
    //    {
    //        // neighbors surface triangle
    //        for (int k = 0; k < vertx.nbrNeiSurfTri; k++)
    //        {
    //            int mTmp = vertx.neiSurfTri[k]; // tri number
    //            if (physSurfTris[mTmp].colDetOn)
    //            {
    //                physSurfTris[mTmp].closeTool = true;
    //            }
    //        }
    //        // neighbors edge
    //        for (int k = 0; k < vertx.nbrNeiSurfEdge; k++)
    //        {
    //            int mTmp = vertx.neiSurfEdge[k];  // value is index in m_edgeCollision
    //            if (physEdges[mTmp].colDetOn)
    //            {
    //                physEdges[mTmp].closeTool = true;
    //            }
    //        }
    //    }
    //}
    //return true;
    //  ////< XZH Method2 END
}

bool 
PbdInteractionPair::distTestBetweenTriAndPoint(Vec3d x1, Vec3d x2, Vec3d x3, Vec3d x4, double& u, double& v, double& signedDist, Vec3d& norm)
{
    Vec3d P, E1, E2, D;
    double a, b, c, d, det, y1, y2;

    E1 = x2 - x1;  // x12
    E2 = x3 - x1; // x13

    norm = (E1.cross(E2)).normalized();  // n.normalized()

    signedDist = (x4 - x1).dot(norm);  // x01.dot(n.normalized())

    P = x4 - (signedDist)* norm;		// projected point on the plane

    a = E1.dot(E1);
    b = E1.dot(E2);
    c = b;
    d = E2.dot(E2);

    D = P - x1;
    y1 = (D).dot(E1);
    y2 = (D).dot(E2);

    det = a*d - b*c;
    if (fabs(det) > (0.000001*0.00000001 ) )  // 0.000001
    {
        det = 1.0 / det;

        u = det * (d * y1 - b * y2);
        v = det * (-c * y1 + a * y2);

        if (u >= 0.0 && v >= 0.0 & u + v <= 1.0){  // the second & is && or &?~~##@@@ need to check XZH
            return true;
        }
    }
    return false;
}

void 
PbdInteractionPair::simpleDistTestBetweenLineSegments(Vec3d x1, Vec3d x2, Vec3d x3, Vec3d x4, double& s, double& t)
{
    Vec3d x21, x43, x31, c1, c2;
    double det, m11, m12, m22, b1, b2;

    x21 = x2 - x1;
    x43 = x4 - x3;
    x31 = x3 - x1;

    m11 = x21.dot(x21); // b
    m12 = x21.dot(x43) * (-1.0);  // -a?
    m22 = x43.dot(x43); // d
    b1 = x21.dot(x31);  // -c
    b2 = x43.dot(x31) * (-1.0);  // f  e=a

    det = 1.0 / (m11*m22 - m12*m12);  // b*d-(-a)*(-a)=b*d-a*a
    s = det * (m22*b1 - m12*b2);  // d*(-c)-(-a)*f =a*f-d*c
    t = det * (-m12*b1 + m11*b2); // a*(-c)+b*f=b*f-a*c  // comparsion with iMSTK

    /*
    if(s<0.0) s=0.0; if(s>1.0) s=1.0;
    if(t<0.0) t=0.0; if(t>1.0) t=1.0;

    c1 = x1 + s * x21;
    c2 = x3 + t * x43;


    return (c1-c2).module();
    */

}

bool
PbdInteractionPair::solvePolynomialEquation(double a, double b, double c, double d, double& root)
{
    double result[3], temp[3];
    double dist;
    int type, nbb, minIdx;
    double ESP = 0.1*0.00000001;
    bool exist = false;
    if (fabs(a) < ESP)
    {
        if (fabs(b) < ESP)
        {
            if (fabs(c) < ESP)
            { //0 order
                exist = false;
            }
            else
            { // 1st order  --c ����ESP��
                root = -d / c;
                if (root >= 0.0&&root <= 1.0)
                {
                    exist = true;
                }
            }
        }
        else
        {	// 2nd order  -- b����=ESP��
            double ts, r[2];
            ts = c*c - 4.0*b*d;
            if (ts < 0.0)
            {
                exist = false;
            }
            else if (ts == 0.0)
            {
                root = -c / (2.0*b);
                if (root >= 0.0&&root <= 1.0)
                {
                    exist = true;
                }
            }
            else
            {
                ts = sqrt(ts);
                r[0] = (-c + ts) / (2.0*b);
                r[1] = (-c - ts) / (2.0*b);
                double min = 1000000.0;
                for (int k = 0; k < 2; k++)
                {
                    if (r[k] >= 0.0&&r[k] <= 1.0)
                    {
                        exist = true;
                        if (r[k] < min) min = r[k];
                    }
                }
                if (exist) root = min;
            }
        }
    }
    else
    {	// 3rd order  -- a>=ESP

        type = solveCubic(a, b, c, d, result);

        exist = false;
        switch (type)
        {
        case 0:	//triple root
            if (result[0] >= 0.0 && result[0] <= 1.0)
            {
                root = result[0];
                exist = true;
            }
            break;
        case 1:	//three real root
            nbb = 0;
            for (int i = 0; i < 3; i++)
            {
                if (result[i] >= 0.0 && result[i] <= 1.0)
                {
                    temp[nbb] = result[i];
                    nbb++;
                }
            }
            if (nbb > 0)
            {

                double min = 100000000.0;
                for (int i = 0; i < nbb; i++)
                {
                    if (temp[i] < min)
                    {
                        min = temp[i];
                        minIdx = i;
                    }
                }
                root = temp[minIdx];
                exist = true;
            }
            break;
        case 2: //one real root
            if (result[0] >= 0.0 && result[0] <= 1.0)
            {
                root = result[0];
                exist = true;
            }
            break;
        case 3:
            exist = false;
            break;
        }
    }
    return exist;
}

int
PbdInteractionPair::solveCubic(double a, double b, double c, double d, double* result)
{
    double ROOTTHREE = 1.73205080756888;
    // find the discriminant
    double f, g, h;
    f = (3 * c / a - pow(b, 2) / pow(a, 2)) / 3;
    g = (2 * pow(b, 3) / pow(a, 3) - 9 * b * c / pow(a, 2) + 27 * d / a) / 27;
    h = pow(g, 2) / 4 + pow(f, 3) / 27;
    // evaluate discriminant
    if (f == 0 && g == 0 && h == 0)
    {
        // 3 equal roots
        double x;
        // when f, g, and h all equal 0 the roots can be found by the following line
        x = -cubeRoot(d / a);
        // print solutions
        result[0] = x;
        result[1] = x;
        result[2] = x;
        return 0;
    }
    else if (h <= 0)
    {
        // 3 real roots
        double q, i, j, k, l, m, n, p;
        // complicated maths making use of the method
        i = pow(pow(g, 2) / 4 - h, 0.5);
        j = cubeRoot(i);
        k = acos(-(g / (2 * i)));
        m = cos(k / 3);
        n = ROOTTHREE * sin(k / 3);
        p = -(b / (3 * a));
        // print solutions
        result[0] = 2 * j * m + p;
        result[1] = -j * (m + n) + p;
        result[2] = -j * (m - n) + p;
        return 1;
    }
    else if (h > 0)
    {
        // 1 real root and 2 complex roots
        double r, s, t, u, p;
        // complicated maths making use of the method
        r = -(g / 2) + pow(h, 0.5);
        s = cubeRoot(r);
        t = -(g / 2) - pow(h, 0.5);
        u = cubeRoot(t);
        p = -(b / (3 * a));
        // print solutions

        result[0] = (s + u) + p;
        result[1] = result[0];
        result[2] = result[0];
        return 2;
    }
    else
    {
        return 3;
    }
}

double 
PbdInteractionPair::cubeRoot(double x)
{
    double THIRD = 0.333333333333333;
    if (x < 0)
        return -pow(-x, THIRD);
    else
        return pow(x, THIRD);
}

void
PbdInteractionPair::updateToolPos()
{
    auto toolObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(first);
    if (!toolObj) printf("cannot convert first to Tool");

    auto& tool = toolObj->getControllerTool();
    tool.finalPos = tool.drawPos;

    auto g1visual = first->getVisualGeometry();
    auto mesh1Visual = std::static_pointer_cast<SurfaceMesh>(g1visual);
    //FILE *outForceFilter = fopen("G:/outInformation.dat", "a");
    //fprintf(outForceFilter, "no CD update visual\n");
    //fclose(outForceFilter);
    // update visual geometry for tool
    Vec3d vertexPos3;
    for (int i = 0; i < mesh1Visual->getNumVertices(); ++i)
    {
        vertexPos3 = mesh1Visual->getInitialVertexPosition(i);  // use the inital position * Omni transform   //->getVertexPosition(i);

        vertexPos3 = tool.rotArr*vertexPos3;
        vertexPos3 += tool.finalPos;

        mesh1Visual->setVertexPosition(i, vertexPos3);
    }
}

void
PbdInteractionPair::doNarrowPhaseCollisionInjection2()
{
	bool isTool = false;
	auto toolObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(first1);
	if (!toolObj) return;  // not pbdvcobj

	auto g1 = first1->getCollidingGeometry();
	auto g2 = second->getCollidingGeometry();
	auto g1visual = first1->getVisualGeometry();
	auto g2visual = second->getVisualGeometry();

	auto map1 = first1->getPhysicsToCollidingMap();
	auto map2 = second->getPhysicsToCollidingMap();

	auto dynaModel1 = std::static_pointer_cast<PbdModel>(first1->getDynamicalModel());
	auto dynaModel2 = std::static_pointer_cast<PbdModel>(second->getDynamicalModel());
	auto state1 = dynaModel1->getCurrentState();
	auto state2 = dynaModel2->getCurrentState();

	auto prox1 = dynaModel1->getProximity();
	auto prox2 = dynaModel2->getProximity();
	auto contactstiff1 = dynaModel1->getContactStiffness(); // 0.001
	auto contactstiff2 = dynaModel2->getContactStiffness(); // 0.1


	auto mesh1 = std::static_pointer_cast<LineMesh>(g1);
	auto mesh2 = std::static_pointer_cast<VESSTetCutMesh>(g2);
	auto mesh1Visual = std::static_pointer_cast<SurfaceMesh>(g1visual);
	auto mesh2Visual = std::static_pointer_cast<SurfaceMesh>(g2visual);

	if ((!mesh1) || (!mesh2)) return; // convertion failed

	auto& tool = toolObj->getControllerTool();
	bool res = findCollisionCandidateInjection(mesh2, tool, tool.cullingThickness[0]);
	if (!res)   //  no collision
	{
		bHasCD = false;
		tool.finalPos = tool.drawPos; // if no, should update finalP
		if (bIsFirstInjected)
		{
			tool.injectPos = tool.finalPos;
		}
		mesh1Visual->setRotation(tool.rotArr);
		mesh1Visual->setTranslation(tool.finalPos);
		return;
	}
	bHasCD = true;

	auto& physVertice = mesh2->getPhysVertex();
	auto& physEdge = mesh2->getPhysEdge();
	auto& physSurfTris = mesh2->getPhysSurfTris();

	// after pbdSolver should update the position
	for (int i = 0; i < physVertice.size(); i++)
	{
		Vec3d& curpos = state2->getVertexPosition(physVertice[i].globalIdx);   // pbdSolver might change it
		physVertice[i].p = curpos;
	}
	// update deform mesh 06/20/2017
    mesh2->initDeformMesh();

	// collision detection state =4 static CD
	bool col = false;
	int n = 0;
	int nbrIter = 5;
	double scale = 0.0;
	double ds = 1.0 / double(nbrIter);
	double ratio = 1;
	Vec3d norm, c1, c2, dV;
	c1 = c2 = dV = norm = Vec3d::Zero();
	double u, v, dist, depth, minDistVertex = 1000000000.0, minDistTri = 1000000000.0;
	// moving to .h file int nVertex=-1, nSurfTri=-1; // record the closest vertex and triangle
	const auto im00 = dynaModel1->getInvMass(0);
	const auto im11 = dynaModel1->getInvMass(1);
	double im0, im1, im2, im3; // edge-edge 0-1  2-3  point-triangle 0  1-2-3
	Vec3d x1, x2, x3, x4;

	// ----------------- triangle-vertex proximity ----------------- //		 // just for the first injection, if continuous pressing the button, not change
	if (bIsFirstInjected)  // output is nSurfTri
	{
		nSurfTri = -1;
		nGlobalClosestVertex0 = -1; // initialize 
        nGlobalClosestVertex1 = -1; // initialize 
        nGlobalClosestVertex2 = -1; // initialize 
		bIsMaximum0 = false;
        bIsMaximum1 = false;
        bIsMaximum2 = false;
		numInjection = 0;
		for (int i = 0; i < physSurfTris.size(); i++)
		{
			im0 = im00;
			auto tris = physSurfTris[i]; //  mesh2->getPhysSurfTris()[i];
			if (tris.closeTool && physEdge[tris.edgeIdx[0]].colDetOn && physEdge[tris.edgeIdx[1]].colDetOn && physEdge[tris.edgeIdx[2]].colDetOn)
			{
				x1 = physVertice[tris.nodeIdx[0]].p;
				x2 = physVertice[tris.nodeIdx[1]].p;
				x3 = physVertice[tris.nodeIdx[2]].p;

				x4 = tool.drawPos + tool.gloVec[1]; // [1] is TIP,  [0] is ROOT, we should use tip ~~**&&&&&????? so maybe need two times for point-triangle?
				injTouchPos = x4;

				Vec3d& pbdx1 = state2->getVertexPosition(tris.nodeIdx[0]);
				Vec3d& pbdx2 = state2->getVertexPosition(tris.nodeIdx[1]); // reference in PbdModel
				Vec3d& pbdx3 = state2->getVertexPosition(tris.nodeIdx[2]); // reference in PbdModel

				const auto im1 = dynaModel2->getInvMass(tris.nodeIdx[0]);
				const auto im2 = dynaModel2->getInvMass(tris.nodeIdx[1]);
				const auto im3 = dynaModel2->getInvMass(tris.nodeIdx[2]);

				////< XZH test output
				if (distTestBetweenTriAndPoint(x1, x2, x3, x4, u, v, dist, norm))//signed distance (triangle normal)
				{
					if (dist < 0) // insert into triangle
					{
						if (fabs(dist) < 0.005)  // 0.008 prox1 * 2 ) //  avoid too long , this is different from CCD's distance determine
						{
							col = true;

							physVertice[tris.nodeIdx[0]].contact = true;
							physVertice[tris.nodeIdx[1]].contact = true;
							physVertice[tris.nodeIdx[2]].contact = true;

							if (fabs(dist) < minDistTri)
							{
								nSurfTri = i;
								minDistTri = fabs(dist);

								//// find the closest vertex in these 3 vertices
								//double minVert = (x4 - x1).norm();
								//nGlobalClosestVertex = tris.nodeIdx[0];
								//double distTmp = (x4 - x2).norm();
								//if (distTmp < minVert)
								//{
								//	minVert = distTmp;
								//	nGlobalClosestVertex = tris.nodeIdx[1];
								//}
								//distTmp = (x4 - x3).norm();
								//if (distTmp < minVert)
								//{
								//	minVert = distTmp;
								//	nGlobalClosestVertex = tris.nodeIdx[2];
								//}

								injTriPos0 = physVertice[tris.nodeIdx[0]].pos;
								injTriPos1 = physVertice[tris.nodeIdx[1]].pos;
								injTriPos2 = physVertice[tris.nodeIdx[2]].pos;

                                nGlobalClosestVertex0 = tris.nodeIdx[0];
                                nGlobalClosestVertex1 = tris.nodeIdx[1];
                                nGlobalClosestVertex2 = tris.nodeIdx[2];
							}
						}
						else
						{
							physVertice[tris.nodeIdx[0]].contact = false;
							physVertice[tris.nodeIdx[1]].contact = false;
							physVertice[tris.nodeIdx[2]].contact = false;
						}
						// END if  fabs(dist) < prox1*2
					}
					else
					{
						physVertice[tris.nodeIdx[0]].contact = false;
						physVertice[tris.nodeIdx[1]].contact = false;
						physVertice[tris.nodeIdx[2]].contact = false;
					}
					// END if dist<0
				} // END if distTest

				else
				{

				}
				////< XZH END output

			} // END if
		} // END for physSurfTris

		  // just for one calculation for each injection solution
		if (nSurfTri >= 0)
		{
            //https://blog.csdn.net/weixin_41737756/article/details/79325570  Vector distance calc
			auto injecTris= mesh2->getInjectSurfTris();
			auto tris = physSurfTris[nSurfTri];
			int v1, v2, v3;
			v1 = tris.nodeIdx[0];
			v2 = tris.nodeIdx[1];
			v3 = tris.nodeIdx[2];
			x1 = physVertice[v1].pos;
			x2 = physVertice[v2].pos;
			x3 = physVertice[v3].pos;
			Vec3d centerPos = (x1 + x2 + x3) / 3;

			// deformation mesh injection solution std::shared_ptr<VESSDeformMesh> 
            deformMesh = mesh2->getVESSDeformMesh();
			deformMesh->setPhysModel(dynaModel2);
			deformMesh->setPhysVertices(physVertice);
			auto tetList = mesh2->getInitTetrahedraVertices();
			auto flagList = mesh2->getDissectFlag();
			// find out the local index of nMinVertex
			nVertex0 = nVertex1 = nVertex2 = -1; // for local index
			int nLocalCloestIndex = -1;
			float minDistInject = 1000000.00;
			size_t minTetIndexGlobal = -1;
			//auto minVertPosTmp= mesh2->getInitialVertexPosition(nGlobalClosestVertex);
   //         auto minVertPos = mesh2->getVertexPosition(nGlobalClosestVertex);

			  ////< XZH 06/05/2019 Method 1 
            for (size_t n = 0; n < injecTris.size();n++)
            {
                auto n0 = injecTris[n].nodeIdx[0];
                auto n1 = injecTris[n].nodeIdx[1];
                auto n2 = injecTris[n].nodeIdx[2];
                auto pos0 = physVertice[n0].pos;
                auto pos1 = physVertice[n1].pos;
                auto pos2 = physVertice[n2].pos;
                //Vec3d a = minVertPos - pos0;
                //Vec3d b = (pos1 - pos0).cross(pos2 - pos0).normalized(); // unit vector b/|b| https://blog.csdn.net/tracing/article/details/46563383
                //double adotb = a.dot(b);
                //double bdotb = b.dot(b);
                //Vec3d c = (adotb / bdotb)*b;
                //double enorm = (a - c).norm();
                //if (enorm < minDistInject)
                //{
                //    minTetIndexGlobal = n0;   ////< XZH 06/05/2019 should 
                //    minDistInject = enorm;
                //}

                auto flag0 = physVertice[n0].bInject;
                auto flag1 = physVertice[n1].bInject;
                auto flag2 = physVertice[n2].bInject;

                if (! (flag0||flag1||flag2)) continue;

                auto posCenter = (pos0 + pos1 + pos2)*0.333333333;
                float dst = (posCenter - injTouchPos).norm();
                if (dst < minDistInject)
                {
                    nGlobalClosestVertex0 = n0;
                    nGlobalClosestVertex1 = n1;
                    nGlobalClosestVertex2 = n2;
                    minDistInject = dst;
                }

                //if (flag0)
                //{
                //    float dst = (pos0 - minVertPosTmp).norm();
                //    if (dst < minDistInject)
                //    {
                //        minTetIndexGlobal = n0;
                //        minDistInject = dst;
                //    }
                //}
                //if (flag1)
                //{
                //    float dst = (pos1 - minVertPosTmp).norm();
                //    if (dst < minDistInject)
                //    {
                //        minTetIndexGlobal = n1;
                //        minDistInject = dst;
                //    }
                //}
                //if (flag2)
                //{
                //    float dst = (pos2 - minVertPosTmp).norm();
                //    if (dst < minDistInject)
                //    {
                //        minTetIndexGlobal = n2;
                //        minDistInject = dst;
                //    }
                //}  // END flag
            }

			//nGlobalClosestVertex0 = minTetIndexGlobal;

			// finding the cloest vertex
			for (std::unordered_map< int, int >::iterator it2 = deformMesh->localToGlobalMappingVertex.begin(); it2 != deformMesh->localToGlobalMappingVertex.end(); it2++)
			{
				if (it2->second == nGlobalClosestVertex0)
				{
					nLocalCloestIndex = it2->first; // local index 
					nVertex0 = nLocalCloestIndex;
					nGlobalVertex0 = nGlobalClosestVertex0;
				}
                if (it2->second == nGlobalClosestVertex1)
                {
                    nLocalCloestIndex = it2->first; // local index 
                    nVertex1 = nLocalCloestIndex;
                    nGlobalVertex1 = nGlobalClosestVertex1;
                }
                if (it2->second == nGlobalClosestVertex2)
                {
                    nLocalCloestIndex = it2->first; // local index 
                    nVertex2 = nLocalCloestIndex;
                    nGlobalVertex2 = nGlobalClosestVertex2;
                }
			}

			// method 2  set ROI
			uniqueVertIdList.clear();
            deformMesh->getNeighborsVertex(uniqueVertIdList, nVertex0, 0, adjLevels); // 0, 7  05/29/2019
			uniqueVertIdList.push_back(nVertex0);
			//uniqueVertIdList.push_back(nVertex1);
			//uniqueVertIdList.push_back(nVertex2);
			uniqueVertIdList.sort();
			uniqueVertIdList.unique();

            //std::list<int> uniqueVertIdListTmp1;
            //deformMesh->getNeighborsVertex(uniqueVertIdListTmp1, nVertex1, 0, adjLevels); // 0, 7  05/29/2019
            //uniqueVertIdListTmp1.push_back(nVertex1);

            //std::list<int> uniqueVertIdListTmp2;
            //deformMesh->getNeighborsVertex(uniqueVertIdListTmp2, nVertex2, 0, adjLevels); // 0, 7  05/29/2019
            //uniqueVertIdListTmp2.push_back(nVertex2);

            //uniqueVertIdList.merge(uniqueVertIdListTmp1);
            //uniqueVertIdList.merge(uniqueVertIdListTmp2);
            //uniqueVertIdList.sort();
            //uniqueVertIdList.unique();
            //deformMesh->updateAdjList();
		}
		// END just for one
	}    // END   collision for first injection

	// handling the cloest triangle
	std::vector<int> vertexCloestList;
	double radiusCloest = 2.5; // in a circle, other vertex are fixed
    double inflationMax = maxInjectIncrement;
	bIsMaximum0 = false;
    bIsMaximum1 = false;
    bIsMaximum2 = false;
	/*******Method 2**********/
	//dynaModel2->resetFixedPoints(); // before injection ,should reset
	if (nSurfTri >= 0) // injection touch
	{
		//// determine whether it up to the maximum
		Vec3d& pbdxG0 = state2->getVertexPosition(nGlobalClosestVertex0); 
        Vec3d normVert = pbdxG0 - mesh2->getInitialVertexPosition(nGlobalClosestVertex0);
        if (normVert.norm() > inflationMax)  //  should change back to 0.008  on 05/31/2019
		{
			bIsMaximum0 = true;
		}
        //Vec3d& pbdxG1 = state2->getVertexPosition(nGlobalClosestVertex1);
        //normVert = pbdxG1 - mesh2->getInitialVertexPosition(nGlobalClosestVertex1);
        //if (normVert.norm() > inflationMax)  //  should change back to 0.008  on 05/31/2019
        //{
        //    bIsMaximum1 = true;
        //}
        //Vec3d& pbdxG2 = state2->getVertexPosition(nGlobalClosestVertex2);
        //normVert = pbdxG2 - mesh2->getInitialVertexPosition(nGlobalClosestVertex2);
        //if (normVert.norm() > inflationMax)  //  should change back to 0.008  on 05/31/2019
        //{
        //    bIsMaximum2 = true;
        //}
		numInjection++;
		if (numInjection < INJECTIONMAX) // if (1) // if not max						 //if (numInjection < INJECTIONMAX)
		{
			// deformation mesh injection solution
			//std::shared_ptr<VESSDeformMesh> deformMesh = mesh2->getVESSDeformMesh();
			//deformMesh->setPhysModel(dynaModel2);
			//deformMesh->setPhysVertices(physVertice);

			// moving the vertex in its normal direction         //  the max moving vertex
			//deformMesh->setConstraint(nVertex0, physVertice[nGlobalVertex0].pos + deformMesh->N.col(nVertex0).normalized() * 0.10);
			//deformMesh->setConstraint(nVertex1, physVertice[nGlobalVertex1].pos + deformMesh->N.col(nVertex1).normalized() * 0.10);
			//deformMesh->setConstraint(nVertex2, physVertice[nGlobalVertex2].pos + deformMesh->N.col(nVertex2).normalized() * 0.10);
			//deformMesh->setConstraint(nVertex0, deformMesh->V.col(nVertex0) + deformMesh->N.col(nVertex0).normalized() * 0.08);
			//deformMesh->setConstraint(nVertex1, deformMesh->V.col(nVertex1) + deformMesh->N.col(nVertex1).normalized() * 0.03);
			//deformMesh->setConstraint(nVertex2, deformMesh->V.col(nVertex2) + deformMesh->N.col(nVertex2).normalized() * 0.03);
			if (!bIsMaximum0)
			{
                //printf("Injecting: %d, ", nGlobalClosestVertex0);
                if (bInflationDir) deformMesh->setConstraint(nVertex0, deformMesh->V.col(nVertex0) + deformMesh->N.col(nVertex0).normalized() * injectIncrement); 
                else deformMesh->setConstraint(nVertex0, deformMesh->V.col(nVertex0) + /*deformMesh->N.col(nVertex0).normalized()*/ Vec3d(0, 0, 1) * injectIncrement);  // deformMesh->setConstraint(nVertex0, deformMesh->V.col(nVertex0) + deformMesh->getInitVertNormal().col(nVertex0).normalized() * 0.08); // using initial normal not changed
			}
            else
            {
                deformMesh->setConstraint(nVertex0, /*deformMesh->V.col(nVertex0)*/pbdxG0);  // no need, this is the bug?!!!??
                dynaModel2->setFixedPoint(nGlobalClosestVertex0);
            }
            //if (!bIsMaximum1)
            //{
            //    printf("%d, ", nGlobalClosestVertex1);
            //    if (bInflationDir) deformMesh->setConstraint(nVertex1, deformMesh->V.col(nVertex1) + deformMesh->N.col(nVertex1).normalized() * injectIncrement);
            //    else deformMesh->setConstraint(nVertex1, deformMesh->V.col(nVertex1) + /*deformMesh->N.col(nVertex1).normalized()*/Vec3d(0, 0, 1) * injectIncrement);  // deformMesh->setConstraint(nVertex0, deformMesh->V.col(nVertex0) + deformMesh->getInitVertNormal().col(nVertex0).normalized() * 0.08); // using initial normal not changed
            //}
            //else
            //{
            //    deformMesh->setConstraint(nVertex1, /*deformMesh->V.col(nVertex1)*/pbdxG1);  // no need, this is the bug?!!!??
            //    dynaModel2->setFixedPoint(nGlobalClosestVertex1);
            //}
            //if (!bIsMaximum2)
            //{
            //    printf(" %d \n", nGlobalClosestVertex2);
            //    if (bInflationDir) deformMesh->setConstraint(nVertex2, deformMesh->V.col(nVertex2) + deformMesh->N.col(nVertex2).normalized() * injectIncrement);
            //    else deformMesh->setConstraint(nVertex2, deformMesh->V.col(nVertex2) + /*deformMesh->N.col(nVertex2).normalized()*/ Vec3d(0, 0, 1) * injectIncrement);  // deformMesh->setConstraint(nVertex0, deformMesh->V.col(nVertex0) + deformMesh->getInitVertNormal().col(nVertex0).normalized() * 0.08); // using initial normal not changed
            //}
            //else
            //{
            //    deformMesh->setConstraint(nVertex2, /*deformMesh->V.col(nVertex2)*/pbdxG2);  // no need, this is the bug?!!!??
            //    dynaModel2->setFixedPoint(nGlobalClosestVertex2);
            //}
			//< XZH neighbor

			//injectList.clear();
            std::list<int>::iterator it;
            int mn = 0;
            bool flag0 = false, flag1 = false, flag2 = false, flag3 = false, flag4 = false;
            for (int m = 0; m < deformMesh->V.cols(); m++)
            {
                int nnGlobal = deformMesh->localToGlobalMappingVertex[m];
                it = find(uniqueVertIdList.begin(), uniqueVertIdList.end(), m); // find in the list
                if (it == uniqueVertIdList.end())  // not in the list
                {
                    deformMesh->setConstraint(m, deformMesh->Vinit.col(m));  // no need, this is the bug?!!!??
                    dynaModel2->setFixedPoint(nnGlobal);
                    mn++;
                }
				else
				{
					injectList.push_back(nnGlobal); // (deformMesh->localToGlobalMappingVertex.at(m));
				}
            }
			////< XZH END

			////< XZH 
			//injectList.clear();
			
			  // END SET ROI Remove duplicates
			int mTmp = deformMesh->constraints.size();
			deformMesh->solve(deformMesh->V);
		} // END if not max       
	}  // END if has touch triangle
	else {}  // -1 no collision and injection touch
	//dynaModel2->resetFixedPoints(); // after injection ,should reset resume the fixed points as initializes , then will not change it
	/*******Method 2 END*******/

	// UPDATE pos as p
	//bHasCD = col;
	tool.finalPos = tool.drawPos; //
	if (bIsFirstInjected)
	{
		tool.injectPos = tool.finalPos;
	}

	for (int i = 0; i < physVertice.size(); i++)
	{
		physVertice[i].pos = physVertice[i].p;
		// update vertex pos and init vertex pos on 10/07/2019
		mesh2->setInitialVertexPositionChangeable(i, physVertice[i].pos);
		mesh2->setVertexPosition(i, physVertice[i].pos);
	}
	//second->updateGeometries();
	//deformMesh->setPhysVertices(physVertice);

	mesh1Visual->setRotation(tool.rotArr);
	mesh1Visual->setTranslation(tool.finalPos);
}

//void 
//PbdInteractionPair::doNarrowPhaseCollisionInjection()
//{
//    bool isTool = false;
//    auto toolObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(first1);
//    if (!toolObj) return;  // not pbdvcobj
//
//    auto g1 = first1->getCollidingGeometry();
//    auto g2 = second->getCollidingGeometry();
//    auto g1visual = first1->getVisualGeometry();
//    auto g2visual = second->getVisualGeometry();
//
//    auto map1 = first1->getPhysicsToCollidingMap();
//    auto map2 = second->getPhysicsToCollidingMap();
//
//    auto dynaModel1 = std::static_pointer_cast<PbdModel>(first1->getDynamicalModel());
//    auto dynaModel2 = std::static_pointer_cast<PbdModel>(second->getDynamicalModel());
//    auto state1 = dynaModel1->getCurrentState();
//    auto state2 = dynaModel2->getCurrentState();
//
//    auto prox1 = dynaModel1->getProximity();
//    auto prox2 = dynaModel2->getProximity();
//    auto contactstiff1 = dynaModel1->getContactStiffness(); // 0.001
//    auto contactstiff2 = dynaModel2->getContactStiffness(); // 0.1
//
//
//    auto mesh1 = std::static_pointer_cast<LineMesh>(g1);
//    auto mesh2 = std::static_pointer_cast<VESSTetrahedralMesh>(g2);
//    auto mesh1Visual = std::static_pointer_cast<SurfaceMesh>(g1visual);
//    auto mesh2Visual = std::static_pointer_cast<SurfaceMesh>(g2visual);
//
//    if ((!mesh1) || (!mesh2)) return; // convertion failed
//
//    auto& tool = toolObj->getControllerTool();
//    bool res = findCollisionCandidateInjection(mesh2, tool, tool.cullingThickness[0]);  //  currentState=0 should use cullingthickness (20) not prox1 (0.2)
//    if (!res)   //  no collision
//    {
//        bHasCD = false;
//        tool.finalPos = tool.drawPos; // if no, should update finalP
//        if (bIsFirstInjected)
//        {
//            tool.injectPos = tool.finalPos;
//        }   
//
//        mesh1Visual->setRotation(tool.rotArr);
//        mesh1Visual->setTranslation(tool.finalPos);
//        return;
//    }
//    bHasCD = true;
//
//    auto& physVertice = mesh2->getPhysVertex();
//    auto& physEdge = mesh2->getPhysEdge();
//    auto& physSurfTris = mesh2->getPhysSurfTris();  // �ҳ�������tip����������Ǹ������Σ��������εľ�������Ǹ��ģ�������ķ��򣬴��������ȥ��
//    // Hash Table
//    auto& hashtableVertices = mesh2->getHashTableVess();
//
//    // after pbdSolver should update the position
//    for (int i = 0; i < physVertice.size(); i++)
//    {
//        Vec3d& curpos = state2->getVertexPosition(physVertice[i].globalIdx);   // pbdSolver might change it
//        physVertice[i].p = curpos;
//    }
//
//    // update deform mesh 06/20/2017
//    mesh2->initDeformMesh(); 
//    // collision detection state =4 static CD
//
//    bool col = false;
//    int n = 0;
//    int nbrIter = 5;
//    double scale = 0.0;
//    double ds = 1.0 / double(nbrIter);
//    double ratio = 1;
//    Vec3d norm, c1, c2, dV;
//    c1 = c2 = dV = norm = Vec3d::Zero();
//    double u, v, dist, depth, minDistVertex = 1000000000.0, minDistTri = 1000000000.0;
//    // moving to .h file int nVertex=-1, nSurfTri=-1; // record the closest vertex and triangle
//    const auto im00 = dynaModel1->getInvMass(0);
//    const auto im11 = dynaModel1->getInvMass(1);
//    double im0, im1, im2, im3; // edge-edge 0-1  2-3  point-triangle 0  1-2-3
//    Vec3d x1, x2, x3, x4;
//
//    // ----------------- triangle-vertex proximity ----------------- //		 // just for the first injection, if continuous pressing the button, not change
//    if (bIsFirstInjected)  // output is nSurfTri
//    {
//        nSurfTri = -1;
//        nGlobalClosestVertex = -1; // initialize 
//        bIsMaximum = false;
//        numInjection = 0;
//        for (int i = 0; i < physSurfTris.size(); i++)
//        {
//            im0 = im00;
//            auto tris = physSurfTris[i]; //  mesh2->getPhysSurfTris()[i];
//            
//            if (tris.closeTool && physEdge[tris.edgeIdx[0]].colDetOn && physEdge[tris.edgeIdx[1]].colDetOn && physEdge[tris.edgeIdx[2]].colDetOn)
//            {
//                x1 = physVertice[tris.nodeIdx[0]].p;
//                x2 = physVertice[tris.nodeIdx[1]].p;
//                x3 = physVertice[tris.nodeIdx[2]].p;
//
//                x4 = tool.drawPos + tool.gloVec[1]; // [1] is TIP,  [0] is ROOT, we should use tip ~~**&&&&&????? so maybe need two times for point-triangle?
//                injTouchPos = x4;
//
//                Vec3d& pbdx1 = state2->getVertexPosition(tris.nodeIdx[0]);
//                Vec3d& pbdx2 = state2->getVertexPosition(tris.nodeIdx[1]); // reference in PbdModel
//                Vec3d& pbdx3 = state2->getVertexPosition(tris.nodeIdx[2]); // reference in PbdModel
//
//                const auto im1 = dynaModel2->getInvMass(tris.nodeIdx[0]);
//                const auto im2 = dynaModel2->getInvMass(tris.nodeIdx[1]);
//                const auto im3 = dynaModel2->getInvMass(tris.nodeIdx[2]);
//
//                  ////< XZH test output
//                FILE *outPos = fopen("i:/outContactInj.dat", "a");                
//                if (distTestBetweenTriAndPoint(x1, x2, x3, x4, u, v, dist, norm))//signed distance (triangle normal)
//                {
//                    fprintf(outPos, "%f\n", dist);
//                    if (dist < 0) // insert into triangle
//                    {
//                        if (fabs(dist) <0.005 )  // 0.008 prox1 * 2 ) //  avoid too long , this is different from CCD's distance determine
//                        {
//                            col = true;
//
//                            physVertice[tris.nodeIdx[0]].contact = true;
//                            physVertice[tris.nodeIdx[1]].contact = true;
//                            physVertice[tris.nodeIdx[2]].contact = true;
//                         
//                            if (fabs(dist) < minDistTri)
//                            {
//                                nSurfTri = i;
//                                minDistTri = fabs(dist);
//
//                                // find the closest vertex in these 3 vertices
//                                double minVert = (x4 - x1).norm();
//                                nGlobalClosestVertex = tris.nodeIdx[0];
//                                double distTmp = (x4 - x2).norm();
//                                if (distTmp<minVert)
//                                {
//                                    minVert = distTmp;
//                                    nGlobalClosestVertex = tris.nodeIdx[1];
//                                }
//                                distTmp = (x4 - x3).norm();
//                                if (distTmp < minVert)
//                                {
//                                    minVert = distTmp;
//                                    nGlobalClosestVertex = tris.nodeIdx[2];
//                                }
//
//                                injTriPos0 = physVertice[tris.nodeIdx[0]].pos;
//                                injTriPos1 = physVertice[tris.nodeIdx[1]].pos;
//                                injTriPos2 = physVertice[tris.nodeIdx[2]].pos;
//                            }
//                        }
//                        else
//                        {
//                            physVertice[tris.nodeIdx[0]].contact = false;
//                            physVertice[tris.nodeIdx[1]].contact = false;
//                            physVertice[tris.nodeIdx[2]].contact = false;
//                        }
//                        // END if  fabs(dist) < prox1*2
//                    }
//                    else
//                    {
//                        physVertice[tris.nodeIdx[0]].contact = false;
//                        physVertice[tris.nodeIdx[1]].contact = false;
//                        physVertice[tris.nodeIdx[2]].contact = false;
//                    }
//                    // END if dist<0
//                } // END if distTest
//
//                else
//                {
//                    fprintf(outPos, "%f\n", dist);
//                }
//                fclose(outPos);
//                ////< XZH END output
//
//            } // END if
//        } // END for physSurfTris
//
//        // just for one calculation for each injection solution
//        if (nSurfTri>=0)
//        {
//            auto tris = physSurfTris[nSurfTri];
//            int v1, v2, v3;
//            v1 = tris.nodeIdx[0];
//            v2 = tris.nodeIdx[1];
//            v3 = tris.nodeIdx[2];
//            x1 = physVertice[v1].pos;
//            x2 = physVertice[v2].pos;
//            x3 = physVertice[v3].pos;
//            Vec3d centerPos = (x1 + x2 + x3) / 3;
//
//            // deformation mesh injection solution std::shared_ptr<VESSDeformMesh> 
//            deformMesh = mesh2->getVESSDeformMesh();
//            deformMesh->setPhysModel(dynaModel2);
//            deformMesh->setPhysVertices(physVertice);
//            // find out the local index of nMinVertex
//            nVertex0 = nVertex1 = nVertex2 = -1; // for local index
//            int nLocalCloestIndex = -1;
//            
//            // finding the cloest vertex
//            for (std::unordered_map< int, int >::iterator it2 = deformMesh->localToGlobalMappingVertex.begin(); it2 != deformMesh->localToGlobalMappingVertex.end(); it2++)
//            {
//                if (it2->second == nGlobalClosestVertex)
//                {
//                    nLocalCloestIndex = it2->first; // local index 
//                    nVertex0 = nLocalCloestIndex;
//                    nGlobalVertex0 = nGlobalClosestVertex;
//                    vecGlobalVertex0=mesh2->getInitialVertexPosition(nGlobalClosestVertex);
//                    Vec3d vecTmp=Vec3d::Zero();
//                    for (int t = 0; t < deformMesh->adjList[nVertex0].size(); t++)
//                    {
//                        int nLocal = deformMesh->adjList[nVertex0][t];
//                        int nGlobal = deformMesh->localToGlobalMappingVertex.at(nLocal);
//                        vecTmp += mesh2->getInitialVertexPosition(nGlobalClosestVertex);
//                    }
//                    centerGlobalPos = vecTmp / deformMesh->adjList[nVertex0].size();
//                    break;
//                }
//            }
//
//            // method 2  set ROI
//            uniqueVertIdList.clear();   //  std::list<int> uniqueVertIdList; std::vector<int> vecROI; // region of interest;   
//			//printf(" local and global: %d %d ", nVertex0, nGlobalClosestVertex);
//            deformMesh->getNeighborsVertex(uniqueVertIdList, nVertex0, 0, 7);  // (uniqueVertIdList, nVertex0, 0, 4); // deformMesh->getNeighborsVertex(vecROI, nVertex0, 0, 3);  // ����������Ӱ���ٶȵĻ����Կ����ڼ�������ʱ��ÿһ������Χ�Ĵ������������Ͳ����ڴ˼���
//            //deformMesh->getNeighborsVertex(uniqueVertIdList, nVertex1, 0, 4);
//            //deformMesh->getNeighborsVertex(uniqueVertIdList, nVertex2, 0, 4);
//            // Renumber the vertices // Remove duplicates
//            
//            //for (int m = 0; m < vecROI.size(); m++)
//            //{
//            //    uniqueVertIdList.push_back(vecROI[m]);
//            //}
//            uniqueVertIdList.push_back(nVertex0);
//            //uniqueVertIdList.push_back(nVertex1);
//            //uniqueVertIdList.push_back(nVertex2);
//            uniqueVertIdList.sort();
//            uniqueVertIdList.unique();
//        }
//        // END just for one
//
//    }    // END   collision for first injection
//
//    // handling the cloest triangle
//    std::vector<int> vertexCloestList;
//    double radiusCloest = 2.5; // in a circle, other vertex are fixed
//
//    /*******Method 2**********/
//    //dynaModel2->resetFixedPoints(); // before injection ,should reset
//    if (nSurfTri >= 0) // injection touch
//    {
//        //// determine whether it up to the maximum
//        Vec3d& pbdxG = state2->getVertexPosition(nGlobalClosestVertex); // global index  �ҳ�����ʼ��Ƚ������˶��٣�����ܳ���0.5-1.0֮��
//        // ��ʵ��������ע����λ�ã�ͨ����ʼʱ�� ע��������ڵ�����ĵ��������ȷ��Ӧ���������٣�������ֹ һЩ����γ�
//        Vec3d normVert = pbdxG - vecGlobalVertex0;
//        if (normVert.norm() > 0.008)
//        {
//            bIsMaximum = true;
//        }
//        numInjection++;
//        
//        if (!bIsMaximum) // if not max
//        //if (numInjection < INJECTIONMAX)
//        {
//            // deformation mesh injection solution
//            //std::shared_ptr<VESSDeformMesh> deformMesh = mesh2->getVESSDeformMesh();
//            //deformMesh->setPhysModel(dynaModel2);
//            //deformMesh->setPhysVertices(physVertice);
//
//            // moving the vertex in its normal direction         //  the max moving vertex
//            //deformMesh->setConstraint(nVertex0, physVertice[nGlobalVertex0].pos + deformMesh->N.col(nVertex0).normalized() * 0.10);
//            //deformMesh->setConstraint(nVertex1, physVertice[nGlobalVertex1].pos + deformMesh->N.col(nVertex1).normalized() * 0.10);
//            //deformMesh->setConstraint(nVertex2, physVertice[nGlobalVertex2].pos + deformMesh->N.col(nVertex2).normalized() * 0.10);
//            //deformMesh->setConstraint(nVertex0, deformMesh->V.col(nVertex0) + deformMesh->N.col(nVertex0).normalized() * 0.08);
//            //deformMesh->setConstraint(nVertex1, deformMesh->V.col(nVertex1) + deformMesh->N.col(nVertex1).normalized() * 0.03);
//            //deformMesh->setConstraint(nVertex2, deformMesh->V.col(nVertex2) + deformMesh->N.col(nVertex2).normalized() * 0.03);
//            deformMesh->setConstraint(nVertex0, deformMesh->V.col(nVertex0) + deformMesh->N.col(nVertex0).normalized() * 0.001);  // deformMesh->setConstraint(nVertex0, deformMesh->V.col(nVertex0) + deformMesh->getInitVertNormal().col(nVertex0).normalized() * 0.08); // using initial normal not changed
//            Vec3d tttmp = deformMesh->N.col(nVertex0).normalized() * 0.001;
//            printf("inj: %f %f %f\n", tttmp.x(), tttmp.y(), tttmp.z());
//			  ////< XZH neighbor
//			std::list<int> uniqueVertIdListLev1;
//			deformMesh->getNeighborsVertex(uniqueVertIdListLev1, nVertex0, 0, 1);
//			uniqueVertIdListLev1.sort();
//			uniqueVertIdListLev1.unique();
//			std::list<int> uniqueVertIdListLev2;
//			deformMesh->getNeighborsVertex(uniqueVertIdListLev2, nVertex0, 0, 2);
//			uniqueVertIdListLev2.sort();
//			uniqueVertIdListLev2.unique();
//			std::list<int> uniqueVertIdListLev3;
//			deformMesh->getNeighborsVertex(uniqueVertIdListLev3, nVertex0, 0, 3);
//			uniqueVertIdListLev3.sort();
//			uniqueVertIdListLev3.unique();
//			std::list<int>::iterator iter;
//			for (iter = uniqueVertIdListLev1.begin(); iter != uniqueVertIdListLev1.end(); iter++)
//			{
//				int nn = *iter;
//				printf("LEV1: %d\n", nn);
//				deformMesh->setConstraint(nn, deformMesh->V.col(nn) + deformMesh->N.col(nn).normalized() * 0.0008);
//			}
//			for (iter = uniqueVertIdListLev2.begin(); iter != uniqueVertIdListLev2.end(); iter++)
//			{
//				int nn = *iter;
//				printf("LEV2: %d\n", nn);
//				deformMesh->setConstraint(nn, deformMesh->V.col(nn) + deformMesh->N.col(nn).normalized() * 0.0005);
//			}
//			for (iter = uniqueVertIdListLev3.begin(); iter != uniqueVertIdListLev3.end(); iter++)
//			{
//				int nn = *iter;
//				printf("LEV3: %d\n", nn);
//				deformMesh->setConstraint(nn, deformMesh->V.col(nn) + deformMesh->N.col(nn).normalized() * 0.0002);
//			}
//			  ////< XZH END
//
//			  ////< XZH 
//			injectList.clear();
//            std::list<int>::iterator it;
//            for (int m = 0; m < deformMesh->V.cols(); m++)
//            {
//                it = find(uniqueVertIdList.begin(), uniqueVertIdList.end(), m); // find in the list
//                if (it == uniqueVertIdList.end())  // not in the list
//                {
//                    deformMesh->setConstraint(m, deformMesh->Vinit.col(m));  // no need, this is the bug?!!!??
//                    dynaModel2->setFixedPoint(deformMesh->localToGlobalMappingVertex.at(m));
//                }
//				else   ////< XZH in the list
//				{
//					injectList.push_back(deformMesh->localToGlobalMappingVertex.at(m));
//				}
//                //for (it = uniqueVertIdList.begin(); it != uniqueVertIdList.end(); it++)
//                //{
//                //    int n = *it;
//                //    if ((m!=n) && (m != nVertex0) && (m != nVertex1) && (m != nVertex2)) // not in this list, and not n0 n1 n2 should set fixed
//                //    {
//                //        deformMesh->setConstraint(m, deformMesh->V.col(m));  // no need, this is the bug?!!!??
//                //        dynaModel2->setFixedPoint(deformMesh->localToGlobalMappingVertex.at(m));
//                //    }
//                //} // end for
//            } // end for all vertices
//            // END SET ROI Remove duplicates
//
//            int mTmp = deformMesh->constraints.size();
//            deformMesh->solve(deformMesh->V);
//
//        } // END if not max       
//    }  // END if has touch triangle
//    else {}  // -1 no collision and injection touch
//    //dynaModel2->resetFixedPoints(); // after injection ,should reset resume the fixed points as initializes , then will not change it
//    /*******Method 2 END*******/
//
//    // UPDATE pos as p
//    //bHasCD = col;
//    tool.finalPos = tool.drawPos; //
//    if (bIsFirstInjected)
//    {
//        tool.injectPos = tool.finalPos;
//    }
//
//    for (int i = 0; i < physVertice.size(); i++)
//    {
//        physVertice[i].pos = physVertice[i].p;
//    }
//    //second->updateGeometries();
//    //deformMesh->setPhysVertices(physVertice);
//    // HashTable 
//    hashtableVertices->clear();
//    hashtableVertices->insertPoints(physVertice);
//    // HashTable END
//
//    mesh1Visual->setRotation(tool.rotArr);
//    mesh1Visual->setTranslation(tool.finalPos);
//}
void
PbdInteractionPair::chooseProcedure()
{
    bool isTool = false;
    auto toolObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(first1);
    if (!toolObj) return;  // not pbdvcobj
    auto& tool = toolObj->getControllerTool();
    if (tool.button[0])
    {
        bIsInjection = true;
        bIsPreInjection = bIsCurInjection;
        bIsCurInjection = bIsInjection; 
        if (bIsPreInjection&&bIsCurInjection) // continuous
        {
            bIsFirstInjected = false;
        }
        else
        {
            bIsFirstInjected = true;
        }
        //if (bIsFirstInjected)
        //{
        //    doNarrowPhaseCollisionInjection();
        //}
        doNarrowPhaseCollisionInjection2();
        noinjCount = 0;
    }
    else
    {
        noinjCount++;
        bIsInjection = false;
        bIsFirstInjected = false;
        bIsPreInjection = bIsCurInjection;
        bIsCurInjection = bIsInjection;
        if (bIsPreInjection && (!bIsCurInjection)) bUpdatePBD = true;
        else bUpdatePBD = false;
        if (bUpdatePBD)
        {
            auto dynaModel2 = std::static_pointer_cast<PbdModel>(second->getDynamicalModel());
            dynaModel2->initializeReset();
        }
        if (noinjCount>50)
        {
            //doNarrowPhaseContinuousCollisionVESS();
            doNarrowPhaseContinuousCollisionVESS3();
        }
        
    }
}

void
PbdInteractionPair::computeContactForceInjection()
{
    auto toolObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(first1);
    auto& tool = toolObj->getControllerTool();
    tool.diffPos = (tool.injectPos -tool.hapticPos);
    Vec3d ddiffPos = tool.diffPos - tool.diffPosPre;

    // set Parameter
    first->setForceFeedbackParams(tool.injectPos , true, true); // injected  should set isCD to true
    return;
}

void
PbdInteractionPair::doMarking()
{
    auto toolObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(first);
    if (!toolObj) return;  // not pbdvcobj
    auto& tool = toolObj->getControllerTool();
    if (tool.button[0])
    {
        bIsMarking = true;
        bIsPreMarking = bIsCurMarking;
        bIsCurMarking = bIsMarking;
        if (bIsPreMarking&&bIsCurMarking) // continuous
        {
            bIsFirstMarked = false;
        }
        else
        {
            bIsFirstMarked = true;
        }
    }
    else
    {
        bIsMarking = false;
        bIsFirstMarked= false;
        bIsPreMarking = bIsCurMarking;
        bIsCurMarking= bIsMarking;
    }
    if (bIsFirstMarked)
    {
        // marking
        //auto g1 = first->getCollidingGeometry();
        auto g2 = second->getVisualGeometry(); //  second->getCollidingGeometry();  // second->getVisualGeometry(); // second->getCollidingGeometry();
        //auto tetMesh = std::static_pointer_cast<VESSTetrahedralMesh>(g2);
        auto visualMesh = std::static_pointer_cast<SurfaceMesh>(g2);

        int j, cn = -1;
        double length;
        double dist, minDist = 1000000000.0;
        Vec3d aveNodePos;
        Vec3d end0, end1;
        Vec3d line;
        double para;
        double threshold = 3.0*0.001;

        end0 = tool.pDrawPos + tool.pGloVec[0]; // ROOT
        end1 = tool.drawPos + tool.pGloVec[1]; // for CD  tool.pDrawPos + tool.pGloVec[1];  // for CCD // Tip  touch colon
        sphereTipPos = end1; // test
        length = (end1 - end0).norm();
        if (length < 0.0001f) return; // not start 
        line = (end1 - end0).normalized();

        //auto& physVertices = tetMesh->getPhysVertex(); // using tet
        auto& vecPosList = visualMesh->getVertexPositions(); // using triangles
        // Method 3 using the distance between the vertex to the tip of tool to do broad phase
        //threshold = 3.0; // add the near vertex to the candidates
        //for (int i = 0; i < physVertices.size(); i++) // for (int i = 0; i < tetMesh->getPhysVertex().size(); i++)
        for (int i = 0; i < vecPosList.size(); i++)
        {
            //auto &vertx = physVertices[i]; //  tetMesh->getPhysVertex()[i];
            auto &verPos = vecPosList[i]; //  physVertices[i].pos; //  vecPosList[i]; //  physVertices[i].pos; //  tetMesh->getPhysVertex()[i].pos;  // use pos or pPos or oPos??
            dist = (verPos - end1).norm();
            //printf("dist %d %f  lencod=%f, length=%f\n", vertx.globalIdx, dist, lencod, length);
            //printf("vertex: %d, dist %f  \n", i, dist);
            if (dist < minDist)
            {
                cn = i; // physVertices[i].globalIdx; //  physVertices[i].globalIdx; //  i;  // i is a local index in physVertices, should use global
                minDist = dist;
            }
        }
        //tetMesh->setClosestVertTool(cn);
        if (minDist < threshold) // cn is the closest one
        {
            markingIndex = cn;
            markingPos = vecPosList[cn]; //  physVertices[cn].pos; //    physVertices[cn].pos;
            bIsMark = true;
        }
        // Method 3 END

        //bIsFirstMarked = false;
    }
}

void 
PbdInteractionPair::doSimpleCollision(int type)
{
    std::shared_ptr<PbdVirtualCouplingObject> toolObj;
    switch (type)
    {
    case 0:
    default:
        toolObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(first);
        break;
    case 1:
        toolObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(first1);
        break;
    case 2:
        toolObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(first2);
        break;
    }
    if (!toolObj) return;  // not pbdvcobj
    auto& tool = toolObj->getControllerTool();

    // paras
    float length, linePara, height1, height2, distance1, distance2, lineDistance, distanceCross;
    auto cylinder = std::dynamic_pointer_cast<Cylinder>(cylinderObj->getVisualModels()[0]->getGeometry());
    auto lineCylinder = std::dynamic_pointer_cast<LineMesh>(lineCylinderObj->getVisualModels()[0]->getGeometry());
    Vec3d line, node1, node2;
    Vec3d center1 = cylinder->getVertexPositions()[0];
    Vec3d center2 = cylinder->getVertexPositions()[1];
    double radius = cylinder->getRadius();
    node1 = tool.pDrawPos + tool.pGloVec[0]; // ROOT
    node2 = tool.pDrawPos + tool.pGloVec[1];  // Tip  touch colon
    Vec3d dirMove = Vec3d::Zero();

    length = (center2 - center1).norm();
    line = (center2 - center1).normalized();
    linePara = 0;

    height1 = line.dot(node1 - center1);
    height2 = line.dot(node2 - center1);
    distance1 = (line.cross(node1 - center1)).norm();
    distance2 = (line.cross(node2 - center1)).norm();

    if ((height1 > length) && (height2 > length))
    {
        lineDistance = -1;
        linePara = 0;
    }
    else if ((height1 < 0) && (height2 < 0))
    {
        lineDistance = -1;
        linePara = 0;
    }
    else if ((height1 < length) && (height1 > 0) && (height2 < length) && (height2 > 0))
    {
        if (distance1 < distance2)
        {
            lineDistance = distance2;
            if (height1<height2)
            {
                linePara = line.dot(node2 - node1) / line.dot(center2 - node1);
                distanceCross = (line.cross((node1 + linePara*(center2 - node1) - center2))).norm();
                dirMove =  (node1 + linePara*(center2 - node1) - node2).normalized();
            }
            else
            {
                linePara = line.dot(node1 - node2) / line.dot(center2 - node2);
                distanceCross = (line.cross((node2 + linePara*(center2 - node2) - center2))).norm();
                dirMove = (node2 + linePara*(center2 - node2) - node1).normalized();
                if (dirMove.dot(center2-node1)<0)
                {
                    dirMove = -dirMove;
                }
            }
            linePara = 1;
        }
        else
        {
            lineDistance = distance1;
            if (height1 < height2)
            {
                linePara = line.dot(node2 - node1) / line.dot(center2 - node1);
                distanceCross = (line.cross((node1 + linePara*(center2 - node1) - center2))).norm();
                dirMove = (node1 + linePara*(center2 - node1) - node2).normalized();
            }
            else
            {
                linePara = line.dot(node1 - node2) / line.dot(center2 - node2);
                distanceCross = (line.cross((node2 + linePara*(center2 - node2) - center2))).norm();
                dirMove = (node2 + linePara*(center2 - node2) - node1).normalized();
            }
            linePara = 0;
        }
    }
    else  // one is inside , one is outside
    {
        if ((distance1 < radius) && (distance2 < radius))
        {
            if ((height1 < length) && (height1>0))  // 1 is inside
            {
                lineDistance = distance1;

                linePara = 0;
            }
            else
            {
                lineDistance = distance2;
                linePara = 1;
            }
        }
        else
        {
            double distanceInside, paraInside;
            if ((length > height1) && (height1>0))
            {
                if (height2 > length)
                {
                    linePara = line.dot(center2 - node1) / line.dot(node2 - node1);
                    distanceCross = (line.cross((node1 + linePara*(node2 - node1) - center2))).norm();
                    dirMove = -1 * (node1 + linePara*(node2 - node1) - center2);
                }
                else
                {
                    linePara = line.dot(center1 - node1) / line.dot(node2 - node1);
                    distanceCross = (line.cross((node1 + linePara*(node2 - node1) - center1))).norm();
                    dirMove = -1 * (node1 + linePara*(node2 - node1) - center1);
                }
                distanceInside = distance1;
                paraInside = 0;
            }
            else
            {
                if (height1 > length)
                {
                    linePara = line.dot(center2 - node1) / line.dot(node2 - node1);
                    distanceCross = (line.cross((node1 + linePara*(node2 - node1) - center2))).norm();
                    dirMove = -1 * (node1 + linePara*(node2 - node1) - center2);
                }
                else
                {
                    linePara = line.dot(center1 - node1) / line.dot(node2 - node1);
                    distanceCross = (line.cross((node1 + linePara*(node2 - node1) - center1))).norm();
                    dirMove = -1 * (node1 + linePara*(node2 - node1) - center1);
                }
                distanceInside = distance2;
                paraInside = 1;
            }

            if (distanceInside > distanceCross)
            {
                lineDistance = distanceInside;
                linePara = paraInside;
            }
            else
            {
                lineDistance = distanceCross;
            }

        }
    }

    ////< XZH force 
    Vec3d force(0., 0., 0.);
    bool isCD = false;
    double ratio = 0.8;
    if (lineDistance >(ratio* radius))
    {
        //tool.drawPos = tool.drawPos + 1.0*dirMove;
        isCD = true;
        force = (lineDistance - (ratio* radius))*dirMove;
    }
    else
    {
        isCD = false;
        force = Vec3d::Zero();
    }

    tool.finalPos = tool.drawPos;
    
    // xzh clear force array when no collision detection 		
    if (force.norm() < 0.0000001)
    {
        toolObj->clearForceList();
    }

    toolObj->setForceFeedbackStatic(force*500.0, true); 
}

} // imstk