/*=========================================================================

   Library: iMSTK

   Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
   & Imaging in Medicine, Rensselaer Polytechnic Institute.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0.txt

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

=========================================================================*/

#ifdef iMSTK_USE_OPENHAPTICS

#ifndef imstkHDAPIDeviceServer_h
#define imstkHDAPIDeviceServer_h

#include <vector>
#include <map>
#include <typeinfo>

// imstk
#include "imstkModule.h"
#include "imstkHDAPIDeviceClient.h"

///
/// \brief introduce the function
///
#include <GLFW/glfw3.h>  

namespace imstk
{
///
/// \class HDAPIDeviceServer
/// \brief Devices server using HDAPI
///
class HDAPIDeviceServer : public Module
{
public:

    ///
    /// \brief Constructor
    ///
    HDAPIDeviceServer() : Module("HDAPIDeviceServer")
    {}

    ///
    /// \brief Destructor
    ///
    virtual ~HDAPIDeviceServer() {}

    ///
    /// \brief Add device client
    ///
    void addDeviceClient(std::shared_ptr<HDAPIDeviceClient> client);

    ///
    /// \brief introduce the function XZH
    ///
    std::shared_ptr<HDAPIDeviceClient> getDeviceClient(int idx) { return m_deviceClients[idx]; }

    ///
    /// \brief introduce the function rad unit
    ///
    void setRollX(double x) { rollX = x; }
    double& getRollX(){ return rollX; }
    void setRollZ(double z) { rollZ = z; }
    double& getRollZ(){ return rollZ; }
    double& getTransY(){ return transY; }
    double& getTransX(){ return transX; }
    double& getTransZ(){ return transZ; }

protected:

    ///
    /// \brief Initialize the server module
    ///
    void initModule() override;

    ///
    /// \brief Run the server module
    ///
    void runModule() override;

    ///
    /// \brief Clean the server module
    ///
    void cleanUpModule() override;

private:

    std::vector<std::shared_ptr<HDAPIDeviceClient>> m_deviceClients; ///< list of OpenHaptics

    double rollX = 0, rollZ = 0;   ////< XZH Y can get from Omni
    double transY = 0;   ////< XZH introduce tool and back from (0-1)
    double transX = 0;
    double transZ = 0;
};
} // imstk

#endif // ifndef imstkHDAPIDeviceServer_h
#endif // ifdef iMSTK_USE_OMNI
