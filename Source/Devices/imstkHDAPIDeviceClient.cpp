/*=========================================================================

   Library: iMSTK

   Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
   & Imaging in Medicine, Rensselaer Polytechnic Institute.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0.txt

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

=========================================================================*/
#ifdef iMSTK_USE_OPENHAPTICS

#define USEOLDOMNI   ////< XZH  
#include "imstkHDAPIDeviceClient.h"
#include <HDU/hduVector.h>
#include <HDU/hduError.h>

#include "g3log/g3log.hpp"

namespace imstk
{
void
HDAPIDeviceClient::init()
{
    m_buttons = std::map < size_t, bool > { { 0, false }, { 1, false }, { 2, false }, { 3, false }};

    //flush error stack
    HDErrorInfo errorFlush;
    while (HD_DEVICE_ERROR(errorFlush = hdGetError())) {}

    // Open Device
    m_handle = hdInitDevice(this->getDeviceName().c_str());

    // If failed
    HDErrorInfo error;
    if (HD_DEVICE_ERROR(error = hdGetError()))
    {
        LOG(FATAL) << "Failed to initialize Phantom Omni " << this->getDeviceName();
        m_handle = -1;
        return;
    }

    // Enable forces
    hdEnable(HD_FORCE_OUTPUT);
    hdEnable(HD_FORCE_RAMPING);

    // Success
    LOG(INFO) << this->getDeviceName() << " successfully initialized.";
}

void
HDAPIDeviceClient::run()
{
    hdScheduleSynchronous(hapticCallback, this, HD_MAX_SCHEDULER_PRIORITY);
}

void
HDAPIDeviceClient::cleanUp()
{
    hdDisableDevice(m_handle);
}

bool
HDAPIDeviceClient::initDevice()
{
    filterSize = 300;
    numberOfForceInFilter = 0;
    arrForce.resize(300, Vec3d::Zero());
    arrForceStatic.resize(300, Vec3d::Zero());

    double scaleForce = 10.0;
    forceCoeff = 1 * scaleForce;
    springConstant = 0.9* scaleForce; // 5
    velocityResistance = 1 * scaleForce;
    hapticVelResistance = 1 * scaleForce;

    diffPos = Vec3d::Zero();
    diffPosPre = Vec3d::Zero();

      ////< XZH alignment
    vertexPos4 = Vec4d::Zero();
    vertexPos4.w() = 1;
    transMat.setIdentity();
    transMat(0, 0) = 1 * m_scaling;   transMat(0, 1) = 0;   transMat(0, 2) = 0;  transMat(0, 3) = 0;
    transMat(1, 0) = 0;    transMat(1, 1) = 0;   transMat(1, 2) = 1 * m_scaling;   transMat(1, 3) = 0;
    transMat(2, 0) = 0;   transMat(2, 1) = 1 * m_scaling;   transMat(2, 2) = 0;  transMat(2, 3) = 0;
    transMat(3, 0) = 0;   transMat(3, 1) = 0;   transMat(3, 2) = 0;  transMat(3, 3) = 1;
    transMatInv = transMat.inverse();
    return true;
}

void 
HDAPIDeviceClient::computeForce(const Vec3d& finalPos, bool& bHasCD)
{
    diffPos = (finalPos - hapticPos);
    Vec3d ddiffPos = diffPos - diffPosPre;

    Vec3d kx;
    if (ddiffPos.norm() > 2 * diffPosPre.norm())
        kx = springConstant *diffPos;
    else
        kx = springConstant *diffPos; // springConstant *diffPos;
    Vec3d test = kx;

    diffPosPre = diffPos;

    Vec3d velocity = hapticPos - pHapticPos;

    Vec3d hapticResistance;
    double HRcoeff = hapticVelResistance;

    double hapticResisFactor = velocity.norm() / 0.1; // 0.1
    if (hapticResisFactor > 1)
        HRcoeff *= hapticResisFactor;


    //hapticResistance.setValue(HRcoeff*velocity.x, HRcoeff*velocity.y, HRcoeff*velocity.z);
    hapticResistance = Vec3d(HRcoeff*velocity.x(), HRcoeff*velocity.y(), HRcoeff*velocity.z());

    double resistCoeff = velocityResistance;
    Vec3d resistance;
    //resistance.setValue(resistCoeff * ddiffPos.x, resistCoeff * ddiffPos.y, resistCoeff * ddiffPos.z);
    resistance = Vec3d(resistCoeff * ddiffPos.x(), resistCoeff * ddiffPos.y(), resistCoeff * ddiffPos.z());

    if (resistance.norm() > 1.4*kx.norm())  // 1.5
    {
        resistance = Vec3d::Zero(); // resistance.setValue(0, 0, 0);
    }

    //curForce = hapticResistance;
    //curForce = kx - hapticResistance;
    //curForce = kx;
    //force = kx - resistance;
    finalForce = kx - resistance; //  -hapticResistance;
    // test kxres
    
    if (finalForce.norm() > MaxOutputForce)
        finalForce = MaxOutputForce*finalForce.normalized();
    test = finalForce;

    numberOfForceInFilter++;
    if (!bHasCD) // if no collision
    {
        finalForce = Vec3d::Zero(); //  .setValue(0.0, 0.0, 0.0);
        for (int i = 0; i < filterSize; i++)
        {
            arrForce[i] = Vec3d::Zero();  // setValue(0.0, 0.0, 0.0);  //  set to 0
        }
        numberOfForceInFilter = 0;
    }

    for (int i = filterSize - 2; i >= 0; i--)
    {
        arrForce[i + 1] = arrForce[i];
    }
    arrForce[0] = finalForce;
    Vec3d kxres = finalForce;
    

    double sizecoeff = 1.0;
    if (numberOfForceInFilter < (filterSize / 4.0))
        sizecoeff = 1.0 / 4.0;
    else if (numberOfForceInFilter < (filterSize / 3.0))
        sizecoeff = 1.0 / 3.0;
    else if (numberOfForceInFilter < (filterSize / 2.0))
        sizecoeff = 1.0 / 2.0;

    finalForce = Vec3d::Zero(); //  .setValue(0.0, 0.0, 0.0);
    for (int i = 0; i<filterSize; i++)
    {
        finalForce = finalForce + arrForce[i] * std::sqrtf(float(filterSize - i) / (sizecoeff*filterSize));
        //	curForce = curForce + arrForce[i];
    }

    finalForce = (1.0 / double(filterSize)*(1.5 / (1 + 1.5 / double(filterSize)))) * finalForce;

    if (finalForce.norm()>MaxOutputForce)
        finalForce = MaxOutputForce*finalForce.normalized();

    curForce = finalForce;

    //first->setForce(forceCoeff*curForce);
    finalForce = forceCoeff*curForce;
    FILE *outForceFilter = fopen("G:/outforcefilter.dat", "a");
    Vec3d temp = forceCoeff*curForce;
    //if (temp.norm()>0.0001)
    //{
    fprintf(outForceFilter, "%f %f %f %f, kx: %f %f %f, kxrex: %f %f %f, finalpos: %f %f %f haptic: %f %f %f\n", temp.x(), temp.y(), temp.z(), temp.norm(), test.x(), test.y(), test.z(), kxres.x(), kxres.y(), kxres.z(), finalPos.x(), finalPos.y(), finalPos.z(), hapticPos.x(), hapticPos.y(), hapticPos.z());
    //}
    fclose(outForceFilter);
}

#ifdef USEOLDOMNI
HDCallbackCode HDCALLBACK
HDAPIDeviceClient::hapticCallback(void* pData)
{
    auto client = reinterpret_cast<HDAPIDeviceClient*>(pData);
    auto handle = client->m_handle;
    auto state = client->m_state;

      ////< XZH 
    double rollx = client->rollX;
    double rollz = client->rollZ;
    client->finalForceStatic = client->m_force;

    hdBeginFrame(handle);
    hdMakeCurrentDevice(handle);
    //hdSetDoublev(HD_CURRENT_FORCE, client->m_force.data());
    hdGetDoublev(HD_CURRENT_POSITION, state.pos);
    hdGetDoublev(HD_CURRENT_VELOCITY, state.vel);
    hdGetDoublev(HD_CURRENT_TRANSFORM, state.trans);
    hdGetIntegerv(HD_CURRENT_BUTTONS, &state.buttons);

    // swithch  Y and Z column-wise  there might be some problems in x-direction tuning in the future 0127/2018
    double transXZH[16];
    transXZH[0] = state.trans[0];    transXZH[4] = -state.trans[8];    transXZH[8] = -state.trans[4];    transXZH[12] = -state.trans[12];
    transXZH[1] = -state.trans[2];    transXZH[5] = state.trans[10];    transXZH[9] = state.trans[6];    transXZH[13] = state.trans[14];
    transXZH[2] = -state.trans[1];    transXZH[6] = state.trans[9];    transXZH[10] = state.trans[5];    transXZH[14] = state.trans[13];
    transXZH[3] = state.trans[3];    transXZH[7] = state.trans[11];    transXZH[11] = state.trans[7];    transXZH[15] = state.trans[15];
    double m_scaling = client->m_scaling*0.01*0.025;  // = 0.5;   ////< XZH  0.1;   ////< XZH  0.025;  ////< XZH  client->m_scaling; //  = 0.25;
    //// XZH 
    client->finalPos = client->m_finalPos;
    client->bHasCD = client->m_isCD;
    client->bIsInjection = client->m_isInjection;
    Vec3d pos; // swith Y and Z
    pos.x() = 0;// transXZH[12]; // state.pos[0];
    pos.y() = transXZH[13]; // state.pos[1];
    pos.z() = 0; // transXZH[14]; // state.pos[2];
	double xzhtmp =  0.0; // 0.03;
    pos = pos * m_scaling + Vec3d(0.018000, 0.100000+xzhtmp-0.020, 0.225000);   ////< XZH  02/07/2019 +Vec3d(3.2, 7.3, 6.0)*0.01*0.25 + Vec3d(-0.012000, 0.000000, 0.123000)*0.25; // Vec3d(0, 0, 0.05) 

      ////< XZH row pitch yaw
    double angx = rollx; // from keyboard   ////< XZH from Omni atan2(transXZH[6], transXZH[10]);
    double angy = atan2(-transXZH[2], sqrt(transXZH[6] * transXZH[6] + transXZH[10] * transXZH[10]));
    double angz = rollz; // from keyboard   ////< XZH from Omni atan2(transXZH[1], transXZH[0]);

      ////< XZH XYZ
    Eigen::Matrix3d rotMatrix, rotMatX, rotMatY, rotMatZ;
    rotMatX(0, 0) = 1;   rotMatX(0, 1) = 0; rotMatX(0, 2) = 0;
    rotMatX(1, 0) = 0;   rotMatX(1, 1) = cos(angx); rotMatX(1, 2) = -sin(angx);
    rotMatX(2, 0) = 0;   rotMatX(2, 1) = sin(angx); rotMatX(2, 2) = cos(angx);

    rotMatY(0, 0) = cos(angy);   rotMatY(0, 1) = 0; rotMatY(0, 2) = sin(angy);
    rotMatY(1, 0) = 0;   rotMatY(1, 1) = 1; rotMatY(1, 2) =0;
    rotMatY(2, 0) = -sin(angy);   rotMatY(2, 1) = 0; rotMatY(2, 2) = cos(angy);

    rotMatZ(0, 0) = cos(angz);   rotMatZ(0, 1) = -sin(angz); rotMatZ(0, 2) = 0;
    rotMatZ(1, 0) = sin(angz);   rotMatZ(1, 1) = cos(angz); rotMatZ(1, 2) = 0;
    rotMatZ(2, 0) = 0;   rotMatZ(2, 1) = 0; rotMatZ(2, 2) = 1;
    rotMatrix = rotMatX*rotMatY*rotMatZ;
    transXZH[0] = rotMatrix(0, 0);    transXZH[4] = rotMatrix(0, 1);    transXZH[8] = rotMatrix(0, 2);    transXZH[12] = 0;
    transXZH[1] = rotMatrix(1, 0);    transXZH[5] = rotMatrix(1, 1);    transXZH[9] = rotMatrix(1, 2);
    transXZH[2] = rotMatrix(2, 0);    transXZH[6] = rotMatrix(2, 1);    transXZH[10] = rotMatrix(2, 2);    transXZH[14] = 0;

    //  ////< XZH only Y
    //transXZH[0] =cos(angy);    transXZH[4] = 0;    transXZH[8] =sin(angy);    transXZH[12] =0;
    //transXZH[1] = 0;    transXZH[5] = 1;    transXZH[9] = 0;    
    //transXZH[2] = -sin(angy);    transXZH[6] = 0;    transXZH[10] = cos(angy);    transXZH[14] = 0;

    // XZH computer force should not call other functions
    client->pHapticPos = client->hapticPos;
    client->hapticPos = pos;
    client->diffPos = (client->finalPos - client->hapticPos);
    Vec3d ddiffPos = client->diffPos - client->diffPosPre;

    Vec3d kx;
    //if (ddiffPos.norm() > 2 * client->diffPosPre.norm())
    if (client->bIsInjection) // for injection solution procedure
        kx = 1.4* client->springConstant *client->diffPos;
    else // for CCD
        kx = client->springConstant *client->diffPos; // springConstant *diffPos;
    Vec3d test = kx;

    client->diffPosPre = client->diffPos;

    Vec3d velocity = client->hapticPos - client->pHapticPos;

    Vec3d hapticResistance;
    double HRcoeff = client->hapticVelResistance;

    double hapticResisFactor = velocity.norm() / 0.07; // 0.1
    if (hapticResisFactor > 1)
        HRcoeff *= hapticResisFactor;


    //hapticResistance.setValue(HRcoeff*velocity.x, HRcoeff*velocity.y, HRcoeff*velocity.z);
    hapticResistance = Vec3d(HRcoeff*velocity.x(), HRcoeff*velocity.y(), HRcoeff*velocity.z());

    double resistCoeff = client->velocityResistance;
    Vec3d resistance;
    //resistance.setValue(resistCoeff * ddiffPos.x, resistCoeff * ddiffPos.y, resistCoeff * ddiffPos.z);
    resistance = Vec3d(resistCoeff * ddiffPos.x(), resistCoeff * ddiffPos.y(), resistCoeff * ddiffPos.z());

    //if (resistance.norm() > 1.4*kx.norm())  // 1.5
    //{
    //    resistance = Vec3d::Zero(); // resistance.setValue(0, 0, 0);
    //}

    //curForce = hapticResistance;
    //curForce = kx - hapticResistance;
    //curForce = kx;
    //force = kx - resistance;
    client->finalForce = kx - resistance - hapticResistance;
    // test kxres

    if (client->finalForce.norm() > client->MaxOutputForce)
        client->finalForce = client->MaxOutputForce*client->finalForce.normalized();
    test = client->finalForce;

    client->numberOfForceInFilter++;
    //printf("isCD: %d\n", client->bHasCD);
    //if (0) // if no collision  also set it to true, then will never from a value drop to 0 ,but this is not completely correct, not a good way
    if (!client->bHasCD) // if no collision
    {
        client->finalForce = Vec3d::Zero(); //  .setValue(0.0, 0.0, 0.0);
        for (int i = 0; i < client->filterSize; i++)
        {
            client->arrForce[i] = Vec3d::Zero();  // setValue(0.0, 0.0, 0.0);  //  set to 0
        }
        client->numberOfForceInFilter = 0;
    }
    else
    {
        //printf("has CD...  ");
    }

    for (int i = client->filterSize - 2; i >= 0; i--)
    {
        client->arrForce[i + 1] = client->arrForce[i];
    }
    client->arrForce[0] = client->finalForce;
    Vec3d kxres = client->finalForce;


    double sizecoeff = 1.0;
    if (client->numberOfForceInFilter < (client->filterSize / 4.0))
        sizecoeff = 1.0 / 4.0;
    else if (client->numberOfForceInFilter < (client->filterSize / 3.0))
        sizecoeff = 1.0 / 3.0;
    else if (client->numberOfForceInFilter < (client->filterSize / 2.0))
        sizecoeff = 1.0 / 2.0;

    client->finalForce = Vec3d::Zero(); //  .setValue(0.0, 0.0, 0.0);
    for (int i = 0; i<client->filterSize; i++)
    {
        client->finalForce = client->finalForce + client->arrForce[i] * std::sqrtf(float(client->filterSize - i) / (sizecoeff*client->filterSize));
        //	curForce = curForce + arrForce[i];
    }

    client->finalForce = (1.0 / double(client->filterSize)*(1.5 / (1 + 1.5 / double(client->filterSize)))) * client->finalForce;

    if (client->finalForce.norm()>client->MaxOutputForce)
        client->finalForce = client->MaxOutputForce*client->finalForce.normalized();

    client->curForce = client->finalForce;

    //first->setForce(forceCoeff*curForce);
    client->finalForce = client->forceCoeff*client->curForce;

    //// iMSTK to Omni
    Vec3d forceTmp;
    forceTmp.x() = -client->finalForce.x();
    forceTmp.y() = client->finalForce.z();
    forceTmp.z() = client->finalForce.y();

    //client->vertexPos4.w() = 1;
    //client->vertexPos4.x() = client->finalForce.x();
    //client->vertexPos4.y() = client->finalForce.y();
    //client->vertexPos4.z() = client->finalForce.z();
    //client->vertexPos4.applyOnTheLeft(client->transMatInv);
    //client->finalForce.x() = client->vertexPos4.x();
    //client->finalForce.y() = client->vertexPos4.y();
    //client->finalForce.z() = client->vertexPos4.z();
    if (client->m_isStatic)
    {
        for (int i = client->filterSize - 2; i >= 0; i--)
        {
            client->arrForceStatic[i + 1] = client->arrForceStatic[i];
        }
        client->arrForceStatic[0] = client->finalForceStatic;

        double sizecoeff = 1.0;
        if (client->numberOfForceInFilterStatic < (client->filterSize / 4.0))
            sizecoeff = 1.0 / 4.0;
        else if (client->numberOfForceInFilterStatic < (client->filterSize / 3.0))
            sizecoeff = 1.0 / 3.0;
        else if (client->numberOfForceInFilterStatic < (client->filterSize / 2.0))
            sizecoeff = 1.0 / 2.0;

        client->finalForceStatic = Vec3d::Zero(); //  .setValue(0.0, 0.0, 0.0);
        for (int i = 0; i < client->filterSize; i++)
        {
            client->finalForceStatic = client->finalForceStatic + client->arrForceStatic[i] * std::sqrtf(float(client->filterSize - i) / (sizecoeff*client->filterSize));
        }

        client->finalForceStatic = (1.0 / double(client->filterSize)*(1.5 / (1 + 1.5 / double(client->filterSize)))) * client->finalForceStatic;

        forceTmp.x() = -client->finalForceStatic.x();
        forceTmp.y() = client->finalForceStatic.z();
        forceTmp.z() = client->finalForceStatic.y();
    }
    Vec3d forceOmni;
    forceOmni.x() = -forceTmp.x();
    forceOmni.y() = forceTmp.z();
    forceOmni.z() = forceTmp.y();
    client->finalForce.x() = 0; //  = forceTmp.x(); // forceOmni;
    client->finalForce.y() = 0;
    if (forceTmp.y()<0)
    {
        client->finalForce.z() = -forceTmp.norm(); 
    }
    else
    {
        client->finalForce.z() = forceTmp.norm();
    }
    hdSetDoublev(HD_CURRENT_FORCE, client->finalForce.data());
    //printf("force %f %f %f \n", forceOmni.x(), forceOmni.y(), forceOmni.z());
    //printf("filter size: %d isCD %d final %f %f %f, pHaptic %f %f %f haptic %f %f %f force %f\n", client->filterSize, client->bHasCD, client->finalPos.x(), client->finalPos.y(), client->finalPos.z(), client->pHapticPos.x(), client->pHapticPos.y(), client->pHapticPos.z(), client->hapticPos.x(), client->hapticPos.y(), client->hapticPos.z(), client->finalForce.norm());
    //printf("%f\n",  client->finalForce.norm());
    //FILE *outForceFilter = fopen("I:/outForce.dat", "a");
    //Vec3d temp = client->finalForce;
    ////printf("%f\n", temp.norm());
    ////if (temp.norm()>0.0001)
    ////{
    //fprintf(outForceFilter, "%f %f %f %f\n", temp.x(), temp.y(), temp.z(), temp.norm());
    ////}
    //fclose(outForceFilter);
    // XZH END

    hdEndFrame(handle);

    client->m_position= pos;   ////< XZH  << state.pos[0], state.pos[1], state.pos[2];
    client->m_velocity << state.vel[0], state.vel[1], state.vel[2];
    client->m_orientation = (Eigen::Affine3d(Eigen::Matrix4d(transXZH))).rotation();   ////< XZH  client->m_orientation = (Eigen::Affine3d(Eigen::Matrix4d(state.trans))).rotation();
    client->m_buttons[0] = state.buttons & HD_DEVICE_BUTTON_1;
    client->m_buttons[1] = state.buttons & HD_DEVICE_BUTTON_2;
    client->m_buttons[2] = state.buttons & HD_DEVICE_BUTTON_3;
    client->m_buttons[3] = state.buttons & HD_DEVICE_BUTTON_4;

    if (client->m_buttons[1])
    {
        FILE* devFile = fopen("i:/dev.dat", "a");
        fprintf(devFile, "Pos: %f %f %f\n",
            state.pos[0], state.pos[1], state.pos[2]
            );
        //fprintf(devFile, "Pos: %f %f %f:  \n%f %f %f\n%f %f %f\n%f %f %f\n",
        //state.pos[0], state.pos[1], state.pos[2],
        //state.trans[0], state.trans[4], state.trans[8],
        //state.trans[1], state.trans[5], state.trans[9],
        //state.trans[2], state.trans[6], state.trans[10]
        //);
        fclose(devFile);
    }

    return HD_CALLBACK_DONE;
}
#else
HDCallbackCode HDCALLBACK
HDAPIDeviceClient::hapticCallback(void* pData)
{
    auto client = reinterpret_cast<HDAPIDeviceClient*>(pData);
    auto handle = client->m_handle;
    auto state = client->m_state;

    hdBeginFrame(handle);
    hdMakeCurrentDevice(handle);
    //hdSetDoublev(HD_CURRENT_FORCE, client->m_force.data());
    hdGetDoublev(HD_CURRENT_POSITION, state.pos);
    hdGetDoublev(HD_CURRENT_VELOCITY, state.vel);
    hdGetDoublev(HD_CURRENT_TRANSFORM, state.trans);
    hdGetIntegerv(HD_CURRENT_BUTTONS, &state.buttons);
    //if (state.buttons==0)
    //{
    //    FILE* devFile = fopen("i:/dev.dat", "a");
    //    fprintf(devFile, "but: %d Pos: %f %f %f\n",
    //        state.buttons, state.pos[0], state.pos[1], state.pos[2]
    //        );
    //    //fprintf(devFile, "Pos: %f %f %f:  \n%f %f %f\n%f %f %f\n%f %f %f\n",
    //    //    state.pos[0], state.pos[1], state.pos[2],
    //    //    state.trans[0], state.trans[4], state.trans[8],
    //    //    state.trans[1], state.trans[5], state.trans[9],
    //    //    state.trans[2], state.trans[6], state.trans[10]
    //    //    );
    //    fclose(devFile);
    //}
    //return 0;
    // swithch  Y and Z column-wise  there might be some problems in x-direction tuning in the future 0127/2018
    double transXZH[16];
    transXZH[0] = state.trans[0];    transXZH[4] = -state.trans[8];    transXZH[8] = -state.trans[4];    transXZH[12] = -state.trans[12];
    transXZH[1] = -state.trans[2];    transXZH[5] = state.trans[10];    transXZH[9] = state.trans[6];    transXZH[13] = state.trans[14];
    transXZH[2] = -state.trans[1];    transXZH[6] = state.trans[9];    transXZH[10] = state.trans[5];    transXZH[14] = state.trans[13];
    transXZH[3] = state.trans[3];    transXZH[7] = state.trans[11];    transXZH[11] = state.trans[7];    transXZH[15] = state.trans[15];
    //// original
    //transXZH[0] = state.trans[0];    transXZH[4] = state.trans[4];    transXZH[8] = state.trans[8];    transXZH[12] = state.trans[12];
    //transXZH[1] = state.trans[1];    transXZH[5] = state.trans[5];    transXZH[9] = state.trans[9];    transXZH[13] = state.trans[13];
    //transXZH[2] = state.trans[2];    transXZH[6] = state.trans[6];    transXZH[10] = state.trans[10];    transXZH[14] = state.trans[14];
    //transXZH[3] = state.trans[3];    transXZH[7] = state.trans[7];    transXZH[11] = state.trans[11];    transXZH[15] = state.trans[15];
    double m_scaling = client->m_scaling; //  = 0.25;
    // axis alignment
    //// XZH 
    client->finalPos = client->m_finalPos;
    client->bHasCD = client->m_isCD;
    client->bIsInjection = client->m_isInjection;
    Vec3d pos; // swith Y and Z
    pos.x() = transXZH[12]; // state.pos[0];
    pos.y() = transXZH[13]; // state.pos[1];
    pos.z() = transXZH[14]; // state.pos[2];
    //client->vertexPos4.x() = pos.x();
    //client->vertexPos4.y() = pos.y();
    //client->vertexPos4.z() = pos.z();
    //client->vertexPos4.applyOnTheLeft(client->transMat);
    //pos.x() = client->vertexPos4.x();
    //pos.y() = client->vertexPos4.y();
    //pos.z() = client->vertexPos4.z();

    Vec3d vel;
    vel.x() = -state.vel[0];
    vel.y() = state.vel[2]; // 2
    vel.z() = state.vel[1]; // 1
    //client->vertexPos4.x() = vel.x();
    //client->vertexPos4.y() = vel.y();
    //client->vertexPos4.z() = vel.z();
    //client->vertexPos4.applyOnTheLeft(client->transMat);
    //vel.x() = client->vertexPos4.x();
    //vel.y() = client->vertexPos4.y();
    //vel.z() = client->vertexPos4.z();
    //vel = transMat*vel;
    //Quatd m_rotationOffset = Quatd::Identity();
    //Vec3d m_translationOffset = Vec3d::Zero();

    // Apply inverse if needed
    if (0x00 & 0x01)
    {
        pos[0] = -pos[0];
    }
    if (0x00 & 0x02)
    {
        pos[1] = -pos[1];
    }
    if (0x00 & 0x04)
    {
        pos[2] = -pos[2];
    }
    //printf("finalPos: %f %f %f is CD %d\n", client->m_finalPos.x(), client->m_finalPos.y(), client->m_finalPos.z(), client->m_isCD);
    //// Apply Offsets
    pos = pos * m_scaling +Vec3d(4.2,4.3,2.0)*0.01 ; // old way //  m_rotationOffset * pos * m_scaling + m_translationOffset;
    //client->pHapticPos = client->hapticPos;
    //client->hapticPos = pos;
    //client->computeForce(client->m_finalPos, client->m_isCD);
    //hdSetDoublev(HD_CURRENT_FORCE, client->finalForce.data());
    //// XZH END
    // XZH computer force should not call other functions
    client->pHapticPos = client->hapticPos;
    client->hapticPos = pos;
    client->diffPos = (client->finalPos - client->hapticPos);
    Vec3d ddiffPos = client->diffPos - client->diffPosPre;

    Vec3d kx;
    //if (ddiffPos.norm() > 2 * client->diffPosPre.norm())
    if (client->bIsInjection) // for injection solution procedure
        kx =1.4* client->springConstant *client->diffPos;
    else // for CCD
        kx = client->springConstant *client->diffPos; // springConstant *diffPos;
    Vec3d test = kx;

    client->diffPosPre = client->diffPos;

    Vec3d velocity = client->hapticPos - client->pHapticPos;

    Vec3d hapticResistance;
    double HRcoeff = client->hapticVelResistance;

    double hapticResisFactor = velocity.norm() / 0.07; // 0.1
    if (hapticResisFactor > 1)
        HRcoeff *= hapticResisFactor;


    //hapticResistance.setValue(HRcoeff*velocity.x, HRcoeff*velocity.y, HRcoeff*velocity.z);
    hapticResistance = Vec3d(HRcoeff*velocity.x(), HRcoeff*velocity.y(), HRcoeff*velocity.z());

    double resistCoeff = client->velocityResistance;
    Vec3d resistance;
    //resistance.setValue(resistCoeff * ddiffPos.x, resistCoeff * ddiffPos.y, resistCoeff * ddiffPos.z);
    resistance = Vec3d(resistCoeff * ddiffPos.x(), resistCoeff * ddiffPos.y(), resistCoeff * ddiffPos.z());

    //if (resistance.norm() > 1.4*kx.norm())  // 1.5
    //{
    //    resistance = Vec3d::Zero(); // resistance.setValue(0, 0, 0);
    //}

    //curForce = hapticResistance;
    //curForce = kx - hapticResistance;
    //curForce = kx;
    //force = kx - resistance;
    client->finalForce = kx - resistance - hapticResistance;
    // test kxres

    if (client->finalForce.norm() > client->MaxOutputForce)
        client->finalForce = client->MaxOutputForce*client->finalForce.normalized();
    test = client->finalForce;

    client->numberOfForceInFilter++;
    //printf("isCD: %d\n", client->bHasCD);
    //if (0) // if no collision  also set it to true, then will never from a value drop to 0 ,but this is not completely correct, not a good way
    if (!client->bHasCD) // if no collision
    {
        client->finalForce = Vec3d::Zero(); //  .setValue(0.0, 0.0, 0.0);
        for (int i = 0; i < client->filterSize; i++)
        {
            client->arrForce[i] = Vec3d::Zero();  // setValue(0.0, 0.0, 0.0);  //  set to 0
        }
        client->numberOfForceInFilter = 0;
    }

    for (int i = client->filterSize - 2; i >= 0; i--)
    {
        client->arrForce[i + 1] = client->arrForce[i];
    }
    client->arrForce[0] = client->finalForce;
    Vec3d kxres = client->finalForce;


    double sizecoeff = 1.0;
    if (client->numberOfForceInFilter < (client->filterSize / 4.0))
        sizecoeff = 1.0 / 4.0;
    else if (client->numberOfForceInFilter < (client->filterSize / 3.0))
        sizecoeff = 1.0 / 3.0;
    else if (client->numberOfForceInFilter < (client->filterSize / 2.0))
        sizecoeff = 1.0 / 2.0;

    client->finalForce = Vec3d::Zero(); //  .setValue(0.0, 0.0, 0.0);
    for (int i = 0; i<client->filterSize; i++)
    {
        client->finalForce = client->finalForce + client->arrForce[i] * std::sqrtf(float(client->filterSize - i) / (sizecoeff*client->filterSize));
        //	curForce = curForce + arrForce[i];
    }

    client->finalForce = (1.0 / double(client->filterSize)*(1.5 / (1 + 1.5 / double(client->filterSize)))) * client->finalForce;

    if (client->finalForce.norm()>client->MaxOutputForce)
        client->finalForce = client->MaxOutputForce*client->finalForce.normalized();

    client->curForce = client->finalForce;

    //first->setForce(forceCoeff*curForce);
    client->finalForce = client->forceCoeff*client->curForce;

    //// iMSTK to Omni
    Vec3d forceTmp;
    forceTmp.x() = -client->finalForce.x();
    forceTmp.y() = client->finalForce.z();
    forceTmp.z() = client->finalForce.y();
    
    //client->vertexPos4.w() = 1;
    //client->vertexPos4.x() = client->finalForce.x();
    //client->vertexPos4.y() = client->finalForce.y();
    //client->vertexPos4.z() = client->finalForce.z();
    //client->vertexPos4.applyOnTheLeft(client->transMatInv);
    //client->finalForce.x() = client->vertexPos4.x();
    //client->finalForce.y() = client->vertexPos4.y();
    //client->finalForce.z() = client->vertexPos4.z();
    if (client->m_isStatic)
    {
        forceTmp.x() = -client->m_force.x();
        forceTmp.y() = client->m_force.z();
        forceTmp.z() = client->m_force.y();
    }
    client->finalForce = forceTmp;
    hdSetDoublev(HD_CURRENT_FORCE, client->finalForce.data());
    //printf("filter size: %d isCD %d final %f %f %f, pHaptic %f %f %f haptic %f %f %f force %f\n", client->filterSize, client->bHasCD, client->finalPos.x(), client->finalPos.y(), client->finalPos.z(), client->pHapticPos.x(), client->pHapticPos.y(), client->pHapticPos.z(), client->hapticPos.x(), client->hapticPos.y(), client->hapticPos.z(), client->finalForce.norm());
    //printf("%f\n",  client->finalForce.norm());
    //FILE *outForceFilter = fopen("G:/outforcefilter.dat", "a");
    Vec3d temp = client->finalForce;
    //printf("%f\n", temp.norm());
    ////if (temp.norm()>0.0001)
    ////{
    //fprintf(outForceFilter, "%f\n",  temp.norm());
    ////}
    //fclose(outForceFilter);
    // XZH END
    hdEndFrame(handle);

    client->m_position = pos; //  old way  << state.pos[0], state.pos[1], state.pos[2]; // pos; //  old way 
    client->m_velocity = vel; // old way << state.vel[0], state.vel[1], state.vel[2]; // vel; // old way 

    //Eigen::Matrix4d matTmp = Eigen::Matrix4d(state.trans);
    //matTmp = transMat*matTmp;
    //Eigen::Affine3d ttmp = Eigen::Affine3d(matTmp);
    //Eigen::Matrix3d rotMat; //  = r.toRotationMatrix();
    //rotMat(0, 0) = matTmp(0, 0);   rotMat(0, 1) = matTmp(0, 1);   rotMat(0, 2) = matTmp(0, 2);  
    //rotMat(1, 0) = matTmp(1, 0);    rotMat(1, 1) = matTmp(1, 1);    rotMat(1, 2) = matTmp(1, 2);  
    //rotMat(2, 0) = matTmp(2, 0);   rotMat(2, 1) = matTmp(2, 1);   rotMat(2, 2) = matTmp(2, 2);  
    //Quatd m_orientation2 = Quatd::Identity();
    //m_orientation2 = Quatd(rotMat);
    //Eigen::Matrix4d matTmp2 = matTmp; //  Eigen::Matrix4d(state.trans);
    //matTmp2(0, 3) = 0; matTmp2(1, 3) = 0; matTmp2(2, 3) = 0;
    //matTmp2(3, 0) = 0; matTmp2(3, 1) = 0; matTmp2(3, 2) = 0; matTmp2(3, 3) = 1;
    //Eigen::Affine3d ttmp2 = (Eigen::Affine3d(matTmp2)); //  Eigen::Matrix4d(state.trans)));
    //m_orientation2 = ttmp2.rotation();
    client->m_orientation = (Eigen::Affine3d(Eigen::Matrix4d(transXZH))).rotation(); // (Eigen::Affine3d(Eigen::Matrix4d(state.trans))).rotation(); //  (Eigen::Affine3d(matTmp)).rotation(); // transMat*; // m_orientationTmp; // (Eigen::Affine3d(transMat* Eigen::Matrix4d(state.trans))).rotation();
    client->m_buttons[0] = state.buttons & HD_DEVICE_BUTTON_1;
    client->m_buttons[1] = state.buttons & HD_DEVICE_BUTTON_2;
    client->m_buttons[2] = state.buttons & HD_DEVICE_BUTTON_3;
    client->m_buttons[3] = state.buttons & HD_DEVICE_BUTTON_4;

    if (client->m_buttons[1])
    {
    FILE* devFile = fopen("i:/dev.dat", "a");
    fprintf(devFile, "Pos: %f %f %f\n",
        state.pos[0], state.pos[1], state.pos[2]
        );
    //fprintf(devFile, "Pos: %f %f %f:  \n%f %f %f\n%f %f %f\n%f %f %f\n",
    //state.pos[0], state.pos[1], state.pos[2],
    //state.trans[0], state.trans[4], state.trans[8],
    //state.trans[1], state.trans[5], state.trans[9],
    //state.trans[2], state.trans[6], state.trans[10]
    //);
    fclose(devFile);
    }

    //printf("Omni Button: %d %d %d %d\n", client->m_buttons[0], client->m_buttons[1], client->m_buttons[2], client->m_buttons[3]);

    return HD_CALLBACK_DONE;
}
#endif

} // imstk
#endif // ifdef iMSTK_USE_OPENHAPTICS
