/*=========================================================================

   Library: iMSTK

   Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
   & Imaging in Medicine, Rensselaer Polytechnic Institute.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0.txt

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

=========================================================================*/

#ifdef iMSTK_USE_OPENHAPTICS

#ifndef imstkHDAPIDeviceClient_h
#define imstkHDAPIDeviceClient_h

#include <map>

#include "imstkDeviceClient.h"

#include <HD/hd.h>

#include <memory>

namespace imstk
{
struct HD_state
{
    HDdouble pos[3];
    HDdouble vel[3];
    HDdouble trans[16];
    HDint buttons;
};

///
/// \class HDAPIDeviceClient
/// \brief Subclass of DeviceClient for phantom omni
///
class HDAPIDeviceClient : public DeviceClient
{
public:

    ///
    /// \brief Constructor/Destructor
    ///
    HDAPIDeviceClient(std::string name) : DeviceClient(name, "localhost"){}
    virtual ~HDAPIDeviceClient(){}

    ///
    /// \brief introduce the function rad unit
    ///
    void setRollX(double x) { rollX = x; }
    double& getRollX(){ return rollX; }
    void setRollZ(double z) { rollZ = z; }
    double& getRollZ(){ return rollZ; }

protected:

    friend class HDAPIDeviceServer;

    ///
    /// \brief Initialize the phantom omni device
    ///
    void init();

    ///
    /// \brief Use callback to get tracking data from phantom omni
    ///
    void run();

    ///
    /// \brief Closes the phantom omni device
    ///
    void cleanUp();

    ///
    /// \brief XZH force feedback
    ///
    bool initDevice();
    void computeForce(const Vec3d& finalPos, bool& bHasCD);

private:
    ///
    /// \brief Phantom omni device api callback
    ///
    static HDCallbackCode HDCALLBACK hapticCallback(void* pData);

    HHD m_handle;     ///< device handle
    HD_state m_state; ///< device reading state

    // XZH force feedback
    unsigned char m_invertFlags = 0x00;
    const double MaxOutputForce = 20;
    Vec3d finalForce;// raw force, without filtering and other process
    Vec3d finalForceStatic;// raw force, without filtering and other process
    Vec3d curForce;// current force to be send to haptic device
    Vec3d preForce ;// previous force sended to haptic device
    double forceCoeff=1;

    Vec3d finalPos;
    Vec3d diffPos ;
    Vec3d diffPosPre ;
    double springConstant=5;
    Vec3d pHapticPos;
    Vec3d hapticPos ;
    double hapticVelResistance=1;
    double velocityResistance=1;
    bool bHasCD = false;
    bool bIsInjection = false;

    int filterSize=100;
    int numberOfForceInFilter = 0;
    int numberOfForceInFilterStatic = 0;
    std::vector<Vec3d> arrForce; // force filter
    std::vector<Vec3d> arrForceStatic; // force filter

    // Omni iMSTK alignment
    Vec4d vertexPos4=Vec4d::Zero();
    Eigen::Matrix4d transMat = Eigen::Matrix4d::Identity();
    Eigen::Matrix4d transMatInv = Eigen::Matrix4d::Identity(); //= transMat.inverse();
    
    double rollX = 0, rollZ = 0;   ////< XZH Y can get from Omni
};
}

#endif // ifndef imstkHDAPIDeviceClient_h
#endif // ifdef iMSTK_USE_OMNI
