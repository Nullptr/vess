/*=========================================================================

   Library: iMSTK

   Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
   & Imaging in Medicine, Rensselaer Polytechnic Institute.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0.txt

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

=========================================================================*/

#include "imstkVulkanInteractorStyle.h"

#include "imstkVulkanViewer.h"
#include "imstkSimulationManager.h"

//// XZH
#include "imstkHDAPIDeviceServer.h"

#define PHANTOM   ////< XZH  
//#define DEVICEINDEX 1  // Custome device
namespace imstk
{
VulkanInteractorStyle::VulkanInteractorStyle()
{
}

void
VulkanInteractorStyle::setWindow(GLFWwindow * window, VulkanViewer * viewer)
{
    m_window = window;
    m_viewer = viewer;

    m_stopWatch.start();
    glfwSetWindowUserPointer(window, (void *)this);

    glfwSetKeyCallback(m_window, VulkanInteractorStyle::OnCharInterface);
    glfwSetMouseButtonCallback(m_window, VulkanInteractorStyle::OnMouseButtonInterface);
    glfwSetCursorPosCallback(m_window, VulkanInteractorStyle::OnMouseMoveInterface);
    glfwSetScrollCallback(m_window, VulkanInteractorStyle::OnMouseWheelInterface);
    glfwSetWindowSizeCallback(m_window, VulkanInteractorStyle::OnWindowResizeInterface);
    glfwSetFramebufferSizeCallback(m_window, VulkanInteractorStyle::OnFramebuffersResizeInterface);
}

void
VulkanInteractorStyle::OnCharInterface(GLFWwindow * window, int keyID, int code, int type, int extra)
{
    auto style = (VulkanInteractorStyle *)glfwGetWindowUserPointer(window);
    style->OnChar(keyID, type);
}

void
VulkanInteractorStyle::OnMouseButtonInterface(GLFWwindow * window, int buttonID, int type, int extra)
{
    auto style = (VulkanInteractorStyle *)glfwGetWindowUserPointer(window);

    switch (buttonID)
    {
    case GLFW_MOUSE_BUTTON_LEFT:
        if (type == GLFW_PRESS)
        {
            style->OnLeftButtonDown();
        }
        else if (type == GLFW_RELEASE)
        {
            style->OnLeftButtonUp();
        }
        break;
    case GLFW_MOUSE_BUTTON_RIGHT:
        if (type == GLFW_PRESS)
        {
            style->OnRightButtonDown();
        }
        else if (type == GLFW_RELEASE)
        {
            style->OnRightButtonUp();
        }
        break;
    case GLFW_MOUSE_BUTTON_MIDDLE:
        if (type == GLFW_PRESS)
        {
            style->OnMiddleButtonDown();
        }
        else if (type == GLFW_RELEASE)
        {
            style->OnMiddleButtonUp();
        }
        break;
    }
}

void
VulkanInteractorStyle::OnTimer()
{
    // Call custom function if exists, and return
    // if it returned `override=true`
    if(m_onTimerFunction &&
       m_onTimerFunction(this))
    {
        return;
    }

      ////< XZH 
    auto viewer = m_simManager->getViewer();
    auto scene = m_simManager->getActiveScene();

    //// XZH
#ifdef PHANTOM
    auto devServer = this->m_simManager->getModule("HDAPIDeviceServer");
    std::shared_ptr<HDAPIDeviceServer> omniServer = std::dynamic_pointer_cast<HDAPIDeviceServer>(devServer);
    std::shared_ptr<HDAPIDeviceClient> omniClient;
#else
    auto devServer = this->m_simManager->getModule("CustomDeviceServer");
    std::shared_ptr<CustomDeviceServer> omniServer = std::dynamic_pointer_cast<CustomDeviceServer>(devServer);
    std::shared_ptr<CustomDeviceClient> omniClient;
#endif
    
    if (omniServer)
    {
        omniClient = omniServer->getDeviceClient(0);
        auto& rollx = omniServer->getRollX();
        auto& rollz = omniServer->getRollZ();
        auto& transy = omniServer->getTransY();
        auto& transx = omniServer->getTransX();
        auto& transz = omniServer->getTransZ();

        double speed = 1.0;
        double angdt = imstk::PI / 3600; // 3.1415926 / 360;
        double disdt = 0.0001; // 0.1* 0.01*0.01;
        auto& testLen = scene->getTestLen();
        if (glfwGetKey(m_viewer->m_window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
        {
            speed = 20;
        }

        ////< XZH X  KP is the right key
        if (glfwGetKey(m_viewer->m_window, GLFW_KEY_KP_1) == GLFW_PRESS) 
        {
            rollx -= angdt*speed;
        }
        else
        {
            rollx -= 0;
        }
        if (glfwGetKey(m_viewer->m_window, GLFW_KEY_KP_4) == GLFW_PRESS)
        {
            rollx += angdt*speed;
        }
        else
        {
            rollx += 0;
        }

        if (glfwGetKey(m_viewer->m_window, GLFW_KEY_KP_3) == GLFW_PRESS)
        {
            rollz -= angdt*speed;
        }
        else
        {
            rollz -= 0;
        }
        if (glfwGetKey(m_viewer->m_window, GLFW_KEY_KP_6) == GLFW_PRESS)
        {
            rollz += angdt*speed;
        }
        else
        {
            rollz += 0;
        }

        if (glfwGetKey(m_viewer->m_window, GLFW_KEY_KP_2) == GLFW_PRESS)
        {
            //if (transy < 0) transy = 0;
            //else if (transy > 1) transy = 1;
            //else 
                transy -= disdt*speed*4;
        }
        else
        {
            transy -= 0;
        }

        if (glfwGetKey(m_viewer->m_window, GLFW_KEY_KP_5) == GLFW_PRESS)
        {
            //if (transy < 0) transy = 0;
            //else if (transy > 1) transy = 1;
            //else 
                transy += disdt*speed*4;
        }
        else
        {
            transy += 0;
        }



        if (glfwGetKey(m_viewer->m_window, GLFW_KEY_1) == GLFW_PRESS)
        {
            transx -= disdt*speed * 1;
        }
        else
        {
            transx -= 0;
        }
        if (glfwGetKey(m_viewer->m_window, GLFW_KEY_2) == GLFW_PRESS)
        {
            transx += disdt*speed * 1;
        }
        else
        {
            transx += 0;
        }

        if (glfwGetKey(m_viewer->m_window, GLFW_KEY_3) == GLFW_PRESS)
        {
            transz -= disdt*speed * 1;
        }
        else
        {
            transz -= 0;
        }
        if (glfwGetKey(m_viewer->m_window, GLFW_KEY_4) == GLFW_PRESS)
        {
            transz += disdt*speed * 1;
        }
        else
        {
            transz += 0;
        }

        if (glfwGetKey(m_viewer->m_window, GLFW_KEY_0) == GLFW_PRESS)
        {
            transy = 0;
            transx = 0;
            transz = 0;
            rollx = 0;
            rollz = 0;
        }

        if (glfwGetKey(m_viewer->m_window, GLFW_KEY_PAGE_UP) == GLFW_PRESS)
        {
            testLen += 0.003;
        }
        else
        {
            testLen += 0;
        }
        if (glfwGetKey(m_viewer->m_window, GLFW_KEY_PAGE_DOWN) == GLFW_PRESS)
        {
            testLen -= 0.003;
        }
        else
        {
            testLen -= 0;
        }

        omniClient->setRollX(rollx);
        omniClient->setRollZ(rollz);
        omniClient->setTransY(transy);
        omniClient->setTransX(transx);
        omniClient->setTransZ(transz);

          ////< XZH update scene
        auto& rx = scene->getTestRotX(); rx = rollx;
        auto& rz = scene->getTestRotZ(); rz = rollz;
        auto& tx = scene->getTestTranX(); tx = transx;
        auto& ty = scene->getTestTranY(); ty = transy;
        auto& tz = scene->getTestTranZ(); tz = transz;
    }
}

void
VulkanInteractorStyle::OnChar(int keyID, int type)
{
    char key = std::tolower((char)keyID);
    const float gap = 0.1;  ////< XZH 
    if (type != GLFW_PRESS)
    {
        return;
    }

    // Call custom function if exists, and return
    // if it returned `override=true`
    if(m_onCharFunctionMap.count(key) &&
       m_onCharFunctionMap.at(key) &&
       m_onCharFunctionMap.at(key)(this))
    {
        return;
    }

    SimulationStatus status = m_simManager->getStatus();
    auto viewer = m_simManager->getViewer();

    //// XZH
#ifdef PHANTOM
    auto devServer = this->m_simManager->getModule("HDAPIDeviceServer");
    std::shared_ptr<HDAPIDeviceServer> omniServer = std::dynamic_pointer_cast<HDAPIDeviceServer>(devServer);
    std::shared_ptr<HDAPIDeviceClient> omniClient;
#else
    auto devServer = this->m_simManager->getModule("CustomDeviceServer");
    std::shared_ptr<CustomDeviceServer> omniServer = std::dynamic_pointer_cast<CustomDeviceServer>(devServer);
    std::shared_ptr<CustomDeviceClient> omniClient;
#endif
    

    auto scene = m_simManager->getActiveScene();
    if (omniServer)
    {
        omniClient = omniServer->getDeviceClient(0);
        //auto& rollx = omniServer->getRollX();
        //auto& rollz = omniServer->getRollZ();
        //auto& transy = omniServer->getTransY();
        //auto& transx = omniServer->getTransX();
        //auto& transz = omniServer->getTransZ();
        auto& toolIdx = omniClient->getToolIndex();
        auto& cameraFlag = omniClient->getCameraFlag();
        bool flagPBD = scene->getIsPBD();
		bool& flagLoadDissect= scene->getIsLoadDissect();
		bool flagDissect = flagLoadDissect;
        int& flagXZ = scene->getTestFlagXZ();

        double speed = 1.0;
        double angdt = imstk::PI / 3600; // 3.1415926 / 360;
        if (glfwGetKey(m_viewer->m_window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
        {
            speed = 10;
        }

        //  ////< XZH X
        //if (glfwGetKey(m_viewer->m_window, GLFW_KEY_KP_1) == GLFW_PRESS)
        //{
        //    rollx -= angdt*speed;
        //}
        //else
        //{
        //    rollx -= 0;
        //}
        //if (glfwGetKey(m_viewer->m_window, GLFW_KEY_KP_4) == GLFW_PRESS)
        //{
        //    rollx += angdt*speed;
        //}
        //else
        //{
        //    rollx += 0;
        //}

        //if (glfwGetKey(m_viewer->m_window, GLFW_KEY_KP_3) == GLFW_PRESS)
        //{
        //    rollz -= angdt*speed;
        //}
        //else
        //{
        //    rollz -= 0;
        //}
        //if (glfwGetKey(m_viewer->m_window, GLFW_KEY_KP_6) == GLFW_PRESS)
        //{
        //    rollz += angdt*speed;
        //}
        //else
        //{
        //    rollz += 0;
        //}

        switch (keyID)
        {
        //case GLFW_KEY_KP_1: ////< XZH -x
        ////default:          
        //    rollx -= 3.1415926 / 360;   ////< XZH  1 degree = 3.1415926 / 180
        //    break;
        //case GLFW_KEY_KP_4: ////< XZH +x
        //    rollx += 3.1415926 / 360;
        //    break;
        //case GLFW_KEY_KP_3: ////< XZH -z
        //    rollz -= 3.1415926 / 360;
        //    break;
        //case GLFW_KEY_KP_6: ////< XZH +z        
        //    rollz += 3.1415926 / 360;
        //    break;
        //case GLFW_KEY_KP_2:  ////< XZH -y
        //    if (transy < 0) transy = 0;
        //    else if (transy > 1) transy = 1;
        //    else transy -= 0.01*0.01;
        //    break;
        //case GLFW_KEY_KP_5:  ////< XZH +y
        //    if (transy < 0) transy = 0;
        //    else if (transy > 1) transy = 1;
        //    else transy += 0.01*0.01;
        //    break;
        //case GLFW_KEY_0:
        //    transx -= 0.01*0.3;
        //    break;
        //case GLFW_KEY_1:
        //    transx += 0.01*0.3;
        //    break;
        //case GLFW_KEY_2:
        //    transz -= 0.01*0.3;
        //    break;
        //case GLFW_KEY_3:
        //    transz += 0.01*0.3;
        //    break;
        case GLFW_KEY_KP_7:  ////< XZH isPBD
            flagPBD = scene->getIsPBD();
            scene->setIsPBD(!flagPBD);
            break;
        case GLFW_KEY_M:
            scene->setProcedureType(0);
            toolIdx = 0;
            break;
        case GLFW_KEY_I:
            scene->setProcedureType(1);
            toolIdx = 1;
            break;
        case GLFW_KEY_T:
            scene->setProcedureType(2);
            toolIdx = 2;
            break;
        case GLFW_KEY_O:
            scene->setProcedureType(3);
            toolIdx = 3;
            break;
        case GLFW_KEY_P:   ////< XZH camera flag
            if (cameraFlag > 0) cameraFlag = 0;
            else cameraFlag = 1;
            break;
		case GLFW_KEY_V:  ////< XZH isPBD
			flagLoadDissect = !flagDissect;
			break;
        case GLFW_KEY_HOME:   ////< XZH switch to rotate along which axis  X
            flagXZ = 0;
            break;
        case GLFW_KEY_END:   ////< XZH switch to rotate along which axis  Z
            flagXZ = 1;
            break;
        }
        //omniClient->setRollX(rollx);
        //omniClient->setRollZ(rollz);
        //omniClient->setTransY(transy);
        //omniClient->setTransX(transx);
        //omniClient->setTransZ(transz);
        omniClient->setToolIndex(toolIdx);
        omniClient->setCameraFlag(cameraFlag);
    }

      ////< XZH 
    auto cam1 = m_simManager->getActiveScene()->getCamera();
    switch (keyID)
    {
    case GLFW_KEY_K:
        auto offset = cam1->getPosition() - cam1->getFocalPoint();
        //auto convertedOffset = glm::vec3(offset.x(), offset.y(), offset.z());
        Vec3d convertedOffset = Vec3d(offset.x(), offset.y(), offset.z());
        //glm::vec3 rotationAxis(0, 1, 0);
        Vec3d rotationAxis = Vec3d(0, 0, 1);
        //glm::mat4 rotation;
        Eigen::Matrix4d rotation = Eigen::Matrix4d();
        rotation(0, 0) = 1; rotation(0, 1) = 0; rotation(0, 2) = 0; rotation(0, 3) = 0;
        rotation(1, 0) = 0; rotation(1, 1) = std::cos(5.0 / 180.0 * imstk::PI); rotation(1, 2) = -std::sin(5.0 / 180.0 * imstk::PI); rotation(1, 3) = 0;
        rotation(2, 0) = 0; rotation(2, 1) = std::sin(5.0 / 180.0 * imstk::PI); rotation(2, 2) = std::cos(5.0 / 180.0 * imstk::PI); rotation(2, 3) = 0;
        rotation(3, 0) = 0; rotation(3, 1) = 0; rotation(3, 2) = 0; rotation(3, 3) = 1;
        //rotation = glm::rotate(rotation, -0.2, rotationAxis);
        Vec4d vertexPos4;
        vertexPos4.w() = 1;
        vertexPos4.x() = convertedOffset.x();
        vertexPos4.y() = convertedOffset.y();
        vertexPos4.z() = convertedOffset.z();
        vertexPos4.applyOnTheLeft(rotation);
        Vec3d new_position; //  = rotation*convertedOffset;
        new_position.x() = vertexPos4.x();
        new_position.y() = vertexPos4.y();
        new_position.z() = vertexPos4.z();
        //Vec3d new_position = rotation*convertedOffset;
        //auto new_position = rotation * glm::vec4(offset.x(), offset.y(), offset.z(), 1);
        imstk::Vec3d position(new_position.x(), new_position.y(), new_position.z()); // (new_position[0], new_position[1], new_position[2]);
        cam1->setPosition(cam1->getFocalPoint() + position);
        break;
    }

    if (key == ' ')
    {
        // pause simulation
        if (status == SimulationStatus::RUNNING)
        {
            m_simManager->pauseSimulation();
        }
        // play simulation
        else if (status == SimulationStatus::PAUSED)
        {
            m_simManager->runSimulation();
        }
        // Launch simulation if inactive
        if (status == SimulationStatus::INACTIVE)
        {
            m_simManager->startSimulation(SimulationStatus::RUNNING);
        }
    }
    else if (status != SimulationStatus::INACTIVE &&
        (keyID == GLFW_KEY_Q))
             //(key == 'q' || key == 'Q' || key == 'e' || key == 'E')) // end Simulation
             //(key == 'q' || key == 'Q'))// || key == 'e' || key == 'E')) // end Simulation
    {
        m_simManager->endSimulation();
    }
    //else if (key == 'f' || key == 'F')//(key == 'd' || key == 'D') // switch rendering mode
    //{
    //    if (m_simManager->getViewer()->getRenderingMode() != Renderer::Mode::SIMULATION)
    //    {
    //        m_simManager->getViewer()->setRenderingMode(Renderer::Mode::SIMULATION);
    //    }
    //    else
    //    {
    //        m_simManager->getViewer()->setRenderingMode(Renderer::Mode::DEBUG);
    //    }
    //}
    else if (keyID == GLFW_KEY_ESCAPE) // quit viewer
    {
        m_simManager->getViewer()->endRenderingLoop();
    }
    else if (key == 'p' || key == 'P') // switch framerate display
    {
    }
    else if (key == 'r' || key == 'R')
    {
        m_simManager->resetSimulation();
    }
    
      ////< XZH simulation 
    switch (keyID)
    {
    case GLFW_KEY_F:
        if (m_simManager->getViewer()->getRenderingMode() != Renderer::Mode::SIMULATION)
        {
            m_simManager->getViewer()->setRenderingMode(Renderer::Mode::SIMULATION);
        }
        else
        {
            m_simManager->getViewer()->setRenderingMode(Renderer::Mode::DEBUG);
        }
        break;
    }

    float& XM = scene->getKeyBoardX();
    float& YM = scene->getKeyBoardY();
    float& ZM = scene->getKeyBoardZ();
    float& XP = scene->getKeyBoardX();
    float& YP = scene->getKeyBoardY();
    float& ZP = scene->getKeyBoardZ();
    float& rotupy = scene->getKeyBoardRot();
    float& rotrightx = scene->getKeyBoardRotRX();
    float& rotbackz = scene->getKeyBoardRotBZ();
    /*switch (keyID)
    {
    case GLFW_KEY_KP_1:   
        XM -= gap*0.01;
        break;
    case GLFW_KEY_KP_2:
        YM -= gap*0.01;
        break;
    case GLFW_KEY_KP_3:
        ZM -= gap*0.01;
        break;
    case GLFW_KEY_KP_4:
        XP += gap*0.01;
        break;
    case GLFW_KEY_KP_5:
        YP += gap*0.01;
        break;
    case GLFW_KEY_KP_6:
        ZP += gap*0.01;
        break;
    case GLFW_KEY_Z:
        rotbackz += gap;
        rotupy = 0;
        rotrightx = 0;
        break;
    case GLFW_KEY_C:
        rotupy += gap;
        rotbackz = 0;
        rotrightx = 0;
        break;
    case GLFW_KEY_X:
        rotrightx += gap;
        rotupy = 0;
        rotbackz = 0;
        break;
    case GLFW_KEY_KP_0:
        rotbackz = 0;
        rotupy = 0;
        rotrightx = 0;
        XM =0;
        YM =0;
        ZM = 0;
        XP =0;
        YP =0;
        ZP =0;
        break;
    }*/

    //else if (key == '1')
    //{
    //    auto scene = m_simManager->getActiveScene();
    //    float& XM = scene->getKeyBoardX();
    //    XM -= gap;
    //}
    //else if (key == '2')
    //{
    //    auto scene = m_simManager->getActiveScene();
    //    float& YM = scene->getKeyBoardY();
    //    YM -= gap;
    //}
    //else if (key == '3')
    //{
    //    auto scene = m_simManager->getActiveScene();
    //    float& ZM = scene->getKeyBoardZ();
    //    ZM -= gap;
    //}
    //else if (key == '4')
    //{
    //    auto scene = m_simManager->getActiveScene();
    //    float& XP = scene->getKeyBoardX();
    //    XP += gap;
    //}
    //else if (key == '5')
    //{
    //    auto scene = m_simManager->getActiveScene();
    //    float& YP = scene->getKeyBoardY();
    //    YP += gap;
    //}
    //else if (key == '6')
    //{
    //    auto scene = m_simManager->getActiveScene();
    //    float& ZP = scene->getKeyBoardZ();
    //    ZP += gap;
    //}
    //else if (key == 'z')  ////< XZH clock
    //{
    //    auto scene = m_simManager->getActiveScene();
    //    float& rot = scene->getKeyBoardRot();
    //    rot =-gap;
    //}
    //else if (key == 'x')   ////< XZH clock-wise
    //{
    //    auto scene = m_simManager->getActiveScene();
    //    float& rot = scene->getKeyBoardRot();
    //    rot = gap;
    //}
    
}

void
VulkanInteractorStyle::OnMouseMove(double x, double y)
{
    m_mousePos[0] = x;
    m_mousePos[1] = y;
    m_mousePosNormalized[0] = x;
    m_mousePosNormalized[1] = y;
    this->normalizeCoordinate(m_mousePosNormalized[0], m_mousePosNormalized[1]);

    if (m_onMouseMoveFunction && m_onMouseMoveFunction(this))
    {
        return;
    }

    if (m_simManager->getViewer()->getRenderingMode() != Renderer::Mode::DEBUG)
    {
        return;
    }
}

void
VulkanInteractorStyle::OnLeftButtonDown()
{
    m_state |= VulkanInteractorStyle::LEFT_MOUSE_DOWN;

    if (m_onLeftButtonDownFunction && m_onLeftButtonDownFunction(this))
    {
        return;
    }

    if (m_simManager->getViewer()->getRenderingMode() != Renderer::Mode::DEBUG)
    {
        return;
    }
}

void
VulkanInteractorStyle::OnLeftButtonUp()
{
    m_state &= ~VulkanInteractorStyle::LEFT_MOUSE_DOWN;

    if (m_onLeftButtonUpFunction && m_onLeftButtonUpFunction(this))
    {
        return;
    }

    if (m_simManager->getViewer()->getRenderingMode() != Renderer::Mode::DEBUG)
    {
        return;
    }
}

void
VulkanInteractorStyle::OnMiddleButtonDown()
{
    m_state |= VulkanInteractorStyle::MIDDLE_MOUSE_DOWN;

    if (m_onMiddleButtonDownFunction && m_onMiddleButtonDownFunction(this))
    {
        return;
    }

    if (m_simManager->getViewer()->getRenderingMode() != Renderer::Mode::DEBUG)
    {
        return;
    }
}

void
VulkanInteractorStyle::OnMiddleButtonUp()
{
    m_state &= ~VulkanInteractorStyle::MIDDLE_MOUSE_DOWN;

    if (m_onMiddleButtonUpFunction && m_onMiddleButtonUpFunction(this))
    {
        return;
    }

    if (m_simManager->getViewer()->getRenderingMode() != Renderer::Mode::DEBUG)
    {
        return;
    }
}

void
VulkanInteractorStyle::OnRightButtonDown()
{
    m_state |= VulkanInteractorStyle::RIGHT_MOUSE_DOWN;

    if (m_onRightButtonDownFunction && m_onRightButtonDownFunction(this))
    {
        return;
    }

    if (m_simManager->getViewer()->getRenderingMode() != Renderer::Mode::DEBUG)
    {
        return;
    }
}

void
VulkanInteractorStyle::OnRightButtonUp()
{
    m_state &= ~VulkanInteractorStyle::RIGHT_MOUSE_DOWN;

    if (m_onRightButtonUpFunction && m_onRightButtonUpFunction(this))
    {
        return;
    }

    if (m_simManager->getViewer()->getRenderingMode() != Renderer::Mode::DEBUG)
    {
        return;
    }
}

void
VulkanInteractorStyle::OnMouseWheelForward(double y)
{
    if (m_onMouseWheelForwardFunction && m_onMouseWheelForwardFunction(this))
    {
        return;
    }

    if (m_simManager->getViewer()->getRenderingMode() != Renderer::Mode::DEBUG)
    {
        return;
    }
}

void
VulkanInteractorStyle::OnMouseWheelBackward(double y)
{
    if (m_onMouseWheelBackwardFunction && m_onMouseWheelBackwardFunction(this))
    {
        return;
    }

    if (m_simManager->getViewer()->getRenderingMode() != Renderer::Mode::DEBUG)
    {
        return;
    }
}

void
VulkanInteractorStyle::OnWindowResizeInterface(GLFWwindow * window, int width, int height)
{
    auto style = (VulkanInteractorStyle *)glfwGetWindowUserPointer(window);
}

void
VulkanInteractorStyle::OnFramebuffersResizeInterface(GLFWwindow * window, int width, int height)
{
    auto style = (VulkanInteractorStyle *)glfwGetWindowUserPointer(window);
    style->OnWindowResize(width, height);
}

void
VulkanInteractorStyle::normalizeCoordinate(double &x, double &y)
{
    x = (x - m_viewer->m_width / 2) / m_viewer->m_width;
    y = (y - m_viewer->m_height / 2) / m_viewer->m_height;
}

void
VulkanInteractorStyle::OnWindowResize(int width, int height)
{
    m_viewer->resizeWindow(width, height);
}

void
VulkanInteractorStyle::OnMouseMoveInterface(GLFWwindow * window, double x, double y)
{
    auto style = (VulkanInteractorStyle *)glfwGetWindowUserPointer(window);
    style->OnMouseMove(x, y);
}

void
VulkanInteractorStyle::OnMouseWheelInterface(GLFWwindow * window, double x, double y)
{
    auto style = (VulkanInteractorStyle *)glfwGetWindowUserPointer(window);
    if (y < 0)
    {
        style->OnMouseWheelBackward(y);
    }
    else
    {
        style->OnMouseWheelForward(y);
    }
}
}