﻿/*=========================================================================

Library: iMSTK

Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
& Imaging in Medicine, Rensselaer Polytechnic Institute.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0.txt

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

=========================================================================*/

// imstk
#include "imstkSceneManager.h"
#include "imstkCameraController.h"
#include "imstkSceneObjectController.h"
#include "imstkDynamicObject.h"
#include "imstkPbdObject.h"
#include "imstkDeformableObject.h"
#include "imstkVirtualCouplingPBDObject.h"
#include "imstkPBDVirtualCouplingObject.h" // xzh
#include "imstkPbdSceneObjectController.h" // xzh
#include "imstkDecalPool.h" // XZH
#include "imstkSphere.h" // XZH

#include "imstkGeometryMap.h"
#include "imstkTimer.h"

#include "g3log/g3log.hpp"

#include <windows.h> // for time test XZH
DWORD t3, t4, t1, t2;

namespace imstk
{
    std::shared_ptr<Scene>
        SceneManager::getScene()
    {
        return m_scene;
    }

    void
        SceneManager::initModule()
    {
        // Start Camera Controller (asynchronous)
        if (auto camController = m_scene->getCamera()->getController())
        {
            this->startModuleInNewThread(camController);
        }

        // Update objects controlled by the device controllers XZH
        for (auto controller : m_scene->getSceneObjectControllers())
        {
            if (auto pbdController = std::dynamic_pointer_cast<PBDSceneObjectController>(controller))
            {
                pbdController->initOffsets();
            }
        }

        // Init virtual coupling objects offsets
        for (auto obj : m_scene->getSceneObjects())
        {
            if (auto virtualCouplingPBD = std::dynamic_pointer_cast<VirtualCouplingPBDObject>(obj))
            {
                virtualCouplingPBD->initOffsets();
            }
        }

        // navgation
        vecCamera.push_back(Vec3d(3.049765, -5.416139, -13.772857));
        vecCamera.push_back(Vec3d(2.066011, -2.092456, -16.283300));
        vecCamera.push_back(Vec3d(2.416454, 0.893009, -18.732140));
        vecCamera.push_back(Vec3d(2.656587, 3.308475, -21.841312));
        vecCamera.push_back(Vec3d(0.744602, 4.685533, -24.712399));
        vecCamera.push_back(Vec3d(-2.111103, 5.906062, -24.198272));

        vecCameraFocal.push_back(Vec3d(-4.065353, 2.550728, -18.045065));
        vecCameraFocal.push_back(Vec3d(-2.318216, 3.125296, -19.496111));
        vecCameraFocal.push_back(Vec3d(-0.275814, 4.097096, -20.704975));
        vecCameraFocal.push_back(Vec3d(-0.668360, 6.501790, -21.953945));
        vecCameraFocal.push_back(Vec3d(-0.777570, 7.189897, -22.593023));
        vecCameraFocal.push_back(Vec3d(-6.532188, 16.588575, -13.986447));

        // cutting and dissection for Graph init
        for (int i = 0; i < GRAPHMAPSIZEVISUAL;i++)
        {
            std::vector<int> glist;
            for (int j = 0; j < GRAPHMAPSIZEVISUAL; j++)
            {
                glist.push_back(0);
            }
            graphVisualCutting.push_back(glist);
        }
        m_graphVisualCutting = std::make_shared<imstk::UndirectedGraph>(graphVisualCutting);
    }

    void
        SceneManager::runModule()
    {
        // XZH
        t3 = t4;
        t4 = GetTickCount();
        //FILE *outForceFilter = fopen("G:/outInformation.dat", "a");
        //fprintf(outForceFilter, "gap time %d ms,  frame rate is: %f\n", t2 - t1, 1000 / double(t2 - t1));
        //fclose(outForceFilter);
        //printf("scene gap time %d ms,  frame rate is: %f\n", t4 - t3, 1000 / double(t4 - t3));
        //extractSurfaceMeshSleep(8);
        //FILE *outForceFilter = fopen("G:/outforcefilter.dat", "a");
        //fprintf(outForceFilter, "gap time %d ms,  frame rate is: %f\n", t2 - t1, 1000 / double(t2 - t1));
        //fclose(outForceFilter);
        // XZH END

        // XZH camera
        auto cam1 = m_scene->getCamera();
        Vec3d camPos = cam1->getPosition();
        Vec3d camFocalPoint = cam1->getFocalPoint();
        double viewAngle = cam1->getViewAngle();
        /*Vec3d vecCamera0 = Vec3d::Zero();
        cam1->setViewUp(0.01, 1, 0.01);
        Vec3d vecCameraFocal0 = Vec3d::Zero();
        if ((camPos - vecCamera[5]).norm() < 0.1)
        {
        numGap = 1000;
        }
        else
        {
        numGap++;
        }
        cam1->setPosition(vecCamera[4] + numGap*0.001*(vecCamera[5] - vecCamera[4]));
        cam1->setFocalPoint(vecCameraFocal[4] + numGap*0.001*(vecCameraFocal[5] - vecCameraFocal[4]));
        for (auto obj : m_scene->getLights())
        {
        if (obj->getName() == "whiteLight")
        {
        if (auto spotlight = std::dynamic_pointer_cast<PointLight>(obj))
        {
        spotlight->setPosition(camPos);
        spotlight->setFocalPoint(camFocalPoint);
        }
        }
        if (obj->getName() == "whiteLight2")
        {
        if (auto spotlight = std::dynamic_pointer_cast<PointLight>(obj))
        {
        spotlight->setPosition(vecCamera[1]);
        spotlight->setFocalPoint(vecCameraFocal[1]);
        }
        }
        if (obj->getName() == "whiteLight3")
        {
        if (auto spotlight = std::dynamic_pointer_cast<PointLight>(obj))
        {
        spotlight->setPosition(vecCamera[2]);
        spotlight->setFocalPoint(vecCameraFocal[2]);
        }
        }
        if (obj->getName() == "whiteLight4")
        {
        if (auto spotlight = std::dynamic_pointer_cast<PointLight>(obj))
        {
        spotlight->setPosition(vecCamera[3]);
        spotlight->setFocalPoint(vecCameraFocal[3]);
        }
        }
        if (obj->getName() == "whiteLight5")
        {
        if (auto spotlight = std::dynamic_pointer_cast<PointLight>(obj))
        {
        spotlight->setPosition(vecCamera[4]);
        spotlight->setFocalPoint(vecCameraFocal[4]);
        }
        }
        if (obj->getName() == "whiteLight6")
        {
        if (auto spotlight = std::dynamic_pointer_cast<PointLight>(obj))
        {
        spotlight->setPosition(vecCamera[5]);
        spotlight->setFocalPoint(vecCameraFocal[5]);
        }
        }
        }*/
        //    cam1->setFocalPoint(vecCameraFocal0 + numGap*0.01*(vecCameraFocal[navStep + 1] - vecCameraFocal[navStep]));//
        //printf("cam pos: %f %f %f, focal: %f %f %f, angle: %f\n", camPos.x(), camPos.y(), camPos.z(), camFocalPoint.x(), camFocalPoint.y(), camFocalPoint.z(), viewAngle);
        // 1:  3.049765, -5.416139, -13.772857     -4.065353, 2.550728, -18.045065
        //for (int m = vecCamera.size() - 1; m >= 0; m--)
        //{
        //    if ((camPos - vecCamera[m]).norm()<0.1)
        //    {
        //        navStep = m;
        //        //numGap = 0;
        //        vecCamera0 = vecCamera[m];//               cam1->setPosition(vecCamera[m]);
        //        vecCameraFocal0 = vecCameraFocal[m]; // cam1->setFocalPoint();//
        //        break;
        //    }
        //}
        //
        //if (navStep==5)
        //{
        //    numGap=0;
        //    cam1->setPosition(vecCamera0);
        //    cam1->setFocalPoint(vecCameraFocal0); 
        //}
        //else
        //{
        //    numGap++;
        //    //if (numGap>100)
        //    //{
        //    //    numGap = 0;
        //    //}
        //    cam1->setPosition(vecCamera0 + numGap*0.01*(vecCamera[navStep+1]-vecCamera[navStep]));
        //    cam1->setFocalPoint(vecCameraFocal0 + numGap*0.01*(vecCameraFocal[navStep + 1] - vecCameraFocal[navStep]));//
        //    if ((camPos - vecCamera[navStep + 1]).norm()<0.05)
        //    {
        //        numGap = 0;
        //    }
        //}
        // Navigation END

        Vec3d tip, root;
        tip = root = Vec3d::Zero();
        Vec3d centralPosition = Vec3d::Zero();
        StopWatch wwt;

        wwt.start();

        // Reset Contact forces to 0
        for (auto obj : m_scene->getSceneObjects())
        {
            if (auto defObj = std::dynamic_pointer_cast<DeformableObject>(obj))
            {
                defObj->getContactForce().setConstant(0.0);
            }
            //else if (auto collidingObj = std::dynamic_pointer_cast<CollidingObject>(obj))
            //{
            //    if (auto pbdvcObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(obj))
            //    {
            //    }
            //    else
            //    {
            //        //collidingObj->resetForce();  // XZH
            //    }
            //}
            if (auto pbdvcObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(obj))
            {
                // XZH start
                auto collidingMesh = std::dynamic_pointer_cast<PointSet>(pbdvcObj->getCollidingGeometry());
                root = collidingMesh->getVertexPosition(0);
                tip = collidingMesh->getVertexPosition(1); // 1 not 2
                pbdvcObj->resetForce();  // XZH
                // XZH END
            }
            // todo: refactor pbd
            // description: so that the transform obtained from device can be applied
            if (auto virtualCouplingPBD = std::dynamic_pointer_cast<VirtualCouplingPBDObject>(obj))
            {
                virtualCouplingPBD->resetCollidingGeometry();
            }
        }

        // Update objects controlled by the device controllers
        for (auto controller : m_scene->getSceneObjectControllers())
        {
            controller->updateControlledObjects();
        }

        // Compute collision data per interaction pair
        for (auto intPair : m_scene->getCollisionGraph()->getInteractionPairList())
        {
            intPair->computeCollisionData(tip);  // for others XZH intPair->computeCollisionData();
            intPair->computeContactForces();
        }

        //// Apply forces on device
        //for (auto controller : m_scene->getSceneObjectControllers())
        //{
        //    controller->applyForces();
        //}

        // Update the solvers
        for (auto solvers : m_scene->getSolvers())
        {
            // XZH test
            bool isColonInjected = false;
            for (auto intPair : m_scene->getCollisionGraph()->getPbdPairList())
            {
                auto pbdObj = intPair->getSecondObj();
                if ((pbdObj->getIsInjection()) && (pbdObj->getName() == "Colon"))
                {
                    isColonInjected = true;
                    break;
                }
            }
            if (!isColonInjected)
            {
                if (isEnd)
                {
                    //solvers->solve();  // original is just this one line code
                }
            }
            // XZH END
            //solvers->solve();
        }
        // XZH test

        std::shared_ptr<VESSTetrahedralMesh> m_xzhPhysicalMesh;
        std::shared_ptr<SurfaceMesh> m_xzhVisualMesh;

        // Apply the geometry and apply maps to all the objects
        for (auto obj : m_scene->getSceneObjects())
        {
            // not update for pbd virtualcoupling
            if (auto pbdvcObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(obj))
            {
                //pbdvcObj->resetCollidingGeometry();
                // XZH start
                auto collidingMesh = std::dynamic_pointer_cast<PointSet>(pbdvcObj->getCollidingGeometry());
                root = collidingMesh->getVertexPosition(0);
                tip = collidingMesh->getVertexPosition(1); // 1 not 2
                // XZH END
            }
            else
            {
                //obj->updateGeometries();
            }

            // testing get visual
            if (obj->getName() == "Colon")
            {
                auto pbdvcObj = std::dynamic_pointer_cast<PbdObject>(obj);
                m_xzhVisualMesh = std::dynamic_pointer_cast<SurfaceMesh>(pbdvcObj->getVisualGeometry());
            }
        }

        // XZH  update two test sphere  one is tip of tool, one is root of tool
        for (auto obj : m_scene->getSceneObjects())
        {
            if (obj->getName() == "Sphere1")
            {
                obj->getVisualGeometry()->setTranslation(root);
            }
            else if (obj->getName() == "Sphere2")
            {
                obj->getVisualGeometry()->setTranslation(tip);
            }
            else if (obj->getName() == "XZHSphere0")
            {
                obj->getVisualGeometry()->setTranslation(m_xzhVisualMesh->getVertexPosition(39));
            }
            else if (obj->getName() == "XZHSphere1")
            {
                obj->getVisualGeometry()->setTranslation(m_xzhVisualMesh->getVertexPosition(165));
            }
            else if (obj->getName() == "XZHSphere2")
            {
                obj->getVisualGeometry()->setTranslation(m_xzhVisualMesh->getVertexPosition(110));
            }
            else if (obj->getName() == "XZHSphere3")
            {
                obj->getVisualGeometry()->setTranslation(m_xzhVisualMesh->getVertexPosition(106));
            }
            else if (obj->getName() == "XZHSphere4")
            {
                obj->getVisualGeometry()->setTranslation(m_xzhVisualMesh->getVertexPosition(102));
            }
            else if (obj->getName() == "XZHSphere5")
            {
                obj->getVisualGeometry()->setTranslation(m_xzhVisualMesh->getVertexPosition(163));
            }
            else if (obj->getName() == "XZHSphere6")
            {
                obj->getVisualGeometry()->setTranslation(m_xzhVisualMesh->getVertexPosition(220));
            }
            else if (obj->getName() == "XZHSphere7")
            {
                obj->getVisualGeometry()->setTranslation(m_xzhVisualMesh->getVertexPosition(9));
            }
            else if (obj->getName() == "XZHSphere8")
            {
                obj->getVisualGeometry()->setTranslation(m_xzhVisualMesh->getVertexPosition(3));
            }
            else if (obj->getName() == "DecalObject0")
            {
                if (auto decalPool = std::dynamic_pointer_cast<DecalPool>(obj->getVisualGeometry()))
                {
                    auto& decallist = decalPool->getDecals();   //->getVisualGeometry()->setTranslation(m_xzhVisualMesh->getVertexPosition(39));
                    decallist.at(0)->setPosition(m_xzhVisualMesh->getVertexPosition(39));
                    //decal1->setPosition(markingPos);
                    ////    decal1->rotate(RIGHT_VECTOR, PI_4);
                    ////    decal1->scale(0.6);
                }
                //auto decalPoolObj = std::dynamic_pointer_cast<VisualObject>(obj); //   <DecalPool>(obj);
                //auto decalPool = decalPoolObj->getVisualGeometry(); // std::shared_ptr<DecalPool>
                //auto& decallist=decalPool->getDecals();   //->getVisualGeometry()->setTranslation(m_xzhVisualMesh->getVertexPosition(39));
                //decallist.at(0)->setTranslation(m_xzhVisualMesh->getVertexPosition(39));
            }
            else if (obj->getName() == "DecalObject1")
            {
                if (auto decalPool = std::dynamic_pointer_cast<DecalPool>(obj->getVisualGeometry()))
                {
                    auto& decallist = decalPool->getDecals();
                    decallist.at(0)->setPosition(m_xzhVisualMesh->getVertexPosition(165));
                }
            }
            else if (obj->getName() == "DecalObject2")
            {
                if (auto decalPool = std::dynamic_pointer_cast<DecalPool>(obj->getVisualGeometry()))
                {
                    auto& decallist = decalPool->getDecals();
                    decallist.at(0)->setPosition(m_xzhVisualMesh->getVertexPosition(110));
                }
            }
            else if (obj->getName() == "DecalObject3")
            {
                if (auto decalPool = std::dynamic_pointer_cast<DecalPool>(obj->getVisualGeometry()))
                {
                    auto& decallist = decalPool->getDecals();
                    decallist.at(0)->setPosition(m_xzhVisualMesh->getVertexPosition(106));
                }
            }
            else if (obj->getName() == "DecalObject4")
            {
                if (auto decalPool = std::dynamic_pointer_cast<DecalPool>(obj->getVisualGeometry()))
                {
                    auto& decallist = decalPool->getDecals();
                    decallist.at(0)->setPosition(m_xzhVisualMesh->getVertexPosition(102));
                }
            }
            else if (obj->getName() == "DecalObject5")
            {
                if (auto decalPool = std::dynamic_pointer_cast<DecalPool>(obj->getVisualGeometry()))
                {
                    auto& decallist = decalPool->getDecals();
                    decallist.at(0)->setPosition(m_xzhVisualMesh->getVertexPosition(163));
                }
            }
            else if (obj->getName() == "DecalObject6")
            {
                if (auto decalPool = std::dynamic_pointer_cast<DecalPool>(obj->getVisualGeometry()))
                {
                    auto& decallist = decalPool->getDecals();
                    decallist.at(0)->setPosition(m_xzhVisualMesh->getVertexPosition(220));
                }
            }
            else if (obj->getName() == "DecalObject7")
            {
                if (auto decalPool = std::dynamic_pointer_cast<DecalPool>(obj->getVisualGeometry()))
                {
                    auto& decallist = decalPool->getDecals();
                    decallist.at(0)->setPosition(m_xzhVisualMesh->getVertexPosition(9));
                }
            }
            else if (obj->getName() == "DecalObject8")
            {
                if (auto decalPool = std::dynamic_pointer_cast<DecalPool>(obj->getVisualGeometry()))
                {
                    auto& decallist = decalPool->getDecals();
                    decallist.at(0)->setPosition(m_xzhVisualMesh->getVertexPosition(3));
                }
            }
            else if (obj->getName() == "visualCutFace")
            {
                auto mesh = obj->getVisualGeometry();
            }
            else if (obj->getName() == "visualColonMeshVolume")
            {
                for (auto obj2 : m_scene->getSceneObjects())
                {
                    if (obj2->getName() == "Cube")
                    {
                        auto pbdvcObj = std::dynamic_pointer_cast<PbdObject>(obj2);
                        m_xzhPhysicalMesh = std::dynamic_pointer_cast<VESSTetrahedralMesh>(pbdvcObj->getPhysicsGeometry());

                        //m_xzhPhysicalMesh->extractSurfaceMeshVolume(); //  getSurfaceMeshVolume();
                        m_xzhPhysicalMesh->extractSurfaceMesh();  // this influence the speed siganificantly
                        auto vMesh = m_xzhPhysicalMesh->getSurfaceMesh();  //->getSurfaceMeshVolume();
                        auto visualMesh = std::dynamic_pointer_cast<PointSet>(obj->getVisualGeometry());
                        //obj->setVisualGeometry(visualMesh);

                        Vec3d vertexPos3;
                        for (int i = 0; i < visualMesh->getNumVertices(); ++i)
                        {
                            vertexPos3 = vMesh->getVertexPosition(i);
                            visualMesh->setVertexPosition(i, vertexPos3);
                        }
                    }
                }
            }
            else if (obj->getName() == "visualColonMesh4")  // wireframe mesh
            {
                continue;
                for (auto obj2 : m_scene->getSceneObjects())
                {
                    if (obj2->getName() == "Colon")
                    {
                        auto pbdvcObj = std::dynamic_pointer_cast<PbdObject>(obj2);
                        m_xzhPhysicalMesh = std::dynamic_pointer_cast<VESSTetrahedralMesh>(pbdvcObj->getPhysicsGeometry());
                    }
                }
                // XZH // for both sides surface of volume mesh
                //m_xzhPhysicalMesh->extractSurfaceMesh();
                //auto vMesh = m_xzhPhysicalMesh->getSurfaceMesh();
                ////obj->setVisualGeometry(m_xzhPhysicalMesh->getSurfaceMesh());

                // for only inside surface of volume mesh
                std::vector<Vec3d> centralLine;
                centralLine.push_back(Vec3d(-19.4, -5.85, 1.15));
                centralLine.push_back(Vec3d(-3.3, 2.4, 1.05));
                break;
                m_xzhPhysicalMesh->extractSurfaceMesh(centralLine); // <<<!!!!Most important !!!!>>> only inside colon of surface of volume mesh
                auto vMesh = m_xzhPhysicalMesh->getSurfaceMesh();  // <<<!!!!Most important !!!!>>> from test found that this part is the most time-consuming process XZH
                // XZH END 
                // TEST visual geometry
                Vec3d vertexPos3;
                auto visualMesh = std::dynamic_pointer_cast<PointSet>(obj->getVisualGeometry());
                for (int i = 0; i < visualMesh->getNumVertices(); ++i)
                {
                    vertexPos3 = vMesh->getVertexPosition(i);
                    visualMesh->setVertexPosition(i, vertexPos3);
                }
                // TEST END
            }
            else if (obj->getName() == "visualColonMesh5")  // inside colon mesh
            {
            }
            else if (obj->getName() == "visualNormal0")
            {
                // TEST visual geometry
                Vec3d vertexPos3 = Vec3d::Zero();
                auto visualMesh = std::dynamic_pointer_cast<PointSet>(obj->getVisualGeometry());
                visualMesh->setVertexPosition(0, tip);
                for (auto intPair : m_scene->getCollisionGraph()->getPbdPairList())
                {
                    vertexPos3 = intPair->getMoveDir();   //getCentralPos(); // get central of colliding vertices  //  intPair->getForce();  // force normal 
                }
                visualMesh->setVertexPosition(1, tip + vertexPos3 * 100); // 2 times of normal
                visualMesh->setVertexPosition(2, tip);
                // TEST END
            }
            else if (obj->getName() == "visualLineMesh")
            {
                auto visualMesh = std::dynamic_pointer_cast<PointSet>(obj->getVisualGeometry());
                visualMesh->setVertexPosition(0, tip);
                visualMesh->setVertexPosition(1, root);
                visualMesh->setVertexPosition(2, tip);
            }
        }
        // XZH END

        // Do collision detection and response for pbd objects
        for (auto intPair : m_scene->getCollisionGraph()->getPbdPairList())
        {
            break; // jump to cutting part
            intPair->resetConstraints();
            if (intPair->doBroadPhaseCollision())
            {
                intPair->chooseProcedure(); // Injection solution || choose via Omni Button 0 intPair->doNarrowPhaseContinuousCollisionVESS(); // for static CD intPair->doNarrowPhaseCollisionVESS();  // for other intPair->doNarrowPhaseCollision();  // for VESS intPair->doNarrowPhaseCollisionVESS(); // intPair->doNarrowPhaseCollision();

                //// marking START
                //intPair->doMarking();
                //auto& markingState = intPair->getMarkState();
                //if (markingState)
                //{
                //    Vec3d markingPos = intPair->getMarkingPos();
                //    printf("marking: %f %f %f\n", markingPos.x(), markingPos.y(), markingPos.z());
                //    //std::shared_ptr<DecalPool> decalPool;
                //    //for (auto obj : m_scene->getSceneObjects())
                //    //{
                //    //    if (obj->getName() == "DecalObject")
                //    //    {
                //    //        decalPool = std::dynamic_pointer_cast<DecalPool>(obj->getVisualGeometry());
                //    //        auto decal1 = decalPool->addDecal();
                //    //        decal1->setPosition(markingPos);
                //    //        decal1->rotate(FORWARD_VECTOR, PI_4);

                //    //        numMarking++;
                //    //        char name[256];
                //    //        sprintf(name, "XZHSphere%d", numMarking);
                //    //            
                //    //        

                //    //    }
                //    //}
                //    numMarking++;
                //    char name[256];
                //    sprintf(name, "XZHSphere%d", numMarking);
                //    auto visualSphere22 = std::make_shared<imstk::Sphere>();
                //    visualSphere22->setTranslation(markingPos);
                //    visualSphere22->setRadius(0.3);
                //    auto sphere2Obj = std::make_shared<SceneObject>(name);
                //    sphere2Obj->setVisualGeometry(visualSphere22);
                //    m_scene->addSceneObject(sphere2Obj);

                //    auto decalTest = std::make_shared<DecalPool>();
                //    auto decalMaterial = std::make_shared<RenderMaterial>();
                //    //auto diffuseTexture = std::make_shared<Texture>("F:/assets/miscBloodDecalParticles/miscBloodDecalParticles/blood_particle_01.png", Texture::DIFFUSE);
                //    auto diffuseTexture2 = std::make_shared<Texture>("I:/iMSTK/resources/textures/test4.png", Texture::DIFFUSE);
                //    decalMaterial->addTexture(diffuseTexture2);
                //    auto decal1 = decalTest->addDecal();
                //    decal1->setPosition(markingPos);
                //    decal1->rotate(RIGHT_VECTOR, PI_4);
                //    decal1->scale(0.6);
                //    decalTest->setRenderMaterial(decalMaterial);

                //    sprintf(name, "DecalObject%d", numMarking);
                //    auto decalObject = std::make_shared<VisualObject>(name); // "DecalObject"
                //    decalObject->setVisualGeometry(decalTest);
                //    m_scene->addSceneObject(decalObject);

                //    markingState = false;
                //    // marking END
                //}        
            }
            else  // no candidate collision
            {
                intPair->updateToolPos();
                //FILE *outForceFilter = fopen("G:/outInformation.dat", "a");
                //fprintf(outForceFilter, "updateToolPos\n");
                //fclose(outForceFilter);
            }
            // injection and CCD
            if (intPair->getInjectionState())
            {
                intPair->computeContactForceInjection();
            }
            else
            {
                intPair->computeContactForceContinuous();
            }
            //intPair->resolveContinuousCollisionVESS(); // for static CD intPair->resolveCollisionVESS();  // for other intPair->resolveCollision();  // for VESS intPair->resolveCollisionVESS();   // intPair->resolveCollision();
            centralPosition = intPair->getCentralPos(); // extra test
        }

        // Update velocity of PBD object  xzh
        // Update velocity of PBD objects
        for (auto obj : m_scene->getSceneObjects())
        {
            if (auto pbdObj = std::dynamic_pointer_cast<PbdObject>(obj))
            {
                if (auto pbdvcObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(obj))
                {
                    //pbdvcObj->resetCollidingGeometry();
                }
                else
                {
                    pbdObj->updateVelocity();
                }
            }
        }

        // Apply forces on device  change the place in this place XZH
        for (auto controller : m_scene->getSceneObjectControllers())
        {
            controller->applyForces();
        }

        // Set the trackers of virtual coupling PBD objects to out-of-date
        for (auto obj : m_scene->getSceneObjects())
        {
            if (auto virtualCouplingPBD = std::dynamic_pointer_cast<VirtualCouplingPBDObject>(obj))
            {
                virtualCouplingPBD->setTrackerToOutOfDate();
            }
            // XZH TEST
            if (obj->getName() == "Sphere3")
            {
                obj->getVisualGeometry()->setTranslation(centralPosition);
                //printf("sphere3: %f %f %f %f\n", centralPosition.x(), centralPosition.y(), centralPosition.z(), centralPosition.norm());
            }
            // XZH TEST
        }

        // Set the trackers of the scene object controllers to out-of-date
        for (auto controller : m_scene->getSceneObjectControllers())
        {
            controller->setTrackerToOutOfDate();
        }

        // XZH  test cutting
        std::shared_ptr<CuttingVESSCutMesh> cutTissueMesh;
        std::shared_ptr<SurfaceMesh> visualColonMesh;
        //using TetTriMap  for cube dense
        for (auto obj : m_scene->getSceneObjects())
        {
            if (obj->getName() == "VirtualObject")
            {
                if (isEnd)
                {
                    endCount++;
                    if (endCount>=50)
                    {
                        isEnd = false;
                        endCount = 0;
                    }
                    return;
                }
                auto pbdObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(obj);
                if (!pbdObj) continue;

                auto collidingMesh = std::dynamic_pointer_cast<PointSet>(pbdObj->getCollidingGeometry());
                auto& tool = pbdObj->getControllerTool();
                if (tool.button[0])
                {
                    printf("start...");
                    cutStartPre = collidingMesh->getVertexPosition(0); // 0
                    cutEndPre = collidingMesh->getVertexPosition(1); // 1
                    isStart = true;
                }
                if (tool.button[1])
                {
                    printf("end...");

                    cutEnd = collidingMesh->getVertexPosition(1); // 3
                    cutStart = cutEnd + (cutStartPre - cutEndPre); //  collidingMesh->getVertexPosition(0);
                    // get the cube
                    std::shared_ptr<PbdObject> pbdvcObj;
                    for (auto objfind : m_scene->getSceneObjects())
                    {
                        if (objfind->getName() == "Colon")
                        {
                            pbdvcObj = std::dynamic_pointer_cast<PbdObject>(objfind);
                            cutTissueMesh = std::dynamic_pointer_cast<CuttingVESSCutMesh>(pbdvcObj->getPhysicsGeometry());
                            visualColonMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(pbdvcObj->getVisualGeometry());
                        }
                    }
                    DWORD t1, t2;
                    t1 = GetTickCount();

                    // start cutting
                    vector<Vec3d> m_vBladeSegments;
                    vector<Vec3d> m_vSweptQuad;
                    Vec3d temp1, temp2;
                    temp1 = cutStartPre;
                    temp2 = cutStart;
                    m_vBladeSegments.push_back(temp1);
                    m_vBladeSegments.push_back(temp2);

                    Vec3d temp3, temp4;
                    temp1 = cutStartPre;
                    temp2 = cutEndPre;
                    temp3 = cutStart;
                    temp4 = cutEnd;
                    //// Test 1
                    //temp1.x() = -10; temp1.y() = -11; temp1.z() = 0;  // 2 x=4
                    //temp2.x() = -10; temp2.y() = -11; temp2.z() = -30; // 1
                    //temp3.x() = 0; temp3.y() = 51; temp3.z() = 0; // 3
                    //temp4.x() = 0; temp4.y() = 51; temp4.z() = -30; // 4  // partial cutting straight
                    //// Test 1
                    //temp1.x() = -10; temp1.y() = 5; temp1.z() = 0;  // 2 x=4
                    //temp2.x() = -10; temp2.y() =5; temp2.z() = -30; // 1
                    //temp3.x() = 0; temp3.y() = 31; temp3.z() = 0; // 3
                    //temp4.x() = 0; temp4.y() = 31; temp4.z() = -30; // 4  // partial cutting straight
                    //// Test 2
                    //temp1.x() = -20; temp1.y() = -11; temp1.z() = 0;  // 2 x=4
                    //temp2.x() = -20; temp2.y() = -11; temp2.z() = -15; // 1
                    //temp3.x() = 0; temp3.y() = 51; temp3.z() = 0; // 3
                    //temp4.x() = 0; temp4.y() = 51; temp4.z() = -15; // 4  // partial cutting straight
                    //// Test 3
                    //temp1.x() = -4; temp1.y() = -11; temp1.z() = 0;  // 2 x=4
                    //temp2.x() = -4; temp2.y() = -11; temp2.z() = -5; // 1
                    //temp3.x() = 0; temp3.y() = 51; temp3.z() = 0; // 3
                    //temp4.x() = 0; temp4.y() = 51; temp4.z() = -5; // 4  // partial cutting straight
                    // //Test 4
                    //temp1.x() = 0; temp1.y() = 31; temp1.z() = 0;  // 2 x=4
                    //temp2.x() = 0; temp2.y() = 31; temp2.z() = -5; // 1
                    //temp3.x() = 0; temp3.y() = 51; temp3.z() = 0; // 3
                    //temp4.x() = 0; temp4.y() = 51; temp4.z() = -5; // 4  // partial cutting straight
                    //// Test 5
                    //temp1.x() = 0; temp1.y() = 35; temp1.z() = 0;  // 2 x=4
                    //temp2.x() = 0; temp2.y() = 35; temp2.z() = -5; // 1
                    //temp3.x() = 0; temp3.y() = 51; temp3.z() = 0; // 3
                    //temp4.x() = 0; temp4.y() = 51; temp4.z() = -5; // 4  // partial cutting straight
                    //// Test 6
                    //temp1.x() = 0; temp1.y() = 31; temp1.z() = 0;  // 2 x=4
                    //temp2.x() = 0; temp2.y() = 31; temp2.z() = -30; // 1
                    //temp3.x() = 0; temp3.y() = 51; temp3.z() = 0; // 3
                    //temp4.x() = 0; temp4.y() = 51; temp4.z() = -30; // 4  // partial cutting straight
                    //// Test 7
                    //temp1.x() = 0; temp1.y() = 31; temp1.z() = -20;  // 2 x=4
                    //temp2.x() = 0; temp2.y() = 31; temp2.z() = -30; // 1
                    //temp3.x() = 0; temp3.y() = 51; temp3.z() = 20; // 3
                    //temp4.x() = 0; temp4.y() = 51; temp4.z() = -30; // 4  // partial cutting straight
                    //// Test 8
                    //temp1.x() = 4; temp1.y() = 35; temp1.z() = 13;  // 2
                    //temp2.x() = 4; temp2.y() =35; temp2.z() = -35; // 1
                    //temp3.x() = 4; temp3.y() = 51; temp3.z() = 13; // 3
                    //temp4.x() = 4; temp4.y() = 51; temp4.z() = -35; // 4  // partial cutting straight
                    //// Test 9
                    //temp1.x() = 4; temp1.y() = 35; temp1.z() = 0;  // 2
                    //temp2.x() = 4; temp2.y() = 35; temp2.z() = -35; // 1
                    //temp3.x() = 4; temp3.y() = 51; temp3.z() = 0; // 3
                    //temp4.x() = 4; temp4.y() = 51; temp4.z() = -35; // 4  // partial cutting straight
                    //// Test 10
                    //temp1.x() = 4; temp1.y() = 35; temp1.z() = -13;  // 2
                    //temp2.x() = 4; temp2.y() = 35; temp2.z() = -35; // 1
                    //temp3.x() = 4; temp3.y() = 51; temp3.z() = -13; // 3
                    //temp4.x() = 4; temp4.y() = 51; temp4.z() = -35; // 4  // partial cutting straight
                    m_vSweptQuad.push_back(temp1);
                    m_vSweptQuad.push_back(temp2);
                    m_vSweptQuad.push_back(temp3);
                    m_vSweptQuad.push_back(temp4);
                    imstk::StdVectorOfVec3d vertListSwept;
                    std::vector<imstk::SurfaceMesh::TriangleArray> trianglesSwept;
                    vertListSwept.push_back(temp1);
                    vertListSwept.push_back(temp2);
                    vertListSwept.push_back(temp3);
                    vertListSwept.push_back(temp4);
                    trianglesSwept.push_back({ { 0, 1, 2 } });
                    trianglesSwept.push_back({ { 2, 1, 3 } });
                    auto surfMeshSwept = std::make_shared<imstk::SurfaceMesh>();
                    surfMeshSwept->initialize(vertListSwept, trianglesSwept);

                    int res = 0;
                    res = cutTissueMesh->cutNew(m_vBladeSegments, m_vSweptQuad, true);

                    // after cutting ,update graph
                    auto& tetVertices = cutTissueMesh->getInitTetrahedraVertices();

                    // after cutting
                    auto& tetList = cutTissueMesh->getCutElements();
                    auto& listGraphVertices = cutTissueMesh->getGraphVertices();
                    auto& listGraphTets = cutTissueMesh->getGraphTetrahedrons();
                    auto& currentTetStates = cutTissueMesh->getTetrahedronStates();
                    auto& graphVolume = cutTissueMesh->getGraphVolume();
                    std::vector<std::vector<int>> graphVolumeCutting;
                    int numGraph = listGraphVertices.size();
                    for (int i = 0; i < numGraph; i++)
                    {
                        std::vector<int> glist;
                        for (int j = 0; j < numGraph; j++)
                        {
                            int graphMatrix = 0;
                            if (j == i)
                            {
                                graphMatrix = 0;
                            }
                            else
                            {
                                int iK = listGraphVertices[i];
                                int jK = listGraphVertices[j];
                                //bool isConnected = false;
                                for (int m = 0; m < listGraphTets.size(); m++)
                                {
                                    int& tetID = listGraphTets[m];
                                    if (!currentTetStates[tetID]) continue; // deleted

                                    auto& tetTmp = tetVertices[tetID];
                                    if (((iK == tetTmp[0]) || (iK == tetTmp[1]) || (iK == tetTmp[2]) || (iK == tetTmp[3])) && ((jK == tetTmp[0]) || (jK == tetTmp[1]) || (jK == tetTmp[2]) || (jK == tetTmp[3])))
                                    {
                                        graphMatrix = 1;
                                        break;
                                    }
                                }
                                //if (isConnected)
                                //{
                                //    graphMatrix = 1;
                                //}
                            }
                            glist.push_back(graphMatrix);
                        } // END for inside
                        graphVolumeCutting.push_back(glist);
                    } // END for outside
                    graphVolume->initGraph();
                    graphVolume->updateGraph(graphVolumeCutting);
                    graphVolume->traverseGraph();
                    printf("block volume graph:\n");
                    for (int t = 0; t < numGraph; t++)
                    {
                        printf("%d\n", graphVolume->getID(t));
                    }
                    printf("\n\n");
                    FILE *fileGraphV = fopen("i:/volumeGraph.dat", "w");
                    for (int i = 0; i < numGraph;i++)
                    {
                        for (int j = 0; j < numGraph;j++)
                        {
                            fprintf(fileGraphV, "%d ", graphVolumeCutting[i][j]);
                        }
                        fprintf(fileGraphV, "\n");
                    }
                    fclose(fileGraphV);
                    // end volume cutting

                    // Method1:  finding out the related triangles in visual surface mesh from vecCutElements
                    std::vector<U32> cutNodesVisual;
                    std::vector<physXLink> links = cutTissueMesh->getPhysXLink();
                    for (int m = 0; m < links.size(); m++) // traverse the whole links
                    {
                        auto link = links[m];
                        std::vector<U32>::iterator result = find(tetList.begin(), tetList.end(), link.tetraIndex);  // find whether 
                        if (result != tetList.end()) // existing
                        {
                            cutNodesVisual.push_back(m); // related nodes in visual surface mesh
                        }
                    }

                    std::vector<U32> m_trianglesNumCut;
                    auto& triLists = visualColonMesh->getTrianglesVerticesChangeable();  // XZH use for change
                    auto& vertPos = visualColonMesh->getVertexPositions();

                    //// Method 2 find out the cut trianges broad phase
                    //auto min_x = std::min(m_vSweptQuad[0].x(), std::min(m_vSweptQuad[1].x(), std::min(m_vSweptQuad[2].x(), m_vSweptQuad[3].x())));
                    //auto max_x = std::max(m_vSweptQuad[0].x(), std::max(m_vSweptQuad[1].x(), std::max(m_vSweptQuad[2].x(), m_vSweptQuad[3].x())));
                    //auto min_y = std::min(m_vSweptQuad[0].y(), std::min(m_vSweptQuad[1].y(), std::min(m_vSweptQuad[2].y(), m_vSweptQuad[3].y())));
                    //auto max_y = std::max(m_vSweptQuad[0].y(), std::max(m_vSweptQuad[1].y(), std::max(m_vSweptQuad[2].y(), m_vSweptQuad[3].y())));
                    //auto min_z = std::min(m_vSweptQuad[0].z(), std::min(m_vSweptQuad[1].z(), std::min(m_vSweptQuad[2].z(), m_vSweptQuad[3].z())));
                    //auto max_z = std::max(m_vSweptQuad[0].z(), std::max(m_vSweptQuad[1].z(), std::max(m_vSweptQuad[2].z(), m_vSweptQuad[3].z())));
                    //cutNodesVisual.clear();
                    //for (int m = 0; m < vertPos.size(); m++)
                    //{
                    //    auto& pos = vertPos[m];
                    //    auto min_x1 = pos.x();
                    //    double prox1 = 1.0;
                    //    double prox2 = 1.0;
                    //    bool res = testTrisAABB(pos.x() - prox1, pos.x() + prox1, pos.y() - prox1, pos.y() + prox1, pos.z() - prox1, pos.z() + prox1,
                    //        min_x1 - prox2, max_x + prox2, min_y - prox2, max_y + prox2, min_z - prox2, max_z + prox2);

                    //    if (res) cutNodesVisual.push_back(m);
                    //} // END for
                    
                    std::vector<SurfaceMesh::TriangleArray> triListsTmp;
                    for (int m = 0; m < triLists.size(); m++)
                    {
                        auto& tri = triLists[m];
                        std::vector<U32>::iterator result = find(cutNodesVisual.begin(), cutNodesVisual.end(), tri[0]);
                        if (result != cutNodesVisual.end()) // existing
                        {
                            m_trianglesNumCut.push_back(m); // related triangle in visual surface mesh
                            //triLists.erase(triLists.begin() + m);
                            continue; // next m
                        }
                        result = find(cutNodesVisual.begin(), cutNodesVisual.end(), tri[1]);
                        if (result != cutNodesVisual.end()) // existing
                        {
                            m_trianglesNumCut.push_back(m); // related triangle in visual surface mesh
                            continue;
                        }
                        result = find(cutNodesVisual.begin(), cutNodesVisual.end(), tri[2]);
                        if (result != cutNodesVisual.end()) // existing
                        {
                            m_trianglesNumCut.push_back(m); // related triangle in visual surface mesh
                            continue;
                        }
                        triListsTmp.push_back(tri); // used for new surface mesh
                    }

                    StdVectorOfVec3d newVertPositions; // deform mesh positions
                    std::list<int> uniqueNewVertIdList; // the vertex list in the deform mesh
                    for (const auto &face : triListsTmp)
                    {
                        uniqueNewVertIdList.push_back(face[0]);
                        uniqueNewVertIdList.push_back(face[1]);
                        uniqueNewVertIdList.push_back(face[2]);
                    }
                    uniqueNewVertIdList.sort();
                    uniqueNewVertIdList.unique();

                    int vertId;
                    std::list<int>::iterator it;
                    // StdVectorOfVec3d vertPositions; 
                    newVertPositions.clear();
                    for (vertId = 0, it = uniqueNewVertIdList.begin(); it != uniqueNewVertIdList.end(); ++vertId, it++)
                    {
                        newVertPositions.push_back(vertPos[*it]); // coords
                        for (auto &face : triListsTmp)  // reference, traverse all the physSTris to check whether this vertex global index number == *it (global index)
                        {
                            for (int i = 0; i < 3; ++i)
                            {
                                if (face[i] == *it)
                                {
                                    face[i] = vertId; // change the global index to local index 
                                }
                            }
                        }
                    } // END for std::list

                    // new colon surface
                    auto surfNewColonMesh = std::make_shared<imstk::SurfaceMesh>();
                    surfNewColonMesh->initialize(newVertPositions, triListsTmp, true); // if true, calculate normal
                    surfNewColonMesh->computeVertexNormals();

                    // sweep surface
                    char nameSweep[256];
                    sprintf(nameSweep, "SweepSurface%d", cutTimes);
                    auto object0 = std::make_shared<imstk::VisualObject>(nameSweep); // "visualColonMesh0");
                    auto material0 = std::make_shared<RenderMaterial>();
                    material0->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME SURFACE
                    auto tmp0 = std::make_shared<Texture>("I:/iMSTK/resources/VESS/testtest.png", Texture::Type::DIFFUSE);
                    material0->addTexture(tmp0);
                    surfMeshSwept->setRenderMaterial(material0);
                    object0->setVisualGeometry(surfMeshSwept); // change to any mesh created above
                    //m_scene->addSceneObject(object0);
                    cutTimes++;

                    // cutting
                    auto& posVisualList = visualColonMesh->getVertexPositionsChangeable(); // changable 
                    int numOrigin = visualColonMesh->getVertexPositions().size(); // original vertex size after previous cut
                    int numOriginInit = visualColonMesh->getInitialVertexPositions().size(); // original init vertex size
                    //vars
                    Vec3d uvw, xyz, ss0, ss1;
                    double t;
                    Vec3d tri1[3] = { m_vSweptQuad[0], m_vSweptQuad[2], m_vSweptQuad[1] };
                    Vec3d tri2[3] = { m_vSweptQuad[2], m_vSweptQuad[3], m_vSweptQuad[1] };
                    res = 0;
                    imstk::StdVectorOfVec3d newVertList;
                    std::vector<Vec3d> newVertListTriangulation; //
                    std::list<size_t> newVertIndex;
                    std::vector<imstk::SurfaceMesh::TriangleArray> newTriangles;

                    //// Method 2 find out the cut trianges broad phase
                    //auto min_x = std::min(m_vSweptQuad[0].x(), std::min(m_vSweptQuad[1].x(), std::min(m_vSweptQuad[2].x(), m_vSweptQuad[3].x())));
                    //auto max_x = std::max(m_vSweptQuad[0].x(), std::max(m_vSweptQuad[1].x(), std::max(m_vSweptQuad[2].x(), m_vSweptQuad[3].x())));
                    //auto min_y = std::min(m_vSweptQuad[0].y(), std::min(m_vSweptQuad[1].y(), std::min(m_vSweptQuad[2].y(), m_vSweptQuad[3].y())));
                    //auto max_y = std::max(m_vSweptQuad[0].y(), std::max(m_vSweptQuad[1].y(), std::max(m_vSweptQuad[2].y(), m_vSweptQuad[3].y())));
                    //auto min_z = std::min(m_vSweptQuad[0].z(), std::min(m_vSweptQuad[1].z(), std::min(m_vSweptQuad[2].z(), m_vSweptQuad[3].z())));
                    //auto max_z = std::max(m_vSweptQuad[0].z(), std::max(m_vSweptQuad[1].z(), std::max(m_vSweptQuad[2].z(), m_vSweptQuad[3].z())));
                    //m_trianglesNumCut.clear();
                    //for (int m = 0; m < triLists.size(); m++)
                    //{
                    //    auto& tri = triLists[m];
                    //    auto vPos0 = posVisualList[tri[0]];
                    //    auto vPos1 = posVisualList[tri[1]];
                    //    auto vPos2 = posVisualList[tri[2]];
                    //    auto min_x1 = std::min(vPos0.x(), std::min(vPos1.x(), vPos2.x()));
                    //    auto max_x1 = std::max(vPos0.x(), std::max(vPos1.x(), vPos2.x()));
                    //    auto min_y1 = std::min(vPos0.y(), std::min(vPos1.y(), vPos2.y()));
                    //    auto max_y1 = std::max(vPos0.y(), std::max(vPos1.y(), vPos2.y()));
                    //    auto min_z1 = std::min(vPos0.z(), std::min(vPos1.z(), vPos2.z()));
                    //    auto max_z1 = std::max(vPos0.z(), std::max(vPos1.z(), vPos2.z()));
                    //    double prox1 = 0.4;
                    //    double prox2 = 0.4;
                    //    bool res = testTrisAABB(min_x - prox1, max_x + prox1, min_y - prox1, max_y + prox1, min_z - prox1, max_z + prox1,
                    //        min_x1 - prox2, max_x1 + prox2, min_y1 - prox2, max_y1 + prox2, min_z1 - prox2, max_z1 + prox2);
                    //    
                    //    if (res) m_trianglesNumCut.push_back(m);                        
                    //} // END for
                    // temp way, should change a broad phase way
                    m_trianglesNumCut.clear();
                    for (int m = 0; m < triLists.size(); m++)
                    {
                        m_trianglesNumCut.push_back(m);
                    } // END for

                    //  intersectoin: finding out the partial cutting triangle
                    std::vector<int> triangleEffectedEdges;
                    std::vector<int> triangleEffectedPartials;
                    for (int m = 0; m < m_trianglesNumCut.size(); m++)
                    {
                        auto& tri = triLists[m_trianglesNumCut[m]];
                        ss0 = posVisualList[tri[0]];
                        ss1 = posVisualList[tri[1]];
                        double deltaLen = (ss0 - ss1).norm();

                        int edgeNum = 0;
                        res = IntersectSegmentTriangle(ss0, ss1, tri1, t, uvw, xyz); //  6  9: res ← IntersectSegmentTri(edge0, edge1, tri0, p)
                        if (res == 0)
                        {
                            res = IntersectSegmentTriangle(ss0, ss1, tri2, t, uvw, xyz);
                            if (res > 0)
                            {
                                edgeNum++;
                            }
                        }
                        else // >0
                        {
                            edgeNum++;
                        }

                        // edge 1
                        ss0 = posVisualList[tri[1]];
                        ss1 = posVisualList[tri[2]];

                        res = IntersectSegmentTriangle(ss0, ss1, tri1, t, uvw, xyz); //  6  9: res ← IntersectSegmentTri(edge0, edge1, tri0, p)
                        if (res == 0)
                        {
                            res = IntersectSegmentTriangle(ss0, ss1, tri2, t, uvw, xyz);
                            if (res > 0)
                            {
                                edgeNum++;
                            }
                        }
                        else // >0
                        {
                            edgeNum++;
                        }

                        // edge 2
                        ss0 = posVisualList[tri[0]];
                        ss1 = posVisualList[tri[2]];

                        res = IntersectSegmentTriangle(ss0, ss1, tri1, t, uvw, xyz); //  6  9: res ← IntersectSegmentTri(edge0, edge1, tri0, p)
                        if (res == 0)
                        {
                            res = IntersectSegmentTriangle(ss0, ss1, tri2, t, uvw, xyz);
                            if (res > 0)
                            {
                                edgeNum++;
                            }
                        }
                        else // >0
                        {
                            edgeNum++;
                        }

                        triangleEffectedEdges.push_back(edgeNum);
                        if (edgeNum==1)
                        {
                            triangleEffectedPartials.push_back(m);  // partial cutting
                        }
                    }
                    // moving the closest vertex to the cutting line
                    Vec3d ss2;
                    for (int i = 0; i < triangleEffectedPartials.size();i++)
                    {
                        auto& tri = triLists[m_trianglesNumCut[triangleEffectedPartials[i]]];
                        Vec3d triSweep[3] = { posVisualList[tri[0]], posVisualList[tri[1]], posVisualList[tri[2]] };
                        double minDist = 1000000.00;
                        int minVert = -1;
                        printf("PartialCuttingTriangle: %d : %d %d %d\n", m_trianglesNumCut[triangleEffectedPartials[i]], tri[0], tri[1], tri[2]);

                        ss0 = tri1[0];
                        ss1 = tri1[1];
                        res = IntersectSegmentTriangle(ss0, ss1, triSweep, t, uvw, xyz);
                        if (res> 0) // cut
                        {
                            Vec3d temp = ss0 + (ss1 - ss0).normalized() * t;
                            for (int e = 0; e < 3;e++)
                            {
                                double dist = (temp - posVisualList[tri[e]]).norm();
                                if (dist<minDist)
                                {
                                    minDist = dist;
                                    minVert = tri[e]; // global vert index
                                }
                            }
                            visualColonMesh->setVertexPosition(minVert, temp);

                            // sphere for visual
                            char nameTmp[256];
                            sprintf(nameTmp, "PartialSphere%d", minVert);
                            auto visualSphere = std::make_shared<imstk::Sphere>();
                            visualSphere->setTranslation(temp);
                            visualSphere->setRadius(0.05);
                            auto sphere2Obj = std::make_shared<SceneObject>(nameTmp);
                            sphere2Obj->setVisualGeometry(visualSphere);
                            m_scene->addSceneObject(sphere2Obj);
                            continue;
                        }

                        ss0 = tri1[1];
                        ss1 = tri1[2];
                        res = IntersectSegmentTriangle(ss0, ss1, triSweep, t, uvw, xyz);
                        if (res > 0) // cut
                        {
                            Vec3d temp = ss0 + (ss1 - ss0).normalized() * t;
                            for (int e = 0; e < 3; e++)
                            {
                                double dist = (temp - posVisualList[tri[e]]).norm();
                                if (dist < minDist)
                                {
                                    minDist = dist;
                                    minVert = tri[e]; // global vert index
                                }
                            }
                            visualColonMesh->setVertexPosition(minVert, temp);

                            // sphere for visual
                            char nameTmp[256];
                            sprintf(nameTmp, "PartialSphere%d", minVert);
                            auto visualSphere = std::make_shared<imstk::Sphere>();
                            visualSphere->setTranslation(temp);
                            visualSphere->setRadius(0.05);
                            auto sphere2Obj = std::make_shared<SceneObject>(nameTmp);
                            sphere2Obj->setVisualGeometry(visualSphere);
                            m_scene->addSceneObject(sphere2Obj);
                            continue;
                        }

                        ss0 = tri1[0];
                        ss1 = tri1[2];
                        res = IntersectSegmentTriangle(ss0, ss1, triSweep, t, uvw, xyz);
                        if (res > 0) // cut
                        {
                            Vec3d temp = ss0 + (ss1 - ss0).normalized() * t;
                            for (int e = 0; e < 3; e++)
                            {
                                double dist = (temp - posVisualList[tri[e]]).norm();
                                if (dist < minDist)
                                {
                                    minDist = dist;
                                    minVert = tri[e]; // global vert index
                                }
                            }
                            visualColonMesh->setVertexPosition(minVert, temp);

                            // sphere for visual
                            char nameTmp[256];
                            sprintf(nameTmp, "PartialSphere%d", minVert);
                            auto visualSphere = std::make_shared<imstk::Sphere>();
                            visualSphere->setTranslation(temp);
                            visualSphere->setRadius(0.05);
                            auto sphere2Obj = std::make_shared<SceneObject>(nameTmp);
                            sphere2Obj->setVisualGeometry(visualSphere);
                            m_scene->addSceneObject(sphere2Obj);
                            continue;
                        }

                        // tri2 with triangle
                        ss0 = tri2[0];
                        ss1 = tri2[1];
                        res = IntersectSegmentTriangle(ss0, ss1, triSweep, t, uvw, xyz);
                        if (res > 0) // cut
                        {
                            Vec3d temp = ss0 + (ss1 - ss0).normalized() * t;
                            for (int e = 0; e < 3; e++)
                            {
                                double dist = (temp - posVisualList[tri[e]]).norm();
                                if (dist < minDist)
                                {
                                    minDist = dist;
                                    minVert = tri[e]; // global vert index
                                }
                            }
                            visualColonMesh->setVertexPosition(minVert, temp);

                            // sphere for visual
                            char nameTmp[256];
                            sprintf(nameTmp, "PartialSphere%d", minVert);
                            auto visualSphere = std::make_shared<imstk::Sphere>();
                            visualSphere->setTranslation(temp);
                            visualSphere->setRadius(0.05);
                            auto sphere2Obj = std::make_shared<SceneObject>(nameTmp);
                            sphere2Obj->setVisualGeometry(visualSphere);
                            m_scene->addSceneObject(sphere2Obj);
                            continue;
                        }

                        ss0 = tri2[1];
                        ss1 = tri2[2];
                        res = IntersectSegmentTriangle(ss0, ss1, triSweep, t, uvw, xyz);
                        if (res > 0) // cut
                        {
                            Vec3d temp = ss0 + (ss1 - ss0).normalized() * t;
                            for (int e = 0; e < 3; e++)
                            {
                                double dist = (temp - posVisualList[tri[e]]).norm();
                                if (dist < minDist)
                                {
                                    minDist = dist;
                                    minVert = tri[e]; // global vert index
                                }
                            }
                            visualColonMesh->setVertexPosition(minVert, temp);

                            // sphere for visual
                            char nameTmp[256];
                            sprintf(nameTmp, "PartialSphere%d", minVert);
                            auto visualSphere = std::make_shared<imstk::Sphere>();
                            visualSphere->setTranslation(temp);
                            visualSphere->setRadius(0.05);
                            auto sphere2Obj = std::make_shared<SceneObject>(nameTmp);
                            sphere2Obj->setVisualGeometry(visualSphere);
                            m_scene->addSceneObject(sphere2Obj);
                            continue;
                        }

                        ss0 = tri2[0];
                        ss1 = tri2[2];
                        res = IntersectSegmentTriangle(ss0, ss1, triSweep, t, uvw, xyz);
                        if (res > 0) // cut
                        {
                            Vec3d temp = ss0 + (ss1 - ss0).normalized() * t;
                            for (int e = 0; e < 3; e++)
                            {
                                double dist = (temp - posVisualList[tri[e]]).norm();
                                if (dist < minDist)
                                {
                                    minDist = dist;
                                    minVert = tri[e]; // global vert index
                                }
                            }
                            visualColonMesh->setVertexPosition(minVert, temp);

                            // sphere for visual
                            char nameTmp[256];
                            sprintf(nameTmp, "PartialSphere%d", minVert);
                            auto visualSphere = std::make_shared<imstk::Sphere>();
                            visualSphere->setTranslation(temp);
                            visualSphere->setRadius(0.05);
                            auto sphere2Obj = std::make_shared<SceneObject>(nameTmp);
                            sphere2Obj->setVisualGeometry(visualSphere);
                            m_scene->addSceneObject(sphere2Obj);
                            continue;;
                        }
                    }
                    // find out partial cutting edge END

                    // finding out the cutted edges which are needs to be duplicated and moving the vertex to the cut plane
                    std::vector<std::list<int>> triangleEffectedVertices;
                    std::vector<int> triangleEffectedEdgeVertices; // record the cut vertices w.r.t. edge
                    std::list<int> triangleEffectedVertexList; // no repeated vertices list
                    for (int m = 0; m < m_trianglesNumCut.size(); m++)
                    {
                        if (triangleEffectedEdges[m]<1) continue; // reduce for time
                        
                        auto& tri = triLists[m_trianglesNumCut[m]];
                        ss0 = posVisualList[tri[0]];
                        ss1 = posVisualList[tri[1]];
                        double deltaLen = (ss0 - ss1).norm();

                        std::list<int> triangleEffectedVertex;  // no repeat for each triangle
                        int triangleEffectedEdgeVertex=0;  // no repeat for edge vertices in each triangle

                        res = IntersectSegmentTriangle(ss0, ss1, tri1, t, uvw, xyz); //  6  9: res ← IntersectSegmentTri(edge0, edge1, tri0, p)
                        if (res == 0)
                        {
                            res = IntersectSegmentTriangle(ss0, ss1, tri2, t, uvw, xyz);
                            if (res > 0)
                            {
                                triangleEffectedEdgeVertex++;
                                Vec3d temp = ss0 + (ss1 - ss0).normalized() * t;
                                if (t <= 0.5*deltaLen) // move to ss0
                                {
                                    visualColonMesh->setVertexPosition(tri[0], temp);
                                    //posVisualList[tri[0]] = temp; // 
                                    newVertIndex.push_back(tri[0]);
                                    triangleEffectedVertex.push_back(tri[0]);
                                    triangleEffectedVertexList.push_back(tri[0]);
                                }
                                else // move to ss1
                                {
                                    visualColonMesh->setVertexPosition(tri[1], temp);
                                    //posVisualList[tri[1]] = temp;  // 
                                    newVertIndex.push_back(tri[1]);
                                    triangleEffectedVertex.push_back(tri[1]);
                                    triangleEffectedVertexList.push_back(tri[1]);
                                }
                                newVertList.push_back(temp);
                            }
                        }
                        else // >0
                        {
                            triangleEffectedEdgeVertex++;
                            Vec3d temp = ss0 + (ss1 - ss0).normalized() * t;
                            if (t <= 0.5*deltaLen) // move to ss0
                            {
                                visualColonMesh->setVertexPosition(tri[0], temp);
                                //posVisualList[tri[0]] = temp;  // 
                                newVertIndex.push_back(tri[0]);
                                triangleEffectedVertex.push_back(tri[0]);
                                triangleEffectedVertexList.push_back(tri[0]);
                            }
                            else // move to ss1
                            {
                                visualColonMesh->setVertexPosition(tri[1], temp);
                                //posVisualList[tri[1]] = temp;  // 
                                newVertIndex.push_back(tri[1]);
                                triangleEffectedVertex.push_back(tri[1]);
                                triangleEffectedVertexList.push_back(tri[1]);
                            }
                            newVertList.push_back(temp);
                        }

                        // edge 1
                        ss0 = posVisualList[tri[1]];
                        ss1 = posVisualList[tri[2]];

                        res = IntersectSegmentTriangle(ss0, ss1, tri1, t, uvw, xyz); //  6  9: res ← IntersectSegmentTri(edge0, edge1, tri0, p)
                        if (res == 0)
                        {
                            res = IntersectSegmentTriangle(ss0, ss1, tri2, t, uvw, xyz);
                            if (res > 0)
                            {
                                triangleEffectedEdgeVertex++;
                                Vec3d temp = ss0 + (ss1 - ss0).normalized() * t;
                                if (t <= 0.5*deltaLen) // move to ss0
                                {
                                    visualColonMesh->setVertexPosition(tri[1], temp);
                                    //posVisualList[tri[1]] = temp;  // 
                                    newVertIndex.push_back(tri[1]);
                                    triangleEffectedVertex.push_back(tri[1]);
                                    triangleEffectedVertexList.push_back(tri[1]);
                                }
                                else // move to ss1
                                {
                                    visualColonMesh->setVertexPosition(tri[2], temp);
                                    //posVisualList[tri[2]] = temp;  // 
                                    newVertIndex.push_back(tri[2]);
                                    triangleEffectedVertex.push_back(tri[2]);
                                    triangleEffectedVertexList.push_back(tri[2]);
                                }
                                newVertList.push_back(temp);
                            }
                        }
                        else // >0
                        {
                            triangleEffectedEdgeVertex++;
                            Vec3d temp = ss0 + (ss1 - ss0).normalized() * t;
                            if (t <= 0.5*deltaLen) // move to ss0
                            {
                                visualColonMesh->setVertexPosition(tri[1], temp);
                                //posVisualList[tri[1]] = temp;  // 
                                newVertIndex.push_back(tri[1]);
                                triangleEffectedVertex.push_back(tri[1]);
                                triangleEffectedVertexList.push_back(tri[1]);
                            }
                            else // move to ss1
                            {
                                visualColonMesh->setVertexPosition(tri[2], temp);
                                //posVisualList[tri[2]] = temp;  // 
                                newVertIndex.push_back(tri[2]);
                                triangleEffectedVertex.push_back(tri[2]);
                                triangleEffectedVertexList.push_back(tri[2]);
                            }
                            newVertList.push_back(temp);
                        }

                        // edge 2
                        ss0 = posVisualList[tri[0]];
                        ss1 = posVisualList[tri[2]];

                        res = IntersectSegmentTriangle(ss0, ss1, tri1, t, uvw, xyz); //  6  9: res ← IntersectSegmentTri(edge0, edge1, tri0, p)
                        if (res == 0)
                        {
                            res = IntersectSegmentTriangle(ss0, ss1, tri2, t, uvw, xyz);
                            if (res > 0)
                            {
                                triangleEffectedEdgeVertex++;
                                Vec3d temp = ss0 + (ss1 - ss0).normalized() * t;
                                if (t <= 0.5*deltaLen) // move to ss0
                                {
                                    visualColonMesh->setVertexPosition(tri[0], temp);
                                    //posVisualList[tri[0]] = temp;  // 
                                    newVertIndex.push_back(tri[0]);
                                    triangleEffectedVertex.push_back(tri[0]);
                                    triangleEffectedVertexList.push_back(tri[0]);
                                }
                                else // move to ss1
                                {
                                    visualColonMesh->setVertexPosition(tri[2], temp);
                                    //posVisualList[tri[2]] = temp;  // 
                                    newVertIndex.push_back(tri[2]);
                                    triangleEffectedVertex.push_back(tri[2]);
                                    triangleEffectedVertexList.push_back(tri[2]);
                                }
                                newVertList.push_back(temp);
                            }
                        }
                        else // >0
                        {
                            triangleEffectedEdgeVertex++;
                            Vec3d temp = ss0 + (ss1 - ss0).normalized() * t;
                            if (t <= 0.5*deltaLen) // move to ss0
                            {
                                visualColonMesh->setVertexPosition(tri[0], temp);
                                //posVisualList[tri[0]] = temp;  // 
                                newVertIndex.push_back(tri[0]);
                                triangleEffectedVertex.push_back(tri[0]);
                                triangleEffectedVertexList.push_back(tri[0]);
                            }
                            else // move to ss1
                            {
                                visualColonMesh->setVertexPosition(tri[2], temp);
                                //posVisualList[tri[2]] = temp;  // 
                                newVertIndex.push_back(tri[2]);
                                triangleEffectedVertex.push_back(tri[2]);
                                triangleEffectedVertexList.push_back(tri[2]);
                            }
                            newVertList.push_back(temp);
                        }
                        triangleEffectedVertex.sort();
                        triangleEffectedVertex.unique();
                        triangleEffectedVertices.push_back(triangleEffectedVertex);

                        triangleEffectedEdgeVertices.push_back(triangleEffectedEdgeVertex);
                    }
                    triangleEffectedVertexList.sort();
                    triangleEffectedVertexList.unique();
                    std::unordered_multimap< int, int > mapOrigin2New;  //::iterator it = mesh2->m_mapEdgeTriIndex.find(edgekey);
                    std::list<int> listEffectedVertices;

                    // duplicate the vertices
                    for (int i = 0; i < triangleEffectedVertices.size(); i++)
                    {
                        auto& effectedVert = triangleEffectedVertices[i]; // effected vert global index
                        auto& tri = triLists[m_trianglesNumCut[i]]; // triangle vert global index
                        if (effectedVert.size()>1) // should be duplicated
                        {
                            std::list<int>::iterator itt;
                            for (vertId = 0, itt = effectedVert.begin(); itt != effectedVert.end(); ++vertId, itt++)
                            {
                                bool isExisted = false;
                                for (std::unordered_multimap< int, int >::iterator it2 = mapOrigin2New.begin(); it2 != mapOrigin2New.end(); it2++)
                                {
                                    if (it2->first == *itt) // find out the local vertex index
                                    {
                                        isExisted = true;
                                        break;
                                    }
                                    else
                                    {
                                        isExisted = false;
                                    }
                                }
                                if (!isExisted) // false exec
                                {
                                    posVisualList.push_back(posVisualList[*itt]); // coords
                                    int num = posVisualList.size() - 1;
                                    visualColonMesh->setVertexPosition(num, posVisualList[*itt]); // update
                                    mapOrigin2New.insert(std::make_pair(*itt, num));
                                    mapOrigin2New.insert(std::make_pair(*itt, *itt));
                                    listEffectedVertices.push_back(*itt);
                                }
                            } // END for std::list
                        }
                    }
                    listEffectedVertices.sort();
                    listEffectedVertices.unique();
                    std::list<int>::iterator itt2;
                    triangleEffectedVertices.clear();
                    int vid;
                    for (int m = 0; m < m_trianglesNumCut.size(); m++)
                    {
                        auto& tri = triLists[m_trianglesNumCut[m]];
                        std::list<int> triangleEffectedVertex;  // no repeat for each triangle
                        for (vid = 0, itt2 = listEffectedVertices.begin(); itt2 != listEffectedVertices.end(); ++vid, itt2++)
                        {
                            if (tri[0] == *itt2)
                            {
                                triangleEffectedVertex.push_back(tri[0]);
                            }
                            if (tri[1] == *itt2)
                            {
                                triangleEffectedVertex.push_back(tri[1]);
                            }
                            if (tri[2] == *itt2)
                            {
                                triangleEffectedVertex.push_back(tri[2]);
                            }
                        }
                        triangleEffectedVertices.push_back(triangleEffectedVertex);
                    }

                    for (int i = 0; i < triangleEffectedVertices.size(); i++)
                    {
                        auto& effectedVert = triangleEffectedVertices[i]; // effected vert global index
                        auto& tri = triLists[m_trianglesNumCut[i]]; // triangle vert global index
                        if (effectedVert.size()>1) // should be duplicated
                        {
                            printf("===%d: %d %d %d\n", i, tri[0], tri[1], tri[2]);
                        }
                    }

                    char name[256];
                    //int numOrigin = visualColonMesh->getInitialVertexPositions().size(); // original vertex size
                    Vec3d normCut = (tri1[1] - tri1[0]).cross(tri1[2] - tri1[0]).normalized();
                    for (int i = 0; i < triangleEffectedVertices.size(); i++)
                    {
                        auto& effectedVert = triangleEffectedVertices[i]; // effected vert global index
                        auto& tri = triLists[m_trianglesNumCut[i]]; // triangle vert global index
                        if (effectedVert.size()>1) // should be duplicated
                        {
                            std::list<int>::iterator itt;
                            int nTmp[2];
                            nTmp[0] = nTmp[1] = -1;
                            for (vertId = 0, itt = effectedVert.begin(); itt != effectedVert.end(); ++vertId, itt++)
                            {
                                nTmp[vertId] = *itt;
                            }

                            int numVert = -1;
                            int numIdxTmp = -1;
                            for (int k = 0; k < 3; k++)
                            {
                                int numTmp = tri[k];
                                if ((numTmp != nTmp[0]) && (numTmp != nTmp[1]))
                                {
                                    numVert = numTmp;
                                    numIdxTmp = k;
                                }
                            }

                            Vec3d vecVert = posVisualList[numVert] - 0.333333*(posVisualList[nTmp[0]] + posVisualList[nTmp[1]] + posVisualList[numVert]);
                            Vec3d vecCenter = 0.333333*(posVisualList[tri[0]] + posVisualList[tri[1]] + posVisualList[tri[2]]);
                            vecVert = vecCenter - tri1[0];
                            int numTmp1 = 0, numTmp2 = 0;
                            int vertIndex1[2], vertIndex2[2];
                            vertIndex1[0] = vertIndex1[1] = vertIndex2[0] = vertIndex2[1] = -1;
                            if (vecVert.dot(normCut) > 0)  // tumor side
                            {
                                int num0 = -1, num1 = -1;
                                for (std::unordered_multimap< int, int >::iterator it2 = mapOrigin2New.begin(); it2 != mapOrigin2New.end(); it2++)
                                {
                                    if (numIdxTmp == 0)
                                    {
                                        if ((it2->first == tri[1]) && (it2->second >= numOrigin)) // 
                                        {
                                            tri[1] = it2->second;
                                            visualColonMesh->setVertexPosition(tri[1], posVisualList[tri[1]] + (vecCenter - posVisualList[tri[1]])*SPLITDIST);
                                        }
                                        else if ((it2->first == tri[2]) && (it2->second >= numOrigin))
                                        {
                                            tri[2] = it2->second;
                                            visualColonMesh->setVertexPosition(tri[2], posVisualList[tri[2]] + (vecCenter - posVisualList[tri[2]])*SPLITDIST);
                                        }
                                        num0 = tri[1]; num1 = tri[2]; // for graph
                                    }
                                    else if (numIdxTmp == 1)
                                    {
                                        if ((it2->first == tri[0]) && (it2->second >= numOrigin)) // 
                                        {
                                            tri[0] = it2->second;
                                            visualColonMesh->setVertexPosition(tri[0], posVisualList[tri[0]] + (vecCenter - posVisualList[tri[0]])*SPLITDIST);
                                        }
                                        else if ((it2->first == tri[2]) && (it2->second >= numOrigin))
                                        {
                                            tri[2] = it2->second;
                                            visualColonMesh->setVertexPosition(tri[2], posVisualList[tri[2]] + (vecCenter - posVisualList[tri[2]])*SPLITDIST);
                                        }
                                        num0 = tri[0]; num1 = tri[2]; // for graph
                                    }
                                    else if (numIdxTmp == 2)
                                    {
                                        if ((it2->first == tri[0]) && (it2->second >= numOrigin)) // 
                                        {
                                            tri[0] = it2->second;
                                            visualColonMesh->setVertexPosition(tri[0], posVisualList[tri[0]] + (vecCenter - posVisualList[tri[0]])*SPLITDIST);
                                        }
                                        else if ((it2->first == tri[1]) && (it2->second >= numOrigin))
                                        {
                                            tri[1] = it2->second;
                                            visualColonMesh->setVertexPosition(tri[1], posVisualList[tri[1]] + (vecCenter - posVisualList[tri[1]])*SPLITDIST);
                                        }
                                        num0 = tri[0]; num1 = tri[1]; // for graph
                                    }
                                }

                                // update graph just >=numOriginInit
                                num0 -= numOriginInit;
                                num1 -= numOriginInit;
                                int cntnew = posVisualList.size() - numOriginInit;
                                for (int in = 0; in < cntnew;in++)
                                {
                                    for (int jn = 0; jn < cntnew;jn++)
                                    {
                                        if ((in==num0) && (jn==num1))
                                        {
                                            graphVisualCutting[in][jn] = 1;
                                        }
                                        else if ((in == num1) && (jn == num0))
                                        {
                                            graphVisualCutting[in][jn] = 1;
                                        }
                                    }
                                } // update
                            }
                            else // colon side
                            {
                                for (std::unordered_multimap< int, int >::iterator it2 = mapOrigin2New.begin(); it2 != mapOrigin2New.end(); it2++)
                                {
                                    if (numIdxTmp == 0)
                                    {
                                        if ((it2->first == tri[1]) && (it2->second < numOrigin)) // 
                                        {
                                            tri[1] = it2->first;
                                            visualColonMesh->setVertexPosition(tri[1], posVisualList[tri[1]] + (vecCenter - posVisualList[tri[1]])*SPLITDIST);
                                        }
                                        else if ((it2->first == tri[2]) && (it2->second < numOrigin))
                                        {
                                            tri[2] = it2->first;
                                            visualColonMesh->setVertexPosition(tri[2], posVisualList[tri[2]] + (vecCenter - posVisualList[tri[2]])*SPLITDIST);
                                        }
                                    }
                                    else if (numIdxTmp == 1)
                                    {
                                        if ((it2->first == tri[0]) && (it2->second < numOrigin)) // 
                                        {
                                            tri[0] = it2->first;
                                            visualColonMesh->setVertexPosition(tri[0], posVisualList[tri[0]] + (vecCenter - posVisualList[tri[0]])*SPLITDIST);
                                        }
                                        else if ((it2->first == tri[2]) && (it2->second < numOrigin))
                                        {
                                            tri[2] = it2->first;
                                            visualColonMesh->setVertexPosition(tri[2], posVisualList[tri[2]] + (vecCenter - posVisualList[tri[2]])*SPLITDIST);
                                        }
                                    }
                                    else if (numIdxTmp == 2)
                                    {
                                        if ((it2->first == tri[0]) && (it2->second < numOrigin)) // 
                                        {
                                            tri[0] = it2->first;
                                            visualColonMesh->setVertexPosition(tri[0], posVisualList[tri[0]] + (vecCenter - posVisualList[tri[0]])*SPLITDIST);
                                        }
                                        else if ((it2->first == tri[1]) && (it2->second < numOrigin))
                                        {
                                            tri[1] = it2->first;
                                            visualColonMesh->setVertexPosition(tri[1], posVisualList[tri[1]] + (vecCenter - posVisualList[tri[1]])*SPLITDIST);
                                        }
                                    }
                                } // END for
                            }  // END else
                            printf("%d: %d %d %d\n", i, tri[0], tri[1], tri[2]);
                        }
                    }          
                    // test graph with file data
                    const int NN = 6;
                    int jz[NN][NN];
                    FILE*infile = fopen("i:/graph.dat", "r");
                    for (int s = 0; s < NN; s++)
                    {
                        fscanf(infile, "%d %d %d %d %d %d", &jz[s][0], &jz[s][1], &jz[s][2], &jz[s][3], &jz[s][4], &jz[s][5]);
                    }
                    fclose(infile);
                    // if there is a cycle
                    int graphSize = posVisualList.size() - numOriginInit;

                    //graphSize = NN;
                    //for (int im = 0; im < graphSize; im++)
                    //{
                    //    for (int in = 0; in < graphSize; in++)
                    //    {
                    //        graphVisualCutting[im][in] = jz[im][in];
                    //    }
                    //}
                    m_graphVisualCutting->setGraphSize(graphSize);
                    m_graphVisualCutting->initGraph();
                    m_graphVisualCutting->updateGraph(graphVisualCutting);
                    m_graphVisualCutting->depthFirstSearch(0, -1); // graphVisualCutting
                    m_graphVisualCutting->traverseGraph(); //  m_graphVisualCutting->depthFirstSearch(0); // conn
                    if (m_graphVisualCutting->getHasCycle())
                    {
                        printf("\nthere is cycle");
                    }
                    printf("block graph:\n");
                    for (int t = 0; t < graphSize;t++)
                    {
                        printf("%d\n", m_graphVisualCutting->getID(t));
                    }
                    FILE *fileGraph = fopen("i:/visualGraph.dat", "w");
                    fprintf(fileGraph, "%d:\n", graphSize);
                    for (int i = 0; i < graphSize;i++)
                    {
                        for (int j = 0; j < graphSize;j++)
                        {
                            fprintf(fileGraph, "%d ", graphVisualCutting[i][j]);
                        }
                        fprintf(fileGraph, "\n");
                    }
                    fclose(fileGraph);
                    printf("\n\n");
                    // for one vertex by cut
                    for (int i = 0; i < triangleEffectedVertices.size(); i++)
                    {
                        auto& effectedVert = triangleEffectedVertices[i]; // effected vert global index
                        auto& tri = triLists[m_trianglesNumCut[i]]; // triangle vert global index
                        if (effectedVert.size() == 1) // should not be duplicated
                        {
                            std::list<int>::iterator itt;
                            int nTmp;
                            for (vertId = 0, itt = effectedVert.begin(); itt != effectedVert.end(); ++vertId, itt++)
                            {
                                nTmp = *itt;
                            }

                            int numVert = -1;
                            int numIdxTmp = -1;
                            for (int k = 0; k < 3; k++)
                            {
                                int numTmp = tri[k];
                                if (numTmp == nTmp)
                                {
                                    numVert = numTmp;
                                    numIdxTmp = k;
                                }
                            }

                            Vec3d vecCenter = 0.333333*(posVisualList[tri[0]] + posVisualList[tri[1]] + posVisualList[tri[2]]);
                            Vec3d vecVert = vecCenter - tri1[0]; //  posVisualList[numVert];
                            int numTmp1 = 0, numTmp2 = 0;
                            int vertIndex1[2], vertIndex2[2];
                            vertIndex1[0] = vertIndex1[1] = vertIndex2[0] = vertIndex2[1] = -1;
                            if (vecVert.dot(normCut) > 0)  // tumor side
                            {
                                for (std::unordered_multimap< int, int >::iterator it2 = mapOrigin2New.begin(); it2 != mapOrigin2New.end(); it2++)
                                {
                                    if (numIdxTmp == 0)
                                    {
                                        if ((it2->first == tri[0]) && (it2->second >= numOrigin)) // 
                                        {
                                            tri[0] = it2->second;
                                            visualColonMesh->setVertexPosition(tri[0], posVisualList[tri[0]] + (vecCenter - posVisualList[tri[0]])*SPLITDIST);
                                        }
                                    }
                                    else if (numIdxTmp == 1)
                                    {
                                        if ((it2->first == tri[1]) && (it2->second >= numOrigin)) // 
                                        {
                                            tri[1] = it2->second;
                                            visualColonMesh->setVertexPosition(tri[1], posVisualList[tri[1]] + (vecCenter - posVisualList[tri[1]])*SPLITDIST);
                                        }
                                    }
                                    else if (numIdxTmp == 2)
                                    {
                                        if ((it2->first == tri[2]) && (it2->second >= numOrigin)) // 
                                        {
                                            tri[2] = it2->second;
                                            visualColonMesh->setVertexPosition(tri[2], posVisualList[tri[2]] + (vecCenter - posVisualList[tri[2]])*SPLITDIST);
                                        }
                                    }
                                }
                            }
                            else // colon side
                            {
                                for (std::unordered_multimap< int, int >::iterator it2 = mapOrigin2New.begin(); it2 != mapOrigin2New.end(); it2++)
                                {
                                    if (numIdxTmp == 0)
                                    {
                                        if ((it2->first == tri[0]) && (it2->second < numOrigin)) // 
                                        {
                                            tri[0] = it2->first;
                                            visualColonMesh->setVertexPosition(tri[0], posVisualList[tri[0]] + (vecCenter - posVisualList[tri[0]])*SPLITDIST);
                                        }
                                    }
                                    else if (numIdxTmp == 1)
                                    {
                                        if ((it2->first == tri[1]) && (it2->second < numOrigin)) // 
                                        {
                                            tri[1] = it2->first;
                                            visualColonMesh->setVertexPosition(tri[1], posVisualList[tri[1]] + (vecCenter - posVisualList[tri[1]])*SPLITDIST);
                                        }
                                    }
                                    else if (numIdxTmp == 2)
                                    {
                                        if ((it2->first == tri[2]) && (it2->second < numOrigin)) // 
                                        {
                                            tri[2] = it2->first;
                                            visualColonMesh->setVertexPosition(tri[2], posVisualList[tri[2]] + (vecCenter - posVisualList[tri[2]])*SPLITDIST);
                                        }
                                    }
                                } // END for
                            }  // END else
                            printf("%d: %d %d %d\n", i, tri[0], tri[1], tri[2]);
                        }
                    }

                    //// sphere list
                    //for (int i = 0; i < triangleEffectedVertices.size(); i++)
                    //{
                    //    auto& effectedVert = triangleEffectedVertices[i]; // effected vert global index
                    //    auto& tri = triLists[m_trianglesNumCut[i]]; // triangle vert global index
                    //    if (effectedVert.size() == 1) // should be duplicated
                    //    {
                    //        sprintf(name, "XZHSphere%d", i);
                    //        auto visualSphere22 = std::make_shared<imstk::Sphere>();
                    //        visualSphere22->setTranslation(posVisualList[tri[0]]);
                    //        visualSphere22->setRadius(0.1);
                    //        auto sphere2Obj = std::make_shared<SceneObject>(name);
                    //        sphere2Obj->setVisualGeometry(visualSphere22);
                    //        scene->addSceneObject(sphere2Obj);

                    //        sprintf(name, "XZHSphere%d", i * 1000 + 1);
                    //        auto visualSphere222 = std::make_shared<imstk::Sphere>();
                    //        visualSphere222->setTranslation(posVisualList[tri[1]]);
                    //        visualSphere222->setRadius(0.1);
                    //        auto sphere22Obj = std::make_shared<SceneObject>(name);
                    //        sphere22Obj->setVisualGeometry(visualSphere222);
                    //        scene->addSceneObject(sphere22Obj);

                    //        sprintf(name, "XZHSphere%d", i * 1000 + 2);
                    //        auto visualSphere2222 = std::make_shared<imstk::Sphere>();
                    //        visualSphere2222->setTranslation(posVisualList[tri[2]]);
                    //        visualSphere2222->setRadius(0.1);
                    //        auto sphere222Obj = std::make_shared<SceneObject>(name);
                    //        sphere222Obj->setVisualGeometry(visualSphere2222);
                    //        scene->addSceneObject(sphere222Obj);
                    //    }
                    //}

                    int mTmp = 0;
                    for (int i = numOriginInit /*477*/; i < posVisualList.size(); i++) 
                    {
                        mTmp++;
                        sprintf(name, "XZHSphere%d", i);
                        auto visualSphere22 = std::make_shared<imstk::Sphere>();
                        visualSphere22->setTranslation(posVisualList[i]);
                        visualSphere22->setRadius(0.2);
                        auto sphere2Obj = std::make_shared<SceneObject>(name);
                        sphere2Obj->setVisualGeometry(visualSphere22);
                        //if (mTmp==7)
                        {
                            m_scene->addSceneObject(sphere2Obj);
                        }

                    }
                    int idxList[10] = { 354, 22, 96, 99, 98, 355, 105, 404, 280, 400 };
                    for (int i = 0; i < 10; i++)
                    {
                        sprintf(name, "XZHSphere%d", idxList[i]);
                        auto visualSphere22 = std::make_shared<imstk::Sphere>();
                        visualSphere22->setTranslation(posVisualList[idxList[i]]);
                        visualSphere22->setRadius(0.1);
                        auto sphere2Obj = std::make_shared<SceneObject>(name);
                        sphere2Obj->setVisualGeometry(visualSphere22);
                        //m_scene->addSceneObject(sphere2Obj);
                    }

                    newVertList.clear();
                    imstk::StdVectorOfVec3d newVertList2;
                    newVertIndex.sort();
                    newVertIndex.unique();
                    std::unordered_map< size_t, size_t> mapFromG2L; // global to local
                    std::unordered_map< size_t, size_t>::iterator itList; // = mesh2->m_mapEdgeTriIndex.find(edgekey);
                    size_t vertID;
                    std::list<size_t>::iterator itt;
                    std::vector<size_t> newVertIndexLocal;
                    auto& posVisualListUpdate = visualColonMesh->getVertexPositions(); // changable 
                    for (vertID = 0, itt = newVertIndex.begin(); itt != newVertIndex.end(); ++vertID, itt++)
                    {
                        newVertList.push_back(posVisualListUpdate[*itt]);
                        newVertList2.push_back(posVisualListUpdate[*itt] + Vec3d(0, 0, 10));
                        mapFromG2L.insert(make_pair(*itt, vertID));
                        newVertIndexLocal.push_back(vertID);
                    }

                    std::vector<SurfaceMesh::TriangleArray> surfaceNewTri;
                    using triArray = SurfaceMesh::TriangleArray;
                    int mCnt = newVertIndexLocal.size();


                    // generating new surface mesh based on the cutted vertex
                    auto delaunayTriangulation = std::make_shared<imstk::Delaunay>();
                    for (auto& vert : newVertList)
                    {
                        newVertListTriangulation.push_back(vert);
                    }

                    std::vector<Triangle> triangles = delaunayTriangulation->triangulate(newVertListTriangulation);
                    std::unordered_map< size_t, Vec3d> mapNewSurface; // global to local
                    std::unordered_map< size_t, Vec3d>::iterator itnew; // = mesh2->m_mapEdgeTriIndex.find(edgekey);
                    newVertList.clear();

                    int cnt = 0;
                    for (int k = 0; k < triangles.size(); k++)
                    {
                        int num[3];
                        bool isin = false;
                        for (int m = 0; m < 3; m++)
                        {
                            Vec3d temp;
                            switch (m)
                            {
                            case 0:
                                temp = triangles[k].p1;
                                isin = false;
                                for (itnew = mapNewSurface.begin(); itnew != mapNewSurface.end(); itnew++)
                                {
                                    if (itnew->second == temp)  // in the original is the same vertex
                                    {
                                        isin = true; // existing
                                        num[0] = itnew->first;
                                        break;;  // next m
                                    }
                                }
                                if (!isin) // not existing
                                {
                                    mapNewSurface.insert(make_pair(cnt, temp));
                                    newVertList.push_back(temp);
                                    num[0] = cnt;
                                    cnt++;
                                }
                                break;
                            case 1:
                                temp = triangles[k].p2;
                                isin = false;
                                for (itnew = mapNewSurface.begin(); itnew != mapNewSurface.end(); itnew++)
                                {
                                    if (itnew->second == temp)  // in the original is the same vertex
                                    {
                                        isin = true; // existing
                                        num[1] = itnew->first;
                                        break;;  // next m
                                    }
                                }
                                if (!isin) // not existing
                                {
                                    mapNewSurface.insert(make_pair(cnt, temp));
                                    newVertList.push_back(temp);
                                    num[1] = cnt;
                                    cnt++;
                                }
                                break;
                            case 2:
                                temp = triangles[k].p3;
                                isin = false;
                                for (itnew = mapNewSurface.begin(); itnew != mapNewSurface.end(); itnew++)
                                {
                                    if (itnew->second == temp)  // in the original is the same vertex
                                    {
                                        isin = true; // existing
                                        num[2] = itnew->first;
                                        break;;  // next m
                                    }
                                }
                                if (!isin) // not existing
                                {
                                    mapNewSurface.insert(make_pair(cnt, temp));
                                    newVertList.push_back(temp);
                                    num[2] = cnt;
                                    cnt++;
                                }
                                break;
                            }
                        }
                        surfaceNewTri.push_back(triArray{ { num[0], num[1], num[2] } });
                    }
                    auto surfMeshVisualNew = std::make_shared<imstk::SurfaceMesh>();
                    surfMeshVisualNew->initialize(newVertList, surfaceNewTri);

                    //visualColonMesh->setInitialVertexPositions(posVisualList);
                    visualColonMesh->setVertexPositions(posVisualList);
                    visualColonMesh->setTrianglesVertices(triLists);
                    visualColonMesh->setTopologyChangedFlag(true);
                    //visualColonMesh->initialize(posVisualList, triLists, true);
                    //tetTriMap->compute(); // needs to be re-calucated

                    auto surfNewGenerate = std::make_shared<imstk::SurfaceMesh>();
                    surfNewGenerate->setInitialVertexPositions(posVisualList);
                    surfNewGenerate->setVertexPositions(posVisualList);
                    surfNewGenerate->setTrianglesVertices(triLists);
                    surfNewGenerate->setTopologyChangedFlag(true);
                    surfNewGenerate->initialize(posVisualList, triLists, true);
                    auto objectVisualNewGenerate = std::make_shared<imstk::VisualObject>("visualColonNewGenerate");
                    auto materialVisualNewGenerate = std::make_shared<RenderMaterial>();
                    materialVisualNewGenerate->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME SURFACE
                    auto tmpVisualNewGenerate = std::make_shared<Texture>("I:/iMSTK/resources/VESS/testtest.png", Texture::Type::DIFFUSE);
                    materialVisualNewGenerate->addTexture(tmpVisualNewGenerate);
                    surfNewGenerate->setRenderMaterial(materialVisualNewGenerate);
                    objectVisualNewGenerate->setVisualGeometry(surfNewGenerate); // change to any mesh created above
                    //scene->addSceneObject(objectVisualNewGenerate);

                    t2 = GetTickCount();
                    printf("cutting time: %d ms, HZ: %f\n", t2 - t1, 1000 / double(t2 - t1));

                    // surfMeshVisualNew
                    auto objectVisualNew = std::make_shared<imstk::VisualObject>("visualColonMeshVisualNew");
                    auto materialVisualNew = std::make_shared<RenderMaterial>();
                    materialVisualNew->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME SURFACE
                    auto tmpVisualNew = std::make_shared<Texture>("I:/iMSTK/resources/VESS/testtest.png", Texture::Type::DIFFUSE);
                    materialVisualNew->addTexture(tmpVisualNew);
                    surfMeshVisualNew->setRenderMaterial(materialVisualNew);
                    objectVisualNew->setVisualGeometry(surfMeshVisualNew); // change to any mesh created above
                    //m_scene->addSceneObject(objectVisualNew);

                    t2 = GetTickCount();
                    printf("cutting time: %d ms, HZ: %f\n", t2 - t1, 1000 / double(t2 - t1));
                    printf("visual cut tris: %d\n", m_trianglesNumCut.size());
                    Sleep(5);

                    isEnd = true;
                }
            } // END if virtual object
        }
        // END using TetTriMap for cube dense

        t2 = GetTickCount();
        //printf("scene body  time %d ms,  frame rate is: %f\n", t2 - t4, 1000 / double(t2 - t4));
        auto timeElapsed = wwt.getTimeElapsed(StopWatch::TimeUnitType::seconds);

        // Update time step size of the dynamic objects
        for (auto obj : m_scene->getSceneObjects())
        {
            if (obj->getType() == SceneObject::Type::Pbd)
            {
                if (auto dynaObj = std::dynamic_pointer_cast<PbdObject>(obj))
                {
                    if (dynaObj->getDynamicalModel()->getTimeStepSizeType() == TimeSteppingType::realTime)
                    {
                        dynaObj->getDynamicalModel()->setTimeStep(timeElapsed);
                    }
                }
            }
            else if (obj->getType() == SceneObject::Type::FEMDeformable)
            {
                if (auto dynaObj = std::dynamic_pointer_cast<DeformableObject>(obj))
                {
                    if (dynaObj->getDynamicalModel()->getTimeStepSizeType() == TimeSteppingType::realTime)
                    {
                        dynaObj->getDynamicalModel()->setTimeStep(timeElapsed);
                    }
                }
            }
        }


    }

    void
        SceneManager::cleanUpModule()
    {
        // End Camera Controller
        if (auto camController = m_scene->getCamera()->getController())
        {
            camController->end();
            m_threadMap.at(camController->getName()).join();
        }
    }
    void
        SceneManager::startModuleInNewThread(std::shared_ptr<Module> module)
    {
        m_threadMap[module->getName()] = std::thread([module] { module->start(); });
    }
} // imstk
