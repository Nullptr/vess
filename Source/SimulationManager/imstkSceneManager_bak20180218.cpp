﻿/*=========================================================================

Library: iMSTK

Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
& Imaging in Medicine, Rensselaer Polytechnic Institute.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0.txt

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

=========================================================================*/

// imstk
#include "imstkSceneManager.h"
#include "imstkCameraController.h"
#include "imstkSceneObjectController.h"
#include "imstkDynamicObject.h"
#include "imstkPbdObject.h"
#include "imstkDeformableObject.h"
#include "imstkVirtualCouplingPBDObject.h"
#include "imstkPBDVirtualCouplingObject.h" // xzh
#include "imstkPbdSceneObjectController.h" // xzh
#include "imstkDecalPool.h" // XZH
#include "imstkSphere.h" // XZH

#include "imstkGeometryMap.h"
#include "imstkTimer.h"

#include "g3log/g3log.hpp"

#include <windows.h> // for time test XZH
#include<thread>   ////< XZH for parallel
DWORD t3, t4, t1, t2;

namespace imstk
{
      ////< XZH thread
    void SceneManager::runGraph()
    {
        // update visual graph
        m_graphVisualCutting->setGraphSize(graphSize);
        m_graphVisualCutting->initGraph();
        m_graphVisualCutting->updateGraph(graphVisualCutting);
        //m_graphVisualCutting->traverseGraph(); //  m_graphVisualCutting->depthFirstSearch(0); // conn
        //m_graphVisualCutting->dfs();
        if (graphSize > 160)
        {
            m_graphVisualCutting->findAllCircles(graphSize); // all circles another method
        }
        printf("thread cycle elem: %d\n", m_graphVisualCutting->getCycleElemNum());
    }

    void SceneManager::runCutting()
    {
        // XZH  test cutting
        std::shared_ptr<CuttingVESSCutMesh> cutTissueMesh;
        std::shared_ptr<SurfaceMesh> visualColonMesh;
        //using TetTriMap  for cube dense
        for (auto obj : m_scene->getSceneObjects())
        {
            //continue;
            if (obj->getName() == "VirtualObject")
            {
                //if (isEnd)
                //{
                //    endCount++;
                //    if (endCount>=50)
                //    {
                //        isEnd = false;
                //        endCount = 0;
                //        continue; // next for loop
                //    }               
                //}
                auto pbdObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(obj);
                if (!pbdObj) continue;

                auto collidingMesh = std::dynamic_pointer_cast<PointSet>(pbdObj->getCollidingGeometry());
                auto& tool = pbdObj->getControllerTool();
                if (tool.button[0])
                {
                    //printf("start...");
                    if (mCntPressButton<1)
                    {
                        cutStartPre = collidingMesh->getVertexPosition(0); // 0 root
                        cutEndPre = collidingMesh->getVertexPosition(1); // 1 tip
                        cutStartPre = cutEndPre + (cutStartPre - cutEndPre)*0.3;
                        cutEndPre = cutEndPre + (cutEndPre - cutStartPre)*0.5;
                    }
                    else
                    {
                        cutStartPre = cutStart;
                        cutEndPre = cutEnd;
                    }
                    if (mCntPressButton <= 30)
                    {
                        cutStartInit = cutStart;
                        cutEndInit = cutEnd;
                    }
                    mCntPressButton++;
                    //cutStartPre = (cutStartPre + cutEndPre)*0.5;
                    //if (isFirst)
                    //{
                    //    cutStart = collidingMesh->getVertexPosition(0); // 0 root
                    //    cutEnd = collidingMesh->getVertexPosition(1); // 1 tip
                    //    isFirst = false;
                    //}
                    //cutStartPre = cutStart;
                    //cutEndPre = cutEnd;
                    //isStart = true;
                }
                //if (tool.button[1])
                if (tool.button[0] && (!isNoCut) && (mCntPressButton>30))
                {
                    //mCntPressButton = 0;
                    DWORD tt0 = GetTickCount();
                    //isNoCut = false;
                    //printf("end...");
                    switch (mCutState)
                    {
                    case 0:
                    default:
                        mCutState = 1;
                        break;
                    case 1:
                        mCutState = 1;
                        break;
                    case 2:
                        mCutState = 3;
                        break;
                    case 3:
                        mCutState = 3;
                        break;
                    }



                    cutStart = collidingMesh->getVertexPosition(0);
                    cutEnd = collidingMesh->getVertexPosition(1); // 3
                    cutStart = cutEnd + (cutStart - cutEnd)*0.3;
                    cutEnd = cutEnd + (cutEnd - cutStart)*0.5;
                    if ((cutEnd - cutEndInit).norm() > 1.0)
                    {
                        if ((cutEnd - cutEndInit).norm() < 0.3)
                        {
                            cutStart = cutStartInit;
                            cutEnd = cutEndInit;
                        }
                    }
                    

                    //cutStart = (cutStart + cutEnd)*0.5;

                    //if ((cutStart - cutStartPre).norm() < 0.02) continue; // cutting path is too short
                    //cutStart = cutEnd + (cutStartPre - cutEndPre); //  collidingMesh->getVertexPosition(0);


                    //cutStartPre = (cutStartPre + cutEndPre)*0.5;

                    // start cutting
                    vector<Vec3d> m_vBladeSegments;
                    vector<Vec3d> m_vSweptQuad;
                    Vec3d temp1, temp2;
                    temp1 = cutStartPre;
                    temp2 = cutStart;
                    m_vBladeSegments.push_back(temp1);
                    m_vBladeSegments.push_back(temp2);

                    Vec3d temp3, temp4;
                    Vec3d dirTmp = (cutStart - cutStartPre).normalized();
                    //cutStartPre = cutStartPre - dirTmp*0.02;
                    //cutEndPre = cutEndPre - dirTmp*0.02;
                    //cutStart = cutStart + dirTmp*0.02;
                    //cutEnd = cutEnd + dirTmp*0.02;
                    temp1 = cutStartPre; // root 0
                    temp2 = cutEndPre; // tip
                    temp3 = cutStart; // root
                    temp4 = cutEnd; // end




                    // get the cube
                    std::shared_ptr<PbdObject> pbdvcObj;
                    for (auto objfind : m_scene->getSceneObjects())
                    {
                        if (objfind->getName() == "Colon")
                        {
                            pbdvcObj = std::dynamic_pointer_cast<PbdObject>(objfind);
                            cutTissueMesh = std::dynamic_pointer_cast<CuttingVESSCutMesh>(pbdvcObj->getPhysicsGeometry());
                            visualColonMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(pbdvcObj->getVisualGeometry());
                        }
                    }
                    DWORD t1, t2;
                    t1 = GetTickCount();


                    m_vSweptQuad.push_back(temp1);
                    m_vSweptQuad.push_back(temp2);
                    m_vSweptQuad.push_back(temp3);
                    m_vSweptQuad.push_back(temp4);

                    //// test specific path for cube
                    //switch (cutTimes)
                    //{
                    //case 0:
                    //default:
                    //    m_vSweptQuad[0] = Vec3d(-45, 30, 4); // root
                    //    m_vSweptQuad[1] = Vec3d(-20, 30, 4);
                    //    m_vSweptQuad[2] = Vec3d(-45, 30, -14);
                    //    m_vSweptQuad[3] = Vec3d(-20, 30, -14);
                    //    break;
                    //case 1:
                    //    m_vSweptQuad[0] = Vec3d(-45, 30, -14); // root
                    //    m_vSweptQuad[1] = Vec3d(-20, 30, -14);
                    //    m_vSweptQuad[2] = Vec3d(-45, 8, -14);
                    //    m_vSweptQuad[3] = Vec3d(-20, 8, -14);
                    //    break;
                    //case 2:
                    //    m_vSweptQuad[0] = Vec3d(-45, 8, -14); // root
                    //    m_vSweptQuad[1] = Vec3d(-20, 8, -14);
                    //    m_vSweptQuad[2] = Vec3d(-45, 8, 4);
                    //    m_vSweptQuad[3] = Vec3d(-20, 8, 4);
                    //    break;
                    //case 3:
                    //    m_vSweptQuad[0] = Vec3d(-45, 8, 4); // root
                    //    m_vSweptQuad[1] = Vec3d(-20, 8, 4);
                    //    m_vSweptQuad[2] = Vec3d(-45, 30, 4);
                    //    m_vSweptQuad[3] = Vec3d(-20, 30, 4);
                    //    break;
                    //}
                    // test specific path for colon
                    // test specific path for colon // 4
                    //switch (cutTimes)
                    //{
                    //case 0:
                    //default:
                    //    m_vSweptQuad[0] = Vec3d(2.5, 3.6, 3); // root
                    //    m_vSweptQuad[1] = Vec3d(2.5, 3.6, 7);
                    //    m_vSweptQuad[2] = Vec3d(4.2, 3.6, 3);
                    //    m_vSweptQuad[3] = Vec3d(4.2, 3.6, 7);
                    //    break;
                    //case 1:
                    //    m_vSweptQuad[0] = Vec3d(4.2, 3.6, 3); // root
                    //    m_vSweptQuad[1] = Vec3d(4.2, 3.6, 7);
                    //    m_vSweptQuad[2] = Vec3d(4.2, 5, 3);
                    //    m_vSweptQuad[3] = Vec3d(4.2, 5, 7);
                    //    break;
                    //case 2:
                    //    m_vSweptQuad[0] = Vec3d(4.2, 5, 3); // root
                    //    m_vSweptQuad[1] = Vec3d(4.2, 5, 7);
                    //    m_vSweptQuad[2] = Vec3d(2.5, 5, 3);
                    //    m_vSweptQuad[3] = Vec3d(2.5, 5, 7);
                    //    break;
                    //case 3:
                    //    m_vSweptQuad[0] = Vec3d(2.5, 5, 3); // root
                    //    m_vSweptQuad[1] = Vec3d(2.5, 5, 7);
                    //    m_vSweptQuad[2] = Vec3d(2.5, 3.6, 3);
                    //    m_vSweptQuad[3] = Vec3d(2.5, 3.6, 7);
                    //    break;
                    //}
                    // 8
                    //switch (cutTimes)
                    //{
                    //case 0:
                    //default:
                    //    m_vSweptQuad[0] = Vec3d(3.1, 3.6, 3); // root
                    //    m_vSweptQuad[1] = Vec3d(3.1, 3.6, 7);
                    //    m_vSweptQuad[2] = Vec3d(3.7, 3.6, 3);
                    //    m_vSweptQuad[3] = Vec3d(3.7, 3.6, 7);
                    //    break;
                    //case 1:
                    //    m_vSweptQuad[0] = Vec3d(3.7, 3.6, 3); // root
                    //    m_vSweptQuad[1] = Vec3d(3.7, 3.6, 7);
                    //    m_vSweptQuad[2] = Vec3d(4.2, 4.1, 3);
                    //    m_vSweptQuad[3] = Vec3d(4.2, 4.1, 7);
                    //    break;
                    //case 2:
                    //    m_vSweptQuad[0] = Vec3d(4.2, 4.1, 3); // root
                    //    m_vSweptQuad[1] = Vec3d(4.2, 4.1, 7);
                    //    m_vSweptQuad[2] = Vec3d(4.2, 4.6, 3);
                    //    m_vSweptQuad[3] = Vec3d(4.2, 4.6, 7);
                    //    break;
                    //case 3:
                    //    m_vSweptQuad[0] = Vec3d(4.2, 4.6, 3); // root
                    //    m_vSweptQuad[1] = Vec3d(4.2, 4.6, 7);
                    //    m_vSweptQuad[2] = Vec3d(3.7, 5, 3);
                    //    m_vSweptQuad[3] = Vec3d(3.7, 5, 7);
                    //    break;
                    //case 4:
                    //    m_vSweptQuad[0] = Vec3d(3.7, 5, 3); // root
                    //    m_vSweptQuad[1] = Vec3d(3.7, 5, 7);
                    //    m_vSweptQuad[2] = Vec3d(3.1, 5, 3);
                    //    m_vSweptQuad[3] = Vec3d(3.1, 5, 7);
                    //    break;
                    //case 5:
                    //    m_vSweptQuad[0] = Vec3d(3.1, 5, 3); // root
                    //    m_vSweptQuad[1] = Vec3d(3.1, 5, 7);
                    //    m_vSweptQuad[2] = Vec3d(2.5, 4.6, 3);
                    //    m_vSweptQuad[3] = Vec3d(2.5, 4.6, 7);
                    //    break;
                    //case 6:
                    //    m_vSweptQuad[0] = Vec3d(2.5, 4.6, 3); // root
                    //    m_vSweptQuad[1] = Vec3d(2.5, 4.6, 7);
                    //    m_vSweptQuad[2] = Vec3d(2.5, 4.1, 3);
                    //    m_vSweptQuad[3] = Vec3d(2.5, 4.1, 7);
                    //    break;
                    //case 7:
                    //    m_vSweptQuad[0] = Vec3d(2.5, 4.1, 3); // root
                    //    m_vSweptQuad[1] = Vec3d(2.5, 4.1, 7);
                    //    m_vSweptQuad[2] = Vec3d(3.1, 3.6, 3);
                    //    m_vSweptQuad[3] = Vec3d(3.1, 3.6, 7);
                    //    break;
                    //}
                      ////< XZH Start
                    //////< XZH seg 0
                    //if (cutTimes <= 30)
                    //{
                    //    if (cutTimes==0)
                    //    {
                    //        m_vSweptQuad[0] = posCutNodesRoot[7] + (posCutNodesRoot[0] - posCutNodesRoot[7])*(29 / 30);
                    //        m_vSweptQuad[1] = posCutNodesTip[7] + (posCutNodesTip[0] - posCutNodesTip[7])*(29 / 30);
                    //    }
                    //    else
                    //    {
                    //        m_vSweptQuad[0] = posCutNodesRoot[0] + (posCutNodesRoot[1] - posCutNodesRoot[0])*((cutTimes - 1) / 30);
                    //        m_vSweptQuad[1] = posCutNodesTip[0] + (posCutNodesTip[1] - posCutNodesTip[0])*((cutTimes - 1) / 30);
                    //    }
                    //    m_vSweptQuad[2] = posCutNodesRoot[0] + (posCutNodesRoot[1] - posCutNodesRoot[0])*(cutTimes / 30);
                    //    m_vSweptQuad[3] = posCutNodesTip[0] + (posCutNodesTip[1] - posCutNodesTip[0])*(cutTimes / 30);
                    //}
                    //else if (cutTimes <= 60) //1
                    //{
                    //    int num = cutTimes - 30;
                    //    if (num == 0)
                    //    {
                    //        m_vSweptQuad[0] = posCutNodesRoot[0] + (posCutNodesRoot[1] - posCutNodesRoot[0])*(29 / 30);
                    //        m_vSweptQuad[1] = posCutNodesTip[0] + (posCutNodesTip[1] - posCutNodesTip[0])*(29 / 30);
                    //    }
                    //    else
                    //    {
                    //        m_vSweptQuad[0] = posCutNodesRoot[1] + (posCutNodesRoot[2] - posCutNodesRoot[1])*((num - 1) / 30);
                    //        m_vSweptQuad[1] = posCutNodesTip[1] + (posCutNodesTip[2] - posCutNodesTip[1])*((num - 1) / 30);
                    //    }
                    //    m_vSweptQuad[2] = posCutNodesRoot[1] + (posCutNodesRoot[2] - posCutNodesRoot[1])*(num / 30);
                    //    m_vSweptQuad[3] = posCutNodesTip[1] + (posCutNodesTip[2] - posCutNodesTip[1])*(num / 30);
                    //}
                    //else if (cutTimes <= 90) // 2
                    //{
                    //    int num = cutTimes - 60;
                    //    if (num == 0)
                    //    {
                    //        m_vSweptQuad[0] = posCutNodesRoot[1] + (posCutNodesRoot[2] - posCutNodesRoot[1])*(29 / 30);
                    //        m_vSweptQuad[1] = posCutNodesTip[1] + (posCutNodesTip[2] - posCutNodesTip[1])*(29 / 30);
                    //    }
                    //    else
                    //    {
                    //        m_vSweptQuad[0] = posCutNodesRoot[2] + (posCutNodesRoot[3] - posCutNodesRoot[2])*((num - 1) / 30);
                    //        m_vSweptQuad[1] = posCutNodesTip[2] + (posCutNodesTip[3] - posCutNodesTip[2])*((num - 1) / 30);
                    //    }
                    //    m_vSweptQuad[2] = posCutNodesRoot[2] + (posCutNodesRoot[3] - posCutNodesRoot[2])*(num / 30);
                    //    m_vSweptQuad[3] = posCutNodesTip[2] + (posCutNodesTip[3] - posCutNodesTip[2])*(num / 30);
                    //}
                    //else if (cutTimes <= 120) // 3
                    //{
                    //    int num = cutTimes - 90;
                    //    if (num == 0)
                    //    {
                    //        m_vSweptQuad[0] = posCutNodesRoot[2] + (posCutNodesRoot[3] - posCutNodesRoot[2])*(29 / 30);
                    //        m_vSweptQuad[1] = posCutNodesTip[2] + (posCutNodesTip[3] - posCutNodesTip[2])*(29 / 30);
                    //    }
                    //    else
                    //    {
                    //        m_vSweptQuad[0] = posCutNodesRoot[3] + (posCutNodesRoot[4] - posCutNodesRoot[3])*((num - 1) / 30);
                    //        m_vSweptQuad[1] = posCutNodesTip[3] + (posCutNodesTip[4] - posCutNodesTip[3])*((num - 1) / 30);
                    //    }
                    //    m_vSweptQuad[2] = posCutNodesRoot[3] + (posCutNodesRoot[4] - posCutNodesRoot[3])*(num / 30);
                    //    m_vSweptQuad[3] = posCutNodesTip[3] + (posCutNodesTip[4] - posCutNodesTip[3])*(num / 30);
                    //}
                    //else if (cutTimes <= 150) // 4
                    //{
                    //    int num = cutTimes - 120;
                    //    if (num == 0)
                    //    {
                    //        m_vSweptQuad[0] = posCutNodesRoot[3] + (posCutNodesRoot[4] - posCutNodesRoot[3])*(29 / 30);
                    //        m_vSweptQuad[1] = posCutNodesTip[3] + (posCutNodesTip[4] - posCutNodesTip[3])*(29 / 30);
                    //    }
                    //    else
                    //    {
                    //        m_vSweptQuad[0] = posCutNodesRoot[4] + (posCutNodesRoot[5] - posCutNodesRoot[4])*((num - 1) / 30);
                    //        m_vSweptQuad[1] = posCutNodesTip[4] + (posCutNodesTip[5] - posCutNodesTip[4])*((num - 1) / 30);
                    //    }
                    //    m_vSweptQuad[2] = posCutNodesRoot[4] + (posCutNodesRoot[5] - posCutNodesRoot[4])*(num / 30);
                    //    m_vSweptQuad[3] = posCutNodesTip[4] + (posCutNodesTip[5] - posCutNodesTip[4])*(num / 30);
                    //}
                    //else if (cutTimes <= 180) // 5
                    //{
                    //    int num = cutTimes - 150;
                    //    if (num == 0)
                    //    {
                    //        m_vSweptQuad[0] = posCutNodesRoot[4] + (posCutNodesRoot[5] - posCutNodesRoot[4])*(29 / 30);
                    //        m_vSweptQuad[1] = posCutNodesTip[4] + (posCutNodesTip[5] - posCutNodesTip[4])*(29 / 30);
                    //    }
                    //    else
                    //    {
                    //        m_vSweptQuad[0] = posCutNodesRoot[5] + (posCutNodesRoot[6] - posCutNodesRoot[5])*((num - 1) / 30);
                    //        m_vSweptQuad[1] = posCutNodesTip[5] + (posCutNodesTip[6] - posCutNodesTip[5])*((num - 1) / 30);
                    //    }
                    //    m_vSweptQuad[2] = posCutNodesRoot[5] + (posCutNodesRoot[6] - posCutNodesRoot[5])*(num / 30);
                    //    m_vSweptQuad[3] = posCutNodesTip[5] + (posCutNodesTip[6] - posCutNodesTip[5])*(num / 30);
                    //}
                    //else if (cutTimes <= 210) // 6
                    //{
                    //    int num = cutTimes - 180;
                    //    if (num == 0)
                    //    {
                    //        m_vSweptQuad[0] = posCutNodesRoot[5] + (posCutNodesRoot[6] - posCutNodesRoot[5])*(29 / 30);
                    //        m_vSweptQuad[1] = posCutNodesTip[5] + (posCutNodesTip[6] - posCutNodesTip[5])*(29 / 30);
                    //    }
                    //    else
                    //    {
                    //        m_vSweptQuad[0] = posCutNodesRoot[6] + (posCutNodesRoot[7] - posCutNodesRoot[6])*((num - 1) / 30);
                    //        m_vSweptQuad[1] = posCutNodesTip[6] + (posCutNodesTip[7] - posCutNodesTip[6])*((num - 1) / 30);
                    //    }
                    //    m_vSweptQuad[2] = posCutNodesRoot[6] + (posCutNodesRoot[7] - posCutNodesRoot[6])*(num / 30);
                    //    m_vSweptQuad[3] = posCutNodesTip[6] + (posCutNodesTip[7] - posCutNodesTip[6])*(num / 30);
                    //}
                    //else if (cutTimes <= 240) // 7
                    //{
                    //    int num = cutTimes - 210;
                    //    if (num == 0)
                    //    {
                    //        m_vSweptQuad[0] = posCutNodesRoot[6] + (posCutNodesRoot[7] - posCutNodesRoot[6])*(29 / 30);
                    //        m_vSweptQuad[1] = posCutNodesTip[6] + (posCutNodesTip[7] - posCutNodesTip[6])*(29 / 30);
                    //    }
                    //    else
                    //    {
                    //        m_vSweptQuad[0] = posCutNodesRoot[7] + (posCutNodesRoot[0] - posCutNodesRoot[7])*((num - 1) / 30);
                    //        m_vSweptQuad[1] = posCutNodesTip[7] + (posCutNodesTip[0] - posCutNodesTip[7])*((num - 1) / 30);
                    //    }
                    //    m_vSweptQuad[2] = posCutNodesRoot[7] + (posCutNodesRoot[0] - posCutNodesRoot[7])*(num / 30);
                    //    m_vSweptQuad[3] = posCutNodesTip[7] + (posCutNodesTip[0] - posCutNodesTip[7])*(num / 30);
                    //}
                    //else if (cutTimes <= 270) // 7
                    //{
                    //    int num = cutTimes - 240;
                    //    if (num == 0)
                    //    {
                    //        m_vSweptQuad[0] = posCutNodesRoot[7] + (posCutNodesRoot[0] - posCutNodesRoot[7])*(29 / 30);
                    //        m_vSweptQuad[1] = posCutNodesTip[7] + (posCutNodesTip[0] - posCutNodesTip[7])*(29 / 30);
                    //    }
                    //    else
                    //    {
                    //        m_vSweptQuad[0] = posCutNodesRoot[0] + (posCutNodesRoot[1] - posCutNodesRoot[0])*((num - 1) / 30);
                    //        m_vSweptQuad[1] = posCutNodesTip[0] + (posCutNodesTip[1] - posCutNodesTip[0])*((num - 1) / 30);
                    //    }
                    //    m_vSweptQuad[2] = posCutNodesRoot[0] + (posCutNodesRoot[1] - posCutNodesRoot[0])*(num / 30);
                    //    m_vSweptQuad[3] = posCutNodesTip[0] + (posCutNodesTip[1] - posCutNodesTip[0])*(num / 30);
                    //}
                    //else
                    //{
                    //    m_vSweptQuad[0] = posCutNodesRoot[1];
                    //    m_vSweptQuad[1] = posCutNodesTip[1];
                    //    m_vSweptQuad[2] = posCutNodesRoot[1];
                    //    m_vSweptQuad[3] = posCutNodesTip[1];
                    //    //continue;
                    //}
                    //////< XZH seg  END
                      ////< XZH END
                    //// 24
                    //switch (cutTimes)
                    //{
                    //case 0:
                    //default:
                    //    m_vSweptQuad[0] = posCutNodesRoot[0]; 
                    //    m_vSweptQuad[1] = posCutNodesTip[0];
                    //    m_vSweptQuad[2] = posCutNodesRoot[0] + (posCutNodesRoot[1] - posCutNodesRoot[0])*0.3333333;  
                    //    m_vSweptQuad[3] = posCutNodesTip[0] + (posCutNodesTip[1] - posCutNodesTip[0])*0.3333333;
                    //    break;
                    //case 1:
                    //    m_vSweptQuad[0] = posCutNodesRoot[0] + (posCutNodesRoot[1] - posCutNodesRoot[0])*0.3333333;
                    //    m_vSweptQuad[1] = posCutNodesTip[0] + (posCutNodesTip[1] - posCutNodesTip[0])*0.3333333;
                    //    m_vSweptQuad[2] = posCutNodesRoot[0] + (posCutNodesRoot[1] - posCutNodesRoot[0])*0.666666;
                    //    m_vSweptQuad[3] = posCutNodesTip[0] + (posCutNodesTip[1] - posCutNodesTip[0])*0.666666;
                    //    break;
                    //case 2:
                    //    m_vSweptQuad[0] = posCutNodesRoot[0] + (posCutNodesRoot[1] - posCutNodesRoot[0])*0.666666;
                    //    m_vSweptQuad[1] = posCutNodesTip[0] + (posCutNodesTip[1] - posCutNodesTip[0])*0.666666;
                    //    m_vSweptQuad[2] = posCutNodesRoot[1];
                    //    m_vSweptQuad[3] = posCutNodesTip[1];
                    //    break;
                    //case 3:
                    //    m_vSweptQuad[0] = posCutNodesRoot[1];
                    //    m_vSweptQuad[1] = posCutNodesTip[1];
                    //    m_vSweptQuad[2] = posCutNodesRoot[1] + (posCutNodesRoot[2] - posCutNodesRoot[1])*0.3333333;
                    //    m_vSweptQuad[3] = posCutNodesTip[1] + (posCutNodesTip[2] - posCutNodesTip[1])*0.3333333;
                    //    break;
                    //case 4:
                    //    m_vSweptQuad[0] = posCutNodesRoot[1] + (posCutNodesRoot[2] - posCutNodesRoot[1])*0.3333333;
                    //    m_vSweptQuad[1] = posCutNodesTip[1] + (posCutNodesTip[2] - posCutNodesTip[1])*0.3333333;
                    //    m_vSweptQuad[2] = posCutNodesRoot[1] + (posCutNodesRoot[2] - posCutNodesRoot[1])*0.666666;
                    //    m_vSweptQuad[3] = posCutNodesTip[1] + (posCutNodesTip[2] - posCutNodesTip[1])*0.666666;
                    //    break;
                    //case 5:
                    //    m_vSweptQuad[0] = posCutNodesRoot[1] + (posCutNodesRoot[2] - posCutNodesRoot[1])*0.666666;
                    //    m_vSweptQuad[1] = posCutNodesTip[1] + (posCutNodesTip[2] - posCutNodesTip[1])*0.666666;
                    //    m_vSweptQuad[2] = posCutNodesRoot[2];
                    //    m_vSweptQuad[3] = posCutNodesTip[2];
                    //    break;
                    //case 6:
                    //    m_vSweptQuad[0] = posCutNodesRoot[2];
                    //    m_vSweptQuad[1] = posCutNodesTip[2];
                    //    m_vSweptQuad[2] = posCutNodesRoot[2] + (posCutNodesRoot[3] - posCutNodesRoot[2])*0.3333333;
                    //    m_vSweptQuad[3] = posCutNodesTip[2] + (posCutNodesTip[3] - posCutNodesTip[2])*0.3333333;
                    //    break;
                    //case 7:
                    //    m_vSweptQuad[0] = posCutNodesRoot[2] + (posCutNodesRoot[3] - posCutNodesRoot[2])*0.3333333;
                    //    m_vSweptQuad[1] = posCutNodesTip[2] + (posCutNodesTip[3] - posCutNodesTip[2])*0.3333333;
                    //    m_vSweptQuad[2] = posCutNodesRoot[2] + (posCutNodesRoot[3] - posCutNodesRoot[2])*0.666666;
                    //    m_vSweptQuad[3] = posCutNodesTip[2] + (posCutNodesTip[3] - posCutNodesTip[2])*0.666666;
                    //    break;
                    //case 8:
                    //    m_vSweptQuad[0] = posCutNodesRoot[2] + (posCutNodesRoot[3] - posCutNodesRoot[2])*0.666666;
                    //    m_vSweptQuad[1] = posCutNodesTip[2] + (posCutNodesTip[3] - posCutNodesTip[2])*0.666666;
                    //    m_vSweptQuad[2] = posCutNodesRoot[3];
                    //    m_vSweptQuad[3] = posCutNodesTip[3];
                    //    break;
                    //case 9:
                    //    m_vSweptQuad[0] = posCutNodesRoot[3];
                    //    m_vSweptQuad[1] = posCutNodesTip[3];
                    //    m_vSweptQuad[2] = posCutNodesRoot[3] + (posCutNodesRoot[4] - posCutNodesRoot[3])*0.3333333;
                    //    m_vSweptQuad[3] = posCutNodesTip[3] + (posCutNodesTip[4] - posCutNodesTip[3])*0.3333333;
                    //    break;
                    //case 10:
                    //    m_vSweptQuad[0] = posCutNodesRoot[3] + (posCutNodesRoot[4] - posCutNodesRoot[3])*0.3333333;
                    //    m_vSweptQuad[1] = posCutNodesTip[3] + (posCutNodesTip[4] - posCutNodesTip[3])*0.3333333;
                    //    m_vSweptQuad[2] = posCutNodesRoot[3] + (posCutNodesRoot[4] - posCutNodesRoot[3])*0.666666;
                    //    m_vSweptQuad[3] = posCutNodesTip[3] + (posCutNodesTip[4] - posCutNodesTip[3])*0.666666;
                    //    break;
                    //case 11:
                    //    m_vSweptQuad[0] = posCutNodesRoot[3] + (posCutNodesRoot[4] - posCutNodesRoot[3])*0.666666;
                    //    m_vSweptQuad[1] = posCutNodesTip[3] + (posCutNodesTip[4] - posCutNodesTip[3])*0.666666;
                    //    m_vSweptQuad[2] = posCutNodesRoot[4];
                    //    m_vSweptQuad[3] = posCutNodesTip[4];
                    //    break;
                    //case 12:
                    //    m_vSweptQuad[0] = posCutNodesRoot[4];
                    //    m_vSweptQuad[1] = posCutNodesTip[4];
                    //    m_vSweptQuad[2] = posCutNodesRoot[4] + (posCutNodesRoot[5] - posCutNodesRoot[4])*0.3333333;
                    //    m_vSweptQuad[3] = posCutNodesTip[4] + (posCutNodesTip[5] - posCutNodesTip[4])*0.3333333;
                    //    break;
                    //case 13:
                    //    m_vSweptQuad[0] = posCutNodesRoot[4] + (posCutNodesRoot[5] - posCutNodesRoot[4])*0.3333333;
                    //    m_vSweptQuad[1] = posCutNodesTip[4] + (posCutNodesTip[5] - posCutNodesTip[4])*0.3333333;
                    //    m_vSweptQuad[2] = posCutNodesRoot[4] + (posCutNodesRoot[5] - posCutNodesRoot[4])*0.666666;
                    //    m_vSweptQuad[3] = posCutNodesTip[4] + (posCutNodesTip[5] - posCutNodesTip[4])*0.666666;
                    //    break;
                    //case 14:
                    //    m_vSweptQuad[0] = posCutNodesRoot[4] + (posCutNodesRoot[5] - posCutNodesRoot[4])*0.666666;
                    //    m_vSweptQuad[1] = posCutNodesTip[4] + (posCutNodesTip[5] - posCutNodesTip[4])*0.666666;
                    //    m_vSweptQuad[2] = posCutNodesRoot[5];
                    //    m_vSweptQuad[3] = posCutNodesTip[5];
                    //    break;
                    //case 15:
                    //    m_vSweptQuad[0] = posCutNodesRoot[5];
                    //    m_vSweptQuad[1] = posCutNodesTip[5];
                    //    m_vSweptQuad[2] = posCutNodesRoot[5] + (posCutNodesRoot[6] - posCutNodesRoot[5])*0.3333333;
                    //    m_vSweptQuad[3] = posCutNodesTip[5] + (posCutNodesTip[6] - posCutNodesTip[5])*0.3333333;
                    //    break;
                    //case 16:
                    //    m_vSweptQuad[0] = posCutNodesRoot[5] + (posCutNodesRoot[6] - posCutNodesRoot[5])*0.3333333;
                    //    m_vSweptQuad[1] = posCutNodesTip[5] + (posCutNodesTip[6] - posCutNodesTip[5])*0.3333333;
                    //    m_vSweptQuad[2] = posCutNodesRoot[5] + (posCutNodesRoot[6] - posCutNodesRoot[5])*0.666666;
                    //    m_vSweptQuad[3] = posCutNodesTip[5] + (posCutNodesTip[6] - posCutNodesTip[5])*0.666666;
                    //    break;
                    //case 17:
                    //    m_vSweptQuad[0] = posCutNodesRoot[5] + (posCutNodesRoot[6] - posCutNodesRoot[5])*0.666666;
                    //    m_vSweptQuad[1] = posCutNodesTip[5] + (posCutNodesTip[6] - posCutNodesTip[5])*0.666666;
                    //    m_vSweptQuad[2] = posCutNodesRoot[6];
                    //    m_vSweptQuad[3] = posCutNodesTip[6];
                    //    break;
                    //case 18:
                    //    m_vSweptQuad[0] = posCutNodesRoot[6];
                    //    m_vSweptQuad[1] = posCutNodesTip[6];
                    //    m_vSweptQuad[2] = posCutNodesRoot[6] + (posCutNodesRoot[7] - posCutNodesRoot[6])*0.3333333;
                    //    m_vSweptQuad[3] = posCutNodesTip[6] + (posCutNodesTip[7] - posCutNodesTip[6])*0.3333333;
                    //    break;
                    //case 19:
                    //    m_vSweptQuad[0] = posCutNodesRoot[6] + (posCutNodesRoot[7] - posCutNodesRoot[6])*0.3333333;
                    //    m_vSweptQuad[1] = posCutNodesTip[6] + (posCutNodesTip[7] - posCutNodesTip[6])*0.3333333;
                    //    m_vSweptQuad[2] = posCutNodesRoot[6] + (posCutNodesRoot[7] - posCutNodesRoot[6])*0.666666;
                    //    m_vSweptQuad[3] = posCutNodesTip[6] + (posCutNodesTip[7] - posCutNodesTip[6])*0.666666;
                    //    break;
                    //case 20:
                    //    m_vSweptQuad[0] = posCutNodesRoot[6] + (posCutNodesRoot[7] - posCutNodesRoot[6])*0.666666;
                    //    m_vSweptQuad[1] = posCutNodesTip[6] + (posCutNodesTip[7] - posCutNodesTip[6])*0.666666;
                    //    m_vSweptQuad[2] = posCutNodesRoot[7];
                    //    m_vSweptQuad[3] = posCutNodesTip[7];
                    //    break;
                    //case 21:
                    //    m_vSweptQuad[0] = posCutNodesRoot[7];
                    //    m_vSweptQuad[1] = posCutNodesTip[7];
                    //    m_vSweptQuad[2] = posCutNodesRoot[7] + (posCutNodesRoot[0] - posCutNodesRoot[7])*0.3333333;
                    //    m_vSweptQuad[3] = posCutNodesTip[7] + (posCutNodesTip[0] - posCutNodesTip[7])*0.3333333;
                    //    break;
                    //case 22:
                    //    m_vSweptQuad[0] = posCutNodesRoot[7] + (posCutNodesRoot[0] - posCutNodesRoot[7])*0.3333333;
                    //    m_vSweptQuad[1] = posCutNodesTip[7] + (posCutNodesTip[0] - posCutNodesTip[7])*0.3333333;
                    //    m_vSweptQuad[2] = posCutNodesRoot[7] + (posCutNodesRoot[0] - posCutNodesRoot[7])*0.666666;
                    //    m_vSweptQuad[3] = posCutNodesTip[7] + (posCutNodesTip[0] - posCutNodesTip[7])*0.666666;
                    //    break;
                    //case 23:
                    //    m_vSweptQuad[0] = posCutNodesRoot[7] + (posCutNodesRoot[0] - posCutNodesRoot[7])*0.666666;
                    //    m_vSweptQuad[1] = posCutNodesTip[7] + (posCutNodesTip[0] - posCutNodesTip[7])*0.666666;
                    //    m_vSweptQuad[2] = posCutNodesRoot[0];
                    //    m_vSweptQuad[3] = posCutNodesTip[0];
                    //    break;
                    //}

                    // for specific cutting path ONLY
                    //m_vSweptQuad[0] = m_vSweptQuad[1] + (m_vSweptQuad[0] - m_vSweptQuad[1])*0.3;
                    //m_vSweptQuad[1] = m_vSweptQuad[1] + (m_vSweptQuad[1] - m_vSweptQuad[0])*0.5;
                    //m_vSweptQuad[2] = m_vSweptQuad[3] + (m_vSweptQuad[2] - m_vSweptQuad[3])*0.3;
                    //m_vSweptQuad[3] = m_vSweptQuad[3] + (m_vSweptQuad[3] - m_vSweptQuad[2])*0.5;

                    //switch (cutTimes)
                    //{
                    //case 0:
                    //default:
                    //    m_vSweptQuad[0] = Vec3d(-4.7, 3.1, 1); // root
                    //    m_vSweptQuad[1] = Vec3d(-4.7, 3.1, -6.5);
                    //    m_vSweptQuad[2] = Vec3d(-4.7, 5.2, 1);
                    //    m_vSweptQuad[3] = Vec3d(-4.7, 5.2, -6.5);
                    //    break;
                    //case 1:
                    //    m_vSweptQuad[0] = Vec3d(-4.7, 5.2, 1); // root
                    //    m_vSweptQuad[1] = Vec3d(-4.7, 5.2, -6.5);
                    //    m_vSweptQuad[2] = Vec3d(-1.50, 5.2, 1);
                    //    m_vSweptQuad[3] = Vec3d(-1.50, 5.2, -6.5);
                    //    break;
                    //case 2:
                    //    m_vSweptQuad[0] = Vec3d(-1.50, 5.2, 1); // root
                    //    m_vSweptQuad[1] = Vec3d(-1.50, 5.2, -6.5);
                    //    m_vSweptQuad[2] = Vec3d(-1.50, 3.1, 1);
                    //    m_vSweptQuad[3] = Vec3d(-1.50, 3.1, -6.5);
                    //    break;
                    //case 3:
                    //    m_vSweptQuad[0] = Vec3d(-1.50, 3.1, 1); // root
                    //    m_vSweptQuad[1] = Vec3d(-1.50, 3.1, -6.5);
                    //    m_vSweptQuad[2] = Vec3d(-4.7, 3.1, 1);
                    //    m_vSweptQuad[3] = Vec3d(-4.7, 3.1, -6.5);
                    //    break;
                    //}
                    //switch (cutTimes)
                    //{
                    //case 0:
                    //default:
                    //    m_vSweptQuad[0] = Vec3d(-4.7, 3.3, 1); // root  Y: 3.8->3.3
                    //    m_vSweptQuad[1] = Vec3d(-4.7, 3.3, -6.5);
                    //    m_vSweptQuad[2] = Vec3d(-4.7, 4.5, 1);
                    //    m_vSweptQuad[3] = Vec3d(-4.7, 4.5, -6.5);
                    //    break;
                    //case 1:
                    //    m_vSweptQuad[0] = Vec3d(-4.7, 4.5, 1); // root
                    //    m_vSweptQuad[1] = Vec3d(-4.7, 4.5, -6.5);
                    //    m_vSweptQuad[2] = Vec3d(-3.7, 5.2, 1);
                    //    m_vSweptQuad[3] = Vec3d(-3.7, 5.2, -6.5);
                    //    break;
                    //case 2:
                    //    m_vSweptQuad[0] = Vec3d(-3.7, 5.2, 1); // root
                    //    m_vSweptQuad[1] = Vec3d(-3.7, 5.2, -6.5);
                    //    m_vSweptQuad[2] = Vec3d(-2.6, 5.2, 1);
                    //    m_vSweptQuad[3] = Vec3d(-2.6, 5.2, -6.5);
                    //    break;
                    //case 3:
                    //    m_vSweptQuad[0] = Vec3d(-2.6, 5.2, 1); // root
                    //    m_vSweptQuad[1] = Vec3d(-2.6, 5.2, -6.5);
                    //    m_vSweptQuad[2] = Vec3d(-1.5, 4.5, 1);
                    //    m_vSweptQuad[3] = Vec3d(-1.5, 4.5, -6.5);
                    //    break;
                    //case 4:
                    //    m_vSweptQuad[0] = Vec3d(-1.5, 4.5, 1); // root
                    //    m_vSweptQuad[1] = Vec3d(-1.5, 4.5, -6.5);
                    //    m_vSweptQuad[2] = Vec3d(-1.5, 3.8, 1);
                    //    m_vSweptQuad[3] = Vec3d(-1.5, 3.8, -6.5);
                    //    break;
                    //case 5:
                    //    m_vSweptQuad[0] = Vec3d(-1.5, 3.8, 1); // root
                    //    m_vSweptQuad[1] = Vec3d(-1.5, 3.8, -6.5);
                    //    m_vSweptQuad[2] = Vec3d(-2.6, 3.1, 1);
                    //    m_vSweptQuad[3] = Vec3d(-2.6, 3.1, -6.5);
                    //    break;
                    //case 6:
                    //    m_vSweptQuad[0] = Vec3d(-2.6, 3.1, 1); // root
                    //    m_vSweptQuad[1] = Vec3d(-2.6, 3.1, -6.5);
                    //    m_vSweptQuad[2] = Vec3d(-3.7, 3.1, 1);
                    //    m_vSweptQuad[3] = Vec3d(-3.7, 3.1, -6.5);
                    //    break;
                    //case 7:
                    //    m_vSweptQuad[0] = Vec3d(-3.7, 3.1, 1); // root
                    //    m_vSweptQuad[1] = Vec3d(-3.7, 3.1, -6.5);
                    //    m_vSweptQuad[2] = Vec3d(-4.7, 3.3, 1);
                    //    m_vSweptQuad[3] = Vec3d(-4.7, 3.3, -6.5);
                    //    break;
                    //}
                    //// test specific path END

                    int res = 0;
                    //res = cutTissueMesh->cutNew(m_vBladeSegments, m_vSweptQuad, true);

                    //DWORD tt4 = GetTickCount();
                    //printf("cut Tet element time %d ms\n", tt4 - tt0);

                    //DWORD tt2 = GetTickCount();
                    //printf("cut traverse Tet graph time %d ms\n", tt2 - tt0);

                    std::vector<int> m_trianglesNumCut;
                    std::vector<SurfaceMesh::TriangleArray> m_trianglesNumCutTris;
                    std::vector<SurfaceMesh::TriangleArray> m_trianglesCutNo;
                    std::vector<SurfaceMesh::TriangleArray> m_trianglesCutCurrent;
                    auto& triLists = visualColonMesh->getTrianglesVerticesChangeable();  // XZH use for change
                    auto& triListsXZH = visualColonMesh->getTrianglesVerticesXZH();  
                    auto& vertPos = visualColonMesh->getVertexPositions();

                    // sweep surface
                    bool isExistSweep = false;
                    std::shared_ptr<SurfaceMesh> surfMeshSwept;
                    for (auto objxzh : m_scene->getSceneObjects())
                    {
                        if (objxzh->getName() == "SweepSurface")
                        {
                            surfMeshSwept = std::dynamic_pointer_cast<SurfaceMesh>(objxzh->getVisualGeometry());
                            break;
                        }
                    }
                    if (surfMeshSwept)
                    {
                        isExistSweep = true;
                        imstk::StdVectorOfVec3d vertListSwept;
                        std::vector<imstk::SurfaceMesh::TriangleArray> trianglesSwept;
                        
                        trianglesSwept.push_back({ { 0, 1, 2 } });
                        trianglesSwept.push_back({ { 2, 1, 3 } });
                        auto& posVisualListXZH = surfMeshSwept->getVertexPositionsChangeable();
                        auto& trisVisualListXZH = surfMeshSwept->getTrianglesVerticesChangeable();
                        posVisualListXZH.push_back(m_vSweptQuad[0]);
                        posVisualListXZH.push_back(m_vSweptQuad[1]);
                        posVisualListXZH.push_back(m_vSweptQuad[2]);
                        posVisualListXZH.push_back(m_vSweptQuad[3]); // 100
                        int cntV = posVisualListXZH.size();
                        trisVisualListXZH.push_back({ { cntV-4, cntV-3, cntV-2} });
                        trisVisualListXZH.push_back({ { cntV - 2, cntV - 3, cntV - 1 } });
                        surfMeshSwept->setVertexPositions(posVisualListXZH); // posVisualList); // vertPositions
                        surfMeshSwept->setTrianglesVertices(trisVisualListXZH);
                        surfMeshSwept->setTopologyChangedFlag(true);
                    }
                    else
                    {
                        isExistSweep = false;
                        imstk::StdVectorOfVec3d vertListSwept;
                        std::vector<imstk::SurfaceMesh::TriangleArray> trianglesSwept;
                        vertListSwept.push_back(m_vSweptQuad[0]);
                        vertListSwept.push_back(m_vSweptQuad[1]);
                        vertListSwept.push_back(m_vSweptQuad[2]);
                        vertListSwept.push_back(m_vSweptQuad[3]);
                        trianglesSwept.push_back({ { 0, 1, 2 } });
                        trianglesSwept.push_back({ { 2, 1, 3 } });
                        for (int k = 0; k < 50;k++)
                        {
                            vertListSwept.push_back(m_vSweptQuad[0]);
                            vertListSwept.push_back(m_vSweptQuad[1]);
                            vertListSwept.push_back(m_vSweptQuad[2]);
                            vertListSwept.push_back(m_vSweptQuad[3]); // 100
                            int cntV = vertListSwept.size();
                            trianglesSwept.push_back({ { cntV - 4, cntV - 3, cntV - 2 } });
                            trianglesSwept.push_back({ { cntV - 2, cntV - 3, cntV - 1 } });
                        }
                        surfMeshSwept = std::make_shared<imstk::SurfaceMesh>();
                        surfMeshSwept->initialize(vertListSwept, trianglesSwept);
                        // visualization
                        auto object0 = std::make_shared<imstk::VisualObject>("SweepSurface"); // "visualColonMesh0");
                        auto material0 = std::make_shared<RenderMaterial>();
                        material0->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME); // WIREFRAME SURFACE
                        auto tmp0 = std::make_shared<Texture>("I:/iMSTK/resources/VESS/testtest.png", Texture::Type::DIFFUSE);
                        material0->addTexture(tmp0);
                        surfMeshSwept->setRenderMaterial(material0);
                        object0->setVisualGeometry(surfMeshSwept); // change to any mesh created above
                        m_scene->addSceneObject(object0);
                    }
                     //END display cutting
                    //imstk::StdVectorOfVec3d vertListSwept;
                    //std::vector<imstk::SurfaceMesh::TriangleArray> trianglesSwept;
                    //vertListSwept.push_back(m_vSweptQuad[0]);
                    //vertListSwept.push_back(m_vSweptQuad[1]);
                    //vertListSwept.push_back(m_vSweptQuad[2]);
                    //vertListSwept.push_back(m_vSweptQuad[3]);
                    //trianglesSwept.push_back({ { 0, 1, 2 } });
                    //trianglesSwept.push_back({ { 2, 1, 3 } });
                    //auto surfMeshSwept = std::make_shared<imstk::SurfaceMesh>();
                    //surfMeshSwept->initialize(vertListSwept, trianglesSwept);
                    //char nameSweep[256];
                    //sprintf(nameSweep, "SweepSurface%d", cutTimes);
                    //auto object0 = std::make_shared<imstk::VisualObject>(nameSweep); // "visualColonMesh0");
                    //auto material0 = std::make_shared<RenderMaterial>();
                    //material0->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME); // WIREFRAME SURFACE
                    //auto tmp0 = std::make_shared<Texture>("I:/iMSTK/resources/VESS/testtest.png", Texture::Type::DIFFUSE);
                    //material0->addTexture(tmp0);
                    //surfMeshSwept->setRenderMaterial(material0);
                    //object0->setVisualGeometry(surfMeshSwept); // change to any mesh created above
                    //m_scene->addSceneObject(object0);

                    cutTimes++;

                    //DWORD tt5 = GetTickCount();
                    //printf("cut Tet graph time %d ms\n", tt5 - tt0);
                    // cutting
                    auto& posVisualList = visualColonMesh->getVertexPositionsChangeable(); // changable 
                    int numOrigin = visualColonMesh->getVertexPositions().size(); // original vertex size after previous cut
                    int numOriginInit = visualColonMesh->getInitialVertexPositions().size(); // original init vertex size
                    auto& vecTrianglesStatesXZH = visualColonMesh->getTrianglesStatesXZH();

                    //vars
                    Vec3d uvw, xyz, ss0, ss1;
                    double t;
                    Vec3d tri1[3] = { m_vSweptQuad[0], m_vSweptQuad[2], m_vSweptQuad[1] };
                    Vec3d tri2[3] = { m_vSweptQuad[2], m_vSweptQuad[3], m_vSweptQuad[1] };
                    res = 0;
                    imstk::StdVectorOfVec3d newVertList;
                    std::vector<Vec3d> newVertListTriangulation; //
                    std::list<size_t> newVertIndex;
                    std::vector<imstk::SurfaceMesh::TriangleArray> newTriangles;

                    // Method 2 find out the cut trianges broad phase
                    auto min_x = std::min(m_vSweptQuad[0].x(), std::min(m_vSweptQuad[1].x(), std::min(m_vSweptQuad[2].x(), m_vSweptQuad[3].x())));
                    auto max_x = std::max(m_vSweptQuad[0].x(), std::max(m_vSweptQuad[1].x(), std::max(m_vSweptQuad[2].x(), m_vSweptQuad[3].x())));
                    auto min_y = std::min(m_vSweptQuad[0].y(), std::min(m_vSweptQuad[1].y(), std::min(m_vSweptQuad[2].y(), m_vSweptQuad[3].y())));
                    auto max_y = std::max(m_vSweptQuad[0].y(), std::max(m_vSweptQuad[1].y(), std::max(m_vSweptQuad[2].y(), m_vSweptQuad[3].y())));
                    auto min_z = std::min(m_vSweptQuad[0].z(), std::min(m_vSweptQuad[1].z(), std::min(m_vSweptQuad[2].z(), m_vSweptQuad[3].z())));
                    auto max_z = std::max(m_vSweptQuad[0].z(), std::max(m_vSweptQuad[1].z(), std::max(m_vSweptQuad[2].z(), m_vSweptQuad[3].z())));
                    m_trianglesNumCut.clear();
                    m_trianglesCutNo.clear();
                    m_trianglesNumCutTris.clear();
                    m_trianglesCutCurrent.clear();
                    //for (int m = 0; m < triLists.size(); m++)
                    for (int m = 0; m < triListsXZH.size(); m++)
                    {
                        auto& tri = triListsXZH[m]; // triLists[m];
                        if (vecTrianglesStatesXZH[m] < 1)
                        {
                            m_trianglesNumCutTris.push_back(tri);
                            continue;
                        }
                        auto vPos0 = posVisualList[tri[0]];
                        auto vPos1 = posVisualList[tri[1]];
                        auto vPos2 = posVisualList[tri[2]];
                        auto min_x1 = std::min(vPos0.x(), std::min(vPos1.x(), vPos2.x()));
                        auto max_x1 = std::max(vPos0.x(), std::max(vPos1.x(), vPos2.x()));
                        auto min_y1 = std::min(vPos0.y(), std::min(vPos1.y(), vPos2.y()));
                        auto max_y1 = std::max(vPos0.y(), std::max(vPos1.y(), vPos2.y()));
                        auto min_z1 = std::min(vPos0.z(), std::min(vPos1.z(), vPos2.z()));
                        auto max_z1 = std::max(vPos0.z(), std::max(vPos1.z(), vPos2.z()));
                        double prox1 = 0.05;
                        double prox2 = 0.05;
                        bool res = testTrisAABB(min_x - prox1, max_x + prox1, min_y - prox1, max_y + prox1, min_z - prox1, max_z + prox1,
                            min_x1 - prox2, max_x1 + prox2, min_y1 - prox2, max_y1 + prox2, min_z1 - prox2, max_z1 + prox2);

                        if (res) m_trianglesNumCut.push_back(m);
                        else m_trianglesCutNo.push_back(tri);
                    } // END for
                    // END method 2
                    //if (m_trianglesNumCut.size()<1) continue;
                    // Method 3 temp way, should change a broad phase way
                    //m_trianglesNumCut.clear();
                    //for (int m = 0; m < triListsXZH.size(); m++)
                    //{
                    //    m_trianglesNumCut.push_back(m);
                    //}
                    //m_trianglesNumCut = visualColonMesh->getOperationTriangles();
                    // END Method 3

                    Vec3d normCut = (tri1[1] - tri1[0]).cross(tri1[2] - tri1[0]).normalized(); // Method 1 compare the cutting surface normal
                    Vec3d posTumorCenter = visualColonMesh->getVertexPosition(indexTumorVertex); // Method 2 compare the distance, if small, new index is bigger
                    Vec3d normCut2Tumor = (posTumorCenter - tri1[0]).normalized();
                    if (normCut.dot(normCut2Tumor) < 0)
                    {
                        normCut = -normCut;
                        //printf("cut norm reverse!\n");
                    }

                    DWORD tt6 = GetTickCount();
                    //printf("cut broad phase time %d ms\n", tt6 - tt0);
                    //  intersectoin: finding out the partial cutting triangle
                    int cutNum = 0;
                    char name[256];
                    std::vector<SurfaceMesh::TriangleArray> triListsTmp;
                    Vec3d contactPos, contactPoint;
                    Vec3d cutNormal = (tri1[1] - tri1[0]).cross(tri1[2] - tri1[0]);
                    double gapDist = 0.7;
                    auto& posVisualListTmp = posVisualList; // 0129/2018  use reference or not, check later, not use in the previous version
                    //int graphSize = 0;
                    std::vector<int> vecCutVerts;
                    for (int m = 0; m < m_trianglesNumCut.size(); m++)
                    {
                        int tidx = m_trianglesNumCut[m];
                        auto& tri = triListsXZH[m_trianglesNumCut[m]]; // triLists[m_trianglesNumCut[m]];
                        if (vecTrianglesStatesXZH[tidx] < 1)
                        {
                            m_trianglesNumCutTris.push_back(tri);
                            continue;
                        }
                        ss0 = posVisualListTmp[tri[0]];
                        ss1 = posVisualListTmp[tri[1]];

                        Vec3d ss2 = posVisualListTmp[tri[2]];
                        Vec3d contactPos;
                        int edgeNum = 0;
                        unsigned char vertCode = 0x00;
                        int graph0 = -1, graph1 = -1, graph2 = -1;

                        double deltaLen = (ss0 - ss1).norm();
                        int res0 = IntersectSegmentTriangle(ss0, ss1, tri1, t, uvw, xyz);
                        if (res == 0)
                        {
                            int res00 = IntersectSegmentTriangle(ss0, ss1, tri2, t, uvw, xyz);
                            if (res00 > 0)
                            {
                                edgeNum++; contactPoint = xyz;
                                if ((ss0 - tri1[0]).dot(normCut) > 0)  vertCode |= (1 << 0); // 0 in the tumor side !!!!!please note use vert, not edge
                                else vertCode |= (1 << 1); // 1
                            }
                        }
                        else // >0
                        {
                            edgeNum++; contactPoint = xyz;
                            if ((ss0 - tri1[0]).dot(normCut) > 0)  vertCode |= (1 << 0); // 0 in the tumor side !!!!!please note use vert, not edge
                            else vertCode |= (1 << 1); // 1
                        }

                        //int res0 = intersectSegmentPlane(contactPos, ss0, ss1, cutNormal, m_vSweptQuad[0]);
                        //if (res0 == 1)
                        //{
                        //    bool flag = insidePointRectangle(contactPos, m_vSweptQuad[0], m_vSweptQuad[1], m_vSweptQuad[3], m_vSweptQuad[2]);
                        //    if (flag)
                        //    {
                        //        edgeNum++; contactPoint = contactPos;
                        //        if ((ss0 - tri1[0]).dot(normCut) > 0)  vertCode |= (1 << 0); // 0 in the tumor side !!!!!please note use vert, not edge
                        //        else vertCode |= (1 << 1); // 1
                        //    }
                        //}

                        deltaLen = (ss1 - ss2).norm();
                        int res1 = IntersectSegmentTriangle(ss1, ss2, tri1, t, uvw, xyz);
                        if (res1 == 0)
                        {
                            int res10 = IntersectSegmentTriangle(ss1, ss2, tri2, t, uvw, xyz);
                            if (res10 > 0)
                            {
                                edgeNum++; contactPoint = xyz;
                                if ((ss1 - tri1[0]).dot(normCut) > 0)  vertCode |= (1 << 1); // 1 in the tumor side !!!!!please note use vert, not edge
                                else vertCode |= (1 << 2); // 2
                            }
                        }
                        else // >0
                        {
                            edgeNum++; contactPoint = xyz;
                            if ((ss1 - tri1[0]).dot(normCut) > 0)  vertCode |= (1 << 1); // 1 in the tumor side !!!!!please note use vert, not edge
                            else vertCode |= (1 << 2); // 2
                        }

                        //int res1 = intersectSegmentPlane(contactPos, ss1, ss2, cutNormal, m_vSweptQuad[0]);
                        //if (res1 == 1)
                        //{
                        //    bool flag = insidePointRectangle(contactPos, m_vSweptQuad[0], m_vSweptQuad[1], m_vSweptQuad[3], m_vSweptQuad[2]);
                        //    if (flag)
                        //    {
                        //        edgeNum++; contactPoint = contactPos;
                        //        if ((ss1 - tri1[0]).dot(normCut) > 0)  vertCode |= (1 << 1); // 1 in the tumor side !!!!!please note use vert, not edge
                        //        else vertCode |= (1 << 2); // 2
                        //    }
                        //}

                        deltaLen = (ss0 - ss2).norm();
                        int res2 = IntersectSegmentTriangle(ss0, ss2, tri1, t, uvw, xyz);
                        if (res2 == 0)
                        {
                            int res20 = IntersectSegmentTriangle(ss0, ss2, tri2, t, uvw, xyz);
                            if (res20 > 0)
                            {
                                edgeNum++; contactPoint = xyz;
                                if ((ss0 - tri1[0]).dot(normCut) > 0)  vertCode |= (1 << 0); // 0 in the tumor side !!!!!please note use vert, not edge
                                else vertCode |= (1 << 2); // 2
                            }
                        }
                        else // >0
                        {
                            edgeNum++; contactPoint = xyz;
                            if ((ss0 - tri1[0]).dot(normCut) > 0)  vertCode |= (1 << 0); // 0 in the tumor side !!!!!please note use vert, not edge
                            else vertCode |= (1 << 2); // 2
                        }

                        //int res2 = intersectSegmentPlane(contactPos, ss0, ss2, cutNormal, m_vSweptQuad[0]);
                        //if (res2 == 1)
                        //{
                        //    bool flag = insidePointRectangle(contactPos, m_vSweptQuad[0], m_vSweptQuad[1], m_vSweptQuad[3], m_vSweptQuad[2]);
                        //    if (flag)
                        //    {
                        //        edgeNum++; contactPoint = contactPos;
                        //        if ((ss0 - tri1[0]).dot(normCut) > 0)  vertCode |= (1 << 0); // 0 in the tumor side !!!!!please note use vert, not edge
                        //        else vertCode |= (1 << 2); // 2
                        //    }
                        //}

                        if (edgeNum>0) // intersection
                        {
                            triListsCutPath.push_back(tri);
                            vecTrianglesStatesXZH[tidx] = 0;  ////< XZH states remove
                            m_trianglesCutCurrent.push_back(tri);
                            triCutPathTriIdx.push_back(m_trianglesNumCut[m]); // triangle global original index
                            vecCutVerts.push_back(tri[0]);
                            vecCutVerts.push_back(tri[1]);
                            vecCutVerts.push_back(tri[2]);
                            //printf("cut tri: %d\n", m_trianglesNumCut[m]);
                            cutNum++;

                            std::unordered_map<int, int>::iterator it;
                            shared_ptr<imstk::Sphere> visualSphere22;
                            shared_ptr<SceneObject> sphere2Obj;
                            shared_ptr<imstk::Sphere> visualSphere23;
                            shared_ptr<SceneObject> sphere3Obj;
                            Vec3d middlePos, refPos, dirPos;   ////< XZH  
                            Vec3d posTmp0, posTmp1, posTmp2;
                            double lenTmp, scaleTmp;
                            double scaleLast;
                            const double moveGap = 0.05;
                            const double rads = 0.01;
                            switch (vertCode)
                            {
                            case 0x01: // 001  only vert 0 is in the tumore side <- 210
                                // vert 0
                                it = mapCutVertices.find(tri[0]);
                                if (it != mapCutVertices.end()) // existing
                                {
                                    graph0 = it->second;
                                }
                                else // not exist
                                {
                                    graph0 = graphSize;
                                    mapCutVertices.insert(make_pair(tri[0], graphSize));  // start from 0
                                    graphSize++;
                                }
                                //// sphere
                                //sprintf(name, "XZHSphere%d", tri[0]);
                                //visualSphere22 = std::make_shared<imstk::Sphere>();
                                //visualSphere22->setTranslation(posVisualListTmp[tri[0]]);
                                //visualSphere22->setRadius(rads);
                                //sphere2Obj = std::make_shared<SceneObject>(name);
                                //sphere2Obj->setVisualGeometry(visualSphere22);
                                //m_scene->addSceneObject(sphere2Obj);
                                //for (auto obj : m_scene->getSceneObjects())
                                //{
                                //    if (obj->getName() == name)
                                //    {
                                //        obj->getVisualGeometry()->setTranslation(posVisualListTmp[tri[0]]);
                                //        break;
                                //    }
                                //}
                                break;
                            case 0x02: // 010  
                                // vert 1
                                it = mapCutVertices.find(tri[1]);
                                if (it != mapCutVertices.end()) // existing
                                {
                                    graph1 = it->second;
                                }
                                else // not exist
                                {
                                    graph1 = graphSize;
                                    mapCutVertices.insert(make_pair(tri[1], graphSize));  // start from 0
                                    graphSize++;
                                }
                                //// sphere
                                //sprintf(name, "XZHSphere%d", tri[1]);
                                //visualSphere22 = std::make_shared<imstk::Sphere>();
                                //visualSphere22->setTranslation(posVisualListTmp[tri[1]]);
                                //visualSphere22->setRadius(rads);
                                //sphere2Obj = std::make_shared<SceneObject>(name);
                                //sphere2Obj->setVisualGeometry(visualSphere22);
                                //m_scene->addSceneObject(sphere2Obj);
                                //for (auto obj : m_scene->getSceneObjects())
                                //{
                                //    if (obj->getName() == name)
                                //    {
                                //        obj->getVisualGeometry()->setTranslation(posVisualListTmp[tri[1]]);
                                //        break;
                                //    }
                                //}
                                break;
                            case 0x04: // 100   
                                // vert 2
                                it = mapCutVertices.find(tri[2]);
                                if (it != mapCutVertices.end()) // existing
                                {
                                    graph2 = it->second;
                                }
                                else // not exist
                                {
                                    graph2 = graphSize;
                                    mapCutVertices.insert(make_pair(tri[2], graphSize));  // start from 0
                                    graphSize++;
                                }
                                //// sphere
                                //sprintf(name, "XZHSphere%d", tri[2]);
                                //visualSphere22 = std::make_shared<imstk::Sphere>();
                                //visualSphere22->setTranslation(posVisualListTmp[tri[2]]);
                                //visualSphere22->setRadius(rads);
                                //sphere2Obj = std::make_shared<SceneObject>(name);
                                //sphere2Obj->setVisualGeometry(visualSphere22);
                                //m_scene->addSceneObject(sphere2Obj);
                                //for (auto obj : m_scene->getSceneObjects())
                                //{
                                //    if (obj->getName() == name)
                                //    {
                                //        obj->getVisualGeometry()->setTranslation(posVisualListTmp[tri[2]]);
                                //        break;
                                //    }
                                //}
                                //break;
                            case 0x03: // 011  
                                // vert 0
                                it = mapCutVertices.find(tri[0]);
                                if (it != mapCutVertices.end()) // existing
                                {
                                    graph0 = it->second;
                                }
                                else // not exist
                                {
                                    graph0 = graphSize;
                                    mapCutVertices.insert(make_pair(tri[0], graphSize));  // start from 0
                                    graphSize++;
                                }
                                // vert 1
                                it = mapCutVertices.find(tri[1]);
                                if (it != mapCutVertices.end()) // existing
                                {
                                    graph1 = it->second;
                                }
                                else // not exist
                                {
                                    graph1 = graphSize;
                                    mapCutVertices.insert(make_pair(tri[1], graphSize));  // start from 0
                                    graphSize++;
                                }
                                graphVisualCutting[graph0][graph1] = 1;
                                graphVisualCutting[graph1][graph0] = 1;

                                //// sphere
                                //sprintf(name, "XZHSphere%d", tri[0]);
                                //visualSphere22 = std::make_shared<imstk::Sphere>();
                                //visualSphere22->setTranslation(posVisualListTmp[tri[0]]);
                                //visualSphere22->setRadius(rads);
                                //sphere2Obj = std::make_shared<SceneObject>(name);
                                //sphere2Obj->setVisualGeometry(visualSphere22);
                                //m_scene->addSceneObject(sphere2Obj);
                                //for (auto obj : m_scene->getSceneObjects())
                                //{
                                //    if (obj->getName() == name)
                                //    {
                                //        obj->getVisualGeometry()->setTranslation(posVisualListTmp[tri[0]]);
                                //        break;
                                //    }
                                //}
                                //// sphere
                                //sprintf(name, "XZHSphere%d", tri[1]);
                                //visualSphere23 = std::make_shared<imstk::Sphere>();
                                //visualSphere23->setTranslation(posVisualListTmp[tri[1]]);
                                //visualSphere23->setRadius(rads);
                                //sphere3Obj = std::make_shared<SceneObject>(name);
                                //sphere3Obj->setVisualGeometry(visualSphere23);
                                //m_scene->addSceneObject(sphere3Obj);
                                //for (auto obj : m_scene->getSceneObjects())
                                //{
                                //    if (obj->getName() == name)
                                //    {
                                //        obj->getVisualGeometry()->setTranslation(posVisualListTmp[tri[1]]);
                                //        break;
                                //    }
                                //}
                                break;
                            case 0x05: // 101
                                // vert 0
                                it = mapCutVertices.find(tri[0]);
                                if (it != mapCutVertices.end()) // existing
                                {
                                    graph0 = it->second;
                                }
                                else // not exist
                                {
                                    graph0 = graphSize;
                                    mapCutVertices.insert(make_pair(tri[0], graphSize));  // start from 0
                                    graphSize++;
                                }
                                // vert 2
                                it = mapCutVertices.find(tri[2]);
                                if (it != mapCutVertices.end()) // existing
                                {
                                    graph2 = it->second;
                                }
                                else // not exist
                                {
                                    graph2 = graphSize;
                                    mapCutVertices.insert(make_pair(tri[2], graphSize));  // start from 0
                                    graphSize++;
                                }
                                graphVisualCutting[graph0][graph2] = 1;
                                graphVisualCutting[graph2][graph0] = 1;

                                //// sphere
                                //sprintf(name, "XZHSphere%d", tri[0]);
                                //visualSphere22 = std::make_shared<imstk::Sphere>();
                                //visualSphere22->setTranslation(posVisualListTmp[tri[0]]);
                                //visualSphere22->setRadius(rads);
                                //sphere2Obj = std::make_shared<SceneObject>(name);
                                //sphere2Obj->setVisualGeometry(visualSphere22);
                                //m_scene->addSceneObject(sphere2Obj);
                                //for (auto obj : m_scene->getSceneObjects())
                                //{
                                //    if (obj->getName() == name)
                                //    {
                                //        obj->getVisualGeometry()->setTranslation(posVisualListTmp[tri[0]]);
                                //        break;
                                //    }
                                //}

                                //// sphere
                                //sprintf(name, "XZHSphere%d", tri[2]);
                                //visualSphere23 = std::make_shared<imstk::Sphere>();
                                //visualSphere23->setTranslation(posVisualListTmp[tri[2]]);
                                //visualSphere23->setRadius(rads);
                                //sphere3Obj = std::make_shared<SceneObject>(name);
                                //sphere3Obj->setVisualGeometry(visualSphere23);
                                //m_scene->addSceneObject(sphere3Obj);
                                //for (auto obj : m_scene->getSceneObjects())
                                //{
                                //    if (obj->getName() == name)
                                //    {
                                //        obj->getVisualGeometry()->setTranslation(posVisualListTmp[tri[2]]);
                                //        break;
                                //    }
                                //}
                                break;
                            case 0x06: // 110
                                // vert 1
                                it = mapCutVertices.find(tri[1]);
                                if (it != mapCutVertices.end()) // existing
                                {
                                    graph1 = it->second;
                                }
                                else // not exist
                                {
                                    graph1 = graphSize;
                                    mapCutVertices.insert(make_pair(tri[1], graphSize));  // start from 0
                                    graphSize++;
                                }
                                // vert 2
                                it = mapCutVertices.find(tri[2]);
                                if (it != mapCutVertices.end()) // existing
                                {
                                    graph2 = it->second;
                                }
                                else // not exist
                                {
                                    graph2 = graphSize;
                                    mapCutVertices.insert(make_pair(tri[2], graphSize));  // start from 0
                                    graphSize++;
                                }
                                graphVisualCutting[graph1][graph2] = 1;
                                graphVisualCutting[graph2][graph1] = 1;

                                //// sphere
                                //sprintf(name, "XZHSphere%d", tri[1]);
                                //visualSphere22 = std::make_shared<imstk::Sphere>();
                                //visualSphere22->setTranslation(posVisualListTmp[tri[1]]);
                                //visualSphere22->setRadius(rads);
                                //sphere2Obj = std::make_shared<SceneObject>(name);
                                //sphere2Obj->setVisualGeometry(visualSphere22);
                                //m_scene->addSceneObject(sphere2Obj);
                                //for (auto obj : m_scene->getSceneObjects())
                                //{
                                //    if (obj->getName() == name)
                                //    {
                                //        obj->getVisualGeometry()->setTranslation(posVisualListTmp[tri[1]]);
                                //        break;
                                //    }
                                //}
                                //// sphere
                                //sprintf(name, "XZHSphere%d", tri[2]);
                                //visualSphere23 = std::make_shared<imstk::Sphere>();
                                //visualSphere23->setTranslation(posVisualListTmp[tri[2]]);
                                //visualSphere23->setRadius(rads);
                                //sphere3Obj = std::make_shared<SceneObject>(name);
                                //sphere3Obj->setVisualGeometry(visualSphere23);
                                //m_scene->addSceneObject(sphere3Obj);
                                //for (auto obj : m_scene->getSceneObjects())
                                //{
                                //    if (obj->getName() == name)
                                //    {
                                //        obj->getVisualGeometry()->setTranslation(posVisualListTmp[tri[2]]);
                                //        break;
                                //    }
                                //}
                                break;
                            }
                        }
                        else // no intersection
                        {
                            triListsTmp.push_back(tri);
                            m_trianglesCutNo.push_back(tri);
                        }
                    }
                    //if (cutNum < 1) continue;  // no cuts

                    ////get the remain triangles
                    //auto& trianglesXZH = visualColonMesh->getTrianglesVerticesXZH();
                    //std::vector<SurfaceMesh::TriangleArray> trianglesXZH2 = trianglesXZH;
                    //for (int i = 0; i < triCutPathTriIdx.size(); i++)
                    //{
                    //    int num = triCutPathTriIdx[i];  // global triangle index
                    //    trianglesXZH2.erase(trianglesXZH2.begin() + num);
                    //}
                    //triListsTmp.clear();
                    //triListsTmp = trianglesXZH2;

                    //DWORD tt7 = GetTickCount();
                    //printf("cut determine time %d ms\n", tt7 - tt0);
                    // update visual graph value
                    //auto& operationTriangles = visualColonMesh->getOperationTriangles();
                    //for (int i = 0; i < operationTriangles.size(); i++)
                    //for (int i = 0; i < triLists.size(); i++)  ////< XZH using this graph is correct
                    //for (int i = 0; i < triCutPathTriIdx.size();i++)
                    for (int i = 0; i < triListsCutPath.size(); i++)   ////< XZH using this graph is not correct, find out why?!???
                        //for (int i = 0; i < triListsTmp.size(); i++)
                    {
                        auto tri = triListsCutPath[i]; // triLists[i]; //  trianglesXZH[operationTriangles[i]];  //   // triLists[i]; // triLists[]; //   triLists[triCutPathTriIdx[i]]; //   triListsCutPath[i]; // triLists[i]; // triListsCutPath[i]; //  triListsTmp[i]; //
                        std::unordered_map<int, int>::iterator iit;
                        int vert0 = tri[0];
                        int vert1 = tri[1];
                        int vert2 = tri[2];

                        int vert0L = -1, vert1L = -1, vert2L = -1;
                        // map
                        iit = mapCutVertices.find(vert0);
                        if (iit != mapCutVertices.end()) // existing
                        {
                            vert0L = iit->second;
                        }
                        iit = mapCutVertices.find(vert1);
                        if (iit != mapCutVertices.end()) // existing
                        {
                            vert1L = iit->second;
                        }
                        iit = mapCutVertices.find(vert2);
                        if (iit != mapCutVertices.end()) // existing
                        {
                            vert2L = iit->second;
                        }
                        if (vert0L >= 0) // 0 yes
                        {
                            if (vert1L >= 0) // 1 yes
                            {
                                if (vert2L >= 0) // 2 yes  111=7
                                {
                                    graphVisualCutting[vert0L][vert1L] = 1;
                                    graphVisualCutting[vert1L][vert0L] = 1;
                                    graphVisualCutting[vert0L][vert2L] = 1;
                                    graphVisualCutting[vert2L][vert0L] = 1;
                                    graphVisualCutting[vert1L][vert2L] = 1;
                                    graphVisualCutting[vert2L][vert1L] = 1;
                                }
                                else // 2 no 011=3
                                {
                                    graphVisualCutting[vert0L][vert1L] = 1;
                                    graphVisualCutting[vert1L][vert0L] = 1;
                                }
                            }
                            else // 1 no   
                            {
                                if (vert2L >= 0) // 2 yes  101=5
                                {
                                    graphVisualCutting[vert0L][vert2L] = 1;
                                    graphVisualCutting[vert2L][vert0L] = 1;
                                }
                                else // 2 no 001=1
                                {

                                }
                            }
                        }
                        else // 0 no
                        {
                            if (vert1L >= 0) // 1 yes
                            {
                                if (vert2L >= 0) // 2 yes 110=6
                                {
                                    graphVisualCutting[vert1L][vert2L] = 1;
                                    graphVisualCutting[vert2L][vert1L] = 1;
                                }
                                else // 2 no 010=2
                                {

                                }
                            }
                            else // 1 no
                            {
                                if (vert2L >= 0) // 2 yes 100=4
                                {

                                }
                                else // 2 no 000=0
                                {

                                }
                            }
                        }
                    }

                    DWORD tt8 = GetTickCount();
                    //printf("cut update visual graph time %d ms\n", tt8 - tt0);

                    // update visual graph
                    m_graphVisualCutting->setGraphSize(graphSize);
                    m_graphVisualCutting->initGraph();
                    m_graphVisualCutting->updateGraph(graphVisualCutting);
                    //m_graphVisualCutting->traverseGraph(); //  m_graphVisualCutting->depthFirstSearch(0); // conn
                    //m_graphVisualCutting->dfs();
                    //if (graphSize>160)
                    //{
                    //    m_graphVisualCutting->findAllCircles(graphSize); // all circles another method
                    //}
                    //thread thread1(&SceneManager::runGraph, this);
                    //printf("cycle elem max num:%d\n", m_graphVisualCutting->getCycleElemNum());
                    //thread1.join();
                    /*thread1.detach();*/
                    /*FILE *fileGraph = fopen("i:/visualGraph.dat", "w");
                    fprintf(fileGraph, "%d:\n", graphSize);
                    auto& vecIDs = m_graphVisualCutting->getIDs();
                    for (int i = 0; i < graphSize; i++)
                    {
                    fprintf(fileGraph, "%d \n", vecIDs[i]);
                    }
                    fprintf(fileGraph, "\n");
                    for (int i = 0; i < graphSize; i++)
                    {
                    for (int j = 0; j < graphSize; j++)
                    {
                    fprintf(fileGraph, "%d ", graphVisualCutting[i][j]);
                    }
                    fprintf(fileGraph, "\n");
                    }
                    fclose(fileGraph);*/
                    // END update visual graph

                    // test
                    triLists = m_trianglesCutNo; // triListsTmp;
                    posVisualList = posVisualListTmp;

                    DWORD tt9 = GetTickCount();
                    //printf("cut visual graph time %d ms\n", tt9 - tt0);
                    //visualColonMesh->setVertexPositions(posVisualListTmp);
                    //auto& posVisualList2 = visualColonMesh->getVertexPositions(Geometry::DataType::PreTransform);  //getVertexPositionsChangeable(); // changable 
                    //FILE *file0 = fopen("i:/visualPos0.dat", "w");
                    //FILE *file1 = fopen("i:/visualPos1.dat", "w");
                    //for (int i = 0; i < posVisualListTmp.size();i++)
                    //{
                    //    visualColonMesh->setVertexPosition(i, Vec3d(0, 0, 0));
                    //    auto post = posVisualListTmp[i];
                    //    auto post1 = posVisualList2[i];
                    //    fprintf(file0, "%f %f %f\n", post.x(), post.y(), post.z());
                    //    fprintf(file1, "%f %f %f\n", post1.x(), post1.y(), post1.z());
                    //}
                    //fclose(file0);
                    //fclose(file1);

                    //auto& posVisualList3 = visualColonMesh->getVertexPositions(Geometry::DataType::PreTransform);
                    //auto surfMeshNew = std::make_shared<imstk::SurfaceMesh>();
                    //surfMeshNew->initialize(posVisualList, triLists, false);
                    //auto objectVolume = std::make_shared<imstk::VisualObject>("surfMeshNew");
                    //auto materialVolume = std::make_shared<RenderMaterial>();
                    //materialVolume->setDisplayMode(imstk::RenderMaterial::DisplayMode::WIREFRAME); // WIREFRAME SURFACE
                    //auto tmpVolume = std::make_shared<Texture>("I:/iMSTK/resources/VESS/testtest.png", Texture::Type::DIFFUSE); // InjectionNeedle_BaseColor
                    //materialVolume->addTexture(tmpVolume);
                    //surfMeshNew->setRenderMaterial(materialVolume);
                    //objectVolume->setVisualGeometry(surfMeshNew); // change to any mesh created above
                    ////m_scene->addSceneObject(objectVolume); 

                    ////< XZH // need or not??!!??  influence efficiency very much by VULKAN?!!!@@@@ 
                    //visualColonMesh->setVertexPositions(posVisualList);
                    //visualColonMesh->setTrianglesVertices(triLists);
                    //visualColonMesh->setTopologyChangedFlag(true);

                    //// display cut path
                    //vecCutPathVertices.insert(vecCutPathVertices.end(), vecCutVerts.begin(), vecCutVerts.end());
                    //sort(vecCutPathVertices.begin(), vecCutPathVertices.end());
                    //vecCutPathVertices.erase(unique(vecCutPathVertices.begin(), vecCutPathVertices.end()), vecCutPathVertices.end());
                    //StdVectorOfVec3d vertPositions;
                    // Renumber the vertices
                    /*std::list<int> uniqueVertIdList;
                    for (const auto &face : triListsCutPath)
                    {
                    uniqueVertIdList.push_back(face[0]);
                    uniqueVertIdList.push_back(face[1]);
                    uniqueVertIdList.push_back(face[2]);
                    }
                    uniqueVertIdList.sort();
                    uniqueVertIdList.unique();
                    int vertId;
                    std::list<int>::iterator it;
                    for (vertId = 0, it = uniqueVertIdList.begin(); it != uniqueVertIdList.end(); ++vertId, it++)
                    {
                    vertPositions.push_back(posVisualList[*it]);
                    for (auto &face : triListsCutPath)
                    {
                    for (size_t i = 0; i < 3; ++i)
                    {
                    if (face[i] == *it)
                    {
                    face[i] = vertId;
                    }
                    }
                    }
                    }*/

                    bool isExist = false;
                    std::shared_ptr<SurfaceMesh> surfaceMeshCutPath;
                    for (auto objxzh : m_scene->getSceneObjects())
                    {
                        if (objxzh->getName() == "visualCuttingPath")
                        {
                            surfaceMeshCutPath = std::dynamic_pointer_cast<SurfaceMesh>(objxzh->getVisualGeometry());
                            break;
                        }
                    }
                    if (surfaceMeshCutPath)
                    {
                        isExist = true;
                        auto posVisualListXZH = posVisualList;
                        auto triCutPathXZH = triListsCutPath;
                        for (int t = 0; t < posVisualList.size(); t++)
                        {
                            posVisualListXZH[t] = posVisualList[t] + Vec3d(0, 0, 0.01);
                        }
                        StdVectorOfVec3d newVertPositions; // deform mesh positions
                        std::list<int> uniqueNewVertIdList; // the vertex list in the deform mesh
                        for (const auto &face : triCutPathXZH)
                        {
                            uniqueNewVertIdList.push_back(face[0]);
                            uniqueNewVertIdList.push_back(face[1]);
                            uniqueNewVertIdList.push_back(face[2]);
                        }
                        uniqueNewVertIdList.sort();
                        uniqueNewVertIdList.unique();
                        int vertId;
                        std::list<int>::iterator it;
                        // StdVectorOfVec3d vertPositions; 
                        newVertPositions.clear();
                        for (vertId = 0, it = uniqueNewVertIdList.begin(); it != uniqueNewVertIdList.end(); ++vertId, it++)
                        {
                            newVertPositions.push_back(posVisualListXZH[*it]); // coords
                            for (auto &face : triCutPathXZH)  // reference, traverse all the physSTris to check whether this vertex global index number == *it (global index)
                            {
                                for (int i = 0; i < 3; ++i)
                                {
                                    if (face[i] == *it)
                                    {
                                        face[i] = vertId; // change the global index to local index 
                                    }
                                }
                            }
                        } // END for std::list
                        if (triCutPathXZH.size()>0)
                        {
                            surfaceMeshCutPath->setVertexPositions(newVertPositions); // newVertPositions); // (posVisualListXZH); // posVisualList); // vertPositions
                            surfaceMeshCutPath->setTrianglesVertices(triCutPathXZH);
                            surfaceMeshCutPath->setTopologyChangedFlag(true);
                        }                    
                    }
                    else
                    {
                        isExist = false;
                        auto posVisualListXZH = posVisualList;
                        auto triCutPathXZH = triListsCutPath;
                        for (int t = 0; t < posVisualList.size(); t++)
                        {
                            posVisualListXZH[t] = posVisualList[t] + Vec3d(0, 0, 0.01);
                        }
                        StdVectorOfVec3d newVertPositions; // deform mesh positions
                        std::list<int> uniqueNewVertIdList; // the vertex list in the deform mesh
                        for (const auto &face : triCutPathXZH)
                        {
                            uniqueNewVertIdList.push_back(face[0]);
                            uniqueNewVertIdList.push_back(face[1]);
                            uniqueNewVertIdList.push_back(face[2]);
                        }
                        uniqueNewVertIdList.sort();
                        uniqueNewVertIdList.unique();
                        int vertId;
                        std::list<int>::iterator it;
                        // StdVectorOfVec3d vertPositions; 
                        newVertPositions.clear();
                        for (vertId = 0, it = uniqueNewVertIdList.begin(); it != uniqueNewVertIdList.end(); ++vertId, it++)
                        {
                            newVertPositions.push_back(posVisualListXZH[*it]); // coords
                            for (auto &face : triCutPathXZH)  // reference, traverse all the physSTris to check whether this vertex global index number == *it (global index)
                            {
                                for (int i = 0; i < 3; ++i)
                                {
                                    if (face[i] == *it)
                                    {
                                        face[i] = vertId; // change the global index to local index 
                                    }
                                }
                            }
                        } // END for std::list

                        surfaceMeshCutPath = std::make_shared<imstk::SurfaceMesh>();
                        surfaceMeshCutPath->initialize(newVertPositions/*newVertPositions*/, triCutPathXZH); // vertPositions 
                        // visualization
                        auto material = std::make_shared<RenderMaterial>();
                        auto diffuseTexture = std::make_shared<Texture>("I:/iMSTK/resources/VESS/testtest3.png", Texture::Type::DIFFUSE); // testtest2 colon_texture_mat_Albedo.png
                        material->addTexture(diffuseTexture);
                        material->setDisplayMode(RenderMaterial::DisplayMode::SURFACE);
                        surfaceMeshCutPath->setRenderMaterial(material);
                        auto objectCuttingPath = std::make_shared<imstk::VisualObject>("visualCuttingPath");
                        objectCuttingPath->setVisualGeometry(surfaceMeshCutPath); // change to any mesh created above
                        m_scene->addSceneObject(objectCuttingPath);
                    }
                    // END display cutting

                      ////< XZH display cutting this time
                    //bool isExistCur = false;
                    //std::shared_ptr<SurfaceMesh> surfaceMeshCutPathCur;
                    //for (auto objxzh : m_scene->getSceneObjects())
                    //{
                    //    if (objxzh->getName() == "visualCuttingPathCur")
                    //    {
                    //        surfaceMeshCutPathCur = std::dynamic_pointer_cast<SurfaceMesh>(objxzh->getVisualGeometry());
                    //        break;
                    //    }
                    //}
                    //if (surfaceMeshCutPathCur)
                    //{
                    //    isExistCur = true;
                    //    auto posVisualListXZH = posVisualList;
                    //    std::vector<SurfaceMesh::TriangleArray> triCutPathCurXZH = m_trianglesCutCurrent;
                    //    
                    //    //for (int k = 0; k < m_trianglesNumCut.size();k++)
                    //    //{
                    //    //    auto& tri = triListsXZH[m_trianglesNumCut[k]];
                    //    //    triCutPathCurXZH.push_back(tri);
                    //    //}
                    //    for (int t = 0; t < posVisualList.size(); t++)
                    //    {
                    //        posVisualListXZH[t] = posVisualList[t] + Vec3d(0, 0, 0.01);
                    //    }
                    //    StdVectorOfVec3d newVertPositions; // deform mesh positions
                    //    std::list<int> uniqueNewVertIdList; // the vertex list in the deform mesh
                    //    for (const auto &face : triCutPathCurXZH)
                    //    {
                    //        uniqueNewVertIdList.push_back(face[0]);
                    //        uniqueNewVertIdList.push_back(face[1]);
                    //        uniqueNewVertIdList.push_back(face[2]);
                    //    }
                    //    uniqueNewVertIdList.sort();
                    //    uniqueNewVertIdList.unique();
                    //    int vertId;
                    //    std::list<int>::iterator it;
                    //    // StdVectorOfVec3d vertPositions; 
                    //    newVertPositions.clear();
                    //    for (vertId = 0, it = uniqueNewVertIdList.begin(); it != uniqueNewVertIdList.end(); ++vertId, it++)
                    //    {
                    //        newVertPositions.push_back(posVisualListXZH[*it]); // coords
                    //        for (auto &face : triCutPathCurXZH)  // reference, traverse all the physSTris to check whether this vertex global index number == *it (global index)
                    //        {
                    //            for (int i = 0; i < 3; ++i)
                    //            {
                    //                if (face[i] == *it)
                    //                {
                    //                    face[i] = vertId; // change the global index to local index 
                    //                }
                    //            }
                    //        }
                    //    } // END for std::list

                    //    surfaceMeshCutPathCur->setVertexPositions(newVertPositions); // (posVisualListXZH); // posVisualList); // vertPositions
                    //    surfaceMeshCutPathCur->setTrianglesVertices(triCutPathCurXZH);
                    //    surfaceMeshCutPathCur->setTopologyChangedFlag(true);
                    //}
                    //else
                    //{
                    //    isExistCur = false;
                    //    auto posVisualListXZH = posVisualList;
                    //    std::vector<SurfaceMesh::TriangleArray> triCutPathCurXZH = m_trianglesCutCurrent;

                    //    //for (int k = 0; k < m_trianglesNumCut.size(); k++)
                    //    //{
                    //    //    auto& tri = triListsXZH[m_trianglesNumCut[k]];
                    //    //    triCutPathCurXZH.push_back(tri);
                    //    //}
                    //    for (int t = 0; t < posVisualList.size(); t++)
                    //    {
                    //        posVisualListXZH[t] = posVisualList[t] + Vec3d(0, 0, 0.01);
                    //    }
                    //    StdVectorOfVec3d newVertPositions; // deform mesh positions
                    //    std::list<int> uniqueNewVertIdList; // the vertex list in the deform mesh
                    //    for (const auto &face : triCutPathCurXZH)
                    //    {
                    //        uniqueNewVertIdList.push_back(face[0]);
                    //        uniqueNewVertIdList.push_back(face[1]);
                    //        uniqueNewVertIdList.push_back(face[2]);
                    //    }
                    //    uniqueNewVertIdList.sort();
                    //    uniqueNewVertIdList.unique();
                    //    int vertId;
                    //    std::list<int>::iterator it;
                    //    // StdVectorOfVec3d vertPositions; 
                    //    newVertPositions.clear();
                    //    for (vertId = 0, it = uniqueNewVertIdList.begin(); it != uniqueNewVertIdList.end(); ++vertId, it++)
                    //    {
                    //        newVertPositions.push_back(posVisualListXZH[*it]); // coords
                    //        for (auto &face : triCutPathCurXZH)  // reference, traverse all the physSTris to check whether this vertex global index number == *it (global index)
                    //        {
                    //            for (int i = 0; i < 3; ++i)
                    //            {
                    //                if (face[i] == *it)
                    //                {
                    //                    face[i] = vertId; // change the global index to local index 
                    //                }
                    //            }
                    //        }
                    //    } // END for std::list

                    //    surfaceMeshCutPathCur = std::make_shared<imstk::SurfaceMesh>();
                    //    surfaceMeshCutPathCur->initialize(newVertPositions, triCutPathCurXZH); // vertPositions 
                    //    // visualization
                    //    auto material = std::make_shared<RenderMaterial>();
                    //    auto diffuseTexture = std::make_shared<Texture>("I:/iMSTK/resources/VESS/testtest3.png", Texture::Type::DIFFUSE); // testtest2 colon_texture_mat_Albedo.png
                    //    material->addTexture(diffuseTexture);
                    //    material->setDisplayMode(RenderMaterial::DisplayMode::SURFACE);
                    //    surfaceMeshCutPathCur->setRenderMaterial(material);
                    //    auto objectCuttingPath = std::make_shared<imstk::VisualObject>("visualCuttingPathCur");
                    //    objectCuttingPath->setVisualGeometry(surfaceMeshCutPathCur); // change to any mesh created above
                    //    m_scene->addSceneObject(objectCuttingPath);
                    //}
                    //// END display cutting this time

                    /*auto tetTriMap = std::dynamic_pointer_cast<TetraTriangleMap>(pbdvcObj->getPhysicsToVisualMap());
                    tetTriMap->update2(numOrigin);
                    pbdvcObj->setPhysicsToVisualMap(tetTriMap);
                    FILE *outMap = fopen("i:/outTetTriMap2.dat", "w");
                    auto& m_verticesEnclosingTetraId = tetTriMap->getTetraID();
                    auto& m_verticesWeights = tetTriMap->getVertWeights();
                    for (size_t vertexId = 0; vertexId < m_verticesEnclosingTetraId.size(); ++vertexId)
                    {
                    fprintf(outMap, "l %d %f %f %f\n", m_verticesEnclosingTetraId.at(vertexId), m_verticesWeights.at(vertexId)[0], m_verticesWeights.at(vertexId)[1], m_verticesWeights.at(vertexId)[2]);
                    }
                    fclose(outMap);*/
                    ////fixed point
                    //std::string fixed_corner;
                    //char intStr[33];
                    //Vec3d tmpPos;
                    //std::vector<int> fixedVertice; // add later 06/04/2017
                    //auto& fixedStates = cutTissueMesh->getFixedVerticeStates();
                    //auto& mapTetrahedrons = cutTissueMesh->getTetrahedronMap();
                    //auto& tetListCutAll = cutTissueMesh->getCutElementsTotal();  // just current 
                    //int minVert = 218; // colon 2467;  //  cube 218;
                    //for (int k = 0; k < posTetlist.size(); k++)
                    //{
                    //    if ((posTetlist[k] - posTetlist[minVert]).norm() > 16.0) //  Colon: 2.9, because 2.6 is tumor radus   CUBE 16.0: so 16.0 for cube because 14.0 sphere w.r.t. tumor
                    //    {
                    //        std::sprintf(intStr, "%d", k + 1);
                    //        fixed_corner += std::string(intStr) + ' ';
                    //        fixedVertice.push_back(k); //  all vertices
                    //    }
                    //}

                    //cutTissueMesh->setFixedVertice(fixedVertice);
                    //cutTissueMesh->updateFixedVertice();
                    //auto dynaModel1 = std::static_pointer_cast<PbdModel>(pbdvcObj->getDynamicalModel());
                    //dynaModel1->configure(/*Number of Constraints*/2,
                    //    /*Constraint configuration*/"Volume 0.7",  // 0.7 "Dihedral 0.8", // 0.01 0.8 effect is couhe not very well
                    //    /*Constraint configuration*/"Distance 1.0",  // 0.9 effect is couhe
                    //    /*Mass*/0.1, // 0.1
                    //    /*Gravity*/"-0.3 0 0",  //"0 0 -9.8",  // "0 -9.8 0",
                    //    /*TimeStep*/0.01, // 0.001
                    //    /*FixedPoint*/ fixed_corner.c_str(),  // "", //  fixed_corner.c_str()
                    //    /*NumberOfIterationInConstraintSolver*/2, //  3  2 12
                    //    /*Proximity*/0.01,  // 0.1
                    //    /*Contact stiffness*/0.1);
                    //dynaModel1->initializeReset();

                    hasCut = true;
                    isEnd = true;

                    // record all the cut and determine
                    baseposCutPlane.push_back(tri1[0]);
                    normCutPlane.push_back(normCut);
                    // generating new surface mesh with new generated points
                    int cycleElemNum = m_graphVisualCutting->getCycleElemNum();
                    //printf("Graph size :%d \n", graphSize);
                    //continue;
                    if (graphSize>=1000)
                    {
                        cycleElemNum = 1000;
                    }
                    if (cycleElemNum <1000) // 264)
                    {
                        //thread1.detach();
                        DWORD tt10 = GetTickCount();
                        //printf("cut time %d ms\n", tt10 - tt0);
                        continue; //  colon: 45 cube: 25 colon: 20 or more  cube:8    dense mesh bigger
                    }
                    else // remove tumor temp
                    {
                        isNoCut = true;
                        printf("Cycle element is more than 8~!!!");
                        //thread1.join();
                        // get all the global cut vertex index
                        auto& cycleElems = m_graphVisualCutting->getCycleElem();
                        cycleElems.clear();
                        for (int t = 0; t < 264; t++)
                        {
                            cycleElems.push_back(t);
                        }
                        std::vector<int> cycleElemsGlobal;
                        cycleElemsGlobal.resize(cycleElems.size());
                        std::unordered_map<int, int>::iterator iit;
                        printf("cycle elem nums is: %d\n", cycleElems.size());
                        /*std::vector<std::vector<int>> vecGroups;
                        vecGroups.resize(1000);
                        for (int n = 0; n < cycleElems.size(); n++)
                        {
                        int idxlocal = cycleElems[n];
                        int vid = vecIDs[idxlocal];
                        vecGroups[vid].push_back(idxlocal);
                        printf("vecIDS: %d\n", vecIDs[idxlocal]);
                        }
                        std::vector<std::vector<int>> vecGroupsLast;
                        for (int n = 0; n < vecGroups.size();n++)
                        {
                        std::vector<int> vec;
                        if (vecGroups[n].size() > 0) vecGroupsLast.push_back(vec);
                        }
                        int maxElemVec = -1;
                        int maxN = -1;
                        for (int n = 0; n < vecGroupsLast.size();n++)
                        {
                        if (vecGroupsLast[n].size() > maxElemVec)
                        {
                        maxElemVec = vecGroupsLast[n].size();
                        maxN = n;
                        }
                        }
                        cycleElems = vecGroupsLast[maxN];*/
                        for (int n = 0; n < cycleElems.size(); n++)
                        {
                            int idxlocal = cycleElems[n];
                            // map
                            for (iit = mapCutVertices.begin(); iit != mapCutVertices.end(); iit++)
                            {
                                int idxg = iit->first;
                                int idxl = iit->second;
                                if (idxlocal == idxl)
                                {
                                    cycleElemsGlobal[n] = idxg;
                                    break;
                                }
                            }
                        }

                        // find out all the vertices in the tumor area
                        std::vector<int> vecAllCutVertices = cycleElemsGlobal;
                        for (int q = 0; q < vecAllCutVertices.size(); q++)
                        {
                            int idxg = vecAllCutVertices[q];
                            for (int t = 0; t < triLists.size(); t++)
                            {
                                auto& tri = triLists[t];
                                if ((tri[0] == idxg) || (tri[1] == idxg) || (tri[2] == idxg)) // if, all these 3 should add into vec
                                {
                                    vecAllCutVertices.push_back(tri[0]);
                                    vecAllCutVertices.push_back(tri[1]);
                                    vecAllCutVertices.push_back(tri[2]);
                                    sort(vecAllCutVertices.begin(), vecAllCutVertices.end());
                                    vecAllCutVertices.erase(unique(vecAllCutVertices.begin(), vecAllCutVertices.end()), vecAllCutVertices.end());
                                }
                            }

                        }
                        // find out some missing
                        for (int q = 0; q < vecAllCutVertices.size(); q++)
                        {
                            int idxg = vecAllCutVertices[q];
                            for (int t = 0; t < triLists.size(); t++)
                            {
                                auto& tri = triLists[t];
                                if ((tri[0] == idxg) || (tri[1] == idxg) || (tri[2] == idxg)) // if, all these 3 should add into vec
                                {
                                    vecAllCutVertices.push_back(tri[0]);
                                    vecAllCutVertices.push_back(tri[1]);
                                    vecAllCutVertices.push_back(tri[2]);
                                    sort(vecAllCutVertices.begin(), vecAllCutVertices.end());
                                    vecAllCutVertices.erase(unique(vecAllCutVertices.begin(), vecAllCutVertices.end()), vecAllCutVertices.end());
                                }
                            }

                        }

                        // remove
                        std::vector<SurfaceMesh::TriangleArray> triListsLast;
                        std::vector<SurfaceMesh::TriangleArray> triListsLastMove;
                        for (int n = 0; n < triLists.size(); n++)
                        {
                            auto& tri = triLists[n];
                            auto center = (posVisualList[tri[0]] + posVisualList[tri[1]] + posVisualList[tri[2]]) / 3;
                            bool flag = true;
                            for (int q = 0; q < vecAllCutVertices.size(); q++)
                            {
                                int idxg = vecAllCutVertices[q];
                                if ((tri[0] == idxg) || (tri[1] == idxg) || (tri[2] == idxg))
                                {
                                    flag = false;
                                    break; // if one is related to cut, just remove
                                }
                            }
                            if ((!flag) && ((center - posVisualList[indexTumorVertex]).norm()<1.0))  // add a 1.7 radius only for colon
                            {
                                triListsLastMove.push_back(tri);
                            }
                            else triListsLast.push_back(tri);
                        }
                        // dissection
                        triLists = triListsLast;
                        //triListsLast.clear();
                        //triListsLastMove.clear();
                        //for (int n = 0; n < triLists.size(); n++)
                        //{
                        //    auto& tri = triLists[n];
                        //    auto center = (posVisualList[tri[0]] + posVisualList[tri[1]] + posVisualList[tri[2]]);
                        //    bool isTumorSide = true;
                        //    for (int n = 0; n < normCutPlane.size();n++) // n-1  the previous all cuts
                        //    {
                        //        Vec3d vecVert = center - baseposCutPlane[n];
                        //        if (vecVert.dot(normCutPlane[n]) <= 0)
                        //        {
                        //            isTumorSide = false;
                        //            break;
                        //        }
                        //    }
                        //    if (isTumorSide) triListsLastMove.push_back(tri);
                        //    else triListsLast.push_back(tri);
                        //}
                        //triLists = triListsLast;
                        // will influence the efficiency ?? need or not
                        visualColonMesh->setVertexPositions(posVisualList);
                        visualColonMesh->setTrianglesVertices(triLists);
                        visualColonMesh->setTopologyChangedFlag(true);

                        // moving dissected part
                        auto surfaceMeshMove = std::make_shared<imstk::SurfaceMesh>();
                        surfaceMeshMove->initialize(posVisualList, triListsLastMove); // vertPositions
                        // visualization
                        auto materialMove = std::make_shared<RenderMaterial>();
                        auto diffuseTextureMove = std::make_shared<Texture>("I:/iMSTK/resources/VESS/testtest2.png", Texture::Type::DIFFUSE); // testtest2 colon_texture_mat_Albedo.png
                        materialMove->addTexture(diffuseTextureMove);
                        materialMove->setDisplayMode(RenderMaterial::DisplayMode::SURFACE);
                        surfaceMeshMove->setRenderMaterial(materialMove);
                        auto objectMove = std::make_shared<imstk::VisualObject>("cuttingSurfaceMove");
                        objectMove->setVisualGeometry(surfaceMeshMove); // change to any mesh created above
                        m_scene->addSceneObject(objectMove);
                    }
                    // END cut

                    printf("done!");

                    // END CUT
                } // END button1
            } // END if virtual object
        }
        // END using TetTriMap for cube dense
    }
      ////< XZH thread END

    std::shared_ptr<Scene>
        SceneManager::getScene()
    {
        return m_scene;
    }

    void
        SceneManager::initModule()
    {
        // Start Camera Controller (asynchronous)
        if (auto camController = m_scene->getCamera()->getController())
        {
            this->startModuleInNewThread(camController);
        }

        // Update objects controlled by the device controllers XZH
        for (auto controller : m_scene->getSceneObjectControllers())
        {
            if (auto pbdController = std::dynamic_pointer_cast<PBDSceneObjectController>(controller))
            {
                pbdController->initOffsets();
            }
        }

        // Init virtual coupling objects offsets
        for (auto obj : m_scene->getSceneObjects())
        {
            if (auto virtualCouplingPBD = std::dynamic_pointer_cast<VirtualCouplingPBDObject>(obj))
            {
                virtualCouplingPBD->initOffsets();
            }
        }

        // test tool position
        for (auto obj : m_scene->getSceneObjects())
        {
            // haptic tool
            if (obj->getName() == "VirtualObject")
            {
                //auto pbdvcObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(obj);
                //auto visualMesh = std::dynamic_pointer_cast<SurfaceMesh>(pbdvcObj->getVisualGeometry());
                //auto collidingMesh = std::dynamic_pointer_cast<PointSet>(pbdvcObj->getCollidingGeometry());
                //Vec3d pos0 = collidingMesh->getVertexPosition(0);
                //Vec3d pos1 = collidingMesh->getVertexPosition(1);
                //Vec3d posV0 = visualMesh->getVertexPosition(0);
                //Vec3d dir = pos0 - pos1;
                //Vec3d dirV = pos0 - posV0;
                //Vec3d root = Vec3d(0, 0, 13.43) + countTool*Vec3d(0, 0, 0.001); // (-4.7, 3.3, 1);
                //Vec3d tip = Vec3d(0, 0, -13.43) + countTool*Vec3d(0, 0, 0.001); // (-4.7, 3.3, -6.5);
                //countTool++;
                //collidingMesh->setVertexPosition(0, root);
                //collidingMesh->setVertexPosition(1, root - dir);
                //auto& posList = visualMesh->getVertexPositionsChangeable();
                //Vec3d ttmp = countTool*Vec3d(0, 0, 0.001);
                //for (int i = 0; i < posList.size();i++)
                //{
                //    Vec3d postmp = posList[i]; // visualMesh->getVertexPosition(i);
                //    visualMesh->setVertexPosition(i, /*root - pos0*/ ttmp+postmp);
                //}
            }            
        }

        // navgation
        vecCamera.push_back(Vec3d(3.049765, -5.416139, -13.772857));
        vecCamera.push_back(Vec3d(2.066011, -2.092456, -16.283300));
        vecCamera.push_back(Vec3d(2.416454, 0.893009, -18.732140));
        vecCamera.push_back(Vec3d(2.656587, 3.308475, -21.841312));
        vecCamera.push_back(Vec3d(0.744602, 4.685533, -24.712399));
        vecCamera.push_back(Vec3d(-2.111103, 5.906062, -24.198272));

        vecCameraFocal.push_back(Vec3d(-4.065353, 2.550728, -18.045065));
        vecCameraFocal.push_back(Vec3d(-2.318216, 3.125296, -19.496111));
        vecCameraFocal.push_back(Vec3d(-0.275814, 4.097096, -20.704975));
        vecCameraFocal.push_back(Vec3d(-0.668360, 6.501790, -21.953945));
        vecCameraFocal.push_back(Vec3d(-0.777570, 7.189897, -22.593023));
        vecCameraFocal.push_back(Vec3d(-6.532188, 16.588575, -13.986447));

        // cutting and dissection for Graph init
        for (int i = 0; i < GRAPHMAPSIZEVISUAL;i++)
        {
            std::vector<int> glist;
            for (int j = 0; j < GRAPHMAPSIZEVISUAL; j++)
            {
                glist.push_back(0);
            }
            graphVisualCutting.push_back(glist);
        }
        m_graphVisualCutting = std::make_shared<imstk::UndirectedGraph>(graphVisualCutting);

        // cut nodes root
        posCutNodesRoot.resize(8);
        posCutNodesTip.resize(8);
        posCutNodesTip[0] = Vec3d(3.1, 3.6, 2);
        posCutNodesTip[1] = Vec3d(3.7, 3.6, 2);
        posCutNodesTip[2] = Vec3d(4.2, 4.1, 2);
        posCutNodesTip[3] = Vec3d(4.2, 4.6, 2);
        posCutNodesTip[4] = Vec3d(3.7, 5, 2);
        posCutNodesTip[5] = Vec3d(3.1, 5, 2);
        posCutNodesTip[6] = Vec3d(2.5, 4.6, 2);
        posCutNodesTip[7] = Vec3d(2.5, 4.1, 2);
        // tip
        posCutNodesRoot[0] = Vec3d(3.1, 3.6, 9.75);
        posCutNodesRoot[1] = Vec3d(3.7, 3.6, 9.75);
        posCutNodesRoot[2] = Vec3d(4.2, 4.1, 9.75);
        posCutNodesRoot[3] = Vec3d(4.2, 4.6, 9.75);
        posCutNodesRoot[4] = Vec3d(3.7, 5, 9.75);
        posCutNodesRoot[5] = Vec3d(3.1, 5, 9.75);
        posCutNodesRoot[6] = Vec3d(2.5, 4.6, 9.75);
        posCutNodesRoot[7] = Vec3d(2.5, 4.1, 9.75);

        for (int i = 0; i < 8;i++)
        {
            posCutNodesRoot[i] = posCutNodesRoot[i] + Vec3d(0, 0, 2.2);
            posCutNodesTip[i] = posCutNodesTip[i] + Vec3d(0, 0, 2.2);
        }

          ////< XZH 
        cutStartInit = Vec3d::Zero();
        cutEndInit = Vec3d::Zero();
    }

    void
        SceneManager::runModule()
    {
        // XZH
        t3 = t4;
        t4 = GetTickCount();
        //FILE *outForceFilter = fopen("G:/outInformation.dat", "a");
        //fprintf(outForceFilter, "gap time %d ms,  frame rate is: %f\n", t2 - t1, 1000 / double(t2 - t1));
        //fclose(outForceFilter);
        //printf("scene gap time %d ms,  frame rate is: %f\n", t4 - t3, 1000 / double(t4 - t3));
        //extractSurfaceMeshSleep(8);
        //FILE *outForceFilter = fopen("G:/outforcefilter.dat", "a");
        //fprintf(outForceFilter, "gap time %d ms,  frame rate is: %f\n", t2 - t1, 1000 / double(t2 - t1));
        //fclose(outForceFilter);
        // XZH END

        // XZH camera
        auto cam1 = m_scene->getCamera();
        Vec3d camPos = cam1->getPosition();
        Vec3d camFocalPoint = cam1->getFocalPoint();
        double viewAngle = cam1->getViewAngle();
        /*Vec3d vecCamera0 = Vec3d::Zero();
        cam1->setViewUp(0.01, 1, 0.01);
        Vec3d vecCameraFocal0 = Vec3d::Zero();
        if ((camPos - vecCamera[5]).norm() < 0.1)
        {
        numGap = 1000;
        }
        else
        {
        numGap++;
        }
        cam1->setPosition(vecCamera[4] + numGap*0.001*(vecCamera[5] - vecCamera[4]));
        cam1->setFocalPoint(vecCameraFocal[4] + numGap*0.001*(vecCameraFocal[5] - vecCameraFocal[4]));
        for (auto obj : m_scene->getLights())
        {
        if (obj->getName() == "whiteLight")
        {
        if (auto spotlight = std::dynamic_pointer_cast<PointLight>(obj))
        {
        spotlight->setPosition(camPos);
        spotlight->setFocalPoint(camFocalPoint);
        }
        }
        if (obj->getName() == "whiteLight2")
        {
        if (auto spotlight = std::dynamic_pointer_cast<PointLight>(obj))
        {
        spotlight->setPosition(vecCamera[1]);
        spotlight->setFocalPoint(vecCameraFocal[1]);
        }
        }
        if (obj->getName() == "whiteLight3")
        {
        if (auto spotlight = std::dynamic_pointer_cast<PointLight>(obj))
        {
        spotlight->setPosition(vecCamera[2]);
        spotlight->setFocalPoint(vecCameraFocal[2]);
        }
        }
        if (obj->getName() == "whiteLight4")
        {
        if (auto spotlight = std::dynamic_pointer_cast<PointLight>(obj))
        {
        spotlight->setPosition(vecCamera[3]);
        spotlight->setFocalPoint(vecCameraFocal[3]);
        }
        }
        if (obj->getName() == "whiteLight5")
        {
        if (auto spotlight = std::dynamic_pointer_cast<PointLight>(obj))
        {
        spotlight->setPosition(vecCamera[4]);
        spotlight->setFocalPoint(vecCameraFocal[4]);
        }
        }
        if (obj->getName() == "whiteLight6")
        {
        if (auto spotlight = std::dynamic_pointer_cast<PointLight>(obj))
        {
        spotlight->setPosition(vecCamera[5]);
        spotlight->setFocalPoint(vecCameraFocal[5]);
        }
        }
        }*/
        //    cam1->setFocalPoint(vecCameraFocal0 + numGap*0.01*(vecCameraFocal[navStep + 1] - vecCameraFocal[navStep]));//
        //printf("cam pos: %f %f %f, focal: %f %f %f, angle: %f\n", camPos.x(), camPos.y(), camPos.z(), camFocalPoint.x(), camFocalPoint.y(), camFocalPoint.z(), viewAngle);
        // 1:  3.049765, -5.416139, -13.772857     -4.065353, 2.550728, -18.045065
        //for (int m = vecCamera.size() - 1; m >= 0; m--)
        //{
        //    if ((camPos - vecCamera[m]).norm()<0.1)
        //    {
        //        navStep = m;
        //        //numGap = 0;
        //        vecCamera0 = vecCamera[m];//               cam1->setPosition(vecCamera[m]);
        //        vecCameraFocal0 = vecCameraFocal[m]; // cam1->setFocalPoint();//
        //        break;
        //    }
        //}
        //
        //if (navStep==5)
        //{
        //    numGap=0;
        //    cam1->setPosition(vecCamera0);
        //    cam1->setFocalPoint(vecCameraFocal0); 
        //}
        //else
        //{
        //    numGap++;
        //    //if (numGap>100)
        //    //{
        //    //    numGap = 0;
        //    //}
        //    cam1->setPosition(vecCamera0 + numGap*0.01*(vecCamera[navStep+1]-vecCamera[navStep]));
        //    cam1->setFocalPoint(vecCameraFocal0 + numGap*0.01*(vecCameraFocal[navStep + 1] - vecCameraFocal[navStep]));//
        //    if ((camPos - vecCamera[navStep + 1]).norm()<0.05)
        //    {
        //        numGap = 0;
        //    }
        //}
        // Navigation END

        Vec3d tip, root;
        tip = root = Vec3d::Zero();
        Vec3d centralPosition = Vec3d::Zero();
        StopWatch wwt;

        wwt.start();

        // Reset Contact forces to 0
        for (auto obj : m_scene->getSceneObjects())
        {
            if (auto defObj = std::dynamic_pointer_cast<DeformableObject>(obj))
            {
                defObj->getContactForce().setConstant(0.0);
            }
            //else if (auto collidingObj = std::dynamic_pointer_cast<CollidingObject>(obj))
            //{
            //    if (auto pbdvcObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(obj))
            //    {
            //    }
            //    else
            //    {
            //        //collidingObj->resetForce();  // XZH
            //    }
            //}
            if (auto pbdvcObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(obj))
            {
                // XZH start
                auto collidingMesh = std::dynamic_pointer_cast<PointSet>(pbdvcObj->getCollidingGeometry());
                root = collidingMesh->getVertexPosition(0);
                tip = collidingMesh->getVertexPosition(1); // 1 not 2
                pbdvcObj->resetForce();  // XZH
                // XZH END
            }
            // todo: refactor pbd
            // description: so that the transform obtained from device can be applied
            if (auto virtualCouplingPBD = std::dynamic_pointer_cast<VirtualCouplingPBDObject>(obj))
            {
                virtualCouplingPBD->resetCollidingGeometry();
            }
        }

        // Update objects controlled by the device controllers
        for (auto controller : m_scene->getSceneObjectControllers())
        {
            controller->updateControlledObjects();
        }

        // Compute collision data per interaction pair
        for (auto intPair : m_scene->getCollisionGraph()->getInteractionPairList())
        {
            intPair->computeCollisionData(tip);  // for others XZH intPair->computeCollisionData();
            intPair->computeContactForces();
        }
        //DWORD t7 = GetTickCount();
        //printf("scene before cutting time Phase0 %d ms,  \n", t7 - t4);
        //// Apply forces on device
        //for (auto controller : m_scene->getSceneObjectControllers())
        //{
        //    controller->applyForces();
        //}

        // Update the solvers
        for (auto solvers : m_scene->getSolvers())
        {
            // XZH test
            bool isColonInjected = false;
            for (auto intPair : m_scene->getCollisionGraph()->getPbdPairList())
            {
                auto pbdObj = intPair->getSecondObj();
                if ((pbdObj->getIsInjection()) && (pbdObj->getName() == "Colon"))
                {
                    isColonInjected = true;
                    break;
                }
            }
            if (!isColonInjected)
            {
                //if (isNoCut) // isEND
                {
                    //solvers->solve();  // original is just this one line code
                }
            }
            // XZH END
            //solvers->solve();
        }
        // XZH test

        std::shared_ptr<VESSTetrahedralMesh> m_xzhPhysicalMesh;
        std::shared_ptr<SurfaceMesh> m_xzhVisualMesh;
        //DWORD t8 = GetTickCount();
        //printf("scene before cutting time Phase1.5 %d ms,  \n", t8 - t4);
        // Apply the geometry and apply maps to all the objects
        for (auto obj : m_scene->getSceneObjects())
        {
            // not update for pbd virtualcoupling
            if (auto pbdvcObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(obj))
            {
                //pbdvcObj->resetCollidingGeometry();
                // XZH start
                auto collidingMesh = std::dynamic_pointer_cast<PointSet>(pbdvcObj->getCollidingGeometry());
                root = collidingMesh->getVertexPosition(0);
                tip = collidingMesh->getVertexPosition(1); // 1 not 2
                if (isBegin)
                {
                    cutStartPre = root;
                    cutEndPre = tip;
                    isBegin = false;
                }           
                // XZH END
            }
            else
            {
                //obj->updateGeometries(); // physical drive visual
            }

            // testing get visual
            if (obj->getName() == "Colon")
            {
                auto pbdvcObj = std::dynamic_pointer_cast<PbdObject>(obj);
                m_xzhVisualMesh = std::dynamic_pointer_cast<SurfaceMesh>(pbdvcObj->getVisualGeometry());
            }
        }

        for (auto obj : m_scene->getSceneObjects())
        {
            if (obj->getName() == "VirtualObject")
            {
                if (isTest)
                {
                    auto pbdvcObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(obj);
                    auto visualMesh = std::dynamic_pointer_cast<SurfaceMesh>(pbdvcObj->getVisualGeometry());
                    auto collidingMesh = std::dynamic_pointer_cast<PointSet>(pbdvcObj->getCollidingGeometry());
                    Vec3d pos0 = collidingMesh->getVertexPosition(0);
                    auto& posList = visualMesh->getVertexPositionsChangeable();
                    for (int i = 0; i < posList.size(); i++)
                    {
                        dirGapVisualColliding.push_back(posList[i] - pos0);
                    }
                    isTest = false;
                }
            }        
        }
        //DWORD t6 = GetTickCount();
        //printf("scene before cutting time Phase1 %d ms,  \n", t6 - t4);
        // XZH  update two test sphere  one is tip of tool, one is root of tool
        for (auto obj : m_scene->getSceneObjects())
        {
            // haptic tool
            if (obj->getName() == "VirtualObject")
            {
                continue;
                auto pbdvcObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(obj);
                auto visualMesh = std::dynamic_pointer_cast<SurfaceMesh>(pbdvcObj->getVisualGeometry());
                auto collidingMesh = std::dynamic_pointer_cast<PointSet>(pbdvcObj->getCollidingGeometry());
                Vec3d pos0 = collidingMesh->getVertexPosition(0);
                Vec3d pos1 = collidingMesh->getVertexPosition(1);
                //Vec3d posV0 = visualMesh->getVertexPosition(0);
                //Vec3d dir = pos0 - pos1;
                
                Vec3d root = pos0; //= Vec3d(0, 0, 13.43) + countTool*Vec3d(0, 0, 0.001); // (-4.7, 3.3, 1);
                Vec3d tip = pos1; // = Vec3d(0, 0, -13.43) + countTool*Vec3d(0, 0, 0.001); // (-4.7, 3.3, -6.5);
                  ////< XZH seg 0
                if (cutTimes<=0)
                {
                    root = posCutNodesRoot[7];
                    tip = posCutNodesTip[7];
                }
                else if (cutTimes<=30)
                {
                    root = posCutNodesRoot[0] + (posCutNodesRoot[1] - posCutNodesRoot[0])*(cutTimes / 30);
                    tip = posCutNodesTip[0] + (posCutNodesTip[1] - posCutNodesTip[0])*(cutTimes/30);
                }
                else if (cutTimes<=60) //1
                {
                    int num = cutTimes - 30;
                    root = posCutNodesRoot[1] + (posCutNodesRoot[2] - posCutNodesRoot[1])*(num / 30);
                    tip = posCutNodesTip[1] + (posCutNodesTip[2] - posCutNodesTip[1])*(num / 30);
                }
                else if (cutTimes <= 90) // 2
                {
                    int num = cutTimes - 60;
                    root = posCutNodesRoot[2] + (posCutNodesRoot[3] - posCutNodesRoot[2])*(num / 30);
                    tip = posCutNodesTip[2] + (posCutNodesTip[3] - posCutNodesTip[2])*(num / 30);
                }
                else if (cutTimes <= 120) // 3
                {
                    int num = cutTimes - 90;
                    root = posCutNodesRoot[3] + (posCutNodesRoot[4] - posCutNodesRoot[3])*(num / 30);
                    tip = posCutNodesTip[3] + (posCutNodesTip[4] - posCutNodesTip[3])*(num / 30);
                }
                else if (cutTimes <= 150) // 4
                {
                    int num = cutTimes - 120;
                    root = posCutNodesRoot[4] + (posCutNodesRoot[5] - posCutNodesRoot[4])*(num / 30);
                    tip = posCutNodesTip[4] + (posCutNodesTip[5] - posCutNodesTip[4])*(num / 30);
                }
                else if (cutTimes <= 180) // 5
                {
                    int num = cutTimes - 150;
                    root = posCutNodesRoot[5] + (posCutNodesRoot[6] - posCutNodesRoot[5])*(num / 30);
                    tip = posCutNodesTip[5] + (posCutNodesTip[6] - posCutNodesTip[5])*(num / 30);
                }
                else if (cutTimes <= 210) // 6
                {
                    int num = cutTimes - 180;
                    root = posCutNodesRoot[6] + (posCutNodesRoot[7] - posCutNodesRoot[6])*(num / 30);
                    tip = posCutNodesTip[6] + (posCutNodesTip[7] - posCutNodesTip[6])*(num / 30);
                }
                else if (cutTimes <= 240) // 7
                {
                    int num = cutTimes - 210;
                    root = posCutNodesRoot[7] + (posCutNodesRoot[0] - posCutNodesRoot[7])*(num / 30);
                    tip = posCutNodesTip[7] + (posCutNodesTip[0] - posCutNodesTip[7])*(num / 30);
                }
                else if (cutTimes <= 270) // 7
                {
                    int num = cutTimes - 240;
                    root = posCutNodesRoot[0] + (posCutNodesRoot[1] - posCutNodesRoot[0])*(num / 30);
                    tip = posCutNodesTip[0] + (posCutNodesTip[1] - posCutNodesTip[0])*(num / 30);
                }
                else
                {
                    root = posCutNodesRoot[0];
                    tip = posCutNodesTip[0];
                }
                  ////< XZH seg  END

                //// 24
                //switch (cutTimes)
                //{
                //default:
                //    root = posCutNodesRoot[0];
                //    tip = posCutNodesTip[0];
                //case 0:
                //    root = posCutNodesRoot[0] + (posCutNodesRoot[1] - posCutNodesRoot[0])*0.3333333;
                //    tip = posCutNodesTip[0] + (posCutNodesTip[1] - posCutNodesTip[0])*0.3333333;
                //    break;
                //case 1:
                //    root = posCutNodesRoot[0] + (posCutNodesRoot[1] - posCutNodesRoot[0])*0.666666;
                //    tip= posCutNodesTip[0] + (posCutNodesTip[1] - posCutNodesTip[0])*0.666666;
                //    break;
                //case 2:
                //    root = posCutNodesRoot[1];
                //    tip= posCutNodesTip[1];
                //    break;
                //case 3:
                //    root = posCutNodesRoot[1] + (posCutNodesRoot[2] - posCutNodesRoot[1])*0.3333333;
                //    tip = posCutNodesTip[1] + (posCutNodesTip[2] - posCutNodesTip[1])*0.3333333;
                //    break;
                //case 4:
                //    root = posCutNodesRoot[1] + (posCutNodesRoot[2] - posCutNodesRoot[1])*0.666666;
                //    tip = posCutNodesTip[1] + (posCutNodesTip[2] - posCutNodesTip[1])*0.666666;
                //    break;
                //case 5:
                //    root = posCutNodesRoot[2];
                //    tip = posCutNodesTip[2];
                //    break;
                //case 6:
                //    root = posCutNodesRoot[2] + (posCutNodesRoot[3] - posCutNodesRoot[2])*0.3333333;
                //    tip = posCutNodesTip[2] + (posCutNodesTip[3] - posCutNodesTip[2])*0.3333333;
                //    break;
                //case 7:
                //    root = posCutNodesRoot[2] + (posCutNodesRoot[3] - posCutNodesRoot[2])*0.666666;
                //    tip = posCutNodesTip[2] + (posCutNodesTip[3] - posCutNodesTip[2])*0.666666;
                //    break;
                //case 8:
                //    root = posCutNodesRoot[3];
                //    tip = posCutNodesTip[3];
                //    break;
                //case 9:
                //    root = posCutNodesRoot[3] + (posCutNodesRoot[4] - posCutNodesRoot[3])*0.3333333;
                //    tip= posCutNodesTip[3] + (posCutNodesTip[4] - posCutNodesTip[3])*0.3333333;
                //    break;
                //case 10:
                //    root = posCutNodesRoot[3] + (posCutNodesRoot[4] - posCutNodesRoot[3])*0.666666;
                //    tip = posCutNodesTip[3] + (posCutNodesTip[4] - posCutNodesTip[3])*0.666666;
                //    break;
                //case 11:
                //    root = posCutNodesRoot[4];
                //    tip = posCutNodesTip[4];
                //    break;
                //case 12:
                //    root = posCutNodesRoot[4] + (posCutNodesRoot[5] - posCutNodesRoot[4])*0.3333333;
                //    tip = posCutNodesTip[4] + (posCutNodesTip[5] - posCutNodesTip[4])*0.3333333;
                //    break;
                //case 13:
                //    root= posCutNodesRoot[4] + (posCutNodesRoot[5] - posCutNodesRoot[4])*0.666666;
                //    tip= posCutNodesTip[4] + (posCutNodesTip[5] - posCutNodesTip[4])*0.666666;
                //    break;
                //case 14:
                //    root = posCutNodesRoot[5];
                //    tip = posCutNodesTip[5];
                //    break;
                //case 15:
                //    root= posCutNodesRoot[5] + (posCutNodesRoot[6] - posCutNodesRoot[5])*0.3333333;
                //    tip = posCutNodesTip[5] + (posCutNodesTip[6] - posCutNodesTip[5])*0.3333333;
                //    break;
                //case 16:
                //    root = posCutNodesRoot[5] + (posCutNodesRoot[6] - posCutNodesRoot[5])*0.666666;
                //    tip = posCutNodesTip[5] + (posCutNodesTip[6] - posCutNodesTip[5])*0.666666;
                //    break;
                //case 17:
                //    root = posCutNodesRoot[6];
                //    tip = posCutNodesTip[6];
                //    break;
                //case 18:
                //    root = posCutNodesRoot[6] + (posCutNodesRoot[7] - posCutNodesRoot[6])*0.3333333;
                //    tip = posCutNodesTip[6] + (posCutNodesTip[7] - posCutNodesTip[6])*0.3333333;
                //    break;
                //case 19:
                //    root = posCutNodesRoot[6] + (posCutNodesRoot[7] - posCutNodesRoot[6])*0.666666;
                //    tip = posCutNodesTip[6] + (posCutNodesTip[7] - posCutNodesTip[6])*0.666666;
                //    break;
                //case 20:
                //    root = posCutNodesRoot[7];
                //    tip = posCutNodesTip[7];
                //    break;
                //case 21:
                //    root = posCutNodesRoot[7] + (posCutNodesRoot[0] - posCutNodesRoot[7])*0.3333333;
                //    tip = posCutNodesTip[7] + (posCutNodesTip[0] - posCutNodesTip[7])*0.3333333;
                //    break;
                //case 22:
                //    root = posCutNodesRoot[7] + (posCutNodesRoot[0] - posCutNodesRoot[7])*0.666666;
                //    tip = posCutNodesTip[7] + (posCutNodesTip[0] - posCutNodesTip[7])*0.666666;
                //    break;
                //case 23:
                //    root = posCutNodesRoot[0];
                //    tip = posCutNodesTip[0];
                //    break;
                //}
                countTool++;
                collidingMesh->setVertexPosition(0, root);
                collidingMesh->setVertexPosition(1, tip);
                auto& posList = visualMesh->getVertexPositionsChangeable();
                Vec3d ttmp = Vec3d(0, 0, 0.001);
                for (int i = 0; i < posList.size(); i++)
                {  
                    visualMesh->setVertexPosition(i, root + dirGapVisualColliding[i]);
                }
            }
            // END
            if (obj->getName() == "Sphere1")
            {
                obj->getVisualGeometry()->setTranslation(root);
            }
            else if (obj->getName() == "Sphere2")
            {
                obj->getVisualGeometry()->setTranslation(tip);
            }
            else if (obj->getName() == "XZHSphere0")
            {
                obj->getVisualGeometry()->setTranslation(m_xzhVisualMesh->getVertexPosition(39));
            }
            else if (obj->getName() == "XZHSphere1")
            {
                obj->getVisualGeometry()->setTranslation(m_xzhVisualMesh->getVertexPosition(165));
            }
            else if (obj->getName() == "XZHSphere2")
            {
                obj->getVisualGeometry()->setTranslation(m_xzhVisualMesh->getVertexPosition(110));
            }
            else if (obj->getName() == "XZHSphere3")
            {
                obj->getVisualGeometry()->setTranslation(m_xzhVisualMesh->getVertexPosition(106));
            }
            else if (obj->getName() == "XZHSphere4")
            {
                obj->getVisualGeometry()->setTranslation(m_xzhVisualMesh->getVertexPosition(102));
            }
            else if (obj->getName() == "XZHSphere5")
            {
                obj->getVisualGeometry()->setTranslation(m_xzhVisualMesh->getVertexPosition(163));
            }
            else if (obj->getName() == "XZHSphere6")
            {
                obj->getVisualGeometry()->setTranslation(m_xzhVisualMesh->getVertexPosition(220));
            }
            else if (obj->getName() == "XZHSphere7")
            {
                obj->getVisualGeometry()->setTranslation(m_xzhVisualMesh->getVertexPosition(9));
            }
            else if (obj->getName() == "XZHSphere8")
            {
                obj->getVisualGeometry()->setTranslation(m_xzhVisualMesh->getVertexPosition(3));
            }
            else if (obj->getName() == "DecalObject0")
            {
                if (auto decalPool = std::dynamic_pointer_cast<DecalPool>(obj->getVisualGeometry()))
                {
                    auto& decallist = decalPool->getDecals();   //->getVisualGeometry()->setTranslation(m_xzhVisualMesh->getVertexPosition(39));
                    decallist.at(0)->setPosition(m_xzhVisualMesh->getVertexPosition(39));
                    //decal1->setPosition(markingPos);
                    ////    decal1->rotate(RIGHT_VECTOR, PI_4);
                    ////    decal1->scale(0.6);
                }
                //auto decalPoolObj = std::dynamic_pointer_cast<VisualObject>(obj); //   <DecalPool>(obj);
                //auto decalPool = decalPoolObj->getVisualGeometry(); // std::shared_ptr<DecalPool>
                //auto& decallist=decalPool->getDecals();   //->getVisualGeometry()->setTranslation(m_xzhVisualMesh->getVertexPosition(39));
                //decallist.at(0)->setTranslation(m_xzhVisualMesh->getVertexPosition(39));
            }
            else if (obj->getName() == "DecalObject1")
            {
                if (auto decalPool = std::dynamic_pointer_cast<DecalPool>(obj->getVisualGeometry()))
                {
                    auto& decallist = decalPool->getDecals();
                    decallist.at(0)->setPosition(m_xzhVisualMesh->getVertexPosition(165));
                }
            }
            else if (obj->getName() == "DecalObject2")
            {
                if (auto decalPool = std::dynamic_pointer_cast<DecalPool>(obj->getVisualGeometry()))
                {
                    auto& decallist = decalPool->getDecals();
                    decallist.at(0)->setPosition(m_xzhVisualMesh->getVertexPosition(110));
                }
            }
            else if (obj->getName() == "DecalObject3")
            {
                if (auto decalPool = std::dynamic_pointer_cast<DecalPool>(obj->getVisualGeometry()))
                {
                    auto& decallist = decalPool->getDecals();
                    decallist.at(0)->setPosition(m_xzhVisualMesh->getVertexPosition(106));
                }
            }
            else if (obj->getName() == "DecalObject4")
            {
                if (auto decalPool = std::dynamic_pointer_cast<DecalPool>(obj->getVisualGeometry()))
                {
                    auto& decallist = decalPool->getDecals();
                    decallist.at(0)->setPosition(m_xzhVisualMesh->getVertexPosition(102));
                }
            }
            else if (obj->getName() == "DecalObject5")
            {
                if (auto decalPool = std::dynamic_pointer_cast<DecalPool>(obj->getVisualGeometry()))
                {
                    auto& decallist = decalPool->getDecals();
                    decallist.at(0)->setPosition(m_xzhVisualMesh->getVertexPosition(163));
                }
            }
            else if (obj->getName() == "DecalObject6")
            {
                if (auto decalPool = std::dynamic_pointer_cast<DecalPool>(obj->getVisualGeometry()))
                {
                    auto& decallist = decalPool->getDecals();
                    decallist.at(0)->setPosition(m_xzhVisualMesh->getVertexPosition(220));
                }
            }
            else if (obj->getName() == "DecalObject7")
            {
                if (auto decalPool = std::dynamic_pointer_cast<DecalPool>(obj->getVisualGeometry()))
                {
                    auto& decallist = decalPool->getDecals();
                    decallist.at(0)->setPosition(m_xzhVisualMesh->getVertexPosition(9));
                }
            }
            else if (obj->getName() == "DecalObject8")
            {
                if (auto decalPool = std::dynamic_pointer_cast<DecalPool>(obj->getVisualGeometry()))
                {
                    auto& decallist = decalPool->getDecals();
                    decallist.at(0)->setPosition(m_xzhVisualMesh->getVertexPosition(3));
                }
            }
            else if (obj->getName() == "visualCutFace")
            {
                auto mesh = obj->getVisualGeometry();
            }
            else if (obj->getName()=="cuttingSurfaceMove")
            {
                auto visualMesh = std::dynamic_pointer_cast<SurfaceMesh>(obj->getVisualGeometry());
                //auto& posLists = visualMesh->getVertexPositionsChangeable();
                Vec3d vertexPos3;
                int num = visualMesh->getNumVertices();
                for (int i = 0; i < num; i++)
                {
                    vertexPos3 = visualMesh->getVertexPosition(i);
                    visualMesh->setVertexPosition(i, vertexPos3 + Vec3d(0, 0, -0.005)); // colon if is cube use  // visualMesh->setVertexPosition(i, vertexPos3 + Vec3d(-0.002, 0, 0));
                }
                //for (int i = 0; i < posLists.size(); i++)
                //{
                //    posLists[i] += Vec3d(-0.0002, 0, 0);
                //}
            }
            else if (obj->getName() == "visualColonMeshVolume")
            {
                for (auto obj2 : m_scene->getSceneObjects())
                {
                    break;
                    if (!hasCut) break;
                    //if (obj2->getName() == "Cube")  // Colon
                    if (obj2->getName() == "Colon")
                    {
                        auto pbdvcObj = std::dynamic_pointer_cast<PbdObject>(obj2);
                        m_xzhPhysicalMesh = std::dynamic_pointer_cast<VESSTetrahedralMesh>(pbdvcObj->getPhysicsGeometry());

                        //m_xzhPhysicalMesh->extractSurfaceMeshVolume(); //  getSurfaceMeshVolume();
                        m_xzhPhysicalMesh->extractSurfaceMesh();  // this influence the speed siganificantly
                        auto vMesh = m_xzhPhysicalMesh->getSurfaceMesh();  //->getSurfaceMeshVolume();
                        auto visualMesh = std::dynamic_pointer_cast<PointSet>(obj->getVisualGeometry());
                        obj->setVisualGeometry(visualMesh);

                        Vec3d vertexPos3;
                        for (int i = 0; i < visualMesh->getNumVertices(); ++i)
                        {
                            vertexPos3 = vMesh->getVertexPosition(i);
                            visualMesh->setVertexPosition(i, vertexPos3);
                        }
                        hasCut = false;
                    }
                }
            }
            else if (obj->getName() == "visualColonMesh4")  // wireframe mesh
            {
                continue;
                for (auto obj2 : m_scene->getSceneObjects())
                {
                    if (obj2->getName() == "Colon")
                    {
                        auto pbdvcObj = std::dynamic_pointer_cast<PbdObject>(obj2);
                        m_xzhPhysicalMesh = std::dynamic_pointer_cast<VESSTetrahedralMesh>(pbdvcObj->getPhysicsGeometry());
                    }
                }
                // XZH // for both sides surface of volume mesh
                //m_xzhPhysicalMesh->extractSurfaceMesh();
                //auto vMesh = m_xzhPhysicalMesh->getSurfaceMesh();
                ////obj->setVisualGeometry(m_xzhPhysicalMesh->getSurfaceMesh());

                // for only inside surface of volume mesh
                std::vector<Vec3d> centralLine;
                centralLine.push_back(Vec3d(-19.4, -5.85, 1.15));
                centralLine.push_back(Vec3d(-3.3, 2.4, 1.05));
                break;
                m_xzhPhysicalMesh->extractSurfaceMesh(centralLine); // <<<!!!!Most important !!!!>>> only inside colon of surface of volume mesh
                auto vMesh = m_xzhPhysicalMesh->getSurfaceMesh();  // <<<!!!!Most important !!!!>>> from test found that this part is the most time-consuming process XZH
                // XZH END 
                // TEST visual geometry
                Vec3d vertexPos3;
                auto visualMesh = std::dynamic_pointer_cast<PointSet>(obj->getVisualGeometry());
                for (int i = 0; i < visualMesh->getNumVertices(); ++i)
                {
                    vertexPos3 = vMesh->getVertexPosition(i);
                    visualMesh->setVertexPosition(i, vertexPos3);
                }
                // TEST END
            }
            else if (obj->getName() == "visualColonMesh5")  // inside colon mesh
            {
            }
            else if (obj->getName() == "visualNormal0")
            {
                // TEST visual geometry
                Vec3d vertexPos3 = Vec3d::Zero();
                auto visualMesh = std::dynamic_pointer_cast<PointSet>(obj->getVisualGeometry());
                visualMesh->setVertexPosition(0, tip);
                for (auto intPair : m_scene->getCollisionGraph()->getPbdPairList())
                {
                    vertexPos3 = intPair->getMoveDir();   //getCentralPos(); // get central of colliding vertices  //  intPair->getForce();  // force normal 
                }
                visualMesh->setVertexPosition(1, tip + vertexPos3 * 100); // 2 times of normal
                visualMesh->setVertexPosition(2, tip);
                // TEST END
            }
            else if (obj->getName() == "visualLineMesh")
            {
                auto visualMesh = std::dynamic_pointer_cast<PointSet>(obj->getVisualGeometry());
                visualMesh->setVertexPosition(0, tip);
                visualMesh->setVertexPosition(1, root);
                visualMesh->setVertexPosition(2, tip);
            }
        }
        // XZH END

        // Do collision detection and response for pbd objects
        for (auto intPair : m_scene->getCollisionGraph()->getPbdPairList())
        {
            break; // jump to cutting part
            intPair->resetConstraints();
            if (intPair->doBroadPhaseCollision())
            {
                intPair->chooseProcedure(); // Injection solution || choose via Omni Button 0 intPair->doNarrowPhaseContinuousCollisionVESS(); // for static CD intPair->doNarrowPhaseCollisionVESS();  // for other intPair->doNarrowPhaseCollision();  // for VESS intPair->doNarrowPhaseCollisionVESS(); // intPair->doNarrowPhaseCollision();

                //// marking START
                //intPair->doMarking();
                //auto& markingState = intPair->getMarkState();
                //if (markingState)
                //{
                //    Vec3d markingPos = intPair->getMarkingPos();
                //    printf("marking: %f %f %f\n", markingPos.x(), markingPos.y(), markingPos.z());
                //    //std::shared_ptr<DecalPool> decalPool;
                //    //for (auto obj : m_scene->getSceneObjects())
                //    //{
                //    //    if (obj->getName() == "DecalObject")
                //    //    {
                //    //        decalPool = std::dynamic_pointer_cast<DecalPool>(obj->getVisualGeometry());
                //    //        auto decal1 = decalPool->addDecal();
                //    //        decal1->setPosition(markingPos);
                //    //        decal1->rotate(FORWARD_VECTOR, PI_4);

                //    //        numMarking++;
                //    //        char name[256];
                //    //        sprintf(name, "XZHSphere%d", numMarking);
                //    //            
                //    //        

                //    //    }
                //    //}
                //    numMarking++;
                //    char name[256];
                //    sprintf(name, "XZHSphere%d", numMarking);
                //    auto visualSphere22 = std::make_shared<imstk::Sphere>();
                //    visualSphere22->setTranslation(markingPos);
                //    visualSphere22->setRadius(0.3);
                //    auto sphere2Obj = std::make_shared<SceneObject>(name);
                //    sphere2Obj->setVisualGeometry(visualSphere22);
                //    m_scene->addSceneObject(sphere2Obj);

                //    auto decalTest = std::make_shared<DecalPool>();
                //    auto decalMaterial = std::make_shared<RenderMaterial>();
                //    //auto diffuseTexture = std::make_shared<Texture>("F:/assets/miscBloodDecalParticles/miscBloodDecalParticles/blood_particle_01.png", Texture::DIFFUSE);
                //    auto diffuseTexture2 = std::make_shared<Texture>("I:/iMSTK/resources/textures/test4.png", Texture::DIFFUSE);
                //    decalMaterial->addTexture(diffuseTexture2);
                //    auto decal1 = decalTest->addDecal();
                //    decal1->setPosition(markingPos);
                //    decal1->rotate(RIGHT_VECTOR, PI_4);
                //    decal1->scale(0.6);
                //    decalTest->setRenderMaterial(decalMaterial);

                //    sprintf(name, "DecalObject%d", numMarking);
                //    auto decalObject = std::make_shared<VisualObject>(name); // "DecalObject"
                //    decalObject->setVisualGeometry(decalTest);
                //    m_scene->addSceneObject(decalObject);

                //    markingState = false;
                //    // marking END
                //}        
            }
            else  // no candidate collision
            {
                intPair->updateToolPos();
                //FILE *outForceFilter = fopen("G:/outInformation.dat", "a");
                //fprintf(outForceFilter, "updateToolPos\n");
                //fclose(outForceFilter);
            }
            // injection and CCD
            if (intPair->getInjectionState())
            {
                intPair->computeContactForceInjection();
            }
            else
            {
                intPair->computeContactForceContinuous();
            }
            //intPair->resolveContinuousCollisionVESS(); // for static CD intPair->resolveCollisionVESS();  // for other intPair->resolveCollision();  // for VESS intPair->resolveCollisionVESS();   // intPair->resolveCollision();
            centralPosition = intPair->getCentralPos(); // extra test
        }

        // Update velocity of PBD object  xzh
        // Update velocity of PBD objects
        for (auto obj : m_scene->getSceneObjects())
        {
            if (auto pbdObj = std::dynamic_pointer_cast<PbdObject>(obj))
            {
                if (auto pbdvcObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(obj))
                {
                    //pbdvcObj->resetCollidingGeometry();
                }
                else
                {
                    pbdObj->updateVelocity();
                }
            }
        }

        // Apply forces on device  change the place in this place XZH
        for (auto controller : m_scene->getSceneObjectControllers())
        {
            controller->applyForces();
        }

        // Set the trackers of virtual coupling PBD objects to out-of-date
        for (auto obj : m_scene->getSceneObjects())
        {
            if (auto virtualCouplingPBD = std::dynamic_pointer_cast<VirtualCouplingPBDObject>(obj))
            {
                virtualCouplingPBD->setTrackerToOutOfDate();
            }
            // XZH TEST
            if (obj->getName() == "Sphere3")
            {
                obj->getVisualGeometry()->setTranslation(centralPosition);
                //printf("sphere3: %f %f %f %f\n", centralPosition.x(), centralPosition.y(), centralPosition.z(), centralPosition.norm());
            }
            // XZH TEST
        }

        // Set the trackers of the scene object controllers to out-of-date
        for (auto controller : m_scene->getSceneObjectControllers())
        {
            controller->setTrackerToOutOfDate();
        }
        //DWORD t5 = GetTickCount();
        //printf("scene before cutting time %d ms,  \n", t5 - t4);
          ////< XZH Cutting start
        std::thread threadCut(&SceneManager::runCutting, this);
        threadCut.join(); //.detach(); // 
          ////< XZH Cutting END

        t2 = GetTickCount();
        //printf("scene body  time %d ms,  frame rate is: %f\n", t2 - t4, 1000 / double(t2 - t4));
        auto timeElapsed = wwt.getTimeElapsed(StopWatch::TimeUnitType::seconds);

        // Update time step size of the dynamic objects
        for (auto obj : m_scene->getSceneObjects())
        {
            if (obj->getType() == SceneObject::Type::Pbd)
            {
                if (auto dynaObj = std::dynamic_pointer_cast<PbdObject>(obj))
                {
                    if (dynaObj->getDynamicalModel()->getTimeStepSizeType() == TimeSteppingType::realTime)
                    {
                        dynaObj->getDynamicalModel()->setTimeStep(timeElapsed);
                    }
                }
            }
            else if (obj->getType() == SceneObject::Type::FEMDeformable)
            {
                if (auto dynaObj = std::dynamic_pointer_cast<DeformableObject>(obj))
                {
                    if (dynaObj->getDynamicalModel()->getTimeStepSizeType() == TimeSteppingType::realTime)
                    {
                        dynaObj->getDynamicalModel()->setTimeStep(timeElapsed);
                    }
                }
            }
        }


    }

    void
        SceneManager::cleanUpModule()
    {
        // End Camera Controller
        if (auto camController = m_scene->getCamera()->getController())
        {
            camController->end();
            m_threadMap.at(camController->getName()).join();
        }
    }
    void
        SceneManager::startModuleInNewThread(std::shared_ptr<Module> module)
    {
        m_threadMap[module->getName()] = std::thread([module] { module->start(); });
    }
} // imstk
