 /*=========================================================================

   Library: iMSTK

   Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
   & Imaging in Medicine, Rensselaer Polytechnic Institute.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0.txt

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

=========================================================================*/

#ifndef imstkSceneManager_h
#define imstkSceneManager_h

#include <unordered_map>
#include <memory>
#include <thread>

#include "imstkModule.h"
#include "imstkScene.h"
// XZH
#include "imstkVESSTetrahedralMesh.h"
//#include "imstkCuttingVESSCutMesh.h"
#include "imstkTetraTriangleMap.h"
#include "imstkAnimationObject.h"
#include "imstkRenderParticles.h"
namespace imstk
{
///
/// \class SceneManager
///
/// \brief
///
class SceneManager : public Module
{
public:
    ///
    /// \brief Constructor
    ///
    SceneManager(std::shared_ptr<Scene> scene) :
        Module(scene->getName()),
        m_scene(scene){}

    ///
    /// \brief Destructor
    ///
    ~SceneManager() = default;

    ///
    /// \brief Get the scene that the scene manager is managing
    ///
    std::shared_ptr<Scene> getScene();

protected:
    ///
    /// \brief Initialize the module
    ///
    void initModule() override;

    ///
    /// \brief Run the module
    ///
    void runModule() override;

    ///
    /// \brief Clean up the module
    ///
    void cleanUpModule() override;

    ///
    /// \brief
    ///
    void startModuleInNewThread(std::shared_ptr<Module> module);

    ///
    /// \brief
    ///
    void runGraph();

    ///
    /// \brief
    ///
    void runMarking();
    void runCutting();
    void runDissection();
	void runDissectionLoad();
	bool cutStrLineBySeparator(string strLine, char separator, vector<string>&strBufferLineVec);

    std::shared_ptr<Scene> m_scene; ///> Scene that is being managed
    std::unordered_map<std::string, std::thread> m_threadMap;   ///>

    // cutting
    Vec3d cutStartPre, cutEndPre, cutStart, cutEnd; // construct the cutting plane
    Vec3d cutStartInit, cutEndInit; // the first cut
    Vec3d cutStartPos = Vec3d::Zero();   ////< XZH the cut pos in the visual mesh
    Vec3d cutCurPos = Vec3d::Zero();
    Vec3d cutCurPos0 = Vec3d::Zero();
    int cutStartIndex = -1;  ////< XZH cut pos index visual mesh
    bool isStart = false, isEnd = false; // cutting flag  if true, call pbdsolver
    bool isNoCut = false; // open PBD solver
    bool isDissecting = false;   ////< XZH whether has dissect now or not
    bool isBegin = true;
    bool isFirst = true;
    bool isFirstCut = true;  ////< XZH start to store the init cutting pos
    bool isStartGraph = false;  ////< XZH start to determine whether there is a cycle in the cutting path
    const float distStartGraph = 1.5;  ////< XZH  the dist between this cut and init cut
    const float distCircleGraph = 0.5;  ////< XZH if less than this, connect this cut to the init cut , form a circle
    bool isProcChanged = false;  ////< XZH if procedure type changed between main and camera, make it to true
    int mProcedure = 0;

    // marking
    int numMarking = 0;

    // Navigation
    int navStep = 0;
    int numGap = 0;
    vector<Vec3d> vecCamera;
    vector<Vec3d> vecCameraFocal;
    int endCount = 0;

    // cutting and dissection
    int cutTimes = 0;
    std::vector<std::vector<int>> graphVisualCutting;
    std::shared_ptr<UndirectedGraph> m_graphVisualCutting; ///> Scene that is being managed
    int indexTumorVertex = 1055; // 150; // 1055; // // 1055 for colon mesh, 150 for cube mesh 150; // center of tumor vertex index visual mesh
    std::vector<int> totalCutTriangles; // the global triangle number std::list<int> totalCutTriangles; // record the first cutting to the end influenced triangles
    std::unordered_multimap< int, int > mapOrigin2New;
    bool hasCut = false;
    int mCutState = 0; // 0: prepare for circumfirential cutting, 1: cutting is on  2: circumfirential cutting completed  3: dissection is on 4: dissection is completed
    //std::vector<Vec3d> baseposCutPlane;
    //std::vector<Vec3d> normCutPlane;

    std::unordered_map<int, int> mapCutVertices;
    int graphSize = 0;
    
    // display for cutting path
    std::vector<SurfaceMesh::TriangleArray> triListsCutPath;
    std::vector<int> triCutPathTriIdx;
    std::vector<int> vecCutPathVertices;
    int countTool = 0;
    std::vector<Vec3d> posCutNodesRoot;
    std::vector<Vec3d> posCutNodesTip;
    std::vector<Vec3d> dirGapVisualColliding;

      ////< XZH w.r.t. dissecting path
    std::vector<Vec3d> posDissectNodesRoot;
    std::vector<Vec3d> posDissectNodesTip;
    std::vector<Vec3d> dirGapVisualDissecting0;
    std::vector<Vec3d> dirGapVisualDissecting1;
    std::vector<Vec3d> dirGapVisualDissecting2;
    int dissectTimes = 0;
    bool isTestDissect0 = true;
    bool isTestDissect1 = true;
    bool isTestDissect2 = true;

    bool isTest = true;

      ////< XZH pressing times
    int mCntPressButton = 0;

      ////< XZH test dissection
    std::vector<Vec3d> normCutPlane;  ////< XZH the norm of each cut
    std::vector<Vec3d> posCutPlane;  ////< XZH the pos of each cut

      ////< XZH dissection triangles
    std::vector<SurfaceMesh::TriangleArray> triListsLastXZH;
    std::vector<SurfaceMesh::TriangleArray> triListsLastMoveXZH;
    bool isFirstDissection = true;

      ////< XZH Smoke and Sparks
    int smokeCount = 0;
    int sparkCount = 0;

	  ////< XZH loading cutting
	bool hasLoadedDissect = false;
	bool isBtnPressing = false;
	bool isBtnPressingPre = false;

      ////< XZH test cylinder
    bool isTestRot = true;

	  ////< XZH injecting change way update on 11/1/2019
	StdVectorOfVec3d visualMeshInjectPosesDiff;
};
} // imstk

#endif // ifndef imstkSceneManager_h
