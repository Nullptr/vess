﻿/*=========================================================================

Library: iMSTK

Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
& Imaging in Medicine, Rensselaer Polytechnic Institute.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0.txt

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

=========================================================================*/

// imstk
#include "imstkSceneManager.h"
#include "imstkCameraController.h"
#include "imstkSceneObjectController.h"
#include "imstkDynamicObject.h"
#include "imstkPbdObject.h"
#include "imstkDeformableObject.h"
#include "imstkVirtualCouplingPBDObject.h"
#include "imstkPBDVirtualCouplingObject.h" // xzh
#include "imstkPbdSceneObjectController.h" // xzh
#include "imstkDecalPool.h" // XZH
#include "imstkSphere.h" // XZH
#include "imstkPlane.h"
#include "imstkCylinder.h"
#include "imstkLineMesh.h"

#include "imstkGeometryMap.h"
#include "imstkTimer.h"
#include "imstkPbdSolver.h"

#include "g3log/g3log.hpp"

#include <windows.h> // for time test XZH
#include<thread>   ////< XZH for parallel
DWORD t3, t4, t1, t2;
clock_t startMain, finishMain;
namespace imstk
{
      ////< XZH 
    void SceneManager::runMarking()
    {
        std::shared_ptr<VESSTetCutMesh> cutTissueMesh;
        std::shared_ptr<SurfaceMesh> visualColonMesh;
        std::shared_ptr<PbdVirtualCouplingObject> pbdObj;
        std::shared_ptr<PbdObject> pbdvcObj;
    }

      ////< XZH thread
    void SceneManager::runDissection()
    {
        // XZH  test cutting
        std::shared_ptr<VESSTetCutMesh> cutTissueMesh;
        std::shared_ptr<SurfaceMesh> visualColonMesh;
        std::shared_ptr<PbdVirtualCouplingObject> pbdObj;
        std::shared_ptr<PbdObject> pbdvcObj;
        //using TetTriMap  for cube dense
        for (auto obj : m_scene->getSceneObjects())
        {
            //continue;
            if (obj->getName() == "VirtualObject")
            {
                pbdObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(obj);
            }
            if (obj->getName() == "Colon")
            {
                pbdvcObj = std::dynamic_pointer_cast<PbdObject>(obj);
                cutTissueMesh = std::dynamic_pointer_cast<VESSTetCutMesh>(pbdvcObj->getPhysicsGeometry());
                visualColonMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(pbdvcObj->getVisualGeometry());
            }
        }
        if (!pbdObj) return;
        if (!pbdvcObj) return;

        auto collidingMesh = std::dynamic_pointer_cast<PointSet>(pbdObj->getCollidingGeometry());
        auto& tool = pbdObj->getControllerTool();
		isBtnPressingPre = isBtnPressing;
		int m_nCnt = 0;
        if (tool.button[0] && (!isDissecting))
        {
			isBtnPressing = true;
            //printf("start...");
            if (mCntPressButton < 1) 
            {
                cutStart = collidingMesh->getVertexPosition(0);
                cutEnd = collidingMesh->getVertexPosition(1); // 3
                cutStart = cutEnd + (cutStart - cutEnd)*0.3;
                cutEnd = cutEnd + (cutEnd - cutStart)*0.5;
                cutStartPre = cutStart;
                cutEndPre = cutEnd;
                isStartGraph = false;
                isFirstCut = true;
            }
            if (mCntPressButton <= 40)
            {
                cutStartInit = cutStart;
                cutEndInit = cutEnd;
            }
            mCntPressButton++;
			m_nCnt++;
        }
		else
		{
			mCntPressButton = 0;
			isBtnPressing = false;
		}
		//if (tool.button[1] && (!isDissecting))
		if (isBtnPressingPre&& (!isBtnPressing) && (!isDissecting))
        //if (tool.button[0] && (!isNoCut) && (mCntPressButton > 27) && (mCntPressButton % 6 == 0))
        {
            isDissecting = true;
            cutStartPre = cutStart;
            cutEndPre = cutEnd;
            cutStart = collidingMesh->getVertexPosition(0);
            cutEnd = collidingMesh->getVertexPosition(1); // 3 tip
            cutCurPos = cutEnd;
            cutCurPos0 = cutEnd + (cutStart - cutEnd)*0.05;

            // start cutting
            vector<Vec3d> m_vBladeSegments;
            vector<Vec3d> m_vSweptQuad;
            Vec3d temp1, temp2;
            Vec3d temp3, temp4;
            temp1 = cutStartPre;
            temp2 = cutStart;
            temp3 = cutEndPre;
            temp4 = cutEnd;

            double toolLen = 0.01; // 7.7510013580322266;
			temp1 = temp3 + (temp1 - temp3).normalized()* 0.004;
			temp2 = temp4 + (temp2 - temp4).normalized()* 0.004;
            temp3 = temp1 + (temp3 - temp1).normalized()*toolLen*0.34;
            temp4 = temp2 + (temp4 - temp2).normalized()*toolLen*0.34;

            //  ////< XZH based on the specific path
            
              ////< XZH Cube END

            dissectTimes++;
            double rato = 1.0; // 0.01;
            temp1 *= rato;
            temp2 *= rato;
            temp3 *= rato;
            temp4 *= rato;
            ////< XZH END specific path

            m_vBladeSegments.push_back(temp1);
            m_vBladeSegments.push_back(temp2);
            
            DWORD t1, t2;
            t1 = GetTickCount();

            m_vSweptQuad.push_back(temp1);
            m_vSweptQuad.push_back(temp2);
            m_vSweptQuad.push_back(temp3);
            m_vSweptQuad.push_back(temp4);


            // sweep surface
            bool isExistSweep = false;
            std::shared_ptr<SurfaceMesh> surfMeshSwept;
            for (auto objxzh : m_scene->getSceneObjects())
            {
                if (objxzh->getName() == "SweepSurface")
                {
                    surfMeshSwept = std::dynamic_pointer_cast<SurfaceMesh>(objxzh->getVisualGeometry());
                    break;
                }
            }
            if (surfMeshSwept)
            {
                isExistSweep = true;
                imstk::StdVectorOfVec3d vertListSwept;
                std::vector<imstk::SurfaceMesh::TriangleArray> trianglesSwept;

                trianglesSwept.push_back({ { 0, 1, 2 } });
                trianglesSwept.push_back({ { 2, 1, 3 } });
                auto& posVisualListXZH = surfMeshSwept->getVertexPositionsChangeable();
                auto& trisVisualListXZH = surfMeshSwept->getTrianglesVerticesChangeable();
                posVisualListXZH.push_back(m_vSweptQuad[0]);
                posVisualListXZH.push_back(m_vSweptQuad[1]);
                posVisualListXZH.push_back(m_vSweptQuad[2]);
                posVisualListXZH.push_back(m_vSweptQuad[3]); // 100
				size_t cntV = posVisualListXZH.size();
                trisVisualListXZH.push_back({ { cntV - 4, cntV - 3, cntV - 2 } });
                trisVisualListXZH.push_back({ { cntV - 2, cntV - 3, cntV - 1 } });
                surfMeshSwept->setVertexPositions(posVisualListXZH); // posVisualList); // vertPositions
                surfMeshSwept->setTrianglesVertices(trisVisualListXZH);
                surfMeshSwept->setTopologyChangedFlag(true);
            }
            else
            {
                isExistSweep = false;
                imstk::StdVectorOfVec3d vertListSwept;
                std::vector<imstk::SurfaceMesh::TriangleArray> trianglesSwept;
                vertListSwept.push_back(m_vSweptQuad[0]);
                vertListSwept.push_back(m_vSweptQuad[1]);
                vertListSwept.push_back(m_vSweptQuad[2]);
                vertListSwept.push_back(m_vSweptQuad[3]);
                trianglesSwept.push_back({ { 0, 1, 2 } });
                trianglesSwept.push_back({ { 2, 1, 3 } });
                for (int k = 0; k < 50; k++)
                {
                    vertListSwept.push_back(m_vSweptQuad[0]);
                    vertListSwept.push_back(m_vSweptQuad[1]);
                    vertListSwept.push_back(m_vSweptQuad[2]);
                    vertListSwept.push_back(m_vSweptQuad[3]); // 100
                    size_t cntV = vertListSwept.size();
                    trianglesSwept.push_back({ { cntV - 4, cntV - 3, cntV - 2 } });
                    trianglesSwept.push_back({ { cntV - 2, cntV - 3, cntV - 1 } });
                }
                surfMeshSwept = std::make_shared<imstk::SurfaceMesh>();
                surfMeshSwept->initialize(vertListSwept, trianglesSwept);
                auto surfMeshSweptModel = std::make_shared<VisualModel>(surfMeshSwept);
                // visualization
                auto object0 = std::make_shared<imstk::VisualObject>("SweepSurface"); // "visualColonMesh0");
                auto material0 = std::make_shared<RenderMaterial>();
                material0->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME SURFACE
                auto tmp0 = std::make_shared<Texture>("I:/iMSTK/resources/VESS/testtest.png", Texture::Type::DIFFUSE);
                //material0->addTexture(tmp0);
                //material0->setTessellated(true);
                material0->setColor(imstk::Color::Green);
                material0->setBackFaceCulling(false);
                surfMeshSweptModel->setRenderMaterial(material0);
                object0->addVisualModel(surfMeshSweptModel); // change to any mesh created above
                //m_scene->addSceneObject(object0);
            }

            int res = cutTissueMesh->cutNewXZH3(m_vBladeSegments, m_vSweptQuad, true);   ////< XZH  0: no cut, should return, 1: has cut ,should 
			if (res < 1)
			{
				isDissecting = false;
				return;
			}

            // after cutting ,update graph
            auto& tetVertices = cutTissueMesh->getInitTetrahedraVertices();
            auto& posTetlist = cutTissueMesh->getVertexPositions();
			StdVectorOfVec3d posTetTest;
			StdVectorOfVec3d posTetlist2 = posTetlist;
			for (int n=0;n<posTetlist2.size();n++)
			{
				auto ppos = posTetlist2[n];
				ppos=ppos*1000;
				posTetTest.push_back(ppos);
			}
            // after cutting
            auto& tetList = cutTissueMesh->getCutElements();  // just current 
            auto& listGraphVertices = cutTissueMesh->getGraphVertices();
            auto& listGraphTets = cutTissueMesh->getGraphTetrahedrons();
            auto& currentTetStates = cutTissueMesh->getTetrahedronStates();
            auto& graphVolume = cutTissueMesh->getGraphVolume();

            //fixed point
            //Vec3d posTumorCenter = Vec3d(3.0, 4.41, 5.38);   ////< XZH for CUBE
            Vec3d posTumorCenter = Vec3d(3.5, 8.9, 19.0)*0.01;   ////< XZH for Colon
            double distTumor = 2.0*0.01;
            std::string fixed_corner;
            char intStr[33];
            Vec3d tmpPos;
            std::vector<int> fixedVertice; // add later 06/04/2017
            auto& fixedStates = cutTissueMesh->getFixedVerticeStates();
            auto& mapTetrahedrons = cutTissueMesh->getTetrahedronMap();
            auto& tetListCutAll = cutTissueMesh->getCutElementsTotal();  // Total 
            auto& tetListCutCur = cutTissueMesh->getCutElements();  // just current 
            sort(tetListCutCur.begin(), tetListCutCur.end());
            tetListCutCur.erase(unique(tetListCutCur.begin(), tetListCutCur.end()), tetListCutCur.end());            

            auto dynaModel1 = std::static_pointer_cast<PbdModel>(pbdvcObj->getDynamicalModel());
            dynaModel1->initializeReset();

              ////< XZH visual mesh
            auto& posVisualList = visualColonMesh->getVertexPositionsChangeable(); // changable 
            //posVisualList = posTetlist;
            auto& triListsXZH = visualColonMesh->getTrianglesVerticesXZH();  // XZH use for change
            auto& triListsTextureOrder = cutTissueMesh->getTriTextureOrder();  // XZH use for texture
            auto& triCutTotal = cutTissueMesh->getCutVisualTotal();
            auto& triNewTotal = cutTissueMesh->getCutNewTriangles(); // cutTissueMesh->getCutNewTotal();
            auto& triIndexInTet = cutTissueMesh->getVisualTriIndexInTet();   ////< XZH original tri index, but after topology changed, the index need to be changed?? this is a bug 836/840
            auto& vecCurCutEdges = cutTissueMesh->getCutEdges();
            auto& vecEdgesAll = cutTissueMesh->getEdgesAll();
			int nSurfTris = cutTissueMesh->getPhysSurfTrisNumber();
            std::vector<SurfaceMesh::TriangleArray> triListsNew;
            for (int k = 0; k < triListsXZH.size(); k++)   ////< XZH original tris surface
            {
                bool isCut = false;
                int nk = triIndexInTet[k];   ////< XZH each tri has index in the whole tris with inside
                for (int n = 0; n < triCutTotal.size();n++)   ////< XZH 
                {
                    if (nk == triCutTotal[n])
                    {
                        isCut = true;  
                        break;
                    }
                }
                if (!isCut)
                {
                    //triListsNew.push_back(triListsXZH[k]);  ////< XZH 
                    triListsNew.push_back(triListsTextureOrder[k]);
                }
            }
            visualColonMesh->setVertexPositions(posVisualList);
            visualColonMesh->setTrianglesVertices(triListsNew);
            visualColonMesh->setTopologyChangedFlag(true);

              ////< XZH new surface mesh 04/23/2019
            auto tetTrianglesAll  = cutTissueMesh->getTetTrianglesAll();
            std::vector<SurfaceMesh::TriangleArray> triListUpdate = triNewTotal; // = triListsNew;

			startMain = clock();
            // sweep surface
            bool isExistNewSurf = false;
            std::shared_ptr<SurfaceMesh> surfMeshXZH;
            for (auto objxzh : m_scene->getSceneObjects())
            {
                if (objxzh->getName() == "SurfaceXZH")
                {
                    surfMeshXZH = std::dynamic_pointer_cast<SurfaceMesh>(objxzh->getVisualGeometry());
                    break;
                }
            }
            if (surfMeshXZH)
            {
                isExistNewSurf = true;
                auto& posVisualListXZH = surfMeshXZH->getVertexPositionsChangeable();
                auto& trisVisualListXZH = surfMeshXZH->getTrianglesVerticesChangeable();
                std::vector<SurfaceMesh::TriangleArray>::iterator it; // = trisVisualListXZH.begin();

                  ////< XZH Method2
                std::vector<SurfaceMesh::TriangleArray> vecList; // = trisVisualListXZH;
                for (int i = 0; i < trisVisualListXZH.size(); i++)
                {
                    auto nd0 = trisVisualListXZH[i].at(0);
                    auto nd1 = trisVisualListXZH[i].at(1);
                    auto nd2 = trisVisualListXZH[i].at(2);

                    bool flag = false;
                    for (int k = 0; k < vecCurCutEdges.size(); k++)
                    {
                        auto m0 = vecEdgesAll[vecCurCutEdges[k]].from;
                        auto m1 = vecEdgesAll[vecCurCutEdges[k]].to;

                        bool flag0 = false, flag1 = false;
                        if ((m0 == nd0) || (m0 == nd1) || (m0 == nd2))
                        {
                            flag0 = true;
                        }
                        if ((m1 == nd0) || (m1 == nd1) || (m1 == nd2))
                        {
                            flag1 = true;
                        }
                        flag = flag || (flag0&&flag1);   ////< XZH need to remove
                    }
                    if (!flag)   ////< XZH retain
                    {
                        vecList.push_back(trisVisualListXZH[i]);
                    }
                }
                trisVisualListXZH.clear();
                trisVisualListXZH = triListUpdate; // vecList;

                //trisVisualListXZH.insert(trisVisualListXZH.end(), triListUpdate.begin(), triListUpdate.end());
                surfMeshXZH->setVertexPositions(posVisualListXZH); // posVisualList); // vertPositions
                surfMeshXZH->setTrianglesVertices(trisVisualListXZH);
                surfMeshXZH->setTopologyChangedFlag(true);
            }
            else
            {
                isExistNewSurf = false;
                ////< XZH from tetrahedral mesh
                auto surfMeshXZH = std::make_shared<imstk::SurfaceMesh>();
                surfMeshXZH->initialize(posTetlist, triListUpdate);   ////< XZH   posTetlist from tetrahedral mesh if use triNewTotal from tet
                surfMeshXZH->setLoadFactor(200);
                auto surfMeshModelXZH = std::make_shared<VisualModel>(surfMeshXZH);
                //surfMeshXZH->initialize(posVisualList, triListUpdate);   ////< XZH from visual mesh if use visual mesh tri
                // visualization
                auto objectXZH = std::make_shared<imstk::VisualDeformableObject>("SurfaceXZH"); // "visualColonMesh0");
                auto materialXZH = std::make_shared<RenderMaterial>();
                materialXZH->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME SURFACE
                auto tmpXZH = std::make_shared<Texture>("I:/iMSTK/resources/VESS/texture/dissecting2.png", Texture::Type::DIFFUSE);
                auto tmpXZHN = std::make_shared<Texture>("I:/iMSTK/resources/VESS/texture/dissecting2NickNormal_2.png", Texture::Type::NORMAL);
                auto tmpXZHR = std::make_shared<Texture>("I:/iMSTK/resources/VESS/texture/dissecting2NickRoughness.png", Texture::Type::ROUGHNESS);
				//materialXZH->setColor(imstk::Color(0.27, 0.36, 0.56, 1));
                materialXZH->addTexture(tmpXZH);
                //materialXZH->addTexture(tmpXZHN);
                materialXZH->addTexture(tmpXZHR);
                materialXZH->setBackFaceCulling(false);
                //materialXZH->setTessellated(true);
                //materialXZH->addTexture(tmpXZH);
                surfMeshModelXZH->setRenderMaterial(materialXZH);
                objectXZH->addVisualModel(surfMeshModelXZH); // change to any mesh created above

				Vectorf UV(2);
				StdVectorOfVectorf UVsXZH;
				for (int i = 0; i < posTetlist.size(); i++)
				{
					auto ppos = posTetlist.at(i);
                    UV[0] = (ppos.x()*0.5 + ppos.z()*0.5) * 300.0;  // (ppos.x()*0.5 + ppos.z()*0.5) * 200.0;  // 300 is fine UV[0] = ppos.x() * 100; // 0.1 -10000
                    UV[1] = (ppos.y()*1.0 + ppos.z()*0.0)  * 300.0;  // UV[1] = ppos.y() * 100;
					UVsXZH.push_back(UV);
				}
				surfMeshXZH->setDefaultTCoords("tCoords");
				surfMeshXZH->setPointDataArray("tCoords", UVsXZH); // generates bug on 20190301
                m_scene->addSceneObject(objectXZH);
            }
			finishMain = clock();
			long dttime = finishMain - startMain;
			printf("generating new mesh :%ld ms\n", dttime);
			startMain = clock();

			////< XZH update collision vertex, edge, surface
			auto& m_edgeCollision = cutTissueMesh->getPhysEdge();
			auto& m_surftrisCollision = cutTissueMesh->getPhysSurfTris();
			auto& m_vertexCollision = cutTissueMesh->getPhysVertex();
			auto& m_mapVertexIndex = cutTissueMesh->getMapVertexIndex();
			int edgeOldCol = m_edgeCollision.size();
			int surfOldCol = m_surftrisCollision.size();

			int eIdx = m_edgeCollision.size() - 1;
			physEdge edgeTmp;
			physSurfTris surftrisTmp;
			auto& m_mapEdgeTriIndex = cutTissueMesh->m_mapEdgeTriIndex;
			auto& m_mapEdgeIndex = cutTissueMesh->m_mapEdgeIndex;
			for (int k = 0; k < triListUpdate.size(); k++)
			{
				int vert0 = triListUpdate.at(k)[0]; // three vertices of triangle
				int vert1 = triListUpdate.at(k)[1];
				int vert2 = triListUpdate.at(k)[2];
				Vec3d vp0 = posTetlist[vert0];
				Vec3d vp1 = posTetlist[vert1];
				Vec3d vp2 = posTetlist[vert2];

				U64 edgekey;
				int edge0, edge1, edge2;
				edge0 = edge1 = edge2 = -1;
				// 3 edges collision 0-1
				int m0, m1;
				m0 = vert0; m1 = vert1;
				if (m0 > m1)
				{
					std::swap(m0, m1);
				}
				edgekey = HASHEDGEID_FROM_IDX(m0, m1);

				std::unordered_map< U64, int >::iterator it = m_mapEdgeTriIndex.find(edgekey);
				if (it == m_mapEdgeTriIndex.end())
				{
					m_mapEdgeTriIndex.insert(std::make_pair(edgekey, k));
					edgeTmp.nodeId[0] = vert0;
					edgeTmp.nodeId[1] = vert1;
					edgeTmp.L0 = (posTetlist[vert0] - posTetlist[vert1]).norm();
					edgeTmp.colDetOn = true;
					m_edgeCollision.push_back(edgeTmp);
					eIdx++;
					edge0 = eIdx;
					m_mapEdgeIndex.insert(std::make_pair(edgekey, eIdx));
				}
				else
				{
					std::unordered_map< U64, int >::iterator it2 = m_mapEdgeIndex.find(edgekey);
					if (it2 != m_mapEdgeIndex.end())
					{
						edge0 = it2->second;
					}
				}
				// 1-2
				m0 = vert1; m1 = vert2;
				if (m0 > m1)
				{
					std::swap(m0, m1);
				}
				edgekey = HASHEDGEID_FROM_IDX(m0, m1);
				it = m_mapEdgeTriIndex.find(edgekey);
				if (it == m_mapEdgeTriIndex.end())
				{
					m_mapEdgeTriIndex.insert(std::make_pair(edgekey, k));
					edgeTmp.nodeId[0] = vert1;
					edgeTmp.nodeId[1] = vert2;
					edgeTmp.L0 = (posTetlist[vert1] - posTetlist[vert2]).norm();
					edgeTmp.colDetOn = true;
					m_edgeCollision.push_back(edgeTmp);
					eIdx++;
					edge1 = eIdx;
					m_mapEdgeIndex.insert(std::make_pair(edgekey, eIdx));
				}
				else
				{
					std::unordered_map< U64, int >::iterator it2 = m_mapEdgeIndex.find(edgekey);
					if (it2 != m_mapEdgeIndex.end())
					{
						edge1 = it2->second;
					}
				}
				// 0-2
				m0 = vert0; m1 = vert2;
				if (m0 > m1)
				{
					std::swap(m0, m1);
				}
				edgekey = HASHEDGEID_FROM_IDX(m0, m1);
				it = m_mapEdgeTriIndex.find(edgekey);
				if (it == m_mapEdgeTriIndex.end())
				{
					m_mapEdgeTriIndex.insert(std::make_pair(edgekey, k));
					edgeTmp.nodeId[0] = vert0;
					edgeTmp.nodeId[1] = vert2;
					edgeTmp.L0 = (posTetlist[vert0] - posTetlist[vert2]).norm();
					edgeTmp.colDetOn = true;
					m_edgeCollision.push_back(edgeTmp);
					eIdx++;
					edge2 = eIdx;
					m_mapEdgeIndex.insert(std::make_pair(edgekey, eIdx));
				}
				else
				{
					std::unordered_map< U64, int >::iterator it2 = m_mapEdgeIndex.find(edgekey);
					if (it2 != m_mapEdgeIndex.end())
					{
						edge2 = it2->second;
					}
				}

				  ////< XZH before to check, determine whether it exists, if all three are on surf, shows it already in
				bool flag = m_vertexCollision[vert0].onSurf&&m_vertexCollision[vert1].onSurf&&m_vertexCollision[vert2].onSurf;
				////< XZH vertex collision vertex for collision detection
				U64 vertexkey;
				physVertex vertexTmp;
				vertexkey = HASHEDGEID_FROM_IDX(vert0, 1000000);  // will not exceed 1 million, so we can use this way to get unique vertex
				it = m_mapVertexIndex.find(vertexkey);
				if (it == m_mapVertexIndex.end())  // not exist
				{
					m_mapVertexIndex.insert(std::make_pair(vertexkey, k));
					m_vertexCollision[vert0].globalIdx = vert0;
					m_vertexCollision[vert0].onSurf = true;
					m_vertexCollision[vert0].colDetOn = true;
				}
				vertexkey = HASHEDGEID_FROM_IDX(vert1, 1000000);  // will not exceed 1 million, so we can use this way to get unique vertex
				it = m_mapVertexIndex.find(vertexkey);
				if (it == m_mapVertexIndex.end())  // not exist
				{
					m_mapVertexIndex.insert(std::make_pair(vertexkey, k));
					m_vertexCollision[vert1].globalIdx = vert1;
					m_vertexCollision[vert1].onSurf = true;
					m_vertexCollision[vert1].colDetOn = true;
				}
				vertexkey = HASHEDGEID_FROM_IDX(vert2, 1000000);  // will not exceed 1 million, so we can use this way to get unique vertex
				it = m_mapVertexIndex.find(vertexkey);
				if (it == m_mapVertexIndex.end())  // not exist
				{
					m_mapVertexIndex.insert(std::make_pair(vertexkey, k));
					m_vertexCollision[vert2].globalIdx = vert2;
					m_vertexCollision[vert2].onSurf = true;
					m_vertexCollision[vert2].colDetOn = true;
				}
				// vertex END

				////< XZH triangle collision  // saving surf tris for collision
				surftrisTmp.nodeIdx[0] = vert0;
				surftrisTmp.nodeIdx[1] = vert1;
				surftrisTmp.nodeIdx[2] = vert2;

				surftrisTmp.edgeIdx[0] = edge0;
				surftrisTmp.edgeIdx[1] = edge1;
				surftrisTmp.edgeIdx[2] = edge2;

				surftrisTmp.tetraIdx = -1; // no need to be correct??? m_triTetlinksXZH[k].tetraIndex; // m_tetrahedraSurfaceIndex[k];
				surftrisTmp.colDetOn = true;
				if (!flag) m_surftrisCollision.push_back(surftrisTmp);
			}

			for (int i = 0; i < m_vertexCollision.size(); i++)
			{
				// re-initialize
				m_vertexCollision[i].nbrNeiSurfTri = 0;
				memset(m_vertexCollision[i].neiSurfTri, -1, 100 * sizeof(int));
				std::vector<U32> vecNeiSurfTri;
				std::vector<U32> vecNeiSurfEdge;

				int vertx = m_vertexCollision[i].globalIdx;

				// neighbor edges in colliding
				//m_vertexCollision[i].nbrNeiSurfEdge = 0;
				//memset(m_vertexCollision[i].neiSurfEdge, -1, 100 * sizeof(int));
				int numTmp = 0;
				for (int k = edgeOldCol; k < m_edgeCollision.size(); k++) // new edge not from 0
				{
					int m0 = m_edgeCollision[k].nodeId[0];
					int m1 = m_edgeCollision[k].nodeId[1];
					if ((m0 == vertx) || (m1 == vertx))
					{
						auto elen = vecNeiSurfEdge.size();
						vecNeiSurfEdge.push_back(k);

						numTmp = m_vertexCollision[i].nbrNeiSurfEdge; // 
						//m_vertexCollision[i].nbrNeiSurfEdge++;
						//m_vertexCollision[i].neiSurfEdge[numTmp] = k;
						numTmp++;   ////< XZH skip

						if (m_vertexCollision[i].nbrNeiSurfEdge >= 10)
						{
							printf("bug happend : vtex index %d,  edge index, %d \n", vertx, k);
						}
					}
				} // End for edge
				std::sort(vecNeiSurfEdge.begin(), vecNeiSurfEdge.end());
				std::unique(vecNeiSurfEdge.begin(), vecNeiSurfEdge.end());
				auto esize = vecNeiSurfEdge.size();
				m_vertexCollision[i].nbrNeiSurfEdge = esize;
				for (int v = 0; v < esize; v++)
				{
					m_vertexCollision[i].neiSurfEdge[v] = vecNeiSurfEdge[v];
				}

				// neighbor triangle  neighbor edges in colliding
				//m_vertexCollision[i].nbrNeiSurfTri = 0;
				//memset(m_vertexCollision[i].neiSurfTri, -1, 100 * sizeof(int));
				numTmp = 0;
				for (int k = 0; k < m_surftrisCollision.size(); k++)
				{
					int m0 = m_surftrisCollision[k].nodeIdx[0];
					int m1 = m_surftrisCollision[k].nodeIdx[1];
					int m2 = m_surftrisCollision[k].nodeIdx[2];
					if ((m0 == vertx) || (m1 == vertx) || (m2 == vertx))
					{
						auto vlen = vecNeiSurfTri.size();
						vecNeiSurfTri.push_back(k);
						
						numTmp = m_vertexCollision[i].nbrNeiSurfTri; // 
						//m_vertexCollision[i].nbrNeiSurfTri++;
						//m_vertexCollision[i].neiSurfTri[numTmp] = k;
						numTmp++;   ////< XZH skip
						
						//if (m_vertexCollision[i].nbrNeiSurfTri>=100)
						if(vecNeiSurfTri.size()>=10)
						{
							printf("bug happend : vtex index %d,  surf index, %d \n", vertx, k);
						}
					}
				} // END for  surf
				std::sort(vecNeiSurfTri.begin(), vecNeiSurfTri.end());
				std::unique(vecNeiSurfTri.begin(), vecNeiSurfTri.end());
				auto vsize = vecNeiSurfTri.size();
				m_vertexCollision[i].nbrNeiSurfTri = vsize;
				for (int v=0;v<vsize;v++)
				{
					m_vertexCollision[i].neiSurfTri[v] = vecNeiSurfTri[v];
				}
			}
			dttime = finishMain - startMain;
			printf("update colliding: %ld ms\n", dttime);
			////< XZH END collision update
            isDissecting = false;
        }
    }
      ////< XZH thread END

	  ////< XZH thread
	void SceneManager::runDissectionLoad()
	{
		if (hasLoadedDissect) return;

		// XZH  test cutting
		std::shared_ptr<VESSTetCutMesh> cutTissueMesh;
		std::shared_ptr<SurfaceMesh> visualColonMesh;
		std::shared_ptr<PbdVirtualCouplingObject> pbdObj;
		std::shared_ptr<PbdObject> pbdvcObj;
		//using TetTriMap  for cube dense
		for (auto obj : m_scene->getSceneObjects())
		{
			//continue;
			if (obj->getName() == "VirtualObject")
			{
				pbdObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(obj);
			}
			if (obj->getName() == "Colon")
			{
				pbdvcObj = std::dynamic_pointer_cast<PbdObject>(obj);
				cutTissueMesh = std::dynamic_pointer_cast<VESSTetCutMesh>(pbdvcObj->getPhysicsGeometry());
				visualColonMesh = std::dynamic_pointer_cast<imstk::SurfaceMesh>(pbdvcObj->getVisualGeometry());
			}
		}
		if (!pbdObj) return;
		if (!pbdvcObj) return;

		auto collidingMesh = std::dynamic_pointer_cast<PointSet>(pbdObj->getCollidingGeometry());
		auto& tool = pbdObj->getControllerTool();

		  ////< XZH loading file
		vector<string> strBufferVec;
		std::fstream infile("I:/dissecting.dat");
		if (!infile)
		{
			cout << "failed" << endl;
			//exit(1);
		}
		cout << "start..." << endl;
		string strLine;
		int numLines = 0;
		int numTotal = 0;
		int iNum=0;
		Vec3d posStart, posStartPre;
		Vec3d posEnd, posEndPre;
		while (getline(infile, strLine))
		{
			if (numLines > numTotal) break;

			strBufferVec.push_back(strLine);

			vector<string> strBufferVecTmp;
			strBufferVecTmp.push_back(strLine);
			////< XZH split
			vector<int> intVec;
			int width;
			for (vector<string>::iterator iter = strBufferVecTmp.begin(); iter != strBufferVecTmp.end(); iter++)
			{
				vector<string> strBufferLineVec;
				char separator = '\x20';
				cutStrLineBySeparator(*iter, separator, strBufferLineVec);
				width = strBufferLineVec.size();
				int numCols = 0;
				for (vector<string>::iterator iterLine = strBufferLineVec.begin(); iterLine != strBufferLineVec.end(); iterLine++)
				{
					stringstream ss;
					ss << *iterLine;
					double numF;
					ss >> numF;
					ss.clear();
					//intVec.push_back(numF);
					//cout << num<<" "; 
					if ((numLines == 0) && (numCols == 0))   ////< XZH total num
					{
						numTotal = (int) numF;
					}
					if ((numLines > 0) && (numLines <= 100))   ////< XZH each index
					{
						switch (numCols)
						{
						case 0:
							iNum = numF;
							break;
						case 1:
							posStartPre.x() = numF;
							break;
						case 2:
							posStartPre.y() = numF;
							break;
						case 3:
							posStartPre.z() = numF;
							break;
						case 4:
							posStart.x() = numF;
							break;
						case 5:
							posStart.y() = numF;
							break;
						case 6:
							posStart.z() = numF;
							break;
						case 7:
							posEndPre.x() = numF;
							break;
						case 8:
							posEndPre.y() = numF;
							break;
						case 9:
							posEndPre.z() = numF;
							break;
						case 10:
							posEnd.x() = numF;
							break;
						case 11:
							posEnd.y() = numF;
							break;
						case 12:
							posEnd.z() = numF;
							break;
						}
						numCols++;
					}
				}
			}

			if (numLines < 1)
			{
				numLines++;
				continue;
			}
			  ////< XZH start cut
			isDissecting = true;
			cutStartPre = cutStart;
			cutEndPre = cutEnd;
			cutStart = collidingMesh->getVertexPosition(0);
			cutEnd = collidingMesh->getVertexPosition(1); // 3 tip
			cutCurPos = cutEnd;
			cutCurPos0 = cutEnd + (cutStart - cutEnd)*0.05;

			// start cutting
			vector<Vec3d> m_vBladeSegments;
			vector<Vec3d> m_vSweptQuad;
			Vec3d temp1, temp2;
			Vec3d temp3, temp4;
			temp1 = posStartPre;
			temp2 = posStart;
			temp3 = posEndPre;
			temp4 = posEnd;

			double toolLen = 0.01; // 7.7510013580322266;
			temp1 = temp3 + (temp1 - temp3).normalized()*toolLen;
			temp2 = temp4 + (temp2 - temp4).normalized()*toolLen;

			//  ////< XZH based on the specific path

			////< XZH Cube END

			dissectTimes++;
			double rato = 1.0; // 0.01;
			temp1 *= rato;
			temp2 *= rato;
			temp3 *= rato;
			temp4 *= rato;
			////< XZH END specific path

			m_vBladeSegments.push_back(temp1);
			m_vBladeSegments.push_back(temp2);

			DWORD t1, t2;
			t1 = GetTickCount();

			m_vSweptQuad.push_back(temp1);
			m_vSweptQuad.push_back(temp2);
			m_vSweptQuad.push_back(temp3);
			m_vSweptQuad.push_back(temp4);


			// sweep surface
			bool isExistSweep = false;
			std::shared_ptr<SurfaceMesh> surfMeshSwept;
			for (auto objxzh : m_scene->getSceneObjects())
			{
				if (objxzh->getName() == "SweepSurface")
				{
					surfMeshSwept = std::dynamic_pointer_cast<SurfaceMesh>(objxzh->getVisualGeometry());
					break;
				}
			}
			if (surfMeshSwept)
			{
				isExistSweep = true;
				imstk::StdVectorOfVec3d vertListSwept;
				std::vector<imstk::SurfaceMesh::TriangleArray> trianglesSwept;

				trianglesSwept.push_back({ { 0, 1, 2 } });
				trianglesSwept.push_back({ { 2, 1, 3 } });
				auto& posVisualListXZH = surfMeshSwept->getVertexPositionsChangeable();
				auto& trisVisualListXZH = surfMeshSwept->getTrianglesVerticesChangeable();
				posVisualListXZH.push_back(m_vSweptQuad[0]);
				posVisualListXZH.push_back(m_vSweptQuad[1]);
				posVisualListXZH.push_back(m_vSweptQuad[2]);
				posVisualListXZH.push_back(m_vSweptQuad[3]); // 100
				size_t cntV = posVisualListXZH.size();
				trisVisualListXZH.push_back({ { cntV - 4, cntV - 3, cntV - 2 } });
				trisVisualListXZH.push_back({ { cntV - 2, cntV - 3, cntV - 1 } });
				surfMeshSwept->setVertexPositions(posVisualListXZH); // posVisualList); // vertPositions
				surfMeshSwept->setTrianglesVertices(trisVisualListXZH);
				surfMeshSwept->setTopologyChangedFlag(true);
			}
			else
			{
				isExistSweep = false;
				imstk::StdVectorOfVec3d vertListSwept;
				std::vector<imstk::SurfaceMesh::TriangleArray> trianglesSwept;
				vertListSwept.push_back(m_vSweptQuad[0]);
				vertListSwept.push_back(m_vSweptQuad[1]);
				vertListSwept.push_back(m_vSweptQuad[2]);
				vertListSwept.push_back(m_vSweptQuad[3]);
				trianglesSwept.push_back({ { 0, 1, 2 } });
				trianglesSwept.push_back({ { 2, 1, 3 } });
				for (int k = 0; k < 50; k++)
				{
					vertListSwept.push_back(m_vSweptQuad[0]);
					vertListSwept.push_back(m_vSweptQuad[1]);
					vertListSwept.push_back(m_vSweptQuad[2]);
					vertListSwept.push_back(m_vSweptQuad[3]); // 100
					size_t cntV = vertListSwept.size();
					trianglesSwept.push_back({ { cntV - 4, cntV - 3, cntV - 2 } });
					trianglesSwept.push_back({ { cntV - 2, cntV - 3, cntV - 1 } });
				}
				surfMeshSwept = std::make_shared<imstk::SurfaceMesh>();
				surfMeshSwept->initialize(vertListSwept, trianglesSwept);
				auto surfMeshSweptModel = std::make_shared<VisualModel>(surfMeshSwept);
				// visualization
				auto object0 = std::make_shared<imstk::VisualObject>("SweepSurface"); // "visualColonMesh0");
				auto material0 = std::make_shared<RenderMaterial>();
				material0->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME SURFACE
				auto tmp0 = std::make_shared<Texture>("I:/iMSTK/resources/VESS/testtest.png", Texture::Type::DIFFUSE);
				//material0->addTexture(tmp0);
				//material0->setTessellated(true);
				material0->setColor(imstk::Color::Green);
				material0->setBackFaceCulling(false);
				surfMeshSweptModel->setRenderMaterial(material0);
				object0->addVisualModel(surfMeshSweptModel); // change to any mesh created above
				m_scene->addSceneObject(object0);
			}

			int res = cutTissueMesh->cutNewXZH3(m_vBladeSegments, m_vSweptQuad, true); ////< XZH  0: no cut, should return, 1: has cut ,should 
			if (res < 1)
			{
				return;
			}

			// after cutting ,update graph
			auto& tetVertices = cutTissueMesh->getInitTetrahedraVertices();
			auto& posTetlist = cutTissueMesh->getVertexPositions();
			StdVectorOfVec3d posTetTest;
			StdVectorOfVec3d posTetlist2 = posTetlist;
			for (int n = 0; n < posTetlist2.size(); n++)
			{
				auto ppos = posTetlist2[n];
				ppos = ppos * 1000;
				posTetTest.push_back(ppos);
			}
			// after cutting
			auto& tetList = cutTissueMesh->getCutElements();  // just current 
			auto& listGraphVertices = cutTissueMesh->getGraphVertices();
			auto& listGraphTets = cutTissueMesh->getGraphTetrahedrons();
			auto& currentTetStates = cutTissueMesh->getTetrahedronStates();
			auto& graphVolume = cutTissueMesh->getGraphVolume();

			//fixed point
			//Vec3d posTumorCenter = Vec3d(3.0, 4.41, 5.38);   ////< XZH for CUBE
			Vec3d posTumorCenter = Vec3d(3.5, 8.9, 19.0)*0.01;   ////< XZH for Colon
			double distTumor = 2.0*0.01;
			std::string fixed_corner;
			char intStr[33];
			Vec3d tmpPos;
			std::vector<int> fixedVertice; // add later 06/04/2017
			auto& fixedStates = cutTissueMesh->getFixedVerticeStates();
			auto& mapTetrahedrons = cutTissueMesh->getTetrahedronMap();
			auto& tetListCutAll = cutTissueMesh->getCutElementsTotal();  // Total 
			auto& tetListCutCur = cutTissueMesh->getCutElements();  // just current 
			sort(tetListCutCur.begin(), tetListCutCur.end());
			tetListCutCur.erase(unique(tetListCutCur.begin(), tetListCutCur.end()), tetListCutCur.end());

			auto dynaModel1 = std::static_pointer_cast<PbdModel>(pbdvcObj->getDynamicalModel());
			dynaModel1->initializeReset();

			////< XZH visual mesh
			auto& posVisualList = visualColonMesh->getVertexPositionsChangeable(); // changable 
																				   //posVisualList = posTetlist;
			auto& triListsXZH = visualColonMesh->getTrianglesVerticesXZH();  // XZH use for change
			auto& triListsTextureOrder = cutTissueMesh->getTriTextureOrder();  // XZH use for texture
			auto& triCutTotal = cutTissueMesh->getCutVisualTotal();
			auto& triNewTotal = cutTissueMesh->getCutNewTriangles(); // cutTissueMesh->getCutNewTotal();
			auto& triIndexInTet = cutTissueMesh->getVisualTriIndexInTet();   ////< XZH original tri index, but after topology changed, the index need to be changed?? this is a bug 836/840
			auto& vecCurCutEdges = cutTissueMesh->getCutEdges();
			auto& vecEdgesAll = cutTissueMesh->getEdgesAll();
			std::vector<SurfaceMesh::TriangleArray> triListsNew;
			for (int k = 0; k < triListsXZH.size(); k++)   ////< XZH original tris surface
			{
				bool isCut = false;
				int nk = triIndexInTet[k];   ////< XZH each tri has index in the whole tris with inside
				for (int n = 0; n < triCutTotal.size(); n++)   ////< XZH 
				{
					if (nk == triCutTotal[n])
					{
						isCut = true;
						break;
					}
				}
				if (!isCut)
				{
					//triListsNew.push_back(triListsXZH[k]);  ////< XZH 
					triListsNew.push_back(triListsTextureOrder[k]);
				}
			}
			visualColonMesh->setVertexPositions(posVisualList);
			visualColonMesh->setTrianglesVertices(triListsNew);
			visualColonMesh->setTopologyChangedFlag(true);

			////< XZH new surface mesh
			auto tetTrianglesAll = cutTissueMesh->getTetTrianglesAll();
			std::vector<SurfaceMesh::TriangleArray> triListUpdate = triNewTotal; // = triListsNew;

																				 // sweep surface
			bool isExistNewSurf = false;
			std::shared_ptr<SurfaceMesh> surfMeshXZH;
			for (auto objxzh : m_scene->getSceneObjects())
			{
				if (objxzh->getName() == "SurfaceXZH")
				{
					surfMeshXZH = std::dynamic_pointer_cast<SurfaceMesh>(objxzh->getVisualGeometry());
					break;
				}
			}
			if (surfMeshXZH)
			{
				isExistNewSurf = true;
				auto& posVisualListXZH = surfMeshXZH->getVertexPositionsChangeable();
				auto& trisVisualListXZH = surfMeshXZH->getTrianglesVerticesChangeable();
				std::vector<SurfaceMesh::TriangleArray>::iterator it; // = trisVisualListXZH.begin();

																	  ////< XZH Method2
				std::vector<SurfaceMesh::TriangleArray> vecList; // = trisVisualListXZH;
				for (int i = 0; i < trisVisualListXZH.size(); i++)
				{
					auto nd0 = trisVisualListXZH[i].at(0);
					auto nd1 = trisVisualListXZH[i].at(1);
					auto nd2 = trisVisualListXZH[i].at(2);

					bool flag = false;
					for (int k = 0; k < vecCurCutEdges.size(); k++)
					{
						auto m0 = vecEdgesAll[vecCurCutEdges[k]].from;
						auto m1 = vecEdgesAll[vecCurCutEdges[k]].to;

						bool flag0 = false, flag1 = false;
						if ((m0 == nd0) || (m0 == nd1) || (m0 == nd2))
						{
							flag0 = true;
						}
						if ((m1 == nd0) || (m1 == nd1) || (m1 == nd2))
						{
							flag1 = true;
						}
						flag = flag || (flag0&&flag1);   ////< XZH need to remove
					}
					if (!flag)   ////< XZH retain
					{
						vecList.push_back(trisVisualListXZH[i]);
					}
				}
				trisVisualListXZH.clear();
				trisVisualListXZH = triListUpdate; // vecList;

												   //trisVisualListXZH.insert(trisVisualListXZH.end(), triListUpdate.begin(), triListUpdate.end());
				surfMeshXZH->setVertexPositions(posVisualListXZH); // posVisualList); // vertPositions
				surfMeshXZH->setTrianglesVertices(trisVisualListXZH);
				surfMeshXZH->setTopologyChangedFlag(true);
			}
			else
			{
				isExistNewSurf = false;
				////< XZH from tetrahedral mesh
				auto surfMeshXZH = std::make_shared<imstk::SurfaceMesh>();
				surfMeshXZH->initialize(posTetlist, triListUpdate);   ////< XZH   posTetlist from tetrahedral mesh if use triNewTotal from tet
				surfMeshXZH->setLoadFactor(20);
				auto surfMeshModelXZH = std::make_shared<VisualModel>(surfMeshXZH);
				//surfMeshXZH->initialize(posVisualList, triListUpdate);   ////< XZH from visual mesh if use visual mesh tri
				// visualization
				auto objectXZH = std::make_shared<imstk::VisualDeformableObject>("SurfaceXZH"); // "visualColonMesh0");
				auto materialXZH = std::make_shared<RenderMaterial>();
				materialXZH->setDisplayMode(imstk::RenderMaterial::DisplayMode::SURFACE); // WIREFRAME SURFACE
				auto tmpXZH = std::make_shared<Texture>("I:/iMSTK/resources/VESS/texture/dissecting2.png", Texture::Type::DIFFUSE);
				//materialXZH->setColor(imstk::Color(0.27, 0.36, 0.56, 1));
				materialXZH->addTexture(tmpXZH);
				materialXZH->setBackFaceCulling(false);
				//materialXZH->setTessellated(true);
				//materialXZH->addTexture(tmpXZH);
				surfMeshModelXZH->setRenderMaterial(materialXZH);
				objectXZH->addVisualModel(surfMeshModelXZH); // change to any mesh created above

				Vectorf UV(2);
				StdVectorOfVectorf UVsXZH;
				for (int i = 0; i < posTetlist.size(); i++)
				{
					auto ppos = posTetlist.at(i);
					UV[0] = ppos.x() * 100; // 0.1 -10000
					UV[1] = ppos.y() * 100;
					UVsXZH.push_back(UV);
				}
				surfMeshXZH->setDefaultTCoords("tCoords");
				surfMeshXZH->setPointDataArray("tCoords", UVsXZH); // generates bug on 20190301
				m_scene->addSceneObject(objectXZH);
			}
			isDissecting = false;
			  ////< XZH END cut
			numLines++;
		}
		cout << numLines << "->end..." << endl;
		infile.close();
		hasLoadedDissect = true;
		auto& flag=m_scene->getIsLoadDissect();
		flag = false;
	}
	////< XZH thread END

    std::shared_ptr<Scene>
        SceneManager::getScene()
    {
        return m_scene;
    }

    void
        SceneManager::initModule()
    {
        // Start Camera Controller (asynchronous)
        if (auto camController = m_scene->getCamera()->getController())
        {
            this->startModuleInNewThread(camController);
        }

        // Update objects controlled by the device controllers XZH
        for (auto controller : m_scene->getSceneObjectControllers())
        {
            if (auto pbdController = std::dynamic_pointer_cast<PBDSceneObjectController>(controller))
            {
                pbdController->initOffsets();
            }
        }

        // Init virtual coupling objects offsets
        for (auto obj : m_scene->getSceneObjects())
        {
            if (auto virtualCouplingPBD = std::dynamic_pointer_cast<VirtualCouplingPBDObject>(obj))
            {
                virtualCouplingPBD->initOffsets();
            }
        }

        // test tool position
        for (auto obj : m_scene->getSceneObjects())
        {
            // haptic tool
            if (obj->getName() == "VirtualObject")
            {

            }            
        }

        // navgation
        vecCamera.push_back(Vec3d(3.049765, -5.416139, -13.772857));
        vecCamera.push_back(Vec3d(2.066011, -2.092456, -16.283300));
        vecCamera.push_back(Vec3d(2.416454, 0.893009, -18.732140));
        vecCamera.push_back(Vec3d(2.656587, 3.308475, -21.841312));
        vecCamera.push_back(Vec3d(0.744602, 4.685533, -24.712399));
        vecCamera.push_back(Vec3d(-2.111103, 5.906062, -24.198272));

        vecCameraFocal.push_back(Vec3d(-4.065353, 2.550728, -18.045065));
        vecCameraFocal.push_back(Vec3d(-2.318216, 3.125296, -19.496111));
        vecCameraFocal.push_back(Vec3d(-0.275814, 4.097096, -20.704975));
        vecCameraFocal.push_back(Vec3d(-0.668360, 6.501790, -21.953945));
        vecCameraFocal.push_back(Vec3d(-0.777570, 7.189897, -22.593023));
        vecCameraFocal.push_back(Vec3d(-6.532188, 16.588575, -13.986447));

        // cutting and dissection for Graph init
        for (int i = 0; i < GRAPHMAPSIZEVISUAL;i++)
        {
            std::vector<int> glist;
            for (int j = 0; j < GRAPHMAPSIZEVISUAL; j++)
            {
                glist.push_back(0);
            }
            graphVisualCutting.push_back(glist);
        }
        m_graphVisualCutting = std::make_shared<imstk::UndirectedGraph>(graphVisualCutting);

        // cut nodes root
        posCutNodesRoot.resize(8);
        posCutNodesTip.resize(8);
        posCutNodesTip[0] = Vec3d(3.1, 3.6, 2);
        posCutNodesTip[1] = Vec3d(3.7, 3.6, 2);
        posCutNodesTip[2] = Vec3d(4.2, 4.1, 2);
        posCutNodesTip[3] = Vec3d(4.2, 4.6, 2);
        posCutNodesTip[4] = Vec3d(3.7, 5, 2);
        posCutNodesTip[5] = Vec3d(3.1, 5, 2);
        posCutNodesTip[6] = Vec3d(2.5, 4.6, 2);
        posCutNodesTip[7] = Vec3d(2.5, 4.1, 2);
        // tip
        posCutNodesRoot[0] = Vec3d(3.1, 3.6, 9.75);
        posCutNodesRoot[1] = Vec3d(3.7, 3.6, 9.75);
        posCutNodesRoot[2] = Vec3d(4.2, 4.1, 9.75);
        posCutNodesRoot[3] = Vec3d(4.2, 4.6, 9.75);
        posCutNodesRoot[4] = Vec3d(3.7, 5, 9.75);
        posCutNodesRoot[5] = Vec3d(3.1, 5, 9.75);
        posCutNodesRoot[6] = Vec3d(2.5, 4.6, 9.75);
        posCutNodesRoot[7] = Vec3d(2.5, 4.1, 9.75);

        for (int i = 0; i < 8;i++)
        {
            posCutNodesRoot[i] = posCutNodesRoot[i] + Vec3d(0, 0, 2.2);
            posCutNodesTip[i] = posCutNodesTip[i] + Vec3d(0, 0, 2.2);
        }

          ////< XZH w.r.t. Dissecting path defination nodes 
        posDissectNodesRoot.resize(16);
        posDissectNodesTip.resize(16);
        for (int i = 0; i < posDissectNodesTip.size(); i++)
        {
            posDissectNodesTip[i] = Vec3d(0, 9.5, 9) /*middle of dissect plane*/ + 0 * Vec3d(0.22872038256899530, -0.33268419282762951, -0.91488153027598118);   ////< XZH normal of dissecting plane
        }
        // root
        posDissectNodesRoot[0] = Vec3d(0, 15, 7);
        posDissectNodesRoot[1] = Vec3d(-5, 15, 5.75);
        posDissectNodesRoot[2] = Vec3d(-10, 15, 4.5);
        posDissectNodesRoot[3] = Vec3d(-10, 12.25, 5.5);
        posDissectNodesRoot[4] = Vec3d(-10, 9.5, 6.5);
        posDissectNodesRoot[5] = Vec3d(-10, 6.75, 7.5);
        posDissectNodesRoot[6] = Vec3d(-10, 4, 8.5);
        posDissectNodesRoot[7] = Vec3d(-5, 4, 9.75);
        posDissectNodesRoot[8] = Vec3d(0, 4, 11);
        posDissectNodesRoot[9] = Vec3d(5, 4, 12.25);
        posDissectNodesRoot[10] = Vec3d(10, 4, 13.5);
        posDissectNodesRoot[11] = Vec3d(10, 6.75, 12.5);
        posDissectNodesRoot[12] = Vec3d(10, 9.5, 11.5);
        posDissectNodesRoot[13] = Vec3d(10, 12.25, 10.5);
        posDissectNodesRoot[14] = Vec3d(10, 15, 9.5);
        posDissectNodesRoot[15] = Vec3d(5, 15, 8.25);
        Vec3d posCen = Vec3d(0, 9.5, 9);
        Vec3d posCenIn = posDissectNodesTip[0];   ////< XZH inside
        double radCen = 6.0;   ////< XZH radus
        for (int i = 0; i < 16;i++)
        {
            Vec3d tmp = posDissectNodesRoot[i];
            posDissectNodesRoot[i] = posCen + (tmp - posCen).normalized()*radCen;
        }
        double toolLen = 7.7510013580322266;
        for (int i = 0; i < 16; i++)
        {
            Vec3d tmp = posDissectNodesRoot[i];
            posDissectNodesRoot[i] = posCenIn + (tmp - posCenIn).normalized()*toolLen;
        }

          ////< XZH 
        cutStartInit = Vec3d::Zero();
        cutEndInit = Vec3d::Zero();
    }

    void
        SceneManager::runModule()
	{
		//Sleep(100);
        // XZH
        t1 = t2;
		//printf("gap time %d, %d,  %d ms\n", t1, t2, t2 - t1);
        t2 = GetTickCount();
		//printf("gap time %d, %d,  %d ms,  frame rate is: %f\n",t1, t2, t2 - t1, 1000 / double(t2 - t1)); //  fprintf(outForceFilter, "gap time %d ms,  frame rate is: %f\n", t2 - t1, 1000 / double(t2 - t1));
        // XZH END

        // XZH camera
        auto cam1 = m_scene->getCamera();
        Vec3d camPos = cam1->getPosition();
        Vec3d camFocalPoint = cam1->getFocalPoint();
        double viewAngle = cam1->getFieldOfView();
        Vec3d viewup = cam1->getViewUp();
		auto light1 = std::dynamic_pointer_cast<imstk::SpotLight>(m_scene->getLight("whiteLight"));
        Vec3d lightPos, lightFocalPoint;
        if (light1)
        {
            Vec3d lightPos = light1->getPosition();
            Vec3d lightFocalPoint = light1->getFocalPoint();
        }

		//printf("Light pos: %.5f %.5f %.5f, focal: %.5f %.5f %.5f\n", lightPos.x(), lightPos.y(), lightPos.z(), lightFocalPoint.x(), lightFocalPoint.y(), lightFocalPoint.z());
          ////< XZH viewer START
        int mProcType = m_scene->getProcedureType();
        if (mProcType == mProcedure) isProcChanged = false;
        else
        {
            isProcChanged = true;
            mProcedure = mProcType;
        }
        bool isPBD = m_scene->getIsPBD();
		bool isLoadDissect = m_scene->getIsLoadDissect();
        //  ////< XZH viewer END
		
        Vec3d tip, root;
        tip = root = Vec3d::Zero();
		imstk::StdVectorOfVec3d vertsTool;
		vertsTool.resize(6);
        Vec3d centralPosition = Vec3d::Zero();
        StopWatch wwt;
        wwt.start();

        std::shared_ptr<PbdVirtualCouplingObject> toolMarking;
        Vec3d toolmTip, toolmRoot = Vec3d::Zero();

        // Reset Contact forces to 0
        for (auto obj : m_scene->getSceneObjects())
        {
            if (auto defObj = std::dynamic_pointer_cast<DeformableObject>(obj))
            {
                defObj->getContactForce().setConstant(0.0);
            }
            if (auto pbdvcObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(obj))
            {
				if (obj->getName() == "VirtualObject1") // marking
				{
					if (mProcType!=0) continue;
				}
				else if (obj->getName() == "VirtualObject2") // injecting
				{
					if (mProcType != 1) continue;
				}
				else if (obj->getName() == "VirtualObject") // cutting
				{
					if (mProcType != 2) continue;
				}
				else continue;
                // XZH start
                toolMarking = pbdvcObj;
                auto vmesh = std::dynamic_pointer_cast<PointSet>(pbdvcObj->getVisualGeometry());
                Vec3d min1, max1;
                vmesh->computeBoundingBox(min1, max1);
                auto collidingMesh = std::dynamic_pointer_cast<PointSet>(pbdvcObj->getCollidingGeometry());
                root = collidingMesh->getVertexPosition(0);
                tip = collidingMesh->getVertexPosition(1); // 1 not 2
				vertsTool[0] = root;
				vertsTool[1] = tip;
				//printf("AA %.5f, %.5f, %.5f, %.5f, %.5f, %.5f\n", vertsTool[2].x(), vertsTool[2].y(), vertsTool[2].z(), vertsTool[3].x(), vertsTool[3].y(), vertsTool[3].z());
                pbdvcObj->resetForce();  // XZH
                //printf("tip: %f %f %f, root: %f %f %f\n", tip.x(), tip.y(), tip.z(), root.x(), root.y(), root.z());
                  ////< XZH tool
                auto& tool = toolMarking->getControllerTool();
                toolmRoot = tool.pDrawPos + tool.pGloVec[0]; // ROOT
                toolmTip = tool.drawPos + tool.pGloVec[1];

				vertsTool[2] = tool.pDrawPos + tool.pGloVec[2];  //  vertsTool[2] = collidingMesh->getVertexPosition(2);
				vertsTool[3] = tool.pDrawPos + tool.pGloVec[3];  // vertsTool[3] = collidingMesh->getVertexPosition(3);
				vertsTool[4] = tool.pDrawPos + tool.pGloVec[4];  // vertsTool[4] = collidingMesh->getVertexPosition(4);
				vertsTool[5] = tool.pDrawPos + tool.pGloVec[5];  // vertsTool[5] = collidingMesh->getVertexPosition(5);
				//printf("S1: %.5f %.5f %.5f, %.5f %.5f %.5f\n", vertsTool[2].x(), vertsTool[2].y(), vertsTool[2].z(), vertsTool[3].x(), vertsTool[3].y(), vertsTool[3].z());
                // XZH END
            }
            // todo: refactor pbd
            // description: so that the transform obtained from device can be applied
            if (auto virtualCouplingPBD = std::dynamic_pointer_cast<VirtualCouplingPBDObject>(obj))
            {
                virtualCouplingPBD->resetCollidingGeometry();
            }
        }

        // Update objects controlled by the device controllers
        for (auto controller : m_scene->getSceneObjectControllers())
        {
            auto pbdController = std::dynamic_pointer_cast<PBDSceneObjectController>(controller);
            if (pbdController)  ////< XZH new
            {
                pbdController->switchToolsXZH(mProcType);
                pbdController->updateControlledObjectsALL();
            }
            else   ////< XZH old
            {
                controller->updateControlledObjects();
            }
        }

        // Compute collision data per interaction pair
        for (auto intPair : m_scene->getCollisionGraph()->getInteractionPairList())
        {
            intPair->computeCollisionData(tip);  // for others XZH intPair->computeCollisionData();
            intPair->computeContactForces();
        }

          ////< XZH only compute simple CD 08/12/2019
        for (auto intPair : m_scene->getCollisionGraph()->getPbdPairList())
        {
            intPair->doSimpleCollision(mProcType);
        }

        //for (auto controller : m_scene->getSceneObjectControllers())
        //{
        //    controller->applyForces();
        //}

        // Update the solvers
        for (auto solvers : m_scene->getSolvers())
        {
            // XZH test
            bool isColonInjected = false;
            for (auto intPair : m_scene->getCollisionGraph()->getPbdPairList())
            {
                auto pbdObj = intPair->getSecondObj();
                if ((pbdObj->getIsInjection()) && (pbdObj->getName() == "Colon"))
                {
                    isColonInjected = true;
                    break;
                }
            }
            if (!isColonInjected)
            {
                //if (isNoCut) // isEND
                if (isPBD)
                {
                    auto intPairPBD = m_scene->getCollisionGraph()->getPbdPairList()[0];
                    bool& flagPBD = intPairPBD->getOpenPBD();
                    //if (!flagPBD)
                    {
                        solvers->solve();  // original is just this one line code
                        flagPBD = false;
                    }           
                }
            }
            // XZH END
            //solvers->solve();
        }
        // XZH test

        std::shared_ptr<VESSTetCutMesh> m_xzhPhysicalMesh;   ////< XZH check??!?!??
        std::shared_ptr<SurfaceMesh> m_xzhVisualMesh;
		StdVectorOfVec3d m_xzhVisualMeshVertPoses;  

        // Apply the geometry and apply maps to all the objects
        for (auto obj : m_scene->getSceneObjects())
        {
            // not update for pbd virtualcoupling
            if (auto pbdvcObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(obj))
            {
                //pbdvcObj->resetCollidingGeometry();
                // XZH start
                auto collidingMesh = std::dynamic_pointer_cast<PointSet>(pbdvcObj->getCollidingGeometry());
                root = collidingMesh->getVertexPosition(0);
                tip = collidingMesh->getVertexPosition(1); // 1 not 2
                if (isBegin)
                {
                    cutStartPre = root;
                    cutEndPre = tip;
                    isBegin = false;
                }           
                // XZH END
            }
            else
            {
                obj->updateGeometries(); // physical drive visual
            }

            // testing get visual
            if (obj->getName() == "Colon")
            {
                auto pbdvcObj = std::dynamic_pointer_cast<PbdObject>(obj);
                m_xzhVisualMesh = std::dynamic_pointer_cast<SurfaceMesh>(pbdvcObj->getVisualGeometry());
				m_xzhVisualMeshVertPoses = m_xzhVisualMesh->getVertexPositionsChangeable();
            }
        }

        for (auto obj : m_scene->getSceneObjects())
        {
            if (obj->getName() == "VirtualObject")   ////< XZH cutting
            {
                if (isTest)
                {
                    auto pbdvcObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(obj);
                    auto visualMesh = std::dynamic_pointer_cast<SurfaceMesh>(pbdvcObj->getVisualGeometry());
                    auto collidingMesh = std::dynamic_pointer_cast<PointSet>(pbdvcObj->getCollidingGeometry());
                    Vec3d pos0 = collidingMesh->getVertexPosition(0);
                    auto& posList = visualMesh->getVertexPositionsChangeable();
                    for (int i = 0; i < posList.size(); i++)
                    {
                        dirGapVisualColliding.push_back(posList[i] - pos0);
                    }
                    isTest = false;
                }
            }      
            if (obj->getName() == "VirtualObject")   ////< XZH dissecting
            {
                if (0) // (isTestDissect0)
                {
                    auto pbdvcObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(obj);
                    auto visualMesh = std::dynamic_pointer_cast<SurfaceMesh>(pbdvcObj->getVisualGeometry());
                    auto collidingMesh = std::dynamic_pointer_cast<PointSet>(pbdvcObj->getCollidingGeometry());
                    Vec3d pos0 = collidingMesh->getVertexPosition(0);
                    auto& posList = visualMesh->getVertexPositionsChangeable();
                    for (int i = 0; i < posList.size(); i++)
                    {
                        dirGapVisualDissecting0.push_back(posList[i] - pos0);
                    }
                    isTestDissect0 = false;
                }
            }
            if (obj->getName() == "VirtualObject1")   ////< XZH dissecting
            {
                if (isTestDissect1)
                {
                    auto pbdvcObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(obj);
                    auto visualMesh = std::dynamic_pointer_cast<SurfaceMesh>(pbdvcObj->getVisualGeometry());
                    auto collidingMesh = std::dynamic_pointer_cast<PointSet>(pbdvcObj->getCollidingGeometry());
                    Vec3d pos0 = collidingMesh->getVertexPosition(0);
                    auto& posList = visualMesh->getVertexPositionsChangeable();
                    for (int i = 0; i < posList.size(); i++)
                    {
                        dirGapVisualDissecting1.push_back(posList[i] - pos0);
                    }
                    isTestDissect1 = false;
                }
            }
            if (obj->getName() == "VirtualObject2")   ////< XZH dissecting
            {
                if (isTestDissect2)
                {
                    auto pbdvcObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(obj);
                    auto visualMesh = std::dynamic_pointer_cast<SurfaceMesh>(pbdvcObj->getVisualGeometry());
                    auto collidingMesh = std::dynamic_pointer_cast<PointSet>(pbdvcObj->getCollidingGeometry());
                    Vec3d pos0 = collidingMesh->getVertexPosition(0);
                    auto& posList = visualMesh->getVertexPositionsChangeable();
                    for (int i = 0; i < posList.size(); i++)
                    {
                        dirGapVisualDissecting2.push_back(posList[i] - pos0);
                    }
                    isTestDissect2 = false;
                }
            }
			if (obj->getName()=="SurroundingTool")
			{
				auto vcObj = std::dynamic_pointer_cast<VisualObject>(obj);
				auto visualMesh = std::dynamic_pointer_cast<SurfaceMesh>(vcObj->getVisualModels()[0]->getGeometry());
				for (int k=0;k<6;k++)
				{
					visualMesh->setVertexPosition(k, vertsTool[k]);
				}
				
				//printf("BB %.5f, %.5f, %.5f, %.5f, %.5f, %.5f\n", vertsTool[2].x(), vertsTool[2].y(), vertsTool[2].z(), vertsTool[3].x(), vertsTool[3].y(), vertsTool[3].z());
				//auto& vvlists=visualMesh->getVertexPositionsChangeable(); 
				//vvlists = vertsTool;
				int tt = 0;
			}
        }

        // XZH  update two test sphere  one is tip of tool, one is root of tool
        float preRot = 0.0;
        float curRot = 0.0;
        for (auto obj : m_scene->getSceneObjects())
        {
              ////< XZH START Marking change  
            if (auto decalPool = std::dynamic_pointer_cast<DecalPool>(obj->getVisualGeometry()))
            {
                auto& decallist = decalPool->getDecals();  
                int idx = decallist.at(0)->getVisualIndex();
                if (idx>0)
                {
                    decallist.at(0)->setPosition(m_xzhVisualMesh->getVertexPosition(idx)+Vec3d(0,0,0.0002));
                }
            }
            if (auto markingSphere = std::dynamic_pointer_cast<Sphere>(obj->getVisualGeometry()))
            {
                int idx = markingSphere->getVisualIndex();
                if (idx > 0)
                {
                    markingSphere->setPosition(m_xzhVisualMesh->getVertexPosition(idx));
                }
            }
              ////< XZH camera pos and focal
            if (obj->getName() == "SphereCam")
            {
                obj->getVisualGeometry()->setTranslation(camPos);
            }
            else if (obj->getName() == "SphereCamFocal")
            {
                obj->getVisualGeometry()->setTranslation(camFocalPoint);
            }
              ////< XZH Marking END
            else if (obj->getName() == "Sphere1")
            {
                obj->getVisualGeometry()->setTranslation(root);
				//obj->getVisualGeometry()->setTranslation(vertsTool[2]);
            }
            else if (obj->getName() == "Sphere2")
            {
                obj->getVisualGeometry()->setTranslation(tip);
				//obj->getVisualGeometry()->setTranslation(vertsTool[3]);
            }
			  ////< XZH test for camera light position
			else if (obj->getName() == "Sphere1d")
			{
				//obj->getVisualGeometry()->setTranslation(lightPos); 
			}
			else if (obj->getName() == "Sphere2d")
			{
				//obj->getVisualGeometry()->setTranslation(lightFocalPoint); 
			}
			  ////< XZH dissecting position
    //        else if (obj->getName() == "Sphere1d")
    //        {
    //            Vec3d temp2 = tip + (root - tip).normalized()*0.0040;
    //            Vec3d temp4 = temp2 + (tip - temp2).normalized()*0.01*0.34;
				//obj->getVisualGeometry()->setTranslation(temp2); //(toolmRoot); // (temp2); // (toolmRoot);
				////obj->getVisualGeometry()->setTranslation(vertsTool[4]);
    //        }
    //        else if (obj->getName() == "Sphere2d")
    //        {
			 //   Vec3d temp2 = tip + (root - tip).normalized()*0.0040;
    //            Vec3d temp4 = temp2 + (tip - temp2).normalized()*0.01*0.34;
    //            obj->getVisualGeometry()->setTranslation(temp4); // (toolmTip);   // (temp4); // (toolmTip);       
				////obj->getVisualGeometry()->setTranslation(vertsTool[5]);
    //        }
            else if (obj->getName() == "Cylinder1")   ////< XZH cylinder for collision
            {
                auto cylinderGeo = std::dynamic_pointer_cast<Cylinder>(obj->getVisualModels()[0]->getGeometry());
                double XX = 0; // m_scene->getTestTranX();
                double YY = 0; // m_scene->getTestTranY();
                double ZZ = 0; // m_scene->getTestTranZ();
                double RXX = 0; //m_scene->getTestRotX();
                double RZZ = 0; // m_scene->getTestRotZ();
                int flagXZ = m_scene->getTestFlagXZ();
                float rad = 0; // m_scene->getTestLen();
                //cylinderGeo->setRadius(rad);
                //if (flagXZ == 0) cylinderGeo->setRotation(imstk::RIGHT_VECTOR, RXX);
                //else cylinderGeo->setRotation(imstk::BACKWARD_VECTOR, RZZ - 0.28798);
                //cylinderGeo->setRotation(imstk::BACKWARD_VECTOR, RZZ - 0.28798);
                if (isTestRot)
                {
                    cylinderGeo->setRotation(imstk::BACKWARD_VECTOR, RZZ - 0.28798);
                    //cylinderGeo->rotate(imstk::BACKWARD_VECTOR, -0.28798, imstk::Geometry::TransformType::ApplyToData);
                    isTestRot = false;
                }
                cylinderGeo->setTranslation(Vec3d(XX + 0.038 - 0.019 + 0.0006+0.0087, YY + 0.088 + 0.055 - 0.01810, ZZ + 0.226 - 0.0073+0.0036));
                //printf("Radius: %.5f Tran: %.5f  %.5f  %.5f,  Flag: %d, Rot: %.5f  %.5f  %.5f\n", rad,  XX, YY, ZZ, flagXZ, RXX, RZZ);
                auto vecLists = cylinderGeo->getVertexPositions();
                for (auto objInside : m_scene->getSceneObjects())
                {
                    if (objInside->getName() == "Sphere1d")
                    {
                        //objInside->getVisualGeometry()->setTranslation(vecLists[0]);
                        //printf("spheres: %f %f %f, ", vecLists[0].x(), vecLists[0].y(), vecLists[0].z());
                    }
                    else if (objInside->getName() == "Sphere2d")
                    {
                        //printf("%f %f %f\n", vecLists[1].x(), vecLists[1].y(), vecLists[1].z());
                        //objInside->getVisualGeometry()->setTranslation(vecLists[1]);
                    }
                }
            }
            else if (obj->getName()=="LineCylinder")
            {
                auto cylinderGeo = std::dynamic_pointer_cast<LineMesh>(obj->getVisualModels()[0]->getGeometry());
                cylinderGeo->setRotation(imstk::BACKWARD_VECTOR,  - 0.28798);
                cylinderGeo->setTranslation(Vec3d(0.038 - 0.019 + 0.0006 + 0.0087, 0.088 + 0.055 - 0.01810, 0.226 - 0.0073 + 0.0036));
            }
            else if (obj->getName() == "cubeDissect")
            {
                auto objPlane = std::dynamic_pointer_cast<Plane>(obj->getVisualGeometry());
                float XX = m_scene->getKeyBoardX();
                float YY = m_scene->getKeyBoardY();
                float ZZ = m_scene->getKeyBoardZ();             
                float rotupy = m_scene->getKeyBoardRot();
                float rotrightx = m_scene->getKeyBoardRotRX();
                float rotbackz = m_scene->getKeyBoardRotBZ();
                auto ptmp = obj->getVisualGeometry()->getTranslation(); //  Vec3d(2.7, 4.7, 4.3); // obj->getVisualGeometry()->getTranslation();
                //objPlane->rotate(imstk::UP_VECTOR, rot);
                if (rotupy>0.0) objPlane->setRotation(imstk::UP_VECTOR, rotupy);
                if (rotrightx>0.0) objPlane->setRotation(imstk::RIGHT_VECTOR, rotrightx);
                if (rotbackz>0.0) objPlane->setRotation(imstk::BACKWARD_VECTOR, rotbackz); 
                
                objPlane->setTranslation(Vec3d(XX + 0.03299, YY + 0.09499, ZZ + 0.195));
            }
            //else if (obj->getName() == "XZHSphere0")
            //{
            //    obj->getVisualGeometry()->setTranslation(m_xzhVisualMesh->getVertexPosition(39));
            //}
            //else if (obj->getName() == "DecalObject0")
            //{
            //    if (auto decalPool = std::dynamic_pointer_cast<DecalPool>(obj->getVisualGeometry()))
            //    {
            //        auto& decallist = decalPool->getDecals();   //->getVisualGeometry()->setTranslation(m_xzhVisualMesh->getVertexPosition(39));
            //        decallist.at(0)->setPosition(m_xzhVisualMesh->getVertexPosition(39));
            //        //decal1->setPosition(markingPos);
            //        ////    decal1->rotate(RIGHT_VECTOR, PI_4);
            //        ////    decal1->scale(0.6);
            //    }
            //    //auto decalPoolObj = std::dynamic_pointer_cast<VisualObject>(obj); //   <DecalPool>(obj);
            //    //auto decalPool = decalPoolObj->getVisualGeometry(); // std::shared_ptr<DecalPool>
            //    //auto& decallist=decalPool->getDecals();   //->getVisualGeometry()->setTranslation(m_xzhVisualMesh->getVertexPosition(39));
            //    //decallist.at(0)->setTranslation(m_xzhVisualMesh->getVertexPosition(39));
            //}
            else if (obj->getName() == "visualCutFace")
            {
                auto mesh = obj->getVisualGeometry();
            }
            else if (obj->getName()=="visualCuttingPath")
            {
                if (isNoCut)
                {
                    auto visualMesh = std::dynamic_pointer_cast<SurfaceMesh>(obj->getVisualGeometry());
                    auto& posLists = visualMesh->getVertexPositions();
                    for (int i = 0; i < posLists.size(); i++)
                    {
                        visualMesh->setVertexPosition(i, Vec3d(0, 0, 0)); // colon if is cube use  // visualMesh->setVertexPosition(i, vertexPos3 + Vec3d(-0.002, 0, 0));
                    }
                }
                
            }
            else if (obj->getName()=="cuttingSurfaceMove")
            {
                auto visualMesh = std::dynamic_pointer_cast<SurfaceMesh>(obj->getVisualGeometry());
                //auto& posLists = visualMesh->getVertexPositionsChangeable();
                Vec3d vertexPos3;
                int num = visualMesh->getNumVertices();
                for (int i = 0; i < num; i++)
                {
                    vertexPos3 = visualMesh->getVertexPosition(i);
                    visualMesh->setVertexPosition(i, vertexPos3 + Vec3d(0, 0, -0.005)); // colon if is cube use  // visualMesh->setVertexPosition(i, vertexPos3 + Vec3d(-0.002, 0, 0));
                }
                //for (int i = 0; i < posLists.size(); i++) 
                //{
                //    posLists[i] += Vec3d(-0.0002, 0, 0);
                //}
            }
            else if (obj->getName() == "visualColonMeshVolume")
            {
                for (auto obj2 : m_scene->getSceneObjects())
                {
                    break;
                    if (!hasCut) break;
                    //if (obj2->getName() == "Cube")  // Colon
                    if (obj2->getName() == "Colon")
                    {
                        auto pbdvcObj = std::dynamic_pointer_cast<PbdObject>(obj2);
                        m_xzhPhysicalMesh = std::dynamic_pointer_cast<VESSTetCutMesh>(pbdvcObj->getPhysicsGeometry());

                        //m_xzhPhysicalMesh->extractSurfaceMeshVolume(); //  getSurfaceMeshVolume();
                        m_xzhPhysicalMesh->extractSurfaceMesh();  // this influence the speed siganificantly
                        auto vMesh = m_xzhPhysicalMesh->getSurfaceMesh();  //->getSurfaceMeshVolume();
                        auto visualMesh = std::dynamic_pointer_cast<PointSet>(obj->getVisualGeometry());
                        obj->setVisualGeometry(visualMesh);

                        Vec3d vertexPos3;
                        for (int i = 0; i < visualMesh->getNumVertices(); ++i)
                        {
                            vertexPos3 = vMesh->getVertexPosition(i);
                            visualMesh->setVertexPosition(i, vertexPos3);
                        }
                        hasCut = false;
                    }
                }
            }
            else if (obj->getName() == "visualColonMesh4")  // wireframe mesh
            {
                continue;
                for (auto obj2 : m_scene->getSceneObjects())
                {
                    if (obj2->getName() == "Colon")
                    {
                        auto pbdvcObj = std::dynamic_pointer_cast<PbdObject>(obj2);
                        m_xzhPhysicalMesh = std::dynamic_pointer_cast<VESSTetCutMesh>(pbdvcObj->getPhysicsGeometry());
                    }
                }
                // XZH // for both sides surface of volume mesh
                //m_xzhPhysicalMesh->extractSurfaceMesh();
                //auto vMesh = m_xzhPhysicalMesh->getSurfaceMesh();
                ////obj->setVisualGeometry(m_xzhPhysicalMesh->getSurfaceMesh());

                // for only inside surface of volume mesh
                std::vector<Vec3d> centralLine;
                centralLine.push_back(Vec3d(-19.4, -5.85, 1.15));
                centralLine.push_back(Vec3d(-3.3, 2.4, 1.05));
                break;
                m_xzhPhysicalMesh->extractSurfaceMesh(centralLine); // <<<!!!!Most important !!!!>>> only inside colon of surface of volume mesh
                auto vMesh = m_xzhPhysicalMesh->getSurfaceMesh();  // <<<!!!!Most important !!!!>>> from test found that this part is the most time-consuming process XZH
                // XZH END 
                // TEST visual geometry
                Vec3d vertexPos3;
                auto visualMesh = std::dynamic_pointer_cast<PointSet>(obj->getVisualGeometry());
                for (int i = 0; i < visualMesh->getNumVertices(); ++i)
                {
                    vertexPos3 = vMesh->getVertexPosition(i);
                    visualMesh->setVertexPosition(i, vertexPos3);
                }
                // TEST END
            }
            else if (obj->getName() == "CutColonMeshBoundary")
            {
                auto visualMesh = std::dynamic_pointer_cast<SurfaceMesh>(obj->getVisualGeometry());
                for (auto obj2 : m_scene->getSceneObjects())
                {
                    if (obj2->getName() == "Colon")
                    {
                        auto pbdvcObj = std::dynamic_pointer_cast<PbdObject>(obj2);
                        m_xzhPhysicalMesh = std::dynamic_pointer_cast<VESSTetCutMesh>(pbdvcObj->getPhysicsGeometry());
                        auto vMeshPoses = m_xzhPhysicalMesh->getVertexPositions();  //->getSurfaceMeshVolume();
                        auto visualMesh = std::dynamic_pointer_cast<PointSet>(obj->getVisualGeometry());
                        visualMesh->setVertexPositions(vMeshPoses);
                    }
                }
            }
            else if (obj->getName() == "visualColonMesh5")  // inside colon mesh
            {
            }
            else if (obj->getName() == "visualNormal0")
            {
                // TEST visual geometry
                Vec3d vertexPos3 = Vec3d::Zero();
                auto visualMesh = std::dynamic_pointer_cast<PointSet>(obj->getVisualGeometry());
                visualMesh->setVertexPosition(0, tip);
                for (auto intPair : m_scene->getCollisionGraph()->getPbdPairList())
                {
                    vertexPos3 = intPair->getMoveDir();   //getCentralPos(); // get central of colliding vertices  //  intPair->getForce();  // force normal 
                }
                visualMesh->setVertexPosition(1, tip + vertexPos3 * 100); // 2 times of normal
                visualMesh->setVertexPosition(2, tip);
                // TEST END
            }
            else if (obj->getName() == "visualLineMesh")
            {
                auto visualMesh = std::dynamic_pointer_cast<PointSet>(obj->getVisualGeometry());
                visualMesh->setVertexPosition(0, tip);
                visualMesh->setVertexPosition(1, root);
                visualMesh->setVertexPosition(2, tip);
            }
        }
        // XZH END

        ////< XZH update Fluid test
        if(0)  // (!isPBD)
        {
            std::shared_ptr<VESSTetCutMesh> cutMesh;
            std::shared_ptr<PbdObject> cutObj;
            std::shared_ptr<SPHXZH> sphFluid;
            for (auto obj : m_scene->getSceneObjects())
            {
                if (obj->getName() == "Colon")
                {
                    cutObj = std::dynamic_pointer_cast<PbdObject>(obj);
                    cutMesh = std::dynamic_pointer_cast<VESSTetCutMesh>(cutObj->getPhysicsGeometry());
                    sphFluid = cutMesh->getSPHParticles();
                    sphFluid->updatePositions();
                }
            }
            std::vector<Vec3d> fluidPoses = sphFluid->getParticlePoses();
            char name[256];
            for (auto obj : m_scene->getSceneObjects())
            {
                for (int i = 0; i < fluidPoses.size(); i++)
                {
                    sprintf(name, "XZHFluidSphere%d", i);
                    if (obj->getName() == name)
                    {
                        obj->getVisualGeometry()->setTranslation(fluidPoses[i]);
                    }
                }

            }
        }
          ////< XZH Fluid test END

          ////< XZH Smoke and Spark, DecalPool
        shared_ptr<VisualModel> smokeVModel;
        shared_ptr<VisualModel> sparkVModel;
        shared_ptr<RenderParticles> smokeVGeo;
        shared_ptr<RenderParticles> sparkVGeo;
		//std::shared_ptr<VisualModel> decalVModel;
		//std::shared_ptr<DecalPool> decalPool;
		//std::deque<std::shared_ptr<Dec al>> decalList;
		//unsigned int decalNum = 0;
        for (auto obj : m_scene->getSceneObjects())
        {
            if (obj->getName() == "Smoke")
            {
                auto smoke = std::dynamic_pointer_cast<AnimationObject>(obj);
                smokeVModel = smoke->getVisualModel(0);
                smokeVGeo = std::dynamic_pointer_cast<RenderParticles>(smokeVModel->getGeometry());
                smokeCount++;
                if (smokeCount > 60) smokeVModel->hide();
            }
            if (obj->getName() == "Sparks")
            {
                auto spark = std::dynamic_pointer_cast<AnimationObject>(obj);
                sparkVModel = spark->getVisualModel(0);
                sparkVGeo = std::dynamic_pointer_cast<RenderParticles>(sparkVModel->getGeometry());
                sparkCount++;
                if (sparkCount > 60) sparkVModel->hide();
            }
			if (obj->getName() == "MarkingObject")
			{
				//auto markingObj = std::dynamic_pointer_cast<VisualObject>(obj);
				//decalVModel = markingObj->getVisualModel(0);
				//decalPool = decalVModel->getGeometry();
				//decalList = decalPool->getDecals();
				//for (int n=0;n<decalList.size();n++)
				//{
				//	auto ddecal = decalList[n]->getVisualIndex();
				//}
			}
        }

        // Do collision detection and response for pbd objects
        Vec3d vecInjectPos = Vec3d::Zero();
        Vec3d vecInjectPos1 = Vec3d::Zero();
        Vec3d vecInjectPos2 = Vec3d::Zero();
        Vec3d injectTouchPos = Vec3d::Zero();
        for (auto intPair : m_scene->getCollisionGraph()->getPbdPairList())
        {
            //break; // jump to cutting part
            intPair->setProcType(mProcType);
            intPair->resetConstraints();
            if (intPair->doBroadPhaseCollision())
            {
                if (mProcType==1)
                {
                    intPair->chooseProcedure(); // Injection solution || choose via Omni Button 0 intPair->doNarrowPhaseContinuousCollisionVESS(); // for static CD intPair->doNarrowPhaseCollisionVESS();  // for other intPair->doNarrowPhaseCollision();  // for VESS intPair->doNarrowPhaseCollisionVESS(); // intPair->doNarrowPhaseCollision();
                    vecInjectPos = intPair->getInjectPos();
                    vecInjectPos1 = intPair->getTrianglePos1();
                    vecInjectPos2 = intPair->getTrianglePos2();
                    injectTouchPos = intPair->getInjectTouchPos();
					if (visualMeshInjectPosesDiff.size()>0)
					{
						for (int nn = 0; nn < visualMeshInjectPosesDiff.size(); nn++)
						{
							if ( ((m_xzhVisualMeshVertPoses[nn] - visualMeshInjectPosesDiff[nn]).norm() >= 0.00007)  && (intPair->getInjectionState()) )  // 0.0001
							{
								m_xzhVisualMesh->setVertexColor(nn, imstk::Color(0.2, 0.5, 0.5, 0.5));
								//printf("dist inj  %.8f\n", (m_xzhVisualMeshVertPoses[nn] - visualMeshInjectPosesDiff[nn]).norm());
							}
						}
					}
					//auto& iList = intPair->getInjectList();
					//for (int nn=0;nn<iList.size();nn++)
					//{
					//	m_xzhVisualMesh->setVertexColor(iList[nn], imstk::Color(0.2, 0.5, 0.5, 0.5));
					//}
                }
                else if (mProcType==2)
                {
                    intPair->doNarrowPhaseContinuousCollisionVESS3();
                    ////< XZH SMOKE
					if (smokeVModel)
					{
						smokeVModel->show();
						smokeCount = 0;
					}
                    if (sparkVModel)
                    {
						sparkVModel->show();
						sparkCount = 0;
                    }
                }
                else if (mProcType == 0) // else if (mProcType == 0)
                {
                    intPair->doNarrowPhaseContinuousCollisionVESS3();   //  intPair->doNarrowPhaseContinuousCollisionVESS2();
                    // marking START
                    intPair->doMarking();
                    auto& markingState = intPair->getMarkState();
                    if (markingState)
                    {
                        //Vec3d markingPos = intPair->getMarkingPos();
                        Vec3d markingPos;
                        int markingIndex;
                        intPair->getMarkingPosIndex(markingPos, markingIndex);
                        printf("marking index and pos:%d, %f %f %f\n",markingIndex, markingPos.x(), markingPos.y(), markingPos.z());

                        numMarking++;
                        char name[256];
                        sprintf(name, "XZHSphere%d", numMarking);
                        auto visualSphere22 = std::make_shared<imstk::Sphere>();
                        auto visualSphere22Model = std::make_shared<VisualModel>(visualSphere22);
                        visualSphere22->setTranslation(markingPos);
                        visualSphere22->setRadius(0.2);
                        visualSphere22->setVisualIndex(markingIndex);
                        auto sphere2Obj = std::make_shared<SceneObject>(name);
                        sphere2Obj->addVisualModel(visualSphere22Model);
                        //m_scene->addSceneObject(sphere2Obj);

                        auto decalTest = std::make_shared<DecalPool>();
                        auto decalTestModel = std::make_shared<VisualModel>(decalTest);
                        auto decalMaterial = std::make_shared<RenderMaterial>();
                        //auto diffuseTexture = std::make_shared<Texture>("F:/assets/miscBloodDecalParticles/miscBloodDecalParticles/blood_particle_01.png", Texture::DIFFUSE);
                        auto diffuseTexture2 = std::make_shared<Texture>("I:/iMSTK/resources/textures/test5.png", Texture::Type::DIFFUSE);
                        decalMaterial->addTexture(diffuseTexture2);
                        auto decal1 = decalTest->addDecal();
                        decal1->setPosition(markingPos);
                        //decal1->rotate(RIGHT_VECTOR, PI_4);
                        decal1->scale(0.01*0.3); // 0.4
                        decalTestModel->setRenderMaterial(decalMaterial);
                        decal1->setVisualIndex(markingIndex);
                        sprintf(name, "DecalObject%d", numMarking);
                        auto decalObject = std::make_shared<VisualObject>(name); // "DecalObject"
                        decalObject->addVisualModel(decalTestModel);
                        m_scene->addSceneObject(decalObject);

                        markingState = false; 

                          ////< XZH SMOKE
						if (smokeVModel)
						{
							smokeVModel->show();
							smokeVGeo->setTranslation(markingPos);
							smokeCount = 0;
						}
                        if (sparkVModel)
                        {
							sparkVModel->show();
							sparkVGeo->setTranslation(markingPos);
							sparkCount = 0;
                        }     
                    }        // marking END
                }                
            }
            else  // no candidate collision
            {

            }
              ////< XZH static force feedback 08/12/2019

            // injection and CCD
            if (intPair->getInjectionState())
            {
                intPair->computeContactForceInjection();
            }
            else
            {
                intPair->computeContactForceContinuous();
            }
            //intPair->resolveContinuousCollisionVESS(); // for static CD intPair->resolveCollisionVESS();  // for other intPair->resolveCollision();  // for VESS intPair->resolveCollisionVESS();   // intPair->resolveCollision();
            centralPosition = intPair->getCentralPos(); // extra test
        }

        // Update velocity of PBD object  xzh
        // Update velocity of PBD objects
        for (auto obj : m_scene->getSceneObjects())
        {
            if (auto pbdObj = std::dynamic_pointer_cast<PbdObject>(obj))
            {
                if (auto pbdvcObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(obj))
                {
                    //pbdvcObj->resetCollidingGeometry();
                }
                else
                {
                    pbdObj->updateVelocity();
                }
            }
        }

        // Apply forces on device  change the place in this place XZH
        for (auto controller : m_scene->getSceneObjectControllers())
        {
            controller->applyForces();
        }

          ////< XZH apply force via XZH Tool
        for (auto obj : m_scene->getSceneObjects())
        {
            switch (mProcType)
            {
                case 0: // marking
                    if (obj->getName() == "VirtualObject1")
                    {
                        auto toolObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(obj);
                        toolObj->applyForces();
                    }
                    break;
                case 1: // injecting
                    if (obj->getName() == "VirtualObject2")
                    {
                        auto toolObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(obj);
                        toolObj->applyForces();
                    }
                    break;
                case 2: // cutting/dissecting 
                    if (obj->getName() == "VirtualObject")
                    {
                        auto toolObj = std::dynamic_pointer_cast<PbdVirtualCouplingObject>(obj);
                        toolObj->applyForces();
                    }
                    break;
                default:
                    break;
            } 
        }

        // Set the trackers of virtual coupling PBD objects to out-of-date
        for (auto obj : m_scene->getSceneObjects())
        {
            if (auto virtualCouplingPBD = std::dynamic_pointer_cast<VirtualCouplingPBDObject>(obj))
            {
                virtualCouplingPBD->setTrackerToOutOfDate();
            }
            // XZH TEST
            if (obj->getName() == "Sphere3")
            {
                obj->getVisualGeometry()->setTranslation(centralPosition);
                //printf("sphere3: %f %f %f %f\n", centralPosition.x(), centralPosition.y(), centralPosition.z(), centralPosition.norm());
            }
            else if (obj->getName() == "SphereInject")
            {
                //obj->getVisualGeometry()->setTranslation(vecInjectPos);
                obj->getVisualGeometry()->setTranslation(injectTouchPos);
            }
            else if (obj->getName() == "SphereInject1")
            {
                obj->getVisualGeometry()->setTranslation(vecInjectPos1);
            }
            else if (obj->getName() == "SphereInject2")
            {
                obj->getVisualGeometry()->setTranslation(vecInjectPos2);
            }
			else if (obj->getName()== "TetSphereObj")   ////< XZH shperes for tetrahedrons
			{
				//continue;
				auto vmodels = obj->getVisualModels();
				std::shared_ptr<VESSTetCutMesh> cutTetMesh;
				for (auto obj2 : m_scene->getSceneObjects())
				{
					if (obj2->getName() == "Colon")
					{
						auto cutObj = std::dynamic_pointer_cast<PbdObject>(obj2);
						cutTetMesh = std::dynamic_pointer_cast<VESSTetCutMesh>(cutObj->getPhysicsGeometry());
					}
				}
				if (cutTetMesh)
				{
					auto phyVetexList = cutTetMesh->getPhysVertex();
					for (int k=0;k<vmodels.size();k++)
					{
						auto vmesh = std::dynamic_pointer_cast<Sphere>(vmodels[k]->getGeometry());
						vmesh->setTranslation(phyVetexList[k].pos);
					}
				}
			}
			else if (obj->getName() == "surfaceCollidingObj")   ////< XZH for injecting surface deformation test
			{
				auto vmodels = obj->getVisualModels()[0];
				std::shared_ptr<VESSTetCutMesh> cutTetMesh;
				for (auto obj2 : m_scene->getSceneObjects())
				{
					if (obj2->getName() == "Colon")
					{
						auto cutObj = std::dynamic_pointer_cast<PbdObject>(obj2);
						cutTetMesh = std::dynamic_pointer_cast<VESSTetCutMesh>(cutObj->getPhysicsGeometry());
					}
				}
				if (cutTetMesh)
				{
					auto phyVetexList = cutTetMesh->getPhysVertex();
					auto vmesh = std::dynamic_pointer_cast<SurfaceMesh>(vmodels->getGeometry());
					auto& posLists = vmesh->getVertexPositionsChangeable();
					for (int n=0;n<posLists.size();n++)
					{
						posLists[n] = phyVetexList[n].pos;
					}
				}
			}
            // XZH TEST
        }

        // Set the trackers of the scene object controllers to out-of-date
        for (auto controller : m_scene->getSceneObjectControllers())
        {
            controller->setTrackerToOutOfDate();
        }

        //if (mProcType==2)
        //{
        //    ////< XZH Cutting start
        //    std::thread threadCut(&SceneManager::runCutting, this);
        //    threadCut.join(); //.detach(); // 
        //    ////< XZH Cutting END
        //}
        if ((mProcType == 0) || (mProcType == 1) || (mProcType == 3))   ////< XZH for injecting
        {
            std::shared_ptr<VESSTetCutMesh> cutTissueMesh;
            //using TetTriMap  for cube dense
            for (auto obj : m_scene->getSceneObjects())
            {
                if (obj->getName() == "Colon")
                {
                    auto pbdvcObj = std::dynamic_pointer_cast<PbdObject>(obj);
                    cutTissueMesh = std::dynamic_pointer_cast<VESSTetCutMesh>(pbdvcObj->getPhysicsGeometry());
                }
            }
            StdVectorOfVec3d posTetlist2;
            if (cutTissueMesh)
            {
                posTetlist2 = cutTissueMesh->getVertexPositions();
            }
            
            ////< XZH update the dissected new surface
            for (auto obj : m_scene->getSceneObjects())
            {
                if (obj->getName() == "SurfaceXZH")
                {
                    auto dissectVisual = std::dynamic_pointer_cast<SurfaceMesh>(obj->getVisualGeometry());
                    //auto& posVisualTmp = dissectVisual->getVertexPositionsChangeable();
                    //posVisualTmp = posTetlist2;
                    dissectVisual->setVertexPositions(posTetlist2);  // will make the dataModified as true
                    //printf("tri size of cut mesh: %d_________\n", dissectVisual->getTrianglesVertices().size());
                }
            }
        }
        if (mProcType == 2)
        {
			if (!isLoadDissect)   ////< XZH no loading dissect
			{
				std::thread threadDissection(&SceneManager::runDissection, this);
				threadDissection.join(); //.detach(); // 
			}
			else   
			{
				std::thread threadDissectionLoad(&SceneManager::runDissectionLoad, this);
				threadDissectionLoad.join(); //.detach(); // 
			}

            std::shared_ptr<VESSTetCutMesh> cutTissueMesh;
            //using TetTriMap  for cube dense
            for (auto obj : m_scene->getSceneObjects())
            {
                if (obj->getName() == "Colon")
                {
                    auto pbdvcObj = std::dynamic_pointer_cast<PbdObject>(obj);
                    cutTissueMesh = std::dynamic_pointer_cast<VESSTetCutMesh>(pbdvcObj->getPhysicsGeometry());
                }
            }
            auto posTetlist2 = cutTissueMesh->getVertexPositions();
            ////< XZH update the dissected new surface
            for (auto obj : m_scene->getSceneObjects())
            {
                if (obj->getName() == "SurfaceXZH")
                {
                    auto dissectVisual = std::dynamic_pointer_cast<SurfaceMesh>(obj->getVisualGeometry());
					//auto& posVisualTmp = dissectVisual->getVertexPositionsChangeable();
					//posVisualTmp = posTetlist2;
                    dissectVisual->setVertexPositions(posTetlist2);  // will make the dataModified as true
                    //printf("tri size of cut mesh: %d_________\n", dissectVisual->getTrianglesVertices().size());
                }
            }
        }

        //t2 = GetTickCount();

        //if (mProcType == 3)   ////< XZH for test output
        //{
        //    FILE *outPos = fopen("i:/outPosCubeZ.dat", "w");
        //    fprintf(outPos, "===============================\n\n");
        //    std::shared_ptr<VESSTetCutMesh> cutTissueMesh;
        //    //using TetTriMap  for cube dense
        //    for (auto obj : m_scene->getSceneObjects())
        //    {
        //        if (obj->getName() == "Colon")
        //        {
        //            auto pbdvcObj = std::dynamic_pointer_cast<PbdObject>(obj);
        //            cutTissueMesh = std::dynamic_pointer_cast<VESSTetCutMesh>(pbdvcObj->getPhysicsGeometry());
        //        }
        //    }
        //    auto posTetlist2 = cutTissueMesh->getVertexPositions();
        //    for (int i = 0; i < posTetlist2.size(); i++)
        //    {
        //        fprintf(outPos, "%d %f %f %f\n", i, posTetlist2[i].x(), posTetlist2[i].y(), posTetlist2[i].z());
        //    }
        //    fclose(outPos);
        //      ////< XZH update the dissected new surface
        //    for (auto obj : m_scene->getSceneObjects())
        //    {
        //        if (obj->getName() == "SurfaceXZH")
        //        {
        //            auto dissectVisual = std::dynamic_pointer_cast<SurfaceMesh>(obj->getVisualGeometry());
        //            dissectVisual->setVertexPositions(posTetlist2);
        //        }
        //    }
        //}
        //printf("scene body  time %d ms,  frame rate is: %f\n", t2 - t4, 1000 / double(t2 - t4));
        auto timeElapsed = wwt.getTimeElapsed(StopWatch::TimeUnitType::seconds);

        // Update time step size of the dynamic objects
        for (auto obj : m_scene->getSceneObjects())
        {
            if (obj->getType() == SceneObject::Type::Pbd)
            {
                if (auto dynaObj = std::dynamic_pointer_cast<PbdObject>(obj))
                {
                    if (dynaObj->getDynamicalModel()->getTimeStepSizeType() == TimeSteppingType::realTime)
                    {
                        dynaObj->getDynamicalModel()->setTimeStep(timeElapsed);
                    }
                }
            }
            else if (obj->getType() == SceneObject::Type::FEMDeformable)
            {
                if (auto dynaObj = std::dynamic_pointer_cast<DeformableObject>(obj))
                {
                    if (dynaObj->getDynamicalModel()->getTimeStepSizeType() == TimeSteppingType::realTime)
                    {
                        dynaObj->getDynamicalModel()->setTimeStep(timeElapsed);
                    }
                }
            }
        }

		  ////< XZH update injecting poses 20191101
        if (m_xzhVisualMesh)
        {
            visualMeshInjectPosesDiff = m_xzhVisualMesh->getVertexPositionsChangeable();
        }
    }

    void
        SceneManager::cleanUpModule()
    {
        // End Camera Controller
        if (auto camController = m_scene->getCamera()->getController())
        {
            camController->end();
            m_threadMap.at(camController->getName()).join();
        }
    }
    void
        SceneManager::startModuleInNewThread(std::shared_ptr<Module> module)
    {
        m_threadMap[module->getName()] = std::thread([module] { module->start(); });
    }

	bool 
		SceneManager::cutStrLineBySeparator(string strLine, char separator, vector<string>&strBufferLineVec)
	{
		vector<string>::size_type startPos = 0;
		vector<string>::size_type endPos = 0;

		endPos = strLine.find_first_of(separator, startPos);
		while (endPos != string::npos)
		{
			strBufferLineVec.push_back(strLine.substr(startPos, endPos - startPos));
			startPos = strLine.find_first_not_of(separator, endPos + 1);
			endPos = strLine.find_first_of(separator, startPos);
		}
		strBufferLineVec.push_back(strLine.substr(startPos, endPos));
		return true;
	}
} // imstk
