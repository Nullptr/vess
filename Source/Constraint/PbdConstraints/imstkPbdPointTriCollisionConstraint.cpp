/*=========================================================================

   Library: iMSTK

   Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
   & Imaging in Medicine, Rensselaer Polytechnic Institute.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0.txt

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

=========================================================================*/

#include "imstkPbdPointTriCollisionConstraint.h"
#include "g3log/g3log.hpp"

namespace imstk
{
void
PbdPointTriangleConstraint::initConstraint(std::shared_ptr<PbdModel> model1, const size_t& pIdx1,
                                           std::shared_ptr<PbdModel> model2, const size_t& pIdx2,
                                           const size_t& pIdx3, const size_t& pIdx4)
{
    m_model1 = model1;
    m_model2 = model2;
    m_bodiesFirst[0] = pIdx1;
    m_bodiesSecond[0] = pIdx2;
    m_bodiesSecond[1] = pIdx3;
    m_bodiesSecond[2] = pIdx4;
}

bool
PbdPointTriangleConstraint::solvePositionConstraint()
{
    const auto i0 = m_bodiesFirst[0];
    const auto i1 = m_bodiesSecond[0];
    const auto i2 = m_bodiesSecond[1];
    const auto i3 = m_bodiesSecond[2];

    auto state1 = m_model1->getCurrentState();
    auto state2 = m_model2->getCurrentState();

    Vec3d& x0 = state1->getVertexPosition(i0);
    Vec3d& x1 = state2->getVertexPosition(i1);
    Vec3d& x2 = state2->getVertexPosition(i2);
    Vec3d& x3 = state2->getVertexPosition(i3);

    Vec3d x12 = x2 - x1;
    Vec3d x13 = x3 - x1;
    Vec3d n = x12.cross(x13);
    Vec3d x01 = x0 - x1;

    double alpha = n.dot(x12.cross(x01))/ (n.dot(n));
    double beta  = n.dot(x01.cross(x13))/ (n.dot(n));

    if (alpha < 0 || beta < 0 || alpha + beta > 1 )
    {
        //LOG(WARNING) << "Projection point not inside the triangle";
        return false;
    }

    const double dist = m_model1->getProximity() + m_model2->getProximity();

    n.normalize();

    double l = x01.dot(n);

    if (l > dist)
    {
        return false;
    }

    double gamma = 1.0 - alpha - beta;
    Vec3d grad0 = n;
    Vec3d grad1 = -alpha*n;
    Vec3d grad2 = -beta*n;
    Vec3d grad3 = -gamma*n;

    const auto im0 = m_model1->getInvMass(i0);
    const auto im1 = m_model2->getInvMass(i1);
    const auto im2 = m_model2->getInvMass(i2);
    const auto im3 = m_model2->getInvMass(i3);

    double lambda = im0*grad0.squaredNorm() +
                    im1*grad1.squaredNorm() +
                    im2*grad2.squaredNorm() +
                    im3*grad3.squaredNorm();

    lambda = (l - dist)/lambda;

    //LOG(INFO) << "Lambda:" << lambda <<" Normal:" << n[0] <<" " << n[1] <<" "<<n[2];

    if (im0 > 0)
    {
        x0 += -im0*lambda*grad0*m_model1->getContactStiffness();
    }

    if (im1 > 0)
    {
        x1 += -im1*lambda*grad1*m_model2->getContactStiffness();
    }

    if (im2 > 0)
    {
        x2 += -im2*lambda*grad2*m_model2->getContactStiffness();
    }

    if (im3 > 0)
    {
        x3 += -im3*lambda*grad3*m_model2->getContactStiffness();
    }

    return true;
}

//bool
//PbdPointTriangleConstraint::solvePositionConstraint(Vec3d& force)
//{
//    const auto i0 = m_bodiesFirst[0];
//    const auto i1 = m_bodiesSecond[0];
//    const auto i2 = m_bodiesSecond[1];
//    const auto i3 = m_bodiesSecond[2];
//
//    auto state1 = m_model1->getCurrentState();
//    auto state2 = m_model2->getCurrentState();
//
//    Vec3d& p = state1->getVertexPosition(i0);
//    Vec3d& p0 = state2->getVertexPosition(i1);
//    Vec3d& p1 = state2->getVertexPosition(i2);
//    Vec3d& p2 = state2->getVertexPosition(i3);
//    // find barycentric coordinates of closest point on triangle
//
//    double b0 = 1.0 / 3.0;		// for singular case
//    double b1 = b0;
//    double b2 = b0;
//
//    Vec3d d1 = p1 - p0;
//    Vec3d d2 = p2 - p0;
//    Vec3d pp0 = p - p0;
//    double a = d1.dot(d1);
//    double b = d2.dot(d1);
//    double c = pp0.dot(d1);
//    double d = b;
//    double e = d2.dot(d2);
//    double f = pp0.dot(d2);
//    double det = a*e - b*d;
//
//    if (det != 0.0) {
//        double s = (c*e - b*f) / det;
//        double t = (a*f - c*d) / det;
//        b0 = 1.0 - s - t;		// inside triangle
//        b1 = s;
//        b2 = t;
//        if (b0 < 0.0) {		// on edge 1-2
//            Vec3d d = p2 - p1;
//            double d2 = d.dot(d);
//            double t = (d2 == 0.0) ? 0.5 : d.dot(p - p1) / d2;
//            if (t < 0.0) t = 0.0;	// on point 1
//            if (t > 1.0) t = 1.0;	// on point 2
//            b0 = 0.0;
//            b1 = (1.0 - t);
//            b2 = t;
//        }
//        else if (b1 < 0.0) {	// on edge 2-0
//            Vec3d d = p0 - p2;
//            double d2 = d.dot(d);
//            double t = (d2 == 0.0) ? 0.5 : d.dot(p - p2) / d2;
//            if (t < 0.0) t = 0.0;	// on point 2
//            if (t > 1.0) t = 1.0; // on point 0
//            b1 = 0.0;
//            b2 = (1.0 - t);
//            b0 = t;
//        }
//        else if (b2 < 0.0) {	// on edge 0-1
//            Vec3d d = p1 - p0;
//            double d2 = d.dot(d);
//            double t = (d2 == 0.0) ? 0.5 : d.dot(p - p0) / d2;
//            if (t < 0.0) t = 0.0;	// on point 0
//            if (t > 1.0) t = 1.0;	// on point 1
//            b2 = 0.0;
//            b0 = (1.0 - t);
//            b1 = t;
//        }
//    }
//    Vec3d q = p0 * b0 + p1 * b1 + p2 * b2;
//    Vec3d n = p - q;
//    double dist = n.norm();
//    n.normalize();
//    double C = dist - (m_model1->getProximity() + m_model2->getProximity());
//    if (C > 0)
//    {
//        return false;
//    }
//    Vec3d grad = n;
//    Vec3d grad0 = -n * b0;
//    Vec3d grad1 = -n * b1;
//    Vec3d grad2 = -n * b2;
//
//    const auto im0 = m_model1->getInvMass(i0);
//    const auto im1 = m_model2->getInvMass(i1);
//    const auto im2 = m_model2->getInvMass(i2);
//    const auto im3 = m_model2->getInvMass(i3);
//
//    double s = im0 + im1 * b0*b0 + im2 * b1*b1 + im3 * b2*b2;
//    if (s == 0.0)
//        return false;
//
//    force = -C*n;
//
//    s = C / s;
//    FILE *outtest = fopen("G:/testNewCD.txt", "a");
//    fprintf(outtest, "Tri Point   S=%f, n: %f %f %f, b: %f %f %f, im %f %f %f %f\n", s, n.x(), n.y(), n.z(), b0, b1, b2, im0, im1, im2, im3);
//    
//    if (im0 > 0)
//    {
//        p += -im0*s*grad*m_model1->getContactStiffness();
//        Vec3d tmp = -im0*s*grad*m_model1->getContactStiffness();
//        fprintf(outtest, "im0: %f %f %f\n", tmp.x(), tmp.y(), tmp.z());
//    }
//
//    if (im1 > 0)
//    {
//        p0 += -im1*s*grad0*m_model2->getContactStiffness();
//        Vec3d tmp = -im1*s*grad0*m_model2->getContactStiffness();
//        fprintf(outtest, "im1: %f %f %f\n", tmp.x(), tmp.y(), tmp.z());
//    }
//
//    if (im2 > 0)
//    {
//        p1 += -im2*s*grad1*m_model2->getContactStiffness();
//        Vec3d tmp = -im2*s*grad1*m_model2->getContactStiffness();
//        fprintf(outtest, "im2: %f %f %f\n", tmp.x(), tmp.y(), tmp.z());
//    }
//
//    if (im3 > 0)
//    {
//        p2 += -im3*s*grad2*m_model2->getContactStiffness();
//        Vec3d tmp = -im3*s*grad2*m_model2->getContactStiffness();
//        fprintf(outtest, "im3: %f %f %f\n", tmp.x(), tmp.y(), tmp.z());
//    }
//    fclose(outtest);
//    return true;
//}

bool
PbdPointTriangleConstraint::solvePositionConstraint(Vec3d& force)
{
    const auto i0 = m_bodiesFirst[0];
    const auto i1 = m_bodiesSecond[0];
    const auto i2 = m_bodiesSecond[1];
    const auto i3 = m_bodiesSecond[2];

    auto state1 = m_model1->getCurrentState();
    auto state2 = m_model2->getCurrentState();

    Vec3d& x0 = state1->getVertexPosition(i0);
    Vec3d& x1 = state2->getVertexPosition(i1);
    Vec3d& x2 = state2->getVertexPosition(i2);
    Vec3d& x3 = state2->getVertexPosition(i3);

    Vec3d x12 = x2 - x1;
    Vec3d x13 = x3 - x1;
    Vec3d n = x12.cross(x13);
    Vec3d x01 = x0 - x1;

    double alpha = n.dot(x12.cross(x01)) / (n.dot(n));
    double beta = n.dot(x01.cross(x13)) / (n.dot(n));

    if (alpha < 0 || beta < 0 || alpha + beta > 1)
    {
        //LOG(WARNING) << "Projection point not inside the triangle";
        return false;
    }

    const double dist = m_model1->getProximity() + m_model2->getProximity();

    n.normalize();

    double l = x01.dot(n);

    if (l > dist)
    {
        return false;
    }

    double gamma = 1.0 - alpha - beta;
    Vec3d grad0 = n;
    Vec3d grad1 = -alpha*n;
    Vec3d grad2 = -beta*n;
    Vec3d grad3 = -gamma*n;

    const auto im0 = m_model1->getInvMass(i0);
    const auto im1 = m_model2->getInvMass(i1);
    const auto im2 = m_model2->getInvMass(i2);
    const auto im3 = m_model2->getInvMass(i3);

    double lambda = im0*grad0.squaredNorm() +
        im1*grad1.squaredNorm() +
        im2*grad2.squaredNorm() +
        im3*grad3.squaredNorm();
    //printf("Tri Point lambda %f, im: %f %f %f %f \n", lambda, im0*grad0.squaredNorm(), im1*grad1.squaredNorm(), im2*grad2.squaredNorm(), im3*grad3.squaredNorm());
    Vec3d temp;
    if (lambda >= 0.000001)  // !=0
    {
        force+= -(l - dist)*lambda*grad0;
        temp = -(l - dist)*lambda*grad0;
        //printf("force PT: %f %f %f %f\n", temp.x(), temp.y(), temp.z(), temp.norm());
    }
    lambda = (l - dist) / lambda; // if im1=im2=im3=0 is fixed point, lambda=-1.#INF00
    
    //    LOG(INFO) << "Lambda:" << lambda <<" Normal:" << n[0] <<" " << n[1] <<" "<<n[2];
    //force = -(l - dist)*n; // original XZH force = -(l - dist)*n; // (l - dist) is penetrationDepth, n is direction
    //printf("force Tri Point: %f %f %f lambda %f \n", force.x(), force.y(), force.z(), lambda);
    if (im0 > 0)
    {
        x0 += -im0*lambda*grad0*m_model1->getContactStiffness();
    }

    if (im1 > 0)
    {
        x1 += -im1*lambda*grad1*m_model2->getContactStiffness();
    }

    if (im2 > 0)
    {
        x2 += -im2*lambda*grad2*m_model2->getContactStiffness();
    }

    if (im3 > 0)
    {
        x3 += -im3*lambda*grad3*m_model2->getContactStiffness();
    }

    return true;
}

} // imstk
