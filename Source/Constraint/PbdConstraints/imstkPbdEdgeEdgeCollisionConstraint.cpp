/*=========================================================================

   Library: iMSTK

   Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
   & Imaging in Medicine, Rensselaer Polytechnic Institute.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0.txt

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

=========================================================================*/

#include "imstkPbdEdgeEdgeCollisionConstraint.h"
#include "g3log/g3log.hpp"

namespace imstk
{
void
PbdEdgeEdgeConstraint::initConstraint(std::shared_ptr<PbdModel> model1,
                                      const size_t& pIdx1, const size_t& pIdx2,
                                      std::shared_ptr<PbdModel> model2,
                                      const size_t& pIdx3, const size_t& pIdx4)
{
    m_model1 = model1;
    m_model2 = model2;
    m_bodiesFirst[0] = pIdx1;
    m_bodiesFirst[1] = pIdx2;
    m_bodiesSecond[0] = pIdx3;
    m_bodiesSecond[1] = pIdx4;
}

bool
PbdEdgeEdgeConstraint::solvePositionConstraint()
{
    const auto i0 = m_bodiesFirst[0];
    const auto i1 = m_bodiesFirst[1];
    const auto i2 = m_bodiesSecond[0];
    const auto i3 = m_bodiesSecond[1];

    auto state1 = m_model1->getCurrentState();
    auto state2 = m_model2->getCurrentState();

    Vec3d& x0 = state1->getVertexPosition(i0);
    Vec3d& x1 = state1->getVertexPosition(i1);
    Vec3d& x2 = state2->getVertexPosition(i2);
    Vec3d& x3 = state2->getVertexPosition(i3);

    auto a = (x3-x2).dot(x1-x0);
    auto b = (x1 - x0).dot(x1 - x0);
    auto c = (x0 - x2).dot(x1 - x0);
    auto d = (x3 - x2).dot(x3 - x2);
    auto e = a;
    auto f = (x0 - x2).dot(x3 - x2);

    auto det = a*e - d*b;
    double s = 0.5;
    double t = 0.5;
    if ( fabs(det) > 1e-12 )
    {
        s = (c*e - b*f)/det;
        t = (c*d - a*f)/det;
        if (s < 0 || s > 1.0 || t < 0 || t > 1.0)
        {
            return false;
        }
    }
    else
    {
        //LOG(WARNING) << "det is null";
    }

    Vec3d P = x0 + t*(x1-x0);
    Vec3d Q = x2 + s*(x3-x2);

    Vec3d n = Q - P;
    auto l = n.norm();
    n /= l;

    const auto dist = m_model1->getProximity() + m_model2->getProximity();

    if (l > dist)
    {
        return false;
    }

    Vec3d grad0 = -(1-t)*n;
    Vec3d grad1 = -(t)*n;
    Vec3d grad2 = (1-s)*n;
    Vec3d grad3 = (s)*n;

    const auto im0 = m_model1->getInvMass(i0);
    const auto im1 = m_model1->getInvMass(i1);
    const auto im2 = m_model2->getInvMass(i2);
    const auto im3 = m_model2->getInvMass(i3);

    auto lambda = im0*grad0.squaredNorm() +
                  im1*grad1.squaredNorm() +
                  im2*grad2.squaredNorm() +
                  im3*grad3.squaredNorm();

    lambda = (l - dist)/lambda;

//    LOG(INFO) << "Lambda:" << lambda <<" Normal:" << n[0] <<" " << n[1] <<" "<<n[2];

    if (im0 > 0)
    {
        x0 += -im0*lambda*grad0*m_model1->getContactStiffness();
    }

    if (im1 > 0)
    {
        x1 += -im1*lambda*grad1*m_model1->getContactStiffness();
    }

    if (im2 > 0)
    {
        x2 += -im2*lambda*grad2*m_model2->getContactStiffness();
    }

    if (im3 > 0)
    {
        x3 += -im3*lambda*grad3*m_model2->getContactStiffness();
    }

    return true;
}

//bool
//PbdEdgeEdgeConstraint::solvePositionConstraint(Vec3d& force)
//{
//    const auto i0 = m_bodiesFirst[0];
//    const auto i1 = m_bodiesFirst[1];
//    const auto i2 = m_bodiesSecond[0];
//    const auto i3 = m_bodiesSecond[1];
//
//    auto state1 = m_model1->getCurrentState();
//    auto state2 = m_model2->getCurrentState();
//
//    Vec3d& p0 = state1->getVertexPosition(i0);
//    Vec3d& p1 = state1->getVertexPosition(i1);
//    Vec3d& p2 = state2->getVertexPosition(i2);
//    Vec3d& p3 = state2->getVertexPosition(i3);
//    Vec3d d0 = p1 - p0;
//    Vec3d d1 = p3 - p2;
//
//    double a = d0.squaredNorm();
//    double b = -d0.dot(d1);
//    double c = d0.dot(d1);
//    double d = -d1.squaredNorm();
//    double e = (p2 - p0).dot(d0);
//    double f = (p2 - p0).dot(d1);
//    double det = a*d - b*c;
//    double s, t;
//    if (det != 0.0) {
//        det = 1.0 / det;
//        s = (e*d - b*f) * det;
//        t = (a*f - e*c) * det;
//    }
//    else {	// d0 and d1 parallel
//        double s0 = p0.dot(d0);
//        double s1 = p1.dot(d0);
//        double t0 = p2.dot(d0);
//        double t1 = p3.dot(d0);
//        bool flip0 = false;
//        bool flip1 = false;
//
//        if (s0 > s1) { double f = s0; s0 = s1; s1 = f; flip0 = true; }
//        if (t0 > t1) { double f = t0; t0 = t1; t1 = f; flip1 = true; }
//
//        if (s0 >= t1) {
//            s = !flip0 ? 0.0 : 1.0;
//            t = !flip1 ? 1.0 : 0.0;
//        }
//        else if (t0 >= s1) {
//            s = !flip0 ? 1.0 : 0.0;
//            t = !flip1 ? 0.0 : 1.0;
//        }
//        else {		// overlap
//            double mid = (s0 > t0) ? (s0 + t1) * 0.5 : (t0 + s1) * 0.5;
//            s = (s0 == s1) ? 0.5 : (mid - s0) / (s1 - s0);
//            t = (t0 == t1) ? 0.5 : (mid - t0) / (t1 - t0);
//        }
//    }
//    if (s < 0.0) s = 0.0;
//    if (s > 1.0) s = 1.0;
//    if (t < 0.0) t = 0.0;
//    if (t > 1.0) t = 1.0;
//
//    double b0 = 1.0 - s;
//    double b1 = s;
//    double b2 = 1.0 - t;
//    double b3 = t;
//
//    Vec3d q0 = p0 * b0 + p1 * b1;
//    Vec3d q1 = p2 * b2 + p3 * b3;
//    Vec3d n = q0 - q1;
//    double dist = n.norm();
//    n.normalize();
//    double C = dist - (m_model1->getProximity() + m_model2->getProximity());
//    if (C > 0)
//    {
//        return false;
//    }
//    Vec3d grad0 = n * b0;
//    Vec3d grad1 = n * b1;
//    Vec3d grad2 = -n * b2;
//    Vec3d grad3 = -n * b3;
//
//    const auto im0 = m_model1->getInvMass(i0);
//    const auto im1 = m_model1->getInvMass(i1);
//    const auto im2 = m_model2->getInvMass(i2);
//    const auto im3 = m_model2->getInvMass(i3);
//
//    s = im0 * b0*b0 + im1 * b1*b1 + im2 * b2*b2 + im3 * b3*b3;
//    if (s == 0.0)
//        return false;
//
//    force = -C*n;
//
//    s = C / s;
//    FILE *outtest = fopen("G:/testNewCD.txt", "a");
//    fprintf(outtest, "Edge Edge S=%f, n: %f %f %f, b: %f %f %f %f, im %f %f %f %f\n", s, n.x(), n.y(), n.z(), b0, b1, b2, b3, im0, im1, im2, im3);
//    
//
//    if (im0 > 0)
//    {
//        p0 += -im0*s*grad0*m_model1->getContactStiffness();
//        Vec3d tmp = -im0*s*grad0*m_model1->getContactStiffness();
//        fprintf(outtest, "im0: %f %f %f\n", tmp.x(), tmp.y(), tmp.z());
//    }
//
//    if (im1 > 0)
//    {
//        p1 += -im1*s*grad1*m_model1->getContactStiffness();
//        Vec3d tmp = -im1*s*grad1*m_model1->getContactStiffness();
//        fprintf(outtest, "im1: %f %f %f\n", tmp.x(), tmp.y(), tmp.z());
//    }
//
//    if (im2 > 0)
//    {
//        p2 += -im2*s*grad2*m_model2->getContactStiffness();
//        Vec3d tmp = -im2*s*grad2*m_model2->getContactStiffness();
//        fprintf(outtest, "im2: %f %f %f\n", tmp.x(), tmp.y(), tmp.z());
//    }
//
//    if (im3 > 0)
//    {
//        p3 += -im3*s*grad3*m_model2->getContactStiffness();
//        Vec3d tmp = -im3*s*grad3*m_model2->getContactStiffness();
//        fprintf(outtest, "im3: %f %f %f\n", tmp.x(), tmp.y(), tmp.z());
//    }
//    fclose(outtest);
//    return true;
//}

bool
PbdEdgeEdgeConstraint::solvePositionConstraint(Vec3d& force)
{
    const auto i0 = m_bodiesFirst[0];
    const auto i1 = m_bodiesFirst[1];
    const auto i2 = m_bodiesSecond[0];
    const auto i3 = m_bodiesSecond[1];

    auto state1 = m_model1->getCurrentState();
    auto state2 = m_model2->getCurrentState();

    Vec3d& x0 = state1->getVertexPosition(i0);
    Vec3d& x1 = state1->getVertexPosition(i1);
    Vec3d& x2 = state2->getVertexPosition(i2);
    Vec3d& x3 = state2->getVertexPosition(i3);

    auto a = (x3 - x2).dot(x1 - x0);
    auto b = (x1 - x0).dot(x1 - x0);
    auto c = (x0 - x2).dot(x1 - x0);
    auto d = (x3 - x2).dot(x3 - x2);
    auto e = a;
    auto f = (x0 - x2).dot(x3 - x2);

    auto det = a*e - d*b;
    double s = 0.5;
    double t = 0.5;
    if (fabs(det) > 1e-12)
    {
        s = (c*e - b*f) / det;
        t = (c*d - a*f) / det;
        if (s < 0 || s > 1.0 || t < 0 || t > 1.0)
        {
            return false;
        }
    }
    else
    {
        //LOG(WARNING) << "det is null";
    }

    Vec3d P = x0 + t*(x1 - x0);
    Vec3d Q = x2 + s*(x3 - x2);

    Vec3d n = Q - P;
    auto l = n.norm();
    n /= l;

    const auto dist = m_model1->getProximity() + m_model2->getProximity();

    if (l > dist)
    {
        return false;
    }

    Vec3d grad0 = -(1 - t)*n;
    Vec3d grad1 = -(t)*n;
    Vec3d grad2 = (1 - s)*n;
    Vec3d grad3 = (s)*n;

    const auto im0 = m_model1->getInvMass(i0);
    const auto im1 = m_model1->getInvMass(i1);
    const auto im2 = m_model2->getInvMass(i2);
    const auto im3 = m_model2->getInvMass(i3);

    auto lambda = im0*grad0.squaredNorm() +   // squaredNorm is equal to dot(grad0, grad0) 
        im1*grad1.squaredNorm() +
        im2*grad2.squaredNorm() +
        im3*grad3.squaredNorm();
    //printf("Edge Edge lambda %f, im: %f %f %f %f \n", lambda, im0*grad0.squaredNorm(), im1*grad1.squaredNorm(), im2*grad2.squaredNorm(), im3*grad3.squaredNorm());
    if (lambda >= 0.000001)  // !=0
    {
        // XZH centrial line of colon among volumetric mesh
        //Vec3d p0 = Vec3d(-19.4, -5.85, 1.15);   // for colon 
        //Vec3d p1 =Vec3d(-3.3, 2.4, 1.05);  
        Vec3d p0 = Vec3d(-19.4, -5, 1.15);   // for rect Test
        Vec3d p1 = Vec3d(13.3, -5, 1.15);
        Vec3d a = Q - p0;
        Vec3d b = p1 - p0;
        Vec3d c = ((a.dot(b)) / (b.dot(b)))*b;
        Vec3d e = a - c;        
        Vec3d temp;
        if ((-n).dot(-e) >= 0)  // Q->P normal indicating the central line of inside colon should be positive
        {
            force += -(l - dist)*(lambda*grad0 + lambda*grad1) / 2;
            temp = -(l - dist)*(lambda*grad0 + lambda*grad1) / 2;
        }
        else  // no cross colon
        {
            force += (l - dist)*(lambda*grad0 + lambda*grad1) / 2;
            temp = (l - dist)*(lambda*grad0 + lambda*grad1) / 2;
        }
        //printf("force EE: %f %f %f %f\n", temp.x(), temp.y(), temp.z(), temp.norm());
        // XZN END
    }
    lambda = (l - dist) / lambda; // if im2=im3=0 is fixed point, lambda=-1.#INF00

    //    LOG(INFO) << "Lambda:" << lambda <<" Normal:" << n[0] <<" " << n[1] <<" "<<n[2];
    //if (n.dot(Vec3d(0,1,0))>=0)
    //{
    //}
    //force = (l - dist)*n; // original XZH force = (l - dist)*n; // (l - dist) is penetrationDepth, n is direction
    //printf("force Edge Edge: %f %f %f  lambda %f \n", force.x(), force.y(), force.z(), lambda);
    if (im0 > 0)
    {
        x0 += -im0*lambda*grad0*m_model1->getContactStiffness();
    }

    if (im1 > 0)
    {
        x1 += -im1*lambda*grad1*m_model1->getContactStiffness();
    }

    if (im2 > 0)
    {
        x2 += -im2*lambda*grad2*m_model2->getContactStiffness();
    }

    if (im3 > 0)
    {
        x3 += -im3*lambda*grad3*m_model2->getContactStiffness();
    }

    return true;
}

} // imstk
