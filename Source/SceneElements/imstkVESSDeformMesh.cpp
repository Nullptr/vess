﻿/*=========================================================================

Library: iMSTK

Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
& Imaging in Medicine, Rensselaer Polytechnic Institute.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0.txt

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

=========================================================================*/

#include "imstkVESSDeformMesh.h"

namespace imstk
{
    void VESSDeformMesh::init() 
    { // std::shared_ptr<VESSTetrahedralMesh> tetMesh) { //  std::vector<physVertex>& physVertices, std::vector<physSurfTris>& physSTris) {
        //V = mesh.V;
        //F = mesh.F;
        //W = mesh.W;
        constraints.clear(); //  = {};
        nListLev0.clear();
        nListLev1.clear();
        nListLev2.clear();
        nListLev3.clear();
        nListLev4.clear();
        //// XZH       //  surface triangle just inside surface of tet, but physVertices are the whole tet
        //setDeformMesh(tetMesh);
        //auto& physVertices = tetMesh->getPhysVertex();
        //auto& physEdge = tetMesh->getPhysEdge();
        //auto& physSTris = tetMesh->getPhysSurfTris();
        //int num = 0;
        //for (int k = 0; k < physVertices.size(); k++)
        //{
        //    auto& vet = physVertices[k];
        //    if (vet.onSurf) num++;
        //}

        //F.resize(3, physSTris.size());      
        //int num2 = 0;
        //std::vector<physSurfTris> physSTrisTemp = physSTris;  // not reference ,so will not change the original value

        //// using iMSTK method
        //localToGlobalMappingVertex.clear();
        //// Renumber the vertices
        //std::list<int> uniqueVertIdList;
        //for (const auto &face : physSTrisTemp)
        //{
        //    uniqueVertIdList.push_back(face.nodeIdx[0]);
        //    uniqueVertIdList.push_back(face.nodeIdx[1]);
        //    uniqueVertIdList.push_back(face.nodeIdx[2]);
        //}
        //uniqueVertIdList.sort();
        //uniqueVertIdList.unique();

        //int vertId;
        //std::list<int>::iterator it;
        //StdVectorOfVec3d vertPositions;
        //for (vertId = 0, it = uniqueVertIdList.begin(); it != uniqueVertIdList.end(); ++vertId, it++)
        //{
        //    vertPositions.push_back(physVertices[*it].pos); // coords
        //    localToGlobalMappingVertex.insert(std::make_pair(vertId, *it));  // mapping between local and global
        //    for (auto &face : physSTrisTemp)  // reference, traverse all the physSTris to check whether this vertex global index number == *it (global index)
        //    {
        //        for (int i = 0; i < 3; ++i)
        //        {
        //            if (face.nodeIdx[i] == *it)
        //            {
        //                face.nodeIdx[i] = vertId; // change the global index to local index 
        //            }
        //        }
        //    }
        //} // END for std::list

        //for (int k = 0; k < physSTrisTemp.size(); k++)
        //{
        //    auto& tris = physSTrisTemp[k];
        //    F(0, k) = tris.nodeIdx[0];
        //    F(1, k) = tris.nodeIdx[1];
        //    F(2, k) = tris.nodeIdx[2];
        //}

        //num2 = localToGlobalMappingVertex.size();
        //V.resize(3, uniqueVertIdList.size());
        //for (int k = 0; k < vertPositions.size();k++)
        //{
        //    //F(0, k) = vertPositions[k].x();
        //    //F(1, k) = vertPositions[k].y();
        //    //F(2, k) = vertPositions[k].z();
        //    V.col(k) = vertPositions[k];
        //}
        //// XZH END

        // after setup V and F
        computeNormals();

        // initial
        if (isFirstMatrix)
        {
            /* Pre-computation for mesh deformation */
            computeAdjacencyMatrix();
            computeWeightMatrix();
            isFirstMatrix = false;
        }
    }

    void VESSDeformMesh::setConstraint(int id, const Eigen::Vector3d &position) 
    {
        constraints[id] = position;
    }

    void VESSDeformMesh::solve(Eigen::MatrixXd &U) 
    {
        Vprime = U;
        initializeRotations();
        initializeConstraints();
        initializeLinearSystem();

        int iter = 0;
        int iterMax = 2; // original value  3;  XZH
        while (iter < iterMax) 
        {
            estimateRotations();
            estimatePositions();
            iter++;
        }
        U = Vprime;
        //std::cout << "Injecting .. ";
        // update vertex position via U XZH
        auto state2 = m_deformModel->getCurrentState();
        for (int m = 0; m < U.cols();m++)
        {
            for (std::unordered_map< int, int >::iterator it2 = localToGlobalMappingVertex.begin(); it2 != localToGlobalMappingVertex.end(); it2++) 
            {
                if (it2->first==m) // find out the local vertex index
                {
                    Vec3d& pbdx1 = state2->getVertexPosition(it2->second); // global index
                    pbdx1 = U.col(m);
                    m_vertexPhys[it2->second].p = U.col(m);
                }
            }
        }

    }

    void VESSDeformMesh::initializeRotations() 
    {
        R.clear();
        R.resize(V.cols(), Eigen::MatrixXd::Identity(3, 3));
    }

    void VESSDeformMesh::initializeConstraints() 
    {
        freeIdxMap.resize(V.cols());
        freeIdx = 0;
        for (int i = 0; i<V.cols(); ++i) 
        {
            int idx = constraints.find(i) != constraints.end() ? -1 : freeIdx++;
            freeIdxMap[i] = idx;
        }

        for (auto i = constraints.begin(); i != constraints.end(); ++i) 
        {
            Vprime.col(i->first) = i->second;
        }
        int nTmp = freeIdx;
        if (nTmp==0)
        {
            printf("freeIdx is 0, not...");
        }
    }

    void VESSDeformMesh::initializeLinearSystem() 
    {
        L.resize(freeIdx, freeIdx);  // 矩阵元素大小为 freeIdx*freeIdx，即n*n，但由于是以稀疏矩阵存储，故 reserve了下面大小
        L.reserve(Eigen::VectorXi::Constant(freeIdx, 7)); // 稀疏矩阵存储节省了大量空间，原来是n*n，变成n*7
        L.setZero();

        bFixed.resize(3, freeIdx); // 3*n大小？
        bFixed.setZero();

        b.resize(3, freeIdx);

        typedef Eigen::Triplet<double> T;  // triplet即是用来存储 i,j, value 的一个具体值，稀疏矩阵的存储格式，i,j为行列，value为该行列对应的值 K(i,j)=value 与matlab对应
        std::vector<T> l_ij;
        l_ij.reserve(freeIdx * 7);  // 如上reserve，这里也是先分配这么多内存，相比n*n少了很多倍

        // test XZH
        int mTmp = W.outerSize();  // W也是和L一样大的稀疏矩阵 n*n, n为顶点数量，但是其中存储了 权重值，见mesh.cpp
        for (int k = 0; k<W.outerSize(); ++k) 
        {   // outerSize，即外层的大小 即行或是列数，矩阵是由行*列数，如果是列优先存，则得到列数，如果是行优先存，则得到行数，Eigen默认列优先
            for (Eigen::SparseMatrix<double>::InnerIterator it(W, k); it; ++it) 
            {  // 迭代访问稀疏矩阵
                int i = it.row(); // 得到 (i,j, value)中的i 即  637*637中的某一行
                int j = it.col();  // 列
                int idx_i = freeIdxMap[i];  // 存的都是freeIdx的值 ，如果是约束点则为-1
                int idx_j = freeIdxMap[j];  // 
                float w_ij = it.value();

                if (idx_i == -1) 
                {
                    continue;
                }
                // Laplacian Matrix http://www.ctralie.com/Teaching/LapMesh/
                if (idx_j == -1) 
                {
                    bFixed.col(idx_i) += w_ij * constraints[j];
                }
                else 
                {
                    l_ij.push_back(T(idx_i, idx_j, -w_ij));  // 将 (i,j, value)加入稀疏矩阵中
                }
                l_ij.push_back(T(idx_i, idx_i, w_ij));
            }
        }

        L.setFromTriplets(l_ij.begin(), l_ij.end());  // //根据三元组列表生成稀疏矩阵
        // std::cout << L.size() << std::endl;
        /*
        for (int i=0; i<L.outerSize(); ++i) {
        for(Eigen::SparseMatrix<float>::InnerIterator it(L, i); it; ++it) {
        std::cout << "(" << it.row() << ","; // row index (j)
        std::cout << it.col() << ")\t"; // col index (i)
        std::cout << it.value() << std::endl;
        }
        }
        */
        solver.compute(L);
    }


    void VESSDeformMesh::estimateRotations() 
    {
        for (int k = 0; k<W.outerSize(); ++k) 
        {
            Eigen::MatrixXd cov;
            cov = Eigen::MatrixXd::Zero(3, 3);

            for (Eigen::SparseMatrix<double>::InnerIterator it(W, k); it; ++it) 
            {
                int i = it.row();
                int j = it.col();
                float w_ij = it.value();

                Eigen::Vector3d p_i = Vinit.col(i); //  V.col(i);  // Vinit.col(i); // 
                Eigen::Vector3d p_j = Vinit.col(j); //  V.col(j);  // Vinit.col(j); // 
                Eigen::Vector3d pp_i = Vprime.col(i);
                Eigen::Vector3d pp_j = Vprime.col(j);

                cov += w_ij * (p_i - p_j) * ((pp_i - pp_j).transpose());
            }

            // Jacobian SVD (Singular Value Decomposition) SVD矩阵分解 m*n矩阵 
            Eigen::JacobiSVD<Eigen::MatrixXd> svd(cov, Eigen::ComputeFullU | Eigen::ComputeFullV); // 加上FullU V，则U= m*m  V=n*n
            Eigen::MatrixXd SV = svd.matrixV();
            Eigen::MatrixXd SU = svd.matrixU().transpose();
            Eigen::MatrixXd I = Eigen::MatrixXd::Identity(3, 3);
            I(2, 2) = (SV * SU).determinant();

            // Rotation matrix for vertex k
            R[k] = (SV * I * SU);
        }
    }

    void VESSDeformMesh::estimatePositions() 
    {
        b = bFixed;

        for (int k = 0; k<W.outerSize(); ++k) 
        {
            for (Eigen::SparseMatrix<double>::InnerIterator it(W, k); it; ++it) 
            {
                int i = it.row();
                int j = it.col();
                int idx_i = freeIdxMap[i];
                if (idx_i == -1) 
                {
                    continue;
                }

                float w_ij = it.value();
                Eigen::MatrixXd r = R[i] + R[j];
                Eigen::Vector3d p_i = Vinit.col(i); //  V.col(i); // Vinit.col(i); // 
                Eigen::Vector3d p_j = Vinit.col(j); //   V.col(j);  // Vinit.col(j); //  
                Eigen::Vector3d p = r * (p_i - p_j) * w_ij * 0.5;
                b.col(idx_i) += p;
            }
        }

        // Solve Lp' = b
        Eigen::Matrix<double, Eigen::Dynamic, 1> LU;
        for (int i = 0; i<3; ++i) 
        {
            LU = solver.solve(b.row(i).transpose());
            int idx = 0;
            for (int j = 0; j < V.cols(); ++j) 
            {
                if (freeIdxMap[j] != -1) 
                {
                    Vprime(i, j) = LU(idx++);
                }
            }
        }
        // std::cout << Vprime << std::endl;
    }

    void VESSDeformMesh::computeNormals() 
    {
        //std::cout << "Computing face and vertex normals .. ";

        Nf.resize(3, F.cols());  // triangle normal
        Nf.setZero();

        N.resize(3, V.cols());  // vertex normal
        N.setZero();

        for (int f = 0; f < F.cols(); f++) 
        {
            Eigen::Vector3d fn = Eigen::Vector3d::Zero();
            for (int i = 0; i < 3; ++i) 
            {
                Eigen::Vector3d v0 = V.col(F(i, f));
                Eigen::Vector3d v1 = V.col(F((i + 1) % 3, f));
                Eigen::Vector3d v2 = V.col(F((i + 2) % 3, f));
                Eigen::Vector3d d0 = v1 - v0;
                Eigen::Vector3d d1 = v2 - v0;

                if (i == 0) 
                {
                    fn = d0.cross(d1);
                    float norm = fn.norm();
                    fn /= norm;
                    Nf.col(f) = fn;
                }

                double cos = d0.dot(d1) / std::sqrt(d0.squaredNorm() * d1.squaredNorm());
                double negate = float(cos < 0.0f);
                cos = std::abs(cos);
                double ret = -0.0187293f;   //   NVIDIA CG Mathematical Functions   http://blog.csdn.net/wolf96/article/details/42736549
                ret *= cos; ret = ret + 0.0742610f;
                ret *= cos; ret = ret - 0.2121144f;
                ret *= cos; ret = ret + 1.5707288f;
                ret = ret * std::sqrt(1.0f - cos);
                ret = ret - 2.0f * negate * ret;
                double angle = negate * (double)M_PI_VESS + ret;  // Line 56-64  acos(x)  // Handbook of Mathematical Functions// M. Abramowitz and I.A. Stegun, Ed. // Absolute error <= 6.7e-5

                for (int k = 0; k < 3; ++k) 
                {
                    double coeff = N.coeffRef(k, F(i, f));  // f tri number，i 0-2 vertex，  each vertex's normal based on its neighbors' triangle, accumulation
                    coeff += fn[k] * angle;  // tri's normal * angle??!? is different from iMSTK
                    N.coeffRef(k, F(i, f)) = coeff;  // coeffRef  set （i, j）value to coeff  
                }
            }
        }
        // initial
        if (isFirstNormal)
        {
            Ninit.resize(3, V.cols());  // vertex normal
            Ninit.setZero();
            Ninit = N;
            Vinit.resize(3, V.cols());  // vertex normal
            Vinit.setZero();
            Vinit = V;
            isFirstNormal = false;
        }
        
        //std::cout << "done." << std::endl;
    }

    void VESSDeformMesh::computeWeightMatrix() 
    {
        //std::cout << "Computing weight matrix.. ";

        typedef Eigen::Triplet<double> T;
        std::vector<T> w_ij;
        w_ij.reserve(F.cols() * 3);

        for (int i = 0; i < F.cols(); i++) 
        {
            int a = F(0, i);
            int b = F(1, i);
            int c = F(2, i);

            Eigen::Vector3d va = V.col(a);
            Eigen::Vector3d vb = V.col(b);
            Eigen::Vector3d vc = V.col(c);

            double la = (vb - vc).squaredNorm();
            double lb = (va - vb).squaredNorm();
            double lc = (vc - va).squaredNorm();

            la = std::sqrt(la);
            lb = std::sqrt(lb);
            lc = std::sqrt(lc);

            // Heron's formula
            double arg =
                (la + (lb + lc)) *
                (lc - (la - lb)) *
                (lc + (la - lb)) *
                (la + (lb - lc));
            double area = 0.25 * sqrt(arg);
            double denom = 1.0 / (4.0 * area);

            // Cotangent weight
            double cot_a = (lb * lb + lc * lc - la * la) * denom;
            double cot_b = (lc * lc + la * la - lb * lb) * denom;
            double cot_c = (la * la + lb * lb - lc * lc) * denom;

            cot_a = std::max<double>(double(1e-10), cot_a);
            cot_b = std::max<double>(double(1e-10), cot_b);
            cot_c = std::max<double>(double(1e-10), cot_c);

            w_ij.push_back(T(a, b, 0.5 * cot_c));  // original 0.5 in these 6 lines trying to other value   wij=0.5*(cot Aij+ cot Bij) http://www.ctralie.com/Teaching/LapMesh/
            w_ij.push_back(T(b, a, 0.5 * cot_c));   

            w_ij.push_back(T(b, c, 0.5 * cot_a));
            w_ij.push_back(T(c, b, 0.5 * cot_a));

            w_ij.push_back(T(c, a, 0.5 * cot_b));
            w_ij.push_back(T(a, c, 0.5 * cot_b));
        }

        W.resize(V.cols(), V.cols());
        W.reserve(Eigen::VectorXi::Constant(V.cols(), 7));
        W.setZero();
        W.setFromTriplets(w_ij.begin(), w_ij.end());

        //std::cout << "done." << std::endl;
    }

    void VESSDeformMesh::computeAdjacencyMatrix() {
        //std::cout << "Computing adjacency matrix.. ";

        typedef Eigen::Triplet<int> T;
        std::vector<T> adj_ij;
        adj_ij.reserve(F.size() * 2);

        adjList.clear();
        adjList.resize(F.maxCoeff() + 1);

        for (int i = 0; i < 3; i++) 
        {
            for (int j = 0; j < F.cols(); j++) 
            {
                int source = F(i, j);
                int dest = F((i + 1) % 3, j);
                adj_ij.push_back(T(source, dest, 1));  // has direct connection  its value is 1
                adj_ij.push_back(T(dest, source, 1));

                adjList.at(source).push_back(dest);
                adjList.at(dest).push_back(source);
            }
        }

        A.resize(V.cols(), V.cols());
        A.reserve(6 * (F.maxCoeff() + 1));  // max element  - vertice number total  *6, since each vertice might have max 6 neighbors
        A.setFromTriplets(adj_ij.begin(), adj_ij.end());  // 1 and 0

        // Set all non-zero to 1 ,  since might be merged with 1+1+1, then should normalize to 1
        for (int k = 0; k < A.outerSize(); ++k) 
        {  // default column-major
            //for (typename Eigen::SparseMatrix<int>::InnerIterator it(A, k); it; ++it) { // typename cannot use here, so use typedef in .h
            for (EIGENINITERATOR it(A, k); it; ++it) 
            {
                assert(it.value() != 0);
                A.coeffRef(it.row(), it.col()) = 1;
            }
        }

        // Remove duplicates
        for (int i = 0; i < (int)adjList.size(); i++) 
        {
            std::sort(adjList[i].begin(), adjList[i].end());
            adjList[i].erase(std::unique(adjList[i].begin(), adjList[i].end()), adjList[i].end());
        }

        //std::cout << "done." << std::endl;
    }

    // set geometry
    void
        VESSDeformMesh::setPhysModel(std::shared_ptr<PbdModel> model)
    {
        m_deformModel = model;
    }

    void
        VESSDeformMesh::setPhysVertices(std::vector<physVertex>& m)
    {
        m_vertexPhys = m;
    }

    void
        VESSDeformMesh::getNeighborsVertex(std::list<int>& nList, int localIdx, int lev, int maxLevel)
    {
        if (lev >= maxLevel) return; 

        for (int t = 0; t < adjList[localIdx].size(); t++)
        {
            nList.push_back(adjList[localIdx][t]);
            switch (lev)
            {
            case 0:
                nListLev0.push_back(adjList[localIdx][t]);
                break;
            case 1:
                nListLev1.push_back(adjList[localIdx][t]);
                break;
            case 2:
                nListLev2.push_back(adjList[localIdx][t]);
                break;
            case 3:
                nListLev3.push_back(adjList[localIdx][t]);
                break;
            case 4:
                nListLev4.push_back(adjList[localIdx][t]);
                break;
            }
            getNeighborsVertex(nList, adjList[localIdx][t], lev + 1, maxLevel);
        }
    }

    void
        VESSDeformMesh::updateAdjList()
    {
        nListLev0.sort();
        nListLev0.unique();
        nListLev1.sort();
        nListLev1.unique();
        nListLev2.sort();
        nListLev2.unique();
        nListLev3.sort();
        nListLev3.unique();
        nListLev4.sort();
        nListLev4.unique();

        std::list<int> nListLev4R;
        for (auto iter = nListLev4.begin(); iter != nListLev4.end(); iter++)
        {
            int nn = *iter;
            auto it = find(nListLev3.begin(), nListLev3.end(), nn); // find in the list
            if (it == nListLev3.end())  // not in the list
            {
                nListLev4R.push_back(nn);
            }
        }
        nListLev4.clear();
        nListLev4 = nListLev4R;

        std::list<int> nListLev3R;
        for (auto iter = nListLev3.begin(); iter != nListLev3.end(); iter++)
        {
            int nn = *iter;
            auto it = find(nListLev2.begin(), nListLev2.end(), nn); // find in the list
            if (it == nListLev2.end())  // not in the list
            {
                nListLev3R.push_back(nn);
            }
        }
        nListLev3.clear();
        nListLev3 = nListLev3R;

        std::list<int> nListLev2R;
        for (auto iter = nListLev2.begin(); iter != nListLev2.end(); iter++)
        {
            int nn = *iter;
            auto it = find(nListLev1.begin(), nListLev1.end(), nn); // find in the list
            if (it == nListLev1.end())  // not in the list
            {
                nListLev2R.push_back(nn);
            }
        }
        nListLev2.clear();
        nListLev2 = nListLev2R;

        std::list<int> nListLev1R;
        for (auto iter = nListLev1.begin(); iter != nListLev1.end(); iter++)
        {
            int nn = *iter;
            auto it = find(nListLev0.begin(), nListLev0.end(), nn); // find in the list
            if (it == nListLev0.end())  // not in the list
            {
                nListLev1R.push_back(nn);
            }
        }
        nListLev1.clear();
        nListLev1 = nListLev1R;
    }
} // imstk
