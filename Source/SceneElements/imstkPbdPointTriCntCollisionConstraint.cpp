/*=========================================================================

Library: iMSTK

Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
& Imaging in Medicine, Rensselaer Polytechnic Institute.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0.txt

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

=========================================================================*/

#include "imstkPbdPointTriCntCollisionConstraint.h"
#include "g3log/g3log.hpp"

namespace imstk
{

    void
        PbdPointTriangleContinuousConstraint::initConstraint(std::shared_ptr<PbdModel> model1, const size_t& pIdx1,
        std::shared_ptr<PbdModel> model2, const size_t& pIdx2,
        const size_t& pIdx3, const size_t& pIdx4,
        int pointIdx1, double pointpara1, int triIdx2, double tri2u, double tri2v, Vec3d colNorm, double colTime
        )
    {
        m_model1 = model1;
        m_model2 = model2;
        m_bodiesFirst[0] = pIdx1;
        m_bodiesSecond[0] = pIdx2;
        m_bodiesSecond[1] = pIdx3;
        m_bodiesSecond[2] = pIdx4;
        pointIndex1 = pointIdx1;
        pointParam1 = pointpara1;
        triIndex2 = triIdx2;
        triParamU2 = tri2u;
        triParamV2 = tri2v;
        colTriNorm = colNorm;
        colTriTime = colTriTime;
    }

    bool
        PbdPointTriangleContinuousConstraint::solvePositionConstraint()
    {
        //const auto i0 = m_bodiesFirst[0];
        //const auto i1 = m_bodiesSecond[0];
        //const auto i2 = m_bodiesSecond[1];
        //const auto i3 = m_bodiesSecond[2];

        //auto tetMesh = std::dynamic_pointer_cast<VESSTetrahedralMesh>(m_model2->getModelGeometry());
        //if (!tetMesh) return false;
        //auto& physEdges = tetMesh->getPhysEdge();
        //auto& physVertices = tetMesh->getPhysVertex();
        //auto& physTris = tetMesh->getPhysSurfTris();
        //// continuousCollisionHandlingWithSurfaceMesh 
        //// check collision repetition to a node to avoid large impact on the node
        //// lack code 

        //int m, k, n;
        //Vec3d x1, x2, x3, x4;
        //double bary[3];
        //Vec3d dV;
        //double ratio;
        ////algorithm step3  
        //// compute impulse based on the collision depth
        //double fS, fT, magImpsNorm, magImpsTang, colWeight;
        //Vec3d toolColPt, objColPt, impsVec, impsTang, impsNorm;
        //ratio = 0.1;

        //toolColPt = tool.ccdPos + tool.gloVec[0];  // 0 or 1??  for line mesh colliding, 1 is for tip, 0 for root? so may be use gloVec[1] ??
        //x1 = physVertices[i1].p;
        //x2 = physVertices[i2].p;
        //x3 = physVertices[i3].p;
        //bary[0] = 1.0 - triParamU2 - triParamV2;
        //bary[1] = triParamU2;
        //bary[2] = triParamV2;

        //objColPt = bary[0] * x1 + bary[1] * x2 + bary[2] * x3;
        //impsVec = toolColPt - objColPt;

        //int index = i1;
        //colWeight = 1.0 / 10;  // XZH ,should calculate smFloat(simObj->ctnColNode[simObj->ctnColPair[n].colTriNodeID[k]].nbrCol);
        //physVertices[index].p += ratio*colWeight * bary[0] * impsVec;
        //index = i2;
        //colWeight = 1.0 / 10;  // XZH ,should calculate smFloat(simObj->ctnColNode[simObj->ctnColPair[n].colTriNodeID[k]].nbrCol);
        //physVertices[index].p += ratio*colWeight * bary[1] * impsVec;
        //index = i3;
        //colWeight = 1.0 / 10;  // XZH ,should calculate smFloat(simObj->ctnColNode[simObj->ctnColPair[n].colTriNodeID[k]].nbrCol);
        //physVertices[index].p += ratio*colWeight * bary[2] * impsVec;
        
        return true;
    }

    bool
        PbdPointTriangleContinuousConstraint::solvePositionConstraint(ToolInfo& tool)
    {
        const auto i0 = m_bodiesFirst[0];
        const auto i1 = m_bodiesSecond[0];
        const auto i2 = m_bodiesSecond[1];
        const auto i3 = m_bodiesSecond[2];

        auto tetMesh = std::dynamic_pointer_cast<VESSTetrahedralMesh>(m_model2->getModelGeometry());
        if (!tetMesh) return false;
        auto& physEdges = tetMesh->getPhysEdge();
        auto& physVertices = tetMesh->getPhysVertex();
        auto& physTris = tetMesh->getPhysSurfTris();
        // continuousCollisionHandlingWithSurfaceMesh 
        // check collision repetition to a node to avoid large impact on the node
        // lack code 

        int m, k, n;
        Vec3d x1, x2, x3, x4;
        double bary[3];
        Vec3d dV;
        double ratio;
        //algorithm step3  
        // compute impulse based on the collision depth
        double fS, fT, magImpsNorm, magImpsTang, colWeight;
        Vec3d toolColPt, objColPt, impsVec, impsTang, impsNorm;
        ratio = 0.1;

        toolColPt = tool.ccdPos + tool.gloVec[0];  // 0 or 1??  for line mesh colliding, 1 is for tip, 0 for root? so may be use gloVec[1] ??
        x1 = physVertices[i1].p;
        x2 = physVertices[i2].p;
        x3 = physVertices[i3].p;
        bary[0] = 1.0 - triParamU2 - triParamV2;
        bary[1] = triParamU2;
        bary[2] = triParamV2;

        objColPt = bary[0] * x1 + bary[1] * x2 + bary[2] * x3;
        impsVec = toolColPt - objColPt;

        int index = i1;
        colWeight = 1.0 / 10;  // XZH ,should calculate smFloat(simObj->ctnColNode[simObj->ctnColPair[n].colTriNodeID[k]].nbrCol);
        physVertices[index].p += ratio*colWeight * bary[0] * impsVec;
        index = i2;
        colWeight = 1.0 / 10;  // XZH ,should calculate smFloat(simObj->ctnColNode[simObj->ctnColPair[n].colTriNodeID[k]].nbrCol);
        physVertices[index].p += ratio*colWeight * bary[1] * impsVec;
        index = i3;
        colWeight = 1.0 / 10;  // XZH ,should calculate smFloat(simObj->ctnColNode[simObj->ctnColPair[n].colTriNodeID[k]].nbrCol);
        physVertices[index].p += ratio*colWeight * bary[2] * impsVec;

        return true;
    }

    bool
        PbdPointTriangleContinuousConstraint::solvePositionConstraint(Vec3d& force)
    {
        const auto i0 = m_bodiesFirst[0];
        const auto i1 = m_bodiesSecond[0];
        const auto i2 = m_bodiesSecond[1];
        const auto i3 = m_bodiesSecond[2];

        auto state1 = m_model1->getCurrentState();
        auto state2 = m_model2->getCurrentState();

        Vec3d& x0 = state1->getVertexPosition(i0);
        Vec3d& x1 = state2->getVertexPosition(i1);
        Vec3d& x2 = state2->getVertexPosition(i2);
        Vec3d& x3 = state2->getVertexPosition(i3);

        Vec3d x12 = x2 - x1;
        Vec3d x13 = x3 - x1;
        Vec3d n = x12.cross(x13);
        Vec3d x01 = x0 - x1;

        double alpha = n.dot(x12.cross(x01)) / (n.dot(n));
        double beta = n.dot(x01.cross(x13)) / (n.dot(n));

        if (alpha < 0 || beta < 0 || alpha + beta > 1)
        {
            //LOG(WARNING) << "Projection point not inside the triangle";
            return false;
        }

        const double dist = m_model1->getProximity() + m_model2->getProximity();

        n.normalize();

        double l = x01.dot(n);

        if (l > dist)
        {
            return false;
        }

        double gamma = 1.0 - alpha - beta;
        Vec3d grad0 = n;
        Vec3d grad1 = -alpha*n;
        Vec3d grad2 = -beta*n;
        Vec3d grad3 = -gamma*n;

        const auto im0 = m_model1->getInvMass(i0);
        const auto im1 = m_model2->getInvMass(i1);
        const auto im2 = m_model2->getInvMass(i2);
        const auto im3 = m_model2->getInvMass(i3);

        double lambda = im0*grad0.squaredNorm() +
            im1*grad1.squaredNorm() +
            im2*grad2.squaredNorm() +
            im3*grad3.squaredNorm();
        //printf("Tri Point lambda %f, im: %f %f %f %f \n", lambda, im0*grad0.squaredNorm(), im1*grad1.squaredNorm(), im2*grad2.squaredNorm(), im3*grad3.squaredNorm());
        Vec3d temp;
        if (lambda >= 0.000001)  // !=0
        {
            force += -(l - dist)*lambda*grad0;
            temp = -(l - dist)*lambda*grad0;
            //printf("force PT: %f %f %f %f\n", temp.x(), temp.y(), temp.z(), temp.norm());
        }
        lambda = (l - dist) / lambda; // if im1=im2=im3=0 is fixed point, lambda=-1.#INF00

        //    LOG(INFO) << "Lambda:" << lambda <<" Normal:" << n[0] <<" " << n[1] <<" "<<n[2];
        //force = -(l - dist)*n; // original XZH force = -(l - dist)*n; // (l - dist) is penetrationDepth, n is direction
        //printf("force Tri Point: %f %f %f lambda %f \n", force.x(), force.y(), force.z(), lambda);
        if (im0 > 0)
        {
            x0 += -im0*lambda*grad0*m_model1->getContactStiffness();
        }

        if (im1 > 0)
        {
            x1 += -im1*lambda*grad1*m_model2->getContactStiffness();
        }

        if (im2 > 0)
        {
            x2 += -im2*lambda*grad2*m_model2->getContactStiffness();
        }

        if (im3 > 0)
        {
            x3 += -im3*lambda*grad3*m_model2->getContactStiffness();
        }

        return true;
    }

} // imstk
