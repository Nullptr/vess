/*=========================================================================

Library: iMSTK

Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
& Imaging in Medicine, Rensselaer Polytechnic Institute.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0.txt

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

=========================================================================*/

#ifndef imstkVESSDeformMesh_h
#define imstkVESSDeformMesh_h

#include <iostream>
#include <unordered_map>

#include "imstkVESSBasic.h"
#include "imstkPbdModel.h"

#include <Eigen/Core>
#include <Eigen/Sparse>
#include <Eigen/SVD>

#define M_PI_VESS       3.14159265358979323846


namespace imstk
{
    //class VESSTetrahedralMesh;

    class VESSDeformMesh {
    public:

        void init(); //  std::shared_ptr<VESSTetrahedralMesh> tetMesh); // std::vector<physVertex>& physVertices, std::vector<physSurfTris>& physSTris);
        void solve(Eigen::MatrixXd &U);
        void setConstraint(int id, const Eigen::Vector3d &position);
        void initializeRotations();
        void initializeConstraints();
        void initializeLinearSystem();
        void estimateRotations();
        void estimatePositions();

        // Mesh
        void computeNormals();
        void computeWeightMatrix();
        void computeAdjacencyMatrix();

        ///
        /// \brief Set/Get the geometry (mesh in this case) used by the deform tet
        ///
        void setPhysVertices(std::vector<physVertex>& m);
        std::vector<physVertex>& getPhysVertices()  { return m_vertexPhys; }
        void setPhysModel(std::shared_ptr<PbdModel> model);
        std::shared_ptr<PbdModel> getPhysModel()  { return m_deformModel; }

        void getNeighborsVertex(std::list<int>& nList, int localIdx, int lev, int maxLevel); // maxLevel is defined by user, might choose different level, lev is current level

        const Eigen::MatrixXd& getInitVertNormal() const { return Ninit; }

        void updateAdjList();

        std::list<int>& getAdjList0() { return nListLev0; }
        std::list<int>& getAdjList1() { return nListLev1; }
        std::list<int>& getAdjList2() { return nListLev2; }
        std::list<int>& getAdjList3() { return nListLev3; }
        std::list<int>& getAdjList4() { return nListLev4; }

    public:
        // Mesh variables
        Eigen::MatrixXd Nf;
        Eigen::MatrixXd N;

        Eigen::MatrixXd Ninit; // initialized normal of all the vertex
        bool isFirstNormal=true; // if initialized , is true
        bool isFirstMatrix = true; // for weight and adlist
        // sparse
        Eigen::SparseMatrix<double> W;
        Eigen::SparseMatrix<int> A;
        std::vector<std::vector<int> > adjList;

        Eigen::MatrixXd V; // vertices
        Eigen::MatrixXd Vinit; // initial vertices
        Eigen::MatrixXi F; // triangles
        Eigen::MatrixXd Vprime;
        Eigen::Matrix<double, 3, Eigen::Dynamic> b;
        Eigen::Matrix<double, 3, Eigen::Dynamic> bFixed;

        int freeIdx;
        std::vector<int> freeIdxMap;
        std::vector<Eigen::MatrixXd> R;
        //Eigen::SparseMatrix<float> W;
        Eigen::SparseMatrix<double> L;

        std::unordered_map<int, Eigen::Vector3d> constraints;
        Eigen::SimplicialLDLT<Eigen::SparseMatrix<double> > solver;

        // mapping for the local vertex order and global vertex order in the tet
        std::unordered_map<int, int> localToGlobalMappingVertex;

        typedef Eigen::SparseMatrix<int>::InnerIterator EIGENINITERATOR;

        // tet mesh
        std::vector<physVertex> m_vertexPhys;
        std::shared_ptr<PbdModel> m_deformModel;

          ////< XZH 06/07/2019 store its neighbors
        std::list<int> nListLev0;
        std::list<int> nListLev1;
        std::list<int> nListLev2;
        std::list<int> nListLev3;
        std::list<int> nListLev4;
    };

}// imstk

#endif // ifndef imstkVESSDeformMesh_h
