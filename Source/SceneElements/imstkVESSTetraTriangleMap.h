/*=========================================================================

Library: iMSTK

Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
& Imaging in Medicine, Rensselaer Polytechnic Institute.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0.txt

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

=========================================================================*/

#ifndef imstkVESSTetraTriangleMap_h
#define imstkVESSTetraTriangleMap_h

#include <limits>

// imstk
#include "imstkGeometryMap.h"
#include "imstkVESSTetrahedralMesh.h"
#include "imstkSurfaceMesh.h"

namespace imstk
{

    ///
    /// \class VESSTetraTriangleMap
    ///
    /// \brief Computes and applies the triangle-tetrahedra map. The master mesh is the
    ///  tetrahedral mesh and the slave is the surface triangular mesh.
    ///
    class VESSTetraTriangleMap : public GeometryMap
    {
    public:
        ///
        /// \brief Constructor
        ///
        VESSTetraTriangleMap() : GeometryMap(GeometryMap::Type::TetraTriangle){}

        ///
        /// \brief Destructor
        ///
        ~VESSTetraTriangleMap() = default;

        ///
        /// \brief Compute the tetra-triangle mesh map
        ///
        void compute() override;
        void computeXZH() override {}
        void computeNewXZH() override {}

        ///
        /// \brief Apply (if active) the tetra-triangle mesh map
        ///
        void apply() override;

        ///
        /// \brief Print the map
        ///
        void print() const override;

        ///
        /// \brief Check the validity of the map
        ///
        bool isValid() const override;

        ///
        /// \brief Set the geometry that dictates the map
        ///
        void setMaster(std::shared_ptr<Geometry> master) override;

        ///
        /// \brief Set the geometry that follows the master
        ///
        void setSlave(std::shared_ptr<Geometry> slave) override;

        ///
        /// \brief Get information XZH
        ///
        std::vector<TetrahedralMesh::WeightsArray>& getVertWeights() { return m_verticesWeights; }
        std::vector<size_t>& getTetraID() { return m_verticesEnclosingTetraId; }

    protected:
        std::vector<TetrahedralMesh::WeightsArray> m_verticesWeights; ///> weights
        std::vector<size_t> m_verticesEnclosingTetraId; ///> Enclosing tetrahedra to interpolate the weights upon
    };

} // imstk

#endif // imstkVESSTetraTriangleMap_h