/*=========================================================================

Library: iMSTK

Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
& Imaging in Medicine, Rensselaer Polytechnic Institute.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0.txt

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

=========================================================================*/

#ifndef imstkVESSTetrahedralMesh_h
#define imstkVESSTetrahedralMesh_h

#include "imstkVESSBasic.h"
#include "imstkTetrahedralMesh.h"
#include "imstkVESSBasicMesh.h"
#include "imstkSpatialHashTableVESS.h"
#include "imstkVESSDeformMesh.h" // injection solution

namespace imstk
{
    class VESSDeformMesh;
    ///
    /// \brief VESSTetrahedralMesh
    ///
    class VESSTetrahedralMesh : public TetrahedralMesh
    {
    public:
        using triArray = SurfaceMesh::TriangleArray;

        ///
        /// \brief Constructor
        ///
        VESSTetrahedralMesh() : TetrahedralMesh() {}

        ///
        /// \brief Destructor
        ///
        ~VESSTetrahedralMesh() = default;

        ///
        /// \brief This method
        /// (a) Extracts the confirming triangular mesh from the tetrahedral mesh
        /// (b) Checks and flips the triangle connectivity order if it is not consistent
        /// (c) Renumbers the vertices
        bool extractSurfaceMesh();
        bool extractSurfaceMesh2(); // another method
        bool extractSurfaceMesh(const std::vector<Vec3d>& centralLine);  // determine from triangle normal and the weight point of triangle to this line
        bool extractSurfaceMeshVolume();

        ///
        /// \brief Set/Get the surface mesh from tetrahedrons
        ///
        std::shared_ptr<SurfaceMesh> getSurfaceMesh() const { return surfaceMesh; }
        std::shared_ptr<SurfaceMesh> getSurfaceInsideMesh() const { return surfaceInsideMesh; }
        std::shared_ptr<SurfaceMesh> getSurfaceMeshVolume() const { return surfaceMeshVolume; }

        ///
        /// \brief Return the vector of array of surface mesh from the tetrahedra
        ///
        const std::vector<triArray>& getTetrahedraSurfaceVertices() const { return m_tetrahedraSurfaceVertices; }
        const std::vector<int>& getTetrahedraSurfaceIndex() const { return m_tetrahedraSurfaceIndex; }

        ///
        /// \brief get and set the links between tetrahedrons and surfacemesh
        ///
        const std::vector<physXLink>& getPhysXLink() const { return m_volsurfLinks; }
        void setPhysXLink(const std::vector<physXLink>& links);

        ///
        /// \brief introduce the function get edge neighbors triangle
        ///
        const std::unordered_map<U64, int>& getEdgeTriMap() const { return m_mapEdgeTriIndex; }

        // get several 
        std::vector<physEdge>& getPhysEdge() { return m_edgeCollision; }
        std::vector<int>& getPhysEdgeIndex() { return m_edgeColIndex; }
        std::vector<physSurfTris>& getPhysSurfTris() { return m_surftrisCollision; }
        std::vector<int>& getPhysSurfTrisIndex() { return m_surftrisColIndex; }
		int getPhysSurfTrisNumber() { return m_nSurftris; }
		std::vector<injectSurfTris>& getInjectSurfTris() { return m_surftrisInject; }
		std::vector<int>& getInjectSurfTrisIndex() { return m_surftrisInjectIndex; }
        std::vector<physVertex>& getPhysVertex() { return m_vertexCollision; }


		std::unordered_map< U64, int >& getMapVertexIndex() { return m_mapVertexIndex; }

        // setup a physVertices list for all the vertices
        void setInitialPhysVertex(const StdVectorOfVec3d& physVertices);

        ///
        /// \brief set fixed vertices and its states for each vertex
        ///
        void setClosestVertTool(const int num) { closestSurfVertToToolTip = num; }
        void setFixedVertice(const std::vector<int>& fixedVert) { m_fixedVerticeList = fixedVert; }
        std::vector<int>& getFixedVertice() { return m_fixedVerticeList; }
        void updateFixedVertice();
        void setFixedVerticeStates(const std::vector<int>& fixedStates) { m_fixedVerticeStates = fixedStates; }
        std::vector<int>& getFixedVerticeStates() { return m_fixedVerticeStates; }

        // get hash table
        std::shared_ptr<SpatialHashTableVESS> getHashTableVess() { return m_spatialhashVESS; }

        // get deform for injection solution
        void initDeformMesh();
        std::shared_ptr<VESSDeformMesh> getVESSDeformMesh() { return m_deformMesh; }

        ///
        /// \brief cutting graph
        ///
        std::shared_ptr<UndirectedGraph> getGraphVolume() { return udgraphVolume; }
        void setGraphVolume(std::shared_ptr<UndirectedGraph> udgraph) { udgraphVolume = udgraph; }
        void getPathVertices(int& vert0, std::vector<int>& vertList) { vert0 = vertStart; vertList = vertEndList; }
        void getPathVertices(int& vert0, int& vert1) { vert0 = vertStart; vert1 = vertEnd; }
        void setPathVertices(int& vert0, std::vector<int>& vertList) { vertStart = vert0; vertEndList = vertList; }
        void setPathVertices(int& vert0, int& vert1) { vertStart = vert0; vertEnd = vert1; }
        std::vector<int>& getGraphVertices() { return vecGraphVertices; }
        void setGraphVertices(std::vector<int>& vecVerts) { vecGraphVertices = vecVerts; }
        std::vector<int>& getGraphTetrahedrons() { return vecGraphTetrahedrons; }
        void setGraphTetrahedrons(std::vector<int>& vecTets) { vecGraphTetrahedrons = vecTets; }

        ///
        /// \brief set and get the vector of array of IDs for the mesh
        ///
        void setInitTetrahedraVertices(const std::vector<TetraArray>& tetrahedrons) { m_tetrahedraInitVertices = tetrahedrons; }
        const std::vector<TetraArray>& getInitTetrahedraVertices() const{ return m_tetrahedraInitVertices; }

          ////< XZH SPH Fluid XZH
        void initSPHParticles();
        std::shared_ptr<SPHXZH> getSPHParticles() { return m_sphParticles; }
    protected:
        
        std::shared_ptr<SurfaceMesh> surfaceMesh;   ///> SurfaceMesh in the surface of tetrahedrons 
        std::shared_ptr<SurfaceMesh> surfaceInsideMesh; // SurfaceMesh in the inside surface of tetrahedrons of colon
        std::shared_ptr<SurfaceMesh> surfaceMeshVolume; // volumetric mesh display for surfaceMesh

        std::vector<triArray> m_tetrahedraSurfaceVertices; ///< vertices of the tetrahedra
        std::vector<int> m_tetrahedraSurfaceIndex; ///< index of the tetrahedra in global

        std::vector<physXLink> m_volsurfLinks;

        std::vector<physEdge> m_edgeCollision;
        std::vector<int> m_edgeColIndex; // local index (just in the collision part , not whole), not global , but its node index is global

        std::vector<physSurfTris> m_surftrisCollision;
		int m_nSurftris; // original surftris number
        std::vector<int> m_surftrisColIndex; // local index (just in the collision part , not whole), not global  , but its node index is global

		std::vector<injectSurfTris> m_surftrisInject;
		std::vector<int> m_surftrisInjectIndex; // local index (just in the inject part , not whole), not global  , but its node index is global

        std::vector<physVertex> m_vertexCollision;  // now change to a global index on 06/03/2017 currently it is a local index for vertex index, should setup a global ,order from 0-end, including non-colliding

        int closestSurfVertToToolTip;

        std::vector<int> m_fixedVerticeList;
        std::vector<int> m_fixedVerticeStates; // record fixed vertice states 0 is not fixed, PBD, 1 is fixed

        // cutting
        std::shared_ptr<UndirectedGraph> udgraphVolume; 
        int vertStart=0, vertEnd=0; // local index!!! if the path between them, connect
        std::vector<int> vertEndList; // multiple
        std::vector<int> vecGraphVertices;  // global index!!!! pre-defining area vertices
        std::vector<int> vecGraphTetrahedrons; // pre-defining area elements

        std::vector<TetraArray> m_tetrahedraInitVertices; // init for graph
    public:
        //maps aedge from-to pair to the corresponding surface of inside colon volumetric mesh
        std::unordered_map< U64, int > m_mapEdgeTriIndex;  // "int" is surface triangle number in colliding  std::map  std::unordered_map
        std::unordered_map< U64, int > m_mapVertexIndex; // map for vertex for colliding
        std::unordered_map< U64, int > m_mapEdgeIndex;  // "int " is edge number in m_edgeCollision   std::map  std::unordered_map

        //iterators
        typedef std::unordered_map< U64, int >::iterator MAPHEDGEINDEXITER;  // std::map  std::unordered_map
        typedef std::unordered_map< U64, int >::const_iterator MAPHEDGEINDEXCONSTITER;  // std::map  std::unordered_map

        std::shared_ptr<SpatialHashTableVESS> m_spatialhashVESS;

        // injection solution procedure
        std::shared_ptr<VESSDeformMesh> m_deformMesh;
        bool isFirstSurface = true; // for creating the surface mesh for inside colon
		//int numVertices = 0; // the number of deform mesh
		StdVectorOfVec3d vertPositions; // deform mesh positions
		std::list<int> uniqueVertIdList; // the vertex list in the deform mesh
		std::unordered_map<int, int> localToGlobalMappingVertex;
		Eigen::MatrixXi F; // record  triangle
		Eigen::MatrixXd V; // vertex

        std::shared_ptr<SPHXZH> m_sphParticles;
    };

}// imstk

#endif // ifndef imstkVESSTetrahedralMesh_h
