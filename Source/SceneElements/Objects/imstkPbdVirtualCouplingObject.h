/*=========================================================================

Library: iMSTK

Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
& Imaging in Medicine, Rensselaer Polytechnic Institute.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0.txt

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

=========================================================================*/

#ifndef imstkPbdVirtualCouplingObject_h
#define imstkPbdVirtualCouplingObject_h

#include "imstkPbdObject.h"
#include "imstkVESSBasic.h"
#include "imstkOneToOneMapVESS.h"
namespace imstk
{

	class Geometry;
	class GeometryMap;

	///
	/// \class PbdVirtualCouplingObject
	///
	/// \brief
	///
	class PbdVirtualCouplingObject : public PbdObject
	{
	public:
		///
		/// \brief Constructor
		///
		PbdVirtualCouplingObject(std::string name) : PbdObject(name)
		{
			m_type = SceneObject::Type::Pbd;
            initTools();
		}

		///
		/// \brief Destructor
		///
		~PbdVirtualCouplingObject() = default;

		// origin xzh should be kept after test
		///
		/// \brief
		///
		virtual void integratePosition(){ return; }

		///
		/// \brief
		///
		virtual void integrateVelocity(){ return; } // END xzh

		///
		/// \brief
		///
		virtual void updatePbdStates();


		///
		/// \brief Reset CollidingGeometry
		///
		void resetCollidingGeometry();

		///
		/// \brief Set/Get the Colliding-to-Physics map
		///
		std::shared_ptr<GeometryMap> getColldingToPhysicsMap() const;
		void setColldingToPhysicsMap(std::shared_ptr<GeometryMap> map);

		void applyCollidingToPhysics();

        inline ToolInfo& getControllerTool() { return m_toolInfo; }
        inline std::shared_ptr<DeviceClient>& getDeviceClient() { return m_deviceClient; }
        void setDeviceClient(std::shared_ptr<DeviceClient> devClient) { m_deviceClient = devClient; }
        void setProcedure(int procType) { mProcedure = procType; }

        std::vector<Vec3d>& getGapVisualDissecting() { return  dirGapVisualDissecting; }

        ///
        /// \brief Reset the dynamic object to its initial state  XZH
        ///
        void reset() override;

        ///
        /// \brief introduce the function two tools
        ///
        std::shared_ptr<SurfaceMesh> getVisualMesh(int tp)
        {
            if (tp == 0) return visualMesh0;
            else if (tp == 1) return visualMesh1;
        }
        void setVisualMesh(std::shared_ptr<SurfaceMesh> smesh, int tp)
        {
            if (tp == 0) visualMesh0=smesh;
            else if (tp == 1) visualMesh1=smesh;
        }
          ////< XZH num is P2V P2C C2V C2P tp is tool0 and tool1
        std::shared_ptr<OneToOneMapVESS> getMap(int num, int tp)
        {
            if (tp == 0)
            {
                switch (num)
                {
                case 0:
                default:
                    return mapP2V0;
                    break;
                case 1:
                    return mapP2C0;
                    break;
                case 2:
                    return mapC2V0;
                    break;
                case 3:
                    return mapC2P0;
                    break;
                }
            }
            else if (tp == 1)
            {
                switch (num)
                {
                case 0:
                default:
                    return mapP2V1;
                    break;
                case 1:
                    return mapP2C1;
                    break;
                case 2:
                    return mapC2V1;
                    break;
                case 3:
                    return mapC2P1;
                    break;
                }
            }
        }
        void setMaps(std::shared_ptr<OneToOneMapVESS> smap0, std::shared_ptr<OneToOneMapVESS> smap1, std::shared_ptr<OneToOneMapVESS> smap2, std::shared_ptr<OneToOneMapVESS> smap3, int tp)
        {
            if (tp==0)
            {
                mapP2V0 = smap0;
                mapP2C0=smap1;
                mapC2V0=smap2;
                mapC2P0=smap3;
            }
            else if (tp==1)
            {
                mapP2V1 = smap0;
                mapP2C1 = smap1;
                mapC2V1 = smap2;
                mapC2P1 = smap3;
            }
        }

        ///
        /// \ add tool information for continuous collision detection
        ///
        void initTools();
        void applyForces();

	protected:
		
		//Maps
		std::shared_ptr<GeometryMap> m_collidingToPhysicsGeomMap;   ///> Maps from collision to physics geometry
        ToolInfo m_toolInfo; // w.r.t controller
        std::shared_ptr<DeviceClient> m_deviceClient;   ////< XZH device client a little coupling

          ////< XZH dissecting
        std::vector<Vec3d> dirGapVisualDissecting;

          ////< XZH two surface mesh
        std::shared_ptr<SurfaceMesh> visualMesh0;  ////< XZH tool 0
        std::shared_ptr<SurfaceMesh> visualMesh1;
        std::shared_ptr<OneToOneMapVESS> mapP2V0;
        std::shared_ptr<OneToOneMapVESS> mapP2C0;
        std::shared_ptr<OneToOneMapVESS> mapC2V0;
        std::shared_ptr<OneToOneMapVESS> mapC2P0;
        std::shared_ptr<OneToOneMapVESS> mapP2V1;
        std::shared_ptr<OneToOneMapVESS> mapP2C1;
        std::shared_ptr<OneToOneMapVESS> mapC2V1;
        std::shared_ptr<OneToOneMapVESS> mapC2P1;

        int mProcedure = 0; // marking, 1, injecting, 2 cutting/dissecting, 3 switch camera
	};

} // imstk

#endif // ifndef imstkPbdVirtualCouplingObject_h