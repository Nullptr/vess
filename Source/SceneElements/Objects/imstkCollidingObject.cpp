/*=========================================================================

   Library: iMSTK

   Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
   & Imaging in Medicine, Rensselaer Polytechnic Institute.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0.txt

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

=========================================================================*/

#include "imstkCollidingObject.h"

namespace imstk
{
std::shared_ptr<Geometry>
CollidingObject::getCollidingGeometry() const
{
    return m_collidingGeometry;
}

void
CollidingObject::setCollidingGeometry(std::shared_ptr<Geometry> geometry)
{
    m_collidingGeometry = geometry;
}

std::shared_ptr<Geometry>
CollidingObject::getMasterGeometry() const
{
    return m_collidingGeometry;
}

std::shared_ptr<GeometryMap>
CollidingObject::getCollidingToVisualMap() const
{
    return m_collidingToVisualMap;
}

void
CollidingObject::setCollidingToVisualMap(std::shared_ptr<GeometryMap> map)
{
    m_collidingToVisualMap = map;
}

const Vec3d&
CollidingObject::getForce() const
{
    return m_force;
}

void
CollidingObject::setForce(Vec3d force)
{
    m_force = force;
}

// back up by XZH
//void
//CollidingObject::setForce(Vec3d force)
//{
//    // xzh		
//    for (int i = m_forceList.size() - 1; i >= 0; i--)
//    {
//        if (i == 0)
//        {
//            m_forceList[0] = force;
//        }
//        else
//        {
//            m_forceList[i] = m_forceList[i - 1];
//        }
//    }
//
//    Vec3d forceTmp = Vec3d::Zero();
//    for (int i = 0; i < m_forceList.size(); i++)
//    {
//        forceTmp += m_forceList[i];
//    }
//    force = forceTmp / m_forceList.size();
//
//    //// xzh END		
//    m_force = force;  // origal code just this line		
//}

// xzh clear force array when no collision detection
void
CollidingObject::clearForceList()
{
    for (int i = 0; i < m_forceList.size(); i++)
    {
        m_forceList[i] = Vec3d::Zero();
    }
}

void
CollidingObject::resetForce()
{
    //m_force.setConstant(0.0);
}

void
CollidingObject::appendForce(Vec3d force)
{
    m_force += force;
}

void
CollidingObject::updateGeometries()
{
    if (m_collidingToVisualMap)
    {
        m_collidingToVisualMap->apply();
    }
}

void
CollidingObject::setForceFeedbackParams(Vec3d finalPos, bool isCD, bool isInjected)
{
    m_finalPos = finalPos;
    m_isCD = isCD;
    m_isInjected = isInjected;
    //printf("colliding isCD: %d", isCD);
}

void
CollidingObject::setForceFeedbackStatic(Vec3d force, bool isStatic)
{
    //// xzh		
    //for (int i = m_forceList.size() - 1; i >= 0; i--)
    //{
    //    if (i == 0)
    //    {
    //        m_forceList[0] = force;
    //    }
    //    else
    //    {
    //        m_forceList[i] = m_forceList[i - 1];
    //    }
    //}

    //Vec3d forceTmp = Vec3d::Zero();
    //for (int i = 0; i < m_forceList.size(); i++)
    //{
    //    forceTmp += m_forceList[i];
    //}
    //force = forceTmp / m_forceList.size();

    //// xzh END		
    m_force = force;  // origal code just this line		
    m_isStatic = isStatic;
}

} // imstk
