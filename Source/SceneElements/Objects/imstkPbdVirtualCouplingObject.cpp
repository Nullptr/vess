/*=========================================================================

Library: iMSTK

Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
& Imaging in Medicine, Rensselaer Polytechnic Institute.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0.txt

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

=========================================================================*/

#include <memory>

#include "imstkPbdVirtualCouplingObject.h"
#include "imstkGeometry.h"
#include "imstkGeometryMap.h"
#include "imstkDeviceClient.h"

#include <g3log/g3log.hpp>

namespace imstk
{

	void
		PbdVirtualCouplingObject::updatePbdStates()
	{
		m_pbdModel->updatePbdStateFromPhysicsGeometry();
	}

	

	void
		PbdVirtualCouplingObject::resetCollidingGeometry()
    {

		if (m_collidingGeometry->isMesh()){
            auto collidingMesh = std::dynamic_pointer_cast<PointSet>(m_collidingGeometry);
			collidingMesh->setVertexPositions(collidingMesh->getInitialVertexPositions());
		}
		else{
			std::cout << "Not a mesh." << std::endl;
		}
	}

	std::shared_ptr<GeometryMap> PbdVirtualCouplingObject::getColldingToPhysicsMap() const
    {
		return m_collidingToPhysicsGeomMap;
	}

	void PbdVirtualCouplingObject::setColldingToPhysicsMap(std::shared_ptr<GeometryMap> map)
    {
		m_collidingToPhysicsGeomMap = map;
	}

	void PbdVirtualCouplingObject::applyCollidingToPhysics()
    {
		if (m_collidingToPhysicsGeomMap && m_physicsGeometry)
		{
			m_collidingToPhysicsGeomMap->apply();
		}
	}

    void PbdVirtualCouplingObject::reset()
    {
        DynamicObject<PbdState>::reset();
        //this->updateVelocity();
    }

    void PbdVirtualCouplingObject::initTools()
    {
        m_toolInfo.touchStartSphere = false;
        m_toolInfo.ccdDistance = 0.01; // 10.0;
        m_toolInfo.startPos = Vec3d::Zero();

        m_toolInfo.filterSize = 100;
        m_toolInfo.numberOfForceInFilter = 0;
        m_toolInfo.arrForce.resize(100, Vec3d::Zero());

        m_toolInfo.forceCoeff = 1;

        float scale = 1.0;// scale factor of the line-segment m_toolInfo

        m_toolInfo.nbrNode = 2; // two node for one line
        m_toolInfo.nbrActSeg = 1;
        m_toolInfo.nbrActTip = 1;

        m_toolInfo.nodeIdxOfSeg[0][0] = 0;
        m_toolInfo.nodeIdxOfSeg[0][1] = 1;

        m_toolInfo.nodeIdxOfSeg[1][0] = 1;
        m_toolInfo.nodeIdxOfSeg[1][1] = 2;

        m_toolInfo.belongToCylinder[0] = 0;
        m_toolInfo.belongToCylinder[1] = 1;

        m_toolInfo.gloVec[0] = Vec3d::Zero();
        m_toolInfo.gloVec[1] = Vec3d::Zero();
        m_toolInfo.pGloVec[0] = Vec3d::Zero();
        m_toolInfo.pGloVec[1] = Vec3d::Zero();

        m_toolInfo.springConstant = 50000; // 5;
        m_toolInfo.velocityResistance = 10000; // 1;
        m_toolInfo.hapticVelResistance = 10000; // 1;

        m_toolInfo.nodeIdxOfTip[0] = 0;

        m_toolInfo.diffPos = Vec3d::Zero();
        m_toolInfo.diffPosPre = Vec3d::Zero();

        m_toolInfo.cullingThickness[0] = 0.005; // 1.5;  // for finding candidate shoud a big than thickness
        m_toolInfo.cullingThickness[1] = 0.005; // 1.5;

        m_toolInfo.nodeIdxOfTip[0] = 0;

        m_toolInfo.diffPos = Vec3d::Zero();
        m_toolInfo.diffPosPre = Vec3d::Zero();


        m_toolInfo.oriVec[0] = Vec3d(-16.4, -1.85, 9.15);
        m_toolInfo.oriVec[1] = Vec3d(-9.3, -1.4, 9.05);

        for (int i = 0; i < m_toolInfo.nbrNode; i++){
            m_toolInfo.locVec[i] = m_toolInfo.oriVec[i];
            m_toolInfo.pLocVec[i] = m_toolInfo.oriVec[i];
        }

        m_toolInfo.thickness[0][0][0] = 0.5; // should smaller than cullingthickness
        m_toolInfo.thickness[1][0][0] = 0.8;

        m_toolInfo.thickness[0][1][0] = 0.1;
        m_toolInfo.thickness[1][1][0] = 0.1;

        m_toolInfo.thickness[0][0][1] = 0.6;
        m_toolInfo.thickness[1][0][1] = 0.9;

        m_toolInfo.thickness[0][1][1] = 0.2;
        m_toolInfo.thickness[1][1][1] = 0.2;
    }

    void PbdVirtualCouplingObject::applyForces()
    {
        Vec3d force(0.0, 0.0, 0.0);
        //Vec3d finalPos = Vec3d::Zero();
        bool isCD = false;
        bool isInjected = false;
        bool isStatic = false;
        force += this->getForce();
        //finalPos = collidingObject->getFinalPos();
        isCD = this->getIsCD();
        isInjected = this->getIsInjection();
        isStatic = this->getIsStatic();

        //m_trackingController->getDeviceClient()->setForce(force);

        //// Adding for force feedback by XZH
        auto& tool = this->getControllerTool();
        Vec3d finalPos;
        if (isInjected) finalPos = tool.injectPos; // Injection
        else finalPos = tool.finalPos; // CCD
        if (isStatic)
        {
            m_deviceClient->setForceFeedbackStatic(force, isStatic);
            //printf("force: %f %f %f\n", force.x(), force.y(), force.z());
            m_deviceClient->setToolIndex(mProcedure);
        }
        else // CCD
        {
            m_deviceClient->setForceFeedbackParams(finalPos, isCD, isInjected);
            m_deviceClient->setToolIndex(mProcedure);
        }
        //// END
    }
} //imstk
