/*=========================================================================

   Library: iMSTK

   Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
   & Imaging in Medicine, Rensselaer Polytechnic Institute.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0.txt

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

=========================================================================*/

#ifndef imstkCollidingObject_h
#define imstkCollidingObject_h

#include <memory>

#include "imstkSceneObject.h"
#include "imstkGeometryMap.h"
#include "imstkMath.h"

namespace imstk
{
class Geometry;

class CollidingObject : public SceneObject
{
public:
    ///
    /// \brief
    ///
    CollidingObject(const std::string& name) : SceneObject(name)
    {
        m_type = Type::Colliding;
    }
    CollidingObject(std::string&& name) : SceneObject(std::move(name))
    {
        m_type = Type::Colliding;
    }

    ///
    /// \brief
    ///
    virtual ~CollidingObject() = default;

    ///
    /// \brief
    ///
    std::shared_ptr<Geometry> getCollidingGeometry() const;
    void setCollidingGeometry(std::shared_ptr<Geometry> geometry);

    ///
    /// \brief Get the master geometry
    ///
    virtual std::shared_ptr<Geometry> getMasterGeometry() const override;

    ///
    /// \brief
    ///
    std::shared_ptr<GeometryMap> getCollidingToVisualMap() const;
    void setCollidingToVisualMap(std::shared_ptr<GeometryMap> map);

    ///
    /// \brief Set/Get the force to be applied to the object
    ///
    const Vec3d& getForce() const;
    void setForce(Vec3d force);
    void resetForce();
    void appendForce(Vec3d force);
    void clearForceList(); // xzh clear force array

    void setForceFeedbackParams(Vec3d finalPos, bool isCD, bool isInjected);
    void setForceFeedbackStatic(Vec3d force, bool isStatic); // static 
    const Vec3d& getFinalPos() const { return m_finalPos; }
    const bool getIsCD() const { return m_isCD; }
    const bool getIsInjection() const { return m_isInjected; }
    const bool getIsStatic() const { return m_isStatic; }

    ///
    /// \brief Updates the geometries from the maps (if defined)
    ///
    virtual void updateGeometries() override;

    ///
    /// \brief Initialize the scene object
    ///
    virtual bool initialize()
    {
        if (SceneObject::initialize())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

protected:

    std::shared_ptr<Geometry> m_collidingGeometry;       ///> Geometry for collisions
    std::shared_ptr<GeometryMap> m_collidingToVisualMap; ///> Maps transformations to visual geometry
    Vec3d m_force = Vec3d::Zero();
    // XZH
    Vec3d m_finalPos = Vec3d::Zero();
    bool m_isCD = false;
    bool m_isInjected = false;
    bool m_isStatic = false; // not CCD
    // xzh filter for force		
    std::vector<Vec3d> m_forceList = std::vector<Vec3d>(200, Vec3d::Zero());
};

using StaticObject = CollidingObject;
}// imstk

#endif // ifndef imstkCollidingObject_h
