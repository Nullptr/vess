/*=========================================================================

Library: iMSTK

Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
& Imaging in Medicine, Rensselaer Polytechnic Institute.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0.txt

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

=========================================================================*/

#include "imstkPbdSceneObjectController.h"

#include "imstkCollidingObject.h"
#include "imstkGeometry.h"

#include <utility>

#include <g3log/g3log.hpp>

namespace imstk
{

	void
		PBDSceneObjectController::initOffsets()
	{
        m_trackingController->setTranslationOffset(m_pbdVCObject->getVisualGeometry()->getTranslation());  // Vec3d(7, 1, -8)); //   // 0, 3.272233, 4.420182)); // Vec3d(0, 6.551071, 8.81142)); //  m_pbdVCObject->getCollidingGeometry()->getPosition()); // Vec3d(0, 0, 0));     // xzh (m_pbdVCObject->getCollidingGeometry()->getPosition()); // getMasterGeometry  changed to getCollidingGeometry by all
        m_trackingController->setRotationOffset(Quatd(m_pbdVCObject->getVisualGeometry()->getRotation())); // Quatd(0.1848, 0.1848, 0.1848, -0.1435)); // m_pbdVCObject->getCollidingGeometry()->getOrientation());  // Quatd(0.14, -0.15415, -0.728989, 0)); //    m_pbdVCObject->getCollidingGeometry()->getOrientation()); // Quatd(0.138521, -0.15415, -0.728989, 0.35)); //  

  //      Eigen::Matrix4d transMat;   ////< XZH test switch XYZ
  //      Eigen::Matrix3d rotMat;
  //      transMat.setIdentity();
  //      double m_scaling = 1.0;
  //      transMat(0, 0) = 1 * m_scaling;   transMat(0, 1) = 0;   transMat(0, 2) = 0;  transMat(0, 3) = 0;
  //      transMat(1, 0) = 0;    transMat(1, 1) = 0;   transMat(1, 2) = 1 * m_scaling;   transMat(1, 3) = 0;
  //      transMat(2, 0) = 0;   transMat(2, 1) = 1 * m_scaling;   transMat(2, 2) = 0;  transMat(2, 3) = 0;
  //      transMat(3, 0) = 0;   transMat(3, 1) = 0;   transMat(3, 2) = 0;  transMat(3, 3) = 1;
  //      rotMat(0, 0) = 1 * m_scaling;   rotMat(0, 1) = 0;   rotMat(0, 2) = 0; 
  //      rotMat(1, 0) = 0;    rotMat(1, 1) = 0;   rotMat(1, 2) = 1 * m_scaling;  
  //      rotMat(2, 0) = 0;   rotMat(2, 1) = 1 * m_scaling;   rotMat(2, 2) = 0;  
  //      Quatd offsetMat = Quatd(rotMat);

  //      Eigen::Matrix3d rotMat2;
  //      rotMat2(0, 0) = 1 * m_scaling;   rotMat2(0, 1) = 0;   rotMat2(0, 2) = 0;
  //      rotMat2(1, 0) = 0;    rotMat2(1, 1) = 1 * m_scaling;   rotMat2(1, 2) = 0;
  //      rotMat2(2, 0) = 0;   rotMat2(2, 1) = 0;   rotMat2(2, 2) = 1 * m_scaling;
  //      Quatd offsetMat2 = Quatd(rotMat2);
  //      m_trackingController->setRotationOffset(offsetMat*offsetMat2);
		////m_trackingController->getDeviceClient()->setButtonsEnabled(true);
        initTools();
        switchToolsXZH(0);    ////< XZH   switchTools(0);
	}

    void
        PBDSceneObjectController::switchToolsXZH(int tp)
    {
        toolIndex = tp;
    }

    void
        PBDSceneObjectController::switchTools(int tp)
    {
        if (tp==2)  ////< XZH cutting tool original
        {
            toolIndex = 0;
            updateControlledObjects3();
            m_pbdVCObject->setVisualGeometry(m_pbdVCObject->getVisualMesh(0));
            m_pbdVCObject->setPhysicsToVisualMap(m_pbdVCObject->getMap(0, 0));
            m_pbdVCObject->setPhysicsToCollidingMap(m_pbdVCObject->getMap(1, 0));
            m_pbdVCObject->setCollidingToVisualMap(m_pbdVCObject->getMap(2, 0));
            m_pbdVCObject->setColldingToPhysicsMap(m_pbdVCObject->getMap(3, 0));
        }
        else if ((tp == 0) || (tp == 1) || (tp == 3))  ////< XZH other tool
        {
            toolIndex = 1;
            updateControlledObjects3();
            m_pbdVCObject->setVisualGeometry(m_pbdVCObject->getVisualMesh(1));
            m_pbdVCObject->setPhysicsToVisualMap(m_pbdVCObject->getMap(0, 1));
            m_pbdVCObject->setPhysicsToCollidingMap(m_pbdVCObject->getMap(1, 1));
            m_pbdVCObject->setCollidingToVisualMap(m_pbdVCObject->getMap(2, 1));
            m_pbdVCObject->setColldingToPhysicsMap(m_pbdVCObject->getMap(3, 1));
        }
    }

    void
        PBDSceneObjectController::updateControlledObjectsALL()
    {
        if (!m_trackingController->isTrackerUpToDate())
        {
            if (!m_trackingController->updateTrackingData())
            {
                LOG(WARNING) << "PBDSceneObjectController::updateControlledObjects warning: could not update tracking info.";
                return;
            }
        }

        Vec3d p = m_trackingController->getPosition();
        Quatd r = m_trackingController->getRotation();

        // update end// TEST
        Eigen::Matrix3d temp = r.toRotationMatrix();
        RigidTransform3d m_transformTmp = RigidTransform3d::Identity(); ///> Transform
        m_transformTmp.linear() = r.toRotationMatrix();
        m_transformTmp.translation() = p;
        // TEST END

        //// update colliding geometry
        //m_pbdVCObject->getCollidingGeometry()->setTranslation(m_trackingController->getPosition());
        //m_pbdVCObject->getCollidingGeometry()->setRotation(m_trackingController->getRotation());

        computeTransform2(p, r, transformDev);

        Vec4d vertexPos4 = Vec4d::Zero();
        vertexPos4.w() = 1;
        Vec3d vertexPos3 = Vec3d::Zero();

        Vec3d moveLine = Vec3d(-9.093, 1.6348, -8.859199524);

        // TEST END

        auto collidingMesh = std::dynamic_pointer_cast<PointSet>(m_pbdVCObject->getCollidingGeometry());
        // TEST visual geometry just for testCuttingCubeDenseHaptic, otherwise should comment
        auto visualMesh = std::dynamic_pointer_cast<PointSet>(m_pbdVCObjectMarking->getVisualGeometry());
        auto visualMesh1 = std::dynamic_pointer_cast<PointSet>(m_pbdVCObjectInject->getVisualGeometry());
        auto visualMesh2 = std::dynamic_pointer_cast<PointSet>(m_pbdVCObject->getVisualGeometry());
        //visualMesh->setRotation(r);
        //visualMesh->setTranslation(p);

        switch (toolIndex)
        {
        default:
        case 0:   ////< XZH show marking
            for (int i = 0; i < visualMesh->getNumVertices(); ++i)
            {
                vertexPos3 = visualMesh->getInitialVertexPosition(i);  // use the inital position * Omni transform   //->getVertexPosition(i);
                //vertexPos3 += Vec3d(1.0, 0, -0.0)+moveLine;   ////< XZH  Vec3d(1.4, 0, -0.8);
                vertexPos4.x() = vertexPos3.x();
                vertexPos4.y() = vertexPos3.y();
                vertexPos4.z() = vertexPos3.z();

                vertexPos4.applyOnTheLeft(transformDev);
                vertexPos3.x() = vertexPos4.x();
                vertexPos3.y() = vertexPos4.y();
                vertexPos3.z() = vertexPos4.z();

                visualMesh->setVertexPosition(i, vertexPos3);
            }
            //visualMesh->setRotation(r);
            //visualMesh->setTranslation(p);
            vertexPos3 = Vec3d::Zero();
            for (int i = 0; i < visualMesh1->getNumVertices(); ++i)
            {
                visualMesh1->setVertexPosition(i, vertexPos3);
            }
            for (int i = 0; i < visualMesh2->getNumVertices(); ++i)
            {
                visualMesh2->setVertexPosition(i, vertexPos3);
            }
            break;
        case 1:   ////< XZH show injection
            for (int i = 0; i < visualMesh1->getNumVertices(); ++i)
            {
                vertexPos3 = visualMesh1->getInitialVertexPosition(i);  // use the inital position * Omni transform   //->getVertexPosition(i);
                //vertexPos3 += Vec3d(1.0, 0, -0.0) + moveLine;   ////< XZH  Vec3d(1.4, 0, -0.8);
                vertexPos4.x() = vertexPos3.x();
                vertexPos4.y() = vertexPos3.y();
                vertexPos4.z() = vertexPos3.z();

                vertexPos4.applyOnTheLeft(transformDev);
                vertexPos3.x() = vertexPos4.x();
                vertexPos3.y() = vertexPos4.y();
                vertexPos3.z() = vertexPos4.z();

                visualMesh1->setVertexPosition(i, vertexPos3 );
            }
            //visualMesh1->setRotation(r);
            //visualMesh1->setTranslation(p);
            vertexPos3 = Vec3d::Zero();
            for (int i = 0; i < visualMesh->getNumVertices(); ++i)
            {
                visualMesh->setVertexPosition(i, vertexPos3);
            }
            for (int i = 0; i < visualMesh2->getNumVertices(); ++i)
            {
                visualMesh2->setVertexPosition(i, vertexPos3);
            }
            break;
        case 2:   ////< XZH show cutting
            for (int i = 0; i < visualMesh2->getNumVertices(); ++i)
            {
                vertexPos3 = visualMesh2->getInitialVertexPosition(i);  // use the inital position * Omni transform   //->getVertexPosition(i);
                //vertexPos3 += Vec3d(1.0, 0, -0.0) + moveLine;   ////< XZH  Vec3d(1.4, 0, -0.8);
                vertexPos4.x() = vertexPos3.x();
                vertexPos4.y() = vertexPos3.y();
                vertexPos4.z() = vertexPos3.z();

                vertexPos4.applyOnTheLeft(transformDev);
                vertexPos3.x() = vertexPos4.x();
                vertexPos3.y() = vertexPos4.y();
                vertexPos3.z() = vertexPos4.z();

                visualMesh2->setVertexPosition(i, vertexPos3);
            }
            //visualMesh2->setRotation(r);
            //visualMesh2->setTranslation(p);
            vertexPos3 = Vec3d::Zero();
            for (int i = 0; i < visualMesh->getNumVertices(); ++i)
            {
                visualMesh->setVertexPosition(i, vertexPos3);
            }
            for (int i = 0; i < visualMesh1->getNumVertices(); ++i)
            {
                visualMesh1->setVertexPosition(i, vertexPos3);
            }
            break;
        }

        for (int i = 0; i < collidingMesh->getNumVertices(); ++i)
        {
            vertexPos3 = collidingMesh->getInitialVertexPosition(i);  // use the inital position * Omni transform   //->getVertexPosition(i);
            vertexPos4.x() = vertexPos3.x();
            vertexPos4.y() = vertexPos3.y();
            vertexPos4.z() = vertexPos3.z();

            vertexPos4.applyOnTheLeft(transformDev);
            vertexPos3.x() = vertexPos4.x();
            vertexPos3.y() = vertexPos4.y();
            vertexPos3.z() = vertexPos4.z();

            collidingMesh->setVertexPosition(i, vertexPos3);
        }

        m_pbdVCObjectMarking->applyCollidingToPhysics();
        m_pbdVCObjectMarking->updatePbdStates();
        m_pbdVCObjectInject->applyCollidingToPhysics();
        m_pbdVCObjectInject->updatePbdStates();
        m_pbdVCObject->applyCollidingToPhysics();
        m_pbdVCObject->updatePbdStates();

        // update tool info w.r.t obj
        Eigen::Matrix3d rotMat; //  = r.toRotationMatrix();
        //rotMat(0, 0) = transformDev(0, 0);   rotMat(0, 1) = transformDev(0, 1);   rotMat(0, 2) = transformDev(0, 2);  rotMat(0, 3) = transformDev(0, 3);
        //rotMat(1, 0) = transformDev(1, 0);    rotMat(1, 1) = transformDev(1, 1);    rotMat(1, 2) = transformDev(1, 2);   rotMat(1, 3) = transformDev(1, 3);
        //rotMat(2, 0) = transformDev(2, 0);   rotMat(2, 1) = transformDev(2, 1);   rotMat(2, 2) = transformDev(2, 2);  rotMat(2, 3) = transformDev(2, 3);
        //rotMat(3, 0) = 0;                    rotMat(3, 1) = 0;                    rotMat(3, 2) = 0;                    rotMat(3, 3) = 1;
        rotMat(0, 0) = transformDev(0, 0);   rotMat(0, 1) = transformDev(0, 1);   rotMat(0, 2) = transformDev(0, 2);
        rotMat(1, 0) = transformDev(1, 0);    rotMat(1, 1) = transformDev(1, 1);    rotMat(1, 2) = transformDev(1, 2);
        rotMat(2, 0) = transformDev(2, 0);   rotMat(2, 1) = transformDev(2, 1);   rotMat(2, 2) = transformDev(2, 2);
        auto& tool = m_pbdVCObjectMarking->getControllerTool();
        auto& tool1 = m_pbdVCObjectInject->getControllerTool();
        auto& tool2 = m_pbdVCObject->getControllerTool();
        tool.hapticRot = rotMat;  // transform R
        tool.pRotArr = tool.rotArr;
        tool.rotArr = rotMat; // r.toRotationMatrix();  // r is equal hapticRot
        tool.pGloVec[0] = tool.gloVec[0];
        tool.pGloVec[1] = tool.gloVec[1];
        tool.gloVec[0] = rotMat* collidingMesh->getInitialVertexPosition(0); // tool[t].rotArr[0] * tool[t].locVec[i].x + tool[t].rotArr[3] * tool[t].locVec[i].y + tool[t].rotArr[6] * tool[t].locVec[i].z;
        tool.gloVec[1] = rotMat* collidingMesh->getInitialVertexPosition(1); // should use the original pos
        // update tool position
        tool.pHapticPos = tool.hapticPos;
        tool.hapticPos = p;  // transform  T
        tool.jointPos = tool.hapticPos; // is equal to tool.hapticPos;
        tool.pDrawPos = tool.finalPos;
        tool.drawPos = tool.jointPos;
        tool.button[0] = m_trackingController->getDeviceClient()->getButton(0);
        tool.button[1] = m_trackingController->getDeviceClient()->getButton(1);

        ////< XZH injection
        tool1.hapticRot = rotMat;
        tool1.pRotArr = tool1.rotArr;
        tool1.rotArr = rotMat;
        tool1.pGloVec[0] = tool1.gloVec[0];
        tool1.pGloVec[1] = tool1.gloVec[1];
        tool1.gloVec[0] = rotMat* collidingMesh->getInitialVertexPosition(0);
        tool1.gloVec[1] = rotMat* collidingMesh->getInitialVertexPosition(1);
        // update tool position
        tool1.pHapticPos = tool1.hapticPos;
        tool1.hapticPos = p;
        tool1.jointPos = tool1.hapticPos;
        tool1.pDrawPos = tool1.finalPos;
        tool1.drawPos = tool1.jointPos;
        tool1.button[0] = m_trackingController->getDeviceClient()->getButton(0);
        tool1.button[1] = m_trackingController->getDeviceClient()->getButton(1);

        ////< XZH cutting
        tool2.hapticRot = rotMat;
        tool2.pRotArr = tool2.rotArr;
        tool2.rotArr = rotMat;
        tool2.pGloVec[0] = tool2.gloVec[0];
        tool2.pGloVec[1] = tool2.gloVec[1];
        tool2.gloVec[0] = rotMat* collidingMesh->getInitialVertexPosition(0);
        tool2.gloVec[1] = rotMat* collidingMesh->getInitialVertexPosition(1);
        // update tool position
        tool2.pHapticPos = tool2.hapticPos;
        tool2.hapticPos = p;
        tool2.jointPos = tool2.hapticPos;
        tool2.pDrawPos = tool2.finalPos;
        tool2.drawPos = tool2.jointPos;
        tool2.button[0] = m_trackingController->getDeviceClient()->getButton(0);
        tool2.button[1] = m_trackingController->getDeviceClient()->getButton(1);
        // update end
    }

	void
		PBDSceneObjectController::updateControlledObjects()
	{
		if (!m_trackingController->isTrackerUpToDate())
		{
			if (!m_trackingController->updateTrackingData())
			{
				LOG(WARNING) << "PBDSceneObjectController::updateControlledObjects warning: could not update tracking info.";
				return;
			}
		}

		Vec3d p = m_trackingController->getPosition();
		Quatd r = m_trackingController->getRotation();
        
		//FILE* output;
		//output = fopen("G:/devicepos.txt","a");
		//fprintf(output, "device: %f %f %f\n", p.x(), p.y(), p.z());
		////fprintf(output, "device: %f %f %f %f\n", r.x(), r.y(), r.z()); //  , r.w(), r.matrix(0, 0));  // r.matrix
		//fclose(output);

		//// Update visual geometry   changed to the below method to update visual geometry
        //m_pbdVCObject->getVisualGeometry()->setTranslation(m_trackingController->getPosition());
        //m_pbdVCObject->getVisualGeometry()->setRotation(m_trackingController->getRotation());

        // TEST
        Eigen::Matrix3d temp = r.toRotationMatrix();
        RigidTransform3d m_transformTmp = RigidTransform3d::Identity(); ///> Transform
        m_transformTmp.linear() = r.toRotationMatrix();
        m_transformTmp.translation() = p;
        // TEST END

        //// update colliding geometry
        //m_pbdVCObject->getCollidingGeometry()->setTranslation(m_trackingController->getPosition());
        //m_pbdVCObject->getCollidingGeometry()->setRotation(m_trackingController->getRotation());

		computeTransform2(p, r, transformDev);

        Vec4d vertexPos4;
        vertexPos4.w() = 1;
        Vec3d vertexPos3;

        Vec3d moveLine = Vec3d(-9.093, 1.6348, -8.859199524);
        
        // TEST END

        auto collidingMesh = std::dynamic_pointer_cast<PointSet>(m_pbdVCObject->getCollidingGeometry());
        // TEST visual geometry just for testCuttingCubeDenseHaptic, otherwise should comment
        auto visualMesh = std::dynamic_pointer_cast<PointSet>(m_pbdVCObject->getVisualGeometry());
        visualMesh->setRotation(r);
        visualMesh->setTranslation(p);
        //Vec3d pos0 = collidingMesh->getInitialVertexPosition(0);
        //auto& posList = visualMesh->getInitialVertexPositions();
        //std::vector<Vec3d> dirGapVisualColliding;
        //for (int i = 0; i < posList.size(); i++)
        //{
        //    dirGapVisualColliding.push_back(posList[i] - pos0);
        //}

		for (int i = 0; i < collidingMesh->getNumVertices(); ++i)
		{
            vertexPos3 = collidingMesh->getInitialVertexPosition(i);  // use the inital position * Omni transform   //->getVertexPosition(i);
			vertexPos4.x() = vertexPos3.x();
			vertexPos4.y() = vertexPos3.y();
			vertexPos4.z() = vertexPos3.z();

			vertexPos4.applyOnTheLeft(transformDev);
			vertexPos3.x() = vertexPos4.x();
			vertexPos3.y() = vertexPos4.y();
			vertexPos3.z() = vertexPos4.z();

			collidingMesh->setVertexPosition(i, vertexPos3);
		}

          ////< XZH for visual no need here
        //for (int i = 0; i < visualMesh->getNumVertices(); ++i)
        //{
        //    vertexPos3 = visualMesh->getInitialVertexPosition(i);  // use the inital position * Omni transform   //->getVertexPosition(i);
        //    //vertexPos4.x() = vertexPos3.x();
        //    //vertexPos4.y() = vertexPos3.y();
        //    //vertexPos4.z() = vertexPos3.z();

        //    //vertexPos4.applyOnTheLeft(transformDev);
        //    //vertexPos3.x() = vertexPos4.x();
        //    //vertexPos3.y() = vertexPos4.y();
        //    //vertexPos3.z() = vertexPos4.z();

        //    vertexPos3 = r.toRotationMatrix()*vertexPos3;
        //    vertexPos3 += p;

        //    visualMesh->setVertexPosition(i, vertexPos3);
        //}

		m_pbdVCObject->applyCollidingToPhysics();
		m_pbdVCObject->updatePbdStates();

        // update tool info w.r.t obj
        Eigen::Matrix3d rotMat; //  = r.toRotationMatrix();
        //rotMat(0, 0) = transformDev(0, 0);   rotMat(0, 1) = transformDev(0, 1);   rotMat(0, 2) = transformDev(0, 2);  rotMat(0, 3) = transformDev(0, 3);
        //rotMat(1, 0) = transformDev(1, 0);    rotMat(1, 1) = transformDev(1, 1);    rotMat(1, 2) = transformDev(1, 2);   rotMat(1, 3) = transformDev(1, 3);
        //rotMat(2, 0) = transformDev(2, 0);   rotMat(2, 1) = transformDev(2, 1);   rotMat(2, 2) = transformDev(2, 2);  rotMat(2, 3) = transformDev(2, 3);
        //rotMat(3, 0) = 0;                    rotMat(3, 1) = 0;                    rotMat(3, 2) = 0;                    rotMat(3, 3) = 1;
        rotMat(0, 0) = transformDev(0, 0);   rotMat(0, 1) = transformDev(0, 1);   rotMat(0, 2) = transformDev(0, 2);
        rotMat(1, 0) = transformDev(1, 0);    rotMat(1, 1) = transformDev(1, 1);    rotMat(1, 2) = transformDev(1, 2);
        rotMat(2, 0) = transformDev(2, 0);   rotMat(2, 1) = transformDev(2, 1);   rotMat(2, 2) = transformDev(2, 2);
        auto& tool = m_pbdVCObject->getControllerTool();
        tool.hapticRot = rotMat;  // transform R
        tool.pRotArr = tool.rotArr;
        tool.rotArr = rotMat; // r.toRotationMatrix();  // r is equal hapticRot
        tool.pGloVec[0] = tool.gloVec[0];
        tool.pGloVec[1] = tool.gloVec[1];
        tool.gloVec[0] = rotMat* collidingMesh->getInitialVertexPosition(0); // tool[t].rotArr[0] * tool[t].locVec[i].x + tool[t].rotArr[3] * tool[t].locVec[i].y + tool[t].rotArr[6] * tool[t].locVec[i].z;
        tool.gloVec[1] = rotMat* collidingMesh->getInitialVertexPosition(1); // should use the original pos
        // update tool position
        tool.pHapticPos = tool.hapticPos;
        tool.hapticPos = p;  // transform  T
        tool.jointPos = tool.hapticPos; // is equal to tool.hapticPos;
        tool.pDrawPos = tool.finalPos;
        tool.drawPos = tool.jointPos;

        tool.button[0]=m_trackingController->getDeviceClient()->getButton(0);
        tool.button[1] = m_trackingController->getDeviceClient()->getButton(1);

        // update end
	}

    void
        PBDSceneObjectController::updateControlledObjects2()
    {
        if (!m_trackingController->isTrackerUpToDate())
        {
            if (!m_trackingController->updateTrackingData())
            {
                LOG(WARNING) << "PBDSceneObjectController::updateControlledObjects warning: could not update tracking info.";
                return;
            }
        }

        Vec3d p = m_trackingController->getPosition();
        Quatd r = m_trackingController->getRotation();

        // TEST
        Eigen::Matrix3d temp = r.toRotationMatrix();
        RigidTransform3d m_transformTmp = RigidTransform3d::Identity(); ///> Transform
        m_transformTmp.linear() = r.toRotationMatrix();
        m_transformTmp.translation() = p;
        // TEST END

        //// update colliding geometry
        //m_pbdVCObject->getCollidingGeometry()->setTranslation(m_trackingController->getPosition());
        //m_pbdVCObject->getCollidingGeometry()->setRotation(m_trackingController->getRotation());

        computeTransform2(p, r, transformDev);

        Vec4d vertexPos4;
        vertexPos4.w() = 1;
        Vec3d vertexPos3;

        Vec3d moveLine = Vec3d(-9.093, 1.6348, -8.859199524);

        // TEST END

        auto collidingMesh = std::dynamic_pointer_cast<PointSet>(m_pbdVCObject->getCollidingGeometry());
        // TEST visual geometry just for testCuttingCubeDenseHaptic, otherwise should comment
        auto visualMesh = std::dynamic_pointer_cast<PointSet>(m_pbdVCObject->getVisualGeometry());
        for (int i = 0; i < collidingMesh->getNumVertices(); ++i)
        {
            vertexPos3 = collidingMesh->getInitialVertexPosition(i);  // use the inital position * Omni transform   //->getVertexPosition(i);
            vertexPos4.x() = vertexPos3.x();
            vertexPos4.y() = vertexPos3.y();
            vertexPos4.z() = vertexPos3.z();

            vertexPos4.applyOnTheLeft(transformDev);
            vertexPos3.x() = vertexPos4.x();
            vertexPos3.y() = vertexPos4.y();
            vertexPos3.z() = vertexPos4.z();

            collidingMesh->setVertexPosition(i, vertexPos3);
        }

        for (int i = 0; i < visualMesh->getNumVertices(); ++i)
        {
            vertexPos3 = visualMesh->getInitialVertexPosition(i);  // use the inital position * Omni transform   //->getVertexPosition(i);
            vertexPos3 = r.toRotationMatrix()*vertexPos3;
            vertexPos3 += p;

            visualMesh->setVertexPosition(i, vertexPos3);
        }
        

        m_pbdVCObject->applyCollidingToPhysics();
        m_pbdVCObject->updatePbdStates();

        // update tool info w.r.t obj
        Eigen::Matrix3d rotMat; //  = r.toRotationMatrix();
        //rotMat(0, 0) = transformDev(0, 0);   rotMat(0, 1) = transformDev(0, 1);   rotMat(0, 2) = transformDev(0, 2);  rotMat(0, 3) = transformDev(0, 3);
        //rotMat(1, 0) = transformDev(1, 0);    rotMat(1, 1) = transformDev(1, 1);    rotMat(1, 2) = transformDev(1, 2);   rotMat(1, 3) = transformDev(1, 3);
        //rotMat(2, 0) = transformDev(2, 0);   rotMat(2, 1) = transformDev(2, 1);   rotMat(2, 2) = transformDev(2, 2);  rotMat(2, 3) = transformDev(2, 3);
        //rotMat(3, 0) = 0;                    rotMat(3, 1) = 0;                    rotMat(3, 2) = 0;                    rotMat(3, 3) = 1;
        rotMat(0, 0) = transformDev(0, 0);   rotMat(0, 1) = transformDev(0, 1);   rotMat(0, 2) = transformDev(0, 2);
        rotMat(1, 0) = transformDev(1, 0);    rotMat(1, 1) = transformDev(1, 1);    rotMat(1, 2) = transformDev(1, 2);
        rotMat(2, 0) = transformDev(2, 0);   rotMat(2, 1) = transformDev(2, 1);   rotMat(2, 2) = transformDev(2, 2);
        auto& tool = m_pbdVCObject->getControllerTool();
        tool.hapticRot = rotMat;  // transform R
        tool.pRotArr = tool.rotArr;
        tool.rotArr = rotMat; // r.toRotationMatrix();  // r is equal hapticRot
        tool.pGloVec[0] = tool.gloVec[0];
        tool.pGloVec[1] = tool.gloVec[1];
        tool.gloVec[0] = rotMat* collidingMesh->getInitialVertexPosition(0); // tool[t].rotArr[0] * tool[t].locVec[i].x + tool[t].rotArr[3] * tool[t].locVec[i].y + tool[t].rotArr[6] * tool[t].locVec[i].z;
        tool.gloVec[1] = rotMat* collidingMesh->getInitialVertexPosition(1); // should use the original pos
        // update tool position
        tool.pHapticPos = tool.hapticPos;
        tool.hapticPos = p;  // transform  T
        tool.jointPos = tool.hapticPos; // is equal to tool.hapticPos;
        tool.pDrawPos = tool.finalPos;
        tool.drawPos = tool.jointPos;

        tool.button[0] = m_trackingController->getDeviceClient()->getButton(0);
        tool.button[1] = m_trackingController->getDeviceClient()->getButton(1);

        // update end
    }

    void
        PBDSceneObjectController::updateControlledObjects3()
    {
        auto collidingMesh = std::dynamic_pointer_cast<PointSet>(m_pbdVCObject->getCollidingGeometry());
        // TEST visual geometry just for testCuttingCubeDenseHaptic, otherwise should comment
        auto visualMesh = std::dynamic_pointer_cast<PointSet>(m_pbdVCObject->getVisualGeometry());
        for (int i = 0; i < collidingMesh->getNumVertices(); ++i)
        {
            collidingMesh->setVertexPosition(i, Vec3d::Zero());
        }
        for (int i = 0; i < visualMesh->getNumVertices(); ++i)
        {
            visualMesh->setVertexPosition(i, Vec3d::Zero());
        }
    }

	void
		PBDSceneObjectController::applyForces()
	{
		Vec3d force(0.0, 0.0, 0.0);
        //Vec3d finalPos = Vec3d::Zero();
        bool isCD = false;
        bool isInjected = false;
        bool isStatic = false;
		if (auto collidingObject = dynamic_cast<CollidingObject*>(m_pbdVCObject.get()))
		{
			force+=collidingObject->getForce();
            //finalPos = collidingObject->getFinalPos();
            isCD = collidingObject->getIsCD();
            isInjected = collidingObject->getIsInjection();
            isStatic = collidingObject->getIsStatic();
            //printf("force sphere from PBDcontroller: %f %f %f %f\n", force.x(), force.y(), force.z(), force.norm());
		}
        
		//m_trackingController->getDeviceClient()->setForce(force);
        
        //// Adding for force feedback by XZH
        auto& tool = m_pbdVCObject->getControllerTool();
        Vec3d finalPos;
        if (isInjected) finalPos = tool.injectPos; // Injection
        else finalPos = tool.finalPos; // CCD
        if (isStatic) 
        {
            m_trackingController->getDeviceClient()->setForceFeedbackStatic(force, isStatic);
        }
        else // CCD
        {
            m_trackingController->getDeviceClient()->setForceFeedbackParams(finalPos, isCD, isInjected);
        }  
        //// END
	}

	void
		PBDSceneObjectController::computeTransform(Vec3d& pos, Quatd& quat, Eigen::Matrix4d& t){
		auto scaling = m_pbdVCObject->getCollidingGeometry()->getScaling();  //  m_collidingGeometry->getScaling();
		auto angleAxis = Rotd(quat);  // get Axis and angle? from quat

		t.setIdentity();
		t(0, 0) = scaling;
		t(1, 1) = scaling;
		t(2, 2) = scaling;

		Eigen::Affine3d rot =
			Eigen::Affine3d(
			Eigen::AngleAxisd(
            angleAxis.angle() * 180 / 3.14159265358979323846,
			Eigen::Vector3d(angleAxis.axis()[0], angleAxis.axis()[1], angleAxis.axis()[2])
			)
			);
		Eigen::Affine3d trans(Eigen::Translation3d(Eigen::Vector3d(pos[0], pos[1], pos[2])));

		t *= trans.matrix();
		t *= rot.matrix();
	}

    void
        PBDSceneObjectController::computeTransform2(Vec3d& pos, Quatd& quat, Eigen::Matrix4d& t){
        auto scaling = m_pbdVCObject->getCollidingGeometry()->getScaling();  //  m_collidingGeometry->getScaling();
        auto angleAxis = Rotd(quat);  // get Axis and angle? from quat
        Eigen::Matrix3d matRot = quat.toRotationMatrix();

        t.setIdentity();

        t(0, 0) = matRot(0, 0);   t(0, 1) = matRot(0, 1);   t(0, 2) = matRot(0, 2);  t(0, 3) = pos.x();
        t(1, 0) = matRot(1, 0);    t(1, 1) = matRot(1, 1);    t(1, 2) = matRot(1, 2);   t(1, 3) = pos.y();
        t(2, 0) = matRot(2, 0);   t(2, 1) = matRot(2, 1);   t(2, 2) = matRot(2, 2);  t(2, 3) = pos.z();
        t(3, 0) = 0;                    t(3, 1) = 0;                    t(3, 2) = 0;                    t(3, 3) = 1;
    }

    void
        PBDSceneObjectController::initTools()
    {
        m_trackingController->getDeviceClient()->initDevice();
        double scaling = m_trackingController->getTranslationScaling();
        m_trackingController->getDeviceClient()->setTranslationScaling(scaling);
        auto& tool = m_pbdVCObject->getControllerTool();
        auto& tool0 = m_pbdVCObjectMarking->getControllerTool();
        auto& tool1 = m_pbdVCObjectInject->getControllerTool();
        tool.touchStartSphere = false;
        tool.ccdDistance = 10.0;
        tool.startPos = Vec3d::Zero();

        tool.filterSize = 100;
        tool.numberOfForceInFilter = 0;
        tool.arrForce.resize(100, Vec3d::Zero());

        tool.forceCoeff = 1;

        float scale = 1.0;// scale factor of the line-segment tool

        tool.nbrNode = 2; // two node for one line
        tool.nbrActSeg = 1;
        tool.nbrActTip = 1;

        tool.nodeIdxOfSeg[0][0] = 0;
        tool.nodeIdxOfSeg[0][1] = 1;

        tool.nodeIdxOfSeg[1][0] = 1;
        tool.nodeIdxOfSeg[1][1] = 2;

        tool.belongToCylinder[0] = 0;
        tool.belongToCylinder[1] = 1;

        tool.gloVec[0] = Vec3d::Zero();
        tool.gloVec[1] = Vec3d::Zero();
        tool.pGloVec[0] = Vec3d::Zero();
        tool.pGloVec[1] = Vec3d::Zero();

        tool.springConstant = 5;
        tool.velocityResistance = 1;
        tool.hapticVelResistance = 1;

        tool.nodeIdxOfTip[0] = 0;

        tool.diffPos = Vec3d::Zero();
        tool.diffPosPre = Vec3d::Zero();

        tool.cullingThickness[0] =1.5;  // for finding candidate shoud a big than thickness
        tool.cullingThickness[1] =1.5;

        tool.nodeIdxOfTip[0] = 0;

        tool.diffPos = Vec3d::Zero();
        tool.diffPosPre = Vec3d::Zero();

        double rato = 0.01;
        tool.oriVec[0] = Vec3d(-16.4, -1.85, 9.15)*rato;
        tool.oriVec[1] = Vec3d(-9.3, -1.4, 9.05)*rato;

        for (int i = 0; i < tool.nbrNode; i++)
        {
            tool.locVec[i] = tool.oriVec[i];
            tool.pLocVec[i] = tool.oriVec[i];
        }

        tool.thickness[0][0][0] = 0.5; // should smaller than cullingthickness
        tool.thickness[1][0][0] = 0.8;

        tool.thickness[0][1][0] = 0.1;
        tool.thickness[1][1][0] = 0.1;

        tool.thickness[0][0][1] = 0.6;
        tool.thickness[1][0][1] = 0.9;

        tool.thickness[0][1][1] = 0.2;
        tool.thickness[1][1][1] = 0.2;

          ////< XZH tool 0
        tool0.touchStartSphere = false;
        tool0.ccdDistance = 10.0;
        tool0.startPos = Vec3d::Zero();

        tool0.filterSize = 100;
        tool0.numberOfForceInFilter = 0;
        tool0.arrForce.resize(100, Vec3d::Zero());

        tool0.forceCoeff = 1;

        scale = 1.0;// scale factor of the line-segment tool0

        tool0.nbrNode = 2; // two node for one line
        tool0.nbrActSeg = 1;
        tool0.nbrActTip = 1;

        tool0.nodeIdxOfSeg[0][0] = 0;
        tool0.nodeIdxOfSeg[0][1] = 1;

        tool0.nodeIdxOfSeg[1][0] = 1;
        tool0.nodeIdxOfSeg[1][1] = 2;

        tool0.belongToCylinder[0] = 0;
        tool0.belongToCylinder[1] = 1;

        tool0.gloVec[0] = Vec3d::Zero();
        tool0.gloVec[1] = Vec3d::Zero();
        tool0.pGloVec[0] = Vec3d::Zero();
        tool0.pGloVec[1] = Vec3d::Zero();

        tool0.springConstant = 5;
        tool0.velocityResistance = 1;
        tool0.hapticVelResistance = 1;

        tool0.nodeIdxOfTip[0] = 0;

        tool0.diffPos = Vec3d::Zero();
        tool0.diffPosPre = Vec3d::Zero();

        tool0.cullingThickness[0] = 1.5;  // for finding candidate shoud a big than thickness
        tool0.cullingThickness[1] = 1.5;

        tool0.nodeIdxOfTip[0] = 0;

        tool0.diffPos = Vec3d::Zero();
        tool0.diffPosPre = Vec3d::Zero();


        tool0.oriVec[0] = Vec3d(-16.4, -1.85, 9.15)*rato;
        tool0.oriVec[1] = Vec3d(-9.3, -1.4, 9.05)*rato;

        for (int i = 0; i < tool0.nbrNode; i++)
        {
            tool0.locVec[i] = tool0.oriVec[i];
            tool0.pLocVec[i] = tool0.oriVec[i];
        }

        tool0.thickness[0][0][0] = 0.5; // should smaller than cullingthickness
        tool0.thickness[1][0][0] = 0.8;

        tool0.thickness[0][1][0] = 0.1;
        tool0.thickness[1][1][0] = 0.1;

        tool0.thickness[0][0][1] = 0.6;
        tool0.thickness[1][0][1] = 0.9;

        tool0.thickness[0][1][1] = 0.2;
        tool0.thickness[1][1][1] = 0.2;

          ////< XZH tool1
        tool1.touchStartSphere = false;
        tool1.ccdDistance = 10.0;
        tool1.startPos = Vec3d::Zero();

        tool1.filterSize = 100;
        tool1.numberOfForceInFilter = 0;
        tool1.arrForce.resize(100, Vec3d::Zero());

        tool1.forceCoeff = 1;

        scale = 1.0;// scale factor of the line-segment tool1

        tool1.nbrNode = 2; // two node for one line
        tool1.nbrActSeg = 1;
        tool1.nbrActTip = 1;

        tool1.nodeIdxOfSeg[0][0] = 0;
        tool1.nodeIdxOfSeg[0][1] = 1;

        tool1.nodeIdxOfSeg[1][0] = 1;
        tool1.nodeIdxOfSeg[1][1] = 2;

        tool1.belongToCylinder[0] = 0;
        tool1.belongToCylinder[1] = 1;

        tool1.gloVec[0] = Vec3d::Zero();
        tool1.gloVec[1] = Vec3d::Zero();
        tool1.pGloVec[0] = Vec3d::Zero();
        tool1.pGloVec[1] = Vec3d::Zero();

        tool1.springConstant = 5;
        tool1.velocityResistance = 1;
        tool1.hapticVelResistance = 1;

        tool1.nodeIdxOfTip[0] = 0;

        tool1.diffPos = Vec3d::Zero();
        tool1.diffPosPre = Vec3d::Zero();

        tool1.cullingThickness[0] = 1.5;  // for finding candidate shoud a big than thickness
        tool1.cullingThickness[1] = 1.5;

        tool1.nodeIdxOfTip[0] = 0;

        tool1.diffPos = Vec3d::Zero();
        tool1.diffPosPre = Vec3d::Zero();


        tool1.oriVec[0] = Vec3d(-16.4, -1.85, 9.15)*rato;
        tool1.oriVec[1] = Vec3d(-9.3, -1.4, 9.05)*rato;

        for (int i = 0; i < tool1.nbrNode; i++)
        {
            tool1.locVec[i] = tool1.oriVec[i];
            tool1.pLocVec[i] = tool1.oriVec[i];
        }

        tool1.thickness[0][0][0] = 0.5; // should smaller than cullingthickness
        tool1.thickness[1][0][0] = 0.8;

        tool1.thickness[0][1][0] = 0.1;
        tool1.thickness[1][1][0] = 0.1;

        tool1.thickness[0][0][1] = 0.6;
        tool1.thickness[1][0][1] = 0.9;

        tool1.thickness[0][1][1] = 0.2;
        tool1.thickness[1][1][1] = 0.2;
    }

} // imstk
