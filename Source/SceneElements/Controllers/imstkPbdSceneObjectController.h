/*=========================================================================

Library: iMSTK

Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
& Imaging in Medicine, Rensselaer Polytechnic Institute.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0.txt

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

=========================================================================*/

#ifndef imstkPBDSceneObjectController_h
#define imstkPBDSceneObjectController_h

#include "imstkSceneObjectControllerBase.h"
#include "imstkDeviceTracker.h"
#include "imstkSceneObject.h"
#include "imstkPbdVirtualCouplingObject.h"

#include <memory>

namespace imstk
{

	///
	/// \class PBDSceneObjectController
	///
	/// \brief This class implements once tracking controller controlling one PBD scnene object
	///
	class PBDSceneObjectController : public SceneObjectControllerBase
	{
	public:
		///
		/// \brief Constructor
		///
		PBDSceneObjectController(std::shared_ptr<PbdVirtualCouplingObject> sceneObject,
			std::shared_ptr<DeviceTracker> trackingController) :
			m_trackingController(trackingController),
			m_pbdVCObject(sceneObject) {}

        ///
        /// \brief introduce the function XZH
        ///
        PBDSceneObjectController(
            std::shared_ptr<PbdVirtualCouplingObject> sceneObject0,
            std::shared_ptr<PbdVirtualCouplingObject> sceneObject1,
            std::shared_ptr<PbdVirtualCouplingObject> sceneObject,
            std::shared_ptr<DeviceTracker> trackingController) :
            m_trackingController(trackingController),
            m_pbdVCObjectMarking(sceneObject0),
            m_pbdVCObjectInject(sceneObject1),
            m_pbdVCObject(sceneObject)
        {}

		PBDSceneObjectController() = delete;

		///
		/// \brief Destructor
		///
		~PBDSceneObjectController() = default;

		///
		/// \brief Initialize offset based on object geometry
		///
		void initOffsets();

		///
		/// \brief Update controlled scene object using latest tracking information
		///
		void updateControlledObjects() override;
        void updateControlledObjects2();
        void updateControlledObjects3();  ////< XZH when multiple tools and not active
        void updateControlledObjectsALL();   ////< XZH 
        void switchTools(int tp);   ////< XZH tp is procedures 0 1 2 3, 2 is cutting
        void switchToolsXZH(int tp);   ////< XZH tp is procedures 0 1 2 3, 2 is cutting

		///
		/// \brief Apply forces to the haptic device
		///
		void applyForces() override;

		///
		/// \brief Set the tracker to out-of-date
		///
		inline void setTrackerToOutOfDate() override { m_trackingController->setTrackerToOutOfDate(); }

		// xzh
		///
		/// \brief compute (offsetted)transform matrix
		///
		void computeTransform(Vec3d& p, Quatd& r, Eigen::Matrix4d& t);
        void computeTransform2(Vec3d& p, Quatd& r, Eigen::Matrix4d& t);

		///
		/// \brief Get/Set controlled scene object
		///
		inline std::shared_ptr<PbdVirtualCouplingObject> getControlledSceneObject() const { return m_pbdVCObject; }
		inline void setControlledSceneObject(std::shared_ptr<PbdVirtualCouplingObject> so) { m_pbdVCObject = so; }

		///
		/// \brief Get/Set tracking controller
		///
		inline std::shared_ptr<DeviceTracker> getTrackingController() const { return m_trackingController; }
		inline void setTrackingController(std::shared_ptr<DeviceTracker> controller) { m_trackingController = controller; }

        ///
        /// \ add tool information for continuous collision detection
        ///
        void initTools();

        ///
        /// \brief introduce the function when multiple tools
        ///
        void setActived(bool flag){ isActived = flag; }
        void setToolIndex(int idx){ toolIndex = idx; }
        bool& getActived() { return isActived; }
        int& getToolIndex(){ return toolIndex; }

	protected:
		std::shared_ptr<DeviceTracker> m_trackingController; ///< Device tracker
		//std::shared_ptr<SceneObject> m_sceneObject;          ///< SceneObject controlled by the Tracker
		std::shared_ptr<PbdVirtualCouplingObject> m_pbdVCObject; // pbdobject controlled by the tracker Cutter Tool XZH
        std::shared_ptr<PbdVirtualCouplingObject> m_pbdVCObjectInject;
        std::shared_ptr<PbdVirtualCouplingObject> m_pbdVCObjectMarking;

		bool m_forceModified;
		Vec3d m_force = Vec3d::Zero();
		Eigen::Matrix4d transformDev = Eigen::Matrix4d();

        bool isActived = false;  ////< XZH which is actived when multiple
        int toolIndex =0;
	};

} // imstk
#endif // ifndef imstkPBDSceneObjectController_h