/*=========================================================================

Library: iMSTK

Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
& Imaging in Medicine, Rensselaer Polytechnic Institute.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0.txt

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

=========================================================================*/

#ifndef imstkXZHTool_h
#define imstkXZHTool_h

#include "imstkCameraController.h"
#include "imstkPbdVirtualCouplingObject.h"
#include "imstkLight.h"

namespace imstk
{
    ///
    /// \class XZHTool
    ///
    /// \brief
    ///
    class XZHTool : public CameraController
    {
    public:
        XZHTool(
            std::shared_ptr<PbdVirtualCouplingObject> tool0,
            std::shared_ptr<PbdVirtualCouplingObject> tool1,
            std::shared_ptr<PbdVirtualCouplingObject> tool2,
            Camera& camera,
            std::shared_ptr<DeviceClient> deviceClient) :
            CameraController(camera, deviceClient),
            m_ToolMarking(tool0),
            m_ToolInjection(tool1),
            m_ToolCutting(tool2),
            m_deviceClient(deviceClient)
        {
            
        }

		XZHTool(
			std::shared_ptr<PbdVirtualCouplingObject> tool0,
			std::shared_ptr<PbdVirtualCouplingObject> tool1,
			std::shared_ptr<PbdVirtualCouplingObject> tool2,
			std::shared_ptr<PbdVirtualCouplingObject> overtube,
			std::shared_ptr<PointLight> light,
			Camera& camera,
			std::shared_ptr<DeviceClient> deviceClient) :
			CameraController(camera, deviceClient),
			m_ToolMarking(tool0),
			m_ToolInjection(tool1),
			m_ToolCutting(tool2),
			m_Overtube(overtube),
			m_Light(light),
			m_deviceClient(deviceClient)
		{}

		XZHTool(
			std::shared_ptr<PbdVirtualCouplingObject> tool0,
			std::shared_ptr<PbdVirtualCouplingObject> tool1,
			std::shared_ptr<PbdVirtualCouplingObject> tool2,
			std::shared_ptr<PbdVirtualCouplingObject> overtube,
			std::shared_ptr<PbdVirtualCouplingObject> surroundingObj,
			std::shared_ptr<PointLight> light,
			Camera& camera,
			std::shared_ptr<DeviceClient> deviceClient) :
			CameraController(camera, deviceClient),
			m_ToolMarking(tool0),
			m_ToolInjection(tool1),
			m_ToolCutting(tool2),
			m_Overtube(overtube),
			m_Surroundings(surroundingObj),
			m_Light(light),
			m_deviceClient(deviceClient)
		{}

        ///
        /// \brief Destructor
        ///
        ~XZHTool() = default;

        ///
        /// \brief introduce the function
        ///
        void computeTransform(Vec3d& pos, Quatd& quat, Eigen::Matrix4d& t);

    protected:
        ///
        /// \brief introduce the function
        ///
        void initModule() override;

        ///
        /// \brief introduce the function
        ///
        void runModule() override;

        ///
        /// \brief introduce the function
        ///
        void cleanUpModule() override {};

        ///
        /// \ add tool information for continuous collision detection
        ///
        void initTools();

        ///
        /// \brief introduce the function when multiple tools
        ///
        void setActived(bool flag){ isActived = flag; }
        void setToolIndex(int idx){ toolIndex = idx; }
        bool& getActived() { return isActived; }
        int& getToolIndex(){ return toolIndex; }

        ///
        /// \brief introduce the function
        ///
        void updateVisualMesh(int idx);

    protected:
        std::shared_ptr<PbdVirtualCouplingObject> m_ToolMarking;    ///< Tool Marking
        std::shared_ptr<PbdVirtualCouplingObject> m_ToolInjection; ///< Tool Injection
        std::shared_ptr<PbdVirtualCouplingObject> m_ToolCutting; ///< Tool Cutting
		std::shared_ptr<PbdVirtualCouplingObject> m_Overtube; ///<  Overtube
		std::shared_ptr<PbdVirtualCouplingObject> m_Surroundings; ///<  Surroundings
		std::shared_ptr<PointLight> m_Light;   ////< XZH Light

        Eigen::Matrix4d transformDev = Eigen::Matrix4d();

        Vec3d pPre = Vec3d::Zero();

        bool isActived = false;  ////< XZH which is actived when multiple
        int toolIndex = 0;   ////< XZH default is marking tool
		int toolIndexPre = 0;   ////< XZH the previous index

        std::shared_ptr<DeviceClient> m_deviceClient;   ////< XZH only one device
    };
}
#endif // ifndef imstkXZHTool_h