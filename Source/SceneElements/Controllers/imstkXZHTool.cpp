/*=========================================================================

Library: iMSTK

Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
& Imaging in Medicine, Rensselaer Polytechnic Institute.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0.txt

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

=========================================================================*/

#include "imstkXZHTool.h"

#include <utility>

#include <g3log/g3log.hpp>

namespace imstk
{
void
XZHTool::initModule()
{
    //auto visualMesh = std::dynamic_pointer_cast<PointSet>(m_ToolMarking->getVisualGeometry());
    //auto visualMesh1 = std::dynamic_pointer_cast<PointSet>(m_ToolInjection->getVisualGeometry());
    //auto visualMesh2 = std::dynamic_pointer_cast<PointSet>(m_ToolCutting->getVisualGeometry());
    //Vec3d vertexPos3;
    //for (int i = 0; i < visualMesh->getNumVertices(); ++i)
    //{
    //    vertexPos3 = visualMesh->getInitialVertexPosition(i);  // use the inital position * Omni transform   //->getVertexPosition(i);
    //    vertexPos3 +=Vec3d(1.4, 0, -0.8);   ////< XZH  Vec3d(1.4, 0, -0.8);
    //    visualMesh->setVertexPosition(i, vertexPos3);
    //} 
    initTools();
    m_ToolMarking->setDeviceClient(this->getDeviceClient());
    m_ToolMarking->setProcedure(0);
    m_ToolInjection->setDeviceClient(this->getDeviceClient());
    m_ToolInjection->setProcedure(1);
    m_ToolCutting->setDeviceClient(this->getDeviceClient());
    m_ToolCutting->setProcedure(2);
}

void
XZHTool::runModule()
{
    if (!m_trackingDataUptoDate)
    {
        if (!updateTrackingData())
        {
            LOG(WARNING) << "CameraController::runModule warning: could not update tracking info.";
            return;
        }
    }

    Vec3d p = getPosition();
    Quatd r = getRotation();
    double scale = 0.01;   ////< XZH environment scale
    //if ((p - pPre).norm() > 0.5)
    //{
    //    printf("test too big");
    //}
    pPre = p;

    // Apply Offsets over the device pose
    p = p + m_cameraTranslationOffset;      // Offset the device position
    r *= m_cameraRotationalOffset;          // Apply camera head rotation offset

	//p = Vec3d(3.2, 4.428, 6.0);
	//r = Quatd(0, -0.47049, 0, 0.882405);


    //printf("test cam: %f %f %f \n", (r*DOWN_VECTOR*scale).x(), (r*DOWN_VECTOR*scale).y(), (r*DOWN_VECTOR*scale).z());
      ////< XZH 
    double fxzh = this->getDeviceClient()->getTransY();
    double fx = this->getDeviceClient()->getTransX();
    double fz = this->getDeviceClient()->getTransZ();
    int camFlag = this->getDeviceClient()->getCameraFlag();
    //printf(" fx fy fz: %f %f %f, ", fx, fxzh, fz);
    // Set camera info
	Vec3d posFocal = (r* (DOWN_VECTOR + Vec3d(0.05, 0, 0))*scale) + p + Vec3d(fx, 0, fz);
	Vec3d posCam = p + Vec3d(fx, 0, fz);
  //  if (camFlag>0)
  //  {
		//m_camera.setPosition(posCam); // (p + Vec3d(fx, 0, fz));
		//m_camera.setFocalPoint(posFocal);  //  ((r* (DOWN_VECTOR + Vec3d(0.0, 0, 0))*scale) + p + Vec3d(fx, 0, fz));  //(Vec3d(0, -3, 0) + p); // ((r*DOWN_VECTOR) + p);  // (r*DOWN_VECTOR) DOWN_VECTOR  r*FORWARD_VECTOR
  //      m_camera.setViewUp(r*BACKWARD_VECTOR*scale); // (r*UP_VECTOR);
  //  }

	  ////< XZH Light
	m_Light->setPosition(posCam + (posFocal - posCam)*0.1);
	m_Light->setFocalPoint(posFocal);

	auto visualMeshOT = std::dynamic_pointer_cast<PointSet>(m_Overtube->getVisualGeometry());
	auto visualMeshOTModel = std::dynamic_pointer_cast<VisualModel>(m_Overtube->getVisualModel(0));
	visualMeshOT->setRotation(r);
	Vec3d posOT = (p + (r*DOWN_VECTOR*scale) + Vec3d(fx, 0, fz)); //  p + Vec3d(fx, 0, fz) + (r* (DOWN_VECTOR + Vec3d(0.5, 0, 0))*scale)*0.6;
	visualMeshOT->setTranslation(posOT); // (p + (r*DOWN_VECTOR*scale) + Vec3d(fx, 0, fz));
	
	Eigen::Matrix4d transformSur = Eigen::Matrix4d();
	computeTransform(posOT, r, transformSur);

	//fxzh = 0.65;
    p = p + (r*DOWN_VECTOR*scale) * (1+ fxzh) + Vec3d(fx, 0, fz); //  +Vec3d(1.4, 0, -0.8);

    //Vec3d ttmp = (r*DOWN_VECTOR*scale) *  fxzh;
    //Vec3d ttmp2 = (r*DOWN_VECTOR*scale);
    //printf("roll trany : %f %f %f, part: %f %f %f, %f\n", ttmp.x(), ttmp.y(), ttmp.z(), ttmp2.x(), ttmp2.y(), ttmp2.z(), fxzh);  // printf("X Y Z: %f %f %f\n", fx, fxzh, fz);
    computeTransform(p, r, transformDev);

	////< XZH surroundings 
	auto visualMeshSur = std::dynamic_pointer_cast<PointSet>(m_Surroundings->getVisualGeometry());
	Vec4d vertexPosSur4 = Vec4d::Zero();
	vertexPosSur4.w() = 1;
	Vec3d vertexPosSur3 = Vec3d::Zero();
	vertexPosSur3 = visualMeshSur->getInitialVertexPosition(6);
	vertexPosSur4.x() = vertexPosSur3.x();
	vertexPosSur4.y() = vertexPosSur3.y();
	vertexPosSur4.z() = vertexPosSur3.z();
	vertexPosSur4.applyOnTheLeft(transformSur);
	vertexPosSur3.x() = vertexPosSur4.x();
	vertexPosSur3.y() = vertexPosSur4.y();
	vertexPosSur3.z() = vertexPosSur4.z();
	visualMeshSur->setVertexPosition(6, vertexPosSur3);

    auto collidingMesh = std::dynamic_pointer_cast<PointSet>(m_ToolMarking->getCollidingGeometry());
    // TEST visual geometry just for testCuttingCubeDenseHaptic, otherwise should comment
    auto visualMesh = std::dynamic_pointer_cast<PointSet>(m_ToolMarking->getVisualGeometry());
    auto visualMesh1 = std::dynamic_pointer_cast<PointSet>(m_ToolInjection->getVisualGeometry());
    auto visualMesh2 = std::dynamic_pointer_cast<PointSet>(m_ToolCutting->getVisualGeometry());
    auto visualMeshModel = std::dynamic_pointer_cast<VisualModel>(m_ToolMarking->getVisualModel(0));
    auto visualMesh1Model = std::dynamic_pointer_cast<VisualModel>(m_ToolInjection->getVisualModel(0));
    auto visualMesh2Model = std::dynamic_pointer_cast<VisualModel>(m_ToolCutting->getVisualModel(0));

    visualMesh->setRotation(r);
    visualMesh->setTranslation(p);
    visualMesh1->setRotation(r);
    visualMesh1->setTranslation(p);
    visualMesh2->setRotation(r);
    visualMesh2->setTranslation(p);

    Vec4d vertexPos4=Vec4d::Zero();
    vertexPos4.w() = 1;
    Vec3d vertexPos3=Vec3d::Zero();

	toolIndex = this->getDeviceClient()->getToolIndex();
	if (toolIndex!=toolIndexPre)
	{
		switch (toolIndex)
		{
		default:
		case 0:   ////< XZH show marking
				  //for (int i = 0; i < visualMesh->getNumVertices(); ++i)
				  //{
				  //    vertexPos3 = visualMesh->getInitialVertexPosition(i);  // use the inital position * Omni transform   //->getVertexPosition(i);
				  //    vertexPos3 += Vec3d(1.0, 0, -0.0)*scale;   ////< XZH  Vec3d(1.4, 0, -0.8);
				  //    vertexPos4.x() = vertexPos3.x();
				  //    vertexPos4.y() = vertexPos3.y();
				  //    vertexPos4.z() = vertexPos3.z();

				  //    vertexPos4.applyOnTheLeft(transformDev);
				  //    vertexPos3.x() = vertexPos4.x();
				  //    vertexPos3.y() = vertexPos4.y();
				  //    vertexPos3.z() = vertexPos4.z();

				  //    visualMesh->setVertexPosition(i, vertexPos3);
				  //}
				  //vertexPos3 = Vec3d::Zero();
				  //for (int i = 0; i < visualMesh1->getNumVertices(); ++i)
				  //{
				  //    visualMesh1->setVertexPosition(i, vertexPos3);
				  //}
				  //for (int i = 0; i < visualMesh2->getNumVertices(); ++i)
				  //{
				  //    visualMesh2->setVertexPosition(i, vertexPos3);
				  //}
			visualMeshModel->show();
			visualMesh1Model->hide();
			visualMesh2Model->hide();
			break;
		case 1:   ////< XZH show injection
				  //for (int i = 0; i < visualMesh1->getNumVertices(); ++i)
				  //{
				  //    vertexPos3 = visualMesh1->getInitialVertexPosition(i);  // use the inital position * Omni transform   //->getVertexPosition(i);
				  //    vertexPos3 += Vec3d(1.0, 0, -0.0)*scale;   ////< XZH  Vec3d(1.4, 0, -0.8);
				  //    vertexPos4.x() = vertexPos3.x();
				  //    vertexPos4.y() = vertexPos3.y();
				  //    vertexPos4.z() = vertexPos3.z();

				  //    vertexPos4.applyOnTheLeft(transformDev);
				  //    vertexPos3.x() = vertexPos4.x();
				  //    vertexPos3.y() = vertexPos4.y();
				  //    vertexPos3.z() = vertexPos4.z();

				  //    visualMesh1->setVertexPosition(i, vertexPos3);
				  //}
				  //vertexPos3 = Vec3d::Zero();
				  //for (int i = 0; i < visualMesh->getNumVertices(); ++i)
				  //{
				  //    visualMesh->setVertexPosition(i, vertexPos3);
				  //}
				  //for (int i = 0; i < visualMesh2->getNumVertices(); ++i)
				  //{
				  //    visualMesh2->setVertexPosition(i, vertexPos3);
				  //}
			visualMesh1Model->show();
			visualMeshModel->hide();
			visualMesh2Model->hide();
			break;
		case 2:   ////< XZH show cutting
				  //for (int i = 0; i < visualMesh2->getNumVertices(); ++i)
				  //{
				  //    vertexPos3 = visualMesh2->getInitialVertexPosition(i);  // use the inital position * Omni transform   //->getVertexPosition(i);
				  //    vertexPos3 += Vec3d(1.0, 0, -0.0)*scale;   ////< XZH  Vec3d(1.4, 0, -0.8);
				  //    vertexPos4.x() = vertexPos3.x();
				  //    vertexPos4.y() = vertexPos3.y();
				  //    vertexPos4.z() = vertexPos3.z();

				  //    vertexPos4.applyOnTheLeft(transformDev);
				  //    vertexPos3.x() = vertexPos4.x();
				  //    vertexPos3.y() = vertexPos4.y();
				  //    vertexPos3.z() = vertexPos4.z();

				  //    visualMesh2->setVertexPosition(i, vertexPos3);
				  //}
				  //vertexPos3 = Vec3d::Zero();
				  //for (int i = 0; i < visualMesh->getNumVertices(); ++i)
				  //{
				  //    visualMesh->setVertexPosition(i, vertexPos3);
				  //}
				  //for (int i = 0; i < visualMesh1->getNumVertices(); ++i)
				  //{
				  //    visualMesh1->setVertexPosition(i, vertexPos3);
				  //}
			visualMesh2Model->show();
			visualMeshModel->hide();
			visualMesh1Model->hide();
			break;
		}
	}
	toolIndexPre = toolIndex;
    
    
    //transformDev(0, 3) = p.x();
    //transformDev(1, 3) = p.y();
    //transformDev(2, 3) = p.z();
    //FILE *fileCod = fopen("i:/testcod.dat", "a");
    //fprintf(fileCod, "\n");
    for (int i = 0; i < collidingMesh->getNumVertices(); ++i)
    {
        vertexPos3 = collidingMesh->getInitialVertexPosition(i);  // use the inital position * Omni transform   //->getVertexPosition(i);
        //vertexPos3 += Vec3d(1.0, 0, -0.0)*scale;
        vertexPos4.x() = vertexPos3.x();
        vertexPos4.y() = vertexPos3.y();
        vertexPos4.z() = vertexPos3.z();

        vertexPos4.applyOnTheLeft(transformDev);
        vertexPos3.x() = vertexPos4.x();
        vertexPos3.y() = vertexPos4.y();
        vertexPos3.z() = vertexPos4.z();

        collidingMesh->setVertexPosition(i, vertexPos3);
        //fprintf(fileCod, "%f %f %f\n", vertexPos3.x(), vertexPos3.y(), vertexPos3.z());
    }
    //fclose(fileCod);
	if (camFlag > 0)
	{
		m_camera.setPosition(vertexPosSur3); // (p + Vec3d(fx, 0, fz));
		m_camera.setFocalPoint(posFocal);  //  ((r* (DOWN_VECTOR + Vec3d(0.0, 0, 0))*scale) + p + Vec3d(fx, 0, fz));  //(Vec3d(0, -3, 0) + p); // ((r*DOWN_VECTOR) + p);  // (r*DOWN_VECTOR) DOWN_VECTOR  r*FORWARD_VECTOR
		m_camera.setViewUp(r*BACKWARD_VECTOR*scale); // (r*UP_VECTOR);
	}

    Vec3d min1, max1;
    //visualMesh->computeBoundingBox(min1, max1);
    //visualMesh1->computeBoundingBox(min1, max1);
    //visualMesh2->computeBoundingBox(min1, max1);
    //collidingMesh->computeBoundingBox(min1, max1);

    m_trackingDataUptoDate = false;

	//m_ToolMarking->applyCollidingToPhysics();
	m_ToolMarking->updatePbdStates();
	//m_ToolInjection->applyCollidingToPhysics();
	m_ToolInjection->updatePbdStates();
	//m_ToolCutting->applyCollidingToPhysics();
	m_ToolCutting->updatePbdStates();

    // update tool info w.r.t obj
    Eigen::Matrix3d rotMat; //  = r.toRotationMatrix();
    //rotMat(0, 0) = transformDev(0, 0);   rotMat(0, 1) = transformDev(0, 1);   rotMat(0, 2) = transformDev(0, 2);  rotMat(0, 3) = transformDev(0, 3);
    //rotMat(1, 0) = transformDev(1, 0);    rotMat(1, 1) = transformDev(1, 1);    rotMat(1, 2) = transformDev(1, 2);   rotMat(1, 3) = transformDev(1, 3);
    //rotMat(2, 0) = transformDev(2, 0);   rotMat(2, 1) = transformDev(2, 1);   rotMat(2, 2) = transformDev(2, 2);  rotMat(2, 3) = transformDev(2, 3);
    //rotMat(3, 0) = 0;                    rotMat(3, 1) = 0;                    rotMat(3, 2) = 0;                    rotMat(3, 3) = 1;
    rotMat(0, 0) = transformDev(0, 0);   rotMat(0, 1) = transformDev(0, 1);   rotMat(0, 2) = transformDev(0, 2);
    rotMat(1, 0) = transformDev(1, 0);    rotMat(1, 1) = transformDev(1, 1);    rotMat(1, 2) = transformDev(1, 2);
    rotMat(2, 0) = transformDev(2, 0);   rotMat(2, 1) = transformDev(2, 1);   rotMat(2, 2) = transformDev(2, 2);
    auto& tool = m_ToolMarking->getControllerTool();
    auto& tool1 = m_ToolInjection->getControllerTool();
    auto& tool2 = m_ToolCutting->getControllerTool();
    tool.hapticRot = rotMat;  // transform R
    tool.pRotArr = tool.rotArr;
    tool.rotArr = rotMat; // r.toRotationMatrix();  // r is equal hapticRot
    tool.pGloVec[0] = tool.gloVec[0];
    tool.pGloVec[1] = tool.gloVec[1];
	tool.pGloVec[2] = tool.gloVec[2];
	tool.pGloVec[3] = tool.gloVec[3];
	tool.pGloVec[4] = tool.gloVec[4];
	tool.pGloVec[5] = tool.gloVec[5];
    tool.gloVec[0] = rotMat* collidingMesh->getInitialVertexPosition(0); // tool[t].rotArr[0] * tool[t].locVec[i].x + tool[t].rotArr[3] * tool[t].locVec[i].y + tool[t].rotArr[6] * tool[t].locVec[i].z;
    tool.gloVec[1] = rotMat* collidingMesh->getInitialVertexPosition(1); // should use the original pos
	tool.gloVec[2] = rotMat * collidingMesh->getInitialVertexPosition(2); 
	tool.gloVec[3] = rotMat * collidingMesh->getInitialVertexPosition(3);
	tool.gloVec[4] = rotMat * collidingMesh->getInitialVertexPosition(4);
	tool.gloVec[5] = rotMat * collidingMesh->getInitialVertexPosition(5);
    // update tool position
    tool.pHapticPos = tool.hapticPos;
    tool.hapticPos = p;  // transform  T
    tool.jointPos = tool.hapticPos; // is equal to tool.hapticPos;
    tool.pDrawPos = tool.finalPos;
    tool.drawPos = tool.jointPos;
    tool.button[0] = m_deviceClient->getButton(0);
    tool.button[1] = m_deviceClient->getButton(1);
    tool.button[2] = m_deviceClient->getButton(2);
    tool.button[3] = m_deviceClient->getButton(3);

      ////< XZH injection
    tool1.hapticRot = rotMat;  
    tool1.pRotArr = tool1.rotArr;
    tool1.rotArr = rotMat;  
    tool1.pGloVec[0] = tool1.gloVec[0];
    tool1.pGloVec[1] = tool1.gloVec[1];
	tool1.pGloVec[2] = tool1.gloVec[2];
	tool1.pGloVec[3] = tool1.gloVec[3];
	tool1.pGloVec[4] = tool1.gloVec[4];
	tool1.pGloVec[5] = tool1.gloVec[5];
    tool1.gloVec[0] = rotMat* collidingMesh->getInitialVertexPosition(0);
    tool1.gloVec[1] = rotMat* collidingMesh->getInitialVertexPosition(1);
	tool1.gloVec[2] = rotMat * collidingMesh->getInitialVertexPosition(2);
	tool1.gloVec[3] = rotMat * collidingMesh->getInitialVertexPosition(3);
	tool1.gloVec[4] = rotMat * collidingMesh->getInitialVertexPosition(4);
	tool1.gloVec[5] = rotMat * collidingMesh->getInitialVertexPosition(5);
    // update tool position
    tool1.pHapticPos = tool1.hapticPos;
    tool1.hapticPos = p;   
    tool1.jointPos = tool1.hapticPos;  
    tool1.pDrawPos = tool1.finalPos;
    tool1.drawPos = tool1.jointPos;
    tool1.button[0] = m_deviceClient->getButton(0);
    tool1.button[1] = m_deviceClient->getButton(1);
    tool1.button[2] = m_deviceClient->getButton(2);
    tool1.button[3] = m_deviceClient->getButton(3);

      ////< XZH cutting
    tool2.hapticRot = rotMat;
    tool2.pRotArr = tool2.rotArr;
    tool2.rotArr = rotMat;
    tool2.pGloVec[0] = tool2.gloVec[0];
    tool2.pGloVec[1] = tool2.gloVec[1];
	tool2.pGloVec[2] = tool2.gloVec[2];
	tool2.pGloVec[3] = tool2.gloVec[3];
	tool2.pGloVec[4] = tool2.gloVec[4];
	tool2.pGloVec[5] = tool2.gloVec[5];
    tool2.gloVec[0] = rotMat* collidingMesh->getInitialVertexPosition(0);
    tool2.gloVec[1] = rotMat* collidingMesh->getInitialVertexPosition(1);
	tool2.gloVec[2] = rotMat * collidingMesh->getInitialVertexPosition(2);
	tool2.gloVec[3] = rotMat * collidingMesh->getInitialVertexPosition(3);
	tool2.gloVec[4] = rotMat * collidingMesh->getInitialVertexPosition(4);
	tool2.gloVec[5] = rotMat * collidingMesh->getInitialVertexPosition(5);
    // update tool position
    tool2.pHapticPos = tool2.hapticPos;
    tool2.hapticPos = p;
    tool2.jointPos = tool2.hapticPos;
    tool2.pDrawPos = tool2.finalPos;
    tool2.drawPos = tool2.jointPos;
    tool2.button[0] = m_deviceClient->getButton(0);
    tool2.button[1] = m_deviceClient->getButton(1);
    tool2.button[2] = m_deviceClient->getButton(2);
    tool2.button[3] = m_deviceClient->getButton(3);

    // update end

      ////< XZH send Force

      ////< XZH send Force END
}

void
XZHTool::computeTransform(Vec3d& pos, Quatd& quat, Eigen::Matrix4d& t)
{
    auto angleAxis = Rotd(quat);  // get Axis and angle? from quat
    Eigen::Matrix3d matRot = quat.toRotationMatrix();
    t.setIdentity();
    t(0, 0) = matRot(0, 0);   t(0, 1) = matRot(0, 1);   t(0, 2) = matRot(0, 2);  t(0, 3) = pos.x();
    t(1, 0) = matRot(1, 0);    t(1, 1) = matRot(1, 1);    t(1, 2) = matRot(1, 2);   t(1, 3) = pos.y();
    t(2, 0) = matRot(2, 0);   t(2, 1) = matRot(2, 1);   t(2, 2) = matRot(2, 2);  t(2, 3) = pos.z();
    t(3, 0) = 0;                    t(3, 1) = 0;                    t(3, 2) = 0;                    t(3, 3) = 1;
}

void
XZHTool::initTools()
{
    m_deviceClient->initDevice();
    double scaling = getTranslationScaling();
    m_deviceClient->setTranslationScaling(scaling);
    auto& tool = m_ToolMarking->getControllerTool();
	auto& tool1 = m_ToolInjection->getControllerTool();
	auto& tool2 = m_ToolCutting->getControllerTool();
	float scale = 1.0;// scale factor of the line-segment tool

    // tool 0
	tool.touchStartSphere = false;
    tool.ccdDistance = 0.01; // 10.0;
    tool.startPos = Vec3d::Zero();

    tool.filterSize = 100;
    tool.numberOfForceInFilter = 0;
    tool.arrForce.resize(100, Vec3d::Zero());

    tool.forceCoeff = 1;

    tool.nbrNode = 2; // two node for one line
	tool.nbrNodeSurrounding = 6;
    tool.nbrActSeg = 1;
	tool.nbrActSegSurrounding = 6;
    tool.nbrActTip = 1;

    tool.nodeIdxOfSeg[0][0] = 0;
    tool.nodeIdxOfSeg[0][1] = 1;

    tool.nodeIdxOfSeg[1][0] = 1;
    tool.nodeIdxOfSeg[1][1] = 2;

	// 0-1
	tool.nodeIdxOfSegSurrounding[0][0] = 0;
	tool.nodeIdxOfSegSurrounding[0][1] = 1;
	// 2-3 
	tool.nodeIdxOfSegSurrounding[1][0] = 2;
	tool.nodeIdxOfSegSurrounding[1][1] = 3;
	// 3-4 
	tool.nodeIdxOfSegSurrounding[2][0] = 3;
	tool.nodeIdxOfSegSurrounding[2][1] = 4;
	// 4-5 
	tool.nodeIdxOfSegSurrounding[3][0] = 4;
	tool.nodeIdxOfSegSurrounding[3][1] = 5;
	// 5-2 
	tool.nodeIdxOfSegSurrounding[4][0] = 5;
	tool.nodeIdxOfSegSurrounding[4][1] = 2;
	// 2-4 
	tool.nodeIdxOfSegSurrounding[5][0] = 2;
	tool.nodeIdxOfSegSurrounding[5][1] = 4;

    tool.belongToCylinder[0] = 0;
    tool.belongToCylinder[1] = 1;

    tool.gloVec[0] = Vec3d::Zero();
    tool.gloVec[1] = Vec3d::Zero();
    tool.pGloVec[0] = Vec3d::Zero();
    tool.pGloVec[1] = Vec3d::Zero();

    tool.springConstant = 5;
    tool.velocityResistance = 1;
    tool.hapticVelResistance = 1;

    tool.nodeIdxOfTip[0] = 0;

    tool.diffPos = Vec3d::Zero();
    tool.diffPosPre = Vec3d::Zero();

    tool.cullingThickness[0] = 0.005;  // 1.5 for finding candidate shoud a big than thickness
    tool.cullingThickness[1] = 0.005; // 1.5

    tool.nodeIdxOfTip[0] = 0;

    tool.diffPos = Vec3d::Zero();
    tool.diffPosPre = Vec3d::Zero();


    tool.oriVec[0] = Vec3d(-16.4, -1.85, 9.15);
    tool.oriVec[1] = Vec3d(-9.3, -1.4, 9.05);
	tool.oriVec[2] = Vec3d(0, 0, 0);
	tool.oriVec[3] = Vec3d(0, 0, 0);
	tool.oriVec[4] = Vec3d(0, 0, 0);
	tool.oriVec[5] = Vec3d(0, 0, 0);

    //for (int i = 0; i < tool.nbrNode; i++)
	for (int i = 0; i < tool.nbrNodeSurrounding; i++)
	{
        tool.locVec[i] = tool.oriVec[i];
        tool.pLocVec[i] = tool.oriVec[i];

		tool.gloVec[i] = Vec3d::Zero();
		tool.pGloVec[i] = Vec3d::Zero();
    }

    tool.thickness[0][0][0] = 0.5; // should smaller than cullingthickness
    tool.thickness[1][0][0] = 0.8;

    tool.thickness[0][1][0] = 0.1;
    tool.thickness[1][1][0] = 0.1;

    tool.thickness[0][0][1] = 0.6;
    tool.thickness[1][0][1] = 0.9;

    tool.thickness[0][1][1] = 0.2;
    tool.thickness[1][1][1] = 0.2;

	// tool 1
	tool1.touchStartSphere = false;
	tool1.ccdDistance = 0.01; // 10.0;
	tool1.startPos = Vec3d::Zero();

	tool1.filterSize = 100;
	tool1.numberOfForceInFilter = 0;
	tool1.arrForce.resize(100, Vec3d::Zero());

	tool1.forceCoeff = 1;

	tool1.nbrNode = 2; // two node for one line
	tool1.nbrNodeSurrounding = 6;
	tool1.nbrActSeg = 1;
	tool1.nbrActSegSurrounding = 6;
	tool1.nbrActTip = 1;

	tool1.nodeIdxOfSeg[0][0] = 0;
	tool1.nodeIdxOfSeg[0][1] = 1;

	tool1.nodeIdxOfSeg[1][0] = 1;
	tool1.nodeIdxOfSeg[1][1] = 2;

	// 0-1
	tool1.nodeIdxOfSegSurrounding[0][0] = 0;
	tool1.nodeIdxOfSegSurrounding[0][1] = 1;
	// 2-3 
	tool1.nodeIdxOfSegSurrounding[1][0] = 2;
	tool1.nodeIdxOfSegSurrounding[1][1] = 3;
	// 3-4 
	tool1.nodeIdxOfSegSurrounding[2][0] = 3;
	tool1.nodeIdxOfSegSurrounding[2][1] = 4;
	// 4-5 
	tool1.nodeIdxOfSegSurrounding[3][0] = 4;
	tool1.nodeIdxOfSegSurrounding[3][1] = 5;
	// 5-2 
	tool1.nodeIdxOfSegSurrounding[4][0] = 5;
	tool1.nodeIdxOfSegSurrounding[4][1] = 2;
	// 2-4 
	tool1.nodeIdxOfSegSurrounding[5][0] = 2;
	tool1.nodeIdxOfSegSurrounding[5][1] = 4;

	tool1.belongToCylinder[0] = 0;
	tool1.belongToCylinder[1] = 1;

	tool1.gloVec[0] = Vec3d::Zero();
	tool1.gloVec[1] = Vec3d::Zero();
	tool1.pGloVec[0] = Vec3d::Zero();
	tool1.pGloVec[1] = Vec3d::Zero();

	tool1.springConstant = 5;
	tool1.velocityResistance = 1;
	tool1.hapticVelResistance = 1;

	tool1.nodeIdxOfTip[0] = 0;

	tool1.diffPos = Vec3d::Zero();
	tool1.diffPosPre = Vec3d::Zero();

	tool1.cullingThickness[0] = 0.005;  // 1.5 for finding candidate shoud a big than thickness
	tool1.cullingThickness[1] = 0.005; // 1.5

	tool1.nodeIdxOfTip[0] = 0;

	tool1.diffPos = Vec3d::Zero();
	tool1.diffPosPre = Vec3d::Zero();


	tool1.oriVec[0] = Vec3d(-16.4, -1.85, 9.15);
	tool1.oriVec[1] = Vec3d(-9.3, -1.4, 9.05);
	tool1.oriVec[2] = Vec3d(0, 0, 0);
	tool1.oriVec[3] = Vec3d(0, 0, 0);
	tool1.oriVec[4] = Vec3d(0, 0, 0);
	tool1.oriVec[5] = Vec3d(0, 0, 0);

	//for (int i = 0; i < tool1.nbrNode; i++)
	for (int i = 0; i < tool1.nbrNodeSurrounding; i++)
	{
		tool1.locVec[i] = tool1.oriVec[i];
		tool1.pLocVec[i] = tool1.oriVec[i];

		tool1.gloVec[i] = Vec3d::Zero();
		tool1.pGloVec[i] = Vec3d::Zero();
	}

	tool1.thickness[0][0][0] = 0.5; // should smaller than cullingthickness
	tool1.thickness[1][0][0] = 0.8;

	tool1.thickness[0][1][0] = 0.1;
	tool1.thickness[1][1][0] = 0.1;

	tool1.thickness[0][0][1] = 0.6;
	tool1.thickness[1][0][1] = 0.9;

	tool1.thickness[0][1][1] = 0.2;
	tool1.thickness[1][1][1] = 0.2;

	// tool 2
	tool2.touchStartSphere = false;
	tool2.ccdDistance = 0.01; // 10.0;
	tool2.startPos = Vec3d::Zero();

	tool2.filterSize = 100;
	tool2.numberOfForceInFilter = 0;
	tool2.arrForce.resize(100, Vec3d::Zero());

	tool2.forceCoeff = 1;

	tool2.nbrNode = 2; // two node for one line
	tool2.nbrNodeSurrounding = 6;
	tool2.nbrActSeg = 1;
	tool2.nbrActSegSurrounding = 6;
	tool2.nbrActTip = 1;

	tool2.nodeIdxOfSeg[0][0] = 0;
	tool2.nodeIdxOfSeg[0][1] = 1;

	tool2.nodeIdxOfSeg[1][0] = 1;
	tool2.nodeIdxOfSeg[1][1] = 2;

	// 0-1
	tool2.nodeIdxOfSegSurrounding[0][0] = 0;
	tool2.nodeIdxOfSegSurrounding[0][1] = 1;
	// 2-3 
	tool2.nodeIdxOfSegSurrounding[1][0] = 2;
	tool2.nodeIdxOfSegSurrounding[1][1] = 3;
	// 3-4 
	tool2.nodeIdxOfSegSurrounding[2][0] = 3;
	tool2.nodeIdxOfSegSurrounding[2][1] = 4;
	// 4-5 
	tool2.nodeIdxOfSegSurrounding[3][0] = 4;
	tool2.nodeIdxOfSegSurrounding[3][1] = 5;
	// 5-2 
	tool2.nodeIdxOfSegSurrounding[4][0] = 5;
	tool2.nodeIdxOfSegSurrounding[4][1] = 2;
	// 2-4 
	tool2.nodeIdxOfSegSurrounding[5][0] = 2;
	tool2.nodeIdxOfSegSurrounding[5][1] = 4;

	tool2.belongToCylinder[0] = 0;
	tool2.belongToCylinder[1] = 1;

	tool2.gloVec[0] = Vec3d::Zero();
	tool2.gloVec[1] = Vec3d::Zero();
	tool2.pGloVec[0] = Vec3d::Zero();
	tool2.pGloVec[1] = Vec3d::Zero();

	tool2.springConstant = 5;
	tool2.velocityResistance = 1;
	tool2.hapticVelResistance = 1;

	tool2.nodeIdxOfTip[0] = 0;

	tool2.diffPos = Vec3d::Zero();
	tool2.diffPosPre = Vec3d::Zero();

	tool2.cullingThickness[0] = 0.005;  // 1.5 for finding candidate shoud a big than thickness
	tool2.cullingThickness[1] = 0.005; // 1.5

	tool2.nodeIdxOfTip[0] = 0;

	tool2.diffPos = Vec3d::Zero();
	tool2.diffPosPre = Vec3d::Zero();


	tool2.oriVec[0] = Vec3d(-16.4, -1.85, 9.15);
	tool2.oriVec[1] = Vec3d(-9.3, -1.4, 9.05);
	tool2.oriVec[2] = Vec3d(0, 0, 0);
	tool2.oriVec[3] = Vec3d(0, 0, 0);
	tool2.oriVec[4] = Vec3d(0, 0, 0);
	tool2.oriVec[5] = Vec3d(0, 0, 0);

	//for (int i = 0; i < tool2.nbrNode; i++)
	for (int i = 0; i < tool2.nbrNodeSurrounding; i++)
	{
		tool2.locVec[i] = tool2.oriVec[i];
		tool2.pLocVec[i] = tool2.oriVec[i];

		tool2.gloVec[i] = Vec3d::Zero();
		tool2.pGloVec[i] = Vec3d::Zero();
	}

	tool2.thickness[0][0][0] = 0.5; // should smaller than cullingthickness
	tool2.thickness[1][0][0] = 0.8;

	tool2.thickness[0][1][0] = 0.1;
	tool2.thickness[1][1][0] = 0.1;

	tool2.thickness[0][0][1] = 0.6;
	tool2.thickness[1][0][1] = 0.9;

	tool2.thickness[0][1][1] = 0.2;
	tool2.thickness[1][1][1] = 0.2;
}
void
XZHTool::updateVisualMesh(int idx)
{

}
}