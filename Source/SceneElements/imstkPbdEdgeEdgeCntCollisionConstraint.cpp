/*=========================================================================

Library: iMSTK

Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
& Imaging in Medicine, Rensselaer Polytechnic Institute.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0.txt

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

=========================================================================*/

#include "imstkPbdEdgeEdgeCntCollisionConstraint.h"
#include "g3log/g3log.hpp"

namespace imstk
{

    void
        PbdEdgeEdgeContinuousConstraint::initConstraint(std::shared_ptr<PbdModel> model1,
        const size_t& pIdx1, const size_t& pIdx2,
        std::shared_ptr<PbdModel> model2,
        const size_t& pIdx3, const size_t& pIdx4,
        int seg1, double segpara1, int seg2, double segpara2, Vec3d colNorm, double colTime // for intersection
        )
    {
        m_model1 = model1;
        m_model2 = model2;
        m_bodiesFirst[0] = pIdx1;
        m_bodiesFirst[1] = pIdx2;
        m_bodiesSecond[0] = pIdx3;
        m_bodiesSecond[1] = pIdx4;
        segEdge1 = seg1;
        segEdge2 = seg2;
        segparaEdge1 = segpara1;
        segparaEdge2 = segpara2;
        colEdgeNorm = colNorm;
        colEdgeTime = colTime;
    }

    bool
        PbdEdgeEdgeContinuousConstraint::solvePositionConstraint()
    {
        //const auto i0 = m_bodiesFirst[0];
        //const auto i1 = m_bodiesFirst[1];  // tool 
        //const auto i2 = m_bodiesSecond[0];
        //const auto i3 = m_bodiesSecond[1]; // obj

        //auto tetMesh = std::dynamic_pointer_cast<VESSTetrahedralMesh>(m_model2->getModelGeometry());
        //if (!tetMesh) return false;
        //auto& physEdges = tetMesh->getPhysEdge();
        //auto& physVertices = tetMesh->getPhysVertex();
        //auto& physTris = tetMesh->getPhysSurfTris();
        //// continuousCollisionHandlingWithSurfaceMesh 
        //// check collision repetition to a node to avoid large impact on the node
        //// lack code 

        //int m, k, n;
        //Vec3d x1, x2, x3, x4;
        //double bary[3];
        //Vec3d dV;
        //double ratio;
        ////algorithm step3  
        //// compute impulse based on the collision depth
        //double fS, fT, magImpsNorm, magImpsTang, colWeight;
        //Vec3d toolColPt, objColPt, impsVec, impsTang, impsNorm;
        //ratio = 0.1;

        //x1 = tool.ccdPos + tool.gloVec[0];
        //x2 = tool.ccdPos + tool.gloVec[1];

        //fS = segparaEdge1;
        //toolColPt = (1.0 - fS) * x1 + fS * x2;

        //x3 = physVertices[i2].p;
        //x4 = physVertices[i3].p;

        //fT = segparaEdge2;
        //objColPt = (1.0 - fT) * x3 + fT * x4;

        //impsVec = toolColPt - objColPt;
        //colWeight = 1.0 / 10; // should computer XZH smFloat(simObj->ctnColNode[simObj->ctnColPair[n].colLineNodeID[0]].nbrCol);
        //physVertices[i2].p += colWeight * ratio  *(1.0 - fT) * impsVec;

        //colWeight = 1.0 / 10; // XZH smFloat(simObj->ctnColNode[simObj->ctnColPair[n].colLineNodeID[1]].nbrCol);
        //physVertices[i3].p += colWeight * ratio  *(1.0 - fT) * impsVec;

        return true;
    }

    bool
        PbdEdgeEdgeContinuousConstraint::solvePositionConstraint(ToolInfo& tool)
    {
        const auto i0 = m_bodiesFirst[0];
        const auto i1 = m_bodiesFirst[1];  // tool 
        const auto i2 = m_bodiesSecond[0];
        const auto i3 = m_bodiesSecond[1]; // obj

        auto tetMesh = std::dynamic_pointer_cast<VESSTetrahedralMesh>(m_model2->getModelGeometry());
        if (!tetMesh) return false;
        auto& physEdges = tetMesh->getPhysEdge();
        auto& physVertices = tetMesh->getPhysVertex();
        auto& physTris = tetMesh->getPhysSurfTris();
        // continuousCollisionHandlingWithSurfaceMesh 
        // check collision repetition to a node to avoid large impact on the node
        // lack code 

        int m, k, n;
        Vec3d x1, x2, x3, x4;
        double bary[3];
        Vec3d dV;
        double ratio;
        //algorithm step3  
        // compute impulse based on the collision depth
        double fS, fT, magImpsNorm, magImpsTang, colWeight;
        Vec3d toolColPt, objColPt, impsVec, impsTang, impsNorm;
        ratio = 0.1;

        x1 = tool.ccdPos + tool.gloVec[0];
        x2 = tool.ccdPos + tool.gloVec[1];

        fS = segparaEdge1;
        toolColPt = (1.0 - fS) * x1 + fS * x2;

        x3 = physVertices[i2].p;
        x4 = physVertices[i3].p;

        fT = segparaEdge2;
        objColPt = (1.0 - fT) * x3 + fT * x4;

        impsVec = toolColPt - objColPt;
        colWeight = 1.0 / 10; // should computer XZH smFloat(simObj->ctnColNode[simObj->ctnColPair[n].colLineNodeID[0]].nbrCol);
        physVertices[i2].p += colWeight * ratio  *(1.0 - fT) * impsVec;

        colWeight = 1.0 / 10; // XZH smFloat(simObj->ctnColNode[simObj->ctnColPair[n].colLineNodeID[1]].nbrCol);
        physVertices[i3].p += colWeight * ratio  *(1.0 - fT) * impsVec;

        return true;
    }

    bool
        PbdEdgeEdgeContinuousConstraint::solvePositionConstraint(Vec3d& force)
    {
        const auto i0 = m_bodiesFirst[0];
        const auto i1 = m_bodiesFirst[1];
        const auto i2 = m_bodiesSecond[0];
        const auto i3 = m_bodiesSecond[1];

        auto state1 = m_model1->getCurrentState();
        auto state2 = m_model2->getCurrentState();

        Vec3d& x0 = state1->getVertexPosition(i0);
        Vec3d& x1 = state1->getVertexPosition(i1);
        Vec3d& x2 = state2->getVertexPosition(i2);
        Vec3d& x3 = state2->getVertexPosition(i3);

        auto a = (x3 - x2).dot(x1 - x0);
        auto b = (x1 - x0).dot(x1 - x0);
        auto c = (x0 - x2).dot(x1 - x0);
        auto d = (x3 - x2).dot(x3 - x2);
        auto e = a;
        auto f = (x0 - x2).dot(x3 - x2);

        auto det = a*e - d*b;
        double s = 0.5;
        double t = 0.5;
        if (fabs(det) > 1e-12)
        {
            s = (c*e - b*f) / det;
            t = (c*d - a*f) / det;
            if (s < 0 || s > 1.0 || t < 0 || t > 1.0)
            {
                return false;
            }
        }
        else
        {
            //LOG(WARNING) << "det is null";
        }

        Vec3d P = x0 + t*(x1 - x0);
        Vec3d Q = x2 + s*(x3 - x2);

        Vec3d n = Q - P;
        auto l = n.norm();
        n /= l;

        const auto dist = m_model1->getProximity() + m_model2->getProximity();

        if (l > dist)
        {
            return false;
        }

        Vec3d grad0 = -(1 - t)*n;
        Vec3d grad1 = -(t)*n;
        Vec3d grad2 = (1 - s)*n;
        Vec3d grad3 = (s)*n;

        const auto im0 = m_model1->getInvMass(i0);
        const auto im1 = m_model1->getInvMass(i1);
        const auto im2 = m_model2->getInvMass(i2);
        const auto im3 = m_model2->getInvMass(i3);

        auto lambda = im0*grad0.squaredNorm() +
            im1*grad1.squaredNorm() +
            im2*grad2.squaredNorm() +
            im3*grad3.squaredNorm();
        //printf("Edge Edge lambda %f, im: %f %f %f %f \n", lambda, im0*grad0.squaredNorm(), im1*grad1.squaredNorm(), im2*grad2.squaredNorm(), im3*grad3.squaredNorm());
        if (lambda >= 0.000001)  // !=0
        {
            // XZH centrial line of colon among volumetric mesh
            //Vec3d p0 = Vec3d(-19.4, -5.85, 1.15);   // for colon 
            //Vec3d p1 =Vec3d(-3.3, 2.4, 1.05);  
            Vec3d p0 = Vec3d(-19.4, -5, 1.15);   // for rect Test
            Vec3d p1 = Vec3d(13.3, -5, 1.15);
            Vec3d a = Q - p0;
            Vec3d b = p1 - p0;
            Vec3d c = ((a.dot(b)) / (b.dot(b)))*b;
            Vec3d e = a - c;
            Vec3d temp;
            if ((-n).dot(-e) >= 0)  // Q->P normal indicating the central line of inside colon should be positive
            {
                force += -(l - dist)*(lambda*grad0 + lambda*grad1) / 2;
                temp = -(l - dist)*(lambda*grad0 + lambda*grad1) / 2;
            }
            else  // no cross colon
            {
                force += (l - dist)*(lambda*grad0 + lambda*grad1) / 2;
                temp = (l - dist)*(lambda*grad0 + lambda*grad1) / 2;
            }
            //printf("force EE: %f %f %f %f\n", temp.x(), temp.y(), temp.z(), temp.norm());
            // XZN END
        }
        lambda = (l - dist) / lambda; // if im2=im3=0 is fixed point, lambda=-1.#INF00

        //    LOG(INFO) << "Lambda:" << lambda <<" Normal:" << n[0] <<" " << n[1] <<" "<<n[2];
        //if (n.dot(Vec3d(0,1,0))>=0)
        //{
        //}
        //force = (l - dist)*n; // original XZH force = (l - dist)*n; // (l - dist) is penetrationDepth, n is direction
        //printf("force Edge Edge: %f %f %f  lambda %f \n", force.x(), force.y(), force.z(), lambda);
        if (im0 > 0)
        {
            x0 += -im0*lambda*grad0*m_model1->getContactStiffness();
        }

        if (im1 > 0)
        {
            x1 += -im1*lambda*grad1*m_model1->getContactStiffness();
        }

        if (im2 > 0)
        {
            x2 += -im2*lambda*grad2*m_model2->getContactStiffness();
        }

        if (im3 > 0)
        {
            x3 += -im3*lambda*grad3*m_model2->getContactStiffness();
        }

        return true;
    }


} // imstk
