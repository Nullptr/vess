/*=========================================================================

Library: iMSTK

Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
& Imaging in Medicine, Rensselaer Polytechnic Institute.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0.txt

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

=========================================================================*/

#ifndef imstkVESSTetCutMesh_h
#define imstkVESSTetCutMesh_h

#include "imstkVESSBasic.h"
#include "imstkVESSTetrahedralMesh.h"
//#include "imstkCuttingVESSTetMeshEntity.h"

namespace imstk
{
    class VESSTetCutMesh : public VESSTetrahedralMesh
    {
    public:
        //CuttingVESSTetMesh(std::shared_ptr<TetrahedralMesh> tetMesh);
        VESSTetCutMesh();
        virtual ~VESSTetCutMesh();

    public:
        //CutEdge
        class CutEdge 
        {
        public:
            double t;
            Vec3d pos;
            Vec3d uvw;
            U32 idxNP0;
            U32 idxNP1;
            U32 idxOrgFrom;
            U32 idxOrgTo;


            CutEdge() 
            {
                t = 0;
                idxNP0 = idxNP1 = INVALID_INDEX;
                idxOrgFrom = idxOrgTo = INVALID_INDEX;
            }

            CutEdge& operator = (const CutEdge& A) 
            {
                t = A.t;
                pos = A.pos;
                uvw = A.uvw;
                idxNP0 = A.idxNP0;
                idxNP1 = A.idxNP1;
                idxOrgFrom = A.idxOrgFrom;
                idxOrgTo = A.idxOrgTo;

                return (*this);
            }
        };

    public:   ////< XZH self-generated volumetric mesh
          ////< XZH loading
        bool cutStrLineBySeparator(string strLine, char separator, vector<string>&strBufferLineVec);
        bool VESSTetCutMesh::loadTetXZH(char *p_FileTetra, char *p_FileFace, char *p_FileFaceAll, char *p_FileEdge, char *p_FileEdgeAll, char *p_FileVerts, imstk::StdVectorOfVec3d &vertList, std::vector<imstk::TetrahedralMesh::TetraArray> &tetConnectivity, std::vector<physXVertTetLink> &links);
        
          ////< XZH Surface 
        bool extractSurfaceMeshVisual();   ////< XZH  surface from .tet mesh reading with mark 1
        bool extractSurfaceMeshBoundary(const std::vector<Vec3d>& centralLine);
        bool extractSurfaceMeshVolume();
        bool extractCutSurfaceMesh();
        bool extractSurfaceMesh2(const std::vector<Vec3d>& centralLine);
        bool extractSurfaceMesh21(const std::vector<Vec3d>& centralLine);   ////< XZH for simple cube

        void setVisualFromTet(std::shared_ptr<SurfaceMesh> surfaceVisualTet) { m_surfaceVisual = surfaceVisualTet; }
        void setVisualMeshXZH(std::shared_ptr<SurfaceMesh> surfaceVisual) { m_surfaceVisualXZH = surfaceVisual; }
        void setVisualMeshTexture(std::shared_ptr<SurfaceMesh> surfaceVisualTex) { m_surfaceTexture = surfaceVisualTex; }
        void calcTex2TetSurfTriLink();   ////< XZH calc MeshTexture's link to surfaceVisualTet (Tet's surface)
        void updateVertexPositions() { m_verticesPostionsXZH = this->getVertexPositions(); }
        std::vector<SurfaceMesh::TriangleArray>& getTriTextureOrder() { return triListsTextureOrder; }

        std::shared_ptr<SurfaceMesh> getVisualSurface() { return m_surfaceVisual; }
        std::vector<int>& getVisualTriIndexInTet() { return m_surfaceVisualTriIndexInTet; }
        std::shared_ptr<SurfaceMesh> getBoundarySurface() { return m_surfaceBoundaryXZH; }
        std::shared_ptr<SurfaceMesh> getBoundarySurfaceInside() { return m_surfaceVisualInsideXZH; }
        std::shared_ptr<SurfaceMesh> getBoundarySurfaceOutside() { return m_surfaceVisualOutsideXZH; }
        std::shared_ptr<SurfaceMesh> getVolumeSurface() { return m_surfaceVolumeXZH; }

          ////< XZH mapping
        bool mappingLinks();
        bool mappingTriTetLinks();
        bool mappingTetTriLinks();
        std::vector<physXVertTetLink>& getTetSurfLinks() { return m_vertinksXZH; }
        std::vector<physXTriTetLink>& getTriTetLinks() { return m_triTetlinksXZH; }
        std::vector<physXTetTriLink>& getTetTriLinks() { return m_tetTrilinksXZH; }

          ////< XZH cutting        //Build
        bool initializeCuttingMesh(); // XZH
        void initializeCuttingPara();

          ////< XZH 
        int cutNewXZH(const vector<Vec3d>& segments, const vector<Vec3d>& quadstrips, bool modifyMesh);
        int cutNewXZH2(const vector<Vec3d>& segments, const vector<Vec3d>& quadstrips, bool modifyMesh);
        int cutNewXZH3(const vector<Vec3d>& segments, const vector<Vec3d>& quadstrips, bool modifyMesh);
        // compute cut-edges with cutting segment
        int computeCutEdges(const Vec3d sweptquad[4], std::map<U32, CutEdge>& mapCutEdges);

          ////< XZH cut tet
        std::vector<U32> getCutElements() const { return vecCutElements; }
        std::vector<U32> getCutElementsTotal() const { return vecCutElementsTotal; }
        std::vector<U32>& getCutVisualTotal() { return vecCutVisualTrisTotal; }
        std::vector<U32>& getCutNewTotal() { return vecCutNewTrisTotal; }
        std::vector<U32>& getCutEdges() { return vecCutEdges; }

          ////< XZH new surface
        std::vector<triArray>& getCutNewTriangles() { return vecCutNewTriangles; }

          ////< XZH Topology
        std::vector<EDGE>& getEdgesAll() { return m_cEdges; }

          ////< XZH mesh states
        std::vector<int>& getEdgeStates(){ return m_egStates; }
        std::vector<std::vector<U32>>& getEdgesPerNode() { return m_vecEdgesPerNode; }
        std::vector<std::vector<U32>>& getTetsPerNode() { return m_vecTetsPerNode; }

		////< XZH mesh
		std::vector<imstk::SurfaceMesh::TriangleArray> getTetTrianglesAll() { return m_triangleVerticesAllXZH; }

		  ////< XZH cut mesh dissecting flag
		std::vector<int> getDissectFlag() const { return m_tetrahedraDissectFlag; }
		void updatePhysVertex();

    public:   ////< XZH variables
        ////< XZH new volumetric mesh self generated
        std::vector<imstk::SurfaceMesh::TriangleArray> m_triangleVerticesBoundXZH;
        std::vector<int> m_surfaceVisualTriIndexInTet;   ////< XZH record the tri index in the whole trianlges of tetra mesh
        std::vector<imstk::SurfaceMesh::TriangleArray> m_triangleVerticesAllXZH;
        std::vector<int> m_triangleIsBoundaryAllXZH;
		std::vector<int> m_triangleIsAllTetIndexXZH;   ////< XZH record each triangle belong to which tet index, on 05/05/2019
		std::vector<int> m_triangleIsAllTetIndexXZH2;   ////< XZH each face might belong to 2 tets

        std::vector<imstk::EdgeArray> m_edgesVerticesXZH;
        std::vector<imstk::EdgeArray> m_edgesVerticesAllXZH;
        std::vector<int> m_edgesBoundaryAllXZH;
        
        std::vector<imstk::Vec3d> m_verticesPosXZH;
        imstk::StdVectorOfVec3d m_verticesPostionsXZH;
        std::shared_ptr<imstk::SurfaceMesh> m_surfaceVisual;   ////< XZH surface from .tet mesh reading with mark 1
        std::shared_ptr<imstk::SurfaceMesh> m_surfaceTexture;   ////< XZH surface from .obj, with texture, mapping between this and tets
        
        std::shared_ptr<imstk::SurfaceMesh> m_surfaceBoundaryXZH;
        std::shared_ptr<imstk::SurfaceMesh> m_surfaceVisualInsideXZH;
        std::shared_ptr<imstk::SurfaceMesh> m_surfaceVisualOutsideXZH;
        std::shared_ptr<imstk::SurfaceMesh> m_surfaceVisualXZH;
        std::shared_ptr<imstk::SurfaceMesh> m_surfaceVolumeXZH;
        std::vector<int> m_statesVisualXZH;   ////< XZH 1 inside 0 outside
        std::vector<physXVertTetLink> m_vertinksXZH;
        std::vector<physXTriTetLink> m_triTetlinksXZH;   ////< XZH based on Tri
        std::vector<physXTetTriLink> m_tetTrilinksXZH;   ////< XZH based on Tet

          ////< XZH link for dissecting
        std::vector<U32> m_texTetSurfTriLinks;   ////< XZH major is texture surface tri
        std::vector<U32> m_tetTexSurfTriLinks;  ////< XZH major is tetrahedral surface tri
        std::vector<U32> m_texTetVertLinks;   ////< XZH major is texture surface tri
        std::vector<U32> m_tetTexVertLinks;  ////< XZH major is tetrahedral surface tri
        std::vector<SurfaceMesh::TriangleArray> triListsTextureOrder;   ////< XZH keep the same vert index with texture surface mesh order, only for visualization

        //iterators
        std::unordered_map< int, int > m_mapTetTris;
        typedef std::unordered_map< int, int >::iterator MAPTETTRIITER;
        typedef std::unordered_map< int, int >::const_iterator MAPTETTRICONSTITER;

        std::unordered_map<U64, int> m_mapEdges;
        typedef std::unordered_map<U64, int>::iterator ITUN;

          ////< XZH Topology
        std::vector<CELL> m_cCells;
        std::vector<FACE> m_cFaces;   ////< XZH all the faces including inside mesh
        std::vector<FACE> m_cFacesIn;   ////< XZH In the boundary
        std::vector<EDGE> m_cEdges;
        std::vector<NODE> m_cNodes;

          ////< XZH record the status for each
        std::vector<int> m_egStates;   ////< XZH record the cut state 0: dissected, 1: retain But we still need to record each node's edges two cycle vectors
        std::vector<std::vector<U32>> m_vecEdgesPerNode;
        std::vector<std::vector<U32>> m_vecTetsPerNode;

        //top-down access
        std::vector< std::vector<U32> > m_cellsPerEdge;
		std::vector< std::vector<U32> > m_facesPerEdge;   ////< XZH including inside faces
        std::vector< std::vector<U32> > m_facesInPerEdge;   ////< XZH only boundary faces

          ////< XZH cutting
        //Cut Edges
        std::map<U32, CutEdge > m_mapCutEdges;
        typedef std::map<U32, CutEdge >::iterator CUTEDGEITER;
          ////< XZH 
        std::vector<U32> vecCutElements; // this cut
        std::vector<U32> vecCutElementsTotal; // total cut
        std::vector<U32> vecCutFaceTotal; // total cut
        std::vector<U32> vecCutFaceInTotal; // total cut in surface visual mesh

          ////< XZH 
        std::vector<U32> vecCutVisualTrisTotal;
        std::vector<U32> vecCutNewTrisTotal;   ////< XZH generating new triangles from the dissecting
        std::vector<U32> vecCutEdges; // this time cut, not assumation this cut

          ////< XZH Cut to gen new surface
        std::vector<triArray> vecCutNewTriangles;
        std::vector<int> tetConnectivityIndex;   ////< XZH this is mapping to the latest tetConnectivity in the original intial Tets

		  ////< XZH dissecting Flag add on 05/05/2019, save the flag of which tet can be dissected
		std::vector<int> m_tetrahedraDissectFlag;
    };
}
#endif // ifndef imstkVESSTetCutMesh_h