/*=========================================================================

Library: iMSTK

Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
& Imaging in Medicine, Rensselaer Polytechnic Institute.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0.txt

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

=========================================================================*/

#include "imstkVESSTetCutMesh.h"
#include <iostream>
#include <fstream>

namespace imstk
{
    VESSTetCutMesh::VESSTetCutMesh()
    {

    }

    VESSTetCutMesh::~VESSTetCutMesh()
    {

    }

    bool VESSTetCutMesh::cutStrLineBySeparator(string strLine, char separator, vector<string>&strBufferLineVec)
    {
        vector<string>::size_type startPos = 0;
        vector<string>::size_type endPos = 0;

        endPos = strLine.find_first_of(separator, startPos);
        while (endPos != string::npos)
        {
            strBufferLineVec.push_back(strLine.substr(startPos, endPos - startPos));
            startPos = strLine.find_first_not_of(separator, endPos + 1);
            endPos = strLine.find_first_of(separator, startPos);
        }
        strBufferLineVec.push_back(strLine.substr(startPos, endPos));
        return true;
    }

    bool VESSTetCutMesh::loadTetXZH(char *p_FileTetra, char *p_FileFace, char *p_FileFaceAll, char *p_FileEdge, char *p_FileEdgeAll, char *p_FileVerts, imstk::StdVectorOfVec3d &vertList, std::vector<imstk::TetrahedralMesh::TetraArray> &tetConnectivity, std::vector<physXVertTetLink> &links)
    {
        ////< XZH tet
        vector<string> strBufferVec;
        std::fstream infile(p_FileTetra);
        if (!infile)
        {
            cout << "failed" << endl;
            exit(1);
        }
        cout << "start..." << endl;
        string strLine;
        int numLines = 0;
        int numTotal = 0;
		int i0 = 0, i1 = 0, i2 = 0, i3 = 0, i4 = 0;
        while (getline(infile, strLine))
        {
            if (numLines > numTotal) break;

            strBufferVec.push_back(strLine);

            vector<string> strBufferVecTmp;
            strBufferVecTmp.push_back(strLine);
            ////< XZH split
            vector<int> intVec;
            int width;
            for (vector<string>::iterator iter = strBufferVecTmp.begin(); iter != strBufferVecTmp.end(); iter++)
            {
                vector<string> strBufferLineVec;
                char separator = '\x20';
                cutStrLineBySeparator(*iter, separator, strBufferLineVec);
                width = strBufferLineVec.size();
                int numCols = 0;
                for (vector<string>::iterator iterLine = strBufferLineVec.begin(); iterLine != strBufferLineVec.end(); iterLine++)
                {
                    stringstream ss;
                    ss << *iterLine;
                    int num;
                    ss >> num;
                    ss.clear();
                    intVec.push_back(num);
                    //cout << num<<" "; 
                    if ((numLines == 0) && (numCols == 0))
                    {
                        numTotal = num;
                    }
                    //if (numLines > 0)   ////< XZH cell
                    //if ((numLines > 0) && (numLines <= 10000)) for file from gen
					if (numLines > 0)   ////< XZH for file from code
                    {
                        if ((numCols > 1) && (numCols < 7))  ////< XZH real tetra vertices 4
                        {
                            switch (numCols)
                            {
                            case 2:
                                i0 = num;
                                break;
                            case 3:
                                i1 = num;
                                break;
                            case 4:
                                i2 = num;
                                break;
                            case 5:
                                i3 = num;
                                break;
							case 6:
								i4 = num;
                            }
                            //cout << num << " ";
                        }
                    }
                    //else if (numLines > 10000)   ////< XZH for file from code
					if(0)   ////< XZH for file from gen
                    {
                        if ((numCols > 0) && (numCols < 5))  ////< XZH real tetra vertices 4
                        {
                            switch (numCols)
                            {
                            case 1:
                                i0 = num;
                                break;
                            case 2:
                                i1 = num;
                                break;
                            case 3:
                                i2 = num;
                                break;
                            case 4:
                                i3 = num;
                                break;
                            }
                            //cout << num << " ";
                        }
                    }
                    numCols++;
                }
            }
            //cout << endl;
            if (numLines > 0)   ////< XZH cell
            {
                imstk::TetrahedralMesh::TetraArray tet1 = { i0, i1, i2, i3 };
                m_tetrahedraVertices.push_back(tet1);
				m_tetrahedraDissectFlag.push_back(i4);
            }
            numLines++;
        }
        cout << numLines << "->end..." << endl;
        infile.close();
        ////< XZH tet
        tetConnectivity = m_tetrahedraVertices;

        ////< XZH face
        strBufferVec.clear();
        std::fstream infileFace(p_FileFace);
        if (!infileFace)
        {
            cout << "Face failed" << endl;
            exit(1);
        }
        cout << "Face start..." << endl;
        strLine = "";
        numLines = 0;
        numTotal = 0;
        i0 = 0; i1 = 0;  i2 = 0;  i3 = 0;
        while (getline(infileFace, strLine))
        {
            if (numLines > numTotal) break;

            strBufferVec.push_back(strLine);

            vector<string> strBufferVecTmp;
            strBufferVecTmp.push_back(strLine);
            ////< XZH split
            vector<int> intVec;
            int width;
            for (vector<string>::iterator iter = strBufferVecTmp.begin(); iter != strBufferVecTmp.end(); iter++)
            {
                vector<string> strBufferLineVec;
                char separator = '\x20';
                cutStrLineBySeparator(*iter, separator, strBufferLineVec);
                width = strBufferLineVec.size();
                int numCols = 0;
                for (vector<string>::iterator iterLine = strBufferLineVec.begin(); iterLine != strBufferLineVec.end(); iterLine++)
                {
                    stringstream ss;
                    ss << *iterLine;
                    int num;
                    ss >> num;
                    ss.clear();
                    intVec.push_back(num);
                    //cout << num<<" "; 
                    if ((numLines == 0) && (numCols == 0))
                    {
                        numTotal = num;
                    }
                    if (numLines > 0)   ////< XZH cell for file by code
                    //if ((numLines > 0) && (numLines <= 10000))   ////< XZH  for file by gen
                    {
                        if ((numCols > 1) && (numCols < 5))  ////< XZH real tetra vertices 4
                        {
                            switch (numCols)
                            {
                            case 2:
                                i0 = num;
                                break;
                            case 3:
                                i1 = num;
                                break;
                            case 4:
                                i2 = num;
                                break;
                            }
                            //cout << num << " ";
                        }
                    }
                    //else if (numLines > 10000)   ////< XZH for file by gen
					if(0)   ////< XZH for file by code
                    {
                        if ((numCols > 0) && (numCols < 4))  ////< XZH real tetra vertices 4
                        {
                            switch (numCols)
                            {
                            case 1:
                                i0 = num;
                                break;
                            case 2:
                                i1 = num;
                                break;
                            case 3:
                                i2 = num;
                                break;
                            }
                            //cout << num << " ";
                        }
                    }
                    numCols++;
                }
            }
            //cout << endl;
            if (numLines > 0)   ////< XZH cell
            {
                imstk::SurfaceMesh::TriangleArray tri1 = { i0, i1, i2 };
                m_triangleVerticesBoundXZH.push_back(tri1);
            }
            numLines++;
        }
        cout << numLines << "->end..." << endl;
        infileFace.close();
          ////< XZH Face END

          ////< XZH Face ALL in
        strBufferVec.clear();
        std::fstream infileFaceAll(p_FileFaceAll);
        if (!infileFaceAll)
        {
            cout << "FaceALL failed" << endl;
            exit(1);
        }
        cout << "FaceALL start..." << endl;
        strLine = "";
        numLines = 0;
        numTotal = 0;
		i0 = 0; i1 = 0;  i2 = 0;  i3 = 0;  i4 = 0;
		int i5 = 0;
        while (getline(infileFaceAll, strLine))
        {
            if (numLines > numTotal) break;

            strBufferVec.push_back(strLine);

            vector<string> strBufferVecTmp;
            strBufferVecTmp.push_back(strLine);
            ////< XZH split
            vector<int> intVec;
            int width;
            for (vector<string>::iterator iter = strBufferVecTmp.begin(); iter != strBufferVecTmp.end(); iter++)
            {
                vector<string> strBufferLineVec;
                char separator = '\x20';
                cutStrLineBySeparator(*iter, separator, strBufferLineVec);
                width = strBufferLineVec.size();
                int numCols = 0;
                for (vector<string>::iterator iterLine = strBufferLineVec.begin(); iterLine != strBufferLineVec.end(); iterLine++)
                {
                    stringstream ss;
                    ss << *iterLine;
                    int num;
                    ss >> num;
                    ss.clear();
                    intVec.push_back(num);
                    //cout << num<<" "; 
                    if ((numLines == 0) && (numCols == 0))
                    {
                        numTotal = num;
                    }
                    if (numLines > 0)   ////< XZH cell for file by code
                    //if ((numLines > 0) && (numLines <= 10000))   ////< XZH  for file by gen
                    {
                        if ((numCols > 1) && (numCols < 8))  ////< XZH real tetra vertices 4
                        {
                            switch (numCols)
                            {
                            case 2:
                                i0 = num;
                                break;
                            case 3:
                                i1 = num;
                                break;
                            case 4:
                                i2 = num;
                                break;
                            case 5:
                                i3 = num;
							case 6:
								i4 = num;
							case 7:
								i5 = num;
                                break;
                            }
                            //cout << num << " ";
                        }
                    }
                    //else if (numLines > 10000)   ////< XZH face> 10000 for fule by gen
					if(0)    ////< XZH for file by code
                    {
                        if ((numCols > 0) && (numCols < 5))  ////< XZH real tetra vertices 4
                        {
                            switch (numCols)
                            {
                            case 1:
                                i0 = num;
                                break;
                            case 2:
                                i1 = num;
                                break;
                            case 3:
                                i2 = num;
                                break;
                            case 4:
                                i3 = num;
                                break;
                            }
                            //cout << num << " ";
                        }
                        
                    }
                    numCols++;
                }
            }
            //cout << endl;
            if (numLines > 0)   ////< XZH cell
            {
                imstk::SurfaceMesh::TriangleArray tri1 = { i0, i1, i2 };
                m_triangleVerticesAllXZH.push_back(tri1);
				m_triangleIsAllTetIndexXZH.push_back(i3);   ////< XZH tet index
				m_triangleIsAllTetIndexXZH2.push_back(i4);
                m_triangleIsBoundaryAllXZH.push_back(i5);   ////< XZH whether is boundary
            }
            numLines++;
        }
        cout << numLines << "->end..." << endl;
        infileFaceAll.close();
          ////< XZH Face ALL END

        ////< XZH edge
        strBufferVec.clear();
        std::fstream infileEdge(p_FileEdge);
        if (!infileEdge)
        {
            cout << "Edge failed" << endl;
            exit(1);
        }
        cout << "Edge start..." << endl;
        strLine = "";
        numLines = 0;
        numTotal = 0;
        i0 = 0; i1 = 0;  i2 = 0;  i3 = 0;
        while (getline(infileEdge, strLine))
        {
            if (numLines > numTotal) break;

            strBufferVec.push_back(strLine);

            vector<string> strBufferVecTmp;
            strBufferVecTmp.push_back(strLine);
            ////< XZH split
            vector<int> intVec;
            int width;
            for (vector<string>::iterator iter = strBufferVecTmp.begin(); iter != strBufferVecTmp.end(); iter++)
            {
                vector<string> strBufferLineVec;
                char separator = '\x20';
                cutStrLineBySeparator(*iter, separator, strBufferLineVec);
                width = strBufferLineVec.size();
                int numCols = 0;
                for (vector<string>::iterator iterLine = strBufferLineVec.begin(); iterLine != strBufferLineVec.end(); iterLine++)
                {
                    stringstream ss;
                    ss << *iterLine;
                    int num;
                    ss >> num;
                    ss.clear();
                    intVec.push_back(num);
                    //cout << num<<" "; 
                    if ((numLines == 0) && (numCols == 0))
                    {
                        numTotal = num;
                    }
                    if (numLines > 0)   ////< XZH cell for file by code
                    //if ((numLines > 0) && (numLines <= 10000))   ////< XZH for file by gen 
                    {
                        if ((numCols > 1) && (numCols < 4))  ////< XZH real tetra vertices 4
                        {
                            switch (numCols)
                            {
                            case 2:
                                i0 = num;
                                break;
                            case 3:
                                i1 = num;
                                break;
                            }
                            //cout << num << " ";
                        }
                    }
                    //else if (numLines > 10000)   ////< XZH for file by gen 
					if(0)   ////< XZH for file by code
                    {
                        if ((numCols > 0) && (numCols < 3))  ////< XZH real tetra vertices 4
                        {
                            switch (numCols)
                            {
                            case 1:
                                i0 = num;
                                break;
                            case 2:
                                i1 = num;
                                break;
                            }
                            //cout << num << " ";
                        }
                    }
                    numCols++;
                }
            }
            //cout << endl;
            if (numLines > 0)   ////< XZH cell
            {
                imstk::EdgeArray eg1 = { i0, i1 };
                m_edgesVerticesXZH.push_back(eg1);
            }
            numLines++;
        }
        cout << numLines << "->end..." << endl;
        infileEdge.close();
          ////< XZH EDGE END

          ////< XZH edge ALL
        strBufferVec.clear();
        std::fstream infileEdgeAll(p_FileEdgeAll);
        if (!infileEdgeAll)
        {
            cout << "EdgeALL failed" << endl;
            exit(1);
        }
        cout << "EdgeALL start..." << endl;
        strLine = "";
        numLines = 0;
        numTotal = 0;
        i0 = 0; i1 = 0;  i2 = 0;  i3 = 0;
        while (getline(infileEdgeAll, strLine))
        {
            if (numLines > numTotal) break;

            strBufferVec.push_back(strLine);

            vector<string> strBufferVecTmp;
            strBufferVecTmp.push_back(strLine);
            ////< XZH split
            vector<int> intVec;
            int width;
            for (vector<string>::iterator iter = strBufferVecTmp.begin(); iter != strBufferVecTmp.end(); iter++)
            {
                vector<string> strBufferLineVec;
                char separator = '\x20';
                cutStrLineBySeparator(*iter, separator, strBufferLineVec);
                width = strBufferLineVec.size();
                int numCols = 0;
                for (vector<string>::iterator iterLine = strBufferLineVec.begin(); iterLine != strBufferLineVec.end(); iterLine++)
                {
                    stringstream ss;
                    ss << *iterLine;
                    int num;
                    ss >> num;
                    ss.clear();
                    intVec.push_back(num);
                    //cout << num<<" "; 
                    if ((numLines == 0) && (numCols == 0))
                    {
                        numTotal = num;
                    }
                    if (numLines > 0)   ////< XZH cell for file by code
                    //if ((numLines > 0) && (numLines <= 10000))   ////< XZH Face ALL for file by gen
                    {
                        if ((numCols > 1) && (numCols < 5))  ////< XZH real tetra vertices 4
                        {
                            switch (numCols)
                            {
                            case 2:
                                i0 = num;
                                break;
                            case 3:
                                i1 = num;
                                break;
                            case 4:
                                i2 = num;
                                break;
                            }
                            //cout << num << " ";
                        }
                    }
                    //else if (numLines > 10000)   ////< XZH face> 10000 for file by gen 
					if(0)   ////< XZH for file by code
                    {
                        if ((numCols > 0) && (numCols < 4))  ////< XZH real tetra vertices 4
                        {
                            switch (numCols)
                            {
                            case 1:
                                i0 = num;
                                break;
                            case 2:
                                i1 = num;
                                break;
                            case 3:
                                i2 = num;
                                break;
                            }
                            //cout << num << " ";
                        }

                    }
                    numCols++;
                }
            }
            //cout << endl;
            if (numLines > 0)   ////< XZH cell
            {
                imstk::EdgeArray eg1 = { i0, i1};
                m_edgesVerticesAllXZH.push_back(eg1);
                m_edgesBoundaryAllXZH.push_back(i2);
            }
            numLines++;
        }
        cout << numLines << "->end..." << endl;
        infileEdgeAll.close();
          ////< XZH edge ALL END

        ////< XZH node
        imstk::StdVectorOfVec3d vertListXYZ;
        strBufferVec.clear();
        std::fstream infileNode(p_FileVerts);
        if (!infileNode)
        {
            cout << "Node failed" << endl;
            exit(1);
        }
        cout << "Node start..." << endl;
        strLine = "";
        numLines = 0;
        numTotal = 0;
        double f0 = 0, f1 = 0, f2 = 0;
        while (getline(infileNode, strLine))
        {
            if (numLines > numTotal) break;

            strBufferVec.push_back(strLine);

            vector<string> strBufferVecTmp;
            strBufferVecTmp.push_back(strLine);
            ////< XZH split
            vector<int> intVec;
            int width;
            int numCols = 0;
            for (vector<string>::iterator iter = strBufferVecTmp.begin(); iter != strBufferVecTmp.end(); iter++)
            {
                vector<string> strBufferLineVec;
                char separator = '\x20';
                cutStrLineBySeparator(*iter, separator, strBufferLineVec);
                width = strBufferLineVec.size();

                for (vector<string>::iterator iterLine = strBufferLineVec.begin(); iterLine != strBufferLineVec.end(); iterLine++)
                {
                    stringstream ss;
                    ss << *iterLine;
                    double numF; //  int num;
                    ss >> numF;
                    ss.clear();
                    //intVec.push_back(num);
                    //cout << num<<" "; 
                    if ((numLines == 0) && (numCols == 0))
                    {
                        numTotal = numF;
                    }
					if (numLines > 0)   ////< XZH for file by code
                    //if ((numLines > 0) && (numLines <= 1000))   ////< XZH cell for file by gen
                    {
                        if ((numCols > 1) && (numCols < 5))  ////< XZH real tetra vertices 4
                        {
                            switch (numCols)
                            {
                            case 2:
                                f0 = numF;
                                break;
                            case 3:
                                f1 = numF;
                                break;
                            case 4:
                                f2 = numF;
                                break;
                            }
                        }
                        //cout << numF << " ";
                    }
                    //else if (numLines > 1000)   ////< XZH for file by gen
					if(0)   ////< XZH for file by code
                    {
                        if ((numCols > 0) && (numCols < 4))  ////< XZH real tetra vertices 4
                        {
                            switch (numCols)
                            {
                            case 1:
                                f0 = numF;
                                break;
                            case 2:
                                f1 = numF;
                                break;
                            case 3:
                                f2 = numF;
                                break;
                            }
                        }
                        //cout << numF << " ";
                    }
                    numCols++;
                }
            }
            //cout << "_" << numCols << "_" << numLines << endl;
            if (numLines > 0)   ////< XZH cell
            {
                imstk::Vec3d tri1 = imstk::Vec3d(f0, f1, f2); // imstk::Vec3d(f0, f1 - 15.0, f2 + 1.0); // imstk::Vec3d(f0, f1, f2);  // imstk::Vec3d(f0, f1 - 5.0, f2 + 15.0);
                m_verticesPosXZH.push_back(tri1);
                vertListXYZ.push_back(tri1);
            }
            numLines++;
        }
        cout << numLines << "->end..." << endl;
        infileNode.close();
        m_verticesPostionsXZH = vertListXYZ;
        m_surfaceBoundaryXZH = std::make_shared<imstk::SurfaceMesh>();
        m_surfaceBoundaryXZH->initialize(vertListXYZ, m_triangleVerticesBoundXZH);
        //m_surfaceBoundaryXZH->rotate(imstk::UP_VECTOR, imstk::PI, imstk::Geometry::TransformType::ApplyToData);
        //m_surfaceBoundaryXZH->rotate(imstk::RIGHT_VECTOR, imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);
        //m_surfaceBoundaryXZH->translate(0, -6.0, 0, imstk::Geometry::TransformType::ApplyToData);

        m_verticesPostionsXZH = m_surfaceBoundaryXZH->getVertexPositions();
        ////< XZH mapping

        ////< XZH initial
        this->setInitialVertexPositions(m_verticesPostionsXZH);
        this->setVertexPositions(m_verticesPostionsXZH);
        this->setTetrahedraVertices(m_tetrahedraVertices);
        this->setInitTetrahedraVertices(m_tetrahedraVertices); // for graph
        return true;
    }

    bool VESSTetCutMesh::extractSurfaceMeshVisual()
    {
        bool isExist = false;
        if (m_surfaceVisual)
        {
            isExist = true;
        }
        else
        {
            isExist = false;
            m_surfaceVisual = std::make_shared<imstk::SurfaceMesh>();
        }
        if (!m_surfaceVisual)
        {
            LOG(WARNING) << "TetrahedralMesh::extractSurfaceMeshVisual error: the surface mesh provided is not instantiated.";
            return false;
        }

        U32 num = m_triangleVerticesAllXZH.size();
        std::vector<triArray> surfaceTrisFromTet; // XZH
        StdVectorOfVec3d posList;   ////< XZH  = m_verticesPosXZH;
        for (U32 i = 0; i < m_verticesPostionsXZH.size(); i++)
        {
            posList.push_back(m_verticesPostionsXZH[i]);
        }
        for (U32 i = 0; i < num; i++)
        {
            auto& nodes = m_triangleVerticesAllXZH[i];
            auto& flag = m_triangleIsBoundaryAllXZH[i];

            if (flag)
            {
                surfaceTrisFromTet.push_back(nodes);
                m_surfaceVisualTriIndexInTet.push_back(i);
            }
        }
        m_triangleVerticesBoundXZH.clear();
        m_triangleVerticesBoundXZH = surfaceTrisFromTet;
        m_surfaceVisual->initialize(posList, surfaceTrisFromTet, true);
        setVisualFromTet(m_surfaceVisual);
    }

    bool VESSTetCutMesh::extractSurfaceMeshBoundary(const std::vector<Vec3d>& centralLine)
    {
        Vec3d p0 = centralLine[0];  //  Vec3d(-19.4, -5.85, 1.15); //  centralLine[0];
        Vec3d p1 = centralLine[1];  // Vec3d(-3.3, 2.4, 1.05); //  centralLine[1];
        Vec3d pcen0 = p0 *0.9 + p1*0.1;
        Vec3d pcen1 = p0 *0.5 + p1*0.5;
        Vec3d pcen2 = p0 *0.3 + p1*0.7;

        bool isExist = false;
        if (m_surfaceVisualInsideXZH)
        {
            isExist = true;
        }
        else
        {
            isExist = false;
            m_surfaceVisualInsideXZH = std::make_shared<imstk::SurfaceMesh>();
        }
        if (!m_surfaceVisualInsideXZH)
        {
            LOG(WARNING) << "TetrahedralMesh::extractSurfaceMeshInside error: the surface mesh provided is not instantiated.";
            return false;
        }

        bool isExist2 = false;
        if (m_surfaceVisualOutsideXZH)
        {
            isExist2 = true;
        }
        else
        {
            isExist2 = false;
            m_surfaceVisualOutsideXZH = std::make_shared<imstk::SurfaceMesh>();
        }
        if (!m_surfaceVisualOutsideXZH)
        {
            LOG(WARNING) << "TetrahedralMesh::extractSurfaceMeshOutside error: the surface mesh provided is not instantiated.";
            return false;
        }

        ////< XZH step1
        // Arrange the surface triangle faces found in order
        Vec3d v0, v1, v2;
        Vec3d centroid;
        Vec3d normal;
        std::vector<triArray> surfaceInsideTri; // XZH
        std::vector<triArray> surfaceOutsideTri; // XZH
        int n0, n1, n2;

        //m_verticesPostionsXZH = m_surfaceVisualXZH->getVertexPositions();
        //m_triangleVerticesXZH = m_surfaceVisualXZH->getTrianglesVertices();
        auto m_verticesPostionsXZHVisual = m_surfaceVisualXZH->getVertexPositions();
        auto m_triangleVerticesXZHVisual = m_surfaceVisualXZH->getTrianglesVertices();
        m_statesVisualXZH.resize(m_triangleVerticesXZHVisual.size());

        for (size_t faceId = 0; faceId < m_triangleVerticesXZHVisual.size(); ++faceId)
        {
            n0 = m_triangleVerticesXZHVisual.at(faceId)[0];
            n1 = m_triangleVerticesXZHVisual.at(faceId)[1];
            n2 = m_triangleVerticesXZHVisual.at(faceId)[2];
            v0 = m_verticesPostionsXZHVisual[n0];
            v1 = m_verticesPostionsXZHVisual[n1];
            v2 = m_verticesPostionsXZHVisual[n2];

            centroid = (v0 + v1 + v2) / 3;

            Vec3d vecXZH0 = pcen0 - centroid;
            Vec3d vecXZH1 = pcen1 - centroid;
            Vec3d vecXZH2 = pcen2 - centroid;
            normal = (v1 - v0).cross(v2 - v0).normalized(); // ((v0 - v1).cross(v0 - v2));
            if ((normal.dot(vecXZH0) > 0) || (normal.dot(vecXZH1) > 0) || (normal.dot(vecXZH2) > 0))  // normal indicating the inside colon
            {
                triArray tttmp = { n0, n1, n2 };
                surfaceInsideTri.push_back(tttmp); //  (surfaceTri[faceId]);
                m_statesVisualXZH[faceId] = 1;
            }
            else
            {
                triArray tttmp = { n0, n1, n2 };
                surfaceOutsideTri.push_back(tttmp);
                m_statesVisualXZH[faceId] = 0;
            }

            //// XZH
            //Vec3d a = centroid - p0;
            //Vec3d b = p1 - p0;
            //Vec3d c = ((a.dot(b)) / (b.dot(b)))*b;
            //Vec3d e = a - c;
            //Vec3d v00 = v0;
            //Vec3d v11 = v1;
            //Vec3d v22 = v2;
            //Vec3d normal2 = (v11 - v00).cross(v22 - v00);
            //if (normal2.dot(-e) > 0)  // normal indicating the inside colon
            //{
            //    triArray tttmp = { n0, n1, n2 };
            //    surfaceInsideTri.push_back(tttmp); //  (surfaceTri[faceId]);
            //}
            //else
            //{
            //    triArray tttmp = { n0, n1, n2 };
            //    surfaceOutsideTri.push_back(tttmp);
            //}
            // XZH END
        }
        //m_tetrahedraSurfaceVertices = surfaceTri;  //  use both sides of surface of volume mesh for collision detection

        // Create and attach surface mesh
        m_surfaceVisualInsideXZH->initialize(m_verticesPostionsXZHVisual, surfaceInsideTri, true); // if true, calculate normal
        m_surfaceVisualOutsideXZH->initialize(m_verticesPostionsXZHVisual, surfaceOutsideTri, true); // if true, calculate normal

        ////< XZH only for using tet mesh self-generated
        //m_surfaceVisualInsideXZH->rotate(imstk::UP_VECTOR, imstk::PI, imstk::Geometry::TransformType::ApplyToData);
        //m_surfaceVisualInsideXZH->rotate(imstk::RIGHT_VECTOR, imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);
        //m_surfaceVisualInsideXZH->translate(0, -6.0, 0, imstk::Geometry::TransformType::ApplyToData);
        //m_surfaceVisualOutsideXZH->rotate(imstk::UP_VECTOR, imstk::PI, imstk::Geometry::TransformType::ApplyToData);
        //m_surfaceVisualOutsideXZH->rotate(imstk::RIGHT_VECTOR, imstk::PI / 2, imstk::Geometry::TransformType::ApplyToData);
        //m_surfaceVisualOutsideXZH->translate(0, -6.0, 0, imstk::Geometry::TransformType::ApplyToData);
        //surfaceMesh->print();
        return true;
    }

    bool VESSTetCutMesh::extractSurfaceMeshVolume()
    {
        bool isExist = false;
        if (m_surfaceVolumeXZH)
        {
            isExist = true;
        }
        else
        {
            isExist = false;
            m_surfaceVolumeXZH = std::make_shared<imstk::SurfaceMesh>();
        }
        if (!m_surfaceVolumeXZH)
        {
            LOG(WARNING) << "TetrahedralMesh::extractSurfaceMeshVolume error: the surface mesh provided is not instantiated.";
            return false;
        }

        auto m_verticesPostions = m_verticesPostionsXZH;
        auto m_triangleVerticesTet = m_tetrahedraVertices;
        const std::vector<triArray> facePattern = { triArray{ { 0, 1, 2 } }, triArray{ { 0, 1, 3 } }, triArray{ { 0, 2, 3 } }, triArray{ { 1, 2, 3 } } };
        std::vector<triArray> surfaceTriVolume;
        for (int i = 0; i < m_triangleVerticesTet.size();i++)
        {
            auto& tetVertArray = m_triangleVerticesTet[i];
			size_t a, b, c;
            for (int t = 0; t < 4;t++)
            {
                a = tetVertArray[facePattern[t][0]];
                b = tetVertArray[facePattern[t][1]];
                c = tetVertArray[facePattern[t][2]];
                surfaceTriVolume.push_back(triArray{ { a, b, c } });
            }
        }
        m_surfaceVolumeXZH->initialize(m_verticesPostions, surfaceTriVolume);
    }

    bool
        VESSTetCutMesh::extractSurfaceMesh2(const std::vector<Vec3d>& centralLine)
    {
        Vec3d p0 = centralLine[0];  //  Vec3d(-19.4, -5.85, 1.15); //  centralLine[0];
        Vec3d p1 = centralLine[1];  // Vec3d(-3.3, 2.4, 1.05); //  centralLine[1];

        bool isExist = false;
        if (surfaceMesh)
        {
            isExist = true;
        }
        else
        {
            isExist = false;
            surfaceMesh = std::make_shared<imstk::SurfaceMesh>();
        }

        if (!surfaceMesh)
        {
            LOG(WARNING) << "TetrahedralMesh::extractSurfaceMesh error: the surface mesh provided is not instantiated.";
            return false;
        }

        //m_tetrahedraSurfaceVertices = surfaceTri;  //  use both sides of surface of volume mesh for collision detection
        m_tetrahedraSurfaceVertices = m_surfaceVisual->getTrianglesVertices(); // surfaceInsideTri; // only use inside surface of volume mesh for collision detection
        auto vPoses = m_surfaceVisual->getInitialVertexPositions();
        m_tetrahedraSurfaceIndex.clear(); // 
        m_mapEdgeTriIndex.clear();
        m_edgeCollision.clear();
        m_edgeColIndex.clear();
        m_surftrisCollision.clear();
        m_surftrisColIndex.clear();
        m_mapEdgeIndex.clear(); // just edge number

        physEdge edgeTmp;
        physSurfTris surftrisTmp;
        int nbrNeiSurfTri0 = -1;
        int nbrNeiSurfTri1 = -1;
        int nbrNeiSurfTri2 = -1;
        int nbrNeiSurfEdge0 = -1;
        int nbrNeiSurfEdge1 = -1;
        int nbrNeiSurfEdge2 = -1;
        int eIdx = -1;
        for (int k = 0; k < m_tetrahedraSurfaceVertices.size(); k++)
        {
			bool flag0 = m_tetrahedraDissectFlag[m_triangleIsAllTetIndexXZH[m_surfaceVisualTriIndexInTet[k]]];
			int idxtmp = m_triangleIsAllTetIndexXZH2[m_surfaceVisualTriIndexInTet[k]];
			bool flag = false;
			if (idxtmp < 0) flag = flag0;
			else
			{
				bool flag1 = m_tetrahedraDissectFlag[idxtmp];
				flag = flag0 || flag1;
			}
			
			if(!flag) continue;
            int vert0 = m_tetrahedraSurfaceVertices.at(k)[0]; // three vertices of triangle
            int vert1 = m_tetrahedraSurfaceVertices.at(k)[1];
            int vert2 = m_tetrahedraSurfaceVertices.at(k)[2];
            Vec3d vp0 = vPoses[vert0];
            Vec3d vp1 = vPoses[vert1];
            Vec3d vp2 = vPoses[vert2];
            Vec3d normtri = (vp1 - vp0).cross(vp2 - vp0).normalized();
            Vec3d centroid = (vp0 + vp1 + vp2) / 3;
            Vec3d a = centroid - p0;
            Vec3d b = p1 - p0;
            Vec3d c = ((a.dot(b)) / (b.dot(b)))*b;
            Vec3d e = a - c;
            if (normtri.dot(-e) <= 0) continue; // normal indicating the inside colon

            U64 edgekey;
            int edge0, edge1, edge2;
            edge0 = edge1 = edge2 = -1;
            // 3 edges 0-1
            int m0, m1;
            m0 = vert0; m1 = vert1;
            if (m0 > m1)
            {
                std::swap(m0, m1);
            }
            edgekey = HASHEDGEID_FROM_IDX(m0, m1);
            MAPHEDGEINDEXITER it = m_mapEdgeTriIndex.find(edgekey);
            if (it == m_mapEdgeTriIndex.end())
            {
                m_mapEdgeTriIndex.insert(std::make_pair(edgekey, k));
                edgeTmp.nodeId[0] = vert0;
                edgeTmp.nodeId[1] = vert1;
                edgeTmp.L0 = (this->getVertexPosition(vert0) - this->getVertexPosition(vert1)).norm();
                edgeTmp.colDetOn = true;
                m_edgeCollision.push_back(edgeTmp);
                eIdx++;
                edge0 = eIdx;
                m_edgeColIndex.push_back(eIdx);
                m_mapEdgeIndex.insert(std::make_pair(edgekey, eIdx));
            }
            else
            {
                MAPHEDGEINDEXITER it2 = m_mapEdgeIndex.find(edgekey);
                if (it2 != m_mapEdgeIndex.end())
                {
                    edge0 = it2->second;
                }
            }
            // 1-2
            m0 = vert1; m1 = vert2;
            if (m0 > m1)
            {
                std::swap(m0, m1);
            }
            edgekey = HASHEDGEID_FROM_IDX(m0, m1);
            it = m_mapEdgeTriIndex.find(edgekey);
            if (it == m_mapEdgeTriIndex.end())
            {
                m_mapEdgeTriIndex.insert(std::make_pair(edgekey, k));
                edgeTmp.nodeId[0] = vert1;
                edgeTmp.nodeId[1] = vert2;
                edgeTmp.L0 = (this->getVertexPosition(vert1) - this->getVertexPosition(vert2)).norm();
                edgeTmp.colDetOn = true;
                m_edgeCollision.push_back(edgeTmp);
                eIdx++;
                edge1 = eIdx;
                m_edgeColIndex.push_back(eIdx);
                m_mapEdgeIndex.insert(std::make_pair(edgekey, eIdx));
            }
            else
            {
                MAPHEDGEINDEXITER it2 = m_mapEdgeIndex.find(edgekey);
                if (it2 != m_mapEdgeIndex.end())
                {
                    edge1 = it2->second;
                }
            }
            // 0-2
            m0 = vert0; m1 = vert2;
            if (m0 > m1)
            {
                std::swap(m0, m1);
            }
            edgekey = HASHEDGEID_FROM_IDX(m0, m1);
            it = m_mapEdgeTriIndex.find(edgekey);
            if (it == m_mapEdgeTriIndex.end())
            {
                m_mapEdgeTriIndex.insert(std::make_pair(edgekey, k));
                edgeTmp.nodeId[0] = vert0;
                edgeTmp.nodeId[1] = vert2;
                edgeTmp.L0 = (this->getVertexPosition(vert0) - this->getVertexPosition(vert2)).norm();
                edgeTmp.colDetOn = true;
                m_edgeCollision.push_back(edgeTmp);
                eIdx++;
                edge2 = eIdx;
                m_edgeColIndex.push_back(eIdx);
                m_mapEdgeIndex.insert(std::make_pair(edgekey, eIdx));
            }
            else
            {
                MAPHEDGEINDEXITER it2 = m_mapEdgeIndex.find(edgekey);
                if (it2 != m_mapEdgeIndex.end())
                {
                    edge2 = it2->second;
                }
            }

            // vertex for collision detection
            U64 vertexkey;
            physVertex vertexTmp;
            vertexkey = HASHEDGEID_FROM_IDX(vert0, 1000000);  // will not exceed 1 million, so we can use this way to get unique vertex
            it = m_mapVertexIndex.find(vertexkey);
            if (it == m_mapVertexIndex.end())  // not exist
            {
                m_mapVertexIndex.insert(std::make_pair(vertexkey, k));
                m_vertexCollision[vert0].globalIdx = vert0;
                m_vertexCollision[vert0].onSurf = true;
                m_vertexCollision[vert0].colDetOn = true;
                //vertexTmp.globalIdx = vert0;
                //vertexTmp.oPos = this->getVertexPosition(vert0);
                //vertexTmp.pos = vertexTmp.oPos;
                //vertexTmp.p = vertexTmp.oPos;
                //vertexTmp.pRot = vertexTmp.oPos;
                //std::vector<int>::iterator result = find(m_fixedVerticeList.begin(), m_fixedVerticeList.end(), vert0);  // find whether 
                //if (result == m_fixedVerticeList.end())  vertexTmp.fixed = false;  // cannot find
                //else vertexTmp.fixed = true;
                //m_vertexCollision.push_back(vertexTmp);
            }
            vertexkey = HASHEDGEID_FROM_IDX(vert1, 1000000);  // will not exceed 1 million, so we can use this way to get unique vertex
            it = m_mapVertexIndex.find(vertexkey);
            if (it == m_mapVertexIndex.end())  // not exist
            {
                m_mapVertexIndex.insert(std::make_pair(vertexkey, k));
                m_vertexCollision[vert1].globalIdx = vert1;
                m_vertexCollision[vert1].onSurf = true;
                m_vertexCollision[vert1].colDetOn = true;
                //vertexTmp.globalIdx = vert1;
                //vertexTmp.oPos = this->getVertexPosition(vert1);
                //vertexTmp.pos = vertexTmp.oPos;
                //vertexTmp.p = vertexTmp.oPos;
                //vertexTmp.pRot = vertexTmp.oPos;
                //m_vertexCollision.push_back(vertexTmp);
            }
            vertexkey = HASHEDGEID_FROM_IDX(vert2, 1000000);  // will not exceed 1 million, so we can use this way to get unique vertex
            it = m_mapVertexIndex.find(vertexkey);
            if (it == m_mapVertexIndex.end())  // not exist
            {
                m_mapVertexIndex.insert(std::make_pair(vertexkey, k));
                m_vertexCollision[vert2].globalIdx = vert2;
                m_vertexCollision[vert2].onSurf = true;
                m_vertexCollision[vert2].colDetOn = true;
                //vertexTmp.globalIdx = vert2;
                //vertexTmp.oPos = this->getVertexPosition(vert2);
                //vertexTmp.pos = vertexTmp.oPos;
                //vertexTmp.p = vertexTmp.oPos;
                //vertexTmp.pRot = vertexTmp.oPos;
                //m_vertexCollision.push_back(vertexTmp);
            }

            // vertex END

            // saving surf tris for collision
            surftrisTmp.nodeIdx[0] = vert0;
            surftrisTmp.nodeIdx[1] = vert1;
            surftrisTmp.nodeIdx[2] = vert2;

            surftrisTmp.edgeIdx[0] = edge0;
            surftrisTmp.edgeIdx[1] = edge1;
            surftrisTmp.edgeIdx[2] = edge2;

            surftrisTmp.tetraIdx = m_triTetlinksXZH[k].tetraIndex; // m_tetrahedraSurfaceIndex[k];
            surftrisTmp.colDetOn = true;
            m_surftrisCollision.push_back(surftrisTmp);
            m_surftrisColIndex.push_back(k);
        }
        FILE *temp = fopen("i:/testHash.dat", "w");
        for (MAPHEDGEINDEXITER it2 = m_mapEdgeTriIndex.begin(); it2 != m_mapEdgeTriIndex.end(); it2++) {
            fprintf(temp, "%lld %d\n", it2->first, it2->second);
        }
        fclose(temp);

        // traverse edge 
        for (int i = 0; i < m_edgeCollision.size(); i++)
        {
            int m0 = m_edgeCollision[i].nodeId[0];
            int m1 = m_edgeCollision[i].nodeId[1];

            for (int k = 0; k < m_vertexCollision.size(); k++)
            {
                m_vertexCollision[k].nbrNeiSurfEdge = -1;
                int k0 = m_vertexCollision[k].globalIdx;  // node global index compare m0 m1
                if ((k0 == m0) || (k0 == m1))
                {
                    m_vertexCollision[k].nbrNeiSurfEdge++;
                    m_vertexCollision[k].neiSurfEdge[m_vertexCollision[k].nbrNeiSurfEdge] = i;
                }
            }
        }
        // traverse tri to get neibors
        for (int i = 0; i < m_surftrisCollision.size(); i++)
        {
            int m0 = m_surftrisCollision[i].nodeIdx[0];
            int m1 = m_surftrisCollision[i].nodeIdx[1];
            int m2 = m_surftrisCollision[i].nodeIdx[2];

            for (int k = 0; k < m_vertexCollision.size(); k++)
            {
                m_vertexCollision[k].nbrNeiSurfTri = -1;
                int k0 = m_vertexCollision[k].globalIdx;  // node global index compare m0 m1
                if ((k0 == m0) || (k0 == m1) || (k0 == m2))
                {
                    m_vertexCollision[k].nbrNeiSurfTri++;
                    m_vertexCollision[k].neiSurfTri[m_vertexCollision[k].nbrNeiSurfTri] = i;
                }
            }
        }
        // END XZH map

        // output 
        FILE* outVEF = fopen("i:/vef.dat", "w");
        for (int i = 0; i < m_vertexCollision.size(); i++)
        {
            int vertx = m_vertexCollision[i].globalIdx;
			if (m_vertexCollision[i].onSurf)
			{
				m_vertexCollision[i].onSurfSurrounding = true;
			}
			std::vector<U32> vecNeiSurfTri;
            // neighbor edges in colliding
            m_vertexCollision[i].nbrNeiSurfEdge = 0;
            memset(m_vertexCollision[i].neiSurfEdge, -1, 100 * sizeof(int));
            fprintf(outVEF, "\n vert: %d\n neiEdge: ", vertx);
            int numTmp = 0;
            for (int k = 0; k < m_edgeCollision.size(); k++)
            {
                int m0 = m_edgeCollision[k].nodeId[0];
                int m1 = m_edgeCollision[k].nodeId[1];
                if ((m0 == vertx) || (m1 == vertx))
                {
                    m_vertexCollision[i].nbrNeiSurfEdge++;
                    m_vertexCollision[i].neiSurfEdge[numTmp] = k;
                    numTmp++;
                    fprintf(outVEF, " %d ", k);
                }
            }

            // neighbor triangle  neighbor edges in colliding
            m_vertexCollision[i].nbrNeiSurfTri = 0;
            memset(m_vertexCollision[i].neiSurfTri, -1, 100 * sizeof(int));
            fprintf(outVEF, "\n neiSurf:");
            numTmp = 0;
			m_nSurftris = m_surftrisCollision.size();
            for (int k = 0; k < m_surftrisCollision.size(); k++)
            {
                int m0 = m_surftrisCollision[k].nodeIdx[0];
                int m1 = m_surftrisCollision[k].nodeIdx[1];
                int m2 = m_surftrisCollision[k].nodeIdx[2];
                if ((m0 == vertx) || (m1 == vertx) || (m2 == vertx))
                {
					auto vlen = vecNeiSurfTri.size();
					vecNeiSurfTri.push_back(k);
                    //m_vertexCollision[i].nbrNeiSurfTri++;
                    //m_vertexCollision[i].neiSurfTri[numTmp] = k;
                    numTmp++;
					if (vecNeiSurfTri.size() >= 100)
					{
						printf("bug happend : vtex index %d,  surf index, %d \n", vertx, k);
					}
                    fprintf(outVEF, " %d ", k);
                }
            } // END for
			std::sort(vecNeiSurfTri.begin(), vecNeiSurfTri.end());
			std::unique(vecNeiSurfTri.begin(), vecNeiSurfTri.end());
			auto vsize = vecNeiSurfTri.size();
			m_vertexCollision[i].nbrNeiSurfTri = vsize;
			for (int v = 0; v < vsize; v++)
			{
				m_vertexCollision[i].neiSurfTri[v] = vecNeiSurfTri[v];
			}
        }
        fclose(outVEF);

        surfaceInsideMesh = std::make_shared<imstk::SurfaceMesh>();

        return true;
    }

	// not used in version4 10/07/2019
    bool
        VESSTetCutMesh::extractSurfaceMesh21(const std::vector<Vec3d>& centralLine)
    {
        Vec3d p0 = centralLine[0];  //  Vec3d(-19.4, -5.85, 1.15); //  centralLine[0];
        Vec3d p1 = centralLine[1];  // Vec3d(-3.3, 2.4, 1.05); //  centralLine[1];

        bool isExist = false;
        if (surfaceMesh)
        {
            isExist = true;
        }
        else
        {
            isExist = false;
            surfaceMesh = std::make_shared<imstk::SurfaceMesh>();
        }

        if (!surfaceMesh)
        {
            LOG(WARNING) << "TetrahedralMesh::extractSurfaceMesh error: the surface mesh provided is not instantiated.";
            return false;
        }

        //m_tetrahedraSurfaceVertices = surfaceTri;  //  use both sides of surface of volume mesh for collision detection
        m_tetrahedraSurfaceVertices = m_surfaceVisual->getTrianglesVertices(); // surfaceInsideTri; // only use inside surface of volume mesh for collision detection
        auto vPoses = m_surfaceVisual->getInitialVertexPositions();
        m_tetrahedraSurfaceIndex.clear(); // 
        m_mapEdgeTriIndex.clear();
        m_edgeCollision.clear();
        m_edgeColIndex.clear();
        m_surftrisCollision.clear();
        m_surftrisColIndex.clear();
        m_mapEdgeIndex.clear(); // just edge number

        physEdge edgeTmp;
        physSurfTris surftrisTmp;
        int nbrNeiSurfTri0 = -1;
        int nbrNeiSurfTri1 = -1;
        int nbrNeiSurfTri2 = -1;
        int nbrNeiSurfEdge0 = -1;
        int nbrNeiSurfEdge1 = -1;
        int nbrNeiSurfEdge2 = -1;
        int eIdx = -1;
        for (int k = 0; k < m_tetrahedraSurfaceVertices.size(); k++)
        {
            int vert0 = m_tetrahedraSurfaceVertices.at(k)[0]; // three vertices of triangle
            int vert1 = m_tetrahedraSurfaceVertices.at(k)[1];
            int vert2 = m_tetrahedraSurfaceVertices.at(k)[2];
            Vec3d vp0 = vPoses[vert0];
            Vec3d vp1 = vPoses[vert1];
            Vec3d vp2 = vPoses[vert2];
            Vec3d normtri = (vp1 - vp0).cross(vp2 - vp0).normalized();
            Vec3d centroid = (vp0 + vp1 + vp2) / 3;
            Vec3d a = centroid - p0;
            Vec3d b = p1 - p0;
            Vec3d c = ((a.dot(b)) / (b.dot(b)))*b;
            Vec3d e = a - c;
            //if (normtri.dot(-e) <= 0) continue; //  normal indicating the inside colon

            U64 edgekey;
            int edge0, edge1, edge2;
            edge0 = edge1 = edge2 = -1;
            // 3 edges 0-1
            int m0, m1;
            m0 = vert0; m1 = vert1;
            if (m0 > m1)
            {
                std::swap(m0, m1);
            }
            edgekey = HASHEDGEID_FROM_IDX(m0, m1);
            MAPHEDGEINDEXITER it = m_mapEdgeTriIndex.find(edgekey);
            if (it == m_mapEdgeTriIndex.end())
            {
                m_mapEdgeTriIndex.insert(std::make_pair(edgekey, k));
                edgeTmp.nodeId[0] = vert0;
                edgeTmp.nodeId[1] = vert1;
                edgeTmp.L0 = (this->getVertexPosition(vert0) - this->getVertexPosition(vert1)).norm();
                edgeTmp.colDetOn = true;
                m_edgeCollision.push_back(edgeTmp);
                eIdx++;
                edge0 = eIdx;
                m_edgeColIndex.push_back(eIdx);
                m_mapEdgeIndex.insert(std::make_pair(edgekey, eIdx));
            }
            else
            {
                MAPHEDGEINDEXITER it2 = m_mapEdgeIndex.find(edgekey);
                if (it2 != m_mapEdgeIndex.end())
                {
                    edge0 = it2->second;
                }
            }
            // 1-2
            m0 = vert1; m1 = vert2;
            if (m0 > m1)
            {
                std::swap(m0, m1);
            }
            edgekey = HASHEDGEID_FROM_IDX(m0, m1);
            it = m_mapEdgeTriIndex.find(edgekey);
            if (it == m_mapEdgeTriIndex.end())
            {
                m_mapEdgeTriIndex.insert(std::make_pair(edgekey, k));
                edgeTmp.nodeId[0] = vert1;
                edgeTmp.nodeId[1] = vert2;
                edgeTmp.L0 = (this->getVertexPosition(vert1) - this->getVertexPosition(vert2)).norm();
                edgeTmp.colDetOn = true;
                m_edgeCollision.push_back(edgeTmp);
                eIdx++;
                edge1 = eIdx;
                m_edgeColIndex.push_back(eIdx);
                m_mapEdgeIndex.insert(std::make_pair(edgekey, eIdx));
            }
            else
            {
                MAPHEDGEINDEXITER it2 = m_mapEdgeIndex.find(edgekey);
                if (it2 != m_mapEdgeIndex.end())
                {
                    edge1 = it2->second;
                }
            }
            // 0-2
            m0 = vert0; m1 = vert2;
            if (m0 > m1)
            {
                std::swap(m0, m1);
            }
            edgekey = HASHEDGEID_FROM_IDX(m0, m1);
            it = m_mapEdgeTriIndex.find(edgekey);
            if (it == m_mapEdgeTriIndex.end())
            {
                m_mapEdgeTriIndex.insert(std::make_pair(edgekey, k));
                edgeTmp.nodeId[0] = vert0;
                edgeTmp.nodeId[1] = vert2;
                edgeTmp.L0 = (this->getVertexPosition(vert0) - this->getVertexPosition(vert2)).norm();
                edgeTmp.colDetOn = true;
                m_edgeCollision.push_back(edgeTmp);
                eIdx++;
                edge2 = eIdx;
                m_edgeColIndex.push_back(eIdx);
                m_mapEdgeIndex.insert(std::make_pair(edgekey, eIdx));
            }
            else
            {
                MAPHEDGEINDEXITER it2 = m_mapEdgeIndex.find(edgekey);
                if (it2 != m_mapEdgeIndex.end())
                {
                    edge2 = it2->second;
                }
            }

            // vertex for collision detection
            U64 vertexkey;
            physVertex vertexTmp;
            vertexkey = HASHEDGEID_FROM_IDX(vert0, 1000000);  // will not exceed 1 million, so we can use this way to get unique vertex
            it = m_mapVertexIndex.find(vertexkey);
            if (it == m_mapVertexIndex.end())  // not exist
            {
                m_mapVertexIndex.insert(std::make_pair(vertexkey, k));
                m_vertexCollision[vert0].globalIdx = vert0;
                m_vertexCollision[vert0].onSurf = true;
                m_vertexCollision[vert0].colDetOn = true;
                //vertexTmp.globalIdx = vert0;
                //vertexTmp.oPos = this->getVertexPosition(vert0);
                //vertexTmp.pos = vertexTmp.oPos;
                //vertexTmp.p = vertexTmp.oPos;
                //vertexTmp.pRot = vertexTmp.oPos;
                //std::vector<int>::iterator result = find(m_fixedVerticeList.begin(), m_fixedVerticeList.end(), vert0);  // find whether 
                //if (result == m_fixedVerticeList.end())  vertexTmp.fixed = false;  // cannot find
                //else vertexTmp.fixed = true;
                //m_vertexCollision.push_back(vertexTmp);
            }
            vertexkey = HASHEDGEID_FROM_IDX(vert1, 1000000);  // will not exceed 1 million, so we can use this way to get unique vertex
            it = m_mapVertexIndex.find(vertexkey);
            if (it == m_mapVertexIndex.end())  // not exist
            {
                m_mapVertexIndex.insert(std::make_pair(vertexkey, k));
                m_vertexCollision[vert1].globalIdx = vert1;
                m_vertexCollision[vert1].onSurf = true;
                m_vertexCollision[vert1].colDetOn = true;
                //vertexTmp.globalIdx = vert1;
                //vertexTmp.oPos = this->getVertexPosition(vert1);
                //vertexTmp.pos = vertexTmp.oPos;
                //vertexTmp.p = vertexTmp.oPos;
                //vertexTmp.pRot = vertexTmp.oPos;
                //m_vertexCollision.push_back(vertexTmp);
            }
            vertexkey = HASHEDGEID_FROM_IDX(vert2, 1000000);  // will not exceed 1 million, so we can use this way to get unique vertex
            it = m_mapVertexIndex.find(vertexkey);
            if (it == m_mapVertexIndex.end())  // not exist
            {
                m_mapVertexIndex.insert(std::make_pair(vertexkey, k));
                m_vertexCollision[vert2].globalIdx = vert2;
                m_vertexCollision[vert2].onSurf = true;
                m_vertexCollision[vert2].colDetOn = true;
                //vertexTmp.globalIdx = vert2;
                //vertexTmp.oPos = this->getVertexPosition(vert2);
                //vertexTmp.pos = vertexTmp.oPos;
                //vertexTmp.p = vertexTmp.oPos;
                //vertexTmp.pRot = vertexTmp.oPos;
                //m_vertexCollision.push_back(vertexTmp);
            }

            // vertex END

            // saving surf tris for collision
            surftrisTmp.nodeIdx[0] = vert0;
            surftrisTmp.nodeIdx[1] = vert1;
            surftrisTmp.nodeIdx[2] = vert2;

            surftrisTmp.edgeIdx[0] = edge0;
            surftrisTmp.edgeIdx[1] = edge1;
            surftrisTmp.edgeIdx[2] = edge2;

            surftrisTmp.tetraIdx = m_triTetlinksXZH[k].tetraIndex; // m_tetrahedraSurfaceIndex[k];
            surftrisTmp.colDetOn = true;
            m_surftrisCollision.push_back(surftrisTmp);
            m_surftrisColIndex.push_back(k);
        }
        FILE *temp = fopen("i:/testHash.dat", "w");
        for (MAPHEDGEINDEXITER it2 = m_mapEdgeTriIndex.begin(); it2 != m_mapEdgeTriIndex.end(); it2++) {
            fprintf(temp, "%lld %d\n", it2->first, it2->second);
        }
        fclose(temp);

        // traverse edge 
        for (int i = 0; i < m_edgeCollision.size(); i++)
        {
            int m0 = m_edgeCollision[i].nodeId[0];
            int m1 = m_edgeCollision[i].nodeId[1];

            for (int k = 0; k < m_vertexCollision.size(); k++)
            {
                m_vertexCollision[k].nbrNeiSurfEdge = -1;
                int k0 = m_vertexCollision[k].globalIdx;  // node global index compare m0 m1
                if ((k0 == m0) || (k0 == m1))
                {
                    m_vertexCollision[k].nbrNeiSurfEdge++;
                    m_vertexCollision[k].neiSurfEdge[m_vertexCollision[k].nbrNeiSurfEdge] = i;
                }
            }
        }
        // traverse tri to get neibors
        for (int i = 0; i < m_surftrisCollision.size(); i++)
        {
            int m0 = m_surftrisCollision[i].nodeIdx[0];
            int m1 = m_surftrisCollision[i].nodeIdx[1];
            int m2 = m_surftrisCollision[i].nodeIdx[2];

            for (int k = 0; k < m_vertexCollision.size(); k++)
            {
                m_vertexCollision[k].nbrNeiSurfTri = -1;
                int k0 = m_vertexCollision[k].globalIdx;  // node global index compare m0 m1
                if ((k0 == m0) || (k0 == m1) || (k0 == m2))
                {
                    m_vertexCollision[k].nbrNeiSurfTri++;
                    m_vertexCollision[k].neiSurfTri[m_vertexCollision[k].nbrNeiSurfTri] = i;
                }
            }
        }
        // END XZH map

        // output 
        FILE* outVEF = fopen("i:/vef.dat", "w");
        for (int i = 0; i < m_vertexCollision.size(); i++)
        {
            int vertx = m_vertexCollision[i].globalIdx;

            // neighbor edges in colliding
            m_vertexCollision[i].nbrNeiSurfEdge = 0;
            memset(m_vertexCollision[i].neiSurfEdge, -1, 100 * sizeof(int));
            fprintf(outVEF, "\n vert: %d\n neiEdge: ", vertx);
            int numTmp = 0;
            for (int k = 0; k < m_edgeCollision.size(); k++)
            {
                int m0 = m_edgeCollision[k].nodeId[0];
                int m1 = m_edgeCollision[k].nodeId[1];
                if ((m0 == vertx) || (m1 == vertx))
                {
                    m_vertexCollision[i].nbrNeiSurfEdge++;
                    m_vertexCollision[i].neiSurfEdge[numTmp] = k;
                    numTmp++;
                    fprintf(outVEF, " %d ", k);
                }
            }

            // neighbor triangle  neighbor edges in colliding
            m_vertexCollision[i].nbrNeiSurfTri = 0;
            memset(m_vertexCollision[i].neiSurfTri, -1, 100 * sizeof(int));
            fprintf(outVEF, "\n neiSurf:");
            numTmp = 0;
            for (int k = 0; k < m_surftrisCollision.size(); k++)
            {
                int m0 = m_surftrisCollision[k].nodeIdx[0];
                int m1 = m_surftrisCollision[k].nodeIdx[1];
                int m2 = m_surftrisCollision[k].nodeIdx[2];
                if ((m0 == vertx) || (m1 == vertx) || (m2 == vertx))
                {
                    m_vertexCollision[i].nbrNeiSurfTri++;
                    m_vertexCollision[i].neiSurfTri[numTmp] = k;
                    numTmp++;
                    fprintf(outVEF, " %d ", k);
                }
            }
        }
        fclose(outVEF);

        surfaceInsideMesh = std::make_shared<imstk::SurfaceMesh>();

        return true;
    }

      ////< XZH mapping
    bool VESSTetCutMesh::mappingLinks()
    {
        physXVertTetLink link;
        //m_verticesPostionsXZH 
        auto visualMeshPoses = m_surfaceVisualXZH->getVertexPositions();
        //m_triangleVerticesXZH
        auto visualMeshTris = m_surfaceVisualXZH->getTrianglesVertices();
        for (int i = 0; i < visualMeshPoses.size(); i++)
        {
            int tetNodeIdx = -1;
            auto iPos = visualMeshPoses[i];
            for (int j = 0; j < m_verticesPostionsXZH.size(); j++)
            {
                auto jPos = m_verticesPostionsXZH[j];
                if ((iPos - jPos).norm() < imstk::SMALLLIMIT)
                {
                    tetNodeIdx = j;
                    //printf(" %d ", j);
                    //break;
                }
            }
            link.tetraNodeIndex = tetNodeIdx;
            //printf("surf ID, tetnodeID: %d %d\n", i, tetNodeIdx);
            m_vertinksXZH.push_back(link);
        }

        return true;
    }

    bool VESSTetCutMesh::mappingTriTetLinks()
    {
        physXTriTetLink link;
        auto visualMeshTris = m_surfaceVisualXZH->getTrianglesVertices();
        auto physMeshTets = this->getTetrahedraVertices();
        for (int i = 0; i < visualMeshTris.size(); i++)
        {
            int tetIdx = -1;
            int visIdx = -1;
            auto tris = visualMeshTris[i];
            int i0 = m_vertinksXZH[tris[0]].tetraNodeIndex;   ////< XZH index in the tet
            int i1 = m_vertinksXZH[tris[1]].tetraNodeIndex;
            int i2 = m_vertinksXZH[tris[2]].tetraNodeIndex;
            bool isInside = false;
            for (int j = 0; j < physMeshTets.size(); j++)
            {
                auto tetras = physMeshTets[j];
                bool isFlag0 = false;
                if ((i0 == tetras[0]) || (i0 == tetras[1]) || (i0 == tetras[2]) || (i0 == tetras[3]))
                {
                    isFlag0 = true;
                }

                bool isFlag1 = false;
                if ((i1 == tetras[0]) || (i1 == tetras[1]) || (i1 == tetras[2]) || (i1 == tetras[3]))
                {
                    isFlag1 = true;
                }

                bool isFlag2 = false;
                if ((i2 == tetras[0]) || (i2 == tetras[1]) || (i2 == tetras[2]) || (i2 == tetras[3]))
                {
                    isFlag2 = true;
                }

                if (isFlag0&&isFlag1&&isFlag2)
                {
                    isInside = true;
                    tetIdx = j;
                    //printf(" %d ", j);
                    break;
                }
            }
            link.tetraIndex = tetIdx;
            //printf("tri ID, tetID: %d %d\n", i, tetIdx);

            ////< XZH visual inside or outside
            int isVInside = 1; // m_statesVisualXZH[i]; // 0; this for two layer, if one layer, just set to 1 -> 07/15/2018
            //auto tries = m_surfaceVisualInsideXZH->getTrianglesVertices();
            //for (int j = 0; j < tries.size(); j++)
            //{
            //    auto tri = tries[j];
            //    bool isFlag0 = false;
            //    if ((i0 == tri[0]) || (i0 == tri[1]) || (i0 == tri[2]))
            //    {
            //        isFlag0 = true;
            //    }

            //    bool isFlag1 = false;
            //    if ((i1 == tri[0]) || (i1 == tri[1]) || (i1 == tri[2]))
            //    {
            //        isFlag1 = true;
            //    }

            //    bool isFlag2 = false;
            //    if ((i2 == tri[0]) || (i2 == tri[1]) || (i2 == tri[2]))
            //    {
            //        isFlag2 = true;
            //    }

            //    if (isFlag0&&isFlag1&&isFlag2)
            //    {
            //        isVInside = 1;
            //        break;
            //    }
            //}
            link.inside = isVInside;
            //printf("tri ID, tetID: %d %d %d\n", i, tetIdx, isVInside);

            m_triTetlinksXZH.push_back(link);
        }

        return true;
    }

    bool VESSTetCutMesh::mappingTetTriLinks()
    {
        physXTetTriLink link;
        auto visualMeshTris = m_surfaceVisualXZH->getTrianglesVertices();
        auto physMeshTets = this->getTetrahedraVertices();
        for (int i = 0; i < physMeshTets.size(); i++)
        {
            auto tetras = physMeshTets[i];
            int triIdx = -1;
            int isVInside = -1;
            int num = 0;   ////< XZH count for 1 2 3 4
            int tri0 = -1, tri1 = -1, tri2 = -1, tri3 = -1;
            int in0 = -1, in1 = -1, in2 = -1, in3 = -1;

            for (int j = 0; j < m_triTetlinksXZH.size(); j++)
            {
                triIdx = m_triTetlinksXZH[j].tetraIndex;
                isVInside = m_triTetlinksXZH[j].inside;
                if (i == triIdx)
                {
                    switch (num)
                    {
                    case 0:
                    default:
                        tri0 = j;
                        in0 = isVInside;
                        break;
                    case 1:
                        tri1 = j;
                        in1 = isVInside;
                        break;
                    case 2:
                        tri2 = j;
                        in2 = isVInside;
                        break;
                    case 3:
                        tri3 = j;
                        in3 = isVInside;
                        break;
                    }
                    num++;
                }
            }   ////< XZH END for traverse tris

            ////< XZH 
            link.tri0 = tri0; link.in0 = in0;
            link.tri1 = tri1; link.in1 = in1;
            link.tri2 = tri2; link.in2 = in2;
            link.tri3 = tri3; link.in3 = in3;
            m_tetTrilinksXZH.push_back(link);
            //printf("TET %d: %d %d %d %d %d %d %d %d\n", i, tri0, in0, tri1, in1, tri2, in2, tri3, in3);
        }

        return true;
    }

      ////< XZH cutting mesh init
    bool VESSTetCutMesh::initializeCuttingMesh()
    {
        EDGE edge;
        FACE face;
        CELL cell;
        U32 from, to = BaseLink::INVALID;
        //add all vertices first  from VESSTetrahedralMesh
        int num = 0;
        auto& vertices = this->getInitialVertexPositions();
        num = vertices.size();
        m_vecEdgesPerNode.clear();
        m_vecEdgesPerNode.resize(num);
        m_vecTetsPerNode.clear();
        m_vecTetsPerNode.resize(num);

        for (U32 i = 0; i < num; i++)
        {
            NODE node;
            node.pos = node.restpos = vertices.at(i); // Vec3d(&vertices[i * 3]);
            m_cNodes.push_back(node);
        }

        num = m_edgesVerticesAllXZH.size();
        for (U32 i = 0; i < num;i++)
        {
            auto& eg = m_edgesVerticesAllXZH[i];
            edge.from = eg[0];
            edge.to = eg[1];
            m_cEdges.push_back(edge);
            m_egStates.push_back(1);
              ////< XZH hash map 
            from = eg[0];
            to = eg[1];
            m_vecEdgesPerNode[from].push_back(i);
            m_vecEdgesPerNode[to].push_back(i);
            if (from>to)
            {
                std::swap(from, to);
            }
            //EdgeKey key(from, to);
            U64 egkey = HASHEDGEID_FROM_IDX(from, to);
            m_mapEdges.insert(std::make_pair(egkey, i));
        }
          ////< XZH sort and remove duplicate for the edges of each node
        for (int i = 0; i < m_vecEdgesPerNode.size();i++)
        {
            auto& vecEdges = m_vecEdgesPerNode[i];
            sort(vecEdges.begin(), vecEdges.end());
            vecEdges.erase(unique(vecEdges.begin(), vecEdges.end()), vecEdges.end());
        }

        m_cellsPerEdge.resize(num);
        m_facesPerEdge.resize(num);
        m_facesInPerEdge.resize(num);
        //ITUN it = m_mapEdgeTriIndex.find(edgekey2);
        //if (it == m_mapEdgeTriIndex.end())
        //{          
        //}
        //else
        //{
        //    newVert2 = it->second; // in the 3rd edge
        //}

        num = m_triangleVerticesAllXZH.size();
        for (U32 i = 0; i < num;i++)
        {
            auto& nodes = m_triangleVerticesAllXZH[i];
            //Add all edges
            for (int e = 0; e < 3; e++)
            {
                from = nodes[e];
                to = nodes[(e + 1) % 3];
                if (from > to)
                {
                    std::swap(from, to);
                }
                //EdgeKey key(from, to); 
                U64 egkey = HASHEDGEID_FROM_IDX(from, to);
                ITUN it = m_mapEdges.find(egkey);
                if (it != m_mapEdges.end())
                {
                    face.edges[e] = it->second;

                    ////< XZH check face
                    auto& vecXZH = m_facesPerEdge[it->second];
                    auto vecIt = std::find(vecXZH.begin(), vecXZH.end(), i); //����3();
                    if (vecIt == vecXZH.end())
                    {
                        m_facesPerEdge[it->second].push_back(i);   ////< XZH face idx
                    }

                    
                }
                else
                {
                    printf("Setting face edges failed! Unable to find edge <%d, %d>",        from, to);
                    return BaseLink::INVALID;
                }
            }   ////< XZH END edge
            m_cFaces.push_back(face);
        }

          ////< XZH boundary face
        num = m_triangleVerticesBoundXZH.size();
        for (U32 i = 0; i < num; i++)
        {
            auto& nodes = m_triangleVerticesBoundXZH[i];
            int& triIdx = m_surfaceVisualTriIndexInTet[i];
            //Add all edges
            for (int e = 0; e < 3; e++)
            {
                from = nodes[e];
                to = nodes[(e + 1) % 3];
                if (from > to)
                {
                    std::swap(from, to);
                }
                //EdgeKey key(from, to); 
                U64 egkey = HASHEDGEID_FROM_IDX(from, to);
                ITUN it = m_mapEdges.find(egkey);
                if (it != m_mapEdges.end())
                {
                    face.edges[e] = it->second;

                    ////< XZH check face inside
                    auto& vecXZHIn = m_facesInPerEdge[it->second];
                    auto vecItIn = std::find(vecXZHIn.begin(), vecXZHIn.end(), i); //����3();
                    if (vecItIn == vecXZHIn.end())
                    {
                        m_facesInPerEdge[it->second].push_back(triIdx); //  (i);   ////< XZH face idx
                    }
                }
                else
                {
                    printf("Setting inside face edges failed! Unable to find edge <%d, %d>", from, to);
                    return BaseLink::INVALID;
                }
            }   ////< XZH END edge
            m_cFacesIn.push_back(face);
        }

        //face mask
        const int maskTetFaceNodes[4][3] = { { 1, 2, 3 }, { 2, 0, 3 }, { 3, 0, 1 }, { 1, 0, 2 } };
        const int maskTetFaceEdges[4][3] = { { 0, 1, 2 }, { 3, 4, 1 }, { 4, 5, 2 }, { 5, 3, 0 } };
        //const int faceMaskNeg[4][3] = { {3, 2, 1}, {3, 0, 2}, {1, 0, 3}, {2, 0, 1} };

        //edge mask
        const int maskTetEdges[6][2] = { { 1, 2 }, { 2, 3 }, { 3, 1 }, { 2, 0 }, { 0, 3 }, { 0, 1 } };

        //insert element
        auto& elements = this->getTetrahedraVertices();
        for (U32 i = 0; i < elements.size(); i++)
        {
            U32 nodes[4];
            const TetraArray& tetVertices = elements.at(i);
            for (int k = 0; k < 4; k++)
            {
                nodes[k] = tetVertices[k];
                m_vecTetsPerNode[nodes[k]].push_back(i);
            }
            //insert_cell(nodes);  // insert_cell(const_cast<U32 *>(&elements[i * 4]));
            for (int k = 0; k < COUNT_CELL_NODES; k++)
                cell.nodes[k] = nodes[k];

            for (int k = 0; k < COUNT_CELL_FACES; k++)
                cell.faces[k] = INVALID_INDEX;

            for (int k = 0; k < COUNT_CELL_EDGES; k++)
                cell.edges[k] = INVALID_INDEX;

              ////< XZH face Loop over faces. Per each tet 6 edges or 12 half-edges added
            for (int f = 0; f < COUNT_CELL_FACES; f++)
            {
                U32 fv[3];

                fv[0] = nodes[maskTetFaceNodes[f][0]];
                fv[1] = nodes[maskTetFaceNodes[f][1]];
                fv[2] = nodes[maskTetFaceNodes[f][2]];

                bool isTri = false;
                for (int j = 0; j < m_triangleVerticesAllXZH.size();j++)
                {
                    auto tris = m_triangleVerticesAllXZH[j];
                    bool isFlag0 = false;
                    if ((fv[0] == tris[0]) || (fv[0] == tris[1]) || (fv[0] == tris[2]))
                    {
                        isFlag0 = true;
                    }

                    bool isFlag1 = false;
                    if ((fv[1] == tris[0]) || (fv[1] == tris[1]) || (fv[1] == tris[2]))
                    {
                        isFlag1 = true;
                    }

                    bool isFlag2 = false;
                    if ((fv[2] == tris[0]) || (fv[2] == tris[1]) || (fv[2] == tris[2]))
                    {
                        isFlag2 = true;
                    }

                    if (isFlag0&&isFlag1&&isFlag2)
                    {
                        isTri = true;
                        cell.faces[f] = j;
                        break;
                    }
                }   ////< XZH tri END
            }
              ////< XZH face END
              ////< XZH edge Set element edges
            for (int e = 0; e < 6; e++)
            {
                from = cell.nodes[maskTetEdges[e][0]];
                to = cell.nodes[maskTetEdges[e][1]];

                if (from > to)
                {
                    std::swap(from, to);
                }
                //EdgeKey key(from, to);
                U64 egkey = HASHEDGEID_FROM_IDX(from, to);
                MAPHEDGEINDEXITER it = m_mapEdges.find(egkey);
                if (it != m_mapEdges.end())
                {
                    cell.edges[e] = it->second;

                      ////< XZH check
                    auto& vecXZH = m_cellsPerEdge[it->second];
                    auto vecIt = std::find(vecXZH.begin(), vecXZH.end(), i); //����3();
                    if (vecIt == vecXZH.end())
                    {
                        m_cellsPerEdge[it->second].push_back(i);   ////< XZH cell idx
                    }                    
                }
                else
                {
                    printf("Setting element edges failed! Unable to find edge <%d, %d>",   from, to);
                    return false;
                }
            }
              ////< XZH edge END
            m_cCells.push_back(cell);
        }   ////< XZH ele
        ////< XZH sort and remove duplicate for the Tets of each node
        for (int i = 0; i < m_vecTetsPerNode.size(); i++)
        {
            auto& vecTets = m_vecTetsPerNode[i];
            sort(vecTets.begin(), vecTets.end());
            vecTets.erase(unique(vecTets.begin(), vecTets.end()), vecTets.end());
        }

        return true;
    }

    void VESSTetCutMesh::initializeCuttingPara()
    {

    }

    // cuting new self generated tet mesh , the visual surface mesh is from reading file
    //int VESSTetCutMesh::cutNewXZH(const vector<Vec3d>& segments, const vector<Vec3d>& quadstrips, bool modifyMesh)
    //{
    //    if (segments.size() < 2)  // size is 2
    //        return CUT_ERR_INVALID_INPUT_ARG;
    //    if (quadstrips.size() < 4 || (quadstrips.size() % 2 != 0))
    //        return CUT_ERR_INVALID_INPUT_ARG;

    //    auto& states = this->getTetrahedronStates();
    //    auto& maps = this->getTetrahedronMap();

    //    m_mapCutEdges.clear();

    //    std::map< U32, CutEdge > mapTempCutEdges;

    //    U32 ctSegments = segments.size() - 1;
    //    U32 ctQuads = (quadstrips.size() - 2) / 2;
    //    assert(ctSegments == ctQuads);

    //    U32 ctRemovedCutEdges = 0;

    //    //scalpel segments
    //    vector<int> vPerSegmentCuts;
    //    vPerSegmentCuts.resize(ctSegments);
    //    for (U32 i = 0; i < ctSegments; i++)
    //    {

    //        Vec3d s0 = segments[i];
    //        Vec3d s1 = segments[i + 1];
    //        vPerSegmentCuts[i] = computeCutEdges(&quadstrips[i * 2], mapTempCutEdges);
    //    }

    //    //nothing has been cut!?
    //    if (mapTempCutEdges.size() == 0)
    //        return 0;

    //    //Copy
    //    m_mapCutEdges.insert(mapTempCutEdges.begin(), mapTempCutEdges.end());
    //    if (m_mapCutEdges.size() > 0)
    //        printf("Cut edges count %u. removed %u\n", (U32)m_mapCutEdges.size(), (U32)ctRemovedCutEdges);

    //    //Find the list of all tets impacted
    //    vector<U32> vCutElements;
    //    vector<U8> vCutEdgeCodes;
    //    vector<U8> vCutNodeCodes;
    //    vCutElements.reserve(128);  //��vectorԤ����洢����С
    //    vCutEdgeCodes.reserve(128);

    //      ////< XZH get all the cut faces
    //    std::vector<U32> vCutFaces;
    //    std::vector<U32> vCutFacesIn;
    //    vCutFaces.reserve(128);
    //    vCutFacesIn.reserve(128);

    //    for (auto it = m_mapCutEdges.begin(); it != m_mapCutEdges.end(); it++)
    //    {
    //        auto egIdx = it->first;
    //        auto& vecEdges = m_cellsPerEdge[egIdx];
    //        for (int k = 0;k< vecEdges.size();k++)
    //        {
    //            vCutElements.push_back(vecEdges[k]);
    //        }

    //          ////< XZH faces
    //        auto& vecFaceEdges = m_facesPerEdge[egIdx];
    //        for (int k = 0; k < vecFaceEdges.size(); k++)
    //        {
    //            vCutFaces.push_back(vecFaceEdges[k]);
    //        }

    //        ////< XZH faces Inside
    //        auto& vecFaceInEdges = m_facesInPerEdge[egIdx];
    //        for (int k = 0; k < vecFaceInEdges.size(); k++)
    //        {
    //            vCutFacesIn.push_back(vecFaceInEdges[k]);
    //        }
    //    }


    //    //for (U32 i = 0; i < m_cCells.size(); i++)
    //    //{
    //    //    if (states[i] < 1) continue; // removed

    //    //    const CELL& cell = m_cCells[i];
    //    //    U8 cutEdgeCode = 0;

    //    //    for (int e = 0; e < COUNT_CELL_EDGES; e++)
    //    //    {
    //    //        U32 edge = cell.edges[e];
    //    //        if (m_mapCutEdges.find(edge) != m_mapCutEdges.end())
    //    //        {
    //    //            cutEdgeCode |= (1 << e);  // ��1����eλ ������ߵȺ�ȡ��
    //    //        }
    //    //    }

    //    //    //if there is a cut in this cell
    //    //    if (cutEdgeCode != 0)
    //    //    {
    //    //        //push back all computed values
    //    //        vCutElements.push_back(i);
    //    //        states[i] = 0;
    //    //        vCutEdgeCodes.push_back(cutEdgeCode);

    //    //    }
    //    //}
    //    //  accumulation
    //    vecCutElements = vCutElements;
    //    vecCutElementsTotal.insert(vecCutElementsTotal.end(), vCutElements.begin(), vCutElements.end());
    //    sort(vecCutElementsTotal.begin(), vecCutElementsTotal.end());
    //    vecCutElementsTotal.erase(unique(vecCutElementsTotal.begin(), vecCutElementsTotal.end()), vecCutElementsTotal.end());

    //    //	int edgeMaskPos[6][2] = { {1, 2}, {2, 3}, {3, 1}, {2, 0}, {0, 3}, {0, 1} };
    //    //	int edgeMaskNeg[6][2] = { {3, 2}, {2, 1}, {1, 3}, {3, 0}, {0, 2}, {1, 0} };
    //    //	int faceMaskPos[4][3] = { {1, 2, 3}, {2, 0, 3}, {3, 0, 1}, {1, 0, 2} };
    //    //	int faceMaskNeg[4][3] = { {3, 2, 1}, {3, 0, 2}, {1, 0, 3}, {2, 0, 1} };

    //    //Return if we won't modify the mesh this time
    //    if (!modifyMesh)
    //        return CUT_ERR_USER_CANCELLED_CUT;

    //    //Now that cutedgecodes and cutnodecodes are computed then subdivide the element
    //    //printf("BEGIN CUTTING# %u", m_ctCompletedCuts + 1);

    //    U32 ctSubdividedTets = 0;
    //    U32 middlePoints[12];
    //    U32 faceintPoints[4]; // for partial cutting
    //    bool faceintFlag[4]; // record flag for partial cutting
    //    //face mask
    //    const int maskTetFaceNodes[4][3] = { { 1, 2, 3 }, { 2, 0, 3 }, { 3, 0, 1 }, { 1, 0, 2 } };

    //    for (U32 i = 0; i < vCutElements.size(); i++)
    //    {
    //        //remove the cell from list
    //        //m_vCells.erase(m_vCells.begin() + vCutElements[i]);

    //    }

    //    // update mesh        //// Tetrahedral mesh
    //    imstk::StdVectorOfVec3d vertListTet = this->getVertexPositions(); // this->getInitialVertexPositions();
    //    std::vector<imstk::TetrahedralMesh::TetraArray> tetConnectivity;
    //    //auto& states = this->getTetrahedronStates();
    //    for (U32 i = 0; i < vCutElements.size(); i++)
    //    {
    //        states[vCutElements[i]] = 0;
    //    }
    //    vecCutVisualTrisTotal.clear();
    //    vecCutNewTrisTotal.clear();
    //    int numCutVis = 0;
    //    //  ////< XZH Method 1: get cutted face from tetrahedrons
    //    //for (int i = 0; i < vecCutElementsTotal.size(); i++)
    //    //{
    //    //    int tetIdx = vecCutElementsTotal[i];
    //    //    printf("cut tet: %d \n", tetIdx);
    //    //    int tri0 = this->m_tetTrilinksXZH[tetIdx].tri0;
    //    //    int tri1 = this->m_tetTrilinksXZH[tetIdx].tri1;
    //    //    int tri2 = this->m_tetTrilinksXZH[tetIdx].tri2;
    //    //    int tri3 = this->m_tetTrilinksXZH[tetIdx].tri3;
    //    //    int in0 = this->m_tetTrilinksXZH[tetIdx].in0;
    //    //    int in1 = this->m_tetTrilinksXZH[tetIdx].in1;
    //    //    int in2 = this->m_tetTrilinksXZH[tetIdx].in2;
    //    //    int in3 = this->m_tetTrilinksXZH[tetIdx].in3;
    //    //    if (tri0 >= 0 && in0 > 0)
    //    //    { 
    //    //        //vecCutVisualTrisTotal[numCutVis] = tri0;
    //    //        //numCutVis++;
    //    //        vecCutVisualTrisTotal.push_back(tri0);
    //    //    }
    //    //    if (tri1 >= 0 && in1 > 0)
    //    //    {
    //    //        //vecCutVisualTrisTotal[numCutVis] = tri1;
    //    //        //numCutVis++;
    //    //        vecCutVisualTrisTotal.push_back(tri1);
    //    //    }
    //    //    if (tri2 >= 0 && in2 > 0)
    //    //    {
    //    //        //vecCutVisualTrisTotal[numCutVis] = tri2;
    //    //        //numCutVis++;
    //    //        vecCutVisualTrisTotal.push_back(tri2);
    //    //    }
    //    //    if (tri3 >= 0 && in3 > 0)
    //    //    {
    //    //        //vecCutVisualTrisTotal[numCutVis] = tri3;
    //    //        //numCutVis++;
    //    //        vecCutVisualTrisTotal.push_back(tri3);
    //    //    }
    //    //}
    //    //  ////< XZH END Method1

    //      ////< XZH Method2: with Tetrahedrons and cutted faces to check which should be displayed
    //    std::vector<U32> vecCutElementFacesAll;
    //    for (int i = 0; i < vecCutElementsTotal.size(); i++)
    //    {
    //        int tetIdx = vecCutElementsTotal[i];
    //        auto facelist = m_cCells[tetIdx].faces;

    //        printf("cut tet: %d \n", tetIdx);
    //        int tri0 = facelist[0]; // this->m_tetTrilinksXZH[tetIdx].tri0;
    //        int tri1 = facelist[1]; //  this->m_tetTrilinksXZH[tetIdx].tri1;
    //        int tri2 = facelist[2]; //  this->m_tetTrilinksXZH[tetIdx].tri2;
    //        int tri3 = facelist[3]; //  this->m_tetTrilinksXZH[tetIdx].tri3;
    //        int in0 = m_triangleIsBoundaryAllXZH[tri0]; //  this->m_tetTrilinksXZH[tetIdx].in0;
    //        int in1 = m_triangleIsBoundaryAllXZH[tri1]; //   this->m_tetTrilinksXZH[tetIdx].in1;
    //        int in2 = m_triangleIsBoundaryAllXZH[tri2]; //   this->m_tetTrilinksXZH[tetIdx].in2;
    //        int in3 = m_triangleIsBoundaryAllXZH[tri3]; //   this->m_tetTrilinksXZH[tetIdx].in3;
    //        if ( in0 < 1)   ////< XZH boundary
    //        {
    //            vecCutElementFacesAll.push_back(tri0);
    //        }
    //        if (  in1 < 1)
    //        {
    //            vecCutElementFacesAll.push_back(tri1);
    //        }
    //        if (  in2 < 1)
    //        {
    //            vecCutElementFacesAll.push_back(tri2);
    //        }
    //        if (  in3 < 1)
    //        {
    //            vecCutElementFacesAll.push_back(tri3);
    //        }    
    //    }

    //      ////< XZH traverse all faces from element but only inside without boundary triangles
    //    for (auto it = vecCutElementFacesAll.begin(); it != vecCutElementFacesAll.end();)
    //    {
    //        int faidx = *it;
    //        ////< XZH a face was cut by tool
    //        bool flag = false;
    //        for (int j = 0; j < vCutFaces.size(); j++)
    //        {
    //            int faidx2 = vCutFaces[j];
    //            bool isBound = m_triangleIsBoundaryAllXZH[faidx2];   ////< XZH whether it is boundary triangle
    //            if (isBound) continue;

    //            if (faidx == faidx2)
    //            {
    //                it=vecCutElementFacesAll.erase(it);
    //                flag = true;
    //                break;
    //            }
    //        }
    //        if (!flag) ++it;
    //    }
    //    vecCutNewTrisTotal = vecCutElementFacesAll;
    //      ////< XZH END Method2

    //    vecCutVisualTrisTotal = vCutFacesIn;
    //    sort(vecCutVisualTrisTotal.begin(), vecCutVisualTrisTotal.end());
    //    vecCutVisualTrisTotal.erase(unique(vecCutVisualTrisTotal.begin(), vecCutVisualTrisTotal.end()), vecCutVisualTrisTotal.end());

    //    FILE *outNodes = fopen("i:/outNodesNew.dat", "w");
    //    for (int k = 0; k < m_cCells.size(); k++)
    //    {
    //        //bool isCut = false;
    //        //for (U32 i = 0; i < vCutElements.size(); i++)
    //        //{
    //        //    if (vCutElements[i]== k) // this element should be removed
    //        //    {
    //        //        //isCut = true;
    //        //        states[k] = 0;
    //        //        break;
    //        //    }
    //        //}
    //        if (states[k] == 1) //  (!isCut)
    //        {
    //            TetrahedralMesh::TetraArray tet1 = { m_cCells[k].nodes[0], m_cCells[k].nodes[1], m_cCells[k].nodes[2], m_cCells[k].nodes[3] };
    //            tetConnectivity.push_back(tet1);
    //            fprintf(outNodes, "%d %d %d %d\n", m_cCells[k].nodes[0], m_cCells[k].nodes[1], m_cCells[k].nodes[2], m_cCells[k].nodes[3]);
    //        }
    //    }
    //    //fprintf(outNodes, "Nodes:\n");
    //    //for (int k = 0; k < m_vNodes.size(); k++)
    //    //{
    //    //    vertListTet.push_back(m_vNodes[k].pos);
    //    //    fprintf(outNodes, "%f %f %f\n", m_vNodes[k].pos.x(), m_vNodes[k].pos.y(), m_vNodes[k].pos.z());
    //    //}
    //    //fclose(outNodes);
    //    this->setInitialVertexPositions(vertListTet);
    //    this->setVertexPositions(vertListTet);
    //    this->setTetrahedraVertices(tetConnectivity);
    //    //this->setTopologyChangedFlag(true);
    //    //this->initialize(vertListTet, tetConnectivity, true);

    //    //Return number of tets cut
    //    return ctSubdividedTets;
    //}

////< XZH Type3 based on Tets
    int VESSTetCutMesh::cutNewXZH3(const vector<Vec3d>& segments, const vector<Vec3d>& quadstrips, bool modifyMesh)
    {
        ////< XZH Step 1: find the cut edges
        auto& states = this->getTetrahedronStates();
        auto& maps = this->getTetrahedronMap();

        m_mapCutEdges.clear();
        std::map< U32, CutEdge > mapTempCutEdges;

        U32 ctSegments = segments.size() - 1;
        U32 ctQuads = (quadstrips.size() - 2) / 2;
        U32 ctRemovedCutEdges = 0;

        //scalpel segments
        vector<int> vPerSegmentCuts;
        vPerSegmentCuts.resize(ctSegments);
        for (U32 i = 0; i < ctSegments; i++)
        {
            Vec3d s0 = segments[i];
            Vec3d s1 = segments[i + 1];
            vPerSegmentCuts[i] = computeCutEdges(&quadstrips[i * 2], mapTempCutEdges);
        }

        //nothing has been cut!?
        if (mapTempCutEdges.size() == 0)
            return 0;

        //Copy
        m_mapCutEdges.insert(mapTempCutEdges.begin(), mapTempCutEdges.end());
        //if (m_mapCutEdges.size() > 0)
        //    printf("Cut edges count %u. removed %u\n", (U32)m_mapCutEdges.size(), (U32)ctRemovedCutEdges);

        // Step 2: Find the list of all tets impacted
        vector<U32> vCutElements;
        vector<U8> vCutEdgeCodes;
        vCutElements.reserve(128);
        vCutEdgeCodes.reserve(128);

        ////< XZH get all the cut faces
        std::vector<U32> vCutFaces;
        std::vector<U32> vCutFacesIn;
        vCutFaces.reserve(128);
        vCutFacesIn.reserve(128);

        vecCutEdges.clear();
        for (auto it = m_mapCutEdges.begin(); it != m_mapCutEdges.end(); it++)
        {
            auto egIdx = it->first;
            m_egStates[egIdx] = 0;
            vecCutEdges.push_back(egIdx);
            auto& vecEdges = m_cellsPerEdge[egIdx];
            for (int k = 0; k < vecEdges.size(); k++)
            {
                vCutElements.push_back(vecEdges[k]);
            }

            ////< XZH faces
            auto& vecFaceEdges = m_facesPerEdge[egIdx];
            for (int k = 0; k < vecFaceEdges.size(); k++)
            {
                vCutFaces.push_back(vecFaceEdges[k]);
            }

            ////< XZH faces Boundary
            auto& vecFaceInEdges = m_facesInPerEdge[egIdx];
            for (int k = 0; k < vecFaceInEdges.size(); k++)
            {
                vCutFacesIn.push_back(vecFaceInEdges[k]);
            }
        }

		if (vCutElements.size() < 1) return 0;
		int nCanCut = 0;
		for (int i=0;i<vCutElements.size();i++)
		{
			int tetIdx = vCutElements[i];
			if (m_tetrahedraDissectFlag[tetIdx] < 1) continue;
			nCanCut++;
		}
		if (nCanCut < 1) return 0;

        //  Tets: accumulation 
        vecCutElements = vCutElements;
        vecCutElementsTotal.insert(vecCutElementsTotal.end(), vCutElements.begin(), vCutElements.end());
        sort(vecCutElementsTotal.begin(), vecCutElementsTotal.end());
        vecCutElementsTotal.erase(unique(vecCutElementsTotal.begin(), vecCutElementsTotal.end()), vecCutElementsTotal.end());

        //vecCutFaceTotal.insert(vecCutFaceTotal.end(), vCutFaces.begin(), vCutFaces.end());
        //sort(vecCutFaceTotal.begin(), vecCutFaceTotal.end());
        //vecCutFaceTotal.erase(unique(vecCutFaceTotal.begin(), vecCutFaceTotal.end()), vecCutFaceTotal.end());

        //vecCutFaceInTotal.insert(vecCutFaceInTotal.end(), vCutFacesIn.begin(), vCutFacesIn.end());
        //sort(vecCutFaceInTotal.begin(), vecCutFaceInTotal.end());
        //vecCutFaceInTotal.erase(unique(vecCutFaceInTotal.begin(), vecCutFaceInTotal.end()), vecCutFaceInTotal.end());
        ////< XZH Tets END

        ////< XZH visual mesh based on the original visual mesh
        imstk::StdVectorOfVec3d vertListTet = this->getVertexPositions(); // this->getInitialVertexPositions();
        imstk::StdVectorOfVec3d& vertListTetXZH = this->getVertexPositionsChangeable(); // this->getInitialVertexPositions();
        std::vector<imstk::TetrahedralMesh::TetraArray> tetConnectivity;
        tetConnectivityIndex.clear();
        vecCutNewTrisTotal.clear();
        int numCutVis = 0;
        vCutFacesIn.clear();   ////< XZH the visual triangles in this method is based on cutted Tet
        std::vector<U32> vecCutElementFacesAll;
        //for (int i = 0; i < vecCutElementsTotal.size(); i++) // the total for all the cuts
		int numRealCuts = 0;
        for (int i = 0; i < vecCutElementsTotal.size(); i++)   ////< XZH only for the current cutting
        {
            int tetIdx = vecCutElementsTotal[i];
            auto facelist = m_cCells[tetIdx].faces;

            //if (states[tetIdx] < 1) continue;   ////< XZH dissected
			if (m_tetrahedraDissectFlag[tetIdx]<1) continue;
			numRealCuts++;
            //printf("cut tet: %d \n", tetIdx);
            int tri0 = facelist[0]; // this->m_tetTrilinksXZH[tetIdx].tri0;
            int tri1 = facelist[1]; //  this->m_tetTrilinksXZH[tetIdx].tri1;
            int tri2 = facelist[2]; //  this->m_tetTrilinksXZH[tetIdx].tri2;
            int tri3 = facelist[3]; //  this->m_tetTrilinksXZH[tetIdx].tri3;
            int in0 = m_triangleIsBoundaryAllXZH[tri0]; //  this->m_tetTrilinksXZH[tetIdx].in0;
            int in1 = m_triangleIsBoundaryAllXZH[tri1]; //   this->m_tetTrilinksXZH[tetIdx].in1;
            int in2 = m_triangleIsBoundaryAllXZH[tri2]; //   this->m_tetTrilinksXZH[tetIdx].in2;
            int in3 = m_triangleIsBoundaryAllXZH[tri3]; //   this->m_tetTrilinksXZH[tetIdx].in3;
            if (in0 < 1)   ////< XZH boundary
            {
                vecCutElementFacesAll.push_back(tri0);
            }
            else
            {
                vCutFacesIn.push_back(tri0);
            }

            if (in1 < 1)
            {
                vecCutElementFacesAll.push_back(tri1);
            }
            else
            {
                vCutFacesIn.push_back(tri1);
            }

            if (in2 < 1)
            {
                vecCutElementFacesAll.push_back(tri2);
            }
            else
            {
                vCutFacesIn.push_back(tri2);
            }

            if (in3 < 1)
            {
                vecCutElementFacesAll.push_back(tri3);
            }
            else
            {
                vCutFacesIn.push_back(tri3);
            }
        }
		if (numRealCuts<1)
		{ 
			return 0;
		}
        //vecCutVisualTrisTotal = vCutFacesIn;
        vecCutVisualTrisTotal.insert(vecCutVisualTrisTotal.end(), vCutFacesIn.begin(), vCutFacesIn.end());
        sort(vecCutVisualTrisTotal.begin(), vecCutVisualTrisTotal.end());
        vecCutVisualTrisTotal.erase(unique(vecCutVisualTrisTotal.begin(), vecCutVisualTrisTotal.end()), vecCutVisualTrisTotal.end());

        //  ////< XZH tuning position at 20190226
        //Vec3d cenp = (quadstrips[0] + quadstrips[1] + quadstrips[2] + quadstrips[3]) / 4;
        //Vec3d normquad = (quadstrips[2] - quadstrips[0]).cross(quadstrips[1] - quadstrips[0]).normalized();
        //for (int t = 0; t < 8;t++)
        //{
        //    for (int i = 0; i < vCutFacesIn.size(); i++)
        //    {
        //        int idxInAllFaces = vCutFacesIn[i];
        //        auto tris = m_triangleVerticesAllXZH[idxInAllFaces];
        //        int n0 = tris[0], n1 = tris[1], n2 = tris[2]; // global idx
        //        //Vec3d pos0 = vertListTet[n0], pos1 = vertListTet[n1], pos2 = vertListTet[n2]; // unchangable
        //        //Vec3d pos0Tmp = Vec3d::Zero(), pos1Tmp = Vec3d::Zero(), pos2Tmp = Vec3d::Zero(); // unchangable
        //        Vec3d& pos0XZH = vertListTetXZH[n0], pos1XZH = vertListTetXZH[n1], pos2XZH = vertListTetXZH[n2]; // changable
        //        Vec3d cenptri = (pos0XZH + pos1XZH + pos2XZH) / 3;
        //        double len0 = (pos0XZH - cenp).dot(normquad);
        //        double len1 = (pos0XZH - cenp).dot(normquad);
        //        double len2 = (pos0XZH - cenp).dot(normquad);
        //        double avglen = (abs(len0) + abs(len1) + abs(len2)) / 3;
        //        //avglen = 0.001;
        //        bool flag0 = ((abs(len0) - avglen)>0);
        //        bool flag1 = ((abs(len1) - avglen)>0);
        //        bool flag2 = ((abs(len2) - avglen) > 0);
        //        bool flagp0 = (len0 > 0); // normal
        //        bool flagp1 = (len1 > 0);
        //        bool flagp2 = (len2 > 0);
        //        pos0XZH = pos0XZH + (-1)*flag0*flagp0*normquad*(abs(len0) - avglen)*0.1;
        //        pos1XZH = pos1XZH + (-1)*flag1*flagp1*normquad*(abs(len1) - avglen)*0.1;
        //        pos2XZH = pos2XZH + (-1)*flag2*flagp2*normquad*(abs(len2) - avglen)*0.1;
        //    }
        //}        
        //  ////< XZH tuning position END


        // Step: new surface mesh
        vecCutNewTrisTotal.clear();
        ////< XZH traverse all faces from element but only inside without boundary triangles
        //for (auto it = vecCutElementFacesAll.begin(); it != vecCutElementFacesAll.end();)
        //{
        //    int faidx = *it;
        //    ////< XZH a face was cut by tool
        //    bool flag = false;
        //    for (int j = 0; j < vecCutFaceTotal.size(); j++)
        //    {
        //        int faidx2 = vecCutFaceTotal[j];
        //        bool isBound = m_triangleIsBoundaryAllXZH[faidx2];   ////< XZH whether it is boundary triangle
        //        if (isBound) continue;

        //        if (faidx == faidx2)
        //        {
        //            it = vecCutElementFacesAll.erase(it);
        //            flag = true;
        //            break;
        //        }
        //    }
        //    if (!flag) ++it;
        //}
        //vecCutNewTrisTotal = vecCutElementFacesAll;
        vecCutNewTrisTotal.insert(vecCutNewTrisTotal.end(), vecCutElementFacesAll.begin(), vecCutElementFacesAll.end());
        sort(vecCutNewTrisTotal.begin(), vecCutNewTrisTotal.end());
        vecCutNewTrisTotal.erase(unique(vecCutNewTrisTotal.begin(), vecCutNewTrisTotal.end()), vecCutNewTrisTotal.end());

        //auto& states = this->getTetrahedronStates();
        for (U32 i = 0; i < vCutElements.size(); i++)
        {
            states[vCutElements[i]] = 0;
        }
        ////< XZH visual and new surface END
        for (int k = 0; k < m_cCells.size(); k++)
        {
            if (states[k] == 1) //  (!isCut)
            {
                TetrahedralMesh::TetraArray tet1 = { m_cCells[k].nodes[0], m_cCells[k].nodes[1], m_cCells[k].nodes[2], m_cCells[k].nodes[3] };
                tetConnectivity.push_back(tet1);
                tetConnectivityIndex.push_back(k);
            }
        }
        this->setInitialVertexPositions(vertListTet);
        this->setVertexPositions(vertListTet);
        this->setTetrahedraVertices(tetConnectivity);

        bool flag = this->extractCutSurfaceMesh();

        return 1;
    }

    ////< XZH Type2
    int VESSTetCutMesh::cutNewXZH2(const vector<Vec3d>& segments, const vector<Vec3d>& quadstrips, bool modifyMesh)
    {
          ////< XZH Step 1: find the cut edges
        auto& states = this->getTetrahedronStates();
        auto& maps = this->getTetrahedronMap();

        m_mapCutEdges.clear();
        std::map< U32, CutEdge > mapTempCutEdges;

        U32 ctSegments = segments.size() - 1;
        U32 ctQuads = (quadstrips.size() - 2) / 2;
        U32 ctRemovedCutEdges = 0;

        //scalpel segments
        vector<int> vPerSegmentCuts;
        vPerSegmentCuts.resize(ctSegments);
        for (U32 i = 0; i < ctSegments; i++)
        {
            Vec3d s0 = segments[i];
            Vec3d s1 = segments[i + 1];
            vPerSegmentCuts[i] = computeCutEdges(&quadstrips[i * 2], mapTempCutEdges);
        }

        //nothing has been cut!?
        if (mapTempCutEdges.size() == 0)
            return 0;

        //Copy
        m_mapCutEdges.insert(mapTempCutEdges.begin(), mapTempCutEdges.end());
        //if (m_mapCutEdges.size() > 0)
        //    printf("Cut edges count %u. removed %u\n", (U32)m_mapCutEdges.size(), (U32)ctRemovedCutEdges);

        // Step 2: Find the list of all tets impacted
        vector<U32> vCutElements;
        vector<U8> vCutEdgeCodes;
        vCutElements.reserve(128);   
        vCutEdgeCodes.reserve(128);

        ////< XZH get all the cut faces
        std::vector<U32> vCutFaces;
        std::vector<U32> vCutFacesIn;
        vCutFaces.reserve(128);
        vCutFacesIn.reserve(128);

        vecCutEdges.clear();
        for (auto it = m_mapCutEdges.begin(); it != m_mapCutEdges.end(); it++)
        {
            auto egIdx = it->first;
            m_egStates[egIdx] = 0;
            vecCutEdges.push_back(egIdx);
            auto& vecEdges = m_cellsPerEdge[egIdx];
            for (int k = 0; k < vecEdges.size(); k++)
            {
                vCutElements.push_back(vecEdges[k]);
            }

            ////< XZH faces
            auto& vecFaceEdges = m_facesPerEdge[egIdx];
            for (int k = 0; k < vecFaceEdges.size(); k++)
            {
                vCutFaces.push_back(vecFaceEdges[k]);
            }

            ////< XZH faces Boundary
            auto& vecFaceInEdges = m_facesInPerEdge[egIdx];
            for (int k = 0; k < vecFaceInEdges.size(); k++)
            {
                vCutFacesIn.push_back(vecFaceInEdges[k]);
            }

        }

        //  Tets: accumulation 
        vecCutElements = vCutElements;
        vecCutElementsTotal.insert(vecCutElementsTotal.end(), vCutElements.begin(), vCutElements.end());
        sort(vecCutElementsTotal.begin(), vecCutElementsTotal.end());
        vecCutElementsTotal.erase(unique(vecCutElementsTotal.begin(), vecCutElementsTotal.end()), vecCutElementsTotal.end());

          ////< XZH Tets END

          ////< XZH visual mesh based on the original visual mesh
        imstk::StdVectorOfVec3d vertListTet = this->getVertexPositions(); // this->getInitialVertexPositions();
        std::vector<imstk::TetrahedralMesh::TetraArray> tetConnectivity;
        vecCutNewTrisTotal.clear();
        int numCutVis = 0;
        vCutFacesIn.clear();   ////< XZH the visual triangles in this method is based on cutted Tet
        std::vector<U32> vecCutElementFacesAll;
        //for (int i = 0; i < vecCutElementsTotal.size(); i++) // the total for all the cuts
        for (int i = 0; i < vecCutElements.size(); i++)   ////< XZH only for the current cutting
        {
            int tetIdx = vecCutElementsTotal[i];
            auto facelist = m_cCells[tetIdx].faces;

            if (states[tetIdx] < 1) continue;   ////< XZH dissected

            //printf("cut tet: %d \n", tetIdx);
            int tri0 = facelist[0]; // this->m_tetTrilinksXZH[tetIdx].tri0;
            int tri1 = facelist[1]; //  this->m_tetTrilinksXZH[tetIdx].tri1;
            int tri2 = facelist[2]; //  this->m_tetTrilinksXZH[tetIdx].tri2;
            int tri3 = facelist[3]; //  this->m_tetTrilinksXZH[tetIdx].tri3;
            int in0 = m_triangleIsBoundaryAllXZH[tri0]; //  this->m_tetTrilinksXZH[tetIdx].in0;
            int in1 = m_triangleIsBoundaryAllXZH[tri1]; //   this->m_tetTrilinksXZH[tetIdx].in1;
            int in2 = m_triangleIsBoundaryAllXZH[tri2]; //   this->m_tetTrilinksXZH[tetIdx].in2;
            int in3 = m_triangleIsBoundaryAllXZH[tri3]; //   this->m_tetTrilinksXZH[tetIdx].in3;
            if (in0 < 1)   ////< XZH boundary
            {
                vecCutElementFacesAll.push_back(tri0);
            }
            else
            {
                vCutFacesIn.push_back(tri0);
            }

            if (in1 < 1)
            {
                vecCutElementFacesAll.push_back(tri1);
            }
            else
            {
                vCutFacesIn.push_back(tri1);
            }

            if (in2 < 1)
            {
                vecCutElementFacesAll.push_back(tri2);
            }
            else
            {
                vCutFacesIn.push_back(tri2);
            }

            if (in3 < 1)
            {
                vecCutElementFacesAll.push_back(tri3);
            }
            else
            {
                vCutFacesIn.push_back(tri3);
            }
        }
        //vecCutVisualTrisTotal = vCutFacesIn;
        vecCutVisualTrisTotal.insert(vecCutVisualTrisTotal.end(), vCutFacesIn.begin(), vCutFacesIn.end());
        sort(vecCutVisualTrisTotal.begin(), vecCutVisualTrisTotal.end());
        vecCutVisualTrisTotal.erase(unique(vecCutVisualTrisTotal.begin(), vecCutVisualTrisTotal.end()), vecCutVisualTrisTotal.end());

        // Step: new surface mesh
        vecCutNewTrisTotal.clear();
        ////< XZH traverse all faces from element but only inside without boundary triangles
        for (auto it = vecCutElementFacesAll.begin(); it != vecCutElementFacesAll.end();)
        {
            int faidx = *it;
            ////< XZH a face was cut by tool
            bool flag = false;
            for (int j = 0; j < vCutFaces.size(); j++)
            {
                int faidx2 = vCutFaces[j];
                bool isBound = m_triangleIsBoundaryAllXZH[faidx2];   ////< XZH whether it is boundary triangle
                if (isBound) continue;

                if (faidx == faidx2)
                {
                    it = vecCutElementFacesAll.erase(it);
                    flag = true;
                    break;
                }
            }
            if (!flag) ++it;
        }
        vecCutNewTrisTotal = vecCutElementFacesAll;

        //auto& states = this->getTetrahedronStates();
        for (U32 i = 0; i < vCutElements.size(); i++)
        {
            states[vCutElements[i]] = 0;
        }
          ////< XZH visual and new surface END
        for (int k = 0; k < m_cCells.size(); k++)
        {
            //bool isCut = false;
            //for (U32 i = 0; i < vCutElements.size(); i++)
            //{
            //    if (vCutElements[i]== k) // this element should be removed
            //    {
            //        //isCut = true;
            //        states[k] = 0;
            //        break;
            //    }
            //}
            if (states[k] == 1) //  (!isCut)
            {
                TetrahedralMesh::TetraArray tet1 = { m_cCells[k].nodes[0], m_cCells[k].nodes[1], m_cCells[k].nodes[2], m_cCells[k].nodes[3] };
                tetConnectivity.push_back(tet1);
                //fprintf(outNodes, "%d %d %d %d\n", m_cCells[k].nodes[0], m_cCells[k].nodes[1], m_cCells[k].nodes[2], m_cCells[k].nodes[3]);
            }
        }
        this->setInitialVertexPositions(vertListTet);
        this->setVertexPositions(vertListTet);
        this->setTetrahedraVertices(tetConnectivity);

        return 0;
    }
    // cuting new self generated tet mesh , the visual surface mesh is from tetrahedral mesh
    int VESSTetCutMesh::cutNewXZH(const vector<Vec3d>& segments, const vector<Vec3d>& quadstrips, bool modifyMesh)
    {
        if (segments.size() < 2)  // size is 2
            return CUT_ERR_INVALID_INPUT_ARG;
        if (quadstrips.size() < 4 || (quadstrips.size() % 2 != 0))
            return CUT_ERR_INVALID_INPUT_ARG;

        auto& states = this->getTetrahedronStates();
        auto& maps = this->getTetrahedronMap();

        m_mapCutEdges.clear();

        std::map< U32, CutEdge > mapTempCutEdges;

        U32 ctSegments = segments.size() - 1;
        U32 ctQuads = (quadstrips.size() - 2) / 2;
        assert(ctSegments == ctQuads);

        U32 ctRemovedCutEdges = 0;

        //scalpel segments
        vector<int> vPerSegmentCuts;
        vPerSegmentCuts.resize(ctSegments);
        for (U32 i = 0; i < ctSegments; i++)
        {

            Vec3d s0 = segments[i];
            Vec3d s1 = segments[i + 1];
            vPerSegmentCuts[i] = computeCutEdges(&quadstrips[i * 2], mapTempCutEdges);
        }

        //nothing has been cut!?
        if (mapTempCutEdges.size() == 0)
            return 0;

        //Copy
        m_mapCutEdges.insert(mapTempCutEdges.begin(), mapTempCutEdges.end());
        //if (m_mapCutEdges.size() > 0)
        //    printf("Cut edges count %u. removed %u\n", (U32)m_mapCutEdges.size(), (U32)ctRemovedCutEdges);

        //Find the list of all tets impacted
        vector<U32> vCutElements;
        vector<U8> vCutEdgeCodes;
        vector<U8> vCutNodeCodes;
        vCutElements.reserve(128);  //��vectorԤ����洢����С
        vCutEdgeCodes.reserve(128);

        ////< XZH get all the cut faces
        std::vector<U32> vCutFaces;
        std::vector<U32> vCutFacesIn;
        vCutFaces.reserve(128);
        vCutFacesIn.reserve(128);

        for (auto it = m_mapCutEdges.begin(); it != m_mapCutEdges.end(); it++)
        {
            auto egIdx = it->first;
            m_egStates[egIdx] = 0;
            auto& vecEdges = m_cellsPerEdge[egIdx];
            for (int k = 0; k < vecEdges.size(); k++)
            {
                vCutElements.push_back(vecEdges[k]);
            }

            ////< XZH faces
            auto& vecFaceEdges = m_facesPerEdge[egIdx];
            for (int k = 0; k < vecFaceEdges.size(); k++)
            {
                vCutFaces.push_back(vecFaceEdges[k]);
            }

            ////< XZH faces Inside
            auto& vecFaceInEdges = m_facesInPerEdge[egIdx];
            for (int k = 0; k < vecFaceInEdges.size(); k++)
            {
                vCutFacesIn.push_back(vecFaceInEdges[k]);
            }

              ////< XZH update EdgesPerNode
            U32 efrom = it->second.idxOrgFrom;
            U32 eto = it->second.idxOrgTo;
            auto& vecEdgesFrom = m_vecEdgesPerNode[efrom];
            auto& vecEdgesTo = m_vecEdgesPerNode[eto];
            std::vector<U32>::iterator itEg;
            for (itEg = vecEdgesFrom.begin(); itEg != vecEdgesFrom.end();)
            {
                auto egx = *itEg;
                if (egx==egIdx)
                    itEg = vecEdgesFrom.erase(itEg);    // delete and indicate next one    
                else
                    ++itEg;    // next
            }
            std::vector<U32>::iterator itEg2;
            for (itEg2 = vecEdgesTo.begin(); itEg2 != vecEdgesTo.end();)
            {
                auto egx = *itEg2;
                if (egx == egIdx)
                    itEg2 = vecEdgesTo.erase(itEg2);    // delete and indicate next one    
                else
                    ++itEg2;    // next
            }
        }

        //  accumulation
        vecCutElements = vCutElements;
        vecCutElementsTotal.insert(vecCutElementsTotal.end(), vCutElements.begin(), vCutElements.end());
        sort(vecCutElementsTotal.begin(), vecCutElementsTotal.end());
        vecCutElementsTotal.erase(unique(vecCutElementsTotal.begin(), vecCutElementsTotal.end()), vecCutElementsTotal.end());

        //	int edgeMaskPos[6][2] = { {1, 2}, {2, 3}, {3, 1}, {2, 0}, {0, 3}, {0, 1} };
        //	int edgeMaskNeg[6][2] = { {3, 2}, {2, 1}, {1, 3}, {3, 0}, {0, 2}, {1, 0} };
        //	int faceMaskPos[4][3] = { {1, 2, 3}, {2, 0, 3}, {3, 0, 1}, {1, 0, 2} };
        //	int faceMaskNeg[4][3] = { {3, 2, 1}, {3, 0, 2}, {1, 0, 3}, {2, 0, 1} };

        //Return if we won't modify the mesh this time
        if (!modifyMesh)
            return CUT_ERR_USER_CANCELLED_CUT;

        //Now that cutedgecodes and cutnodecodes are computed then subdivide the element
        //printf("BEGIN CUTTING# %u", m_ctCompletedCuts + 1);

        U32 ctSubdividedTets = 0;
        U32 middlePoints[12];
        U32 faceintPoints[4]; // for partial cutting
        bool faceintFlag[4]; // record flag for partial cutting
        //face mask
        const int maskTetFaceNodes[4][3] = { { 1, 2, 3 }, { 2, 0, 3 }, { 3, 0, 1 }, { 1, 0, 2 } };

        for (U32 i = 0; i < vCutElements.size(); i++)
        {
            //remove the cell from list
            //m_vCells.erase(m_vCells.begin() + vCutElements[i]);

        }

        // update mesh        //// Tetrahedral mesh
        imstk::StdVectorOfVec3d vertListTet = this->getVertexPositions(); // this->getInitialVertexPositions();
        std::vector<imstk::TetrahedralMesh::TetraArray> tetConnectivity;
        
        //vecCutVisualTrisTotal.clear();
        vecCutNewTrisTotal.clear();
        int numCutVis = 0;
        //  ////< XZH Method 1: get cutted face from tetrahedrons
        //for (int i = 0; i < vecCutElementsTotal.size(); i++)
        //{
        //    int tetIdx = vecCutElementsTotal[i];
        //    printf("cut tet: %d \n", tetIdx);
        //    int tri0 = this->m_tetTrilinksXZH[tetIdx].tri0;
        //    int tri1 = this->m_tetTrilinksXZH[tetIdx].tri1;
        //    int tri2 = this->m_tetTrilinksXZH[tetIdx].tri2;
        //    int tri3 = this->m_tetTrilinksXZH[tetIdx].tri3;
        //    int in0 = this->m_tetTrilinksXZH[tetIdx].in0;
        //    int in1 = this->m_tetTrilinksXZH[tetIdx].in1;
        //    int in2 = this->m_tetTrilinksXZH[tetIdx].in2;
        //    int in3 = this->m_tetTrilinksXZH[tetIdx].in3;
        //    if (tri0 >= 0 && in0 > 0)
        //    { 
        //        //vecCutVisualTrisTotal[numCutVis] = tri0;
        //        //numCutVis++;
        //        vecCutVisualTrisTotal.push_back(tri0);
        //    }
        //    if (tri1 >= 0 && in1 > 0)
        //    {
        //        //vecCutVisualTrisTotal[numCutVis] = tri1;
        //        //numCutVis++;
        //        vecCutVisualTrisTotal.push_back(tri1);
        //    }
        //    if (tri2 >= 0 && in2 > 0)
        //    {
        //        //vecCutVisualTrisTotal[numCutVis] = tri2;
        //        //numCutVis++;
        //        vecCutVisualTrisTotal.push_back(tri2);
        //    }
        //    if (tri3 >= 0 && in3 > 0)
        //    {
        //        //vecCutVisualTrisTotal[numCutVis] = tri3;
        //        //numCutVis++;
        //        vecCutVisualTrisTotal.push_back(tri3);
        //    }
        //}
        //  ////< XZH END Method1

        ////< XZH Method2: with Tetrahedrons and cutted faces to check which should be displayed
        vCutFacesIn.clear();   ////< XZH the visual triangles in this method is based on cutted Tet
        std::vector<U32> vecCutElementFacesAll;
        //for (int i = 0; i < vecCutElementsTotal.size(); i++) // the total for all the cuts
        for (int i = 0; i < vecCutElements.size(); i++)   ////< XZH only for the current cutting
        {
            int tetIdx = vecCutElementsTotal[i];
            auto facelist = m_cCells[tetIdx].faces;

            if (states[tetIdx] < 1) continue;   ////< XZH dissected

            //printf("cut tet: %d \n", tetIdx);
            int tri0 = facelist[0]; // this->m_tetTrilinksXZH[tetIdx].tri0;
            int tri1 = facelist[1]; //  this->m_tetTrilinksXZH[tetIdx].tri1;
            int tri2 = facelist[2]; //  this->m_tetTrilinksXZH[tetIdx].tri2;
            int tri3 = facelist[3]; //  this->m_tetTrilinksXZH[tetIdx].tri3;
            int in0 = m_triangleIsBoundaryAllXZH[tri0]; //  this->m_tetTrilinksXZH[tetIdx].in0;
            int in1 = m_triangleIsBoundaryAllXZH[tri1]; //   this->m_tetTrilinksXZH[tetIdx].in1;
            int in2 = m_triangleIsBoundaryAllXZH[tri2]; //   this->m_tetTrilinksXZH[tetIdx].in2;
            int in3 = m_triangleIsBoundaryAllXZH[tri3]; //   this->m_tetTrilinksXZH[tetIdx].in3;
            if (in0 < 1)   ////< XZH boundary
            {
                vecCutElementFacesAll.push_back(tri0);
            }
            else
            {
                vCutFacesIn.push_back(tri0);
            }

            if (in1 < 1)
            {
                vecCutElementFacesAll.push_back(tri1);
            }
            else
            {
                vCutFacesIn.push_back(tri1);
            }

            if (in2 < 1)
            {
                vecCutElementFacesAll.push_back(tri2);
            }
            else
            {
                vCutFacesIn.push_back(tri2);
            }

            if (in3 < 1)
            {
                vecCutElementFacesAll.push_back(tri3);
            }
            else
            {
                vCutFacesIn.push_back(tri3);
            }

            ////< XZH update EdgesPerNode
            auto ndlist = m_cCells[tetIdx].nodes;
            for (int k = 0; k < 4;k++)
            {
                auto& vecTets = m_vecTetsPerNode[ndlist[k]];
                std::vector<U32>::iterator itEg;
                for (itEg = vecTets.begin(); itEg != vecTets.end();)
                {
                    auto egx = *itEg;
                    if (egx == tetIdx)
                        itEg = vecTets.erase(itEg);    // delete and indicate next one    
                    else
                        ++itEg;    // next
                }
            }            
        }

        ////< XZH traverse all faces from element but only inside without boundary triangles
        for (auto it = vecCutElementFacesAll.begin(); it != vecCutElementFacesAll.end();)
        {
            int faidx = *it;
            ////< XZH a face was cut by tool
            bool flag = false;
            for (int j = 0; j < vCutFaces.size(); j++)
            {
                int faidx2 = vCutFaces[j];
                bool isBound = m_triangleIsBoundaryAllXZH[faidx2];   ////< XZH whether it is boundary triangle
                if (isBound) continue;

                if (faidx == faidx2)
                {
                    it = vecCutElementFacesAll.erase(it);
                    flag = true;
                    break;
                }
            }
            if (!flag) ++it;
        }
        vecCutNewTrisTotal = vecCutElementFacesAll;
        ////< XZH END Method2

        //vecCutVisualTrisTotal = vCutFacesIn;
        vecCutVisualTrisTotal.insert(vecCutVisualTrisTotal.end(), vCutFacesIn.begin(), vCutFacesIn.end());
        sort(vecCutVisualTrisTotal.begin(), vecCutVisualTrisTotal.end());
        vecCutVisualTrisTotal.erase(unique(vecCutVisualTrisTotal.begin(), vecCutVisualTrisTotal.end()), vecCutVisualTrisTotal.end());

        

        //auto& states = this->getTetrahedronStates();
        for (U32 i = 0; i < vCutElements.size(); i++)
        {
            states[vCutElements[i]] = 0;
        }

        FILE *outNodes = fopen("i:/outNodesNew.dat", "w");
        for (int k = 0; k < m_cCells.size(); k++)
        {
            //bool isCut = false;
            //for (U32 i = 0; i < vCutElements.size(); i++)
            //{
            //    if (vCutElements[i]== k) // this element should be removed
            //    {
            //        //isCut = true;
            //        states[k] = 0;
            //        break;
            //    }
            //}
            if (states[k] == 1) //  (!isCut)
            {
                TetrahedralMesh::TetraArray tet1 = { m_cCells[k].nodes[0], m_cCells[k].nodes[1], m_cCells[k].nodes[2], m_cCells[k].nodes[3] };
                tetConnectivity.push_back(tet1);
                fprintf(outNodes, "%d %d %d %d\n", m_cCells[k].nodes[0], m_cCells[k].nodes[1], m_cCells[k].nodes[2], m_cCells[k].nodes[3]);
            }
        }
        //fprintf(outNodes, "Nodes:\n");
        //for (int k = 0; k < m_vNodes.size(); k++)
        //{
        //    vertListTet.push_back(m_vNodes[k].pos);
        //    fprintf(outNodes, "%f %f %f\n", m_vNodes[k].pos.x(), m_vNodes[k].pos.y(), m_vNodes[k].pos.z());
        //}
        //fclose(outNodes);
        this->setInitialVertexPositions(vertListTet);
        this->setVertexPositions(vertListTet);
        this->setTetrahedraVertices(tetConnectivity);
        //this->setTopologyChangedFlag(true);
        //this->initialize(vertListTet, tetConnectivity, true);

        //Return number of tets cut
        return ctSubdividedTets;
    }

    int VESSTetCutMesh::computeCutEdges(const Vec3d sweptquad[4], std::map<U32, CutEdge>& mapCutEdges)
    {
        //if the swept surface is degenerate then return
        double area = (sweptquad[1] - sweptquad[0]).squaredNorm() * (sweptquad[2] - sweptquad[0]).squaredNorm(); //  (sweptquad[1] - sweptquad[0]).length2() * (sweptquad[2] - sweptquad[0]).length2();
        double epsxzh = EPSILON*0.00000001;
        if (area < epsxzh)
            return -1;
        double l2 = (sweptquad[3] - sweptquad[2]).squaredNorm(); // length2
        if (l2 < epsxzh)
            return -1;


        //vars
        Vec3d uvw, xyz, ss0, ss1;
        double t;

        Vec3d tri1[3] = { sweptquad[0], sweptquad[2], sweptquad[1] };
        Vec3d tri2[3] = { sweptquad[2], sweptquad[3], sweptquad[1] };


        //Cut-Edges
        U32 ctEdges = m_cEdges.size();
        int found = 0;
        for (U32 i = 0; i < ctEdges; i++)
        {
            if (m_egStates[i] < 1) continue;   ////< XZH dissected, so no need
            const EDGE& e = m_cEdges[i];  

            ss0 = m_cNodes[e.from].pos;  
            ss1 = m_cNodes[e.to].pos; 

            int res = IntersectSegmentTriangle(ss0, ss1, tri1, t, uvw, xyz); // ��Ӧ���� �㷨6  9: res �� IntersectSegmentTri(edge0, edge1, tri0, p)
            if (res == 0)
                res = IntersectSegmentTriangle(ss0, ss1, tri2, t, uvw, xyz);
            if (res > 0)
            {
                CutEdge ce;
                ce.idxOrgFrom = e.from;
                ce.idxOrgTo = e.to;
                ce.pos = xyz;
                ce.uvw = uvw;
                ce.t = t;

                //test
                Vec3d temp = ss0 + (ss1 - ss0).normalized() * t;
                assert((xyz - temp).norm() < EPSILON); // length   

                //add to cut edges map
                if (mapCutEdges.find(i) == mapCutEdges.end())
                {
                    mapCutEdges.insert(std::make_pair(i, ce));
                    found++;
                    m_egStates[i] = 0;
                }
                else
                {
                    //mapCutEdges.erase(i);    ////< XZH why need to erase?
                    //printf("Edge %d has already been cut!", i);
                }
            }
        }

        return found;
    }

    bool VESSTetCutMesh::extractCutSurfaceMesh()
    {
        vecCutNewTriangles.clear();
        std::vector<bool> m_triangleIsRevertFlagAll; // for reverse
        m_triangleIsRevertFlagAll.resize(m_triangleIsBoundaryAllXZH.size());
        for (int i = 0; i < m_triangleIsRevertFlagAll.size();i++)
        {
            m_triangleIsRevertFlagAll[i] = false;
        }
        using triArray = SurfaceMesh::TriangleArray;
        const std::vector<triArray> facePattern = { triArray{ { 0, 1, 2 } }, triArray{ { 0, 1, 3 } }, triArray{ { 0, 2, 3 } }, triArray{ { 1, 2, 3 } } };
        const std::vector<size_t> facePatternTmp = { 3, 2, 1, 0 }; // xzh
        // Find and store the tetrahedral faces that are unique
        auto& vertArray = vecCutElementsTotal; // this->getTetrahedraVertices();
        std::vector<triArray> surfaceTri;
        std::vector<U32> surfaceTriIdx;
        std::vector<size_t> surfaceTriTet;
        std::vector<size_t> tetRemainingVert;
        bool unique = true;
        size_t foundAt = 0, tetId = 0;
        size_t a, b, c;
        size_t d; // add by xzh

        for (int i = 0; i < vertArray.size();i++)
        {
            int tetIdx = vertArray[i]; // tetConnectivityIndex[i];   ////< XZH idx in the original tet mesh
            //auto& tetVertArray = vertArray[i];
            auto& facelst = m_cCells[tetIdx].faces;
            auto& nodelst = m_cCells[tetIdx].nodes;   ////< XZH tet nodes

              ////< XZH face traverse
            for (int t = 0; t < 4; ++t)
            {
                unique = true;
                foundAt = 0;

                int fcid = facelst[t];
                int bflag = m_triangleIsBoundaryAllXZH[fcid];
                if (bflag>0)   ////< XZH surface boundary
                {
                    continue;
                }

                auto& ndlst = m_triangleVerticesAllXZH[fcid];   ////< XZH face nodes 0 1 2
                int nid0 = nodelst[0];
                int nid1 = nodelst[1];
                int nid2 = nodelst[2];
                int nid3 = nodelst[3];
                bool flag0 = false, flag1 = false, flag2 = false, flag3 = false;
                if ((nid0 != ndlst[0]) && (nid0 != ndlst[1]) && (nid0 != ndlst[2])) flag0 = true;   ////< XZH nid0 is another vert of this tet
                if ((nid1 != ndlst[0]) && (nid1 != ndlst[1]) && (nid1 != ndlst[2])) flag1 = true;
                if ((nid2 != ndlst[0]) && (nid2 != ndlst[1]) && (nid2 != ndlst[2])) flag2 = true;
                if ((nid3 != ndlst[0]) && (nid3 != ndlst[1]) && (nid3 != ndlst[2])) flag3 = true;

                auto vecIt = std::find(surfaceTriIdx.begin(), surfaceTriIdx.end(), fcid);  
                if (vecIt == surfaceTriIdx.end())
                {
                    surfaceTriIdx.push_back(fcid);

                      ////< XZH reverse
                    auto v0 = m_vertexPositions[ndlst[0]];
                    auto v1 = m_vertexPositions[ndlst[1]];
                    auto v2 = m_vertexPositions[ndlst[2]];
                    auto centroid = (v0 + v1 + v2) / 3;
                    auto normal = (v1 - v0).cross(v2 - v0); // ((v0 - v1).cross(v0 - v2));
                    if (flag0)
                    {
                        if (normal.dot(centroid - m_vertexPositions[nid0]) < 0)
                        {
                            if (!m_triangleIsRevertFlagAll[fcid]) m_triangleIsRevertFlagAll[fcid] = false;
                        }
                        else m_triangleIsRevertFlagAll[fcid] = true;
                        continue;
                    }
                    if (flag1)
                    {
                        if (normal.dot(centroid - m_vertexPositions[nid1]) < 0)
                        {
                            if (!m_triangleIsRevertFlagAll[fcid]) m_triangleIsRevertFlagAll[fcid] = false;
                        }
                        else m_triangleIsRevertFlagAll[fcid] = true;
                        continue;
                    }
                    if (flag2)
                    {
                        if (normal.dot(centroid - m_vertexPositions[nid2]) < 0)
                        {
                            if (!m_triangleIsRevertFlagAll[fcid]) m_triangleIsRevertFlagAll[fcid] = false;
                        }
                        else m_triangleIsRevertFlagAll[fcid] = true;
                        continue;
                    }
                    if (flag3)
                    {
                        if (normal.dot(centroid - m_vertexPositions[nid3]) < 0)
                        {
                            if (!m_triangleIsRevertFlagAll[fcid]) m_triangleIsRevertFlagAll[fcid] = false;
                        }
                        else m_triangleIsRevertFlagAll[fcid] = true;
                        continue;
                    }
                }
                else
                {
                    vecIt = surfaceTriIdx.erase(vecIt);
                }
            }
            tetId++;
        }
        for (int i = 0; i < surfaceTriIdx.size();i++)
        {
            int ix = surfaceTriIdx[i];
            bool iflag = m_triangleIsRevertFlagAll[ix];   ////< XZH using ix for index
            auto ttri = m_triangleVerticesAllXZH[ix];
            imstk::SurfaceMesh::TriangleArray triaTmp;
            triaTmp[2] = ttri[2];
            if (iflag)
            {
                triaTmp[0] = ttri[1];
                triaTmp[1] = ttri[0];
            }
            else
            {
                triaTmp[0] = ttri[0];
                triaTmp[1] = ttri[1];
            }
            surfaceTri.push_back(triaTmp); //  m_triangleVerticesAllXZH[ix]);
            //surfaceTri.push_back(m_triangleVerticesAllXZH[ix]);
        }
        vecCutNewTriangles = surfaceTri;
        return true;
    }

    void VESSTetCutMesh::calcTex2TetSurfTriLink()
    {
        triListsTextureOrder.clear();
        FILE *outPos = fopen("i:/checktextet.dat", "w");
        m_texTetSurfTriLinks.clear();
        m_tetTexSurfTriLinks.clear();
        m_texTetVertLinks.clear();
        m_tetTexVertLinks.clear();
        auto& posesXZH = m_surfaceVisualXZH->getVertexPositions();
        auto& posesTex = m_surfaceTexture->getVertexPositions();
        auto& trisXZH = m_surfaceVisualXZH->getTrianglesVerticesChangeable();
        auto& trisTex = m_surfaceTexture->getTrianglesVertices();
        m_texTetSurfTriLinks.resize(trisTex.size());
        m_tetTexSurfTriLinks.resize(trisXZH.size());
        m_texTetVertLinks.resize(posesTex.size());
        m_tetTexVertLinks.resize(posesXZH.size());

        double thres = 0.0001;
        Vec3d min1, max1;
        Vec3d min2, max2;
        m_surfaceTexture->computeBoundingBox(min1, max1);
        m_surfaceVisualXZH->computeBoundingBox(min2, max2);
        for (int i = 0; i < trisTex.size(); i++)
        {
            auto in0 = trisTex[i].at(0);
            auto in1 = trisTex[i].at(1);
            auto in2 = trisTex[i].at(2);
            auto nd0 = posesTex[in0];
            auto nd1 = posesTex[in1];
            auto nd2 = posesTex[in2];
            
            for (int j = 0; j < trisXZH.size();j++)
            {
                bool flag0 = false, flag1 = false, flag2 = false;
                auto inn0 = trisXZH[j][0];
                auto inn1 = trisXZH[j][1];
                auto inn2 = trisXZH[j][2];
                auto nnd0 = posesXZH[inn0];
                auto nnd1 = posesXZH[inn1];
                auto nnd2 = posesXZH[inn2];
                auto& vertlist = trisXZH[j];                

                  ////< XZH nd0
                bool flag00 = false, flag01 = false, flag02 = false;
                if ((nd0 - nnd0).norm() < thres)
                {
                    flag00 = true;
                    m_texTetVertLinks[in0] = inn0;
                    m_tetTexVertLinks[inn0] = in0;
                }
                if ((nd0 - nnd1).norm() < thres)
                {
                    flag01 = true;
                    m_texTetVertLinks[in0] = inn1;
                    m_tetTexVertLinks[inn1] = in0;
                }
                if ((nd0 - nnd2).norm() < thres)
                {
                    flag02 = true;
                    m_texTetVertLinks[in0] = inn2;
                    m_tetTexVertLinks[inn2] = in0;
                }
                if (flag00 || flag01 || flag02 )  
                {
                    flag0 = true;
                }

                  ////< XZH nd1
                bool flag10 = false, flag11 = false, flag12 = false;
                if ((nd1 - nnd0).norm() < thres)
                {
                    flag10 = true;
                    m_texTetVertLinks[in1] = inn0;
                    m_tetTexVertLinks[inn0] = in1;
                }
                if ((nd1 - nnd1).norm() < thres)
                {
                    flag11 = true;
                    m_texTetVertLinks[in1] = inn1;
                    m_tetTexVertLinks[inn1] = in1;
                }
                if ((nd1 - nnd2).norm() < thres)
                {
                    flag12 = true;
                    m_texTetVertLinks[in1] = inn2;
                    m_tetTexVertLinks[inn2] = in1;
                }
                if (flag10 || flag11 || flag12)
                {
                    flag1 = true;
                }

                  ////< XZH nd2
                bool flag20 = false, flag21 = false, flag22 = false;
                if ((nd2 - nnd0).norm() < thres)
                {
                    flag20 = true;
                    m_texTetVertLinks[in2] = inn0;
                    m_tetTexVertLinks[inn0] = in2;
                }
                if ((nd2 - nnd1).norm() < thres)
                {
                    flag21 = true;
                    m_texTetVertLinks[in2] = inn1;
                    m_tetTexVertLinks[inn1] = in2;
                }
                if ((nd2 - nnd2).norm() < thres)
                {
                    flag22 = true;
                    m_texTetVertLinks[in2] = inn2;
                    m_tetTexVertLinks[inn2] = in2;
                }
                if (flag20 || flag21 || flag22)
                {
                    flag2 = true;
                }

                  ////< XZH 
                if (flag0&&flag1&&flag2)
                {
                    m_texTetSurfTriLinks[i] = j;
                    m_tetTexSurfTriLinks[j] = i;

                      ////< XZH update triangles
                    if (flag00) vertlist[0] = inn0;
                    if (flag01) vertlist[0] = inn1;
                    if (flag02) vertlist[0] = inn2;

                    if (flag10) vertlist[1] = inn0;
                    if (flag11) vertlist[1] = inn1;
                    if (flag12) vertlist[1] = inn2;

                    if (flag20) vertlist[2] = inn0;
                    if (flag21) vertlist[2] = inn1;
                    if (flag22) vertlist[2] = inn2;

                      ////< XZH output
                    fprintf(outPos, "%f %f %f,  %f %f %f,  \n%f %f %f,  %f %f %f,  \n%f %f %f,  %f %f %f,  \n",
                        nd0.x(), nd0.y(), nd0.z(), nnd0.x(), nnd0.y(), nnd0.z(),
                        nd1.x(), nd1.y(), nd1.z(), nnd1.x(), nnd1.y(), nnd1.z(),
                        nd2.x(), nd2.y(), nd2.z(), nnd2.x(), nnd2.y(), nnd2.z()
                        );
                    break;
                }
            }
        }
        fclose(outPos);

        triListsTextureOrder = trisXZH;
          ////< XZH update tet surface mesh
        auto& normalstex = m_surfaceTexture->getVertexNormals();
        const StdVectorOfVectorf* UVstex = m_surfaceTexture->getPointDataArray(m_surfaceTexture->getDefaultTCoords());
        StdVectorOfVectorf UVsTmp ;

        auto& tangentstex = m_surfaceTexture->getVertexTangents();
        StdVectorOfVec3d normals(posesXZH.size());
        StdVectorOfVectorf UVs(posesXZH.size());
        StdVectorOfVec3d tangents(posesXZH.size());
        for (int i = 0; i < posesXZH.size();i++)
        {
            auto itex = m_tetTexVertLinks[i];   ////< XZH obtain the index in visual texture mesh
            normals[i] = normalstex[itex];
            tangents[i] = tangentstex[itex];
            
            Vectorf UV(2);
            UV[0] = (*UVstex)[itex][0]; //  .TextureCoordinate.x(); // TCoords[i].x(); // texcoords[i].x;
            UV[1] = (*UVstex)[itex][1]; //  .TextureCoordinate.y(); // TCoords[i].y(); //  texcoords[i].y;
            UVs[i] = UV;
        }
        m_surfaceVisualXZH->setVertexNormals(normals);

        m_surfaceVisualXZH->setDefaultTCoords("tCoords");
        m_surfaceVisualXZH->setPointDataArray("tCoords", UVs);

        m_surfaceVisualXZH->setVertexTangents(tangents);

        //  ////< XZH major tet
        //for (int j = 0; j < m_texTetSurfTriLinks.size(); j++)
        //{
        //    int tetid = m_texTetSurfTriLinks[j];  ////< XZH the above j
        //    m_tetTexSurfTriLinks[tetid] = j;
        //}

        //  ////< XZH TEST check
        //FILE *outPos = fopen("i:/checktextet.dat", "w");
        //for (int i = 0; i < trisTex.size(); i++)
        //{    
        //    auto& nd0 = posesTex[trisTex[i].at(0)];
        //    auto& nd1 = posesTex[trisTex[i].at(1)];
        //    auto& nd2 = posesTex[trisTex[i].at(2)];

        //    int tetid = m_texTetSurfTriLinks[i];  ////< XZH the above j
        //    auto& nnd0 = posesXZH[trisXZH[tetid].at(0)];
        //    auto& nnd1 = posesXZH[trisXZH[tetid].at(1)];
        //    auto& nnd2 = posesXZH[trisXZH[tetid].at(2)];
        //    fprintf(outPos, "%f %f %f,  %f %f %f,  \n%f %f %f,  %f %f %f,  \n%f %f %f,  %f %f %f,  \n",
        //        nd0.x(), nd0.y(), nd0.z(), nnd0.x(), nnd0.y(), nnd0.z(),
        //        nd1.x(), nd1.y(), nd1.z(), nnd1.x(), nnd1.y(), nnd1.z(),
        //        nd2.x(), nd2.y(), nd2.z(), nnd2.x(), nnd2.y(), nnd2.z()
        //        );
        //    
        //}
        //fclose(outPos);
          int m = 0;
    }

	void
		VESSTetCutMesh::updatePhysVertex()
	{
		//////< XZH test
		//FILE *infile, *outfile;
		//char line1[100];//title[80],
		//int lnsize = 120;
		//infile = fopen("I:\\iMSTK\\resources\\VESS\\Nick\\matlab4\\injVerts.dat", "r");
		////fgets(line1, lnsize, infile);
		//int NN = 7092;
		//std::vector<int> Y;
		//Y.resize(NN);
		//std::vector<float> YPosX;
		//YPosX.resize(NN);
		//for (int k = 0; k < NN; k++)
		//{
		//	fscanf(infile, "%d %f %f %f", &Y[k], &YPosX[k], &YPosY[k], &YPosZ[k]);
		//}

		  ////< XZH update injection flag
		int m = 0;
		for (int i = 0; i < m_vertexCollision.size(); i++)
		{
			physVertex physVert = m_vertexCollision[i];
			bool flagTet = false;
			bool flagSurf = false;

			for (int j=0;j<m_tetrahedraVertices.size();j++)
			{ 
				bool flag = m_tetrahedraDissectFlag[j];
				if (flag)   ////< XZH on the outside boundary tets
				{
					auto vlist = m_tetrahedraVertices[j];
					if( (i==vlist[0]) || (i == vlist[1])  || (i == vlist[2]) || (i == vlist[3]) )
					{
						flagTet = true;
						break;
					}
				}
			}

			for (int j=0;j<m_triangleVerticesBoundXZH.size();j++)
			{ 
				auto slist = m_triangleVerticesBoundXZH[j];
				if ((i != slist[0]) && (i != slist[1]) && (i != slist[2]) )   ////< XZH not belong to the surface
				{
					flagSurf = true;
				}
			}

			if (flagSurf&&flagTet)
			{ 
				m_vertexCollision[i].bInject = true;   ////< XZH can be injecting (moving)
			}

			if (flagTet)
			{
				m_vertexCollision[i].bDissect = true;   ////< XZH cannot be fixed
			}
		}

		  ////< XZH update injecting surface triangles
		Vec3d p0 = Vec3d(0.02706308, 0.154157, 0.2207738);
		Vec3d p1 = Vec3d(0.02706308, 0.08378759, 0.2207738);
		m_surftrisInject.clear();
		m_surftrisInjectIndex.clear(); // injecting 06/01/2019
		for (size_t i = 0; i < m_triangleVerticesAllXZH.size(); i++)
		{
			auto tri = m_triangleVerticesAllXZH[i];
			int tetIndex1 = m_triangleIsAllTetIndexXZH[i];
			int tetIndex2= m_triangleIsAllTetIndexXZH2[i];
			bool flag1 = m_tetrahedraDissectFlag[tetIndex1];
			bool flag2 = false;
			bool flag = false;
			if (tetIndex2 < 0)   ////< XZH on the outside surface
			{
				flag2 = false;
				flag = false;
			}
			else   ////< XZH for the injecting
			{
				flag2 = m_tetrahedraDissectFlag[tetIndex2];
				if ((flag1 || flag2) && (!(flag1&&flag2)))
				{
					flag = true;
				}
			}	
			if (flag)   ////< XZH save for inject
			{
				injectSurfTris injTri;

				int vert0 = tri[0];
				int vert1 = tri[1];
				int vert2 = tri[2];
				Vec3d vp0 = m_vertexCollision[vert0].pos;
				Vec3d vp1 = m_vertexCollision[vert1].pos;
				Vec3d vp2 = m_vertexCollision[vert2].pos;
				Vec3d normtri = (vp1 - vp0).cross(vp2 - vp0).normalized();
				Vec3d centroid = (vp0 + vp1 + vp2) / 3;
				Vec3d a = centroid - p0;
				Vec3d b = p1 - p0;
				Vec3d c = ((a.dot(b)) / (b.dot(b)))*b;
				Vec3d e = a - c;
				if (normtri.dot(-e) <= 0)  // normal indicating the inside colon, need to reverse
				{
					injTri.nodeIdx[0] = tri[1];
					injTri.nodeIdx[1] = tri[0];
				}
				else
				{
					injTri.nodeIdx[0] = tri[0];
					injTri.nodeIdx[1] = tri[1];
				}
				injTri.nodeIdx[2] = tri[2];

				injTri.tetraIdx1 = tetIndex1;
				injTri.tetraIdx1 = tetIndex2;
				injTri.injectOn = true;
				injTri.closeInject = false;

				m_surftrisInject.push_back(injTri);
				m_surftrisInjectIndex.push_back(i);
			}
		}
	}
}