/*=========================================================================

Library: iMSTK

Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
& Imaging in Medicine, Rensselaer Polytechnic Institute.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0.txt

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

=========================================================================*/

#include "imstkVESSTetrahedralMesh.h"

namespace imstk
{
    bool
        VESSTetrahedralMesh::extractSurfaceMesh()
    {
        bool isExist = false;
        if (surfaceMesh)
        {
            isExist = true;
        }
        else
        {
            isExist = false;
            surfaceMesh = std::make_shared<imstk::SurfaceMesh>();
        }
        
        if (!surfaceMesh)
        {
            LOG(WARNING) << "TetrahedralMesh::extractSurfaceMesh error: the surface mesh provided is not instantiated.";
            return false;
        }

        
        const std::vector<triArray> facePattern = { triArray{ { 0, 1, 2 } }, triArray{ { 0, 1, 3 } }, triArray{ { 0, 2, 3 } }, triArray{ { 1, 2, 3 } } };
        const std::vector<size_t> facePatternTmp = { 3, 2, 1, 0 }; // xzh

        // Find and store the tetrahedral faces that are unique
        auto& states = this->getTetrahedronStates();
        auto vertArray = this->getTetrahedraVertices();
        std::vector<triArray> surfaceTri;
        std::vector<size_t> surfaceTriTet;
        std::vector<size_t> tetRemainingVert;
        bool unique = true;
        size_t foundAt = 0, tetId = 0;
        size_t a, b, c;
        size_t d; // add by xzh
        int ord =-1;
        for (auto &tetVertArray : vertArray)
        {
            ord++;
            //std::cout << "tet: " << tetId << std::endl;
            if (states[ord]<1) continue; // removed

            for (int t = 0; t < 4; ++t)
            {
                unique = true;
                foundAt = 0;
                a = tetVertArray[facePattern[t][0]];
                b = tetVertArray[facePattern[t][1]];
                c = tetVertArray[facePattern[t][2]];
                d = tetVertArray[facePatternTmp[t]]; // add by xzh

                // search in reverse
                for (int i = surfaceTri.size() - 1; i >= 0; i--)
                {
                    const auto &triA = surfaceTri.at(i);
                    if (((triA[0] == a) && ((triA[1] == b && triA[2] == c) || (triA[1] == c && triA[2] == b))) ||
                        ((triA[1] == a) && ((triA[0] == b && triA[2] == c) || (triA[0] == c && triA[2] == b))) ||
                        ((triA[2] == a) && ((triA[1] == b && triA[0] == c) || (triA[1] == c && triA[0] == b))))
                    {
                        unique = false;
                        foundAt = i;
                        break;
                    }
                }

                if (unique)
                {
                    surfaceTri.push_back(triArray{ { a, b, c } });
                    surfaceTriTet.push_back(tetId);
                    tetRemainingVert.push_back(d);  // xzh tetRemainingVert.push_back(3 - t);
                }
                else
                {
                    surfaceTri.erase(surfaceTri.begin() + foundAt);
                    tetRemainingVert.erase(tetRemainingVert.begin() + foundAt);  // add by xzh
                }
            }
            tetId++;
        }

        // Arrange the surface triangle faces found in order
        Vec3d v0, v1, v2;
        Vec3d centroid;
        Vec3d normal;
        for (size_t faceId = 0; faceId < surfaceTri.size(); ++faceId)
        {
            v0 = this->getVertexPosition(surfaceTri.at(faceId)[0]);
            v1 = this->getVertexPosition(surfaceTri.at(faceId)[1]);
            v2 = this->getVertexPosition(surfaceTri.at(faceId)[2]);

            centroid = (v0 + v1 + v2) / 3;

            normal = ((v0 - v1).cross(v0 - v2));

            if (normal.dot(centroid - this->getVertexPosition(tetRemainingVert.at(faceId))) < 0)  // original ..>0   XZH changed to <0
            {
                // swap
                int tmpIndex = surfaceTri[faceId][2];
                surfaceTri[faceId][2] = surfaceTri[faceId][1];
                surfaceTri[faceId][1] = tmpIndex;   // XZH is there any bug here? swap should not be run if using original : surfaceTri[faceId][2] = tmpIndex; 
            }
        }
        m_tetrahedraSurfaceVertices = surfaceTri;
        

        // Renumber the vertices
        std::list<size_t> uniqueVertIdList;
        for (const auto &face : surfaceTri)
        {
            uniqueVertIdList.push_back(face[0]);
            uniqueVertIdList.push_back(face[1]);
            uniqueVertIdList.push_back(face[2]);
        }
        uniqueVertIdList.sort();
        uniqueVertIdList.unique();

        size_t vertId;
        std::list<size_t>::iterator it;
        StdVectorOfVec3d vertPositions;
        for (vertId = 0, it = uniqueVertIdList.begin(); it != uniqueVertIdList.end(); ++vertId, it++)
        {
            vertPositions.push_back(this->getVertexPosition(*it));
            for (auto &face : surfaceTri)
            {
                for (size_t i = 0; i < 3; ++i)
                {
                    if (face[i] == *it)
                    {
                        face[i] = vertId;
                    }
                }
            }
        }

        // Create and attach surface mesh
        if (isExist)
        {
            surfaceMesh->setVertexPositions(vertPositions);
            surfaceMesh->setTrianglesVertices(surfaceTri); 
            //surfaceMesh->computeVertexNormals();
        }
        else
        {
            surfaceMesh->initialize(vertPositions, surfaceTri);
        }

        //surfaceMesh->print();
        return true;
    }

    bool
        VESSTetrahedralMesh::extractSurfaceMesh2()
    {
        bool isExist = false;
        if (surfaceMesh)
        {
            isExist = true;
        }
        else
        {
            isExist = false;
            surfaceMesh = std::make_shared<imstk::SurfaceMesh>();
        }

        if (!surfaceMesh)
        {
            LOG(WARNING) << "TetrahedralMesh::extractSurfaceMesh error: the surface mesh provided is not instantiated.";
            return false;
        }

        const std::vector<triArray> facePattern = { triArray{ { 0, 1, 2 } }, triArray{ { 0, 1, 3 } }, triArray{ { 0, 2, 3 } }, triArray{ { 1, 2, 3 } } };
        const std::vector<size_t> facePatternTmp = { 3, 2, 1, 0 }; // xzh

        // Find and store the tetrahedral faces that are unique
        auto& states = this->getTetrahedronStates();
        auto vertArray = this->getTetrahedraVertices();
        std::vector<triArray> surfaceTri;
        std::vector<size_t> surfaceTriTet;
        std::vector<size_t> tetRemainingVert;
        bool unique = true;
        size_t foundAt = 0, tetId = 0;
        size_t a, b, c;
        size_t d; // add by xzh
        int i = -1;
        for (auto &tetVertArray : vertArray)
        {
            i++;
            //std::cout << "tet: " << tetId << std::endl;
            if (states[i] < 1) continue; // removed

            size_t ni[4];
            ni[0] = tetVertArray[0];
            ni[1] = tetVertArray[1];
            ni[2] = tetVertArray[2];
            ni[3] = tetVertArray[3];

            bool inFlag = false;
            bool in[4] = { false, false, false, false };

            for (int j = 0; j < vertArray.size(); j++)
            {
                if (states[j] < 1) continue; // removed
                int s = 0;
                for (int b = 0; b < 4; b++)
                {
                    if (ni[0] == vertArray[j][b]) s++;
                    if (ni[1] == vertArray[j][b]) s++;
                    if (ni[2] == vertArray[j][b]) s++;
                }
                if (s == 3 && i != j)
                {
                    in[0] = true;
                    break;
                }
            }
            if (!in[0]) // surf
            {
                surfaceTri.push_back(triArray{ { ni[0], ni[1], ni[2] } });
                surfaceTriTet.push_back(tetId);
                tetRemainingVert.push_back(ni[3]);  // xzh tetRemainingVert.push_back(3 - t);
            }

            for (int j = 0; j < vertArray.size(); j++)
            {
                if (states[j] < 1) continue; // removed
                int s = 0;
                for (int b = 0; b < 4; b++)
                {
                    if (ni[0] == vertArray[j][b]) s++;
                    if (ni[1] == vertArray[j][b]) s++;
                    if (ni[3] == vertArray[j][b]) s++;
                }
                if (s == 3 && i != j)
                {
                    in[1] = true;
                    break;
                }
            }
            if (!in[1]) // surf
            {
                surfaceTri.push_back(triArray{ { ni[0], ni[1], ni[3] } });
                surfaceTriTet.push_back(tetId);
                tetRemainingVert.push_back(ni[2]);  // xzh tetRemainingVert.push_back(3 - t);
            }

            for (int j = 0; j < vertArray.size(); j++)
            {
                if (states[j] < 1) continue; // removed
                int s = 0;
                for (int b = 0; b < 4; b++)
                {
                    if (ni[0] == vertArray[j][b]) s++;
                    if (ni[2] == vertArray[j][b]) s++;
                    if (ni[3] == vertArray[j][b]) s++;
                }
                if (s == 3 && i != j)
                {
                    in[2] = true;
                    break;
                }
            }
            if (!in[2]) // surf
            {
                surfaceTri.push_back(triArray{ { ni[0], ni[2], ni[3] } });
                surfaceTriTet.push_back(tetId);
                tetRemainingVert.push_back(ni[1]);  // xzh tetRemainingVert.push_back(3 - t);
            }

            for (int j = 0; j < vertArray.size(); j++)
            {
                if (states[j] < 1) continue; // removed
                int s = 0;
                for (int b = 0; b < 4; b++)
                {
                    if (ni[1] == vertArray[j][b]) s++;
                    if (ni[2] == vertArray[j][b]) s++;
                    if (ni[3] == vertArray[j][b]) s++;
                }
                if (s == 3 && i != j)
                {
                    in[3] = true;
                    break;
                }
            }
            if (!in[3]) // surf
            {
                surfaceTri.push_back(triArray{ { ni[1], ni[2], ni[3] } });
                surfaceTriTet.push_back(tetId);
                tetRemainingVert.push_back(ni[0]);  // xzh tetRemainingVert.push_back(3 - t);
            }
            tetId++;
        }

        // Arrange the surface triangle faces found in order
        Vec3d v0, v1, v2;
        Vec3d centroid;
        Vec3d normal;
        for (size_t faceId = 0; faceId < surfaceTri.size(); ++faceId)
        {
            v0 = this->getVertexPosition(surfaceTri.at(faceId)[0]);
            v1 = this->getVertexPosition(surfaceTri.at(faceId)[1]);
            v2 = this->getVertexPosition(surfaceTri.at(faceId)[2]);

            centroid = (v0 + v1 + v2) / 3;

            normal = ((v0 - v1).cross(v0 - v2));

            if (normal.dot(centroid - this->getVertexPosition(tetRemainingVert.at(faceId))) < 0)  // original ..>0   XZH changed to <0
            {
                // swap
                int tmpIndex = surfaceTri[faceId][2];
                surfaceTri[faceId][2] = surfaceTri[faceId][1];
                surfaceTri[faceId][1] = tmpIndex;   // XZH is there any bug here? swap should not be run if using original : surfaceTri[faceId][2] = tmpIndex; 
            }
        }
        m_tetrahedraSurfaceVertices = surfaceTri;


        // Renumber the vertices
        std::list<size_t> uniqueVertIdList;
        for (const auto &face : surfaceTri)
        {
            uniqueVertIdList.push_back(face[0]);
            uniqueVertIdList.push_back(face[1]);
            uniqueVertIdList.push_back(face[2]);
        }
        uniqueVertIdList.sort();
        uniqueVertIdList.unique();

        size_t vertId;
        std::list<size_t>::iterator it;
        StdVectorOfVec3d vertPositions;
        for (vertId = 0, it = uniqueVertIdList.begin(); it != uniqueVertIdList.end(); ++vertId, it++)
        {
            vertPositions.push_back(this->getVertexPosition(*it));
            for (auto &face : surfaceTri)
            {
                for (size_t i = 0; i < 3; ++i)
                {
                    if (face[i] == *it)
                    {
                        face[i] = vertId;
                    }
                }
            }
        }

        // Create and attach surface mesh
        if (isExist)
        {
            surfaceMesh->setVertexPositions(vertPositions);
            surfaceMesh->setTrianglesVertices(surfaceTri);
            //surfaceMesh->computeVertexNormals();
        }
        else
        {
            surfaceMesh->initialize(vertPositions, surfaceTri);
        }

        //surfaceMesh->print();
        return true;
    }

    bool
        VESSTetrahedralMesh::extractSurfaceMesh(const std::vector<Vec3d>& centralLine)
    {
        Vec3d p0 = centralLine[0];  //  Vec3d(-19.4, -5.85, 1.15); //  centralLine[0];
        Vec3d p1 = centralLine[1];  // Vec3d(-3.3, 2.4, 1.05); //  centralLine[1];

        bool isExist = false;
        if (surfaceMesh)
        {
            isExist = true;
        }
        else
        {
            isExist = false;
            surfaceMesh = std::make_shared<imstk::SurfaceMesh>();
        }

        if (!surfaceMesh)
        {
            LOG(WARNING) << "TetrahedralMesh::extractSurfaceMesh error: the surface mesh provided is not instantiated.";
            return false;
        }
        surfaceInsideMesh = std::make_shared<imstk::SurfaceMesh>(); // for inside using for debug
        // not need initialize here, just when do the injection solution , call init()
        //m_deformMesh = std::make_shared<imstk::VESSDeformMesh>();


        const std::vector<triArray> facePattern = { triArray{ { 0, 1, 2 } }, triArray{ { 0, 1, 3 } }, triArray{ { 0, 2, 3 } }, triArray{ { 1, 2, 3 } } };
        const std::vector<size_t> facePatternTmp = { 3, 2, 1, 0 }; // xzh

        // Find and store the tetrahedral faces that are unique
        auto vertArray = this->getTetrahedraVertices();
        std::vector<triArray> surfaceTri;
        std::vector<size_t> surfaceTriTet;
        std::vector<size_t> tetRemainingVert;
        bool unique = true;
        size_t foundAt = 0, tetId = 0;
        size_t a, b, c;
        size_t d; // add by xzh

        m_tetrahedraSurfaceIndex.clear();
        std::vector<int> m_tetrahedraSurfaceIndex2;
        int tetsurfid = -1;

        for (auto &tetVertArray : vertArray)
        {
            //std::cout << "tet: " << tetId << std::endl;
            tetsurfid++;
            for (int t = 0; t < 4; ++t)
            {
                unique = true;
                foundAt = 0;
                a = tetVertArray[facePattern[t][0]];
                b = tetVertArray[facePattern[t][1]];
                c = tetVertArray[facePattern[t][2]];
                d = tetVertArray[facePatternTmp[t]]; // add by xzh

                // search in reverse
                for (int i = surfaceTri.size() - 1; i >= 0; i--)
                {
                    const auto &triA = surfaceTri.at(i);
                    if (((triA[0] == a) && ((triA[1] == b && triA[2] == c) || (triA[1] == c && triA[2] == b))) ||
                        ((triA[1] == a) && ((triA[0] == b && triA[2] == c) || (triA[0] == c && triA[2] == b))) ||
                        ((triA[2] == a) && ((triA[1] == b && triA[0] == c) || (triA[1] == c && triA[0] == b))))
                    {
                        unique = false;
                        foundAt = i;
                        break;
                    }
                }

                if (unique)
                {
                    surfaceTri.push_back(triArray{ { a, b, c } });
                    m_tetrahedraSurfaceIndex2.push_back(tetsurfid);
                    surfaceTriTet.push_back(tetId);
                    tetRemainingVert.push_back(d);  // xzh tetRemainingVert.push_back(3 - t);
                }
                else
                {
                    surfaceTri.erase(surfaceTri.begin() + foundAt);
                    m_tetrahedraSurfaceIndex2.erase(m_tetrahedraSurfaceIndex2.begin() + foundAt);
                    tetRemainingVert.erase(tetRemainingVert.begin() + foundAt);  // add by xzh
                }
            }
            tetId++;
        }

        // Arrange the surface triangle faces found in order
        Vec3d v0, v1, v2;
        Vec3d centroid;
        Vec3d normal;
        std::vector<triArray> surfaceInsideTri; // XZH

        for (size_t faceId = 0; faceId < surfaceTri.size(); ++faceId)
        {
            v0 = this->getVertexPosition(surfaceTri.at(faceId)[0]);
            v1 = this->getVertexPosition(surfaceTri.at(faceId)[1]);
            v2 = this->getVertexPosition(surfaceTri.at(faceId)[2]);

            centroid = (v0 + v1 + v2) / 3;

            normal = (v1 - v0).cross(v2 - v0); // ((v0 - v1).cross(v0 - v2));

            if (normal.dot(centroid - this->getVertexPosition(tetRemainingVert.at(faceId))) < 0)  // original ..>0   XZH changed to <0
            {
                // swap
                int tmpIndex = surfaceTri[faceId][2];
                surfaceTri[faceId][2] = surfaceTri[faceId][1];
                surfaceTri[faceId][1] = tmpIndex;   // XZH is there any bug here? swap should not be run if using original : surfaceTri[faceId][2] = tmpIndex; 
            }
            // XZH
            Vec3d a = centroid - p0;
            Vec3d b = p1 - p0;
            Vec3d c = ((a.dot(b)) / (b.dot(b)))*b;
            Vec3d e = a - c;
            Vec3d v00 = this->getVertexPosition(surfaceTri.at(faceId)[0]);
            Vec3d v11 = this->getVertexPosition(surfaceTri.at(faceId)[1]);
            Vec3d v22 = this->getVertexPosition(surfaceTri.at(faceId)[2]);
            Vec3d normal2 = (v11 - v00).cross(v22 - v00);
            if (normal2.dot(-e) > 0)  // normal indicating the inside colon
            {
                triArray tttmp = { surfaceTri.at(faceId)[0], surfaceTri.at(faceId)[1], surfaceTri.at(faceId)[2] };
                surfaceInsideTri.push_back(tttmp); //  (surfaceTri[faceId]);
                int tetid = m_tetrahedraSurfaceIndex2[faceId];
                m_tetrahedraSurfaceIndex.push_back(tetid);
            }
            // XZH END
        }
        //m_tetrahedraSurfaceVertices = surfaceTri;  //  use both sides of surface of volume mesh for collision detection
        m_tetrahedraSurfaceVertices = surfaceInsideTri; // only use inside surface of volume mesh for collision detection
        m_mapEdgeTriIndex.clear();
        m_edgeCollision.clear();
        m_edgeColIndex.clear();
        m_surftrisCollision.clear();
        m_surftrisColIndex.clear();
        m_mapEdgeIndex.clear(); // just edge number

        physEdge edgeTmp;
        physSurfTris surftrisTmp;
        int nbrNeiSurfTri0 = -1;
        int nbrNeiSurfTri1 = -1;
        int nbrNeiSurfTri2 = -1;
        int nbrNeiSurfEdge0 = -1;
        int nbrNeiSurfEdge1 = -1;
        int nbrNeiSurfEdge2 = -1;
        int eIdx = -1;
        for (int k = 0; k < m_tetrahedraSurfaceVertices.size(); k++)
        {
            int vert0 = m_tetrahedraSurfaceVertices.at(k)[0]; // three vertices of triangle
            int vert1 = m_tetrahedraSurfaceVertices.at(k)[1];
            int vert2 = m_tetrahedraSurfaceVertices.at(k)[2];
            U64 edgekey;
            int edge0, edge1, edge2;
            edge0 = edge1 = edge2 = -1;
            // 3 edges 0-1
            int m0, m1;
            m0 = vert0; m1 = vert1;
            if (m0 > m1)
            {
                std::swap(m0, m1);
            }
            edgekey = HASHEDGEID_FROM_IDX(m0, m1);
            MAPHEDGEINDEXITER it = m_mapEdgeTriIndex.find(edgekey);
            if (it == m_mapEdgeTriIndex.end())
            {
                m_mapEdgeTriIndex.insert(std::make_pair(edgekey, k));
                edgeTmp.nodeId[0] = vert0;
                edgeTmp.nodeId[1] = vert1;
                edgeTmp.L0 = (this->getVertexPosition(vert0) - this->getVertexPosition(vert1)).norm();
                edgeTmp.colDetOn = true;
                m_edgeCollision.push_back(edgeTmp);
                eIdx++;
                edge0 = eIdx;
                m_edgeColIndex.push_back(eIdx);
                m_mapEdgeIndex.insert(std::make_pair(edgekey, eIdx));
            }
            else
            {
                MAPHEDGEINDEXITER it2 = m_mapEdgeIndex.find(edgekey);
                if (it2 != m_mapEdgeIndex.end())
                {
                    edge0 = it2->second;
                }
            }
            // 1-2
            m0 = vert1; m1 = vert2;
            if (m0 > m1)
            {
                std::swap(m0, m1);
            }
            edgekey = HASHEDGEID_FROM_IDX(m0, m1);
            it = m_mapEdgeTriIndex.find(edgekey);
            if (it == m_mapEdgeTriIndex.end())
            {
                m_mapEdgeTriIndex.insert(std::make_pair(edgekey, k));
                edgeTmp.nodeId[0] = vert1;
                edgeTmp.nodeId[1] = vert2;
                edgeTmp.L0 = (this->getVertexPosition(vert1) - this->getVertexPosition(vert2)).norm();
                edgeTmp.colDetOn = true;
                m_edgeCollision.push_back(edgeTmp);
                eIdx++;
                edge1 = eIdx;
                m_edgeColIndex.push_back(eIdx);
                m_mapEdgeIndex.insert(std::make_pair(edgekey, eIdx));
            }
            else
            {
                MAPHEDGEINDEXITER it2 = m_mapEdgeIndex.find(edgekey);
                if (it2 != m_mapEdgeIndex.end())
                {
                    edge1 = it2->second;
                }
            }
            // 0-2
            m0 = vert0; m1 = vert2;
            if (m0 > m1)
            {
                std::swap(m0, m1);
            }
            edgekey = HASHEDGEID_FROM_IDX(m0, m1);
            it = m_mapEdgeTriIndex.find(edgekey);
            if (it == m_mapEdgeTriIndex.end())
            {
                m_mapEdgeTriIndex.insert(std::make_pair(edgekey, k));
                edgeTmp.nodeId[0] = vert0;
                edgeTmp.nodeId[1] = vert2;
                edgeTmp.L0 = (this->getVertexPosition(vert0) - this->getVertexPosition(vert2)).norm();
                edgeTmp.colDetOn = true;
                m_edgeCollision.push_back(edgeTmp);
                eIdx++;
                edge2 = eIdx;
                m_edgeColIndex.push_back(eIdx);
                m_mapEdgeIndex.insert(std::make_pair(edgekey, eIdx));
            }
            else
            {
                MAPHEDGEINDEXITER it2 = m_mapEdgeIndex.find(edgekey);
                if (it2 != m_mapEdgeIndex.end())
                {
                    edge2 = it2->second;
                }
            }

            // vertex for collision detection
            U64 vertexkey;
            physVertex vertexTmp;
            vertexkey = HASHEDGEID_FROM_IDX(vert0, 1000000);  // will not exceed 1 million, so we can use this way to get unique vertex
            it = m_mapVertexIndex.find(vertexkey);
            if (it == m_mapVertexIndex.end())  // not exist
            {
                m_mapVertexIndex.insert(std::make_pair(vertexkey, k));
                m_vertexCollision[vert0].globalIdx = vert0;
                m_vertexCollision[vert0].onSurf = true;
                m_vertexCollision[vert0].colDetOn = true;
                //vertexTmp.globalIdx = vert0;
                //vertexTmp.oPos = this->getVertexPosition(vert0);
                //vertexTmp.pos = vertexTmp.oPos;
                //vertexTmp.p = vertexTmp.oPos;
                //vertexTmp.pRot = vertexTmp.oPos;
                //std::vector<int>::iterator result = find(m_fixedVerticeList.begin(), m_fixedVerticeList.end(), vert0);  // find whether 
                //if (result == m_fixedVerticeList.end())  vertexTmp.fixed = false;  // cannot find
                //else vertexTmp.fixed = true;
                //m_vertexCollision.push_back(vertexTmp);
            }
            vertexkey = HASHEDGEID_FROM_IDX(vert1, 1000000);  // will not exceed 1 million, so we can use this way to get unique vertex
            it = m_mapVertexIndex.find(vertexkey);
            if (it == m_mapVertexIndex.end())  // not exist
            {
                m_mapVertexIndex.insert(std::make_pair(vertexkey, k));
                m_vertexCollision[vert1].globalIdx = vert1;
                m_vertexCollision[vert1].onSurf = true;
                m_vertexCollision[vert1].colDetOn = true;
                //vertexTmp.globalIdx = vert1;
                //vertexTmp.oPos = this->getVertexPosition(vert1);
                //vertexTmp.pos = vertexTmp.oPos;
                //vertexTmp.p = vertexTmp.oPos;
                //vertexTmp.pRot = vertexTmp.oPos;
                //m_vertexCollision.push_back(vertexTmp);
            }
            vertexkey = HASHEDGEID_FROM_IDX(vert2, 1000000);  // will not exceed 1 million, so we can use this way to get unique vertex
            it = m_mapVertexIndex.find(vertexkey);
            if (it == m_mapVertexIndex.end())  // not exist
            {
                m_mapVertexIndex.insert(std::make_pair(vertexkey, k));
                m_vertexCollision[vert2].globalIdx = vert2;
                m_vertexCollision[vert2].onSurf = true;
                m_vertexCollision[vert2].colDetOn = true;
                //vertexTmp.globalIdx = vert2;
                //vertexTmp.oPos = this->getVertexPosition(vert2);
                //vertexTmp.pos = vertexTmp.oPos;
                //vertexTmp.p = vertexTmp.oPos;
                //vertexTmp.pRot = vertexTmp.oPos;
                //m_vertexCollision.push_back(vertexTmp);
            }

            // vertex END

            // saving surf tris for collision
            surftrisTmp.nodeIdx[0] = vert0;
            surftrisTmp.nodeIdx[1] = vert1;
            surftrisTmp.nodeIdx[2] = vert2;

            surftrisTmp.edgeIdx[0] = edge0;
            surftrisTmp.edgeIdx[1] = edge1;
            surftrisTmp.edgeIdx[2] = edge2;

            surftrisTmp.tetraIdx = m_tetrahedraSurfaceIndex[k];
            surftrisTmp.colDetOn = true;
            m_surftrisCollision.push_back(surftrisTmp);
            m_surftrisColIndex.push_back(k);
        }
        FILE *temp = fopen("i:/testHash.dat", "w");
        for (MAPHEDGEINDEXITER it2 = m_mapEdgeTriIndex.begin(); it2 != m_mapEdgeTriIndex.end(); it2++) {
            fprintf(temp, "%lld %d\n", it2->first, it2->second);
        }
        fclose(temp);

        // traverse edge 
        for (int i = 0; i < m_edgeCollision.size(); i++)
        {
            int m0 = m_edgeCollision[i].nodeId[0];
            int m1 = m_edgeCollision[i].nodeId[1];

            for (int k = 0; k < m_vertexCollision.size(); k++)
            {
                m_vertexCollision[k].nbrNeiSurfEdge = -1;
                int k0 = m_vertexCollision[k].globalIdx;  // node global index compare m0 m1
                if ((k0 == m0) || (k0 == m1))
                {
                    m_vertexCollision[k].nbrNeiSurfEdge++;
                    m_vertexCollision[k].neiSurfEdge[m_vertexCollision[k].nbrNeiSurfEdge] = i;
                }
            }
        }
        // traverse tri to get neibors
        for (int i = 0; i < m_surftrisCollision.size(); i++)
        {
            int m0 = m_surftrisCollision[i].nodeIdx[0];
            int m1 = m_surftrisCollision[i].nodeIdx[1];
            int m2 = m_surftrisCollision[i].nodeIdx[2];

            for (int k = 0; k < m_vertexCollision.size(); k++)
            {
                m_vertexCollision[k].nbrNeiSurfTri = -1;
                int k0 = m_vertexCollision[k].globalIdx;  // node global index compare m0 m1
                if ((k0 == m0) || (k0 == m1) || (k0 == m2))
                {
                    m_vertexCollision[k].nbrNeiSurfTri++;
                    m_vertexCollision[k].neiSurfTri[m_vertexCollision[k].nbrNeiSurfTri] = i;
                }
            }
        }
        // END XZH map

        // output 
        FILE* outVEF = fopen("i:/vef.dat", "w");
        for (int i = 0; i < m_vertexCollision.size(); i++)
        {
            int vertx = m_vertexCollision[i].globalIdx;

            // neighbor edges in colliding
            m_vertexCollision[i].nbrNeiSurfEdge = 0;
            memset(m_vertexCollision[i].neiSurfEdge, -1, 100 * sizeof(int));
            fprintf(outVEF, "\n vert: %d\n neiEdge: ", vertx);
            int numTmp = 0;
            for (int k = 0; k < m_edgeCollision.size(); k++)
            {
                int m0 = m_edgeCollision[k].nodeId[0];
                int m1 = m_edgeCollision[k].nodeId[1];
                if ((m0 == vertx) || (m1 == vertx))
                {
                    m_vertexCollision[i].nbrNeiSurfEdge++;
                    m_vertexCollision[i].neiSurfEdge[numTmp] = k;
                    numTmp++;
                    fprintf(outVEF, " %d ", k);
                }
            }

            // neighbor triangle  neighbor edges in colliding
            m_vertexCollision[i].nbrNeiSurfTri = 0;
            memset(m_vertexCollision[i].neiSurfTri, -1, 100 * sizeof(int));
            fprintf(outVEF, "\n neiSurf:");
            numTmp = 0;
            for (int k = 0; k < m_surftrisCollision.size(); k++)
            {
                int m0 = m_surftrisCollision[k].nodeIdx[0];
                int m1 = m_surftrisCollision[k].nodeIdx[1];
                int m2 = m_surftrisCollision[k].nodeIdx[2];
                if ((m0 == vertx) || (m1 == vertx) || (m2 == vertx))
                {
                    m_vertexCollision[i].nbrNeiSurfTri++;
                    m_vertexCollision[i].neiSurfTri[numTmp] = k;
                    numTmp++;
                    fprintf(outVEF, " %d ", k);
                }
            }
        }
        fclose(outVEF);
        // output END
        //surfaceTri = surfaceInsideTri;  // test just use the only inside one

        // Renumber the vertices
        std::list<size_t> uniqueVertIdList;
        for (const auto &face : surfaceTri)
        {
            uniqueVertIdList.push_back(face[0]);
            uniqueVertIdList.push_back(face[1]);
            uniqueVertIdList.push_back(face[2]);
        }
        uniqueVertIdList.sort();
        uniqueVertIdList.unique();

        size_t vertId;
        std::list<size_t>::iterator it;
        StdVectorOfVec3d vertPositions;
        for (vertId = 0, it = uniqueVertIdList.begin(); it != uniqueVertIdList.end(); ++vertId, it++)
        {
            vertPositions.push_back(this->getVertexPosition(*it));
            for (auto &face : surfaceTri)
            {
                for (size_t i = 0; i < 3; ++i)
                {
                    if (face[i] == *it)
                    {
                        face[i] = vertId;
                    }
                }
            }
        }

        // Create and attach surface mesh
        surfaceMesh->initialize(vertPositions, surfaceTri, true); // if true, calculate normal

        //surfaceMesh->print();
        return true;
    }

    // below is correct, but m_vertexCollision is local order for vertex, so should change
    //bool
    //    VESSTetrahedralMesh::extractSurfaceMesh(const std::vector<Vec3d>& centralLine)
    //{
    //    Vec3d p0 = centralLine[0];  //  Vec3d(-19.4, -5.85, 1.15); //  centralLine[0];
    //    Vec3d p1 = centralLine[1];  // Vec3d(-3.3, 2.4, 1.05); //  centralLine[1];

    //    surfaceMesh = std::make_shared<imstk::SurfaceMesh>();
    //    if (!surfaceMesh)
    //    {
    //        LOG(WARNING) << "TetrahedralMesh::extractSurfaceMesh error: the surface mesh provided is not instantiated.";
    //        return false;
    //    }


    //    const std::vector<triArray> facePattern = { triArray{ { 0, 1, 2 } }, triArray{ { 0, 1, 3 } }, triArray{ { 0, 2, 3 } }, triArray{ { 1, 2, 3 } } };
    //    const std::vector<size_t> facePatternTmp = { 3, 2, 1, 0 }; // xzh

    //    // Find and store the tetrahedral faces that are unique
    //    auto vertArray = this->getTetrahedraVertices();
    //    std::vector<triArray> surfaceTri;
    //    std::vector<size_t> surfaceTriTet;
    //    std::vector<size_t> tetRemainingVert;
    //    bool unique = true;
    //    size_t foundAt = 0, tetId = 0;
    //    size_t a, b, c;
    //    size_t d; // add by xzh

    //    m_tetrahedraSurfaceIndex.clear();
    //    std::vector<int> m_tetrahedraSurfaceIndex2;
    //    int tetsurfid = -1;

    //    for (auto &tetVertArray : vertArray)
    //    {
    //        //std::cout << "tet: " << tetId << std::endl;
    //        tetsurfid++;
    //        for (int t = 0; t < 4; ++t)
    //        {
    //            unique = true;
    //            foundAt = 0;
    //            a = tetVertArray[facePattern[t][0]];
    //            b = tetVertArray[facePattern[t][1]];
    //            c = tetVertArray[facePattern[t][2]];
    //            d = tetVertArray[facePatternTmp[t]]; // add by xzh

    //            // search in reverse
    //            for (int i = surfaceTri.size() - 1; i >= 0; i--)
    //            {
    //                const auto &triA = surfaceTri.at(i);
    //                if (((triA[0] == a) && ((triA[1] == b && triA[2] == c) || (triA[1] == c && triA[2] == b))) ||
    //                    ((triA[1] == a) && ((triA[0] == b && triA[2] == c) || (triA[0] == c && triA[2] == b))) ||
    //                    ((triA[2] == a) && ((triA[1] == b && triA[0] == c) || (triA[1] == c && triA[0] == b))))
    //                {
    //                    unique = false;
    //                    foundAt = i;
    //                    break;
    //                }
    //            }

    //            if (unique)
    //            {
    //                surfaceTri.push_back(triArray{ { a, b, c } });
    //                m_tetrahedraSurfaceIndex2.push_back(tetsurfid);
    //                surfaceTriTet.push_back(tetId);
    //                tetRemainingVert.push_back(d);  // xzh tetRemainingVert.push_back(3 - t);
    //            }
    //            else
    //            {
    //                surfaceTri.erase(surfaceTri.begin() + foundAt);
    //                m_tetrahedraSurfaceIndex2.erase(m_tetrahedraSurfaceIndex2.begin() + foundAt);
    //                tetRemainingVert.erase(tetRemainingVert.begin() + foundAt);  // add by xzh
    //            }
    //        }
    //        tetId++;
    //    }

    //    // Arrange the surface triangle faces found in order
    //    Vec3d v0, v1, v2;
    //    Vec3d centroid;
    //    Vec3d normal;
    //    std::vector<triArray> surfaceInsideTri; // XZH

    //    for (size_t faceId = 0; faceId < surfaceTri.size(); ++faceId)
    //    {
    //        v0 = this->getVertexPosition(surfaceTri.at(faceId)[0]);
    //        v1 = this->getVertexPosition(surfaceTri.at(faceId)[1]);
    //        v2 = this->getVertexPosition(surfaceTri.at(faceId)[2]);

    //        centroid = (v0 + v1 + v2) / 3;

    //        normal = (v1-v0).cross(v2-v0); // ((v0 - v1).cross(v0 - v2));

    //        if (normal.dot(centroid - this->getVertexPosition(tetRemainingVert.at(faceId))) < 0)  // original ..>0   XZH changed to <0
    //        {
    //            // swap
    //            int tmpIndex = surfaceTri[faceId][2];
    //            surfaceTri[faceId][2] = surfaceTri[faceId][1];
    //            surfaceTri[faceId][1] = tmpIndex;   // XZH is there any bug here? swap should not be run if using original : surfaceTri[faceId][2] = tmpIndex; 
    //        }
    //        // XZH
    //        Vec3d a = centroid - p0;
    //        Vec3d b = p1 - p0;
    //        Vec3d c = ((a.dot(b)) / (b.dot(b)))*b;
    //        Vec3d e = a - c;
    //        Vec3d v00 = this->getVertexPosition(surfaceTri.at(faceId)[0]);
    //        Vec3d v11 = this->getVertexPosition(surfaceTri.at(faceId)[1]);
    //        Vec3d v22 = this->getVertexPosition(surfaceTri.at(faceId)[2]);
    //        Vec3d normal2 = (v11 - v00).cross(v22 - v00);
    //        if (normal2.dot(-e)>0)  // normal indicating the inside colon
    //        {
    //            triArray tttmp = { surfaceTri.at(faceId)[0], surfaceTri.at(faceId)[1], surfaceTri.at(faceId)[2] };
    //            surfaceInsideTri.push_back(tttmp); //  (surfaceTri[faceId]);
    //            int tetid = m_tetrahedraSurfaceIndex2[faceId];
    //            m_tetrahedraSurfaceIndex.push_back(tetid);
    //        }
    //        // XZH END
    //    }
    //    //m_tetrahedraSurfaceVertices = surfaceTri;  //  use both sides of surface of volume mesh for collision detection
    //    m_tetrahedraSurfaceVertices = surfaceInsideTri; // only use inside surface of volume mesh for collision detection
    //    m_mapEdgeTriIndex.clear();
    //    m_edgeCollision.clear();
    //    m_edgeColIndex.clear();
    //    m_surftrisCollision.clear();
    //    m_surftrisColIndex.clear();
    //    m_mapEdgeIndex.clear(); // just edge number

    //    physEdge edgeTmp;
    //    physSurfTris surftrisTmp;
    //    int nbrNeiSurfTri0 = -1;
    //    int nbrNeiSurfTri1 = -1;
    //    int nbrNeiSurfTri2 = -1;
    //    int nbrNeiSurfEdge0 = -1;
    //    int nbrNeiSurfEdge1 = -1;
    //    int nbrNeiSurfEdge2 = -1;
    //    int eIdx = -1;
    //    for (int k = 0; k < m_tetrahedraSurfaceVertices.size(); k++)
    //    {
    //        int vert0 = m_tetrahedraSurfaceVertices.at(k)[0]; // three vertices of triangle
    //        int vert1 = m_tetrahedraSurfaceVertices.at(k)[1];
    //        int vert2 = m_tetrahedraSurfaceVertices.at(k)[2];
    //        U64 edgekey;
    //        int edge0, edge1, edge2;
    //        edge0 = edge1 = edge2=-1;
    //        // 3 edges 0-1
    //        int m0, m1;
    //        m0 = vert0; m1 = vert1;
    //        if (m0 > m1)
    //        {
    //            std::swap(m0, m1);
    //        }
    //        edgekey = HASHEDGEID_FROM_IDX(m0, m1);
    //        MAPHEDGEINDEXITER it = m_mapEdgeTriIndex.find(edgekey);
    //        if (it == m_mapEdgeTriIndex.end())
    //        {
    //            m_mapEdgeTriIndex.insert(std::make_pair(edgekey, k));
    //            edgeTmp.nodeId[0] = vert0;
    //            edgeTmp.nodeId[1] = vert1;
    //            edgeTmp.L0 = (this->getVertexPosition(vert0) - this->getVertexPosition(vert1)).norm(); 
    //            edgeTmp.colDetOn = true;
    //            m_edgeCollision.push_back(edgeTmp);
    //            eIdx++;
    //            edge0 = eIdx;
    //            m_edgeColIndex.push_back(eIdx);
    //            m_mapEdgeIndex.insert(std::make_pair(edgekey, eIdx));
    //        }          
    //        else
    //        {
    //            MAPHEDGEINDEXITER it2 = m_mapEdgeIndex.find(edgekey);
    //            if (it2 != m_mapEdgeIndex.end())
    //            {
    //                edge0 = it2->second;
    //            }
    //        }
    //        // 1-2
    //        m0 = vert1; m1 = vert2;
    //        if (m0 > m1)
    //        {
    //            std::swap(m0, m1);
    //        }
    //        edgekey = HASHEDGEID_FROM_IDX(m0, m1);
    //        it = m_mapEdgeTriIndex.find(edgekey);
    //        if (it == m_mapEdgeTriIndex.end())
    //        {
    //            m_mapEdgeTriIndex.insert(std::make_pair(edgekey, k));
    //            edgeTmp.nodeId[0] = vert1;
    //            edgeTmp.nodeId[1] = vert2;
    //            edgeTmp.L0 = (this->getVertexPosition(vert1) - this->getVertexPosition(vert2)).norm();
    //            edgeTmp.colDetOn = true;
    //            m_edgeCollision.push_back(edgeTmp);
    //            eIdx++;
    //            edge1 = eIdx;
    //            m_edgeColIndex.push_back(eIdx);
    //            m_mapEdgeIndex.insert(std::make_pair(edgekey, eIdx));
    //        }
    //        else
    //        {
    //            MAPHEDGEINDEXITER it2 = m_mapEdgeIndex.find(edgekey);
    //            if (it2 != m_mapEdgeIndex.end())
    //            {
    //                edge1 = it2->second;
    //            }
    //        }
    //        // 0-2
    //        m0 = vert0; m1 = vert2;
    //        if (m0 > m1)
    //        {
    //            std::swap(m0, m1);
    //        }
    //        edgekey = HASHEDGEID_FROM_IDX(m0, m1);
    //        it = m_mapEdgeTriIndex.find(edgekey);
    //        if (it == m_mapEdgeTriIndex.end())
    //        {
    //            m_mapEdgeTriIndex.insert(std::make_pair(edgekey, k));
    //            edgeTmp.nodeId[0] = vert0;
    //            edgeTmp.nodeId[1] = vert2;
    //            edgeTmp.L0 = (this->getVertexPosition(vert0) - this->getVertexPosition(vert2)).norm();
    //            edgeTmp.colDetOn = true;
    //            m_edgeCollision.push_back(edgeTmp);
    //            eIdx++;
    //            edge2 = eIdx;
    //            m_edgeColIndex.push_back(eIdx);
    //            m_mapEdgeIndex.insert(std::make_pair(edgekey, eIdx));
    //        }
    //        else
    //        {
    //            MAPHEDGEINDEXITER it2 = m_mapEdgeIndex.find(edgekey);
    //            if (it2 != m_mapEdgeIndex.end())
    //            {
    //                edge2 = it2->second;
    //            }
    //        }

    //        // vertex for collision detection
    //        U64 vertexkey;
    //        physVertex vertexTmp;
    //        vertexkey = HASHEDGEID_FROM_IDX(vert0, 1000000);  // will not exceed 1 million, so we can use this way to get unique vertex
    //        it = m_mapVertexIndex.find(vertexkey);
    //        if (it == m_mapVertexIndex.end())  // not exist
    //        {
    //            m_mapVertexIndex.insert(std::make_pair(vertexkey, k));
    //            vertexTmp.globalIdx = vert0;
    //            vertexTmp.oPos = this->getVertexPosition(vert0);
    //            vertexTmp.pos = vertexTmp.oPos;
    //            vertexTmp.p = vertexTmp.oPos;
    //            vertexTmp.pRot = vertexTmp.oPos;
    //            //std::vector<int>::iterator result = find(m_fixedVerticeList.begin(), m_fixedVerticeList.end(), vert0);  // find whether 
    //            //if (result == m_fixedVerticeList.end())  vertexTmp.fixed = false;  // cannot find
    //            //else vertexTmp.fixed = true;
    //            m_vertexCollision.push_back(vertexTmp);
    //        }
    //        vertexkey = HASHEDGEID_FROM_IDX(vert1, 1000000);  // will not exceed 1 million, so we can use this way to get unique vertex
    //        it = m_mapVertexIndex.find(vertexkey);
    //        if (it == m_mapVertexIndex.end())  // not exist
    //        {
    //            m_mapVertexIndex.insert(std::make_pair(vertexkey, k));
    //            vertexTmp.globalIdx = vert1;
    //            vertexTmp.oPos = this->getVertexPosition(vert1);
    //            vertexTmp.pos = vertexTmp.oPos;
    //            vertexTmp.p = vertexTmp.oPos;
    //            vertexTmp.pRot = vertexTmp.oPos;
    //            m_vertexCollision.push_back(vertexTmp);
    //        }
    //        vertexkey = HASHEDGEID_FROM_IDX(vert2, 1000000);  // will not exceed 1 million, so we can use this way to get unique vertex
    //        it = m_mapVertexIndex.find(vertexkey);
    //        if (it == m_mapVertexIndex.end())  // not exist
    //        {
    //            m_mapVertexIndex.insert(std::make_pair(vertexkey, k));
    //            vertexTmp.globalIdx = vert2;
    //            vertexTmp.oPos = this->getVertexPosition(vert2);
    //            vertexTmp.pos = vertexTmp.oPos;
    //            vertexTmp.p = vertexTmp.oPos;
    //            vertexTmp.pRot = vertexTmp.oPos;
    //            m_vertexCollision.push_back(vertexTmp);
    //        }

    //        // vertex END

    //        // saving surf tris for collision
    //        surftrisTmp.nodeIdx[0] = vert0;
    //        surftrisTmp.nodeIdx[1] = vert1;
    //        surftrisTmp.nodeIdx[2] = vert2;

    //        surftrisTmp.edgeIdx[0] = edge0; 
    //        surftrisTmp.edgeIdx[1] = edge1;
    //        surftrisTmp.edgeIdx[2] = edge2;

    //        surftrisTmp.tetraIdx = m_tetrahedraSurfaceIndex[k];
    //        surftrisTmp.colDetOn = true;
    //        m_surftrisCollision.push_back(surftrisTmp);
    //        m_surftrisColIndex.push_back(k);
    //    }
    //    FILE *temp = fopen("G:/testHash.dat","w");
    //    for (MAPHEDGEINDEXITER it2 = m_mapEdgeTriIndex.begin(); it2 != m_mapEdgeTriIndex.end(); it2++) {
    //        fprintf(temp, "%lld %d\n", it2->first, it2->second);
    //    }  
    //    fclose(temp);

    //    // traverse edge 
    //    for (int i = 0; i < m_edgeCollision.size();i++)
    //    {
    //        int m0= m_edgeCollision[i].nodeId[0];
    //        int m1 = m_edgeCollision[i].nodeId[1];

    //        for (int k = 0; k < m_vertexCollision.size();k++)
    //        {
    //            m_vertexCollision[k].nbrNeiSurfEdge = -1;
    //            int k0 = m_vertexCollision[k].globalIdx;  // node global index compare m0 m1
    //            if ((k0==m0)||(k0==m1))
    //            {
    //                m_vertexCollision[k].nbrNeiSurfEdge++;
    //                m_vertexCollision[k].neiSurfEdge[m_vertexCollision[k].nbrNeiSurfEdge] = i;
    //            }
    //        }
    //    }
    //    // traverse tri to get neibors
    //    for (int i = 0; i < m_surftrisCollision.size(); i++)
    //    {
    //        int m0 = m_surftrisCollision[i].nodeIdx[0];
    //        int m1 = m_surftrisCollision[i].nodeIdx[1];
    //        int m2 = m_surftrisCollision[i].nodeIdx[2];

    //        for (int k = 0; k < m_vertexCollision.size(); k++)
    //        {
    //            m_vertexCollision[k].nbrNeiSurfTri = -1;
    //            int k0 = m_vertexCollision[k].globalIdx;  // node global index compare m0 m1
    //            if ((k0 == m0) || (k0 == m1)||(k0==m2))
    //            {
    //                m_vertexCollision[k].nbrNeiSurfTri++;
    //                m_vertexCollision[k].neiSurfTri[m_vertexCollision[k].nbrNeiSurfTri] = i;
    //            }
    //        }
    //    }
    //    // END XZH map

    //    // output 
    //    FILE* outVEF = fopen("G:/vef.dat", "w");
    //    for (int i = 0; i < m_vertexCollision.size(); i++)
    //    {
    //        int vertx = m_vertexCollision[i].globalIdx;

    //        // neighbor edges in colliding
    //        m_vertexCollision[i].nbrNeiSurfEdge = 0;
    //        memset(m_vertexCollision[i].neiSurfEdge, -1, 100 * sizeof(int));
    //        fprintf(outVEF, "\n vert: %d\n neiEdge: ", vertx);
    //        int numTmp = 0;
    //        for (int k = 0; k < m_edgeCollision.size();k++)
    //        {
    //            int m0 = m_edgeCollision[k].nodeId[0];
    //            int m1 = m_edgeCollision[k].nodeId[1];
    //            if ((m0==vertx)||(m1==vertx))
    //            {
    //                m_vertexCollision[i].nbrNeiSurfEdge++;
    //                m_vertexCollision[i].neiSurfEdge[numTmp] = k;
    //                numTmp++;
    //                fprintf(outVEF, " %d ", k);
    //            }
    //        }

    //        // neighbor triangle  neighbor edges in colliding
    //        m_vertexCollision[i].nbrNeiSurfTri = 0;
    //        memset(m_vertexCollision[i].neiSurfTri, -1, 100 * sizeof(int));
    //        fprintf(outVEF, "\n neiSurf:");
    //        numTmp = 0;
    //        for (int k = 0; k < m_surftrisCollision.size();k++)
    //        {
    //            int m0 = m_surftrisCollision[k].nodeIdx[0];
    //            int m1 = m_surftrisCollision[k].nodeIdx[1];
    //            int m2 = m_surftrisCollision[k].nodeIdx[2];
    //            if ((m0 == vertx) || (m1 == vertx)||(m2==vertx))
    //            {
    //                m_vertexCollision[i].nbrNeiSurfTri++;
    //                m_vertexCollision[i].neiSurfTri[numTmp] = k;
    //                numTmp++;
    //                fprintf(outVEF, " %d ", k);
    //            }
    //        }
    //    }
    //    fclose(outVEF);
    //    // output END
    //    //surfaceTri = surfaceInsideTri;  // test just use the only inside one

    //    // Renumber the vertices
    //    std::list<size_t> uniqueVertIdList;
    //    for (const auto &face : surfaceTri)
    //    {
    //        uniqueVertIdList.push_back(face[0]);
    //        uniqueVertIdList.push_back(face[1]);
    //        uniqueVertIdList.push_back(face[2]);
    //    }
    //    uniqueVertIdList.sort();
    //    uniqueVertIdList.unique();

    //    size_t vertId;
    //    std::list<size_t>::iterator it;
    //    StdVectorOfVec3d vertPositions;
    //    for (vertId = 0, it = uniqueVertIdList.begin(); it != uniqueVertIdList.end(); ++vertId, it++)
    //    {
    //        vertPositions.push_back(this->getVertexPosition(*it));
    //        for (auto &face : surfaceTri)
    //        {
    //            for (size_t i = 0; i < 3; ++i)
    //            {
    //                if (face[i] == *it)
    //                {
    //                    face[i] = vertId;
    //                }
    //            }
    //        }
    //    }

    //    // Create and attach surface mesh
    //    surfaceMesh->initialize(vertPositions, surfaceTri, true); // if true, calculate normal

    //    //surfaceMesh->print();
    //    return true;
    //}

    bool
        VESSTetrahedralMesh::extractSurfaceMeshVolume()
    {
        surfaceMeshVolume = std::make_shared<imstk::SurfaceMesh>();
        // volumetric surface mesh for whole tetrahedrons
        StdVectorOfVec3d vertPositions2Volume;
        std::list<size_t> uniqueVertIdList2Volume;

        //std::vector<TetraArray> vertArray;
        bool unique = true;
        size_t foundAt = 0, tetId = 0;
        size_t a, b, c;
        size_t d; // add by xzh
        const std::vector<triArray> facePattern = { triArray{ { 0, 1, 2 } }, triArray{ { 0, 1, 3 } }, triArray{ { 0, 2, 3 } }, triArray{ { 1, 2, 3 } } };
        const std::vector<size_t> facePatternTmp = { 3, 2, 1, 0 }; // xzh
        std::vector<triArray> surfaceTri2Volume;
        auto vertArray = this->getTetrahedraVertices(); 
        auto vertPos = this->getVertexPositions();
        size_t vertId;
        std::list<size_t>::iterator it;

        for (auto &tetVertArray : vertArray)
        {
            a = tetVertArray[facePattern[0][0]];
            b = tetVertArray[facePattern[0][1]];
            c = tetVertArray[facePattern[0][2]];
            surfaceTri2Volume.push_back(triArray{ { a, b, c } });
            a = tetVertArray[facePattern[1][0]];
            b = tetVertArray[facePattern[1][1]];
            c = tetVertArray[facePattern[1][2]];
            surfaceTri2Volume.push_back(triArray{ { a, b, c } });
            a = tetVertArray[facePattern[2][0]];
            b = tetVertArray[facePattern[2][1]];
            c = tetVertArray[facePattern[2][2]];
            surfaceTri2Volume.push_back(triArray{ { a, b, c } });
            a = tetVertArray[facePattern[3][0]];
            b = tetVertArray[facePattern[3][1]];
            c = tetVertArray[facePattern[3][2]];
            surfaceTri2Volume.push_back(triArray{ { a, b, c } });
        }
        for (const auto &face : surfaceTri2Volume)
        {
            uniqueVertIdList2Volume.push_back(face[0]);
            uniqueVertIdList2Volume.push_back(face[1]);
            uniqueVertIdList2Volume.push_back(face[2]);
        }
        uniqueVertIdList2Volume.sort();
        uniqueVertIdList2Volume.unique();

        for (vertId = 0, it = uniqueVertIdList2Volume.begin(); it != uniqueVertIdList2Volume.end(); ++vertId, it++)
        {
            auto pos = vertPos[*it];
            vertPositions2Volume.push_back(pos);
            for (auto &face : surfaceTri2Volume)
            {
                for (size_t i = 0; i < 3; ++i)
                {
                    if (face[i] == *it)
                    {
                        face[i] = vertId;
                    }
                }
            }
        }

        surfaceMeshVolume->initialize(vertPositions2Volume, surfaceTri2Volume);

        return true;
    }

    void
        VESSTetrahedralMesh::setPhysXLink(const std::vector<physXLink>& links)
    {
        m_volsurfLinks = links;
    }

    void
        VESSTetrahedralMesh::updateFixedVertice()
    {
        for (int i = 0; i < m_vertexCollision.size();i++)
        {
            int m=m_vertexCollision[i].globalIdx;
            for (int k = 0; k < m_fixedVerticeList.size();k++)
            {
                if (m == m_fixedVerticeList[k]) m_vertexCollision[i].fixed = true;
                else m_vertexCollision[i].fixed = false;
            }
        }
    }

    void
        VESSTetrahedralMesh::setInitialPhysVertex(const StdVectorOfVec3d& physVertices)
    {
        for (int i = 0; i < physVertices.size();i++)
        {
            physVertex physVert;
            physVert.globalIdx = i;
            physVert.oPos = physVertices[i];
            physVert.pos = physVert.oPos;
            physVert.p = physVert.oPos;
            physVert.pRot = physVert.oPos;
            physVert.fixed = false;
            physVert.onSurf = false;
			physVert.onSurfSurrounding = false;
            physVert.contact = false;
            physVert.colDetOn = false;
            physVert.touched = false;
            physVert.nbrNeiSurfEdge = 0;
            physVert.nbrNeiSurfTri = 0;
            m_vertexCollision.push_back(physVert);
        }

        // test hash table
        m_spatialhashVESS = std::make_shared<imstk::SpatialHashTableVESS>();
        m_spatialhashVESS->insertPoints(m_vertexCollision);  
    }

    void
        VESSTetrahedralMesh::initDeformMesh()
    {
		// not need initialize here, just when do the injection solution , call init()
		m_deformMesh = std::make_shared<imstk::VESSDeformMesh>(); // put this in the initial place
		std::vector<triArray> surfaceTri;

		// XZH       //  surface triangle just inside surface of tet, but physVertices are the whole tet
		auto& physVertices = this->getPhysVertex();
		auto& physEdge = this->getPhysEdge();
		auto& physSTris = this->getInjectSurfTris();   ////< XZH  by 06/04/2019 this->getPhysSurfTris();

		if (isFirstSurface)
		{
			int num = 0;
			for (int k = 0; k < physVertices.size(); k++)
			{
				auto& vet = physVertices[k];
				if (vet.onSurf) num++;
			}

			F.resize(3, physSTris.size());  // m_deformMesh->F.resize(3, physSTris.size());
			int num2 = 0;
			std::vector<injectSurfTris> physSTrisTemp = physSTris;  ////< XZH  by 06/04/2019 std::vector<physSurfTris> physSTrisTemp = physSTris;  // not reference ,so will not change the original value

																  // using iMSTK method
			localToGlobalMappingVertex.clear();  // m_deformMesh->localToGlobalMappingVertex.clear();
			// Renumber the vertices
			//std::list<int> uniqueVertIdList;
			for (const auto &face : physSTrisTemp)
			{
				uniqueVertIdList.push_back(face.nodeIdx[0]);
				uniqueVertIdList.push_back(face.nodeIdx[1]);
				uniqueVertIdList.push_back(face.nodeIdx[2]);
			}
			uniqueVertIdList.sort();
			uniqueVertIdList.unique();

			int vertId;
			std::list<int>::iterator it;
			// StdVectorOfVec3d vertPositions; 
			vertPositions.clear();
			for (vertId = 0, it = uniqueVertIdList.begin(); it != uniqueVertIdList.end(); ++vertId, it++)
			{
				vertPositions.push_back(physVertices[*it].pos); // coords
				localToGlobalMappingVertex.insert(std::make_pair(vertId, *it));  // m_deformMesh->localToGlobalMappingVertex.insert(std::make_pair(vertId, *it));  // mapping between local and global
				for (auto &face : physSTrisTemp)  // reference, traverse all the physSTris to check whether this vertex global index number == *it (global index)
				{
					for (int i = 0; i < 3; ++i)
					{
						if (face.nodeIdx[i] == *it)
						{
							face.nodeIdx[i] = vertId; // change the global index to local index 
						}
					}
				}
			} // END for std::list

			for (int k = 0; k < physSTrisTemp.size(); k++)
			{
				auto& tris = physSTrisTemp[k];
				F(0, k) = tris.nodeIdx[0];  // m_deformMesh->F(0, k) = tris.nodeIdx[0];
				F(1, k) = tris.nodeIdx[1];   // m_deformMesh->F(1, k) = tris.nodeIdx[1];
				F(2, k) = tris.nodeIdx[2];  // m_deformMesh->F(2, k) = tris.nodeIdx[2];
				surfaceTri.push_back(triArray{ { (size_t) tris.nodeIdx[0], (size_t) tris.nodeIdx[1], (size_t) tris.nodeIdx[2] } });
			}

			//num2 = m_deformMesh->localToGlobalMappingVertex.size();
			V.resize(3, uniqueVertIdList.size());  // m_deformMesh->V.resize(3, uniqueVertIdList.size());

			// inside  // Create and attach surface inside mesh of colon
			surfaceInsideMesh->initialize(vertPositions, surfaceTri, true); // if true, calculate normal
			isFirstSurface = false;
		}
		

		std::list<int>::iterator it2;
		vertPositions.clear();
		for ( it2 = uniqueVertIdList.begin(); it2 != uniqueVertIdList.end(); it2++)
		{
			vertPositions.push_back(physVertices[*it2].pos); // coords
		} // END for std::list

		if (!isFirstSurface)	 // not the first just update position
		{
			surfaceInsideMesh->setVertexPositions(vertPositions);
		}

		m_deformMesh->localToGlobalMappingVertex = localToGlobalMappingVertex;
		m_deformMesh->F.resize(3, physSTris.size());
		m_deformMesh->V.resize(3, uniqueVertIdList.size());
		m_deformMesh->F = F;
		for (int k = 0; k < vertPositions.size(); k++)
		{
			//F(0, k) = vertPositions[k].x();
			//F(1, k) = vertPositions[k].y();
			//F(2, k) = vertPositions[k].z();
			m_deformMesh->V.col(k) = vertPositions[k];
		}

		m_deformMesh->init();

		return;
		//// Create and attach surface inside mesh of colon
		//if (isFirstSurface)
		//{
		//	surfaceInsideMesh->initialize(vertPositions, surfaceTri, true); // if true, calculate normal
		//	isFirstSurface = false;
		//}
		//else // not the first just update position
		//{
		//	surfaceInsideMesh->setVertexPositions(vertPositions);
		//}

		//// 08012017  Old way to too much loop will reduce the rate
  //      // not need initialize here, just when do the injection solution , call init()
  //      //m_deformMesh = std::make_shared<imstk::VESSDeformMesh>(); // put this in the initial place
  //      std::vector<triArray> surfaceTri;
  //      // init()
  //      //V = mesh.V;
  //      //F = mesh.F;
  //      //W = mesh.W;

  //      // XZH       //  surface triangle just inside surface of tet, but physVertices are the whole tet
  //      auto& physVertices = this->getPhysVertex();
  //      auto& physEdge = this->getPhysEdge();
  //      auto& physSTris = this->getPhysSurfTris();
  //      int num = 0;
  //      for (int k = 0; k < physVertices.size(); k++)
  //      {
  //          auto& vet = physVertices[k];
  //          if (vet.onSurf) num++;
  //      }

  //      m_deformMesh->F.resize(3, physSTris.size());
  //      int num2 = 0;
  //      std::vector<physSurfTris> physSTrisTemp = physSTris;  // not reference ,so will not change the original value

  //      // using iMSTK method
  //      m_deformMesh->localToGlobalMappingVertex.clear();
  //      // Renumber the vertices
  //      std::list<int> uniqueVertIdList;
  //      for (const auto &face : physSTrisTemp)
  //      {
  //          uniqueVertIdList.push_back(face.nodeIdx[0]);
  //          uniqueVertIdList.push_back(face.nodeIdx[1]);
  //          uniqueVertIdList.push_back(face.nodeIdx[2]);
  //      }
  //      uniqueVertIdList.sort();
  //      uniqueVertIdList.unique();

  //      int vertId;
  //      std::list<int>::iterator it;
  //      StdVectorOfVec3d vertPositions;
  //      for (vertId = 0, it = uniqueVertIdList.begin(); it != uniqueVertIdList.end(); ++vertId, it++)
  //      {
  //          vertPositions.push_back(physVertices[*it].pos); // coords
  //          m_deformMesh->localToGlobalMappingVertex.insert(std::make_pair(vertId, *it));  // mapping between local and global
  //          for (auto &face : physSTrisTemp)  // reference, traverse all the physSTris to check whether this vertex global index number == *it (global index)
  //          {
  //              for (int i = 0; i < 3; ++i)
  //              {
  //                  if (face.nodeIdx[i] == *it)
  //                  {
  //                      face.nodeIdx[i] = vertId; // change the global index to local index 
  //                  }
  //              }
  //          }
  //      } // END for std::list

  //      for (int k = 0; k < physSTrisTemp.size(); k++)
  //      {
  //          auto& tris = physSTrisTemp[k];
  //          m_deformMesh->F(0, k) = tris.nodeIdx[0];
  //          m_deformMesh->F(1, k) = tris.nodeIdx[1];
  //          m_deformMesh->F(2, k) = tris.nodeIdx[2];
  //          surfaceTri.push_back(triArray{ { tris.nodeIdx[0], tris.nodeIdx[1], tris.nodeIdx[2] } });
  //      }

  //      num2 = m_deformMesh->localToGlobalMappingVertex.size();
  //      m_deformMesh->V.resize(3, uniqueVertIdList.size());
  //      for (int k = 0; k < vertPositions.size(); k++)
  //      {
  //          //F(0, k) = vertPositions[k].x();
  //          //F(1, k) = vertPositions[k].y();
  //          //F(2, k) = vertPositions[k].z();
  //          m_deformMesh->V.col(k) = vertPositions[k];
  //      }

  //      m_deformMesh->init();

  //      // Create and attach surface inside mesh of colon
  //      if (isFirstSurface)
  //      {
  //          surfaceInsideMesh->initialize(vertPositions, surfaceTri, true); // if true, calculate normal
  //          isFirstSurface = false;
  //      }
  //      else // not the first just update position
  //      {
  //          surfaceInsideMesh->setVertexPositions(vertPositions);
  //      }    
    }

    void
        VESSTetrahedralMesh::initSPHParticles()
    {
        m_sphParticles = std::make_shared<imstk::SPHXZH>(); // put this in the initial place
        m_sphParticles->init_system();
    }
}// imstk
