/*=========================================================================

Library: iMSTK

Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
& Imaging in Medicine, Rensselaer Polytechnic Institute.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0.txt

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

=========================================================================*/

#ifndef imstkVESSBasicMesh_h
#define imstkVESSBasicMesh_h

#include "imstkVESSBasic.h"
#include "imstkTetrahedralMesh.h"

///
/// \for Hash
///
#include <unordered_map> // #include <unordered_map>
#include <limits>
#include <math.h>
#include <stdlib.h>
#include "imstkMath.h"

namespace imstk
{

    bool loadTetFile(char *p_TetFileName, imstk::StdVectorOfVec3d &vertList, std::vector<imstk::TetrahedralMesh::TetraArray> &tetConnectivity, std::vector<physXLink> &links);
    
    class ParticleXZH
    {
    public:
        uintXZH id;
        floatXZH3 pos;
        floatXZH3 vel;
        floatXZH3 acc;
        floatXZH3 ev;

        float dens;
        float pres;

        float surf_norm;

        ParticleXZH *next;
    };

    class SPHXZH
    {
    public:
        uintXZH max_particle;
        uintXZH num_particle;

        float kernel;
        float mass;

        floatXZH3 world_size;
        float cell_size;
        uintXZH3 grid_size;
        uintXZH tot_cell;

        floatXZH3 gravity;
        float wall_damping;
        float rest_density;
        float gas_constant;
        float viscosity;
        float time_step;
        float surf_norm;
        float surf_coe;

        float poly6_value;
        float spiky_value;
        float visco_value;

        float grad_poly6;
        float lplc_poly6;

        float kernel_2;
        float self_dens;
        float self_lplc_color;

        ParticleXZH *mem;
        ParticleXZH **cell;

        uintXZH sys_running;

          ////< XZH param
    public:
        float window_width = 1000;
        float window_height = 1000;

        float xRot = 15.0f;
        float yRot = 0.0f;
        float xTrans = 0.0;
        float yTrans = 0;
        float zTrans = -35.0;

        int ox;
        int oy;
        int buttonState;
        float xRotLength = 0.0f;
        float yRotLength = 0.0f;

        floatXZH3 real_world_origin;
        floatXZH3 real_world_side;
        floatXZH3 sim_ratio;

        float world_width;
        float world_height;
        float world_length;

        std::vector<Vec3d> m_particlePoses;
    public:
        SPHXZH();
        ~SPHXZH();
        void animation();
        void init_system();
        void add_particle(floatXZH3 pos, floatXZH3 vel);

          ////< XZH update
        void updatePositions(); 
        std::vector<Vec3d>& getParticlePoses()  { return m_particlePoses; }
    private:
        void build_table();
        void comp_dens_pres();
        void comp_force_adv();
        void advection();


    private:
        intXZH3 calc_cell_pos(floatXZH3 p);
        uintXZH calc_cell_hash(intXZH3 cell_pos);
    };
}// imstk

#endif // ifndef imstkVESSBasic_h
