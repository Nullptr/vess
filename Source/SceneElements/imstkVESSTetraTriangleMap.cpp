/*=========================================================================

Library: iMSTK

Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
& Imaging in Medicine, Rensselaer Polytechnic Institute.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0.txt

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

=========================================================================*/

#include "imstkVESSTetraTriangleMap.h"

namespace imstk
{

    void
        VESSTetraTriangleMap::compute()
    {
        if (!m_master || !m_slave)
        {
            LOG(WARNING) << "TetraTriangle map is being applied without valid geometries";
            return;
        }

        auto tetMesh = std::dynamic_pointer_cast<VESSTetrahedralMesh> (m_master);
        auto triMesh = std::dynamic_pointer_cast<SurfaceMesh> (m_slave);

        m_verticesEnclosingTetraId.clear();
        m_verticesWeights.clear();

        std::vector<physXLink> m_links = tetMesh->getPhysXLink();

        for (size_t vertexId = 0; vertexId < triMesh->getNumVertices(); ++vertexId)
        {
            // Compute the weights
            VESSTetrahedralMesh::WeightsArray weights = { 0.0, 0.0, 0.0, 0.0 };
            weights[0] = m_links[vertexId].baryCetricDistance[0];
            weights[1] = m_links[vertexId].baryCetricDistance[1];
            weights[2] = m_links[vertexId].baryCetricDistance[2];
            weights[3] = m_links[vertexId].baryCetricDistance[3];
            m_verticesEnclosingTetraId.push_back(m_links[vertexId].tetraIndex); // store nearest tetrahedron
            m_verticesWeights.push_back(weights); // store weights
        }
    }

    void
        VESSTetraTriangleMap::apply()
    {
        //return;
        // Check if map is active
        if (!m_isActive)
        {
            LOG(WARNING) << "TetraTriangle map is not active";
            return;
        }

        // Check geometries
        if (!m_master || !m_slave)
        {
            LOG(WARNING) << "TetraTriangle map is being applied without valid geometries";
            return;
        }

        auto tetMesh = std::dynamic_pointer_cast<VESSTetrahedralMesh> (m_master);
        auto triMesh = std::dynamic_pointer_cast<SurfaceMesh> (m_slave);

        Vec3d newPos;
        for (size_t vertexId = 0; vertexId < triMesh->getNumVertices(); ++vertexId)
        {
            newPos.setZero();
            size_t tetId = m_verticesEnclosingTetraId.at(vertexId);
            auto tetVerts = tetMesh->getTetrahedronVertices(tetId);
            auto weights = m_verticesWeights.at(vertexId);
            for (size_t i = 0; i < 4; ++i)
            {
                newPos += tetMesh->getVertexPosition(tetVerts[i]) * weights[i];
            }
            triMesh->setVertexPosition(vertexId, newPos);
        }
    }

    void
        VESSTetraTriangleMap::print() const
    {
        // Print Type
        GeometryMap::print();

        // Print vertices and weight info
        LOG(INFO) << "Vertex (<vertNum>): Tetrahedra: <TetNum> - Weights: (w1, w2, w3, w4)\n";
        for (size_t vertexId = 0; vertexId < m_verticesEnclosingTetraId.size(); ++vertexId)
        {
            LOG(INFO) << "Vertex (" << vertexId << "):"
                << "\tTetrahedra: " << m_verticesEnclosingTetraId.at(vertexId)
                << " - Weights: " << "("
                << m_verticesWeights.at(vertexId)[0] << ", "
                << m_verticesWeights.at(vertexId)[1] << ", "
                << m_verticesWeights.at(vertexId)[2] << ", "
                << m_verticesWeights.at(vertexId)[3] << ")";
        }
    }

    bool
        VESSTetraTriangleMap::isValid() const
    {
        auto meshMaster = std::dynamic_pointer_cast<VESSTetrahedralMesh>(m_master);
        auto totalElementsMaster = meshMaster->getNumTetrahedra();

        for (size_t tetId = 0; tetId < m_verticesEnclosingTetraId.size(); ++tetId)
        {
            if (!(m_verticesEnclosingTetraId.at(tetId) < totalElementsMaster &&
                m_verticesEnclosingTetraId.at(tetId) >= 0))
            {
                return false;
            }
        }
        return true;
    }

    void
        VESSTetraTriangleMap::setMaster(std::shared_ptr<Geometry> master)
    {
        if (master->getType() != Geometry::Type::TetrahedralMesh)
        {
            LOG(WARNING) << "The geometry provided as master is not of tetrahedral type";
            return;
        }
        GeometryMap::setMaster(master);
    }

    void
        VESSTetraTriangleMap::setSlave(std::shared_ptr<Geometry> slave)
    {
        if (slave->getType() != Geometry::Type::SurfaceMesh)
        {
            LOG(WARNING) << "The geometry provided as slave is not of triangular type (surface)";
            return;
        }
        GeometryMap::setSlave(slave);
    }

} // imstk