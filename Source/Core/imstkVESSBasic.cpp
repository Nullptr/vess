/*=========================================================================

Library: iMSTK

Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
& Imaging in Medicine, Rensselaer Polytechnic Institute.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0.txt

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

=========================================================================*/
#include "imstkVESSBasic.h"
#include "imstkMath.h"

namespace imstk
{
    spline::spline(unsigned long inStride, unsigned long inOrder, float* inPointsP) throw() :
        mOrder(inOrder)
    {
        try
        {
            mPointsP.clear();
            for (int i = 0; i < mOrder; i++)
            {
                VertexXZH vertexP;
                vertexP.x = 0.0; vertexP.y = 0.0; vertexP.z = 0.0;
                mPointsP.push_back(vertexP);
            }
            ////mPointsP = std::make_shared<Vertex>([mOrder]); //  std::vector<Vertex>(new Vertex[mOrder]); // shared_array

            //// Copy in the points to our internal array

            //auto& theNextPointP = mPointsP[0];  // Vertex* theNextPointP = mPointsP.get();
            //const auto& theEndPointP= mPointsP[0]; // const Vertex* theEndPointP = theNextPointP + mOrder;

            //inStride -= 3;  // We'll be incrementing for 3 of the stride value
            //// so our stride can be decremented here

            //while (theNextPointP < theEndPointP)
            //{
            //    Vertex& thePoint = *theNextPointP++;

            //    thePoint.x = *inPointsP++;
            //    thePoint.y = *inPointsP++;
            //    thePoint.z = *inPointsP++;

            //    inPointsP += inStride;
            //}
            int num = 0;
            for (int i = 0; i < mOrder; i++)
            {
                auto& thePoint = mPointsP[i];
                thePoint.x = inPointsP[num]; num++;
                thePoint.y = inPointsP[num]; num++;
                thePoint.z = inPointsP[num]; num++;
            }
        }
        catch (double)
        {
            mOrder = 0;
        }
    }

    bool spline::update(unsigned long inStride, unsigned long inOrder, float* inPointsP) throw()
    {
        try
        {
            mPointsP.clear();
            for (int i = 0; i < mOrder; i++)
            {
                VertexXZH vertexP; // = std::make_shared < Vertex > ;
                vertexP.x = 0.0; vertexP.y = 0.0; vertexP.z = 0.0;
                mPointsP.push_back(vertexP);
            }

            //mPointsP = shared_array<Vertex>(new Vertex[mOrder]);

            //// Copy in the points to our internal array

            //Vertex* theNextPointP = mPointsP.get();
            //const Vertex* theEndPointP = theNextPointP + mOrder;

            //inStride -= 3;  // We'll be incrementing for 3 of the stride value
            //// so our stride can be decremented here

            //while (theNextPointP < theEndPointP)
            //{
            //    Vertex& thePoint = *theNextPointP++;

            //    thePoint.x = *inPointsP++;
            //    thePoint.y = *inPointsP++;
            //    thePoint.z = *inPointsP++;

            //    inPointsP += inStride;
            //}
            int num = 0;
            for (int i = 0; i < mOrder; i++)
            {
                auto& thePoint = mPointsP[i];
                thePoint.x = inPointsP[num]; num++;
                thePoint.y = inPointsP[num]; num++;
                thePoint.z = inPointsP[num]; num++;
            }
            return true;
        }
        catch (double)
        {
            mOrder = 0;
            return false;
        }
    }

    void spline::Eval(float f, float* outVertexP) const throw()
    {
        // Determine which control point we're actually talking about

        const float t = f * (mOrder - 1);

        // Get the index of the mPointsP point corresponding
        // to the global time parameter

        if (t <= 0.0)           // Boundary condition
        {
            ::memmove(outVertexP, &(mPointsP[0].x), sizeof(VertexXZH));
        }
        else if (t >= mOrder)   // Boundary condition
        {
            ::memmove(outVertexP, &(mPointsP[mOrder - 1].x), sizeof(VertexXZH));
        }
        else                    // Common condition
        {
            // Start of local time interval.

            const unsigned u0 = unsigned(t);

            // Calc the local u relative to start of interval

            const float u = t - u0;

            // Grab the 4 mPointsP points for the interval 
            // replicating the endpoints as necessary

            const unsigned theIndexAtMinusOne = std::numeric_limits<unsigned>::max();

            VertexXZH cpP[4];
            for (unsigned i = 0; i < 4; i++)
            {
                unsigned index = u0 - 1 + i;
                if (index == theIndexAtMinusOne) // Effectively == -1
                    index = 0;
                else if (index >= mOrder)
                    index = mOrder - 1;
                cpP[i] = mPointsP[index];
            }

            const VertexXZH& cp0 = cpP[0];
            const VertexXZH& cp1 = cpP[1];
            const VertexXZH& cp2 = cpP[2];
            const VertexXZH& cp3 = cpP[3];

            // Evaluate the polynomial at u

            VertexXZH theVertex;

            theVertex.x =
                0.5 * (
                (2 * cp1.x) +
                u * (
                (-cp0.x + cp2.x) +
                u * (
                (2 * cp0.x - 5 * cp1.x + 4 * cp2.x - cp3.x) +
                u * (-cp0.x + 3 * cp1.x - 3 * cp2.x + cp3.x)
                )
                )
                );

            theVertex.y =
                0.5 * (
                (2 * cp1.y) +
                u * (
                (-cp0.y + cp2.y) +
                u * (
                (2 * cp0.y - 5 * cp1.y + 4 * cp2.y - cp3.y) +
                u * (-cp0.y + 3 * cp1.y - 3 * cp2.y + cp3.y)
                )
                )
                );

            theVertex.z =
                0.5 * (
                (2 * cp1.z) +
                u * (
                (-cp0.z + cp2.z) +
                u * (
                (2 * cp0.z - 5 * cp1.z + 4 * cp2.z - cp3.z) +
                u * (-cp0.z + 3 * cp1.z - 3 * cp2.z + cp3.z)
                )
                )
                );

            ::memmove(outVertexP, &(theVertex.x), sizeof(VertexXZH));
        }
    }
}// imstk
