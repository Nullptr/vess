/*=========================================================================

Library: iMSTK

Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
& Imaging in Medicine, Rensselaer Polytechnic Institute.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0.txt

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

=========================================================================*/

#ifndef imstkVESSBasic_h
#define imstkVESSBasic_h

//#include "imstkTetrahedralMesh.h"

///
/// \for Hash
///
#include <unordered_map> // #include <unordered_map>
#include <limits>
#include <math.h>
#include <stdlib.h>
#include <array>
#include <iostream>
#include "imstkMath.h"
#include <chrono>

namespace imstk
{
    /* graph for cutting**/
    // undirected graph for cutting split
#define GRAPHMAPSIZE 1000 // 300 // 70 // volumetric mesh
#define GRAPHMAPSIZEVISUAL 1000 // 100 // visual mesh for cutting
#define SPLITDIST 0.0 // 0.15
    /* END graph for cutting**/

    /**** macro definition ******/
    // hash map
    typedef unsigned __int64	U64;
#define HASHEDGE_SHIFT 32
    //can keep up to 2097151 nodes
#define HASHEDGE_BITMASK 0xFFFFFFFF
#define HASHEDGEID_FROM_IDX(a,b)	(((U64)((b) & HASHEDGE_BITMASK) << HASHEDGE_SHIFT) | ((a) & HASHEDGE_BITMASK))
    // hash map END

    // cutting
#define FLAT_CELL_VOLUME 1e-8  //  org is 1e-4, should set to this value, not smaller, but for debug, set to 1e-5 first
#define MIN_EDGE_LENGTH 1e-8  // the same  1e-4

#define PS_PLUS_INFINITY FLT_MAX
#define PS_MINUS_INFINITY -1.0f*FLT_MAX

#ifndef SAFE_DELETE
#define SAFE_DELETE(p) { if(p) {delete (p); (p) = NULL;}}
#endif

#ifndef SAFE_DELETE_ARRAY
#define SAFE_DELETE_ARRAY(p) { if( (p) ) {delete [] (p); (p) = NULL;}}
#endif

#define PS_UNUSED(arg) (void)arg;

#ifndef COMFORT_TYPES
#define COMFORT_TYPES

    typedef unsigned char		U8;
    typedef unsigned short		U16;
    typedef unsigned int		U32;
    typedef			 char		I8;
    typedef			 short		I16;
    typedef			 int		I32;

#if defined(_MSC_VER)
    typedef unsigned __int64	U64;
    typedef	__int64				I64;
#else
    typedef unsigned long long	U64;
    typedef	long long			I64;
#endif

    //typedef unsigned char	u_char;
    //typedef unsigned short	u_short;
    //typedef unsigned int	u_int;

#endif

    //Constants for conversion
    //const float DEG_TO_RAD = ((2.0f * 3.14152654f) / 360.0f);
    //const float RAD_TO_DEG = (360.0f / (2.0f * 3.141592654f));

    //PI
    const float Pi = 3.14159265358979323846f;

    //PI over 2
    const float PiOver2 = Pi / 2.0f;

    //2PI
    const float TwoPi = Pi * 2.0f;

    // error tolerance for check
    const float EPSILON = 0.0001f;

    //////////////////////////////////////////////////////////////////////////
    // Angle Conversions
#define	DEGTORAD(x)	( ((x) * Pi) / 180.0f )
#define	RADTODEG(x)	( ((x) * 180.0f) / Pi )

#define	SQR(x)		( (x) * (x) )

    //Min and Max
#define MATHMIN(_A_,_B_) ((_A_) < (_B_))?(_A_):(_B_)
#define MATHMAX(_A_,_B_) ((_A_) > (_B_))?(_A_):(_B_)

    //Angle between Vectors
#define VAngleDegree(p,q) ((acos((p).x * (q).x + (p).y * (q).y + (p).z * (q).z) * 180.0f) / Pi)

    // limits a value to low and high
#define LIMIT_RANGE(low, value, high)	{	if (value < low)	value = low;	else if(value > high)	value = high;	}

    // set float to 0 if within tolerance
#define ZERO_CLAMP(x)	((((x) > 0 && ((x) < EPSILON)) || ((x) < 0 && ((x) > -EPSILON)))?0.0f:(x) )

    // mesh entity
#define FACE_SHIFT_A 0
#define FACE_SHIFT_B 21
#define FACE_SHIFT_C 42

#define HEDGE_SHIFT_A 0
#define HEDGE_SHIFT_B 32

#define COUNT_FACE_EDGES 3
#define COUNT_CELL_EDGES 6
#define COUNT_CELL_FACES 4
#define COUNT_CELL_NODES 4

    //can keep up to 2097151 nodes
#define FACE_BITMASK 0x001FFFFF
#define HEDGE_BITMASK 0xFFFFFFFF

#define FACEID_HASHSIZE (U64)(1<<(3*FACE_SHIFT_B))
#define FACEID_FROM_IDX(a,b,c) (((U64) ((c) & FACE_BITMASK) << FACE_SHIFT_C) | ((U64) ((b) & FACE_BITMASK) << FACE_SHIFT_B) | ((a) & FACE_BITMASK))

#define HEDGEID_HASHSIZE	(U64)(1<<(HEDGE_SHIFT_B))
#define HEDGEID_FROM_IDX(a,b)	(((U64)((b) & HEDGE_BITMASK) << HEDGE_SHIFT_B) | ((a) & HEDGE_BITMASK))

    // cuttable mesh CuttingVESSCutMesh
#define CUT_ERR_INVALID_INPUT_ARG -1
#define CUT_ERR_NO_INTERSECTION -2
#define CUT_ERR_UNHANDLED_CUT_STATE -3
#define CUT_ERR_UNABLE_TO_CUT_EDGE -4
#define CUT_ERR_USER_CANCELLED_CUT -5

#define DEFAULT_MESH_SPLIT_DIST 0.2 //  0.1

#define SMALL_NUM 1e-8  // anything that avoids division overflow

    // VolMesh CuttingVESSTetMesh
    static const U32 INVALID_INDEX = -1;
    enum TopologyEvent { teAdded, teRemoved, teUpdated };

    enum ErrorCodes {
        err_op_failed = -1,
        err_elem_not_found = -2,
        err_face_not_found = -3,
        err_edge_not_found = -4,
        err_node_not_found = -5,
    };

    // cutting END

      ////< XZH self-generated volumetric mesh


      ////< XZH END
    //Base link
    class BaseLink
    {
    public:
        static const U32 INVALID = -1;
        explicit BaseLink(U32 idx) { m_idx = idx; }
        explicit BaseLink(const BaseLink& other) { m_idx = other.m_idx; }


        bool isValid() const { return (m_idx != INVALID); }

        //ops
        BaseLink& operator=(const BaseLink& other)
        {
            this->m_idx = other.m_idx;
            return (*this);
        }

        BaseLink& operator=(U32 idx)
        {
            this->m_idx = idx;
            return (*this);
        }

        bool operator<(const BaseLink& other) const { return this->m_idx < other.m_idx; }
        bool operator>(const BaseLink& other) const { return this->m_idx > other.m_idx; }
        bool operator==(const BaseLink& other) const { return this->m_idx == other.m_idx; }
        bool operator<(U32 idx) const { return this->m_idx < idx; }
        bool operator>(U32 idx) const { return this->m_idx > idx; }
        bool operator==(U32 idx) const { return this->m_idx == idx; }

        inline operator U32() const { return m_idx; }

    protected:
        U32 m_idx;
    };

    //links for all entities
    class NodeLink : public BaseLink
    {
        NodeLink(U32 idx = INVALID) : BaseLink(idx){}
    public:
        static NodeLink create(U32 idx) { return NodeLink(idx); }
    };

    class EdgeLink : public BaseLink
    {
        EdgeLink(U32 idx = INVALID) : BaseLink(idx){}
    public:
        static EdgeLink create(U32 idx) { return EdgeLink(idx); }
    };

    class FaceLink : public BaseLink
    {
        FaceLink(U32 idx = INVALID) : BaseLink(idx){}
    public:
        static FaceLink create(U32 idx) { return FaceLink(idx); }
    };

    class CellLink : public BaseLink
    {
        CellLink(U32 idx = INVALID) : BaseLink(idx){}
    public:
        static CellLink create(U32 idx) { return CellLink(idx); }
    };

    //vertices
    struct NODE
    {
        //public:
        Vec3d pos;
        Vec3d restpos;

        NODE& operator = (const NODE& A) {
            pos = A.pos;
            restpos = A.restpos;
            return (*this);
        }
    };

    //Key to access edges in a unique order
    class EdgeKey
    {
    public:
        U32 min0;
        U32 max0;
        EdgeKey() : key(0) {}
        explicit EdgeKey(U64 k) { this->key = k; }
        explicit EdgeKey(U32 a, U32 b)
        {
            if (a > b)
            {
                min0 = b;
                max0 = a;
            }
            else
            {
                min0 = a;
                max0 = b;
            }
            key = HEDGEID_FROM_IDX(min0, max0);
            //setup(a, b);
        }

        //void setup(U32 a, U32 b) 
        //{
        //    if (a > b)
        //        std::swap(a, b);
        //    key = HEDGEID_FROM_IDX(a, b);
        //}

        bool operator<(const EdgeKey& k) const { return key < k.key; }

        bool operator>(const EdgeKey& k) const { return key > k.key; }

        void operator=(const EdgeKey& other)
        {
            this->key = other.key;
        }

        //bool operator==(const EdgeKey& other) const
        //{
        //    return (this->key == other.key);
        //}

        bool operator==(const EdgeKey& other) const
        {
            return ((this->min0 == other.min0) && (this->max0 == other.max0));
        }

        //U64 operator() () const
        //{
        //    return HEDGEID_FROM_IDX(min0, max0);
        //}

        U64 key;
    };

    //edge
    class EDGE
    {
    public:
        U32 from, to;

        EDGE() { init(); }
        EDGE(U32 from_, U32 to_)
        {
            setup(from_, to_);
        }

        void init()
        {
            from = to = BaseLink::INVALID;
        }

        void setup(U32 from_, U32 to_)
        {
            from = from_;
            to = to_;
        }

        EDGE& operator =(const EDGE& A)
        {
            from = A.from;
            to = A.to;
            return (*this);
        }
    };

    //face
    class FACE
    {
    public:
        U32 edges[COUNT_FACE_EDGES];

        FACE()
        {
            init();
        }

        void init()
        {
            for (int i = 0; i < COUNT_FACE_EDGES; i++)
                edges[i] = BaseLink::INVALID;
        }

        FACE& operator = (const FACE& A)
        {
            for (int i = 0; i < COUNT_FACE_EDGES; i++)
                edges[i] = A.edges[i];
            return (*this);
        }
    };

    //Key to access faces in a unique order
    class FaceKey
    {
    public:
        FaceKey() :m_key(0) {}
        explicit FaceKey(U64 k) { this->m_key = k; }
        explicit FaceKey(U32 n[3])
        {
            setup(n[0], n[1], n[2]);
        }

        explicit FaceKey(U32 a, U32 b, U32 c)
        {
            setup(a, b, c);
        }

        void setup(U32 a, U32 b, U32 c)
        {
            order_lo2hi(a, b, c);
            m_key = FACEID_FROM_IDX(a, b, c);
        }

        static void order_lo2hi(U32& a, U32& b, U32& c)
        {
            if (a > b)
                std::swap(a, b);
            if (b > c)
                std::swap(b, c);
            if (a > b)
                std::swap(a, b);
        }

        U64 key() const { return m_key; }

        bool operator<(const FaceKey& k) const { return m_key < k.m_key; }

        bool operator>(const FaceKey& k) const { return m_key > k.m_key; }

        void operator=(const FaceKey& other)
        {
            this->m_key = other.m_key;
        }

        bool operator==(const FaceKey& other)
        {
            return (this->m_key == other.m_key);
        }

    private:
        U64 m_key;
    };



    //elements
    struct CELL
    {
        //public:
        U32 nodes[COUNT_CELL_NODES];
        U32 faces[COUNT_CELL_FACES];
        U32 edges[COUNT_CELL_EDGES];

        CELL()
        {
            init();
        }

        void init()
        {
            for (int i = 0; i < 4; i++)
            {
                nodes[i] = BaseLink::INVALID;
                faces[i] = BaseLink::INVALID;
            }

            for (int i = 0; i < 6; i++)
                edges[i] = BaseLink::INVALID;
        }

        CELL& operator = (const CELL& A)
        {
            for (int i = 0; i < 4; i++)
            {
                nodes[i] = A.nodes[i];
                faces[i] = A.faces[i];
            }

            for (int i = 0; i < 6; i++)
                edges[i] = A.edges[i];

            return (*this);
        }
    };
      ////< XZH DataStructure

      ////< XZH DataStructure END

    /******macro definition END*****/

    ////< XZH idx is visual vertex to tet node
      ////< XZH visual mesh with texture .obj, its mapping triangle in the tet's surface mesh
    struct tex2TetSurfTriLink
    {
        int tetSurfTriIndex;
    };
    ////< XZH visual mesh with texture .obj, its mapping triangle in the tet's surface mesh
    struct tex2TetSurfVertLink
    {
        int tetSurfVertIndex;
    };

    struct physXLink
    {
        int tetraIndex;
        float baryCetricDistance[4];
    };

      ////< XZH idx is visual vertex to tet node
    struct physXVertTetLink
    {
        int tetraNodeIndex;
    };

      ////< XZH visual triangle belong to tetrahedron 0 outside, 1 inside
    struct physXTriTetLink
    {
        int tetraIndex;
        int inside;
    };

      ////< XZH tet index including those visual tri, if not exist, set -1
    struct physXTetTriLink
    {
        int tri0;
        int in0;
        int tri1;
        int in1;
        int tri2;
        int in2;
        int tri3;
        int in3;
    };

    using EdgeArray = std::array<size_t, 2>;

    // record each collision detection edge w.r.t. tetrahedrons, surface mesh inside colon
    //struct cdEdgeSurfTris
    //{
    //    U64 key;  // hash coding value combining two node number, from small to big
    //    int nodeIdx[2]; // one edge has two vertices
    //    int tetraIndex; // 
    //    int surfaceIdxInside; // the order in m_tetrahedraSurfaceVertices
    //};
    

    struct physEdge
    {
        int nodeId[2];
        float L0; // original length of spring

        bool colDetOn;// if collision detection is enabled
        bool closeTool;// springs close to tool [tools][cylinder]   
    };

    struct physSurfTris
    {
        int nodeIdx[3];
        int edgeIdx[3];

        int tetraIdx;// the tetrahedron the triangle belongs to

        bool colDetOn;// if collision detection is enabled
        bool closeTool;// close to tool [tools][cylinder] 
    };

    struct physVertex
    {
        int globalIdx;
        //****************//
        // state variables//
        //****************//
        Vec3d oPos = Vec3d::Zero();// original location of node
        Vec3d pos = Vec3d::Zero();// current location of node  
        Vec3d p = Vec3d::Zero();// temp location of node within timestep, will be assigned to pos at the end of time step
        Vec3d pRot = Vec3d::Zero();// rotated non-deformed location of node

        Vec3d vel = Vec3d::Zero();// velocity of node
        Vec3d force = Vec3d::Zero();// external force applied on node

        //****************//
        //  geometry      //
        //****************//
        int nbrNeiSurfEdge;
        int neiSurfEdge[100];

        int nbrNeiSurfTri;
        int neiSurfTri[100];	//CAREFUL! Search for 'if (simObj->node[i].nbrNeiSurfTri < 100)' to adjust accordingly
        int tetraIdx;// tetrahedron the node belongs to   

        //****************//
        //  flags         //
        //****************//
        bool fixed;// node is fixed
        bool onSurf;// node is on surface
		bool onSurfSurrounding; // only used for surrounding tip of the tool's collision 10/06/2019
        bool colDetOn;// if collision detection is enabled
        bool contact;// node is contacted
        bool firstPart;// node belongs to part1 of the mesh
        bool touched;// noce can be touched by tool
		bool contactXZH=false; // for extra surrounding CD

        bool closeTool;	// node is close to the tool [tools][cylinder]   

		bool bInject=false;   ////< XZH node can be injected, which belong to the fixed layer tetrahedrons, and not on the outside surface of this layer
		bool bDissect = false;   ////< XZH, used for those fixed nodes for deformation, if is not dissected, should be fixed node can be dissected if it is belongs to the tets that can be dissected on 12/05/2019
	};

	struct injectSurfTris
	{
		int nodeIdx[3];
		int tetraIdx1; // the tetrahedron the triangle belongs to
		int tetraIdx2;
		bool injectOn;  // if inject is enabled
		bool closeInject;  // close to inject  
	};

	struct continuousCollisionPairXZH 
	{
		int type; // 0: line-line, 1: vertex-triangle

		// tool
		int toolID;
		int toolTriID;

		//object (tool trialge - node)
		int nodeIdx;
		double u, v;

		Vec3d colNorm;

		int colPointNodeID;

		double colTime;
	};

    struct continuousCollisionPair
    {
        int type; // 0: line-line, 1: vertex-triangle

        //tool
        int toolID;
        int toolSegID;
        double toolSegPara;// barycentric parameter, to locate the collided point

        //object (line-line) edge-edge
        int edgeIdx;
        double edgePara;// barycentric parameter, to locate the collided point
        int colEdgeNodeID[2];// two ctnColNode on a line segment

        //object (vertex - triangle)
        int triSurfIdx;
        double u, v;// barycentric parameter, to locate the collided point
        int colTriNodeID[3];// three ctnColNode on a triangle

        Vec3d colNorm = Vec3d::Zero();// normal direction of collision plane

        double colTime;// time when collision happens
    };


    struct continuousCollisionNode
    {
        int idx;
        int nbrCol=0;// nbr of collision pairs it is involved into
        double colWeight;// reciprocal of nbrCol

        Vec3d locPos=Vec3d::Zero();// local position wrt tool origin
    };

    struct ToolInfo{

        //****************//
        // state variables//
        //****************//
        bool button[4];// Omni button states

        Vec3d jointPos = Vec3d::Zero();// haptic position at beginning of simulation iteration ÿ�ε�����ʼ����һ��ֵ
        Vec3d pJointPos = Vec3d::Zero();// previous haptic position at beginning of simulation iteration

        Vec3d hapticPos = Vec3d::Zero();// real-time haptic position ���ֵ ���ڸ����߳��и��� 1000HZ
        Vec3d pHapticPos = Vec3d::Zero();//previous real-time haptic position

        Eigen::Matrix3d pRotArr; // float pRotArr[9];// previous haptic orientation at beginning of simulation iteraion
        Eigen::Matrix3d rotArr; //float rotArr[9];// haptic orientation at beginning of simulation iteraion
        Eigen::Matrix3d hapticRot; // float hapticRot[9]; // real-time haptic orientation

        Vec3d pDrawPos = Vec3d::Zero();// previous rendering postion
        Vec3d drawPos = Vec3d::Zero();// temp in-process tool position before final pos is computed  ��ʱ����
        Vec3d ccdPos = Vec3d::Zero();// temp in-process tool position at current time for CCD  ��ʱ
        Vec3d finalPos = Vec3d::Zero();// final rendering position at the end of simulation iteration 
        Vec3d injectPos = Vec3d::Zero(); // rendering position at the injection solution part when pressing the button continuously

        float ccdDistance;// threshold for ccdPos

        //****************//
        // force and coeff//
        //****************//
        Vec3d diffPos = Vec3d::Zero();// difference of finalPos of two simulation iteration
        Vec3d diffPosPre = Vec3d::Zero(); // difference between the finalPos of two simulation iteration

        float springConstant;// spring constant for virtual coupling
        float velocityResistance;// velocity damping for virtual coupling
        float hapticVelResistance;// viscosity damping for haptic device
        Vec3d force = Vec3d::Zero();// raw force, without filtering and other process
        Vec3d curForce = Vec3d::Zero();// current force to be send to haptic device
        Vec3d preForce = Vec3d::Zero();// previous force sended to haptic device
        float forceCoeff;

        //****************//
        // geometry       //
        //****************//
        static const int MAX_NBR_LINES = 8;
        int nbrNode;
        int nbrActSeg;
        int nbrActTip;

		// surrounding by XZH
		int nbrNodeSurrounding;
		int nbrActSegSurrounding;

        int belongToCylinder[4];// indicating which cylinder the line segment belongs to, for collision detection/handling
		int belongToCylinderXZH[15];

        Vec3d oriVec[MAX_NBR_LINES];// original local position of lineSeg nodes, wrt tool origin
        Vec3d locVec[MAX_NBR_LINES];// current local position of lineSeg nodes, wrt tool origin
        Vec3d pLocVec[MAX_NBR_LINES];// previous local position of lineSeg nodes, wrt tool origin

        Vec3d gloVec[MAX_NBR_LINES];// global position wrt tool origin
        Vec3d pGloVec[MAX_NBR_LINES];// previous global position wrt tool origin

        int nodeIdxOfSeg[MAX_NBR_LINES][2];//[lineIndex][nodeIndex]
        int nodeIdxOfTip[MAX_NBR_LINES];//[lineIndex]

		int nodeIdxOfSegSurrounding[MAX_NBR_LINES][2]; // 09/20/2019 using for surrounding 

        int filterSize;
        int numberOfForceInFilter=0;
        std::vector<Vec3d> arrForce;// force filter

        float thickness[MAX_NBR_LINES][2][2];	//[lineIndex] [edge/tip] [CCD, tool correction]
        float cullingThickness[2];// for proximity test

        //****************//
        // simulation     //
        //****************//
        Vec3d startPos = Vec3d::Zero();// position of starting sphere
        bool touchStartSphere;// start sphere touched

        bool nodeInsideCylinder[2];// for cylinder CD
        Vec3d counterForce = Vec3d::Zero();// ????
    };

    /****** Cutting ******/
    // intersection <- imstkCuttingIntersection.h
    //  edge triangle intersection
    inline int IntersectRayTriangle(const Vec3d& ro, const Vec3d& rd, const Vec3d p[3], Vec3d& uvt)
    {
        double epsxzh = EPSILON*0.0001;
        Vec3d e1 = p[1] - p[0];
        Vec3d e2 = p[2] - p[0];
        Vec3d q = rd.cross(e2); // Vec3d::cross(rd, e2);

        //Test Determinant
        double a = e1.dot(q); //  Vec3d::dot(e1, q);
        //if (fabs(a) < epsxzh)
		if (fabs(a) < (epsxzh*0.0001))
            return 0;

        //Test U
        double f = 1.0 / a;
        Vec3d s = ro - p[0];
        uvt.x() = f *(s.dot(q)); // Vec3d::dot(s, q);
        if (uvt.x() < 0.0)
            return 0;

        //Test V
        Vec3d r = s.cross(e1); //  Vec3d::cross(s, e1);
        uvt.y() = f *(rd.dot(r)); // Vec3d::dot(rd, r);
        if ((uvt.y() < 0.0) || ((uvt.x() + uvt.y()) > 1.0))
            return 0;

        //Test T
        uvt.z() = f *(e2.dot(r)); // Vec3d::dot(e2, r);

        return 1;
    }
    inline int IntersectRayTriangleF(const Vec3f& ro, const Vec3f& rd, const Vec3f p[3], Vec3f& uvt)
    {

        Vec3f e1 = p[1] - p[0];
        Vec3f e2 = p[2] - p[0];
        Vec3f q = rd.cross(e2);  // Vec3f::cross(rd, e2);

        //Test Determinant
        float a = e1.dot(q); //  Vec3f::dot(e1, q);
        if (fabs(a) < EPSILON)
            return 0;

        //Test U
        float f = 1.0 / a;
        Vec3f s = ro - p[0];
        uvt.x() = f *(s.dot(q)); // Vec3f::dot(s, q);
        if (uvt.x() < 0.0)
            return 0;

        //Test V
        Vec3f r = s.cross(e1); //  Vec3f::cross(s, e1);
        uvt.y() = f *(rd.dot(r)); // Vec3f::dot(rd, r);
        if ((uvt.y() < 0.0) || ((uvt.x() + uvt.y()) > 1.0))
            return 0;

        //Test T
        uvt.z() = f *(e2.dot(r)); // Vec3f::dot(e2, r);

        return 1;
    }

    inline int IntersectSegmentTriangleF(const Vec3f& s0, const Vec3f& s1, const Vec3f p[3], float& t, Vec3f& uvw, Vec3f& xyz)
    {

        Vec3f delta = s1 - s0;
        Vec3f rd = delta.normalized();
        double deltaLen = delta.norm(); //  delta.length();
        Vec3f uvt;
        int res = IntersectRayTriangleF(s0, rd, p, uvt);
        if (res > 0) {
            if (uvt.z() >= 0.0 && uvt.z() <= deltaLen) {
                //Barycentric coordinate
                uvw = Vec3f(uvt.x(), uvt.y(), 1.0 - uvt.x() - uvt.y());

                //Cartesian Coordinate
                xyz = s0 + rd * uvt.z();

                //return the t distance on the segment
                t = uvt.z();

                return res;
            }
        }
        return 0;
    }

    inline int IntersectSegmentTriangle(const Vec3d& s0, const Vec3d& s1, const Vec3d p[3], double& t, Vec3d& uvw, Vec3d& xyz)
    {

        Vec3d delta = s1 - s0;
        Vec3d rd = delta.normalized();
        double deltaLen = delta.norm(); //  delta.length();
        Vec3d uvt;
        int res = IntersectRayTriangle(s0, rd, p, uvt);
        if (res > 0) 
        {
            if (uvt.z() >= 0.0 && uvt.z() <= deltaLen) 
            {
                //Barycentric coordinate
                uvw = Vec3d(uvt.x(), uvt.y(), 1.0 - uvt.x() - uvt.y());

                //Cartesian Coordinate
                xyz = s0 + rd * uvt.z();

                //return the t distance on the segment
                t = uvt.z();

                return res;
            }
        }
        return 0;
    }

    // contact = the contact point on the plane  https://stackoverflow.com/questions/7168484/3d-line-segment-and-plane-intersection
    //    ray = B - A, simply the line from A to B
    //    rayOrigin = A, the origin of the line segement
    //    normal = normal of the plane(normal of a triangle)
    //    coord = a point on the plane(vertice of a triangle)
    inline bool intersectLinePlane(Vec3d& contact, const Vec3d& ray, const Vec3d& rayOrigin, const Vec3d& normal, const Vec3d& coord)
    {
        // get d value
        float d = normal.dot(coord); //  Dot(normal, coord);

        if (normal.dot(ray)==0)  // (Dot(normal, ray) == 0) 
        {
            return false; // No intersection, the line is parallel to the plane
        }

        // Compute the X value for the directed line ray intersecting the plane
        float x = (d - normal.dot(rayOrigin)) / (normal.dot(ray)); //   // float x = (d - Dot(normal, rayOrigin)) / Dot(normal, ray);

        // output contact point
        contact = rayOrigin + ray.normalized()*x; //  contact = rayOrigin + normalize(ray)*x; //Make sure your ray vector is normalized
        return true;
    }

    // intersect3D_SegmentPlane(): find the 3D intersection of a segment and a plane http://geomalgorithms.com/a05-_intersect-1.html
    //    Input:  S = a segment, and Pn = a plane = {Point V0;  Vector n;}
    //    Output: *I0 = the intersect point (when it exists)
    //    Return: 0 = disjoint (no intersection)
    //            1 =  intersection in the unique point *I0
    //            2 = the  segment lies in the plane
    inline int intersectSegmentPlane(Vec3d& contact, const Vec3d& p0, const Vec3d& p1, const Vec3d& pnorm, const Vec3d& pcoord) // Segment S, Plane Pn, Point* I)
    {
        Vec3d u = p1 - p0; // Vector    u = S.P1 - S.P0;
        Vec3d w = p0 - pcoord; // Vector    w = S.P0 - Pn.V0;

        float     D = pnorm.dot(u); // dot(Pn.n, u);
        float     N = (-pnorm).dot(w); //  -dot(Pn.n, w);

        if (fabs(D) < SMALL_NUM) 
        {           // segment is parallel to plane
            if (N == 0)                      // segment lies in plane
                return 2;
            else
                return 0;                    // no intersection
        }
        // they are not parallel
        // compute intersect param
        float sI = N / D;
        if (sI < 0 || sI > 1)
            return 0;                        // no intersection

        contact = p0 + sI*u; // *I = S.P0 + sI * u;                  // compute segment intersect point
        return 1;
    }

    // https://www.gamedev.net/forums/topic/481835-3d-point-in-triangle-algorithm/
    inline bool insidePointTriangle(const Vec3d& contact_point, const Vec3d& V1, const Vec3d& V2, const Vec3d& V3)
    {
        double accumilator = 0;
        double dot1 = 0;
        double dot2 = 0;
        double dot3 = 0;

        Vec3d line1, line2;

        //first angle
        line1 = (V1 - contact_point).normalized();
        line2 = (V2 - contact_point).normalized();
        dot1 = line1.dot(line2);  
        accumilator += acos(dot1);

        //second angle
        line1 = (V2 - contact_point).normalized();
        line2 = (V3 - contact_point).normalized();
        dot2 = line1.dot(line2);
        accumilator += acos(dot1);

        //third angle
        line1 = (V3 - contact_point).normalized();
        line2 = (V1 - contact_point).normalized();
        dot3 = line1.dot(line2);

        accumilator = acos(dot1) + acos(dot2) + acos(dot3);

        if (accumilator < (2.0*3.1415926)) // 359.9*pirad
            return false;
        else
            return true;
    };

    // https://stackoverflow.com/questions/2752725/finding-whether-a-point-lies-inside-a-rectangle-or-not
    inline bool insidePointRectangle(const Vec3d& contact_point, const Vec3d& A, const Vec3d& B, const Vec3d& C, const Vec3d& D)
    {
        Vec3d AB = B - A; // vector(r.A, r.B);
        Vec3d AM = contact_point - A; // vector(r.A, m);
        Vec3d BC = C - B; // vector(r.B, r.C);
        Vec3d BM = contact_point - B; // vector(r.B, m);
        float dotABAM = AB.dot(AM); //  dot(AB, AM);
        float dotABAB = AB.dot(AB); // dot(AB, AB);
        float dotBCBM = BC.dot(BM); // dot(BC, BM);
        float dotBCBC = BC.dot(BC); // dot(BC, BC);
        return 0 <= dotABAM && dotABAM <= dotABAB && 0 <= dotBCBM && dotBCBM <= dotBCBC;
    }
    //===================================================================
    // intersection END
    //////////////////////////////////////////////////////////////////////////
    inline int Log2f(float x)
    {
        unsigned int ix = static_cast<unsigned int>(x);
        unsigned int exp = (ix >> 23) & 0xFF;
        int log2 = int(exp) - 127;
        return log2;
    }

    inline int Log2i(unsigned int x)
    {
        return Log2f((float)x);
    }

    inline float Lerp(float t, float s1, float s2)
    {
        return (1 - t)*s1 + t*s2;
    }

    template <class T>
    inline void Clamp(T &v, T minVal, T maxVal)
    {
        if (v < minVal)
            v = minVal;
        else if (v > maxVal)
            v = maxVal;
    }

    inline bool FLOAT_EQ(float x, float v)
    {
        return (((v - EPSILON) < x) && (x < (v + EPSILON)));
    }

    inline bool FLOAT_EQ(float x, float v, float epsi)
    {
        return (((v - epsi) < x) && (x < (v + epsi)));
    }

    //Swap 2 floating point numbers
    inline void SWAP(float &x, float &y)
    {
        float temp;
        temp = x;
        x = y;
        y = temp;
    }

    inline void SWAP(int &x, int &y)
    {
        int temp;
        temp = x;
        x = y;
        y = temp;
    }

    //Round for Accuracy. The same method we used by AMin
    // round a float to a specified degree of accuracy
    inline float ROUND(const float value, const int accuracy)
    {
        double integer, fraction;

        fraction = modf(value, &integer);		// get fraction and int components

        return (float(integer + (float(int(fraction*pow(float(10), float(accuracy))))) / pow(float(10), float(accuracy))));
    }

    // If num is less than zero, we want to return the absolute value of num.
    // This is simple, either we times num by -1 or subtract it from 0.
    inline double Absoluted(double num)
    {
        if (num < 0)
            return (0 - num);

        // Return the original number because it was already positive
        return num;
    }

    inline float Absolutef(float num)
    {
        if (num < 0)
            return (0 - num);

        // Return the original number because it was already positive
        return num;
    }

    inline float maxf(float a, float b)
    {
        return (a>b) ? a : b;
    }

    inline float minf(float a, float b)
    {
        return (a<b) ? a : b;
    }

    template <class T>
    inline T gmax(T a, T b)
    {
        return (a > b) ? a : b;
    }

    template <class T>
    inline T gmin(T a, T b)
    {
        return (a < b) ? a : b;
    }

#ifdef max
#undef max
#endif // #ifdef min
    template <typename T>
    T GetMaxLimit(void)
    {
        return std::numeric_limits<T>::max();
    }

#ifdef min
#undef min
#endif // #ifdef min
    template <typename T>
    T GetMinLimit(void)
    {
        return std::numeric_limits<T>::min();
    }

    //Fast SquareRoot Found on internet http://www.codemaestro.com/reviews/9
    /*
    inline float FastSqrt(float number)
    {
    long i;
    float x, y;
    const float f = 1.5F;

    x = number * 0.5F;
    y  = number;
    i  = * (reinterpret_cast<long *>(&y));
    i  = 0x5f3759df - ( i >> 1 );
    y  = * ( float * ) &i;
    y  = y * ( f - ( x * y * y ) );
    y  = y * ( f - ( x * y * y ) );
    return number * y;
    }

    inline float FastInvSqrt(float x)
    {
    float xhalf = 0.5f*x;
    int i = *(int*)&x; // get bits for floating value
    i = 0x5f375a86- (i>>1); // gives initial guess y0
    x = *(float*)&i; // convert bits back to float
    x = x*(1.5f-xhalf*x*x); // Newton step, repeating increases accuracy
    return x;
    }
    */

    template <typename T>
    inline T RandRangeT(T nMin, T nMax)
    {
        // random double in range 0.0 to 1.0 (non inclusive)
        T r = static_cast<T>(rand()) / (static_cast<T>(RAND_MAX)+1);
        // transform to wanted range
        return nMin + r*(nMax - nMin);
    }

    //Floating Point Comparison
    inline bool ApproximatelyEqual(float a, float b, float epsilon)
    {
        return fabs(a - b) <= ((fabs(a) < fabs(b) ? fabs(b) : fabs(a)) * epsilon);
    }

    inline bool EssentiallyEqual(float a, float b, float epsilon)
    {
        return fabs(a - b) <= ((fabs(a) > fabs(b) ? fabs(b) : fabs(a)) * epsilon);
    }

    inline bool EssentiallyEquald(double a, double b, double epsilon)
    {
        return fabs(a - b) <= ((fabs(a) > fabs(b) ? fabs(b) : fabs(a)) * epsilon);
    }

    inline bool DefinitelyGreaterThan(float a, float b, float epsilon)
    {
        return (a - b) > ((fabs(a) < fabs(b) ? fabs(b) : fabs(a)) * epsilon);
    }

    inline bool DefinitelyLessThan(float a, float b, float epsilon)
    {
        return (b - a) > ((fabs(a) < fabs(b) ? fabs(b) : fabs(a)) * epsilon);
    }

    inline float MAXF(float a, float b)
    {
        if (DefinitelyGreaterThan(a, b, EPSILON))
            return a;
        else
            return b;
    }

    inline float MINF(float a, float b)
    {
        if (DefinitelyLessThan(a, b, EPSILON))
            return a;
        else
            return b;
    }
    /******* Cutting END*******/

    /**** Intersection******/
    ///
    /// \brief Do ranges [a,b] and [c,d] intersect?
    ///
    inline bool testIsIntersect(const double& a, const double& b, const double& c, const double& d)
    {
        return ((a <= d && a >= c) || (c <= b && c >= a)) ? true : false;
    }
    ///
    /// \brief Check if triangle and tri are intersecting with AABB test
    ///
    inline bool testTrisAABB(const double& min1_x, const double& max1_x,
        const double& min1_y, const double& max1_y,
        const double& min1_z, const double& max1_z,
        const double& min2_x, const double& max2_x,
        const double& min2_y, const double& max2_y,
        const double& min2_z, const double& max2_z)
    {
        return (testIsIntersect(min1_x, max1_x, min2_x, max2_x) &&
            testIsIntersect(min1_y, max1_y, min2_y, max2_y) &&
            testIsIntersect(min1_z, max1_z, min2_z, max2_z));
    }
    /**** Intersection END******/

    /******* Generating surface mesh vertex index *********/
    // Edge
    class EdgeXZH
    {
    public:
        using VertexType = Vec3d;

        EdgeXZH(const VertexType &p1, const VertexType &p2) : p1(p1), p2(p2) {};
        EdgeXZH(const EdgeXZH &e) : p1(e.p1), p2(e.p2) {};

        VertexType p1;
        VertexType p2;
    };

    inline std::ostream &operator << (std::ostream &str, EdgeXZH const &e)
    {
        return str << "EdgeXZH " << e.p1 << ", " << e.p2;
    }

    inline bool operator == (const EdgeXZH & e1, const EdgeXZH & e2)
    {
        return 	(e1.p1 == e2.p1 && e1.p2 == e2.p2) ||
            (e1.p1 == e2.p2 && e1.p2 == e2.p1);
    }

    // Triangle
    class Triangle
    {
    public:
        using EdgeType = EdgeXZH;
        using VertexType = Vec3d;

        Triangle(const VertexType &_p1, const VertexType &_p2, const VertexType &_p3)
            : p1(_p1), p2(_p2), p3(_p3),
            e1(_p1, _p2), e2(_p2, _p3), e3(_p3, _p1)
        {}

        bool containsVertex(const VertexType &v)
        {
            return p1 == v || p2 == v || p3 == v;
        }
        // https://www.zhihu.com/question/40422123
        // http://blog.csdn.net/yanmy2012/article/details/8111600
        // https://stackoverflow.com/questions/4103405/what-is-the-algorithm-for-finding-the-center-of-a-circle-from-three-points
        bool circumCircleContains(const VertexType &v)
        {
            double a1, b1, c1, d1;
            double a2, b2, c2, d2;
            double a3, b3, c3, d3; 

            double x1 = p1.x(), y1 = p1.y(), z1 = p1.z();
            double x2 = p2.x(), y2 = p2.y(), z2 = p2.z();
            double x3 = p3.x(), y3 = p3.y(), z3 = p3.z();

                a1 = (y1*z2 - y2*z1 - y1*z3 + y3*z1 + y2*z3 - y3*z2);
            b1 = -(x1*z2 - x2*z1 - x1*z3 + x3*z1 + x2*z3 - x3*z2);
            c1 = (x1*y2 - x2*y1 - x1*y3 + x3*y1 + x2*y3 - x3*y2);
            d1 = -(x1*y2*z3 - x1*y3*z2 - x2*y1*z3 + x2*y3*z1 + x3*y1*z2 - x3*y2*z1);

            a2 = 2 * (x2 - x1);
            b2 = 2 * (y2 - y1);
            c2 = 2 * (z2 - z1);
            d2 = x1 * x1 + y1 * y1 + z1 * z1 - x2 * x2 - y2 * y2 - z2 * z2;

            a3 = 2 * (x3 - x1);
            b3 = 2 * (y3 - y1);
            c3 = 2 * (z3 - z1);
            d3 = x1 * x1 + y1 * y1 + z1 * z1 - x3 * x3 - y3 * y3 - z3 * z3;

            double centerpoint[3];
            centerpoint[0] = -(b1*c2*d3 - b1*c3*d2 - b2*c1*d3 + b2*c3*d1 + b3*c1*d2 - b3*c2*d1)
                / (a1*b2*c3 - a1*b3*c2 - a2*b1*c3 + a2*b3*c1 + a3*b1*c2 - a3*b2*c1);
            centerpoint[1] = (a1*c2*d3 - a1*c3*d2 - a2*c1*d3 + a2*c3*d1 + a3*c1*d2 - a3*c2*d1)
                / (a1*b2*c3 - a1*b3*c2 - a2*b1*c3 + a2*b3*c1 + a3*b1*c2 - a3*b2*c1);
            centerpoint[2] = -(a1*b2*d3 - a1*b3*d2 - a2*b1*d3 + a2*b3*d1 + a3*b1*d2 - a3*b2*d1)
                / (a1*b2*c3 - a1*b3*c2 - a2*b1*c3 + a2*b3*c1 + a3*b1*c2 - a3*b2*c1);

            float circum_radius = sqrtf(((p1.x() - centerpoint[0]) * (p1.x() - centerpoint[0])) + ((p1.y() - centerpoint[1]) * (p1.y() - centerpoint[1])) + ((p1.z() - centerpoint[2]) * (p1.z() - centerpoint[2])));

            float dist = sqrtf(((v.x() - centerpoint[0]) * (v.x() - centerpoint[0])) + ((v.y() - centerpoint[1]) * (v.y() - centerpoint[1])) + ((v.z() - centerpoint[2]) * (v.z() - centerpoint[2])));
            return dist <= circum_radius;

            //float ab = (p1.x() * p1.x()) + (p1.y() * p1.y()) + (p1.z() * p1.z());
            //float cd = (p2.x() * p2.x()) + (p2.y() * p2.y()) + (p2.z() * p2.z());
            //float ef = (p3.x() * p3.x()) + (p3.y() * p3.y()) + (p3.z() * p3.z());

            //float circum_x = (ab * (p3.y() - p2.y()) + cd * (p1.y() - p3.y()) + ef * (p2.y() - p1.y())) / (p1.x() * (p3.y() - p2.y()) + p2.x() * (p1.y() - p3.y()) + p3.x() * (p2.y() - p1.y())) / 2.f;
            //float circum_y = (ab * (p3.x() - p2.x()) + cd * (p1.x() - p3.x()) + ef * (p2.x() - p1.x())) / (p1.y() * (p3.x() - p2.x()) + p2.y() * (p1.x() - p3.x()) + p3.y() * (p2.x() - p1.x())) / 2.f;
            //float circum_radius = sqrtf(((p1.x() - circum_x) * (p1.x() - circum_x)) + ((p1.y() - circum_y) * (p1.y() - circum_y)));

            //float dist = sqrtf(((v.x() - circum_x) * (v.x() - circum_x)) + ((v.y() - circum_y) * (v.y() - circum_y)));
            //return dist <= circum_radius;
        }

        VertexType p1;
        VertexType p2;
        VertexType p3;
        EdgeType e1;
        EdgeType e2;
        EdgeType e3;
    };

    inline std::ostream &operator << (std::ostream &str, const Triangle & t)
    {
        return str << "Triangle:" << std::endl << "\t" << t.p1 << std::endl << "\t" << t.p2 << std::endl << "\t" << t.p3 << std::endl << "\t" << t.e1 << std::endl << "\t" << t.e2 << std::endl << "\t" << t.e3 << std::endl;

    }

    inline bool operator == (const Triangle &t1, const Triangle &t2)
    {
        return	(t1.p1 == t2.p1 || t1.p1 == t2.p2 || t1.p1 == t2.p3) &&
            (t1.p2 == t2.p1 || t1.p2 == t2.p2 || t1.p2 == t2.p3) &&
            (t1.p3 == t2.p1 || t1.p3 == t2.p2 || t1.p3 == t2.p3);
    }

    // Delaunay triangulation
    class Delaunay
    {
    public:
        using TriangleType = Triangle;
        using EdgeType = EdgeXZH;
        using VertexType = Vec3d;

        const std::vector<TriangleType>& triangulate(std::vector<VertexType> &vertices)
        {
            // Store the vertices localy
            _vertices = vertices;

            double minX=0.0, minY=0.0, minZ=0.0;

            // Determinate the super triangle
            if (vertices.size() >0)
            {
                minX = vertices[0].x();
                minY = vertices[0].y();
                minZ = vertices[0].z();
            }                 
            double maxX = minX;
            double maxY = minY;
            double maxZ = minZ;

            for (std::size_t i = 0; i < vertices.size(); ++i)
            {
                if (vertices[i].x() < minX) minX = vertices[i].x();
                if (vertices[i].y() < minY) minY = vertices[i].y();
                if (vertices[i].z() < minZ) minZ = vertices[i].z();
                if (vertices[i].x() > maxX) maxX = vertices[i].x();
                if (vertices[i].y() > maxY) maxY = vertices[i].y();
                if (vertices[i].z() > maxZ) maxZ = vertices[i].z();
            }

            double dx = maxX - minX;
            double dy = maxY - minY;
            double dz = maxZ - minZ;
            double deltaMax = std::max(dx, dy);
            deltaMax = std::max(deltaMax, dz);
            double midx = (minX + maxX) / 2.f;
            double midy = (minY + maxY) / 2.f;
            double midz = (minZ + maxZ) / 2.f;

            VertexType p1(midx - 20 * deltaMax, midy - deltaMax, midz - 20 * deltaMax);
            VertexType p2(midx, midy + 20 * deltaMax, midz);
            VertexType p3(midx + 20 * deltaMax, midy - deltaMax, midz + 20 * deltaMax);

            //std::cout << "Super triangle " << std::endl << Triangle(p1, p2, p3) << std::endl;

            // Create a list of triangles, and add the supertriangle in it
            _triangles.push_back(TriangleType(p1, p2, p3));

            for (auto p = begin(vertices); p != end(vertices); p++)
            {
                //std::cout << "Traitement du point " << *p << std::endl;
                //std::cout << "_triangles contains " << _triangles.size() << " elements" << std::endl;	

                std::vector<TriangleType> badTriangles;
                std::vector<EdgeType> polygon;

                for (auto t = begin(_triangles); t != end(_triangles); t++)
                {
                    //std::cout << "Processing " << std::endl << *t << std::endl;

                    if (t->circumCircleContains(*p))
                    {
                        //std::cout << "Pushing bad triangle " << *t << std::endl;
                        badTriangles.push_back(*t);
                        polygon.push_back(t->e1);
                        polygon.push_back(t->e2);
                        polygon.push_back(t->e3);
                    }
                    else
                    {
                        //std::cout << " does not contains " << *p << " in his circum center" << std::endl;
                    }
                }

                _triangles.erase(std::remove_if(begin(_triangles), end(_triangles), [badTriangles](TriangleType &t)
                {
                    for (auto bt = begin(badTriangles); bt != end(badTriangles); bt++)
                    {
                        if (*bt == t)
                        {
                            //std::cout << "Removing bad triangle " << std::endl << *bt << " from _triangles" << std::endl;
                            return true;
                        }
                    }
                    return false;
                }), end(_triangles));

                std::vector<EdgeType> badEdges;
                for (auto e1 = begin(polygon); e1 != end(polygon); e1++)
                {
                    for (auto e2 = begin(polygon); e2 != end(polygon); e2++)
                    {
                        if (e1 == e2)
                            continue;

                        if (*e1 == *e2)
                        {
                            badEdges.push_back(*e1);
                            badEdges.push_back(*e2);
                        }
                    }
                }

                polygon.erase(std::remove_if(begin(polygon), end(polygon), [badEdges](EdgeType &e)
                {
                    for (auto it = begin(badEdges); it != end(badEdges); it++)
                    {
                        if (*it == e)
                            return true;
                    }
                    return false;
                }), end(polygon));

                for (auto e = begin(polygon); e != end(polygon); e++)
                    _triangles.push_back(TriangleType(e->p1, e->p2, *p));

            }

            _triangles.erase(std::remove_if(begin(_triangles), end(_triangles), [p1, p2, p3](TriangleType &t)
            {
                return t.containsVertex(p1) || t.containsVertex(p2) || t.containsVertex(p3);
            }), end(_triangles));

            for (auto t = begin(_triangles); t != end(_triangles); t++)
            {
                _edges.push_back(t->e1);
                _edges.push_back(t->e2);
                _edges.push_back(t->e3);
            }

            return _triangles;
        }

        const std::vector<TriangleType>& getTriangles() const { return _triangles; };
        const std::vector<EdgeType>& getEdges() const { return _edges; };
        const std::vector<VertexType>& getVertices() const { return _vertices; };

    private:
        std::vector<TriangleType> _triangles;
        std::vector<EdgeType> _edges;
        std::vector<VertexType> _vertices;
    };

    /****** Graph***********/
    // Undirected Graph  cycle determination in the undir-Graph
    class UndirectedGraph
    {
    public:
        UndirectedGraph(std::vector<std::vector<int>>& graph)
        {
            udGraph = graph;
            initGraph();
        }
        void updateGraph(std::vector<std::vector<int>>& graph)
        {
            udGraph = graph;
        }
        // dfs for cycle
        void depthFirstSearch(int v, int prev) // dfs std::vector<std::vector<int>> graph, 
        {
            vecMarked[v] = true;
            std::vector<int>& vecAdj = udGraph[v];
            //for (int w = 0; w < vecAdj.size(); w++)
            for (int w = 0; w < graphSize; w++)
            {
                if (vecAdj[w]) // true , connect in the adj
                {
                    if (!vecMarked[w])  // not accessed
                    {
                        depthFirstSearch(w, v);
                        printf("%d--", v);
                    }
                    else if (w != prev) // accessed, and not the pre v it is a cycle
                    {
                        hasCycle = true;
                        printf("ED:%d\n", w);
                    }
                }
            }
        }    
        // dfs for connection
        void traverseGraph()
        {
            initGraph();
            for (int i = 0; i < graphSize;i++)  // traverse all nodes
            {
                if (!vecMarked[i])
                {
                    depthFirstSearch(i);
                    count++;
                }
            }
        }
        void depthFirstSearch(int v)
        {
            vecMarked[v] = true;
            vecID[v] = count;
            std::vector<int>& vecAdj = udGraph[v];
            for (int w = 0; w < graphSize; w++)
            {
                if (vecAdj[w]) // true , connect in the adj
                {
                    if (!vecMarked[w])
                    {
                        depthFirstSearch(w);
                    }
                }
            }

            //printf("compute completed\n");
        }

        void initGraph()
        {
            vecMarked.clear();
            vecID.clear();
            for (int i = 0; i < udGraph.size(); i++)
            {
                vecMarked.push_back(false);
                vecID.push_back(-1);
            }

            cycleElem.clear();
            cycleElemNum = 0;  //???
        }

        const bool& getHasCycle() const { return hasCycle; }
        bool getConnected(int v, int w) { return (vecID[v] == vecID[w]); }
        bool getDissected(int v, std::vector<int>& wlist) 
        { 
            if (wlist.size() < 1) return false;
            bool isDissected = true;
            for (int i = 0; i < wlist.size();i++)
            {
                int w = wlist[i];
                bool flagDissected = false;
                if (vecID[v] == vecID[w]) flagDissected = false;
                else flagDissected = true;
                isDissected = isDissected&&flagDissected;
            }
            return isDissected;
        }
        int& getID(int v) { return vecID[v]; }
        std::vector<int>& getIDs() { return vecID; }
        int& getCount() { return count; }
        void setGraphSize(int gsize) { graphSize = gsize; }
        int& getCycleElemNum() { return cycleElemNum; }
        std::vector<int>& getCycleElem() { return cycleElem; }

        // new method
        void dfsVisit(int node, std::vector<int>&visit, std::vector<int>&father) // void dfsVisit(std::vector<std::vector<int>>& graph, int node, std::vector<int>&visit, std::vector<int>&father)
        {
            int n = graphSize;
            visit[node] = 1;
            //cout<<node<<"-\n";
            for (int i = 0; i < n; i++)
            {
                if (i != node && udGraph[node][i] != 0)
                {
                    if (visit[i] == 1 && i != father[node]) //find a cycle
                    {
                        int tmp = node;
                        std::cout << "cycle: ";
                        std::vector<int> cycleElemTmp;
                        while (tmp != i)
                        {
                            std::cout << tmp << "->";
                            tmp = father[tmp];
                            cycleElemTmp.push_back(tmp);
                        }
                        std::cout << tmp << std::endl;
                        cycleElemTmp.push_back(tmp);
                        if (cycleElemTmp.size()>cycleElemNum) // just get the biggest cycle
                        {
                            cycleElemNum = cycleElemTmp.size();
                            cycleElem = cycleElemTmp;
                        }
                    }
                    else if (visit[i] == 0)
                    {
                        father[i] = node;
                        dfsVisit(i, visit, father);
                    }
                }
            }
            visit[node] = 2;
        }

        void dfs() // void dfs(std::vector<std::vector<int>>& graph)
        {
            int n = graphSize;
            std::vector<int> visit(n, 0); //visit -> follow  <<Introduction to Algorithms>>S22.3 white grey and black 3 states 
            std::vector<int> father(n, -1); // father[i]  record processing i's father 
            for (int i = 0; i < n; i++)
            {
                if (visit[i] == 0)
                {
                    dfsVisit(i, visit, father);
                }
            }
        }

        // finding out all cyclyes # G: graph G   # n: number of nodes
        int findAllCircles(int n)
        {
            int cnt = 0;
            for (int i = 3; i < (n+1);i++) // find all circles of length i
            {
                cnt += findCircleLength(n, i);
            }
            return cnt;
        }
        // circle lenght # G: graph G        # n: number of nodes        # length : length of circles
        int findCircleLength(int n, int length)
        {
            int cnt = 0;          
            for (int i = 0; i < (n - length + 1); i++) // find all circles starts with i
            {
                std::vector<int> path;
                path.push_back(i);
                cnt += findCircleStartWith(length, path);
            }
            return cnt;
        }
        // find starts with # G: graph G        # length: length of circles        # path : circles starts with nodes in path
        int findCircleStartWith(int length, std::vector<int>& path)
        {
            int l = path.size();
            int last = path[path.size() - 1];
            int cnt = 0;

            std::vector<int>& vecAdj = udGraph[last]; 
            if (l == (length - 1)) // choose the final node in the circle
            {
                for (int i = 0; i < graphSize; i++)
                {
                    if (vecAdj[i]) // true , connect in the adj  for i in G[last] choose internal nodes in the circle
                    {
                        std::vector<int>& vecAdjGi = udGraph[i]; // G[i]
                        bool flag0 = false;
                        for (int k = 0; k<path.size();k++)
                        {
                            if (i == path[k]) { flag0 = true; break; }
                        }

                        bool flag1 = false;
                        int pt0 = path[0];
                        for (int g = 0; g<vecAdjGi.size(); g++)
                        {
                            if (vecAdjGi[g]) // g in G[i]
                            {
                                if (pt0 == g) { flag1 = true; break; }
                            }
                        }

                        if ( (i>path[1])&&(!flag0)&&(flag1) )
                        {
                            path.push_back(i);
                            cnt += 1;
                            printf("Cycle:");
                            std::vector<int> cycleElemTmp;           
                            for (int t = 0; t < path.size(); t++) 
                            { 
                                cycleElemTmp.push_back(path[t]);
                                printf("%d>>", path[t]); 
                            }
                            if (cycleElemTmp.size() > cycleElemNum) // just get the biggest cycle
                            {
                                cycleElemNum = cycleElemTmp.size();
                                cycleElem = cycleElemTmp;
                            }
                            printf("\n");
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < graphSize; i++)
                {
                    if (vecAdj[i]) // true , connect in the adj  for i in G[last] choose internal nodes in the circle
                    {
                        bool flag = false;
                        for (int k = 0; k<path.size(); k++)
                        {
                            if (i == path[k]) { flag = true; break; }
                        }

                        if ((i>path[0]) && (!flag))
                        {
                            path.push_back(i);
                            cnt += findCircleStartWith(length, path);
                        }
                    }
                }
            }
            return cnt;
        }

    private:
        std::vector<bool> vecMarked; // whether the node is accessed
        // for visual
        bool hasCycle = false;
        std::vector<std::vector<int>> udGraph;
        int graphSize = GRAPHMAPSIZEVISUAL;
        int cycleElemNum = 0;
        std::vector<int> cycleElem;
        // for tet
        std::vector<int> vecID; // the block count of each node , block count the same, it is belong to the same block
        int count=0; // for different block
    };
    /****** END Graph***********/
    
    
    /**************************************************************************
     * file imstkVESSBasic.h
     * TODO: smooth spline
     * author Zhaohui Xia
     * date 2018/1/30 15:18
     * Contact: unihui@gmail.com
    ***************************************************************************/
    struct VertexXZH
    {
        float x;
        float y;
        float z;
    };
    class spline
    {
    public:
        // ctor/dtor

        spline(unsigned long inStride, unsigned long inOrder, float* inPointsP) throw();
        bool update(unsigned long inStride, unsigned long inOrder, float* inPointsP) throw();
        // An array of a vertex expressed as x, y, z control points. The points array
        // will be copied by this object. Thus the caller is responsible
        // for any deallocation of the points array passed in.
        // Stride specifies the number of floats between the beginning
        // of  one  control point and the beginning of the next one in the
        // data structure referenced in inPointsP.
        // Order is the number of control points.

        // Accessors

        void Eval(float f, float* outVertexP) const throw();
        std::vector<VertexXZH>& getPoints() { return mPointsP; }  // boost::shared_array
        float& getLength() { return mLength; }
        void setLength(float len) { mLength = len; }
        // For a given percentage, f (0.0 -> 1.0) return the vertex

    protected:
        // ctor/dtor

        spline() throw();

    private:
        // Miscellaneous state


        float mLength;
        unsigned long mOrder; // const
        std::vector<VertexXZH> mPointsP; // boost::shared_array
    };

	  ////< XZH time micro second/minisecond
	class TimerXZH
	{
	public:
		TimerXZH() : m_begin(std::chrono::high_resolution_clock::now()) {}
		void reset() { m_begin = std::chrono::high_resolution_clock::now(); }
		// default micro second
		int64_t elapsed() const
		{
			return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - m_begin).count();
		}
		// micro second
		int64_t elapsed_micro() const
		{
			return std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - m_begin).count();
		}
		// nano second
		int64_t elapsed_nano() const
		{
			return std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now() - m_begin).count();
		}
		// second
		int64_t elapsed_seconds() const
		{
			return std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now() - m_begin).count();
		}
		// minute
		int64_t elapsed_minutes() const
		{
			return std::chrono::duration_cast<std::chrono::minutes>(std::chrono::high_resolution_clock::now() - m_begin).count();
		}
		// hour
		int64_t elapsed_hours() const
		{
			return std::chrono::duration_cast<std::chrono::hours>(std::chrono::high_resolution_clock::now() - m_begin).count();
		}
	private:
		std::chrono::time_point<std::chrono::high_resolution_clock> m_begin;
	};

      ////< XZH SPH Fluid
#define BOUNDARYXZH 0.0001f
#define INFXZH 1E-12f
    typedef unsigned int uintXZH;
    struct floatXZH3
    {
        float x;
        float y;
        float z;
    };

    struct intXZH3
    {
        int x;
        int y;
        int z;
    };

    struct uintXZH3
    {
        unsigned int x;
        unsigned int y;
        unsigned int z;
    };
      ////< XZH SPH Data
    //float window_width = 1000;
    //float window_height = 1000;

    //float xRot = 15.0f;
    //float yRot = 0.0f;
    //float xTrans = 0.0;
    //float yTrans = 0;
    //float zTrans = -35.0;

    //int ox;
    //int oy;
    //int buttonState;
    //float xRotLength = 0.0f;
    //float yRotLength = 0.0f;

    //floatXZH3 real_world_origin;
    //floatXZH3 real_world_side;
    //floatXZH3 sim_ratio;

    //float world_width;
    //float world_height;
    //float world_length;

    
      ////< XZH SPH Fluid END
    ////< XZH END
}// imstk

#endif // ifndef imstkVESSBasic_h
