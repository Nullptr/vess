/*=========================================================================

   Library: iMSTK

   Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
   & Imaging in Medicine, Rensselaer Polytechnic Institute.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0.txt

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

   =========================================================================*/

#include <sys/stat.h>

#include "imstkMeshIO.h"
#include "imstkVTKMeshIO.h"
#include "imstkAssimpMeshIO.h"
#include "imstkVegaMeshIO.h"
#include "imstkMSHMeshIO.h"
#include <unordered_map> // XZH
#include "imstkVESSBasic.h"

#include "g3log/g3log.hpp"

namespace imstk
{
    // XZH
    // Structure: Vertex
    //
    // Description: Model Vertex object that holds
    //	a Position, Normal, and Texture Coordinate
    struct Vertex
    {
        // Position Vector
        Vec3d Position;

        // Normal Vector
        Vec3d Normal;

        // Texture Coordinate Vector
        Vec2d TextureCoordinate;
    };

    // ---------------------------------------------------------------------------
    /** Data structure to represent the bit pattern of a 32 Bit
    *         IEEE 754 floating-point number. */
    union _IEEESingleXZH
    {
        float Float;
        struct
        {
            uint32_t Frac : 23;
            uint32_t Exp : 8;
            uint32_t Sign : 1;
        } IEEEXZH;
    };
    // ---------------------------------------------------------------------------
    /** @brief check whether a float is either NaN or (+/-) INF.
    *
    *  Denorms return false, they're treated like normal values.
    *  @param in Input value */
    inline bool isSpecialFloat(float in)
    {
        return (reinterpret_cast<_IEEESingleXZH*>(&in)->IEEEXZH.Exp == (1u << 8) - 1);
    }


    // Check to see if a Vector3 Point is within a 3 Vector3 Triangle
    bool inTriangle(Vec3d point, Vec3d tri1, Vec3d tri2, Vec3d tri3)
    {
        // Starting vars
        Vec3d u = tri2 - tri1;
        Vec3d v = tri3 - tri1;
        Vec3d w = point - tri1;
        Vec3d n = u.cross(v); //  math::CrossV3(u, v);

        float y = (u.cross(w)).dot(n) / n.dot(n); //   (math::DotV3(math::CrossV3(u, w), n) / math::DotV3(n, n));
        float b = (u.cross(w)).dot(n) / n.dot(n); //   (math::DotV3(math::CrossV3(u, w), n) / math::DotV3(n, n));
        float a = 1 - y - b;

        // Projected point
        Vec3d  p = (a * tri1) + (b * tri2) + (y * tri3);

        if (a >= 0 && a <= 1
            && b >= 0 && b <= 1
            && y >= 0 && y <= 1)
        {
            return true;
        }
        else
            return false;
    }

    // Angle between 2 Vector3 Objects
    float angleBetweenVec(const Vec3d a, const Vec3d b)
    {
        float angle = a.dot(b); // DotV3(a, b);
        angle /= a.norm()*b.norm(); //  (MagnitudeV3(a) * MagnitudeV3(b));
        return angle = acosf(angle);
    }

    // Get first token of string
    inline std::string firstToken(const std::string &in)
    {
        if (!in.empty())
        {
            size_t token_start = in.find_first_not_of(" \t");
            size_t token_end = in.find_first_of(" \t", token_start);
            if (token_start != std::string::npos && token_end != std::string::npos)
            {
                return in.substr(token_start, token_end - token_start);
            }
            else if (token_start != std::string::npos)
            {
                return in.substr(token_start);
            }
        }
        return "";
    }

    // Get tail of string after first token and possibly following spaces
    inline std::string tail(const std::string &in)
    {
        size_t token_start = in.find_first_not_of(" \t");
        size_t space_start = in.find_first_of(" \t", token_start);
        size_t tail_start = in.find_first_not_of(" \t", space_start);
        size_t tail_end = in.find_last_not_of(" \t");
        if (tail_start != std::string::npos && tail_end != std::string::npos)
        {
            return in.substr(tail_start, tail_end - tail_start + 1);
        }
        else if (tail_start != std::string::npos)
        {
            return in.substr(tail_start);
        }
        return "";
    }

    // Split a String into a string array at a given token
    inline void split(const std::string &in,
        std::vector<std::string> &out,
        std::string token)
    {
        out.clear();

        std::string temp;

        for (int i = 0; i < int(in.size()); i++)
        {
            std::string test = in.substr(i, token.size());

            if (test == token)
            {
                if (!temp.empty())
                {
                    out.push_back(temp);
                    temp.clear();
                    i += (int)token.size() - 1;
                }
                else
                {
                    out.push_back("");
                }
            }
            else if (i + token.size() >= in.size())
            {
                temp += in.substr(i, token.size());
                out.push_back(temp);
                break;
            }
            else
            {
                temp += in[i];
            }
        }
    }

    // Get element at given index position
    template <class T>
    inline const T & getElement(const std::vector<T> &elements, std::string &index)
    {
        int idx = std::stoi(index);
        if (idx < 0)
            idx = int(elements.size()) + idx;
        else
            idx--;
        return elements[idx];
    }

    // Generate vertices from a list of positions, 
    //	tcoords, normals and a face line
    void generateVerticesFromRawOBJ(std::vector<Vertex>& oVerts,
        const std::vector<Vec3d>& iPositions,
        const std::vector<Vec2d>& iTCoords,
        const std::vector<Vec3d>& iNormals,
        std::array<size_t, 3>& iTriangles, // XZH
        std::string icurline)
    {
        std::vector<std::string> sface, svert;
        Vertex vVert;
        split(tail(icurline), sface, " ");

        bool noNormal = false;

        // For every given vertex do this
        for (int i = 0; i < int(sface.size()); i++)
        {
            // See What type the vertex is.
            int vtype;

            split(sface[i], svert, "/");

            // Check for just position - v1
            if (svert.size() == 1)
            {
                // Only position
                vtype = 1;
            }

            // Check for position & texture - v1/vt1
            if (svert.size() == 2)
            {
                // Position & Texture
                vtype = 2;
            }

            // Check for Position, Texture and Normal - v1/vt1/vn1
            // or if Position and Normal - v1//vn1
            if (svert.size() == 3)
            {
                if (svert[1] != "")
                {
                    // Position, Texture, and Normal
                    vtype = 4;
                }
                else
                {
                    // Position & Normal
                    vtype = 3;
                }
            }

            // Calculate and store the vertex
            switch (vtype)
            {
            case 1: // P
            {
                vVert.Position = getElement(iPositions, svert[0]);
                vVert.TextureCoordinate = Vec2d(0, 0);
                noNormal = true;
                oVerts.push_back(vVert);
                break;
            }
            case 2: // P/T
            {
                vVert.Position = getElement(iPositions, svert[0]);
                vVert.TextureCoordinate = getElement(iTCoords, svert[1]);
                noNormal = true;
                oVerts.push_back(vVert);
                break;
            }
            case 3: // P//N
            {
                vVert.Position = getElement(iPositions, svert[0]);
                vVert.TextureCoordinate = Vec2d(0, 0);
                vVert.Normal = getElement(iNormals, svert[2]);
                oVerts.push_back(vVert);
                break;
            }
            case 4: // P/T/N
            {
                vVert.Position = getElement(iPositions, svert[0]);
                vVert.TextureCoordinate = getElement(iTCoords, svert[1]);
                vVert.Normal = getElement(iNormals, svert[2]);
                oVerts.push_back(vVert);
                break;
            }
            default:
            {
                break;
            }
            }

            iTriangles.at(i) = std::stoi(svert[0]) - 1;  // .push_back(std::stoi(svert[0]) - 1);
        }

        // take care of missing normals
        // these may not be truly acurate but it is the 
        // best they get for not compiling a mesh with normals	
        if (noNormal)
        {
            Vec3d A = oVerts[0].Position - oVerts[1].Position;
            Vec3d B = oVerts[2].Position - oVerts[1].Position;

            Vec3d normal = A.cross(B); // math::CrossV3(A, B);

            for (int i = 0; i < int(oVerts.size()); i++)
            {
                oVerts[i].Normal = normal;
            }
        }
    }

    // Triangulate a list of vertices into a face by printing
    //	inducies corresponding with triangles within it
    void VertexTriangluation(std::vector<unsigned int>& oIndices,
        const std::vector<Vertex>& iVerts)
    {
        // If there are 2 or less verts,
        // no triangle can be created,
        // so exit
        if (iVerts.size() < 3)
        {
            return;
        }
        // If it is a triangle no need to calculate it
        if (iVerts.size() == 3)
        {
            oIndices.push_back(0);
            oIndices.push_back(1);
            oIndices.push_back(2);
            return;
        }

        // Create a list of vertices
        std::vector<Vertex> tVerts = iVerts;

        while (true)
        {
            // For every vertex
            for (int i = 0; i < int(tVerts.size()); i++)
            {
                // pPrev = the previous vertex in the list
                Vertex pPrev;
                if (i == 0)
                {
                    pPrev = tVerts[tVerts.size() - 1];
                }
                else
                {
                    pPrev = tVerts[i - 1];
                }

                // pCur = the current vertex;
                Vertex pCur = tVerts[i];

                // pNext = the next vertex in the list
                Vertex pNext;
                if (i == tVerts.size() - 1)
                {
                    pNext = tVerts[0];
                }
                else
                {
                    pNext = tVerts[i + 1];
                }

                // Check to see if there are only 3 verts left
                // if so this is the last triangle
                if (tVerts.size() == 3)
                {
                    // Create a triangle from pCur, pPrev, pNext
                    for (int j = 0; j < int(tVerts.size()); j++)
                    {
                        if (iVerts[j].Position == pCur.Position)
                            oIndices.push_back(j);
                        if (iVerts[j].Position == pPrev.Position)
                            oIndices.push_back(j);
                        if (iVerts[j].Position == pNext.Position)
                            oIndices.push_back(j);
                    }

                    tVerts.clear();
                    break;
                }
                if (tVerts.size() == 4)
                {
                    // Create a triangle from pCur, pPrev, pNext
                    for (int j = 0; j < int(iVerts.size()); j++)
                    {
                        if (iVerts[j].Position == pCur.Position)
                            oIndices.push_back(j);
                        if (iVerts[j].Position == pPrev.Position)
                            oIndices.push_back(j);
                        if (iVerts[j].Position == pNext.Position)
                            oIndices.push_back(j);
                    }

                    Vec3d tempVec;
                    for (int j = 0; j < int(tVerts.size()); j++)
                    {
                        if (tVerts[j].Position != pCur.Position
                            && tVerts[j].Position != pPrev.Position
                            && tVerts[j].Position != pNext.Position)
                        {
                            tempVec = tVerts[j].Position;
                            break;
                        }
                    }

                    // Create a triangle from pCur, pPrev, pNext
                    for (int j = 0; j < int(iVerts.size()); j++)
                    {
                        if (iVerts[j].Position == pPrev.Position)
                            oIndices.push_back(j);
                        if (iVerts[j].Position == pNext.Position)
                            oIndices.push_back(j);
                        if (iVerts[j].Position == tempVec)
                            oIndices.push_back(j);
                    }

                    tVerts.clear();
                    break;
                }

                // If Vertex is not an interior vertex
                float angle = angleBetweenVec(pPrev.Position - pCur.Position, pNext.Position - pCur.Position) * (180 / 3.14159265359);
                if (angle <= 0 && angle >= 180)
                    continue;

                // If any vertices are within this triangle
                bool inTri = false;
                for (int j = 0; j < int(iVerts.size()); j++)
                {
                    if (inTriangle(iVerts[j].Position, pPrev.Position, pCur.Position, pNext.Position)
                        && iVerts[j].Position != pPrev.Position
                        && iVerts[j].Position != pCur.Position
                        && iVerts[j].Position != pNext.Position)
                    {
                        inTri = true;
                        break;
                    }
                }
                if (inTri)
                    continue;

                // Create a triangle from pCur, pPrev, pNext
                for (int j = 0; j < int(iVerts.size()); j++)
                {
                    if (iVerts[j].Position == pCur.Position)
                        oIndices.push_back(j);
                    if (iVerts[j].Position == pPrev.Position)
                        oIndices.push_back(j);
                    if (iVerts[j].Position == pNext.Position)
                        oIndices.push_back(j);
                }

                // Delete pCur from the list
                for (int j = 0; j < int(tVerts.size()); j++)
                {
                    if (tVerts[j].Position == pCur.Position)
                    {
                        tVerts.erase(tVerts.begin() + j);
                        break;
                    }
                }

                // reset i to the start
                // -1 since loop will add 1 to it
                i = -1;
            }

            // if no triangles were created
            if (oIndices.size() == 0)
                break;

            // if no more vertices
            if (tVerts.size() == 0)
                break;
        }
    }
    // XZH END

    std::shared_ptr<PointSet>
        MeshIO::read(const std::string& filePath)
    {
        if (!MeshIO::fileExists(filePath))
        {
            LOG(WARNING) << "MeshIO::read error: file not found: " << filePath;
            return nullptr;
        }

        MeshFileType meshType = MeshIO::getFileType(filePath);
        switch (meshType)
        {
        case MeshFileType::VTK:
        case MeshFileType::VTU:
        case MeshFileType::VTP:
        case MeshFileType::STL:
        case MeshFileType::PLY:
            return VTKMeshIO::read(filePath, meshType);
            break;
        case MeshFileType::OBJ:
            //return readMeshDataXZH2(filePath);
            //break; // XZH
        case MeshFileType::DAE:
        case MeshFileType::FBX:
        case MeshFileType::_3DS:
            return AssimpMeshIO::read(filePath, meshType);
            break;
        case MeshFileType::VEG:
            return VegaMeshIO::read(filePath, meshType);
            break;
        case MeshFileType::MSH:
            return MSHMeshIO::read(filePath, meshType);
            break;
        }

        LOG(WARNING) << "MeshIO::read error: file type not supported";
        return nullptr;
    }

    bool
        MeshIO::fileExists(const std::string& file)
    {
        struct stat buf;
        return (stat(file.c_str(), &buf) == 0);
    }

    const MeshFileType
        MeshIO::getFileType(const std::string& filePath)
    {
        MeshFileType meshType = MeshFileType::UNKNOWN;

        std::string extString = filePath.substr(filePath.find_last_of(".") + 1);
        if (extString.empty())
        {
            LOG(WARNING) << "MeshIO::getFileType error: invalid file name";
            return meshType;
        }

        if (extString == "vtk" || extString == "VTK")
        {
            meshType = MeshFileType::VTK;
        }
        else if (extString == "vtp" || extString == "VTP")
        {
            meshType = MeshFileType::VTP;
        }
        else if (extString == "vtu" || extString == "VTU")
        {
            meshType = MeshFileType::VTU;
        }
        else if (extString == "obj" || extString == "OBJ")
        {
            meshType = MeshFileType::OBJ;
        }
        else if (extString == "stl" || extString == "STL")
        {
            meshType = MeshFileType::STL;
        }
        else if (extString == "ply" || extString == "PLY")
        {
            meshType = MeshFileType::PLY;
        }
        else if (extString == "dae" || extString == "DAE")
        {
            meshType = MeshFileType::DAE;
        }
        else if (extString == "fbx" || extString == "FBX")
        {
            meshType = MeshFileType::FBX;
        }
        else if (extString == "3ds" || extString == "3DS")
        {
            meshType = MeshFileType::_3DS;
        }
        else if (extString == "veg" || extString == "VEG")
        {
            meshType = MeshFileType::VEG;
        }
        else if (extString == "msh" || extString == "MSH")
        {
            meshType = MeshFileType::MSH;
        }
        else
        {
            LOG(WARNING) << "MeshIO::getFileType error: unknown file extension";
        }

        return meshType;
    }

    bool
        MeshIO::write(const std::shared_ptr<imstk::PointSet> imstkMesh, const std::string& filePath)
    {
        MeshFileType meshType = MeshIO::getFileType(filePath);
        switch (meshType)
        {
        case MeshFileType::VEG:
            return VegaMeshIO::write(imstkMesh, filePath, meshType);
            break;
        case MeshFileType::VTU:
        case MeshFileType::VTP:
        case MeshFileType::STL:
        case MeshFileType::PLY:
            return VTKMeshIO::write(imstkMesh, filePath, meshType);
            break;
        }

        LOG(WARNING) << "MeshIO::write error: file type not supported";
        return false;
    }
    std::shared_ptr<SurfaceMesh>
        MeshIO::readMeshDataXZH(const std::string& filePath)
    {
        // Build SurfaceMesh
        auto mesh = std::make_shared<SurfaceMesh>();

        // read
        std::ifstream file(filePath);

        if (!file.is_open())
            return false;

        std::vector<Vec3d> Positions;
        std::vector<Vec2d> TCoords;
        std::vector<Vec3d> Normals;

        std::vector<Vertex> Vertices;
        std::vector<unsigned int> Indices;

        std::vector<std::string> MeshMatNames;

        // Loaded Vertex Objects
        std::vector<Vertex> LoadedVertices;
        // Loaded Index Positions
        std::vector<unsigned int> LoadedIndices;

        //using TriangleArray = std::array<size_t, 3>;
        std::vector<std::array<size_t, 3>> trianglesVerts;

        std::vector<std::vector<size_t>> triangleVertices;

        bool listening = false;
        std::string meshname;

#ifdef OBJL_CONSOLE_OUTPUT
        const unsigned int outputEveryNth = 1000;
        unsigned int outputIndicator = outputEveryNth;
#endif

        std::string curline;
        while (std::getline(file, curline))
        {
#ifdef OBJL_CONSOLE_OUTPUT
            if ((outputIndicator = ((outputIndicator + 1) % outputEveryNth)) == 1)
            {
                if (!meshname.empty())
                {
                    std::cout
                        << "\r- " << meshname
                        << "\t| vertices > " << Positions.size()
                        << "\t| texcoords > " << TCoords.size()
                        << "\t| normals > " << Normals.size()
                        << "\t| triangles > " << (Vertices.size() / 3)
                        << (!MeshMatNames.empty() ? "\t| material: " + MeshMatNames.back() : "");
                }
            }
#endif

            // Generate a Mesh Object or Prepare for an object to be created
            if (firstToken(curline) == "o" || firstToken(curline) == "g" || curline[0] == 'g')
            {
                if (!listening)
                {
                    listening = true;

                    if (firstToken(curline) == "o" || firstToken(curline) == "g")
                    {
                        meshname = tail(curline);
                    }
                    else
                    {
                        meshname = "unnamed";
                    }
                }
                else
                {
                    // Generate the mesh to put into the array

                    if (!Indices.empty() && !Vertices.empty())
                    {
                        // Create Mesh
                        //tempMesh = Mesh(Vertices, Indices);
                        //tempMesh.MeshName = meshname;

                        //// Insert Mesh
                        //LoadedMeshes.push_back(tempMesh);

                        // Cleanup
                        Vertices.clear();
                        Indices.clear();
                        meshname.clear();

                        meshname = tail(curline);
                    }
                    else
                    {
                        if (firstToken(curline) == "o" || firstToken(curline) == "g")
                        {
                            meshname = tail(curline);
                        }
                        else
                        {
                            meshname = "unnamed";
                        }
                    }
                }
#ifdef OBJL_CONSOLE_OUTPUT
                std::cout << std::endl;
                outputIndicator = 0;
#endif
            }
            // Generate a Vertex Position
            if (firstToken(curline) == "v")
            {
                std::vector<std::string> spos;
                Vec3d vpos;
                split(tail(curline), spos, " ");

                vpos.x() = std::stof(spos[0]);
                vpos.y() = std::stof(spos[1]);
                vpos.z() = std::stof(spos[2]);

                Positions.push_back(vpos);
            }
            // Generate a Vertex Texture Coordinate
            if (firstToken(curline) == "vt")
            {
                std::vector<std::string> stex;
                Vec2d vtex;
                split(tail(curline), stex, " ");

                vtex.x() = std::stof(stex[0]);
                vtex.y() = std::stof(stex[1]);

                TCoords.push_back(vtex);
            }
            // Generate a Vertex Normal;
            if (firstToken(curline) == "vn")
            {
                std::vector<std::string> snor;
                Vec3d vnor;
                split(tail(curline), snor, " ");

                vnor.x() = std::stof(snor[0]);
                vnor.y() = std::stof(snor[1]);
                vnor.z() = std::stof(snor[2]);

                Normals.push_back(vnor);
            }

            // Generate a Face (vertices & indices)
            if (firstToken(curline) == "f")
            {
                // Generate the vertices
                std::vector<Vertex> vVerts;
                //std::vector<unsigned int> iTriangles; // XZH
                std::array<size_t, 3> iTriangles; // TriangleArray iTriangles;
                generateVerticesFromRawOBJ(vVerts, Positions, TCoords, Normals, iTriangles, curline);
                trianglesVerts.push_back(iTriangles);
                // Add Vertices
                for (int i = 0; i < int(vVerts.size()); i++)
                {
                    Vertices.push_back(vVerts[i]);

                    LoadedVertices.push_back(vVerts[i]);
                }

                std::vector<unsigned int> iIndices;

                VertexTriangluation(iIndices, vVerts);
                //TriangleArray tmpTri = { { iIndices[0], iIndices[1], iIndices[2] } };
                //trianglesVerts.push_back(tmpTri);
                //printf("%d %d %d ", iIndices[0], iIndices[1], iIndices[2]);
                // Add Indices
                for (int i = 0; i < int(iIndices.size()); i++)
                {
                    unsigned int indnum = (unsigned int)((Vertices.size()) - vVerts.size()) + iIndices[i];
                    Indices.push_back(indnum);

                    indnum = (unsigned int)((LoadedVertices.size()) - vVerts.size()) + iIndices[i];
                    LoadedIndices.push_back(indnum);
                    //printf("%d ", indnum);
                }
                //printf("\n");
            }          
        }

#ifdef OBJL_CONSOLE_OUTPUT
        std::cout << std::endl;
#endif

        // Deal with last mesh

        if (!Indices.empty() && !Vertices.empty())
        {
            //// Create Mesh
            //tempMesh = Mesh(Vertices, Indices);
            //tempMesh.MeshName = meshname;

            //// Insert Mesh
            //LoadedMeshes.push_back(tempMesh);
        }

        file.close();

        StdVectorOfVec3d poses;
        for (int i = 0; i < Positions.size();i++)
        {
            poses.push_back(Positions[i]);
        }
        if ((poses.size() == 1062) || (poses.size() == 2686) || (poses.size() == 5245))  // tool 1255) // colon 1 2 3: 1062 2686 5245
        {
            for (int i = 0; i < poses.size(); i++)
            {
                poses[i] = poses[i] + Vec3d(0, -5, 15);
            }
        }
        std::vector<SurfaceMesh::TriangleArray> triangles;
        for (int i = 0; i < trianglesVerts.size();i++)
        {
            triangles.push_back(trianglesVerts[i]);
        }
        StdVectorOfVec3d normals;
        for (int i = 0; i < Normals.size(); i++)
        {
            normals.push_back(Normals[i]);
        }
        mesh->initialize(poses, triangles, normals, false);
        mesh->setVertexNormals(normals);

        // UV coordinates
        unsigned int numVertices = Positions.size();
        StdVectorOfVectorf UVs(numVertices);
        //auto texcoords = importedMesh->mTextureCoords[0];
        for (unsigned int i = 0; i < numVertices; i++)
        {
            Vectorf UV(2);
            UV[0] = Vertices[i].TextureCoordinate.x(); // TCoords[i].x(); // texcoords[i].x;
            UV[1] = Vertices[i].TextureCoordinate.y(); // TCoords[i].y(); //  texcoords[i].y;
            UVs[i] = UV;
        }

        mesh->setDefaultTCoords("tCoords");
        mesh->setPointDataArray("tCoords", UVs);

        // tangents and bitangents
        auto& vertNormals = mesh->getVertexNormals();
        StdVectorOfVec3d tangents;
        StdVectorOfVec3d bitangents;
        for (int i = 0; i < Positions.size(); i++)
        {
            tangents.push_back(Vec3d::Zero());
            bitangents.push_back(Vec3d::Zero());
        }

        const float angleEpsilon = 0.9999f;

        std::vector<bool> vertexDone(numVertices, false);
        const float qnan = std::numeric_limits<float>::quiet_NaN(); 

        // calculate the tangent and bitangent for every face
        for (unsigned int a = 0; a < triangles.size(); a++)
        {
            //const aiFace& face = pMesh->mFaces[a];
            auto& face = triangles[a];

            // triangle or polygon... we always use only the first three indices. A polygon
            // is supposed to be planar anyways....
            // FIXME: (thom) create correct calculation for multi-vertex polygons maybe?
            const unsigned int p0 = face[0], p1 = face[1], p2 = face[2];

            // position differences p1->p2 and p1->p3
            Vec3d v = Positions[p1] - Positions[p0], w = Positions[p2] - Positions[p0];

            // texture offset p1->p2 and p1->p3
            float sx = UVs[p1][0] - UVs[p0][0], sy = UVs[p1][1] - UVs[p0][1];
            float tx = UVs[p2][0] - UVs[p0][0], ty = UVs[p2][1] - UVs[p0][1];
            float dirCorrection = (tx * sy - ty * sx) < 0.0f ? -1.0f : 1.0f;
            // when t1, t2, t3 in same position in UV space, just use default UV direction.
            if (0 == sx && 0 == sy && 0 == tx && 0 == ty) 
            {
                sx = 0.0; sy = 1.0;
                tx = 1.0; ty = 0.0;
            }

            // tangent points in the direction where to positive X axis of the texture coord's would point in model space
            // bitangent's points along the positive Y axis of the texture coord's, respectively
            Vec3d tangent, bitangent;
            tangent.x() = (w.x() * sy - v.x() * ty) * dirCorrection;
            tangent.y() = (w.y() * sy - v.y() * ty) * dirCorrection;
            tangent.z() = (w.z() * sy - v.z() * ty) * dirCorrection;
            bitangent.x() = (w.x() * sx - v.x() * tx) * dirCorrection;
            bitangent.y() = (w.y() * sx - v.y() * tx) * dirCorrection;
            bitangent.z() = (w.z() * sx - v.z() * tx) * dirCorrection;

            // store for every vertex of that face
            for (unsigned int b = 0; b < 3; ++b) 
            {
                unsigned int p = face[b];

                // project tangent and bitangent into the plane formed by the vertex' normal
                Vec3d localTangent = tangent - vertNormals[p].cross(tangent.cross(vertNormals[p]));
                Vec3d localBitangent = bitangent - vertNormals[p].cross(bitangent.cross(vertNormals[p]));
                localTangent.normalize();
                localBitangent.normalize();

                // reconstruct tangent/bitangent according to normal and bitangent/tangent when it's infinite or NaN.
                bool invalid_tangent = isSpecialFloat(localTangent.x()) || isSpecialFloat(localTangent.y()) || isSpecialFloat(localTangent.z());
                bool invalid_bitangent = isSpecialFloat(localBitangent.x()) || isSpecialFloat(localBitangent.y()) || isSpecialFloat(localBitangent.z());
                if (invalid_tangent != invalid_bitangent) 
                {
                    if (invalid_tangent) 
                    {
                        localTangent = vertNormals[p].cross(localBitangent);
                        localTangent.normalize();
                    }
                    else 
                    {
                        localBitangent = localTangent.cross(vertNormals[p]);
                        localBitangent.normalize();
                    }
                }

                // and write it into the mesh.
                tangents[p] = localTangent;
                bitangents[p] = localBitangent;
            }
        }

        // smooth tangent XZH comment out
        //// create a helper to quickly find locally close vertices among the vertex array
        //// FIX: check whether we can reuse the SpatialSort of a previous step
        //SpatialSort* vertexFinder = NULL;
        //SpatialSort  _vertexFinder;
        //float posEpsilon;
        //if (shared)
        //{
        //    std::vector<std::pair<SpatialSort, float> >* avf;
        //    shared->GetProperty(AI_SPP_SPATIAL_SORT, avf);
        //    if (avf)
        //    {
        //        std::pair<SpatialSort, float>& blubb = avf->operator [] (meshIndex);
        //        vertexFinder = &blubb.first;
        //        posEpsilon = blubb.second;;
        //    }
        //}
        //if (!vertexFinder)
        //{
        //    _vertexFinder.Fill(pMesh->mVertices, pMesh->mNumVertices, sizeof(aiVector3D));
        //    vertexFinder = &_vertexFinder;
        //    posEpsilon = ComputePositionEpsilon(pMesh);
        //}
        //std::vector<unsigned int> verticesFound;

        //const float fLimit = cosf(configMaxAngle);
        //std::vector<unsigned int> closeVertices;

        //// in the second pass we now smooth out all tangents and bitangents at the same local position
        //// if they are not too far off.
        //for (unsigned int a = 0; a < pMesh->mNumVertices; a++)
        //{
        //    if (vertexDone[a])
        //        continue;

        //    const aiVector3D& origPos = pMesh->mVertices[a];
        //    const aiVector3D& origNorm = pMesh->mNormals[a];
        //    const aiVector3D& origTang = pMesh->mTangents[a];
        //    const aiVector3D& origBitang = pMesh->mBitangents[a];
        //    closeVertices.resize(0);

        //    // find all vertices close to that position
        //    vertexFinder->FindPositions(origPos, posEpsilon, verticesFound);

        //    closeVertices.reserve(verticesFound.size() + 5);
        //    closeVertices.push_back(a);

        //    // look among them for other vertices sharing the same normal and a close-enough tangent/bitangent
        //    for (unsigned int b = 0; b < verticesFound.size(); b++)
        //    {
        //        unsigned int idx = verticesFound[b];
        //        if (vertexDone[idx])
        //            continue;
        //        if (meshNorm[idx] * origNorm < angleEpsilon)
        //            continue;
        //        if (meshTang[idx] * origTang < fLimit)
        //            continue;
        //        if (meshBitang[idx] * origBitang < fLimit)
        //            continue;

        //        // it's similar enough -> add it to the smoothing group
        //        closeVertices.push_back(idx);
        //        vertexDone[idx] = true;
        //    }

        //    // smooth the tangents and bitangents of all vertices that were found to be close enough
        //    aiVector3D smoothTangent(0, 0, 0), smoothBitangent(0, 0, 0);
        //    for (unsigned int b = 0; b < closeVertices.size(); ++b)
        //    {
        //        smoothTangent += meshTang[closeVertices[b]];
        //        smoothBitangent += meshBitang[closeVertices[b]];
        //    }
        //    smoothTangent.Normalize();
        //    smoothBitangent.Normalize();

        //    // and write it back into all affected tangents
        //    for (unsigned int b = 0; b < closeVertices.size(); ++b)
        //    {
        //        meshTang[closeVertices[b]] = smoothTangent;
        //        meshBitang[closeVertices[b]] = smoothBitangent;
        //    }
        //}
        // END tangents

        mesh->setVertexTangents(tangents);

        return mesh;
    }


    //////////////////////////////////////////////////////////////////////////
    // Returns true iif v1 can be considered equal to v2
    bool is_near(float v1, float v2)
    {
        return fabs(v1 - v2) < 0.01f;
    }

    // Searches through all already-exported vertices
    // for a similar one.
    // Similar = same position + same UVs + same normal
    bool getSimilarVertexIndex(
        Vec3d & in_vertex,
        Vec2d & in_uv,
        Vec3d & in_normal,
        std::vector<Vec3d> & out_vertices,
        std::vector<Vec2d> & out_uvs,
        std::vector<Vec3d> & out_normals,
        unsigned short & result
        )
    {
        // Lame linear search
        for (unsigned int i = 0; i < out_vertices.size(); i++)
        {
            if (
                is_near(in_vertex.x(), out_vertices[i].x()) &&
                is_near(in_vertex.y(), out_vertices[i].y()) &&
                is_near(in_vertex.z(), out_vertices[i].z()) &&
                is_near(in_uv.x(), out_uvs[i].x()) &&
                is_near(in_uv.y(), out_uvs[i].y()) &&
                is_near(in_normal.x(), out_normals[i].x()) &&
                is_near(in_normal.y(), out_normals[i].y()) &&
                is_near(in_normal.z(), out_normals[i].z())
                )
            {
                result = i;
                return true;
            }
        }
        // No other vertex could be used instead.
        // Looks like we'll have to add it to the VBO.
        return false;
    }
    std::shared_ptr<SurfaceMesh>
        MeshIO::readMeshDataXZH2(const std::string& filePath)
    {
        // Build SurfaceMesh
        auto mesh = std::make_shared<SurfaceMesh>();
        printf("Loading OBJ file %s...\n", filePath.c_str());

        bool bSubdivsion = false;
        if (filePath=="I:/iMSTK/resources/VESS/colontumor_new_SB.obj")
        {
            bSubdivsion = true;
        }

        std::vector<Vec3d> out_vertices;
        std::vector<Vec2d> out_uvs;
        std::vector<Vec3d> out_normals;
        
        std::vector<unsigned int> vertexIndices, uvIndices, normalIndices;
        std::vector<Vec3d> temp_vertices;
        std::vector<Vec2d> temp_uvs;
        std::vector<Vec3d> temp_normals;

        FILE * file = fopen(filePath.c_str(), "r");
        if (file == NULL)
        {
            printf("Impossible to open the file ! Are you in the right path ? See Tutorial 1 for details\n");
            getchar();
            return false;
        }

        // load obj
        while (1)
        {
            char lineHeader[128];
            // read the first word of the line
            int res = fscanf(file, "%s", lineHeader);
            if (res == EOF)
                break; // EOF = End Of File. Quit the loop.

            // else : parse lineHeader

            if (strcmp(lineHeader, "v") == 0)
            {
                Vec3d vertex;
                float vertx[3];
                fscanf(file, "%f %f %f\n", &vertx[0], &vertx[1], &vertx[2]);
                vertex.x() = vertx[0]; vertex.y() = vertx[1]; vertex.z() = vertx[2];
                temp_vertices.push_back(vertex);
            }
            else if (strcmp(lineHeader, "vt") == 0)
            {
                Vec2d uv;
                float uvcd[2];
                fscanf(file, "%f %f\n", &uvcd[0], &uvcd[1]);
                uvcd[1] = -uvcd[1]; // Invert V coordinate since we will only use DDS texture, which are inverted. Remove if you want to use TGA or BMP loaders.
                uv.x() = uvcd[0]; uv.y() = uvcd[1];
                temp_uvs.push_back(uv);
            }
            else if (strcmp(lineHeader, "vn") == 0)
            {
                Vec3d normal;
                float vertx[3]; // fscanf(file, "%f %f %f\n", &normal.x(), &normal.y(), &normal.z());
                fscanf(file, "%f %f %f\n", &vertx[0], &vertx[1], &vertx[2]);
                normal.x() = vertx[0]; normal.y() = vertx[1]; normal.z() = vertx[2];             
                temp_normals.push_back(normal);
            }
            else if (strcmp(lineHeader, "f") == 0)
            {
                std::string vertex1, vertex2, vertex3;
                unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
                int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2]);
                if (matches != 9)
                {
                    printf("File can't be read by our simple parser :-( Try exporting with other options\n");
                    fclose(file);
                    return false;
                }
                vertexIndices.push_back(vertexIndex[0]);
                vertexIndices.push_back(vertexIndex[1]);
                vertexIndices.push_back(vertexIndex[2]);
                uvIndices.push_back(uvIndex[0]);
                uvIndices.push_back(uvIndex[1]);
                uvIndices.push_back(uvIndex[2]);
                normalIndices.push_back(normalIndex[0]);
                normalIndices.push_back(normalIndex[1]);
                normalIndices.push_back(normalIndex[2]);
            }
            else
            {
                // Probably a comment, eat up the rest of the line
                char stupidBuffer[1000];
                fgets(stupidBuffer, 1000, file);
            }
        }

        // For each vertex of each triangle
        for (unsigned int i = 0; i < vertexIndices.size(); i++)
        {
            // Get the indices of its attributes
            unsigned int vertexIndex = vertexIndices[i];
            unsigned int uvIndex = uvIndices[i];
            unsigned int normalIndex = normalIndices[i];

            // Get the attributes thanks to the index
            Vec3d vertex = temp_vertices[vertexIndex - 1];
            Vec2d uv = temp_uvs[uvIndex - 1];
            Vec3d normal = temp_normals[normalIndex - 1];

            // Put the attributes in buffers
            out_vertices.push_back(vertex);
            out_uvs.push_back(uv);
            out_normals.push_back(normal);

        }
        fclose(file);
        // END load obj

        // compute tangent
        std::vector<Vec3d> out_tangents;
        std::vector<Vec3d> out_bitangents;
        for (unsigned int i = 0; i < out_vertices.size(); i += 3)
        {
            // Shortcuts for vertices
            Vec3d & v0 = out_vertices[i + 0];
            Vec3d & v1 = out_vertices[i + 1];
            Vec3d & v2 = out_vertices[i + 2];

            // Shortcuts for UVs
            Vec2d & uv0 = out_uvs[i + 0];
            Vec2d & uv1 = out_uvs[i + 1];
            Vec2d & uv2 = out_uvs[i + 2];

            // Edges of the triangle : postion delta
            Vec3d deltaPos1 = v1 - v0;
            Vec3d deltaPos2 = v2 - v0;

            // UV delta
            Vec2d deltaUV1 = uv1 - uv0;
            Vec2d deltaUV2 = uv2 - uv0;

            float r = 1.0f / (deltaUV1.x() * deltaUV2.y() - deltaUV1.y() * deltaUV2.x());
            Vec3d tangent = (deltaPos1 * deltaUV2.y() - deltaPos2 * deltaUV1.y())*r;
            Vec3d bitangent = (deltaPos2 * deltaUV1.x() - deltaPos1 * deltaUV2.x())*r;

            // Set the same tangent for all three vertices of the triangle.
            // They will be merged later, in vboindexer.cpp
            out_tangents.push_back(tangent);
            out_tangents.push_back(tangent);
            out_tangents.push_back(tangent);

            // Same thing for binormals
            out_bitangents.push_back(bitangent);
            out_bitangents.push_back(bitangent);
            out_bitangents.push_back(bitangent);
        }

        // See "Going Further"
        for (unsigned int i = 0; i < out_vertices.size(); i += 1)
        {
            Vec3d & n = out_normals[i];
            Vec3d & t = out_tangents[i];
            Vec3d & b = out_bitangents[i];

            // Gram-Schmidt orthogonalize
            t = (t - n*(n.dot(t))).normalized(); //  glm::normalize(t - n * glm::dot(n, t));

            // Calculate handedness
            if ( n.cross(t).dot(b)<0.0f)
            //if (glm::dot(glm::cross(n, t), b) < 0.0f)
            {
                t = t * -1.0f;
            }
        }
        // END tangent

        // index VBO_TBN
        std::vector<unsigned short> out_indices2;
        std::vector<unsigned short> out_indicesNum2;
        std::vector<Vec3d> out_vertices2;
        std::vector<Vec2d> out_uvs2;
        std::vector<Vec3d> out_normals2;
        std::vector<Vec3d> out_tangents2;
        std::vector<Vec3d> out_bitangents2;
        out_indicesNum2.resize(out_vertices.size());
        for (int i = 0; i < out_indicesNum2.size();i++)
        {
            out_indicesNum2[i] = 0;
        }
        //out_indices2.resize(temp_vertices.size());
        //out_vertices2.resize(temp_vertices.size());
        //out_uvs2.resize(temp_vertices.size());
        //out_normals2.resize(temp_vertices.size());
        //out_tangents2.resize(temp_vertices.size());
        //out_bitangents2.resize(temp_vertices.size());
        // //For each input vertex
        //for (unsigned int i = 0; i < out_vertices.size(); i++)
        //{
        //    // new method
        //    // Get the indices of its attributes
        //    unsigned int vertexIndex = vertexIndices[i];
        //    unsigned int uvIndex = uvIndices[i];
        //    unsigned int normalIndex = normalIndices[i];
        //    // Get the attributes thanks to the index
        //    Vec3d vertex = temp_vertices[vertexIndex - 1];
        //    Vec2d uv = temp_uvs[uvIndex - 1];
        //    Vec3d normal = temp_normals[normalIndex - 1];

        //    //out_uvs2[vertexIndex - 1] += out_uvs[uvIndex - 1];
        //    //out_normals2[vertexIndex - 1] += out_normals[normalIndex - 1];
        //    //out_tangents2[vertexIndex - 1] += out_tangents[i];
        //    //out_bitangents2[vertexIndex - 1] += out_bitangents[i];
        //    out_uvs2[vertexIndex - 1] = uv; // out_uvs[uvIndex - 1];
        //    out_normals2[vertexIndex - 1] = normal; // out_normals[normalIndex - 1];
        //    out_tangents2[vertexIndex - 1] = out_tangents[i]; 
        //    out_bitangents2[vertexIndex - 1] = out_bitangents[i];
        //    // END new way

        //    //// old method 
        //    //// Try to find a similar vertex in out_XXXX
        //    //unsigned short index;
        //    //bool found = getSimilarVertexIndex(out_vertices[i], out_uvs[i], out_normals[i], out_vertices2, out_uvs2, out_normals2, index);

        //    //if (found)
        //    //{ // A similar vertex is already in the VBO, use it instead !
        //    //    out_indices2.push_back(index);

        //    //    // Average the tangents and the bitangents
        //    //    out_tangents2[index] += out_tangents[i];
        //    //    out_bitangents2[index] += out_bitangents[i];
        //    //}
        //    //else
        //    //{ // If not, it needs to be added in the output data.
        //    //    out_vertices2.push_back(out_vertices[i]);
        //    //    out_uvs2.push_back(out_uvs[i]);
        //    //    out_normals2.push_back(out_normals[i]);
        //    //    out_tangents2.push_back(out_tangents[i]);
        //    //    out_bitangents2.push_back(out_bitangents[i]);
        //    //    out_indices2.push_back((unsigned short)out_vertices.size() - 1);
        //    //}
        //    //// END old way
        //}

        // index VBO_TBN new method
        for (unsigned int i = 0; i < out_vertices.size(); i++)
        {
            // Try to find a similar vertex in out_XXXX
            unsigned short index;
            bool found = getSimilarVertexIndex(out_vertices[i], out_uvs[i], out_normals[i], out_vertices2, out_uvs2, out_normals2, index);

            if (found)
            { // A similar vertex is already in the VBO, use it instead !
                out_indices2.push_back(index);

                // Average the tangents and the bitangents
                //out_uvs2[index] += out_uvs[i];
                //out_normals2[index] += out_normals[i];
                out_tangents2[index] += out_tangents[i];
                out_bitangents2[index] += out_bitangents[i];
                //out_indicesNum2[index]++;
            }
            else
            { // If not, it needs to be added in the output data.
                out_vertices2.push_back(out_vertices[i]);
                out_uvs2.push_back(out_uvs[i]);
                out_normals2.push_back(out_normals[i]);
                out_tangents2.push_back(out_tangents[i]);
                out_bitangents2.push_back(out_bitangents[i]);
                out_indices2.push_back((unsigned short)out_vertices2.size() - 1);
            }
        }

        //  ////< XZH handling duplicates one node in the tris
        //std::vector<unsigned short> out_indicesNum3;
        //std::vector<Vec3d> out_vertices3;
        //std::vector<Vec2d> out_uvs3;
        //std::vector<Vec3d> out_normals3;
        //std::vector<Vec3d> out_tangents3;
        //std::vector<Vec3d> out_bitangents3;
        //out_indicesNum3.resize(temp_vertices.size());
        //out_vertices3.resize(temp_vertices.size());
        //out_uvs3.resize(temp_vertices.size());
        //out_normals3.resize(temp_vertices.size());
        //out_tangents3.resize(temp_vertices.size());
        //out_bitangents3.resize(temp_vertices.size());
        //for (unsigned int i = 0; i < out_vertices.size(); i++)
        //{
        //    // Get the indices of its attributes
        //    unsigned int vertexIndex = vertexIndices[i]-1;
        //    unsigned int uvIndex = uvIndices[i]-1;
        //    unsigned int normalIndex = normalIndices[i]-1;
        //    out_indicesNum3[vertexIndex]++;

        //    out_uvs3[vertexIndex] += temp_uvs[uvIndex]; //+= out_uvs[uvIndex];            //    Vec2d uv = temp_uvs[uvIndex - 1];
        //    out_normals3[vertexIndex] += temp_normals[normalIndex]; //  out_normals[normalIndex];
        //    out_tangents3[vertexIndex] += out_tangents[vertexIndex];
        //    out_bitangents3[vertexIndex] += out_bitangents[vertexIndex];
        //}
        //for (int i = 0; i < temp_vertices.size(); i++)
        //{
        //    out_uvs2[i] = out_uvs3[i] / out_indicesNum3[i];
        //    out_normals2[i] = out_normals3[i] / out_indicesNum3[i];
        //    out_tangents2[i] = out_tangents3[i] / out_indicesNum3[i];
        //    out_bitangents2[i] = out_bitangents3[i] / out_indicesNum3[i];
        //}
        // VBO_TBN END 

        StdVectorOfVec3d poses;
        //for (int i = 0; i < temp_vertices.size(); i++)
        //{
        //    poses.push_back(temp_vertices[i]);
        //}
        for (int i = 0; i < out_vertices2.size(); i++)
        {
            poses.push_back(out_vertices2[i]);
        }
        if ((poses.size() == 1062) || (poses.size() == 2686) || (poses.size() == 5245) || (poses.size() == 1880) || (poses.size() == 2018) || (poses.size() == 56) || (poses.size() == 96) || (poses.size() == 2334) )  // tool 1255) // colon 1 2 3: 1062 2686 5245  cubeTet: 
        {
            for (int i = 0; i < poses.size(); i++)
            {
                poses[i] = poses[i] + Vec3d(0, -5, 15);
            }
        }
        std::vector<SurfaceMesh::TriangleArray> triangles;
        for (int i = 0; i < vertexIndices.size(); i=i+3)
        {
            //SurfaceMesh::TriangleArray tmp = { vertexIndices[i] - 1, vertexIndices[i + 1] - 1, vertexIndices[i + 2] - 1 };
            SurfaceMesh::TriangleArray tmp = { out_indices2[i], out_indices2[i + 1], out_indices2[i + 2] };
            triangles.push_back(tmp);
        }
        StdVectorOfVec3d normals;
        for (int i = 0; i < out_normals2.size(); i++)
        {
            normals.push_back(out_normals2[i]);
        }

        StdVectorOfVectorf UVs(poses.size());
        //auto texcoords = importedMesh->mTextureCoords[0];
        for (unsigned int i = 0; i < poses.size(); i++)
        {
            Vectorf UV(2);
            UV[0] = out_uvs2[i].x(); //  .TextureCoordinate.x(); // TCoords[i].x(); // texcoords[i].x;
            UV[1] = out_uvs2[i].y(); //  .TextureCoordinate.y(); // TCoords[i].y(); //  texcoords[i].y;
            UVs[i] = UV;
        }

        StdVectorOfVec3d tangentsVec;
        StdVectorOfVec3d bitangentsVec;
        for (int i = 0; i < out_tangents2.size(); i++)
        {
            tangentsVec.push_back(out_tangents2[i]);
            bitangentsVec.push_back(out_bitangents2[i]);
        }

          ////< XZH colon should be subdivsion
        //bSubdivsion = false;
        if (bSubdivsion)
        {
              ////< XZH subdivision 1
            std::vector<SurfaceMesh::TriangleArray> triListsTmp; // = triangles;
            StdVectorOfVec3d posVisualListTmp = poses;
            StdVectorOfVec3d normalsTmp = normals;
            StdVectorOfVectorf UVsTmp = UVs;
            StdVectorOfVec3d tangentsVecTmp = tangentsVec;
            StdVectorOfVec3d bitangentsVecTmp = bitangentsVec;

            std::unordered_map<U64, int> m_mapEdgeTriIndex;
            std::unordered_map<U64, int>::iterator it;
            int eIdx = poses.size() - 1; // -1;
            const double OPTRADIUS = 2.5; // 2.0
            Vec3d posRef = Vec3d(-3.0, 4.4, -5.38); // posTetlist[minVert];
            for (int i = 0; i < triangles.size(); i++)
            {
                auto& tri = triangles[i];
                int vert0 = tri[0];
                int vert1 = tri[1];
                int vert2 = tri[2];
                //auto& vtt = tri[0];
                auto& pos0 = poses[vert0];
                auto& pos1 = poses[vert1];
                auto& pos2 = poses[vert2];

                auto& norm0 = normals[vert0];
                auto& norm1 = normals[vert1];
                auto& norm2 = normals[vert2];

                auto& uvs0x = UVs[vert0].x(); 
                auto& uvs0y = UVs[vert0].y();
                auto& uvs1x = UVs[vert1].x();
                auto& uvs1y = UVs[vert1].y();
                auto& uvs2x = UVs[vert2].x();
                auto& uvs2y = UVs[vert2].y();

                auto& tans0 = tangentsVec[vert0];
                auto& tans1 = tangentsVec[vert1];
                auto& tans2 = tangentsVec[vert2];

                auto& bitans0 = bitangentsVec[vert0];
                auto& bitans1 = bitangentsVec[vert1];
                auto& bitans2 = bitangentsVec[vert2];

                double la = (pos0 - pos1).norm();
                double lb = (pos1 - pos2).norm();
                double lc = (pos0 - pos2).norm();
                // Heron's formula
                double arg =
                    (la + (lb + lc)) *
                    (lc - (la - lb)) *
                    (lc + (la - lb)) *
                    (la + (lb - lc));
                double area = 0.25 * sqrt(arg);
                // in a circle outside not subdivision
                Vec3d center = 0.333333*(pos0 + pos1 + pos2);

                // in a circle outside not subdivision
                //if ((center - posRef).norm() > OPTRADIUS)   //  area 0.0035
                if (((center - posRef).norm() > OPTRADIUS) || (area<0.0035))
                {
                    triListsTmp.push_back(tri);
                    continue;
                }
                // end a circle

                int newVert0, newVert1, newVert2;
                Vec3d newPos0, newPos1, newPos2; // 0->1 ed0, 1->2 ed1, 0->2 ed2
                //vert0 = 100000;
                //vtt = 80000;
                //printf("edge nodes %d %d ", m0, m1);
                int m0 = vert0, m1 = vert1;
                if (m0 > m1)
                {
                    std::swap(m0, m1);
                }
                U64 edgekey0 = HASHEDGEID_FROM_IDX(m0, m1);

                m0 = vert1; m1 = vert2;
                if (m0 > m1)
                {
                    std::swap(m0, m1);
                }
                U64 edgekey1 = HASHEDGEID_FROM_IDX(m0, m1);

                m0 = vert0; m1 = vert2;
                if (m0 > m1)
                {
                    std::swap(m0, m1);
                }
                U64 edgekey2 = HASHEDGEID_FROM_IDX(m0, m1);

                it = m_mapEdgeTriIndex.find(edgekey0);
                if (it == m_mapEdgeTriIndex.end())  // not add this middle point yet
                {
                    eIdx++;
                    m_mapEdgeTriIndex.insert(std::make_pair(edgekey0, eIdx)); // from 0 start
                    newVert0 = eIdx; // in the first edge
                    posVisualListTmp.resize(eIdx + 1);
                    posVisualListTmp[eIdx] = (pos0 + pos1)*0.5;
                    normalsTmp.resize(eIdx + 1);
                    normalsTmp[eIdx] = (norm0 + norm1)*0.5;
                    //UVsTmp.resize(eIdx + 1);
                    Vectorf UV(2);
                    UV[0] = (uvs0x + uvs1x)*0.5;
                    UV[1] = (uvs0y + uvs1y)*0.5;
                    UVsTmp.push_back(UV);
                    //UVsTmp[eIdx].x() = (uvs0x + uvs1x)*0.5;
                    //UVsTmp[eIdx].y() = (uvs0y + uvs1y)*0.5;
                    tangentsVecTmp.resize(eIdx + 1);
                    tangentsVecTmp[eIdx] = (tans0 + tans1)*0.5;
                    bitangentsVecTmp.resize(eIdx + 1);
                    bitangentsVecTmp[eIdx] = (bitans0 + bitans1)*0.5;
                }
                else  //existing find out
                {
                    newVert0 = it->second; // in the first edge
                }

                it = m_mapEdgeTriIndex.find(edgekey1);
                if (it == m_mapEdgeTriIndex.end())
                {
                    eIdx++;
                    m_mapEdgeTriIndex.insert(std::make_pair(edgekey1, eIdx)); // from 0 start
                    newVert1 = eIdx; // in the 2nd edge
                    posVisualListTmp.resize(eIdx + 1);
                    posVisualListTmp[eIdx] = (pos1 + pos2)*0.5;
                    normalsTmp.resize(eIdx + 1);
                    normalsTmp[eIdx] = (norm1 + norm2)*0.5;
                    //UVsTmp.resize(eIdx + 1);
                    Vectorf UV(2);
                    UV[0] = (uvs1x + uvs2x)*0.5;
                    UV[1] = (uvs1y + uvs2y)*0.5;
                    UVsTmp.push_back(UV);
                    //UVsTmp[eIdx].x() = (uvs0x + uvs1x)*0.5;
                    //UVsTmp[eIdx].y() = (uvs0y + uvs1y)*0.5;
                    tangentsVecTmp.resize(eIdx + 1);
                    tangentsVecTmp[eIdx] = (tans1 + tans2)*0.5;
                    bitangentsVecTmp.resize(eIdx + 1);
                    bitangentsVecTmp[eIdx] = (bitans1 + bitans2)*0.5;
                }
                else
                {
                    newVert1 = it->second; // in the 2nd edge
                }

                it = m_mapEdgeTriIndex.find(edgekey2);
                if (it == m_mapEdgeTriIndex.end())
                {
                    eIdx++;
                    m_mapEdgeTriIndex.insert(std::make_pair(edgekey2, eIdx)); // from 0 start
                    newVert2 = eIdx; // in the 2nd edge
                    posVisualListTmp.resize(eIdx + 1);
                    posVisualListTmp[eIdx] = (pos0 + pos2)*0.5;
                    normalsTmp.resize(eIdx + 1);
                    normalsTmp[eIdx] = (norm0 + norm2)*0.5;
                    //UVsTmp.resize(eIdx + 1);
                    Vectorf UV(2);
                    UV[0] = (uvs0x + uvs2x)*0.5;
                    UV[1] = (uvs0y + uvs2y)*0.5;
                    UVsTmp.push_back(UV);
                    //UVsTmp[eIdx].x() = (uvs0x + uvs1x)*0.5;
                    //UVsTmp[eIdx].y() = (uvs0y + uvs1y)*0.5;
                    tangentsVecTmp.resize(eIdx + 1);
                    tangentsVecTmp[eIdx] = (tans0 + tans2)*0.5;
                    bitangentsVecTmp.resize(eIdx + 1);
                    bitangentsVecTmp[eIdx] = (bitans0 + bitans2)*0.5;
                }
                else
                {
                    newVert2 = it->second; // in the 3rd edge
                }

                SurfaceMesh::TriangleArray tmpTri0 = { { vert0, newVert0, newVert2 } };
                SurfaceMesh::TriangleArray tmpTri1 = { { newVert0, vert1, newVert1 } };
                SurfaceMesh::TriangleArray tmpTri2 = { { newVert0, newVert1, newVert2 } };
                SurfaceMesh::TriangleArray tmpTri3 = { { newVert2, newVert1, vert2 } };
                triListsTmp.push_back(tmpTri0);
                triListsTmp.push_back(tmpTri1);
                triListsTmp.push_back(tmpTri2);
                triListsTmp.push_back(tmpTri3);
            }
            // END subdivision 1

            ////< XZH subdivision 2
            std::vector<SurfaceMesh::TriangleArray> triListsTmp2; // = triListsTmp;
            StdVectorOfVec3d posVisualListTmp2 = posVisualListTmp;
            StdVectorOfVec3d normalsTmp2 = normalsTmp;
            StdVectorOfVectorf UVsTmp2 = UVsTmp;
            StdVectorOfVec3d tangentsVecTmp2 = tangentsVecTmp;
            StdVectorOfVec3d bitangentsVecTmp2 = bitangentsVecTmp;

            std::unordered_map<U64, int> m_mapEdgeTriIndex2;
            std::unordered_map<U64, int>::iterator it2;
            eIdx = posVisualListTmp.size() - 1; // -1;
            for (int i = 0; i < triListsTmp.size(); i++)
            {
                auto& tri = triListsTmp[i];
                int vert0 = tri[0];
                int vert1 = tri[1];
                int vert2 = tri[2];
                //auto& vtt = tri[0];
                auto& pos0 = posVisualListTmp[vert0];
                auto& pos1 = posVisualListTmp[vert1];
                auto& pos2 = posVisualListTmp[vert2];

                auto& norm0 = normalsTmp[vert0];
                auto& norm1 = normalsTmp[vert1];
                auto& norm2 = normalsTmp[vert2];

                auto& uvs0x = UVsTmp[vert0].x();
                auto& uvs0y = UVsTmp[vert0].y();
                auto& uvs1x = UVsTmp[vert1].x();
                auto& uvs1y = UVsTmp[vert1].y();
                auto& uvs2x = UVsTmp[vert2].x();
                auto& uvs2y = UVsTmp[vert2].y();

                auto& tans0 = tangentsVecTmp[vert0];
                auto& tans1 = tangentsVecTmp[vert1];
                auto& tans2 = tangentsVecTmp[vert2];

                auto& bitans0 = bitangentsVecTmp[vert0];
                auto& bitans1 = bitangentsVecTmp[vert1];
                auto& bitans2 = bitangentsVecTmp[vert2];

                double la = (pos0 - pos1).norm();
                double lb = (pos1 - pos2).norm();
                double lc = (pos0 - pos2).norm();
                // Heron's formula
                double arg =
                    (la + (lb + lc)) *
                    (lc - (la - lb)) *
                    (lc + (la - lb)) *
                    (la + (lb - lc));
                double area = 0.25 * sqrt(arg);
                // in a circle outside not subdivision
                Vec3d center = 0.333333*(pos0 + pos1 + pos2);

                // in a circle outside not subdivision
                //if ((center - posRef).norm() > OPTRADIUS)   //  area 0.0035
                if (((center - posRef).norm() > OPTRADIUS) || (area<0.0035))
                {
                    triListsTmp2.push_back(tri);
                    continue;
                }
                // end a circle

                int newVert0, newVert1, newVert2;
                Vec3d newPos0, newPos1, newPos2; // 0->1 ed0, 1->2 ed1, 0->2 ed2
                //vert0 = 100000;
                //vtt = 80000;
                //printf("edge nodes %d %d ", m0, m1);
                int m0 = vert0, m1 = vert1;
                if (m0 > m1)
                {
                    std::swap(m0, m1);
                }
                U64 edgekey0 = HASHEDGEID_FROM_IDX(m0, m1);

                m0 = vert1; m1 = vert2;
                if (m0 > m1)
                {
                    std::swap(m0, m1);
                }
                U64 edgekey1 = HASHEDGEID_FROM_IDX(m0, m1);

                m0 = vert0; m1 = vert2;
                if (m0 > m1)
                {
                    std::swap(m0, m1);
                }
                U64 edgekey2 = HASHEDGEID_FROM_IDX(m0, m1);

                it = m_mapEdgeTriIndex2.find(edgekey0);
                if (it == m_mapEdgeTriIndex2.end())  // not add this middle point yet
                {
                    eIdx++;
                    m_mapEdgeTriIndex2.insert(std::make_pair(edgekey0, eIdx)); // from 0 start
                    newVert0 = eIdx; // in the first edge
                    posVisualListTmp2.resize(eIdx + 1);
                    posVisualListTmp2[eIdx] = (pos0 + pos1)*0.5;
                    normalsTmp2.resize(eIdx + 1);
                    normalsTmp2[eIdx] = (norm0 + norm1)*0.5;
                    //UVsTmp.resize(eIdx + 1);
                    Vectorf UV(2);
                    UV[0] = (uvs0x + uvs1x)*0.5;
                    UV[1] = (uvs0y + uvs1y)*0.5;
                    UVsTmp2.push_back(UV);
                    //UVsTmp[eIdx].x() = (uvs0x + uvs1x)*0.5;
                    //UVsTmp[eIdx].y() = (uvs0y + uvs1y)*0.5;
                    tangentsVecTmp2.resize(eIdx + 1);
                    tangentsVecTmp2[eIdx] = (tans0 + tans1)*0.5;
                    bitangentsVecTmp2.resize(eIdx + 1);
                    bitangentsVecTmp2[eIdx] = (bitans0 + bitans1)*0.5;
                }
                else  //existing find out
                {
                    newVert0 = it->second; // in the first edge
                }

                it = m_mapEdgeTriIndex2.find(edgekey1);
                if (it == m_mapEdgeTriIndex2.end())
                {
                    eIdx++;
                    m_mapEdgeTriIndex2.insert(std::make_pair(edgekey1, eIdx)); // from 0 start
                    newVert1 = eIdx; // in the 2nd edge
                    posVisualListTmp2.resize(eIdx + 1);
                    posVisualListTmp2[eIdx] = (pos1 + pos2)*0.5;
                    normalsTmp2.resize(eIdx + 1);
                    normalsTmp2[eIdx] = (norm1 + norm2)*0.5;
                    //UVsTmp.resize(eIdx + 1);
                    Vectorf UV(2);
                    UV[0] = (uvs1x + uvs2x)*0.5;
                    UV[1] = (uvs1y + uvs2y)*0.5;
                    UVsTmp2.push_back(UV);
                    //UVsTmp[eIdx].x() = (uvs0x + uvs1x)*0.5;
                    //UVsTmp[eIdx].y() = (uvs0y + uvs1y)*0.5;
                    tangentsVecTmp2.resize(eIdx + 1);
                    tangentsVecTmp2[eIdx] = (tans1 + tans2)*0.5;
                    bitangentsVecTmp2.resize(eIdx + 1);
                    bitangentsVecTmp2[eIdx] = (bitans1 + bitans2)*0.5;
                }
                else
                {
                    newVert1 = it->second; // in the 2nd edge
                }

                it = m_mapEdgeTriIndex2.find(edgekey2);
                if (it == m_mapEdgeTriIndex2.end())
                {
                    eIdx++;
                    m_mapEdgeTriIndex2.insert(std::make_pair(edgekey2, eIdx)); // from 0 start
                    newVert2 = eIdx; // in the 2nd edge
                    posVisualListTmp2.resize(eIdx + 1);
                    posVisualListTmp2[eIdx] = (pos0 + pos2)*0.5;
                    normalsTmp2.resize(eIdx + 1);
                    normalsTmp2[eIdx] = (norm0 + norm2)*0.5;
                    //UVsTmp.resize(eIdx + 1);
                    Vectorf UV(2);
                    UV[0] = (uvs0x + uvs2x)*0.5;
                    UV[1] = (uvs0y + uvs2y)*0.5;
                    UVsTmp2.push_back(UV);
                    //UVsTmp[eIdx].x() = (uvs0x + uvs1x)*0.5;
                    //UVsTmp[eIdx].y() = (uvs0y + uvs1y)*0.5;
                    tangentsVecTmp2.resize(eIdx + 1);
                    tangentsVecTmp2[eIdx] = (tans0 + tans2)*0.5;
                    bitangentsVecTmp2.resize(eIdx + 1);
                    bitangentsVecTmp2[eIdx] = (bitans0 + bitans2)*0.5;
                }
                else
                {
                    newVert2 = it->second; // in the 3rd edge
                }

                SurfaceMesh::TriangleArray tmpTri0 = { { vert0, newVert0, newVert2 } };
                SurfaceMesh::TriangleArray tmpTri1 = { { newVert0, vert1, newVert1 } };
                SurfaceMesh::TriangleArray tmpTri2 = { { newVert0, newVert1, newVert2 } };
                SurfaceMesh::TriangleArray tmpTri3 = { { newVert2, newVert1, vert2 } };
                triListsTmp2.push_back(tmpTri0);
                triListsTmp2.push_back(tmpTri1);
                triListsTmp2.push_back(tmpTri2);
                triListsTmp2.push_back(tmpTri3);
            }
            // END subdivision 2
            //mesh->initialize(posVisualListTmp2, triListsTmp2, normalsTmp2, false);
            //mesh->setVertexNormals(normalsTmp2);

            //mesh->setDefaultTCoords("tCoords");
            //mesh->setPointDataArray("tCoords", UVsTmp2);

            //mesh->setVertexTangents(tangentsVecTmp2);
            ////< XZH subdivision 3
            std::vector<SurfaceMesh::TriangleArray> triListsTmp3; // = triListsTmp;
            StdVectorOfVec3d posVisualListTmp3 = posVisualListTmp2;
            StdVectorOfVec3d normalsTmp3 = normalsTmp2;
            StdVectorOfVectorf UVsTmp3 = UVsTmp2;
            StdVectorOfVec3d tangentsVecTmp3 = tangentsVecTmp2;
            StdVectorOfVec3d bitangentsVecTmp3 = bitangentsVecTmp2;

            std::unordered_map<U64, int> m_mapEdgeTriIndex3;
            std::unordered_map<U64, int>::iterator it3;
            eIdx = posVisualListTmp2.size() - 1; // -1;
            for (int i = 0; i < triListsTmp2.size(); i++)
            {
                auto& tri = triListsTmp2[i];
                int vert0 = tri[0];
                int vert1 = tri[1];
                int vert2 = tri[2];
                //auto& vtt = tri[0];
                auto& pos0 = posVisualListTmp2[vert0];
                auto& pos1 = posVisualListTmp2[vert1];
                auto& pos2 = posVisualListTmp2[vert2];

                auto& norm0 = normalsTmp2[vert0];
                auto& norm1 = normalsTmp2[vert1];
                auto& norm2 = normalsTmp2[vert2];

                auto& uvs0x = UVsTmp2[vert0].x();
                auto& uvs0y = UVsTmp2[vert0].y();
                auto& uvs1x = UVsTmp2[vert1].x();
                auto& uvs1y = UVsTmp2[vert1].y();
                auto& uvs2x = UVsTmp2[vert2].x();
                auto& uvs2y = UVsTmp2[vert2].y();

                auto& tans0 = tangentsVecTmp2[vert0];
                auto& tans1 = tangentsVecTmp2[vert1];
                auto& tans2 = tangentsVecTmp2[vert2];

                auto& bitans0 = bitangentsVecTmp2[vert0];
                auto& bitans1 = bitangentsVecTmp2[vert1];
                auto& bitans2 = bitangentsVecTmp2[vert2];

                double la = (pos0 - pos1).norm();
                double lb = (pos1 - pos2).norm();
                double lc = (pos0 - pos2).norm();
                // Heron's formula
                double arg =
                    (la + (lb + lc)) *
                    (lc - (la - lb)) *
                    (lc + (la - lb)) *
                    (la + (lb - lc));
                double area = 0.25 * sqrt(arg);
                // in a circle outside not subdivision
                Vec3d center = 0.333333*(pos0 + pos1 + pos2);

                // in a circle outside not subdivision
                //if ((center - posRef).norm() > OPTRADIUS)   //  area 0.0035
                if (((center - posRef).norm() > OPTRADIUS) || (area < 0.0035))
                {
                    triListsTmp3.push_back(tri);
                    continue;
                }
                // end a circle

                int newVert0, newVert1, newVert2;
                Vec3d newPos0, newPos1, newPos2; // 0->1 ed0, 1->2 ed1, 0->2 ed2
                //vert0 = 100000;
                //vtt = 80000;
                //printf("edge nodes %d %d ", m0, m1);
                int m0 = vert0, m1 = vert1;
                if (m0 > m1)
                {
                    std::swap(m0, m1);
                }
                U64 edgekey0 = HASHEDGEID_FROM_IDX(m0, m1);

                m0 = vert1; m1 = vert2;
                if (m0 > m1)
                {
                    std::swap(m0, m1);
                }
                U64 edgekey1 = HASHEDGEID_FROM_IDX(m0, m1);

                m0 = vert0; m1 = vert2;
                if (m0 > m1)
                {
                    std::swap(m0, m1);
                }
                U64 edgekey2 = HASHEDGEID_FROM_IDX(m0, m1);

                it = m_mapEdgeTriIndex3.find(edgekey0);
                if (it == m_mapEdgeTriIndex3.end())  // not add this middle point yet
                {
                    eIdx++;
                    m_mapEdgeTriIndex3.insert(std::make_pair(edgekey0, eIdx)); // from 0 start
                    newVert0 = eIdx; // in the first edge
                    posVisualListTmp3.resize(eIdx + 1);
                    posVisualListTmp3[eIdx] = (pos0 + pos1)*0.5;
                    normalsTmp3.resize(eIdx + 1);
                    normalsTmp3[eIdx] = (norm0 + norm1)*0.5;
                    //UVsTmp.resize(eIdx + 1);
                    Vectorf UV(2);
                    UV[0] = (uvs0x + uvs1x)*0.5;
                    UV[1] = (uvs0y + uvs1y)*0.5;
                    UVsTmp3.push_back(UV);
                    //UVsTmp[eIdx].x() = (uvs0x + uvs1x)*0.5;
                    //UVsTmp[eIdx].y() = (uvs0y + uvs1y)*0.5;
                    tangentsVecTmp3.resize(eIdx + 1);
                    tangentsVecTmp3[eIdx] = (tans0 + tans1)*0.5;
                    bitangentsVecTmp3.resize(eIdx + 1);
                    bitangentsVecTmp3[eIdx] = (bitans0 + bitans1)*0.5;
                }
                else  //existing find out
                {
                    newVert0 = it->second; // in the first edge
                }

                it = m_mapEdgeTriIndex3.find(edgekey1);
                if (it == m_mapEdgeTriIndex3.end())
                {
                    eIdx++;
                    m_mapEdgeTriIndex3.insert(std::make_pair(edgekey1, eIdx)); // from 0 start
                    newVert1 = eIdx; // in the 2nd edge
                    posVisualListTmp3.resize(eIdx + 1);
                    posVisualListTmp3[eIdx] = (pos1 + pos2)*0.5;
                    normalsTmp3.resize(eIdx + 1);
                    normalsTmp3[eIdx] = (norm1 + norm2)*0.5;
                    //UVsTmp.resize(eIdx + 1);
                    Vectorf UV(2);
                    UV[0] = (uvs1x + uvs2x)*0.5;
                    UV[1] = (uvs1y + uvs2y)*0.5;
                    UVsTmp3.push_back(UV);
                    //UVsTmp[eIdx].x() = (uvs0x + uvs1x)*0.5;
                    //UVsTmp[eIdx].y() = (uvs0y + uvs1y)*0.5;
                    tangentsVecTmp3.resize(eIdx + 1);
                    tangentsVecTmp3[eIdx] = (tans1 + tans2)*0.5;
                    bitangentsVecTmp3.resize(eIdx + 1);
                    bitangentsVecTmp3[eIdx] = (bitans1 + bitans2)*0.5;
                }
                else
                {
                    newVert1 = it->second; // in the 2nd edge
                }

                it = m_mapEdgeTriIndex3.find(edgekey2);
                if (it == m_mapEdgeTriIndex3.end())
                {
                    eIdx++;
                    m_mapEdgeTriIndex3.insert(std::make_pair(edgekey2, eIdx)); // from 0 start
                    newVert2 = eIdx; // in the 2nd edge
                    posVisualListTmp3.resize(eIdx + 1);
                    posVisualListTmp3[eIdx] = (pos0 + pos2)*0.5;
                    normalsTmp3.resize(eIdx + 1);
                    normalsTmp3[eIdx] = (norm0 + norm2)*0.5;
                    //UVsTmp.resize(eIdx + 1);
                    Vectorf UV(2);
                    UV[0] = (uvs0x + uvs2x)*0.5;
                    UV[1] = (uvs0y + uvs2y)*0.5;
                    UVsTmp3.push_back(UV);
                    //UVsTmp[eIdx].x() = (uvs0x + uvs1x)*0.5;
                    //UVsTmp[eIdx].y() = (uvs0y + uvs1y)*0.5;
                    tangentsVecTmp3.resize(eIdx + 1);
                    tangentsVecTmp3[eIdx] = (tans0 + tans2)*0.5;
                    bitangentsVecTmp3.resize(eIdx + 1);
                    bitangentsVecTmp3[eIdx] = (bitans0 + bitans2)*0.5;
                }
                else
                {
                    newVert2 = it->second; // in the 3rd edge
                }

                SurfaceMesh::TriangleArray tmpTri0 = { { vert0, newVert0, newVert2 } };
                SurfaceMesh::TriangleArray tmpTri1 = { { newVert0, vert1, newVert1 } };
                SurfaceMesh::TriangleArray tmpTri2 = { { newVert0, newVert1, newVert2 } };
                SurfaceMesh::TriangleArray tmpTri3 = { { newVert2, newVert1, vert2 } };
                triListsTmp3.push_back(tmpTri0);
                triListsTmp3.push_back(tmpTri1);
                triListsTmp3.push_back(tmpTri2);
                triListsTmp3.push_back(tmpTri3);
            }         

            //mesh->initialize(posVisualListTmp3, triListsTmp3, normalsTmp3, false);
            //mesh->setVertexNormals(normalsTmp3);

            //mesh->setDefaultTCoords("tCoords");
            //mesh->setPointDataArray("tCoords", UVsTmp3);

            //mesh->setVertexTangents(tangentsVecTmp3);
            //mesh->setIsColon(bSubdivsion);
            // END subdivision 3

            ////< XZH subdivision 4
            std::vector<SurfaceMesh::TriangleArray> triListsTmp4; // = triListsTmp;
            StdVectorOfVec3d posVisualListTmp4 = posVisualListTmp3;
            StdVectorOfVec3d normalsTmp4 = normalsTmp3;
            StdVectorOfVectorf UVsTmp4 = UVsTmp3;
            StdVectorOfVec3d tangentsVecTmp4 = tangentsVecTmp3;
            StdVectorOfVec3d bitangentsVecTmp4 = bitangentsVecTmp3;

            std::unordered_map<U64, int> m_mapEdgeTriIndex4;
            std::unordered_map<U64, int>::iterator it4;
            eIdx = posVisualListTmp3.size() - 1; // -1;
            for (int i = 0; i < triListsTmp3.size(); i++)
            {
                auto& tri = triListsTmp3[i];
                int vert0 = tri[0];
                int vert1 = tri[1];
                int vert2 = tri[2];
                //auto& vtt = tri[0];
                auto& pos0 = posVisualListTmp3[vert0];
                auto& pos1 = posVisualListTmp3[vert1];
                auto& pos2 = posVisualListTmp3[vert2];

                auto& norm0 = normalsTmp3[vert0];
                auto& norm1 = normalsTmp3[vert1];
                auto& norm2 = normalsTmp3[vert2];

                auto& uvs0x = UVsTmp3[vert0].x();
                auto& uvs0y = UVsTmp3[vert0].y();
                auto& uvs1x = UVsTmp3[vert1].x();
                auto& uvs1y = UVsTmp3[vert1].y();
                auto& uvs2x = UVsTmp3[vert2].x();
                auto& uvs2y = UVsTmp3[vert2].y();

                auto& tans0 = tangentsVecTmp3[vert0];
                auto& tans1 = tangentsVecTmp3[vert1];
                auto& tans2 = tangentsVecTmp3[vert2];

                auto& bitans0 = bitangentsVecTmp3[vert0];
                auto& bitans1 = bitangentsVecTmp3[vert1];
                auto& bitans2 = bitangentsVecTmp3[vert2];

                double la = (pos0 - pos1).norm();
                double lb = (pos1 - pos2).norm();
                double lc = (pos0 - pos2).norm();
                // Heron's formula
                double arg =
                    (la + (lb + lc)) *
                    (lc - (la - lb)) *
                    (lc + (la - lb)) *
                    (la + (lb - lc));
                double area = 0.25 * sqrt(arg);
                // in a circle outside not subdivision
                Vec3d center = 0.333333*(pos0 + pos1 + pos2);

                // in a circle outside not subdivision
                //if ((center - posRef).norm() > OPTRADIUS)   //  area 0.0035
                if (((center - posRef).norm() > OPTRADIUS) || (area < 0.0035))
                {
                    triListsTmp4.push_back(tri);
                    continue;
                }
                // end a circle

                int newVert0, newVert1, newVert2;
                Vec3d newPos0, newPos1, newPos2; // 0->1 ed0, 1->2 ed1, 0->2 ed2
                //vert0 = 100000;
                //vtt = 80000;
                //printf("edge nodes %d %d ", m0, m1);
                int m0 = vert0, m1 = vert1;
                if (m0 > m1)
                {
                    std::swap(m0, m1);
                }
                U64 edgekey0 = HASHEDGEID_FROM_IDX(m0, m1);

                m0 = vert1; m1 = vert2;
                if (m0 > m1)
                {
                    std::swap(m0, m1);
                }
                U64 edgekey1 = HASHEDGEID_FROM_IDX(m0, m1);

                m0 = vert0; m1 = vert2;
                if (m0 > m1)
                {
                    std::swap(m0, m1);
                }
                U64 edgekey2 = HASHEDGEID_FROM_IDX(m0, m1);

                it = m_mapEdgeTriIndex4.find(edgekey0);
                if (it == m_mapEdgeTriIndex4.end())  // not add this middle point yet
                {
                    eIdx++;
                    m_mapEdgeTriIndex4.insert(std::make_pair(edgekey0, eIdx)); // from 0 start
                    newVert0 = eIdx; // in the first edge
                    posVisualListTmp4.resize(eIdx + 1);
                    posVisualListTmp4[eIdx] = (pos0 + pos1)*0.5;
                    normalsTmp4.resize(eIdx + 1);
                    normalsTmp4[eIdx] = (norm0 + norm1)*0.5;
                    //UVsTmp.resize(eIdx + 1);
                    Vectorf UV(2);
                    UV[0] = (uvs0x + uvs1x)*0.5;
                    UV[1] = (uvs0y + uvs1y)*0.5;
                    UVsTmp4.push_back(UV);
                    //UVsTmp[eIdx].x() = (uvs0x + uvs1x)*0.5;
                    //UVsTmp[eIdx].y() = (uvs0y + uvs1y)*0.5;
                    tangentsVecTmp4.resize(eIdx + 1);
                    tangentsVecTmp4[eIdx] = (tans0 + tans1)*0.5;
                    bitangentsVecTmp4.resize(eIdx + 1);
                    bitangentsVecTmp4[eIdx] = (bitans0 + bitans1)*0.5;
                }
                else  //existing find out
                {
                    newVert0 = it->second; // in the first edge
                }

                it = m_mapEdgeTriIndex4.find(edgekey1);
                if (it == m_mapEdgeTriIndex4.end())
                {
                    eIdx++;
                    m_mapEdgeTriIndex4.insert(std::make_pair(edgekey1, eIdx)); // from 0 start
                    newVert1 = eIdx; // in the 2nd edge
                    posVisualListTmp4.resize(eIdx + 1);
                    posVisualListTmp4[eIdx] = (pos1 + pos2)*0.5;
                    normalsTmp4.resize(eIdx + 1);
                    normalsTmp4[eIdx] = (norm1 + norm2)*0.5;
                    //UVsTmp.resize(eIdx + 1);
                    Vectorf UV(2);
                    UV[0] = (uvs1x + uvs2x)*0.5;
                    UV[1] = (uvs1y + uvs2y)*0.5;
                    UVsTmp4.push_back(UV);
                    //UVsTmp[eIdx].x() = (uvs0x + uvs1x)*0.5;
                    //UVsTmp[eIdx].y() = (uvs0y + uvs1y)*0.5;
                    tangentsVecTmp4.resize(eIdx + 1);
                    tangentsVecTmp4[eIdx] = (tans1 + tans2)*0.5;
                    bitangentsVecTmp4.resize(eIdx + 1);
                    bitangentsVecTmp4[eIdx] = (bitans1 + bitans2)*0.5;
                }
                else
                {
                    newVert1 = it->second; // in the 2nd edge
                }

                it = m_mapEdgeTriIndex4.find(edgekey2);
                if (it == m_mapEdgeTriIndex4.end())
                {
                    eIdx++;
                    m_mapEdgeTriIndex4.insert(std::make_pair(edgekey2, eIdx)); // from 0 start
                    newVert2 = eIdx; // in the 2nd edge
                    posVisualListTmp4.resize(eIdx + 1);
                    posVisualListTmp4[eIdx] = (pos0 + pos2)*0.5;
                    normalsTmp4.resize(eIdx + 1);
                    normalsTmp4[eIdx] = (norm0 + norm2)*0.5;
                    //UVsTmp.resize(eIdx + 1);
                    Vectorf UV(2);
                    UV[0] = (uvs0x + uvs2x)*0.5;
                    UV[1] = (uvs0y + uvs2y)*0.5;
                    UVsTmp4.push_back(UV);
                    //UVsTmp[eIdx].x() = (uvs0x + uvs1x)*0.5;
                    //UVsTmp[eIdx].y() = (uvs0y + uvs1y)*0.5;
                    tangentsVecTmp4.resize(eIdx + 1);
                    tangentsVecTmp4[eIdx] = (tans0 + tans2)*0.5;
                    bitangentsVecTmp4.resize(eIdx + 1);
                    bitangentsVecTmp4[eIdx] = (bitans0 + bitans2)*0.5;
                }
                else
                {
                    newVert2 = it->second; // in the 3rd edge
                }

                SurfaceMesh::TriangleArray tmpTri0 = { { vert0, newVert0, newVert2 } };
                SurfaceMesh::TriangleArray tmpTri1 = { { newVert0, vert1, newVert1 } };
                SurfaceMesh::TriangleArray tmpTri2 = { { newVert0, newVert1, newVert2 } };
                SurfaceMesh::TriangleArray tmpTri3 = { { newVert2, newVert1, vert2 } };
                triListsTmp4.push_back(tmpTri0);
                triListsTmp4.push_back(tmpTri1);
                triListsTmp4.push_back(tmpTri2);
                triListsTmp4.push_back(tmpTri3);
            }

            mesh->initialize(posVisualListTmp4, triListsTmp4, normalsTmp4, false);
            mesh->setVertexNormals(normalsTmp4);

            mesh->setDefaultTCoords("tCoords");
            mesh->setPointDataArray("tCoords", UVsTmp4);

            mesh->setVertexTangents(tangentsVecTmp4);
            mesh->setIsColon(bSubdivsion);
            // END subdivision 4

            return mesh;
        }

        mesh->initialize(poses, triangles, normals, false);
        mesh->setVertexNormals(normals);   

        mesh->setDefaultTCoords("tCoords");
        mesh->setPointDataArray("tCoords", UVs);

        mesh->setVertexTangents(tangentsVec);
        mesh->setIsColon(bSubdivsion);
        return mesh;
    }
} // imstk
