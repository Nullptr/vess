/*=========================================================================

   Library: iMSTK

   Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
   & Imaging in Medicine, Rensselaer Polytechnic Institute.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0.txt

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

=========================================================================*/

#ifndef imstkCylinder_h
#define imstkCylinder_h

// imstk
#include "imstkAnalyticalGeometry.h"

namespace imstk
{
///
/// \class Cylinder
///
/// \brief Cylinder geometry
///
class Cylinder : public AnalyticalGeometry
{
public:
    ///
    /// \brief Constructor
    ///
    Cylinder() : AnalyticalGeometry(Type::Cylinder) {}

    ///
    /// \brief Default destructor
    ///
    ~Cylinder() = default;

    ///
    /// \brief Print the cylinder info
    ///
    void print() const override;

    ///
    /// \brief Returns the volume of the cylinder
    ///
    double getVolume() const override;

    ///
    /// \brief Returns the radius of the cylinder
    ///
    double getRadius(DataType type = DataType::PostTransform);

    ///
    /// \brief Sets the radius of the cylinder
    ///
    void setRadius(const double r);

    ///
    /// \brief Returns the length of the cylinder
    ///
    double getLength(DataType type = DataType::PostTransform);

    ///
    /// \brief Sets the length of the cylinder
    ///
    void setLength(const double r);

      ////< XZH addition
    ///
    /// \brief Sets initial positions from an array
    ///
    void setInitialVertexPositions(const StdVectorOfVec3d& vertices);
    void setInitialVertexPositionsChangeable(const StdVectorOfVec3d& vertices) { m_initialVertexPositions = vertices; }  ////< XZH 

    ///
    /// \brief Returns the vector of initial positions of the mesh vertices
    ///
    const StdVectorOfVec3d& getInitialVertexPositions() const;

    ///
    /// \brief Returns the initial position of a vertex given its index
    ///
    const Vec3d& getInitialVertexPosition(const size_t& vertNum) const;

    ///
    /// \brief Sets current vertex positions of the mesh from an array
    ///
    void setVertexPositions(const StdVectorOfVec3d& vertices);

    ///
    /// \brief Returns the vector of current positions of the mesh vertices
    ///
    const StdVectorOfVec3d& getVertexPositions(DataType type = DataType::PostTransform);
    StdVectorOfVec3d& getVertexPositionsChangeable(); // XZH

    ///
    /// \brief Set the current position of a vertex given its index to certain position
    ///
    void setVertexPosition(const size_t& vertNum, const Vec3d& pos);

    ///
    /// \brief Returns the position of a vertex given its index
    ///
    const Vec3d& getVertexPosition(const size_t& vertNum,
        DataType type = DataType::PostTransform);

protected:
    friend class VTKCylinderRenderDelegate;

    void applyScaling(const double s) override;
    void updatePostTransformData() override;

    double m_radius = 0.1; // 1.;              ///> Radius of the cylinder
    double m_length = 0.2; //  1.;              ///> Length of the cylinder
    double m_radiusPostTransform = 1.; ///> Radius of the cylinder oncee transform applied
    double m_lengthPostTransform = 1.; ///> Length of the cylinder onc transform applied
    std::vector<Color> m_vertexColors;

      ////< XZH addition params
    double m_loadFactor = 1.0;
    size_t m_maxNumVertices = 2;
    size_t m_originalNumVertices = 2;

    StdVectorOfVec3d m_initialVertexPositions;       ///> Initial positions of vertices
    StdVectorOfVec3d m_vertexPositions;              ///> Current positions of vertices
    StdVectorOfVec3d m_vertexPositionsPostTransform; ///> Positions of vertices after transform
};
} // imstk

#endif // ifndef imstkCylinder_h
