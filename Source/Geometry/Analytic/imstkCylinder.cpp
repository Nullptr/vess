/*=========================================================================

   Library: iMSTK

   Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
   & Imaging in Medicine, Rensselaer Polytechnic Institute.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0.txt

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

=========================================================================*/

#include "imstkCylinder.h"

#include "g3log/g3log.hpp"

namespace imstk
{
void
Cylinder::print() const
{
    AnalyticalGeometry::print();
    LOG(INFO) << "Radius: " << m_radius;
    LOG(INFO) << "Length: " << m_length;
}

double
Cylinder::getVolume() const
{
    return PI * m_radius * m_radius * m_length;
}

double
Cylinder::getRadius(DataType type /* = DataType::PostTransform */)
{
    if (type == DataType::PostTransform)
    {
        this->updatePostTransformData();
        return m_radiusPostTransform;
    }
    return m_radius;
}

double
Cylinder::getLength(DataType type /* = DataType::PostTransform */)
{
    if (type == DataType::PostTransform)
    {
        this->updatePostTransformData();
        return m_lengthPostTransform;
    }
    return m_length;
}

void
Cylinder::setRadius(const double r)
{
    if(r <= 0)
    {
        LOG(WARNING) << "Cylinder::setRadius error: radius should be positive.";
        return;
    }
    if (m_radius == r)
    {
        return;
    }
    m_radius = r;
    m_dataModified = true;
    m_transformApplied = false;
}

void
Cylinder::setLength(const double l)
{
    if (l <= 0)
    {
        LOG(WARNING) << "Cylinder::setLength error: length should be positive.";
        return;
    }
    if (m_length == l)
    {
        return;
    }
    m_length = l;
    m_dataModified = true;
    m_transformApplied = false;
}

void
Cylinder::applyScaling(const double s)
{
    this->setRadius(m_radius * s);
    this->setLength(m_length * s);
}

void
Cylinder::updatePostTransformData()
{
    if (m_transformApplied)
    {
        return;
    }
    //AnalyticalGeometry::updatePostTransformData();
    m_radiusPostTransform = m_scaling * m_radius;
    m_lengthPostTransform = m_scaling * m_length;
    m_vertexPositionsPostTransform = m_initialVertexPositions;

    // if identity ,return
    if ((m_transform(0, 0) == 1) && ((m_transform(1, 1) == 1)) && ((m_transform(2, 2) == 1)) && ((m_transform(3, 3) == 1)))
    {
        m_vertexPositionsPostTransform = m_initialVertexPositions;
        return; // no change
    }
    //// XZH END
    for (size_t i = 0; i < m_vertexPositions.size(); ++i)
    {
        m_vertexPositionsPostTransform[i] = m_transform * (m_initialVertexPositions[i] * m_scaling);
    }
    m_transformApplied = true;
}

void
Cylinder::setInitialVertexPositions(const StdVectorOfVec3d& vertices)
{
    if (m_originalNumVertices == 0)
    {
        m_initialVertexPositions = vertices;
        m_originalNumVertices = vertices.size();
        m_maxNumVertices = (size_t)(m_originalNumVertices * m_loadFactor);
        m_vertexPositions.reserve(m_maxNumVertices);
    }
    else
    {
        LOG(WARNING) << "Already set initial vertices";
    }
}

const StdVectorOfVec3d&
Cylinder::getInitialVertexPositions() const
{
    return m_initialVertexPositions;
}

const Vec3d&
Cylinder::getInitialVertexPosition(const size_t& vertNum) const
{
    return m_initialVertexPositions.at(vertNum);
}

void
Cylinder::setVertexPositions(const StdVectorOfVec3d& vertices)
{
    if (vertices.size() <= m_maxNumVertices)
    {
        m_vertexPositions = vertices;
        while (m_vertexColors.size() < m_vertexPositions.size())
        {
            m_vertexColors.push_back(Color::White);
        }
        m_dataModified = true;
        m_transformApplied = false;
    }
    else
    {
        LOG(WARNING) << "Vertices not set, exceeded maximum number of vertices" << vertices.size() << "," << m_maxNumVertices << "";
    }
}

const StdVectorOfVec3d&
Cylinder::getVertexPositions(DataType type /* = DataType::PostTransform */)
{
    if (type == DataType::PostTransform)
    {
        this->updatePostTransformData();
        return m_vertexPositionsPostTransform;
    }
    return m_vertexPositions;
}

StdVectorOfVec3d&
Cylinder::getVertexPositionsChangeable()
{
    return m_vertexPositions;
}

void
Cylinder::setVertexPosition(const size_t& vertNum, const Vec3d& pos)
{
    m_vertexPositions.at(vertNum) = pos;
    m_dataModified = true;
    m_transformApplied = false; // or no need this line XZH  when we don't change the transformation matrix, then we don't need to set this to false, transformation matrix is just suitable for rigid body
}

const Vec3d&
Cylinder::getVertexPosition(const size_t& vertNum, DataType type)
{
    return m_vertexPositions.at(vertNum); // XZH return this->getVertexPositions(type).at(vertNum);  //  // Kitware  return this->getVertexPositions(type).at(vertNum);
}

} // imstk
