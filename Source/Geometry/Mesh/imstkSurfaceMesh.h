/*=========================================================================

   Library: iMSTK

   Copyright (c) Kitware, Inc. & Center for Modeling, Simulation,
   & Imaging in Medicine, Rensselaer Polytechnic Institute.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0.txt

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

=========================================================================*/

#ifndef imstkSurfaceMesh_h
#define imstkSurfaceMesh_h

// std library
#include <array>
#include <list>
#include <set>
#include <unordered_set>
#include <memory>

// imstk
#include "imstkPointSet.h"

namespace imstk
{
///
/// \brief Helper class for indentifying duplicate points
///
struct NormalGroup
{
    Vec3d position;
    Vec3d normal;
};
}

// This method is defined to allow for the map to be properly indexed by Texture objects
namespace std
{
template<> struct less<imstk::NormalGroup>
{
    bool operator() (const imstk::NormalGroup& group1,
                     const imstk::NormalGroup& group2) const
    {
        if (group1.position != group2.position)
        {
            return (group1.position.x() < group2.position.x());
        }

        if (group1.normal != group2.normal)
        {
            return (group1.normal.x() < group2.normal.x());
        }

        return false;
    }
};
}

namespace imstk {
///
/// \class SurfaceMesh
///
/// \brief Surface triangular mesh
///
class SurfaceMesh : public PointSet
{
public:

    using TriangleArray = std::array<size_t, 3>;
    using NeighborsType = std::set<size_t>;

    ///
    /// \brief Constructor
    ///
    SurfaceMesh() : PointSet(Geometry::Type::SurfaceMesh) {}

    ///
    /// \brief Default destructor
    ///
    ~SurfaceMesh() = default;

    ///
    /// \brief Initializes the rest of the data structures given vertex positions and
    ///  triangle connectivity
    ///
    void initialize(const StdVectorOfVec3d& vertices,
                    const std::vector<TriangleArray>& triangles,
                    const bool computeDerivedData = false);

    ///
    /// \brief Initializes the rest of the data structures given vertex positions,
    ///  triangle connectivity, and normals
    ///
    void initialize(const StdVectorOfVec3d& vertices,
                    const std::vector<TriangleArray>& triangles,
                    const StdVectorOfVec3d& normals,
                    const bool computeDerivedData = false);

    ///
    /// \brief Clear all the mesh data
    ///
    void clear() override;

    ///
    /// \brief Print the surface mesh
    ///
    void print() const override;

    ///
    /// \brief Get the volume enclosed by the surface mesh
    ///
    double getVolume() const override;

    ///
    /// \brief Computes neighboring triangles for all vertices
    ///
    void computeVertexNeighborTriangles();

    ///
    /// \brief Computes neighboring vertices for all vertices
    ///
    void computeVertexNeighborVertices();

    ///
    /// \brief Compute the normals to the triangles
    ///
    void computeTrianglesNormals();

    ///
    /// \brief Computes the normals of all the vertices
    ///
    void computeVertexNormals();

    ///
    /// \brief Rewire the node order and triangle connectivity to optimize for memory layout
    ///  The intended use is for large meshes that doesn't fit into CPU/GPU memory.
    ///  TODO: Further optimization to find a 1-d uninterrupted sub-graph at each iteration.
    ///
    void optimizeForDataLocality();

    // Accessors

    ///
    /// \brief Get/Set triangle connectivity
    ///
    void setTrianglesVertices(const std::vector<TriangleArray>& triangles);
    const std::vector<TriangleArray>& getTrianglesVertices() const;
    std::vector<TriangleArray>& getTrianglesVerticesChangeable(); // XZH
    void setTrianglesVerticesXZH(const std::vector<TriangleArray>& triangles) { m_trianglesVerticesXZH = triangles; }
    std::vector<TriangleArray>& getTrianglesVerticesXZH() { return m_trianglesVerticesXZH; }
    void setTrianglesStatesXZH(const std::vector<int>& triangles) { m_trianglesStatesXZH = triangles; }
    std::vector<int>& getTrianglesStatesXZH() { return m_trianglesStatesXZH; }
    void setTrianglesStatesDissectionXZH(const std::vector<int>& triangles) { m_trianglesStatesDissectionXZH = triangles; }
    std::vector<int>& getTrianglesStatesDissectionXZH() { return m_trianglesStatesDissectionXZH; }
    void setVertexPositionsXZH(const StdVectorOfVec3d& vertices){ m_vertPosionsXZH = vertices; }
    StdVectorOfVec3d& getVertexPositionsXZH(){ return m_vertPosionsXZH; }
    void setIsColon(bool flag){ isColon = flag; }
    bool getIsColon(){ return isColon; }

    ///
    /// \brief Get vector of normals of all the triangles
    ///
    const StdVectorOfVec3d& getTriangleNormals() const;

    ///
    /// \brief Get normal of a triangle given its index
    ///
    const Vec3d& getTriangleNormal(size_t i) const;

    ///
    /// \brief Set/Get vector of normals of all the vertices
    ///
    void setVertexNormals(const StdVectorOfVec3d& normals);
    const StdVectorOfVec3d& getVertexNormals() const;

    ///
    /// \brief Set/Get vector of tangents of all the vertices
    ///
    void setVertexTangents(const StdVectorOfVec3d& tangents);
    const StdVectorOfVec3d& getVertexTangents() const;

    void setVertexColor(int index, Color color);
    const std::vector<Color>& getVertexColors() const;

    ///
    /// \brief Returns the number of triangles
    ///
    size_t getNumTriangles() const;

    ///
    /// \brief Set/Get the array defining the default material coordinates
    ///
    void setDefaultTCoords(std::string arrayName);
    std::string getDefaultTCoords();

    ///
    /// \brief Flip the normals for the whole mesh by reversing the winding order
    ///
    void flipNormals();

    ///
    /// \brief Enforces consistency in the winding order of the triangles
    ///
    void correctWindingOrder();

    ///
    /// \brief Finds vertices along vertex seams that share geometric properties
    ///
    void computeUVSeamVertexGroups();

    ///
    /// \brief Set load factor
    /// \param loadFactor the maximum number of vertices; a multiple of the original vertex count
    ///
    virtual void setLoadFactor(double loadFactor);

    ///
    /// \brief Get the maximum number of triangles
    ///
    size_t getMaxNumTriangles();

    ///
    /// \brief Set/Get vector of operation area of triangles XZH
    ///
    void setOperationTriangles(std::vector<int>& optTriangles) { m_optTriangles = optTriangles; }
    std::vector<int>& getOperationTriangles() { return m_optTriangles; }
    /// \brief Returns the mesh graph
    ///
    Graph getMeshGraph() override;
    ///
    /// \brief introduce the function XZH each vertex has its neighbors
    ///
    std::vector<NeighborsType>& getNeighborTriangles() { return m_vertexNeighborTriangles; }

protected:

    friend class VTKSurfaceMeshRenderDelegate;

    ///
    /// \brief Get vertex normals
    ///
    StdVectorOfVec3d& getVertexNormalsNotConst();

    std::vector<TriangleArray> m_trianglesVertices; ///> Triangle connectivity
    std::vector<TriangleArray> m_trianglesVerticesXZH; ///> Triangle connectivity
    std::vector<int> m_trianglesStatesXZH; ///> Triangle connectivity
    std::vector<int> m_trianglesStatesDissectionXZH; ///> Triangle connectivity
    StdVectorOfVec3d m_vertPosionsXZH; ///> Normals to the triangles

    std::vector<NeighborsType> m_vertexNeighborTriangles; ///> Neighbor triangles to vertices
    std::vector<NeighborsType> m_vertexNeighborVertices; ///> Neighbor vertices to vertices

    StdVectorOfVec3d m_triangleNormals; ///> Normals to the triangles
    StdVectorOfVec3d m_triangleTangents; ///> Tangents to the triangles
    StdVectorOfVec3d m_vertexNormals; ///> Normals of the vertices
    StdVectorOfVec3d m_vertexTangents; ///> Tangents of the vertices

    std::map<NormalGroup, std::shared_ptr<std::vector<size_t>>> m_UVSeamVertexGroups;

    std::string m_defaultTCoords = ""; ///> Name of the array used as default material coordinates

    size_t m_originalNumTriangles = 0;
    size_t m_maxNumTriangles = 0;

    std::vector<int> m_optTriangles;   ////< XZH for operation, e.g., cutting?
    bool isInited = false;  ////< XZH whether it is initialized, not ing, is ed
    bool isColon = false;  ////< XZH whether it is the operating colon
};
} // imstk

#endif // ifndef imstkSurfaceMesh_h
